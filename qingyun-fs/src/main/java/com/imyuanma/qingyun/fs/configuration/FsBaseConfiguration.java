package com.imyuanma.qingyun.fs.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.io.File;

/**
 * 文件系统配置
 *
 * @author wangjy
 * @date 2022/07/24 00:22:34
 */
@Configuration
@ConfigurationProperties(prefix = "qingyun.fs")
public class FsBaseConfiguration {
    /**
     * linux环境根目录
     */
    private String rootDirLinux;
    /**
     * windows环境根目录
     */
    private String rootDirWindows;

    public String getRootDir() {
        return "/".equals(File.separator) ? this.getRootDirLinux() : this.getRootDirWindows();
    }

    public String getRootDirLinux() {
        return rootDirLinux;
    }

    public void setRootDirLinux(String rootDirLinux) {
        this.rootDirLinux = rootDirLinux;
    }

    public String getRootDirWindows() {
        return rootDirWindows;
    }

    public void setRootDirWindows(String rootDirWindows) {
        this.rootDirWindows = rootDirWindows;
    }
}
