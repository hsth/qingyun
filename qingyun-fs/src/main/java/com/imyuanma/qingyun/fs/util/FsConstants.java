package com.imyuanma.qingyun.fs.util;

/**
 * 文件系统常量
 *
 * @author wangjy
 * @date 2023/07/29 15:22:49
 */
public class FsConstants {
    /**
     * 根目录id
     */
    public static final String ROOT_FOLDER_ID = "root";
}
