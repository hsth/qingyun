package com.imyuanma.qingyun.fs.util;

import com.imyuanma.qingyun.fs.configuration.FsBaseConfiguration;
import com.imyuanma.qingyun.fs.configuration.FsCosConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 配置bean持有
 *
 * @author wangjy
 * @date 2023/07/30 11:23:14
 */
@Component
public class FsConfigurationHolder {
    /**
     * 文件系统配置信息
     */
    private static FsBaseConfiguration fsBaseConfiguration = null;
    /**
     * 腾讯云cos对象存储配置
     */
    private static FsCosConfiguration fsCosConfiguration = null;

    @Autowired
    private void setFsBaseConfiguration(FsBaseConfiguration fsBaseConfiguration) {
        FsConfigurationHolder.fsBaseConfiguration = fsBaseConfiguration;
    }

    @Autowired
    private void setFsCosConfiguration(FsCosConfiguration fsCosConfiguration) {
        FsConfigurationHolder.fsCosConfiguration = fsCosConfiguration;
    }

    /**
     * 获取文件系统配置bean
     *
     * @return
     */
    public static FsBaseConfiguration getFsBaseConfiguration() {
        return fsBaseConfiguration;
    }

    /**
     * 腾讯云cos对象存储配置
     * @return
     */
    public static FsCosConfiguration getFsCosConfiguration() {
        return fsCosConfiguration;
    }
}
