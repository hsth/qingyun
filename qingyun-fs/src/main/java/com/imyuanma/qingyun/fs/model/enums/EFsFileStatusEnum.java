package com.imyuanma.qingyun.fs.model.enums;

/**
 * 文件状态
 *
 * @author wangjy
 * @date 2022/07/24 11:41:09
 */
public enum EFsFileStatusEnum {
    NORMAL("NORMAL", "正常")
    ;
    private String code;
    private String text;

    EFsFileStatusEnum(String code, String text) {
        this.code = code;
        this.text = text;
    }

    public String getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
