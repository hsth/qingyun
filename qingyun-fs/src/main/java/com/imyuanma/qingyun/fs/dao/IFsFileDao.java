package com.imyuanma.qingyun.fs.dao;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.fs.model.FsFile;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 文件信息dao
 *
 * @author YuanMaKeJi
 * @date 2023-07-23 00:20:38
 */
@Mapper
public interface IFsFileDao {

    /**
     * 列表查询
     *
     * @param fsFile 查询条件
     * @return
     */
    @Trace("查询文件信息列表")
    List<FsFile> getList(FsFile fsFile);

    /**
     * 分页查询
     *
     * @param fsFile 查询条件
     * @param pageQuery 分页参数
     * @return
     */
    @Trace("分页查询文件信息")
    List<FsFile> getList(FsFile fsFile, PageQuery pageQuery);

    /**
     * 统计数量
     *
     * @param fsFile 查询条件
     * @return
     */
    @Trace("统计文件信息数量")
    int count(FsFile fsFile);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    @Trace("主键查询文件信息")
    FsFile get(String id);

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    @Trace("主键批量查询文件信息")
    List<FsFile> getListByIds(List<String> list);

    /**
     * 根据status查询
     *
     * @param status 状态,隐藏/只读..
     * @return
     */
    @Trace("根据status查询文件信息")
    List<FsFile> getListByStatus(@Param("status") String status);

    /**
     * 根据mediaType查询
     *
     * @param mediaType 类型,图片/视频..
     * @return
     */
    @Trace("根据mediaType查询文件信息")
    List<FsFile> getListByMediaType(@Param("mediaType") String mediaType);

    /**
     * 根据folderId查询
     *
     * @param folderId 所属文件夹
     * @return
     */
    @Trace("根据folderId查询文件信息")
    List<FsFile> getListByFolderId(@Param("folderId") String folderId);

    /**
     * 根据fileType查询
     *
     * @param fileType 文件类型,1:文件,2:文件夹
     * @return
     */
    @Trace("根据fileType查询文件信息")
    List<FsFile> getListByFileType(@Param("fileType") String fileType);

    /**
     * 根据businessCode查询
     *
     * @param businessCode 业务编号
     * @return
     */
    @Trace("根据businessCode查询文件信息")
    List<FsFile> getListByBusinessCode(@Param("businessCode") String businessCode);

    /**
     * 根据storageMode查询
     *
     * @param storageMode 存储方式
     * @return
     */
    @Trace("根据storageMode查询文件信息")
    List<FsFile> getListByStorageMode(@Param("storageMode") String storageMode);

    /**
     * 插入
     *
     * @param fsFile 参数
     * @return
     */
    @Trace("插入文件信息")
    int insert(FsFile fsFile);

    /**
     * 选择性插入
     *
     * @param fsFile 参数
     * @return
     */
    @Trace("选择性插入文件信息")
    int insertSelective(FsFile fsFile);

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量插入文件信息")
    int batchInsert(List<FsFile> list);

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量选择性插入文件信息")
    int batchInsertSelective(List<FsFile> list);

    /**
     * 修改
     *
     * @param fsFile 参数
     * @return
     */
    @Trace("修改文件信息")
    int update(FsFile fsFile);

    /**
     * 选择性修改
     *
     * @param fsFile 参数
     * @return
     */
    @Trace("选择性修改文件信息")
    int updateSelective(FsFile fsFile);

    /**
     * 批量修改某字段
     *
     * @param idList      主键集合
     * @param fieldDbName 待修改的字段名
     * @param fieldValue  修改后的值
     * @return
     */
    @Trace("批量修改文件信息属性")
    int batchUpdateColumn(@Param("idList") List<String> idList, @Param("fieldDbName") String fieldDbName, @Param("fieldValue") Object fieldValue);

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    @Trace("删除文件信息")
    int delete(String id);

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    @Trace("批量删除文件信息")
    int batchDelete(List<String> list);

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param fsFile 参数
     * @return
     */
    @Trace("条件删除文件信息")
    int deleteByCondition(FsFile fsFile);

    /**
     * 删除全部
     *
     * @return
     */
    @Trace("全量删除文件信息")
    int deleteAll();

}
