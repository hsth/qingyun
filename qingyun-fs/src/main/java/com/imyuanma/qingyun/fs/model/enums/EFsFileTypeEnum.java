package com.imyuanma.qingyun.fs.model.enums;

/**
 * 文件类型
 *
 * @author wangjy
 * @date 2022/07/24 11:17:40
 */
public enum EFsFileTypeEnum {
    FILE("FILE", "文件"),
    FOLDER("FOLDER", "文件夹"),
    ;
    private String code;
    private String text;

    EFsFileTypeEnum(String code, String text) {
        this.code = code;
        this.text = text;
    }

    public String getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
