package com.imyuanma.qingyun.fs.controller;

import com.imyuanma.qingyun.common.core.structure.tree.TreeBuilder;
import com.imyuanma.qingyun.common.factory.BaseDOBuilder;
import com.imyuanma.qingyun.common.model.request.WebRequest;
import com.imyuanma.qingyun.common.model.response.Page;
import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.common.util.StringUtil;
import com.imyuanma.qingyun.fs.model.FsFile;
import com.imyuanma.qingyun.fs.model.FsFolder;
import com.imyuanma.qingyun.fs.model.enums.EFsFileStatusEnum;
import com.imyuanma.qingyun.fs.model.enums.EFsFileTypeEnum;
import com.imyuanma.qingyun.fs.model.enums.EFsMediaType;
import com.imyuanma.qingyun.fs.model.enums.EFsStorageModeEnum;
import com.imyuanma.qingyun.fs.service.IFsFileService;
import com.imyuanma.qingyun.fs.util.FsBusinessUtil;
import com.imyuanma.qingyun.fs.util.ImageUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.tencent.cloud.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 文件信息web
 *
 * @author YuanMaKeJi
 * @date 2022-07-24 00:05:08
 */
@RestController
@RequestMapping(value = "/fs/fsFile")
public class FsFileController {

    /**
     * 文件信息服务
     */
    @Autowired
    private IFsFileService fsFileService;

    @Trace("查询文件夹树")
    @PostMapping("/getFolderTree")
    public Result<List<FsFolder>> getFolderTree(@RequestBody WebRequest<FsFile> webRequest) {
        List<FsFile> list = fsFileService.getListByFileType(EFsFileTypeEnum.FOLDER.getCode());
        if (CollectionUtil.isNotEmpty(list)) {
            List<FsFolder> folders = list.stream().map(FsFolder::of).collect(Collectors.toList());
            return Result.success(TreeBuilder.buildTree(folders));
        }
        return Result.success(new ArrayList<>());
    }

    /**
     * 列表查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("查询文件信息列表")
    @PostMapping("/getList")
    public Result<List<FsFile>> getList(@RequestBody WebRequest<FsFile> webRequest) {
        return Result.success(fsFileService.getList(webRequest.getBody()));
    }

    /**
     * 分页查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("分页查询文件信息")
    @PostMapping("/getPage")
    public Page<List<FsFile>> getPage(@RequestBody WebRequest<FsFile> webRequest) {
        PageQuery pageQuery = webRequest.buildPageQuery();
        List<FsFile> list = fsFileService.getPage(webRequest.getBody(), pageQuery);
        return Page.success(list, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("统计文件信息数量")
    @PostMapping("/count")
    public Result<Integer> count(@RequestBody WebRequest<FsFile> webRequest) {
        return Result.success(fsFileService.count(webRequest.getBody()));
    }

    /**
     * 查询
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("查询文件信息")
    @PostMapping("/get")
    public Result<FsFile> get(@RequestBody WebRequest<String> webRequest) {
        AssertUtil.notNull(webRequest.getBody());
        return Result.success(fsFileService.get(webRequest.getBody()));
    }

    /**
     * 新增
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("新增文件信息")
    @PostMapping("/insert")
    public Result insert(@RequestBody WebRequest<FsFile> webRequest) {
        FsFile fsFile = webRequest.getBody();
        AssertUtil.notNull(fsFile);
        BaseDOBuilder.fillBaseDOForInsert(fsFile);
        fsFileService.insertSelective(fsFile);
        return Result.success();
    }

    @Trace("COS上传成功后写入记录")
    @PostMapping("/insertAfterCosUpload")
    public Result<FsFile> writeFileRecordAfterSuccess(@RequestBody WebRequest<FsFile> webRequest) {
        FsFile fsFile = webRequest.getBody();
        AssertUtil.notNull(fsFile);
        BaseDOBuilder.fillBaseDOForInsert(fsFile);
        // 后缀
        String suffix = FsBusinessUtil.getFileSuffixContainPoint(fsFile.getName());
        EFsMediaType mediaType = EFsMediaType.ofSuffix(suffix);
        // 媒体类型
        fsFile.setMediaType(mediaType.getCode());
        // 后缀
        fsFile.setNameSuffix(suffix);
        // 存储方式
        fsFile.setStorageMode(EFsStorageModeEnum.CODE_cos.getCode());
        fsFileService.insertSelective(fsFile);
        return Result.success(fsFile);
    }

    /**
     * 修改
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("修改文件信息")
    @PostMapping("/update")
    public Result update(@RequestBody WebRequest<FsFile> webRequest) {
        FsFile fsFile = webRequest.getBody();
        AssertUtil.notNull(fsFile);
        AssertUtil.notNull(fsFile.getId());
        BaseDOBuilder.fillBaseDOForUpdate(fsFile);
        fsFileService.updateSelective(fsFile);
        return Result.success();
    }

    /**
     * 删除
     *
     * @param webRequest 参数, 待删除主键,多个使用英文逗号拼接
     * @return
     */
    @Trace("删除文件信息")
    @PostMapping("/delete")
    public Result delete(@RequestBody WebRequest<String> webRequest) {
        String ids = webRequest.getBody();
        AssertUtil.notBlank(ids);
        if (ids.contains(",")) {
            fsFileService.batchDelete(Arrays.stream(ids.split(",")).collect(Collectors.toList()));
        } else {
            fsFileService.delete(ids);
        }
        return Result.success();
    }

}
