package com.imyuanma.qingyun.fs.model;

import com.imyuanma.qingyun.common.util.StringUtil;
import com.imyuanma.qingyun.fs.model.enums.EFsFileTypeEnum;
import com.imyuanma.qingyun.fs.util.FsBusinessUtil;
import lombok.Data;

import java.util.Date;

import com.imyuanma.qingyun.interfaces.common.model.BaseDO;

/**
 * 文件信息实体类
 *
 * @author YuanMaKeJi
 * @date 2022-07-24 00:05:08
 */
@Data
public class FsFile extends BaseDO {

    /**
     * 编号
     */
    private String id;

    /**
     * 文件(夹)名称
     */
    private String name;
    /**
     * 实际磁盘里保存的名字
     * 例如上传时用户的文件叫: 测试.txt, 则name=测试.txt, saveName=ewqhdjsahduisa.txt
     */
    private String saveName;

    /**
     * 状态,隐藏/只读..
     */
    private String status;

    /**
     * 备注
     */
    private String remark;

    /**
     * 类型,图片/视频..
     */
    private String mediaType;
    /**
     * 图片宽度,像素
     */
    private Integer imageWidth;
    /**
     * 图片高度,像素
     */
    private Integer imageHeight;

    /**
     * 所属文件夹
     */
    private String folderId;

    /**
     * 文件大小, bytes
     */
    private Long fileSize;

    /**
     * 文件类型
     */
    private String fileType;

    /**
     * 物理地址,相对路径
     */
    private String addr;
    /**
     * 访问路径
     */
    private String path;

    /**
     * 业务编号
     */
    private String businessCode;
    /**
     * 后缀名
     */
    private String nameSuffix;
    /**
     * 存储方式
     */
    private String storageMode;
    /**
     * 存储配置
     */
    private String storageConfig;
    /**
     * 扩展信息
     */
    private String extInfo;


    /**
     * 物理地址,绝对路径
     * 数据库没有存储这个字段
     */
    private String absoluteAddr;

//    /**
//     * 补全文件绝对路径
//     *
//     * @param rootDir
//     */
//    public void completionAbsoluteAddr(String rootDir) {
//        if (StringUtil.isNotBlank(rootDir)) {
//            this.absoluteAddr = rootDir + this.addr;
//        }
//    }

    /**
     * 补全绝对路径
     */
    public void completionAbsoluteAddr() {
        this.absoluteAddr = FsBusinessUtil.calcAbsoluteAddr(this);
    }

    public String getAbsoluteAddr() {
        if (StringUtil.isBlank(this.absoluteAddr)) {
            this.completionAbsoluteAddr();
        }
        return this.absoluteAddr;
    }

    /**
     * 判断是否文件夹
     *
     * @return
     */
    public boolean isFolder() {
        return EFsFileTypeEnum.FOLDER.getCode().equals(this.fileType);
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("{");
        sb.append("\"id\":").append(id != null ? "\"" : "").append(id).append(id != null ? "\"" : "");
        sb.append(", \"name\":").append(name != null ? "\"" : "").append(name).append(name != null ? "\"" : "");
        sb.append(", \"saveName\":").append(saveName != null ? "\"" : "").append(saveName).append(saveName != null ? "\"" : "");
        sb.append(", \"status\":").append(status != null ? "\"" : "").append(status).append(status != null ? "\"" : "");
        sb.append(", \"remark\":").append(remark != null ? "\"" : "").append(remark).append(remark != null ? "\"" : "");
        sb.append(", \"mediaType\":").append(mediaType != null ? "\"" : "").append(mediaType).append(mediaType != null ? "\"" : "");
        sb.append(", \"folderId\":").append(folderId != null ? "\"" : "").append(folderId).append(folderId != null ? "\"" : "");
        sb.append(", \"fileSize\":").append(fileSize);
        sb.append(", \"fileType\":").append(fileType != null ? "\"" : "").append(fileType).append(fileType != null ? "\"" : "");
        sb.append(", \"addr\":").append(addr != null ? "\"" : "").append(addr).append(addr != null ? "\"" : "");
        sb.append(", \"path\":").append(path != null ? "\"" : "").append(path).append(path != null ? "\"" : "");
        sb.append(", \"businessCode\":").append(businessCode != null ? "\"" : "").append(businessCode).append(businessCode != null ? "\"" : "");
        sb.append('}');
        return sb.toString();
    }
}