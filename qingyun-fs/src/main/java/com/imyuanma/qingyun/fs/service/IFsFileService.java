package com.imyuanma.qingyun.fs.service;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.fs.model.FsFile;

import java.util.List;

/**
 * 文件信息服务
 *
 * @author YuanMaKeJi
 * @date 2022-07-24 00:05:08
 */
public interface IFsFileService {

    /**
     * 获取根节点信息
     *
     * @return
     */
    FsFile getRoot();

    /**
     * 列表查询
     *
     * @param fsFile 查询条件
     * @return
     */
    List<FsFile> getList(FsFile fsFile);

    /**
     * 分页查询
     *
     * @param fsFile    查询条件
     * @param pageQuery 分页参数
     * @return
     */
    List<FsFile> getPage(FsFile fsFile, PageQuery pageQuery);

    /**
     * 统计数量
     *
     * @param fsFile 查询条件
     * @return
     */
    int count(FsFile fsFile);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    FsFile get(String id);

    /**
     * 根据文件夹id查询下面的文件
     *
     * @param folderId 文件夹id
     * @return
     */
    List<FsFile> getListByFolderId(String folderId);

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    List<FsFile> getListByIds(List<String> list);

    /**
     * 根据status查询
     *
     * @param status 状态,隐藏/只读..
     * @return
     */
    List<FsFile> getListByStatus(String status);

    /**
     * 根据mediaType查询
     *
     * @param mediaType 类型,图片/视频..
     * @return
     */
    List<FsFile> getListByMediaType(String mediaType);

    /**
     * 根据fileType查询
     *
     * @param fileType 文件类型,1:文件,2:文件夹
     * @return
     */
    List<FsFile> getListByFileType(String fileType);

    /**
     * 根据businessCode查询
     *
     * @param businessCode 业务编号
     * @return
     */
    List<FsFile> getListByBusinessCode(String businessCode);

    /**
     * 根据storageMode查询
     *
     * @param storageMode 存储方式
     * @return
     */
    List<FsFile> getListByStorageMode(String storageMode);

    /**
     * 插入
     *
     * @param fsFile 参数
     * @return
     */
    int insert(FsFile fsFile);

    /**
     * 选择性插入
     *
     * @param fsFile 参数
     * @return
     */
    int insertSelective(FsFile fsFile);

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    int batchInsert(List<FsFile> list);

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    int batchInsertSelective(List<FsFile> list);

    /**
     * 修改
     *
     * @param fsFile 参数
     * @return
     */
    int update(FsFile fsFile);

    /**
     * 选择性修改
     *
     * @param fsFile 参数
     * @return
     */
    int updateSelective(FsFile fsFile);

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    int delete(String id);

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    int batchDelete(List<String> list);

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param fsFile 参数
     * @return
     */
    int deleteByCondition(FsFile fsFile);


    /**
     * 删除文件夹下的文件信息
     *
     * @param folderId
     * @return
     */
    int deleteAllByFolder(String folderId);

}
