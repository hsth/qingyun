package com.imyuanma.qingyun.fs.service.impl;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.common.util.StringUtil;
import com.imyuanma.qingyun.fs.configuration.FsBaseConfiguration;
import com.imyuanma.qingyun.fs.dao.IFsFileDao;
import com.imyuanma.qingyun.fs.model.FsFile;
import com.imyuanma.qingyun.fs.model.enums.EFsFileStatusEnum;
import com.imyuanma.qingyun.fs.model.enums.EFsFileTypeEnum;
import com.imyuanma.qingyun.fs.model.enums.EFsMediaType;
import com.imyuanma.qingyun.fs.model.enums.EFsStorageModeEnum;
import com.imyuanma.qingyun.fs.service.IFsFileService;
import com.imyuanma.qingyun.fs.util.FsBusinessUtil;
import com.imyuanma.qingyun.fs.util.FsConstants;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 文件信息服务
 *
 * @author YuanMaKeJi
 * @date 2022-07-24 00:05:08
 */
@Slf4j
@Service
public class FsFileServiceImpl implements IFsFileService {

    /**
     * 文件信息dao
     */
    @Autowired
    private IFsFileDao fsFileDao;
    /**
     * 文件配置
     */
    @Autowired
    private FsBaseConfiguration fsBaseConfiguration;

    /**
     * 获取根节点信息
     *
     * @return
     */
    @Trace("获取根节点信息")
    @Override
    public FsFile getRoot() {
        return this.get(FsConstants.ROOT_FOLDER_ID);
    }

    /**
     * 列表查询
     *
     * @param fsFile 查询条件
     * @return
     */
    @Trace("查询文件信息列表")
    @Override
    public List<FsFile> getList(FsFile fsFile) {
        List<FsFile> list = fsFileDao.getList(fsFile);
        FsBusinessUtil.completionAbsoluteAddr(list);
        return list;
    }

    /**
     * 分页查询
     *
     * @param fsFile    查询条件
     * @param pageQuery 分页参数
     * @return
     */
    @Trace("分页查询文件信息")
    @Override
    public List<FsFile> getPage(FsFile fsFile, PageQuery pageQuery) {
        List<FsFile> list = fsFileDao.getList(fsFile, pageQuery);
        FsBusinessUtil.completionAbsoluteAddr(list);
        return list;
    }

    /**
     * 统计数量
     *
     * @param fsFile 查询条件
     * @return
     */
    @Trace("统计文件信息数量")
    @Override
    public int count(FsFile fsFile) {
        return fsFileDao.count(fsFile);
    }

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    @Trace("主键查询文件信息")
    @Override
    public FsFile get(String id) {
        FsFile fsFile = fsFileDao.get(id);
        FsBusinessUtil.completionAbsoluteAddr(fsFile);
        return fsFile;
    }

    /**
     * 根据文件夹id查询下面的文件
     *
     * @param folderId 文件夹id
     * @return
     */
    @Trace("根据folderId查询文件信息")
    @Override
    public List<FsFile> getListByFolderId(String folderId) {
        List<FsFile> list = fsFileDao.getListByFolderId(folderId);
        FsBusinessUtil.completionAbsoluteAddr(list);
        return list;
    }



    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    @Trace("主键批量查询文件信息")
    @Override
    public List<FsFile> getListByIds(List<String> list) {
        List<FsFile> result = fsFileDao.getListByIds(list);
        FsBusinessUtil.completionAbsoluteAddr(result);
        return result;
    }

    /**
     * 根据status查询
     *
     * @param status 状态,隐藏/只读..
     * @return
     */
    @Trace("根据status查询文件信息")
    @Override
    public List<FsFile> getListByStatus(String status) {
        List<FsFile> list = fsFileDao.getListByStatus(status);
        FsBusinessUtil.completionAbsoluteAddr(list);
        return list;
    }

    /**
     * 根据mediaType查询
     *
     * @param mediaType 类型,图片/视频..
     * @return
     */
    @Trace("根据mediaType查询文件信息")
    @Override
    public List<FsFile> getListByMediaType(String mediaType) {
        List<FsFile> list = fsFileDao.getListByMediaType(mediaType);
        FsBusinessUtil.completionAbsoluteAddr(list);
        return list;
    }

    /**
     * 根据fileType查询
     *
     * @param fileType 文件类型,1:文件,2:文件夹
     * @return
     */
    @Trace("根据fileType查询文件信息")
    @Override
    public List<FsFile> getListByFileType(String fileType) {
        List<FsFile> list = fsFileDao.getListByFileType(fileType);
        FsBusinessUtil.completionAbsoluteAddr(list);
        return list;
    }

    /**
     * 根据businessCode查询
     *
     * @param businessCode 业务编号
     * @return
     */
    @Trace("根据businessCode查询文件信息")
    @Override
    public List<FsFile> getListByBusinessCode(String businessCode) {
        List<FsFile> list = fsFileDao.getListByBusinessCode(businessCode);
        FsBusinessUtil.completionAbsoluteAddr(list);
        return list;
    }

    /**
     * 根据storageMode查询
     *
     * @param storageMode 存储方式
     * @return
     */
    @Trace("根据storageMode查询文件信息")
    @Override
    public List<FsFile> getListByStorageMode(String storageMode) {
        List<FsFile> list = fsFileDao.getListByStorageMode(storageMode);
        FsBusinessUtil.completionAbsoluteAddr(list);
        return list;
    }

    /**
     * 插入
     *
     * @param fsFile 参数
     * @return
     */
    @Trace("插入文件信息")
    @Override
    public int insert(FsFile fsFile) {
        return fsFileDao.insert(fsFile);
    }

    /**
     * 选择性插入
     *
     * @param fsFile 参数
     * @return
     */
    @Trace("选择性插入文件信息")
    @Override
    public int insertSelective(FsFile fsFile) {
        AssertUtil.notBlank(fsFile.getId(), "id can not blank");
        AssertUtil.notBlank(fsFile.getFileType(), "fileType can not blank");
//        if (StringUtil.isBlank(fsFile.getId())) {
//            fsFile.setId(StringUtil.getUUID());
//        }
        if (StringUtil.isBlank(fsFile.getFolderId())) {
            fsFile.setFolderId(FsConstants.ROOT_FOLDER_ID);
        }
        if (StringUtil.isBlank(fsFile.getStatus())) {
            fsFile.setStatus(EFsFileStatusEnum.NORMAL.getCode());
        }
        if (StringUtil.isBlank(fsFile.getFileType())) {
            fsFile.setFileType(EFsFileTypeEnum.FILE.getCode());
        }
        if (StringUtil.isBlank(fsFile.getStorageMode())) {
            fsFile.setStorageMode(EFsStorageModeEnum.CODE_local.getCode());
        }
        return fsFileDao.insertSelective(fsFile);
    }

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量插入文件信息")
    @Override
    public int batchInsert(List<FsFile> list) {
        return fsFileDao.batchInsert(list);
    }

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量选择性插入文件信息")
    @Override
    public int batchInsertSelective(List<FsFile> list) {
        return fsFileDao.batchInsertSelective(list);
    }

    /**
     * 修改
     *
     * @param fsFile 参数
     * @return
     */
    @Trace("修改文件信息")
    @Override
    public int update(FsFile fsFile) {
        return fsFileDao.update(fsFile);
    }

    /**
     * 选择性修改
     *
     * @param fsFile 参数
     * @return
     */
    @Trace("选择性修改文件信息")
    @Override
    public int updateSelective(FsFile fsFile) {
        return fsFileDao.updateSelective(fsFile);
    }

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    @Trace("删除文件信息")
    @Override
    public int delete(String id) {
        return fsFileDao.delete(id);
    }

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    @Trace("批量删除文件信息")
    @Override
    public int batchDelete(List<String> list) {
        return fsFileDao.batchDelete(list);
    }

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param fsFile 参数
     * @return
     */
    @Trace("根据条件删除文件信息")
    @Override
    public int deleteByCondition(FsFile fsFile) {
        return fsFileDao.deleteByCondition(fsFile);
    }

    /**
     * 删除文件夹下的文件信息
     *
     * @param folderId
     * @return
     */
    @Trace("删除文件夹下的文件信息")
    @Override
    public int deleteAllByFolder(String folderId) {
        int count = 0;
        // 查询文件夹下的所有文件
        List<FsFile> childList = this.getListByFolderId(folderId);
        if (CollectionUtil.isNotEmpty(childList)) {
            for (FsFile file : childList) {
                // 计算该子文件夹下的文件数
                if (file.isFolder()) {
                    // 如果是文件夹, 则递归删除文件夹下内容
                    count += this.deleteAllByFolder(file.getId());
                } else {
                    // 文件类型, 直接删除
                    count += this.delete(file.getId());
                }
            }
        }
        log.info("[清空文件夹信息] 文件夹[{}]下的信息已清空,总计删除文件信息(含文件夹){}个!", folderId, count);
        return count;
    }

}
