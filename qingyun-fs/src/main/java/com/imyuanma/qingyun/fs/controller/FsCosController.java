package com.imyuanma.qingyun.fs.controller;

import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.fs.util.CosUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.tencent.cloud.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 腾讯云cos对象存储
 *
 * @author wangjy
 * @date 2023/08/05 12:03:58
 */
@Slf4j
@RestController
@RequestMapping(value = "/fs/cos")
public class FsCosController {


    @Trace("对象存储临时签名")
    @PostMapping("/sts")
    public Result<Response> updateLowestVersion() throws IOException {
        return Result.success(CosUtil.sign());
    }

    @Trace("COS对象下载")
    @GetMapping("/download/{key}")
    public void download(@PathVariable("key") String key, HttpServletRequest request, HttpServletResponse response) throws IOException {
        String url = CosUtil.getDownloadUrl(key);
        log.info("[COS对象下载] 待下载对象key={}", key);
        response.sendRedirect(url);
    }

}
