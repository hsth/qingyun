package com.imyuanma.qingyun.fs.model;

import com.imyuanma.qingyun.common.core.structure.tree.ITreeNode;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * xxx
 *
 * @author wangjy
 * @date 2023/07/27 00:04:11
 */
@Data
public class FsFolder implements ITreeNode<String> {
    /**
     * 编号
     */
    private String id;
    /**
     * 文件(夹)名称
     */
    private String name;
    /**
     * 状态,隐藏/只读..
     */
    private String status;
    /**
     * 备注
     */
    private String remark;
    /**
     * 所属文件夹
     */
    private String folderId;
    /**
     * 存储方式
     */
    private String storageMode;

    /**
     * 子目录集合
     */
    private List<FsFolder> children;


    public static FsFolder of(FsFile fsFile) {
        FsFolder fsFolder = new FsFolder();
        fsFolder.setId(fsFile.getId());
        fsFolder.setName(fsFile.getName());
        fsFolder.setStatus(fsFile.getStatus());
        fsFolder.setRemark(fsFile.getRemark());
        fsFolder.setFolderId(fsFile.getFolderId());
        fsFolder.setStorageMode(fsFile.getStorageMode());
        return fsFolder;
    }

    /**
     * 当前节点id
     *
     * @return
     */
    @Override
    public String nodeId() {
        return id;
    }

    /**
     * 父节点id
     *
     * @return
     */
    @Override
    public String parentNodeId() {
        return folderId;
    }

    /**
     * 添加子节点
     *
     * @param node 节点对象
     */
    @Override
    public void appendChildren(ITreeNode<String> node) {
        if (children == null) {
            children = new ArrayList<>();
        }
        children.add((FsFolder) node);
    }
}
