package com.imyuanma.qingyun.fs.util;

import com.imyuanma.qingyun.fs.configuration.FsCosConfiguration;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicSessionCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.http.HttpMethodName;
import com.qcloud.cos.http.HttpProtocol;
import com.qcloud.cos.model.GeneratePresignedUrlRequest;
import com.qcloud.cos.model.ResponseHeaderOverrides;
import com.qcloud.cos.region.Region;
import com.qcloud.cos.utils.DateUtils;
import com.tencent.cloud.CosStsClient;
import com.tencent.cloud.Response;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.TreeMap;

/**
 * 腾讯对象存储工具类
 *
 * @author wangjy
 * @date 2023/03/05 15:19:55
 */
public class CosUtil {


    public static boolean isCosFile(String fileId) {
        return fileId.length() != 32 || fileId.contains(".");
    }

    /**
     * 获取文件下载链接
     *
     * @param key
     * @return
     * @throws IOException
     */
    public static String getDownloadUrl(String key) throws IOException {
        COSClient cosClient = createCosClient();

        // 存储桶的命名格式为 BucketName-APPID，此处填写的存储桶名称必须为此格式
        String bucketName = FsConfigurationHolder.getFsCosConfiguration().getBucket();

        GeneratePresignedUrlRequest req =
                new GeneratePresignedUrlRequest(bucketName, key, HttpMethodName.GET);

        // 设置下载时返回的 http 头
        ResponseHeaderOverrides responseHeaders = new ResponseHeaderOverrides();
        String responseContentType = "application/octet-stream";
        String responseContentLanguage = "zh-CN";
        // 设置返回头部里包含文件名信息
        String responseContentDispositon = "attachment";
        String responseCacheControl = "no-cache";
        String cacheExpireStr = DateUtils.formatRFC822Date(new Date(System.currentTimeMillis() + 24L * 3600L * 1000L));
        responseHeaders.setContentType(responseContentType);
        responseHeaders.setContentLanguage(responseContentLanguage);
        responseHeaders.setContentDisposition(responseContentDispositon);
        responseHeaders.setCacheControl(responseCacheControl);
        responseHeaders.setExpires(cacheExpireStr);

        req.setResponseHeaders(responseHeaders);


        URL url = cosClient.generatePresignedUrl(req);
        // 关闭
        cosClient.shutdown();
        return url.toString();
    }

    /**
     * 创建COS客户端
     *
     * @return
     * @throws IOException
     */
    private static COSClient createCosClient() throws IOException {
        // 不需要验证身份信息
//        COSCredentials cred = new AnonymousCOSCredentials();
        Response response = sign();
        COSCredentials cred = new BasicSessionCredentials(response.credentials.tmpSecretId, response.credentials.tmpSecretKey, response.credentials.sessionToken);

        // ClientConfig 中包含了后续请求 COS 的客户端设置：
        ClientConfig clientConfig = new ClientConfig();

        // 设置 bucket 的地域
        // COS_REGION 请参照 https://cloud.tencent.com/document/product/436/6224
        clientConfig.setRegion(new Region(FsConfigurationHolder.getFsCosConfiguration().getRegion()));

        // 设置生成的 url 的请求协议, http 或者 https
        // 5.6.53 及更低的版本，建议设置使用 https 协议
        // 5.6.54 及更高版本，默认使用了 https
        clientConfig.setHttpProtocol(HttpProtocol.https);
        // 设置 socket 读取超时，默认 30s
        clientConfig.setSocketTimeout(30 * 1000);
        // 设置建立连接超时，默认 30s
        clientConfig.setConnectionTimeout(30 * 1000);

        // 生成cos客户端
        return new COSClient(cred, clientConfig);
    }

    public static String getObjectUrl(String key) throws IOException {
        // 生成cos客户端
        COSClient cosClient = createCosClient();
        URL url = cosClient.getObjectUrl(FsConfigurationHolder.getFsCosConfiguration().getBucket(), key);
        // 关闭
        cosClient.shutdown();
        return url.toString();
    }

    /**
     * 生成对象存储临时签名
     *
     * @return
     * @throws IOException
     */
    public static Response sign() throws IOException {
        FsCosConfiguration cosConfiguration = FsConfigurationHolder.getFsCosConfiguration();
        //这里的 SecretId 和 SecretKey 代表了用于申请临时密钥的永久身份（主账号、子账号等），子账号需要具有操作存储桶的权限。
        String secretId = cosConfiguration.getSecretId();//用户的 SecretId，建议使用子账号密钥，授权遵循最小权限指引，降低使用风险。子账号密钥获取可参见 https://cloud.tencent.com/document/product/598/37140
        String secretKey = cosConfiguration.getSecretKey();//用户的 SecretKey，建议使用子账号密钥，授权遵循最小权限指引，降低使用风险。子账号密钥获取可参见 https://cloud.tencent.com/document/product/598/37140
        TreeMap<String, Object> config = new TreeMap<String, Object>();
        // 替换为您的云 api 密钥 SecretId
        config.put("secretId", secretId);
        // 替换为您的云 api 密钥 SecretKey
        config.put("secretKey", secretKey);

        // 设置域名:
        // 如果您使用了腾讯云 cvm，可以设置内部域名
        //config.put("host", "sts.internal.tencentcloudapi.com");

        // 临时密钥有效时长，单位是秒，默认 1800 秒，目前主账号最长 2 小时（即 7200 秒），子账号最长 36 小时（即 129600）秒
        config.put("durationSeconds", 1800);

        // 换成您的 bucket
        config.put("bucket", cosConfiguration.getBucket());
        // 换成 bucket 所在地区
        config.put("region", cosConfiguration.getRegion());

        // 这里改成允许的路径前缀，可以根据自己网站的用户登录态判断允许上传的具体路径
        // 列举几种典型的前缀授权场景：
        // 1、允许访问所有对象："*"
        // 2、允许访问指定的对象："a/a1.txt", "b/b1.txt"
        // 3、允许访问指定前缀的对象："a*", "a/*", "b/*"
        // 如果填写了“*”，将允许用户访问所有资源；除非业务需要，否则请按照最小权限原则授予用户相应的访问权限范围。
        config.put("allowPrefixes", new String[]{"*"});

        // 密钥的权限列表。必须在这里指定本次临时密钥所需要的权限。
        // 简单上传、表单上传和分块上传需要以下的权限，其他权限列表请参见 https://cloud.tencent.com/document/product/436/31923
        String[] allowActions = new String[]{
                // 简单上传
                "name/cos:PutObject",
                // 表单上传、小程序上传
                "name/cos:PostObject",
                // 分块上传
                "name/cos:InitiateMultipartUpload",
                "name/cos:ListMultipartUploads",
                "name/cos:ListParts",
                "name/cos:UploadPart",
                "name/cos:CompleteMultipartUpload",
                "name/cos:HeadObject",
                "name/cos:GetObject",
                "name/cos:GetBucket",
                "name/cos:OptionsObject"
        };
        config.put("allowActions", allowActions);

        return CosStsClient.getCredential(config);
    }

}
