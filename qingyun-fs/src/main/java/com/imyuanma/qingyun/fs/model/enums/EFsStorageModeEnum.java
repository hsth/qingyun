package com.imyuanma.qingyun.fs.model.enums;

/**
 * 存储方式枚举
 *
 * @author YuanMaKeJi
 * @date 2023-07-23 00:20:38
 */
public enum EFsStorageModeEnum {
    CODE_local("local", "本地存储"),
    CODE_cos("cos", "腾讯云对象存储"),
    ;

    /**
     * code
     */
    private String code;
    /**
     * 文案
     */
    private String text;

    EFsStorageModeEnum(String code, String text) {
        this.code = code;
        this.text = text;
    }

    /**
     * 根据code获取枚举
     *
     * @param code code值
     */
    public static EFsStorageModeEnum getByCode(String code) {
        for (EFsStorageModeEnum e : EFsStorageModeEnum.values()) {
            if (e.code != null && e.code.equals(code)) {
                return e;
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
