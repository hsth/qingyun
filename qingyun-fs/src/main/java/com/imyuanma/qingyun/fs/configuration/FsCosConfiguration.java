package com.imyuanma.qingyun.fs.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 腾讯云对象存储配置
 *
 * @author wangjy
 * @date 2023/08/05 12:11:19
 */
@Configuration
@ConfigurationProperties(prefix = "qingyun.fs.cos")
public class FsCosConfiguration {
    /**
     * 应用id
     */
    private String appId;
    /**
     * 桶
     */
    private String bucket;
    /**
     * 区域
     */
    private String region;
    /**
     * 安全id
     */
    private String secretId;
    /**
     * 安全key
     */
    private String secretKey;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getSecretId() {
        return secretId;
    }

    public void setSecretId(String secretId) {
        this.secretId = secretId;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }
}
