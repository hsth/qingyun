package com.imyuanma.qingyun.fs.service;

import com.imyuanma.qingyun.fs.model.FsFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 文件流服务
 *
 * @author wangjy
 * @date 2022/07/24 00:16:56
 */
public interface IFsFileStreamService {

    /**
     * 保存文件,默认存放到根目录
     *
     * @param file         文件信息
     * @param businessCode 业务编号,可以为空
     * @return
     */
    FsFile saveFile(MultipartFile file, String businessCode);

    /**
     * 上传文件到指定文件夹
     *
     * @param file         文件信息
     * @param folderId     文件夹id
     * @param businessCode 业务编号,可以为空
     * @return
     */
    FsFile saveFile(MultipartFile file, String folderId, String businessCode);

    /**
     * 上传文件到指定文件夹
     *
     * @param file         文件信息
     * @param folder       文件夹
     * @param businessCode 业务编号,可以为空
     * @return
     */
    FsFile saveFile(MultipartFile file, FsFile folder, String businessCode);

    /**
     * 批量上传文件到指定文件夹
     *
     * @param files
     * @param folderId
     * @param businessCode 业务编号,可以为空
     * @return
     */
    List<FsFile> saveFiles(List<MultipartFile> files, String folderId, String businessCode);

    /**
     * 删除文件
     *
     * @param fileId 文件编号
     * @return
     */
    boolean dropFile(String fileId);

    /**
     * 删除文件夹
     *
     * @param folderId 文件夹编号
     * @return
     */
    boolean dropFolder(String folderId);

    /**
     * 创建文件夹
     *
     * @param folder 文件夹信息
     * @return 文件夹信息
     */
    FsFile createFolder(FsFile folder);
}
