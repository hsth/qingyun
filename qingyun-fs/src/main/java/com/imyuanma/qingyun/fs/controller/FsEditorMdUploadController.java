package com.imyuanma.qingyun.fs.controller;

import com.imyuanma.qingyun.fs.model.FsFile;
import com.imyuanma.qingyun.fs.service.IFsFileStreamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * editor md插件上传
 *
 * @author wangjy
 * @date 2022/03/28 23:30:00
 */
@Controller
@RequestMapping(value = "/fs/editorMd/upload", method = {RequestMethod.POST})
public class FsEditorMdUploadController {

    @Autowired
    private IFsFileStreamService fileStreamService;

//    @Log("editor上传图片")
    @RequestMapping("/image")
    @ResponseBody
    public Object image(@RequestParam("editormd-image-file") MultipartFile file, String folderId, HttpServletRequest request) {
        FsFile fileInfo = fileStreamService.saveFile(file, folderId, null);
        Map<String, Object> map = new HashMap<>();
        if (fileInfo != null) {
            map.put("success", 1);
            map.put("message", "成功");
            map.put("url", fileInfo.getPath());
        } else {
            map.put("success", 0);
            map.put("message", "上传失败");
        }
        return map;
    }
}
