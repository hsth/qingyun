package com.imyuanma.qingyun.fs.util;

import com.imyuanma.qingyun.common.util.StringUtil;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * 图片工具类
 *
 * @author wangjy
 * @date 2022/09/03 14:37:32
 */
public class ImageUtil {
    private static final Logger logger = LoggerFactory.getLogger(ImageUtil.class);
    public final static Map<String, String> FILE_TYPE_MAP = new HashMap<String, String>();

    static {
        getAllFileType(); //初始化文件类型信息
    }

    /**
     * Discription:[getAllFileType,常见文件头信息]
     */
    private static void getAllFileType() {
        FILE_TYPE_MAP.put("ffd8ffe000104a464946", "jpg"); //JPEG (jpg)
        FILE_TYPE_MAP.put("89504e470d0a1a0a0000", "png"); //PNG (png)
        FILE_TYPE_MAP.put("47494638396126026f01", "gif"); //GIF (gif)
        FILE_TYPE_MAP.put("49492a00227105008037", "tif"); //TIFF (tif)
        FILE_TYPE_MAP.put("424d228c010000000000", "bmp"); //16色位图(bmp)
        FILE_TYPE_MAP.put("424d8240090000000000", "bmp"); //24位位图(bmp)
        FILE_TYPE_MAP.put("424d8e1b030000000000", "bmp"); //256色位图(bmp)
        FILE_TYPE_MAP.put("41433130313500000000", "dwg"); //CAD (dwg)
//        FILE_TYPE_MAP.put("3c21444f435459504520", "html"); //HTML (html)
//        FILE_TYPE_MAP.put("3c21646f637479706520", "htm"); //HTM (htm)
//        FILE_TYPE_MAP.put("48544d4c207b0d0a0942", "css"); //css
//        FILE_TYPE_MAP.put("696b2e71623d696b2e71", "js"); //js
//        FILE_TYPE_MAP.put("7b5c727466315c616e73", "rtf"); //Rich Text Format (rtf)
//        FILE_TYPE_MAP.put("38425053000100000000", "psd"); //Photoshop (psd)
//        FILE_TYPE_MAP.put("46726f6d3a203d3f6762", "eml"); //Email [Outlook Express 6] (eml)
//        FILE_TYPE_MAP.put("d0cf11e0a1b11ae10000", "doc"); //MS Excel 注意：word、msi 和 excel的文件头一样
//        FILE_TYPE_MAP.put("d0cf11e0a1b11ae10000", "vsd"); //Visio 绘图
//        FILE_TYPE_MAP.put("5374616E64617264204A", "mdb"); //MS Access (mdb)
//        FILE_TYPE_MAP.put("252150532D41646F6265", "ps");
//        FILE_TYPE_MAP.put("255044462d312e350d0a", "pdf"); //Adobe Acrobat (pdf)
//        FILE_TYPE_MAP.put("2e524d46000000120001", "rmvb"); //rmvb/rm相同
//        FILE_TYPE_MAP.put("464c5601050000000900", "flv"); //flv与f4v相同
//        FILE_TYPE_MAP.put("00000020667479706d70", "mp4");
//        FILE_TYPE_MAP.put("49443303000000002176", "mp3");
//        FILE_TYPE_MAP.put("000001ba210001000180", "mpg"); //
//        FILE_TYPE_MAP.put("3026b2758e66cf11a6d9", "wmv"); //wmv与asf相同
//        FILE_TYPE_MAP.put("52494646e27807005741", "wav"); //Wave (wav)
//        FILE_TYPE_MAP.put("52494646d07d60074156", "avi");
//        FILE_TYPE_MAP.put("4d546864000000060001", "mid"); //MIDI (mid)
//        FILE_TYPE_MAP.put("504b0304140000000800", "zip");
//        FILE_TYPE_MAP.put("526172211a0700cf9073", "rar");
//        FILE_TYPE_MAP.put("235468697320636f6e66", "ini");
//        FILE_TYPE_MAP.put("504b03040a0000000000", "jar");
//        FILE_TYPE_MAP.put("4d5a9000030000000400", "exe");//可执行文件
//        FILE_TYPE_MAP.put("3c25402070616765206c", "jsp");//jsp文件
//        FILE_TYPE_MAP.put("4d616e69666573742d56", "mf");//MF文件
//        FILE_TYPE_MAP.put("3c3f786d6c2076657273", "xml");//xml文件
//        FILE_TYPE_MAP.put("494e5345525420494e54", "sql");//xml文件
//        FILE_TYPE_MAP.put("7061636b616765207765", "java");//java文件
//        FILE_TYPE_MAP.put("406563686f206f66660d", "bat");//bat文件
//        FILE_TYPE_MAP.put("1f8b0800000000000000", "gz");//gz文件
//        FILE_TYPE_MAP.put("6c6f67346a2e726f6f74", "properties");//bat文件
//        FILE_TYPE_MAP.put("cafebabe0000002e0041", "class");//bat文件
//        FILE_TYPE_MAP.put("49545346030000006000", "chm");//bat文件
//        FILE_TYPE_MAP.put("04000000010000001300", "mxp");//bat文件
//        FILE_TYPE_MAP.put("504b0304140006000800", "docx");//docx文件
//        FILE_TYPE_MAP.put("d0cf11e0a1b11ae10000", "wps");//WPS文字wps、表格et、演示dps都是一样的
//        FILE_TYPE_MAP.put("6431303a637265617465", "torrent");


//        FILE_TYPE_MAP.put("6D6F6F76", "mov"); //Quicktime (mov)
//        FILE_TYPE_MAP.put("FF575043", "wpd"); //WordPerfect (wpd)
//        FILE_TYPE_MAP.put("CFAD12FEC5FD746F", "dbx"); //Outlook Express (dbx)
//        FILE_TYPE_MAP.put("2142444E", "pst"); //Outlook (pst)
//        FILE_TYPE_MAP.put("AC9EBD8F", "qdf"); //Quicken (qdf)
//        FILE_TYPE_MAP.put("E3828596", "pwl"); //Windows Password (pwl)
//        FILE_TYPE_MAP.put("2E7261FD", "ram"); //Real Audio (ram)
    }

    /**
     * 得到上传文件的文件头
     *
     * @param src
     * @return
     */
    public static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder();
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

    /**
     * 判断是否图片类型
     *
     * @param file
     * @return
     */
    public static boolean isImageNotThrow(MultipartFile file) {
        try {
            return isImage(file.getInputStream());
        } catch (IOException e) {
            logger.error("[判断是否图片类型] 发生异常", e);
        }
        return false;
    }

    /**
     * 判断是否图片类型
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static boolean isImage(MultipartFile file) throws IOException {
        return isImage(file.getInputStream());
    }

    /**
     * 判断是否图片类型
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static boolean isImage(File file) throws IOException {
        return isImage(new FileInputStream(file));
    }

    /**
     * 判断是否图片类型
     *
     * @param is
     * @return
     * @throws IOException
     */
    public static boolean isImage(InputStream is) throws IOException {
        byte[] b = new byte[10];
        is.read(b, 0, b.length);
        String fileCode = bytesToHexString(b);
        System.out.println(fileCode);
        if (StringUtil.isNotBlank(fileCode)) {
            return FILE_TYPE_MAP.keySet().stream().anyMatch(key -> key.toLowerCase().startsWith(fileCode.toLowerCase()));
        }
        return false;
    }

    /**
     * 裁剪图片
     *
     * @param file
     * @param dw
     * @param dh
     * @param x
     * @param y
     * @return
     * @throws IOException
     */
    public static String cutImage(File file, Integer dw, Integer dh, Integer x, Integer y) throws IOException {
        BufferedImage br = ImageIO.read(new FileInputStream(file));
        BufferedImage dst = new BufferedImage(dw, dh, 1);
        Graphics2D g = dst.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_DEFAULT);
        g.drawImage(br, 0, 0, dw, dh, x, y, x + dw, dh + y, null);
        g.dispose();

        String src = file.getAbsolutePath();

        //获取原图片的名称
        String n1 = src.substring(0, src.lastIndexOf("."));
        //获取原图片的扩展名
        String n2 = src.substring(src.lastIndexOf("."));
        //获取裁剪后的图片名称
        String nn = n1 + "_n1" + n2;
        //保存
        ImageIO.write(dst, FsBusinessUtil.getFileSuffix(src), new File(nn));
        return nn;
    }

    /**
     * tif格式图片转png格式
     *
     * @param file
     * @throws IOException
     */
    public static File tifToPng(File file, String pngDiskPath) throws IOException {
        BufferedImage bufferImage = ImageIO.read(file);
        File pngFile = new File(pngDiskPath);
        ImageIO.write(bufferImage, "png", pngFile);
        return pngFile;
    }

    /**
     * 将图片写入响应
     *
     * @param file
     * @param response
     * @throws IOException
     */
    public static void writeImageResponse(File file, HttpServletResponse response) throws IOException {
        if (file.exists()) {
            //图片流
            InputStream is = new FileInputStream(file);
            //设置响应类型
            response.setContentType("image/*");
            //写入到响应
            OutputStream os = response.getOutputStream();
            //拷贝到输出流
            IOUtils.copy(is, os);
        }
    }



}
