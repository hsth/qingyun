package com.imyuanma.qingyun.fs.model.enums;


/**
 * 文件媒体类型
 */
public enum EFsMediaType {
    FOLDER("FOLDER", "文件夹"),
    IMG("IMG", "图片", "image/*"),
    VIDEO("VIDEO", "视频", "video/*"),
    VOICE("VOICE", "音频", "video/*"),
    TXT("TXT", "文本", "text/plain"),
    CSS("CSS", "CSS文件"),
    JS("JS", "JS文件"),
    HTML("HTML", "HTML文件"),
    PDF("PDF", "PDF文档", "application/pdf"),
    DOC("DOC", "文档", "application/msword"),
    EXCEL("EXCEL", "EXCEL表格", "application/vnd.ms-excel"),
    PPT("PPT", "PPT", "application/vnd.ms-powerpoint"),
    ZIP("ZIP", "压缩文件"),
    OTHER("OTHER", "其他"),
    ;

    /**
     * code
     */
    private String code;
    /**
     * 文案
     */
    private String text;
    /**
     * 文件响应类型, 预览时使用
     */
    private String contentType;

    public static EFsMediaType ofType(String type) {
        for (EFsMediaType e : EFsMediaType.values()) {
            if (e.getCode().equals(type)) {
                return e;
            }
        }
        return OTHER;
    }

    /**
     * 根据文件后缀名获取文件类型
     *
     * @param suffix
     * @return
     */
    public static EFsMediaType ofSuffix(String suffix) {
        if (".jpg".equalsIgnoreCase(suffix) || ".jpeg".equalsIgnoreCase(suffix) || ".png".equalsIgnoreCase(suffix) || ".gif".equalsIgnoreCase(suffix) || ".tif".equalsIgnoreCase(suffix)) {
            return IMG;
        } else if (".pdf".equals(suffix)) {
            return PDF;
        } else if (".doc".equals(suffix) || ".docx".equals(suffix)) {
            return DOC;
        } else if (".txt".equals(suffix)) {
            return TXT;
        } else if (".css".equals(suffix)) {
            return CSS;
        } else if (".js".equals(suffix)) {
            return JS;
        } else if (".html".equals(suffix)) {
            return HTML;
        } else if (".xls".equals(suffix) || ".xlsx".equals(suffix)) {
            return EXCEL;
        } else if (".ppt".equals(suffix) || ".pptx".equals(suffix)) {
            return PPT;
        } else if (".mp4".equals(suffix) || ".avi".equals(suffix) || ".wmv".equals(suffix) || ".navi".equals(suffix) || ".3gp".equals(suffix) || ".flv".equals(suffix)) {
            return VIDEO;
        } else if (".mp3".equals(suffix) || ".pptx".equals(suffix)) {
            return VOICE;
        } else if (".zip".equals(suffix) || ".rar".equals(suffix)) {
            return ZIP;
        } else {
            return OTHER;
        }
    }

    EFsMediaType(String code, String text) {
        this.code = code;
        this.text = text;
    }

    EFsMediaType(String code, String text, String contentType) {
        this.code = code;
        this.text = text;
        this.contentType = contentType;
    }

    public String getCode() {
        return code;
    }

    public String getText() {
        return text;
    }

    public String getContentType() {
        return contentType;
    }
}

