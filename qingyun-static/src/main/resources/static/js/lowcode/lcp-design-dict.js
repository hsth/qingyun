// 页面配置-值选项常量
const LCP_VALUE_OPTIONS_CONSTANT = {
    BOOLEAN_OPTIONS: [{value: true, text: 'true'}, {value: false, text: 'false'}],
    BOOLEAN_NUMBER_OPTIONS: [{value: 1, text: '1'}, {value: 0, text: '0'}],
    POSITION_OPTIONS: [{value: 'left', text: 'left'}, {value: 'right', text: 'right'}, {value: 'top', text: 'top'}],
    SIZE_OPTIONS: [{value: 'large', text: 'large'}, {value: 'default', text: 'default'}, {value: 'small', text: 'small'}],
};

// 页面配置-组件分类
const PAGE_CONFIG_COMPONENT_CATEGORY = [
    {name: 'layout', title: '布局组件', icon: 'fa-cubes'},
    {name: 'formMix', title: '表单项组件', icon: 'fa-cubes'},
    {name: 'form', title: '表单基础元素', icon: 'fa-cubes'},
    {name: 'container', title: '容器组件', icon: 'fa-cubes'},
    {name: 'data', title: '数据组件', icon: 'fa-table'},
    {name: 'high', title: '高级组件', icon: 'fa-map-o'},
    {name: 'chart', title: '图表组件', icon: 'fa-bar-chart'},
    {name: 'other', title: '其他组件', icon: 'fa-cubes'}
];

// 元素style样式选项元数据
const PAGE_CONFIG_STYLE_META = [
    {
        name: '尺寸属性',
        children: [
            {name: 'width', text: '宽度'},
            {name: 'min-width', text: '最小宽度'},
            {name: 'max-width', text: '最大宽度'},
            {name: 'height', text: '高度'},
            {name: 'min-height', text: '最小高度'},
            {name: 'max-height', text: '最大高度'},
        ]
    },
    {
        name: '布局/定位属性',
        children: [
            {name: 'display', text: 'display'},
            {name: 'position', text: '定位Position'},
            {name: 'padding', text: '内边距'},
            {name: 'padding-left', text: '左内边距'},
            {name: 'padding-right', text: '右内边距'},
            {name: 'padding-top', text: '上内边距'},
            {name: 'padding-bottom', text: '下内边距'},
            {name: 'margin', text: '外边距'},
            {name: 'margin-left', text: '左外边距'},
            {name: 'margin-right', text: '右外边距'},
            {name: 'margin-top', text: '上外边距'},
            {name: 'margin-bottom', text: '下外边距'},
            {name: 'overflow', text: 'overflow'},
        ]
    },
    {
        name: 'Flex布局',
        children: [
            {name: 'flex-direction', text: 'flex水平方向'},
            {name: 'justify-content', text: '水平轴内容位置'},
            {name: 'align-items', text: '交叉轴内容位置'},
            {name: 'flex-wrap', text: '换行属性'},
            {name: 'flex', text: 'flex'},
        ]
    },
    {
        name: '边框/阴影/背景',
        children: [
            {name: 'border', text: '边框线'},
            {name: 'border-radius', text: '边框弧度'},
            {name: 'box-shadow', text: '阴影'},
            {name: 'background-color', text: '背景色'},
        ]
    },
    {
        name: '文本属性',
        children: [
            {name: 'text-align', text: '文本位置'},
            {name: 'color', text: '字体颜色'},
            {name: 'font-size', text: '字体大小'},
            {name: 'font-weight', text: '字体重量'},
            {name: 'line-height', text: '行高'},
        ]
    },

    {
        name: '其他',
        children: [
            {name: 'cursor', text: '光标样式'},
        ]
    },
];

// 元素class样式内置选项
const LCP_DESIGN_CLASS_META = [
    {value: 'qy-flex', text: '水平flex'},
    {value: 'qy-flex-y', text: '垂直flex'},
    {value: 'qy-flex-1', text: 'flex=1'},
    {value: 'qy-flex-row-center', text: 'flex水平居中'},
    {value: 'qy-flex-vertical-center', text: 'flex垂直居中'},
    {value: 'qy-height-60', text: '高度60px'},
    {value: 'qy-width-250', text: '宽度250px'},
    {value: 'qy-width-100p', text: '宽度100%'},
    {value: 'qy-height-100p', text: '高度100%'},
    {value: 'qy-padding-15', text: '内边距15px'},
];

// 组件属性数据类型
const LCP_COMPONENT_ATTR_DATA_TYPE = [
    // {key: 'var', value: 'vue变量', styleType: ''},
    {key: 'string', value: '字符串', styleType: ''},
    {key: 'number', value: '数字', styleType: ''},
    {key: 'boolean', value: '布尔', styleType: ''},
    {key: 'object', value: '对象', styleType: ''},
    {key: 'array', value: '数组', styleType: ''},
    {key: 'time', value: '时间', styleType: ''},
    {key: 'function', value: '方法', styleType: ''},
];



// 页面配置-组件选项
// const PAGE_CONFIG_COMPONENT_OPTIONS = [
//     {
//         name: 'layout', title: '布局组件', icon: 'fa-cubes',
//         componentList: [
//             {
//                 type: 'row', name: '栅格-ROW', icon: 'fa-bars'
//             },
//             {type: 'col', name: '栅格-COL', icon: 'fa-ellipsis-v'},
//             {type: 'div', name: 'DIV容器', icon: 'fa-window-maximize', classList: ['f-c-padding-15']},
//             {
//                 type: 'div',
//                 name: '布局-上下',
//                 icon: 'fa-th-large',
//                 classList: ['qy-flex-y', 'qy-padding-15', 'qy-height-100p'],
//                 childrenList: [
//                     {type: 'div', name: 'DIV容器', classList: ['qy-height-60']},
//                     {type: 'div', name: 'DIV容器', classList: ['qy-flex-1']},
//                 ]
//             },
//             {
//                 type: 'div',
//                 name: '布局-左右',
//                 icon: 'fa-th-large',
//                 classList: ['qy-flex', 'qy-padding-15', 'qy-height-100p'],
//                 childrenList: [
//                     {type: 'div', name: 'DIV容器', classList: ['qy-width-250']},
//                     {type: 'div', name: 'DIV容器', classList: ['qy-flex-1']},
//                 ]
//             },
//             {type: 'divider', name: '分割线', icon: 'fa-bars'},
//         ]
//     },
//     {
//         name: 'form', title: '表单项组件', icon: 'fa-keyboard-o',
//         componentList: [
//             {type: 'form', name: '表单容器', icon: 'fa-object-group'},
//             {
//                 type: 'input', name: '输入框', icon: 'fa-keyboard-o'
//             },
//             {type: 'textarea', name: '大文本框', icon: 'fa-font'},
//             {type: 'number', name: '数字输入框', icon: 'fa-subscript'},
//             {type: 'radio', name: '单选框', icon: 'fa-dot-circle-o'},
//             {type: 'checkbox', name: '多选框', icon: 'fa-check-square-o'},
//             {type: 'select', name: '下拉选择框', icon: 'fa-caret-square-o-down'},
//             {type: 'cascade', name: '级联选择器', icon: 'fa-caret-square-o-down'},
//             {type: 'date', name: '日期选择', icon: 'fa-calendar'},
//             {type: 'time', name: '时间选择', icon: 'fa-clock-o'},
//             {type: 'slider', name: '滑块', icon: 'fa-sliders'},
//             {type: 'switch', name: '开关', icon: 'fa-toggle-on'},
//             {type: 'color', name: '取色器', icon: 'fa-tint'},
//             {type: 'rate', name: '评分', icon: 'fa-star-o'},
//             {type: 'upload', name: '文件上传', icon: 'fa-file-o'},
//         ]
//     },
//     {
//         name: 'base', title: '数据组件', icon: 'fa-table',
//         componentList: [
//             {type: 'text', name: '文字', icon: 'fa-font'},
//         ]
//     },
//     {
//         name: 'high', title: '高级组件', icon: 'fa-map-o',
//         componentList: [
//             // {type: 'xxx', name: 'xxx', icon: 'xxx'}
//         ]
//     },
//     {
//         name: 'chart', title: '图表组件', icon: 'fa-bar-chart',
//         componentList: [
//             // {type: 'xxx', name: 'xxx', icon: 'xxx'}
//         ]
//     },
// ];

// 页面配置-基础组件元数据
// const PAGE_CONFIG_COMPONENT_META = {
//     'row': {
//         name: '栅格-行',
//         attrDescriptions: [
//             {attrName: 'gutter', text: '栅格间隔'},
//             {attrName: 'justify', text: 'flex 布局下的水平排列方式(start)'},
//             {attrName: 'align', text: 'flex 布局下的垂直排列方式(top)'},
//             {attrName: 'id', text: 'id'},
//             {attrName: 'v-if', text: 'v-if'},
//             {attrName: 'v-show', text: 'v-show插入'},
//         ]
//     },
//     'col': {
//         name: '栅格-列',
//         attrDescriptions: [
//             {attrName: 'span', text: '栅格列宽', dataType: 'number'},
//             {attrName: 'xs', text: '<768px栅格列宽'},
//             {attrName: 'sm', text: '≥768px栅格列宽'},
//             {attrName: 'md', text: '≥992px栅格列宽'},
//             {attrName: 'lg', text: '≥1200px栅格列宽'},
//             {attrName: 'xl', text: '≥1920px栅格列宽'},
//             {attrName: 'offset', text: '栅格左侧的间隔格数'},
//             {attrName: 'push', text: '栅格向右移动格数'},
//             {attrName: 'pull', text: '栅格向左移动格数'},
//             {attrName: 'id', text: 'id'},
//             {attrName: 'v-if', text: 'v-if'},
//             {attrName: 'v-show', text: 'v-show插入'},
//         ]
//     },
//     'div': {
//         name: 'DIV',
//         attrDescriptions: [
//             {attrName: 'id', text: 'id'},
//             {attrName: 'v-if', text: 'v-if'},
//             {attrName: 'v-show', text: 'v-show插入'},
//         ]
//     },
//     'divider': {
//         name: '分割线',
//         attrDescriptions: []
//     },
//
//     'form': {
//         name: '表单容器form',
//         attrDescriptions: [
//             {attrName: 'model', text: '表单数据对象'},
//             {attrName: 'rules', text: '表单验证规则'},
//             {attrName: ':inline', text: '行内表单模式', valueOptions: LCP_VALUE_OPTIONS_CONSTANT.BOOLEAN_OPTIONS},
//             {
//                 attrName: 'label-position',
//                 text: '表单域标签的位置',
//                 valueOptions: LCP_VALUE_OPTIONS_CONSTANT.POSITION_OPTIONS
//             },
//             {attrName: 'label-width', text: '标签的宽度'},
//             {
//                 attrName: 'size',
//                 text: '表单内组件尺寸',
//                 valueOptions: LCP_VALUE_OPTIONS_CONSTANT.SIZE_OPTIONS
//             },
//             {
//                 attrName: 'disabled',
//                 text: '是否禁用该表单内的所有组件',
//                 valueOptions: LCP_VALUE_OPTIONS_CONSTANT.BOOLEAN_OPTIONS
//             },
//         ]
//     },
//     'input': {
//         name: '输入框input',
//         attrDescriptions: [
//             {attrName: 'label', text: '标题'},
//             {attrName: 'label-width', text: '标题宽度'},
//             {attrName: 'prop', text: '键名(校验用到)'},
//             {
//                 attrName: 'size',
//                 text: '表单内组件尺寸',
//                 valueOptions: LCP_VALUE_OPTIONS_CONSTANT.SIZE_OPTIONS
//             },
//
//             {attrName: 'type', text: '类型(text,其他)'},
//             {attrName: 'modelValue', text: '绑定值(modelValue)'},
//             {attrName: 'v-model', text: '绑定值(v-model)'},
//             {attrName: 'maxlength', text: '最大输入长度'},
//             {attrName: 'minlength', text: '最小输入长度'},
//             {
//                 attrName: 'show-word-limit',
//                 text: '是否显示输入字数统计',
//                 valueOptions: LCP_VALUE_OPTIONS_CONSTANT.BOOLEAN_OPTIONS
//             },
//             {attrName: 'placeholder', text: '输入框占位文本'},
//             {attrName: 'clearable', text: '是否可清空'},
//             {attrName: 'disabled', text: '是否禁用'},
//             {
//                 attrName: 'size',
//                 text: '组件尺寸',
//                 valueOptions: LCP_VALUE_OPTIONS_CONSTANT.SIZE_OPTIONS
//             },
//             {attrName: 'prefix-icon', text: '自定义前缀图标'},
//             {attrName: 'suffix-icon', text: '自定义后缀图标'},
//             {attrName: 'rows', text: '输入框行数'},
//             {attrName: 'autosize', text: 'textarea 高度是否自适应', valueOptions: LCP_VALUE_OPTIONS_CONSTANT.BOOLEAN_OPTIONS},
//             {attrName: 'readonly', text: '是否只读', valueOptions: LCP_VALUE_OPTIONS_CONSTANT.BOOLEAN_OPTIONS},
//             {attrName: 'max', text: '最大值'},
//             {attrName: 'min', text: '最小值'},
//             {attrName: 'step', text: '步长'},
//         ]
//     },
//     'textarea': {
//         name: '大文本框textarea',
//         attrDescriptions: [
//             {attrName: 'label', text: '标题'},
//             {attrName: 'label-width', text: '标题宽度'},
//             {attrName: 'prop', text: '键名(校验用到)'},
//             {
//                 attrName: 'size',
//                 text: '表单内组件尺寸',
//                 valueOptions: LCP_VALUE_OPTIONS_CONSTANT.SIZE_OPTIONS
//             },
//
//             {attrName: 'type', text: '类型(text,其他)'},
//             {attrName: 'modelValue', text: '绑定值(modelValue)'},
//             {attrName: 'v-model', text: '绑定值(v-model)'},
//             {attrName: 'maxlength', text: '最大输入长度'},
//             {attrName: 'minlength', text: '最小输入长度'},
//             {
//                 attrName: 'show-word-limit',
//                 text: '是否显示输入字数统计',
//                 valueOptions: LCP_VALUE_OPTIONS_CONSTANT.BOOLEAN_OPTIONS
//             },
//             {attrName: 'placeholder', text: '输入框占位文本'},
//             {attrName: 'clearable', text: '是否可清空'},
//             {attrName: 'disabled', text: '是否禁用'},
//             {
//                 attrName: 'size',
//                 text: '组件尺寸',
//                 valueOptions: LCP_VALUE_OPTIONS_CONSTANT.SIZE_OPTIONS
//             },
//             {attrName: 'rows', text: '输入框行数'},
//             {attrName: 'autosize', text: 'textarea 高度是否自适应', valueOptions: LCP_VALUE_OPTIONS_CONSTANT.BOOLEAN_OPTIONS},
//             {attrName: 'readonly', text: '是否只读', valueOptions: LCP_VALUE_OPTIONS_CONSTANT.BOOLEAN_OPTIONS},
//         ]
//     },
//     'number': {
//         name: '数字输入框',
//         attrDescriptions: []
//     },
//     'radio': {
//         name: '单选框radio',
//         attrDescriptions: []
//     },
//     'checkbox': {
//         name: '多选框checkbox',
//         attrDescriptions: []
//     },
//     'select': {
//         name: '下拉选择select',
//         attrDescriptions: []
//     },
//     'cascade': {
//         name: '级联选择',
//         attrDescriptions: []
//     },
//     'date': {
//         name: '时间选择',
//         attrDescriptions: []
//     },
//     'time': {
//         name: '时间选择',
//         attrDescriptions: []
//     },
//     'slider': {
//         name: '滑块',
//         attrDescriptions: []
//     },
//     'switch': {
//         name: '开关switch',
//         attrDescriptions: []
//     },
//     'color': {
//         name: '取色器',
//         attrDescriptions: []
//     },
//     'rate': {
//         name: '评分',
//         attrDescriptions: []
//     },
//     'upload': {
//         name: '上传',
//         attrDescriptions: []
//     },
//     'text': {
//         name: '文字',
//         attrDescriptions: []
//     },
// };
