// 组件选项
joEl.register('form-config-ele-option', {
    props: {
        ele: {
            type: Object,
            default: '',
        },
    },
    data: function () {
        return {};
    },
    template: `
        <div class="form-config-ele-option"
            draggable="true"
            @dragstart.stop.self="dragStartEle($event, ele)"
            @dragend.stop.self="dragEndEle($event, ele)"
        >
            <div class="form-config-icon">
                <i class="fa" :class="ele.icon"></i>
            </div>
            <div class="form-config-ele-name">{{ele.name}}</div>
        </div>
    `,
    methods: {
        dragStartEle(event, ele) {
            this.$root.dragStartEleOption(event, ele);
        },
        dragEndEle(event, ele) {
            this.$root.dragEndEleOption(event, ele);
        },
    },
    mounted() {

    }
});
// 组件包装器
joEl.register('form-config-ele-wrap', {
    props: {
        ele: {
            type: Object,
            default: {}
        },
    },
    data: function () {
        return {};
    },
    computed: {
        wrapActiveEleId() {
            return this.$root.formConfigActiveEleId;
        },
        // class
        classCompute() {
            var clz = {
                'form-config-ele-wrap-active' : this.ele.id == this.wrapActiveEleId
            };
            jo.forEach(this.ele.classList, function (c) {
                if (c) {
                    clz[c] = true;
                }
            });
            return clz;
        },
        // 样式style
        styleCompute() {
            var style = {};
            jo.forEach(this.ele.styleList, function (item) {
                if (item.name && item.value) {
                    style[item.name] = item.value;
                }
            });
            return style;
        },
    },
    template: `
        <form-config-ele-instance class="form-config-ele-wrap"
            :ele="ele"
            :class="classCompute"
            :style="styleCompute"
            @click.stop="eleWrapChecked(ele)"
            @mouseover.stop="eleWrapMouseOver($event, ele)"
            @mouseout.stop="eleWrapMouseOut($event, ele)"
            draggable="true"
            @dragstart.stop.self="dragStartEle($event, ele)"
            @dragend.stop.self="dragEndEle($event, ele)"
            @dragover.stop.prevent="dragOverEle($event, ele)"
            @drop.stop="dropEle($event, ele)"
        >
        </form-config-ele-instance>
    `,
    methods: {
        eleWrapMouseOver(event, ele) {
            if (!$(event.currentTarget).hasClass('form-config-ele-wrap-hover')) {
                $(event.currentTarget).addClass('form-config-ele-wrap-hover');
            }
        },
        eleWrapMouseOut(event, ele) {
            if ($(event.currentTarget).hasClass('form-config-ele-wrap-hover')) {
                $(event.currentTarget).removeClass('form-config-ele-wrap-hover');
            }
        },
        // 页面配置中组件选中
        eleWrapChecked(ele) {
            if (this.$root.formConfigActiveEle && this.$root.formConfigActiveEle.id === ele.id) {
                this.$root.formConfigActiveEle = null;
            } else {
                this.$root.formConfigActiveEle = ele;
            }
        },
        // 组件拖拽开始
        dragStartEle(event, ele) {
            this.$root.dragStartEleWrap(event, ele);
        },
        // 组件拖拽结束
        dragEndEle(event, ele) {
            this.$root.dragEndEleWrap(event, ele);
        },
        // 组件拖拽在目标对象拖动
        dragOverEle(event, ele) {
            this.$root.dragOverEleWrap(event, ele);
        },
        // 拖放组件
        dropEle(event, ele) {
            this.$root.dropEleWrap(event, ele);
        },
    }
});
// 组件实例, 每个被拖拽到面板上的组件都会转换为一个组件实例对象, 内含真正的组件html
joEl.register('form-config-ele-instance', {
    props: {
        ele: {
            type: Object,
            default: {}
        },
    },
    data: function () {
        return {};
    },
    computed: {
        wrapActiveEleId() {
            return this.$root.formConfigActiveEleId;
        },
        // 有子组件
        hasChildren() {
            return this.ele.childrenList && this.ele.childrenList.length > 0;
        },
        // 组件属性集合
        attrMap() {
            return jo.array2Object(this.ele.attrList, function (item) {
                return item.name;
            }, function (item) {
                if (jo.isNumber(item.value)) {
                    return Number(item.value);
                }
                return item.value;
            });
        },
    },
    template: `
        <template v-if="ele.type=='row'">
            <el-row >
                <template v-if="hasChildren">
                    <form-config-ele-wrap v-for="child in ele.childrenList" :ele="child"></form-config-ele-wrap>
                </template>
                <template v-else="">&nbsp;&nbsp;&nbsp;&nbsp;</template>
            </el-row>
        </template>
        <template v-else-if="ele.type=='col'">
            <el-col :span="attrMap['span']">
                 <template v-if="hasChildren">
                    <form-config-ele-wrap v-for="child in ele.childrenList" :ele="child"></form-config-ele-wrap>
                </template>
                <template v-else="">&nbsp;&nbsp;&nbsp;&nbsp;</template>
            </el-col>
        </template>
        <template v-else-if="ele.type=='div'">
            <div >
                <template v-if="hasChildren">
                    <form-config-ele-wrap v-for="child in ele.childrenList" :ele="child"></form-config-ele-wrap>
                </template>
                <template v-else="">&nbsp;&nbsp;&nbsp;&nbsp;</template>
            </div>
        </template>
        <template v-else-if="ele.type=='divider'">
            <el-divider class="ele-wrap-no-default-border" ></el-divider>
        </template>
        
        <template v-else-if="ele.type=='form'">
            <el-form label-width="120px">
                <template v-if="hasChildren">
                    <form-config-ele-wrap v-for="child in ele.childrenList" :ele="child"></form-config-ele-wrap>
                </template>
                <template v-else="">&nbsp;&nbsp;&nbsp;&nbsp;</template>
            </el-form>
        </template>
        <template v-else-if="ele.type=='input'">
            <el-form-item label="Activity name">
                <el-input placeholder="Please input" ></el-input>
            </el-form-item>
        </template>
        <template v-else-if="ele.type=='textarea'">
            <el-form-item label="Activity name">
                <el-input type="textarea" placeholder="Please input"></el-input>
            </el-form-item>
        </template>
        <template v-else-if="ele.type=='inputNumber'">
            <el-form-item label="Activity name">
                <el-input-number :min="1" :max="10" ></el-input-number>
            </el-form-item>
        </template>
        <template v-else-if="ele.type=='radio'">
            <el-form-item label="Activity name">
                <el-radio-group>
                    <el-radio label="1">Option 1</el-radio>
                    <el-radio label="2">Option 2</el-radio>
                </el-radio-group>
            </el-form-item>
        </template>
        <template v-else-if="ele.type=='checkbox'">
            <el-form-item label="Activity name">
                <el-checkbox-group>
                    <el-checkbox label="Option A" ></el-checkbox>
                    <el-checkbox label="Option B" ></el-checkbox>
                </el-checkbox-group>
            </el-form-item>
        </template>
        <template v-else-if="ele.type=='select'">
            <el-form-item label="Activity name">
                <el-select placeholder="Select">
                    <el-option label="1" value="Option A"></el-option>
                    <el-option label="2" value="Option B"></el-option>
                </el-select>
            </el-form-item>
        </template>
        <template v-else-if="ele.type=='cascade'">
            <el-form-item label="Activity name">
                <el-cascader></el-cascader>
            </el-form-item>
        </template>
        <template v-else-if="ele.type=='date'">
            <el-form-item label="Activity name">
                <el-date-picker type="date" placeholder="Pick a day" ></el-date-picker>
            </el-form-item>
        </template>
        <template v-else-if="ele.type=='time'">
            <el-form-item label="Activity name">
                <el-date-picker type="datetime" placeholder="Select date and time" ></el-date-picker>
            </el-form-item>
        </template>
        <template v-else-if="ele.type=='slider'">
            <el-form-item label="Activity name">
                <el-slider ></el-slider>
            </el-form-item>
        </template>
        <template v-else-if="ele.type=='switch'">
            <el-form-item label="Activity name">
                <el-switch ></el-switch>
            </el-form-item>
        </template>
        <template v-else-if="ele.type=='color'">
            <el-form-item label="Activity name">
                <el-color-picker show-alpha ></el-color-picker>
            </el-form-item>
        </template>
        <template v-else-if="ele.type=='rate'">
            <el-form-item label="Activity name">
                <el-rate ></el-rate>
            </el-form-item>
        </template>
        <template v-else-if="ele.type=='upload'">
            <el-form-item label="Activity name">
                
            </el-form-item>
        </template>
        
        <template v-else-if="ele.type=='text'">
            <span>2</span>
        </template>
        <template v-else="">{{ele}}</template>
    `,
    methods: {},
    mounted() {

    }
});

