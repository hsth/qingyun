// 代码生成-视图配置
joEl.register('lcp-generate-view', {
    emits: [],
    props: {
        formData: {
            type: Object,
            default: function () {
                return {
                    extInfo: {}
                    , fieldList: []
                };
            }
        }
    },
    data() {
        return {
            windowHeight: $(window).height(),
            // 查询条件选择表单展示
            searchConditionListForm: false,
            // 当前选中字段
            curCol: null,
            // 视图配置tab项,tablePageConfig:页面配置,tableColumnConfig:列配置
            viewConfigTab: 'tablePageConfig',
            // 格式集合 -post
            showFormatList: [],
            // 查询条件类型 -post
            asConditionTypeList: [],
            // 按钮表单
            btnFormDialog: false,
            // 按钮表单类型, 1:按钮栏, 2:行级按钮
            btnFormType: 1,
            // 按钮表单实际数据
            btnFormData: {},
            // 按钮表单展示数据
            btnFormShowData: {},
            // 被拖拽数据
            dragData: null,

            // 查询条件选中项
            searchConditionChecked: [],
        }
    },
    computed: {
        defaultHeight() {
            return this.windowHeight - 160;
        },
        // 字段列集合
        columnList() {
            if (this.formData) {
                return this.formData.fieldList;
            }
            return [];
        },
        // 视图配置
        columnListForView() {
            var arr = [];
            jo.forEach(this.columnList, function (item, i) {
                arr.push(item);
            });
            // 排序
            jo.sort(arr, function (a, b) {
                var one = jo.getDefVal(a.listColumnOrder, 0) + (a.listColumnShow == 1 ? 0 : 900000000);
                var two = jo.getDefVal(b.listColumnOrder, 0) + (b.listColumnShow == 1 ? 0 : 900000000);
                return one < two;
            });
            return arr;
        },
        // 查询条件可选项
        columnListForViewCondition() {
            var arr = [];
            var map = jo.array2Object(this.formData.extInfo.searchConditionList, function (item) {
                return item.fieldJavaName;
            })
            jo.forEach(this.columnListForView, function (item, i) {
                if (item.listQueryCondition && item.listQueryCondition != 1) {
                    // 过滤掉已经选择了的
                    if (!map[item.fieldJavaName]) {
                        arr.push(item);
                    }
                }
            });
            return arr;
        },
        // 默认排序字段选项
        columnListForDefaultSort() {
            var arr = [];
            jo.forEach(this.columnList, function (item, i) {
                if (item.listColumnSort == 1) {
                    arr.push(item);
                }
            });
            return arr;
        },
    },
    //<div class="view-config-bar" :style="{'height' : contentHeight + 'px'}">
    template: `
    <div class="view-config-bar" style="height: 100%;">
        <!--左侧视图配置选项-->
        <div class="view-config-left-bar">

            <el-tabs v-model="viewConfigTab" class="qy-full-tabs">

                <el-tab-pane label="页面配置" name="tablePageConfig">
                    <el-scrollbar>
                        <el-form :model="formData" label-position="top" label-width="100px">
                            <el-form-item label="按钮栏">
                                <template #label="scope">
                                    <el-divider content-position="left">
                                        <span>按钮栏 </span>
                                        <el-button link type="primary" @click="addListBtn(1)"><i
                                                class="fa fa-plus" aria-hidden="true"></i></el-button>
                                    </el-divider>
                                </template>
                                <div id="btnBarConfigBox" style="display: flex;flex-wrap: wrap;"
                                     v-if="formData.extInfo.buttonBarConfig && formData.extInfo.buttonBarConfig.buttonList">
                                    <div v-for="btn in formData.extInfo.buttonBarConfig.buttonList"
                                         class="btn-bar-item"
                                         draggable="true"
                                         @dragstart.stop.self="dragStart($event, btn)"
                                         @drag.stop.self="drag($event, btn)"
                                         @dragend.stop.self="dragEnd($event, btn)"
                                         @dragenter.stop="dragEnter($event, btn)"
                                         @dragleave.stop="dragLeave($event, btn)"
                                         @dragover.stop.prevent="dragOver($event, btn)"
                                         @drop.stop="drop($event, btn, 1)"
                                    >
                                        <div>
                                            <el-button v-if=" btn.category == 'plain' " 
                                                       :type="btn.type" :size="btn.size" plain><span
                                                    v-html="btn.icon"></span>{{btn.name}}
                                            </el-button>
                                            <el-button v-else-if=" btn.category == 'link' " link
                                                       :type="btn.type" :size="btn.size" ><span
                                                    v-html="btn.icon"></span>{{btn.name}}
                                            </el-button>
                                            <el-button v-else-if=" btn.category == 'text' " text
                                                       :type="btn.type" :size="btn.size" plain><span
                                                    v-html="btn.icon"></span>{{btn.name}}
                                            </el-button>
                                            <el-button v-else-if=" btn.category == 'round' " round
                                                       :type="btn.type" :size="btn.size" plain><span
                                                    v-html="btn.icon"></span>{{btn.name}}
                                            </el-button>
                                            <el-button v-else-if=" btn.category == 'circle' " circle
                                                       :type="btn.type" :size="btn.size" plain><span
                                                    v-html="btn.icon"></span>{{btn.name}}
                                            </el-button>
                                            <el-button v-else="" :type="btn.type" :size="btn.size"><span
                                                    v-html="btn.icon"></span>{{btn.name}}
                                            </el-button>
                                        </div>
                                        <div style="font-size: 10px;line-height: 1;display: flex;flex-direction:column;width: 16px;text-align: center;margin-left: 2px;">
                                <span class="btn-config-oper" @click="editListBtn(btn, 1)"><i
                                        class="fa fa-cog" aria-hidden="true"></i></span>
                                            <span class="btn-config-oper" @click="deleteListBtn(btn, 1)"><i
                                                    class="fa fa-trash-o" aria-hidden="true"></i></span>
                                        </div>
                                    </div>
                                </div>

                            </el-form-item>
                            <el-form-item label="表格首列类型：">
                                <template #label="scope">
                                    <el-divider content-position="left">表格首列类型</el-divider>
                                </template>
                                <jo-el-data url="{URL_CMS}lowcode/generate/getFirstColumnTypeList">
                                    <template #default="scope">
                                        <el-radio-group v-model="formData.extInfo.tableFirstColumnType">
                                            <el-radio :label="item.key" v-for="item in scope.data"
                                                      style="width: 60px;">
                                                {{item.value}}
                                            </el-radio>
                                        </el-radio-group>
                                    </template>
                                </jo-el-data>
                            </el-form-item>
                            <el-form-item label="表格行级按钮：">
                                <template #label="scope">
                                    <el-divider content-position="left">
                                        <span>表格行级按钮 </span>
                                        <el-button link type="primary" @click="addListBtn(2)"><i
                                                class="fa fa-plus" aria-hidden="true"></i></el-button>
                                    </el-divider>
                                </template>
                                <div style="display: flex;flex-wrap: wrap;"
                                     v-if="formData.extInfo.tableRowButtonConfig && formData.extInfo.tableRowButtonConfig.buttonList">
                                    <div class="btn-bar-item"
                                         v-for="btn in formData.extInfo.tableRowButtonConfig.buttonList"
                                         draggable="true"
                                         @dragstart.stop.self="dragStart($event, btn)"
                                         @dragend.stop.self="dragEnd($event, btn)"
                                         @dragover.stop.prevent="dragOver($event, btn)"
                                         @drop.stop="drop($event, btn, 2)"
                                    >
                                        <div>
                                            <el-button v-if=" btn.category == 'plain' " plain
                                                       :type="btn.type" :size="btn.size" ><span
                                                    v-html="btn.icon"></span>{{btn.name}}
                                            </el-button>
                                            <el-button v-else-if=" btn.category == 'link' " link
                                                       :type="btn.type" :size="btn.size" plain><span
                                                    v-html="btn.icon"></span>{{btn.name}}
                                            </el-button>
                                            <el-button v-else-if=" btn.category == 'text' " text
                                                       :type="btn.type" :size="btn.size" plain><span
                                                    v-html="btn.icon"></span>{{btn.name}}
                                            </el-button>
                                            <el-button v-else-if=" btn.category == 'round' " round
                                                       :type="btn.type" :size="btn.size" plain><span
                                                    v-html="btn.icon"></span>{{btn.name}}
                                            </el-button>
                                            <el-button v-else-if=" btn.category == 'circle' " circle
                                                       :type="btn.type" :size="btn.size" plain><span
                                                    v-html="btn.icon"></span>{{btn.name}}
                                            </el-button>
                                            <el-button v-else="" :type="btn.type" :size="btn.size"><span
                                                    v-html="btn.icon"></span>{{btn.name}}
                                            </el-button>
                                        </div>
                                        <div style="font-size: 10px;line-height: 1;display: flex;flex-direction:column;width: 16px;text-align: center;margin-left: 2px;">
                                <span class="btn-config-oper" @click="editListBtn(btn, 2)"><i
                                        class="fa fa-cog" aria-hidden="true"></i></span>
                                            <span class="btn-config-oper" @click="deleteListBtn(btn, 2)"><i
                                                    class="fa fa-trash-o" aria-hidden="true"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </el-form-item>

                            <el-form-item label="搜索条件：">
                                <template #label="scope">
                                    <el-divider content-position="left">
                                        搜索条件（
                                        <el-button link type="primary"
                                                   @click="searchConditionListForm = true">添加
                                        </el-button>
                                        ）
                                    </el-divider>
                                </template>
                                <div id="searchConditionBox" style="display: flex;flex-wrap: wrap;"
                                     v-if="formData.extInfo.searchConditionList">
                                    <div v-for="(btn,idx) in formData.extInfo.searchConditionList"
                                         class="btn-bar-item"
                                         draggable="true"
                                         @dragstart.stop.self="dragStart2($event, btn)"
                                         @dragend.stop.self="dragEnd2($event, btn)"
                                         @dragover.stop.prevent="dragOver2($event, btn)"
                                         @drop.stop="drop2($event, btn, 1)"
                                    >
                                        <div>
                                            <el-tag>
                                                {{btn.listHeaderTitle}}
                                            </el-tag>
                                        </div>
                                        <div class="drag-item-oper-bar">
                                            <span class="btn-config-oper"
                                                  @click="formData.extInfo.searchConditionList.splice(idx,1)"><i
                                                    class="fa fa-trash-o" aria-hidden="true"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </el-form-item>

                            <el-form-item label="默认排序：">
                                <template #label="scope">
                                    <el-divider content-position="left">默认排序</el-divider>
                                </template>
                                <el-descriptions :column="1" border size="small" style="width: 100%;">
                                    <el-descriptions-item label="字段">
                                        <el-select v-model="formData.extInfo.defaultSortBy" size="small" placeholder="默认排序字段">
                                            <el-option v-for="item in columnListForDefaultSort" :value="item.fieldDbName">{{item.fieldDbName}}：{{item.listHeaderTitle}}</el-option>
                                        </el-select>
                                    </el-descriptions-item>
                                    <el-descriptions-item label="类型">
                                        <el-select v-model="formData.extInfo.defaultSortType" size="small" placeholder="默认排序类型">
                                            <el-option value="asc" label="asc"></el-option>
                                            <el-option value="desc" label="desc"></el-option>
                                        </el-select>
                                    </el-descriptions-item>
                                </el-descriptions>
                            </el-form-item>

                            <el-form-item>
                                <template #label="scope">
                                    <el-divider content-position="left">树形表格</el-divider>
                                </template>
                                <lcp-config-item title="启用状态" width="6em">
                                    <el-switch v-model="formData.extInfo.supportTreeTable"
                                               :active-value="1"
                                               :inactive-value="0"></el-switch>
                                </lcp-config-item>
                                <lcp-config-item title="row-key" width="6em" v-if="formData.extInfo.supportTreeTable==1">
                                    <el-input v-model="formData.extInfo.treeTableRowKey" placeholder="数据行唯一标识字段,默认为id"></el-input>
                                </lcp-config-item>
                                <lcp-config-item title="子数据字段名" width="6em" v-if="formData.extInfo.supportTreeTable==1">
                                    <el-input v-model="formData.extInfo.treeTableChildrenField" placeholder="子数据字段名,默认为children"></el-input>
                                </lcp-config-item>

                            </el-form-item>



                            <el-form-item>
                                <template #label="scope">
                                    <el-divider content-position="left">加载数据时loading</el-divider>
                                </template>
                                <lcp-config-item title="开启loading" width="6em">
                                    <el-switch v-model="formData.extInfo.enableTableLoading"
                                               :active-value="1"
                                               :inactive-value="0"></el-switch>
                                </lcp-config-item>
                            </el-form-item>

                            <el-form-item>
                                <template #label="scope">
                                    <el-divider content-position="left">分页大小（pageSize）</el-divider>
                                </template>
                                <el-input-number v-model="formData.extInfo.pageSize" :step="10"
                                                 :min="1"></el-input-number>
                            </el-form-item>
                            <el-form-item label="表格数据URL：">
                                <template #label="scope">
                                    <el-divider content-position="left">表格数据URL</el-divider>
                                </template>
                                <el-input v-model="formData.extInfo.tableDataUrl"
                                          placeholder="非必要可不填，为空会自动生成url"></el-input>
                            </el-form-item>
                            <el-form-item label="数据删除URL：">
                                <template #label="scope">
                                    <el-divider content-position="left">数据删除URL</el-divider>
                                </template>
                                <el-input v-model="formData.extInfo.deleteUrl"
                                          placeholder="非必要可不填，为空会自动生成url"></el-input>
                            </el-form-item>
                        </el-form>
                    </el-scrollbar>
                </el-tab-pane>

                <el-tab-pane label="列配置" name="tableColumnConfig" :lazy="true">
                    <el-scrollbar>
                        <div class="column-config-detail" v-if="curCol">
                            <lcp-config-item title="字段名">&nbsp;{{curCol.fieldDbName}}</lcp-config-item>
                            <lcp-config-item title="字段类型">
                                &nbsp;{{curCol.fieldDbType}}
                                <span v-if="curCol.fieldDbLength">({{curCol.fieldDbLength}})</span>
                            </lcp-config-item>
                            <lcp-config-item title="Java属性">&nbsp;{{curCol.fieldJavaName}}</lcp-config-item>
                            <lcp-config-item title="表头标题">
                                <el-input v-model="curCol.listHeaderTitle"></el-input>
                            </lcp-config-item>
                            <lcp-config-item title="是否展示">
                                <el-switch v-model="curCol.listColumnShow" :active-value="1"
                                           :inactive-value="0"></el-switch>
                            </lcp-config-item>
                            <lcp-config-item title="内容格式">
                                <el-select v-model="curCol.listValueFormat">
                                    <el-option :value="item.key" :label="item.value"
                                               v-for="item in showFormatList"></el-option>
                                </el-select>
                            </lcp-config-item>
                            <lcp-config-item title="" :y="true" v-if="curCol.listValueFormat == 'formatter'">
                                <el-input v-model="curCol.formControlConfig.listValueFormatHtml"
                                          type="textarea"
                                          :autosize="{ minRows: 1, maxRows: 4 }"
                                          placeholder="自定义格式化html代码"></el-input>
                            </lcp-config-item>
                            <lcp-config-item title="展示顺序">
                                <el-input-number v-model="curCol.listColumnOrder" :min="1" :step="5"></el-input-number>
                            </lcp-config-item>
                            <lcp-config-item title="支持排序">
                                <el-switch v-model="curCol.listColumnSort" :active-value="1"
                                           :inactive-value="0"></el-switch>
                            </lcp-config-item>
                            <lcp-config-item title="搜索类型">
                                <el-select v-model="curCol.listQueryCondition">
                                    <el-option :value="item.key" :label="item.value"
                                               v-for="item in asConditionTypeList"></el-option>
                                </el-select>
                            </lcp-config-item>
                            <lcp-config-item title="列宽">
                                <el-input v-model="curCol.listColumnWidth"></el-input>
                            </lcp-config-item>
                            <lcp-config-item title="表头对齐">
                                <el-radio-group v-model="curCol.listHeaderAlign">
                                    <el-radio-button label="left">左</el-radio-button>
                                    <el-radio-button label="center">中</el-radio-button>
                                    <el-radio-button label="right">右</el-radio-button>
                                </el-radio-group>
                            </lcp-config-item>
                            <lcp-config-item title="内容对齐">
                                <el-radio-group v-model="curCol.listValueAlign">
                                    <el-radio-button label="left">左</el-radio-button>
                                    <el-radio-button label="center">中</el-radio-button>
                                    <el-radio-button label="right">右</el-radio-button>
                                </el-radio-group>
                            </lcp-config-item>
                            <lcp-config-item title="固定列">
                                <el-radio-group v-model="curCol.listColumnFixed">
                                    <el-radio-button label="left">左</el-radio-button>
                                    <el-radio-button label="no">无</el-radio-button>
                                    <el-radio-button label="right">右</el-radio-button>
                                </el-radio-group>
                            </lcp-config-item>

                            <lcp-config-item title="表头提示">
                                <el-input v-model="curCol.listHeaderTips"
                                          type="textarea"
                                          :autosize="{ minRows: 1, maxRows: 4 }"
                                          placeholder="表头提示"></el-input>
                            </lcp-config-item>
                        </div>

                    </el-scrollbar>
                </el-tab-pane>
            </el-tabs>

        </div>


        <div class="view-config-right-bar">
            <!--表格配置-->
            <el-divider content-position="left">表格列配置</el-divider>
            <el-table :data="columnListForView" border header-cell-class-name="jo-el-table-header"
                      :height="defaultHeight-120" :row-style="tableConfigRowStyle" row-key="id">
                <el-table-column type="index" label="#" width="50" fixed="left" align="center"
                                 header-align="center"></el-table-column>
                <el-table-column prop="fieldDbName" label="操作" width="125" fixed="left">
                    <template #default="scope">
                        <el-button type="primary" link @click="tableColDetail(scope.row)">详情</el-button>
                        <el-button type="success" :disabled="scope.$index<1" link @click="updateListColumnOrder(scope.row,scope.$index,1)"><i class="fa fa-arrow-up"></i></el-button>
                        <el-button type="warning" :disabled="scope.$index>=(columnListForView.length-1)" link @click="updateListColumnOrder(scope.row,scope.$index,2)"><i class="fa fa-arrow-down"></i></el-button>
                    </template>
                </el-table-column>
                <el-table-column prop="fieldDbName" label="字段" min-width="160" fixed="left">
                    <template #default="scope">
                        <div :class="{'field-not-null' : scope.row.fieldDbNotNull==1}">
                            <div :title="scope.row.fieldDbRemark">
                                {{scope.row.fieldDbName}}
                                <el-tag type="success">
                                    {{scope.row.fieldDbType}}
                                    <span v-if="scope.row.fieldDbLength">({{scope.row.fieldDbLength}})</span>
                                </el-tag>
                            </div>
                        </div>
                    </template>
                </el-table-column>
                <el-table-column prop="fieldJavaName" label="Java属性" min-width="120"></el-table-column>

                <el-table-column prop="listHeaderTitle" label="表头标题" min-width="120">
                    <template #default="scope">
                        <el-input v-model="scope.row.listHeaderTitle" size="small"></el-input>
                    </template>
                </el-table-column>
                <el-table-column prop="listColumnShow" label="列展示" width="90">
                    <template #default="scope">
                        <el-switch v-model="scope.row.listColumnShow" :active-value="1"
                                   :inactive-value="0"></el-switch>
                    </template>
                </el-table-column>

<!--                <el-table-column prop="listColumnOrder" label="展示顺序" width="135">-->
<!--                    <template #default="scope">-->
<!--                        <el-input-number v-model="scope.row.listColumnOrder" :min="1" :step="5"-->
<!--                                         size="small" style="width: 100px;"></el-input-number>-->
<!--                    </template>-->
<!--                </el-table-column>-->


                <el-table-column prop="listColumnSort" label="列排序">
                    <template #default="scope">
                        <el-switch v-model="scope.row.listColumnSort" :active-value="1"
                                   :inactive-value="0"></el-switch>
                    </template>
                </el-table-column>
                <el-table-column prop="listQueryCondition" label="搜索类型" width="100">
                    <template #default="scope">
                        <el-select v-model="scope.row.listQueryCondition" size="small">
                            <el-option :value="item.key" :label="item.value"
                                       v-for="item in asConditionTypeList"></el-option>
                        </el-select>
                    </template>
                </el-table-column>

                <el-table-column prop="listColumnWidth" label="列宽" width="100">
                    <template #default="scope">
                        <el-input v-model="scope.row.listColumnWidth" size="small"></el-input>
                    </template>
                </el-table-column>
                <el-table-column prop="listValueFormat" label="内容格式" width="120">
                    <template #default="scope">
                        <el-select v-model="scope.row.listValueFormat" size="small">
                            <el-option :value="item.key" :label="item.value"
                                       v-for="item in showFormatList"></el-option>
                        </el-select>
                    </template>
                </el-table-column>

                <el-table-column prop="listHeaderAlign" label="表头align" width="135">
                    <template #default="scope">
                        <el-radio-group v-model="scope.row.listHeaderAlign" size="small">
                            <el-radio-button label="left">左</el-radio-button>
                            <el-radio-button label="center">中</el-radio-button>
                            <el-radio-button label="right">右</el-radio-button>
                        </el-radio-group>
                    </template>
                </el-table-column>
                <el-table-column prop="listValueAlign" label="内容align" width="135">
                    <template #default="scope">
                        <el-radio-group v-model="scope.row.listValueAlign" size="small">
                            <el-radio-button label="left">左</el-radio-button>
                            <el-radio-button label="center">中</el-radio-button>
                            <el-radio-button label="right">右</el-radio-button>
                        </el-radio-group>
                    </template>
                </el-table-column>
                <el-table-column prop="listColumnFixed" label="固定列" width="135">
                    <template #default="scope">
                        <el-radio-group v-model="scope.row.listColumnFixed" size="small">
                            <el-radio-button label="left">左</el-radio-button>
                            <el-radio-button label="no">无</el-radio-button>
                            <el-radio-button label="right">右</el-radio-button>
                        </el-radio-group>
                    </template>
                </el-table-column>

            </el-table>

            <!--表格预览-->
            <el-divider content-position="left">表格预览</el-divider>
            <el-table :data="[{}]" border header-cell-class-name="jo-el-table-header"
                      style="margin-top: 15px;">
                <el-table-column v-if=" formData.extInfo.tableFirstColumnType == 'checkbox' "
                                 prop="selection" label="#" type="selection" header-align="center"
                                 align="center" fixed="left" width="50"></el-table-column>
                <el-table-column v-if=" formData.extInfo.tableFirstColumnType == 'radio' " type="radio"
                                 header-align="center" align="center" fixed="left" width="50">
                    <template #default="scope2">
                        <el-radio model-value="0" :label="scope2.$index"><span></span></el-radio>
                    </template>
                </el-table-column>
                <el-table-column v-if=" formData.extInfo.tableFirstColumnType == 'index' " prop="index"
                                 label="#" type="index" header-align="center" align="center"
                                 fixed="left" width="50"></el-table-column>
                <template v-for="item in columnListForView">
                    <el-table-column v-if="item.listColumnShow==1" :key="item.id"
                                     :prop="item.fieldJavaName" :label="item.listHeaderTitle"
                                     :min-width="item.listColumnWidth"
                                     :header-align="item.listHeaderAlign" :align="item.listValueAlign"
                                     :fixed=" item.listColumnFixed != 'no' ? item.listColumnFixed : false "
                                     :sortable=" item.listColumnSort == 1 ? 'custom' : false ">
                        <span v-if="item.fieldJavaType=='Integer' || item.fieldJavaType=='Long'">10</span>
                        <span v-else-if="item.fieldJavaType=='Date'">2022-07-03 12:00:00</span>
                        <span v-else="">示例文字</span>
                    </el-table-column>
                </template>
            </el-table>
        </div>
        
    </div>
    
    <!--查询条件选择表单-->
    <el-dialog v-model="searchConditionListForm" title="添加查询条件">
        <span v-if="!columnListForViewCondition || columnListForViewCondition.length == 0">暂无可选字段~</span>
        <el-checkbox-group v-model="searchConditionChecked">
            <el-checkbox v-for="item in columnListForViewCondition"
                         style="width: 110px;"
                         :label="item.fieldJavaName">{{item.listHeaderTitle}}
            </el-checkbox>
        </el-checkbox-group>
        <template #footer>
            <span class="dialog-footer">
                <el-button @click="searchConditionListForm = false">取消</el-button>
                <el-button type="primary" @click="searchConditionConfirm">确定</el-button>
            </span>
        </template>
    </el-dialog>
    
    <!--按钮弹层-->
    <el-dialog v-model="btnFormDialog" title="按钮">
        <el-form :model="btnFormShowData" label-position="right" label-width="85px">
            <el-form-item label="按钮编号" v-if="btnFormData.code">
                <el-input v-model="btnFormShowData.code" :disabled="!!btnFormData.code"></el-input>
            </el-form-item>
            <el-form-item label="按钮名称">
                <el-input v-model="btnFormShowData.name" placeholder="例如：新增"></el-input>
            </el-form-item>
            <el-form-item label="按钮尺寸">
                <el-radio-group v-model="btnFormShowData.size">
                    <el-radio-button label="large">大</el-radio-button>
                    <el-radio-button label="default">正常</el-radio-button>
                    <el-radio-button label="small">小</el-radio-button>
                </el-radio-group>
            </el-form-item>
            <el-form-item label="按钮样式">
                <el-select v-model="btnFormShowData.type">
                    <jo-el-data url="/lowcode/generate/getButtonTypeList">
                        <template #default="scope">
                            <el-option v-for="item in scope.data" :key="item.key" :value="item.key" :label="item.value"></el-option>
                        </template>
                    </jo-el-data>
                </el-select>
            </el-form-item>
            <el-form-item label="按钮类型">
                <el-select v-model="btnFormShowData.category">
                    <jo-el-data url="/lowcode/generate/getButtonCategoryList">
                        <template #default="scope">
                            <el-option v-for="item in scope.data" :key="item.key" :value="item.key" :label="item.value"></el-option>
                        </template>
                    </jo-el-data>
                </el-select>
            </el-form-item>
            <el-form-item label="显示条件">
                <el-input v-model="btnFormShowData.showRule" placeholder="显示条件表达式"></el-input>
            </el-form-item>
            <el-form-item label="点击事件">
                <el-input v-model="btnFormShowData.clickFunctionName" placeholder="例如：search，没有则为空"></el-input>
            </el-form-item>
            <el-form-item label="事件脚本">
                <el-input type="textarea" :rows="2" autosize v-model="btnFormShowData.clickScript"
                          placeholder="例如：search(){...}，没有则为空"></el-input>
            </el-form-item>
        </el-form>
        <template #footer>
            <span class="dialog-footer">
                <el-button @click="btnFormDialog=false">取消</el-button>
                <el-button type="primary" @click="saveListBtn">确认</el-button>
            </span>
        </template>
    </el-dialog>
    `,
    methods: {
        // 更新表格列展示顺序
        updateListColumnOrder(item, idx, type) {
            if (!this.columnListForView) {
                return;
            }
            var target;
            if (type===1){// 向上
                // 找到上面那个
                if (idx > 0) {
                    target = this.columnListForView[idx-1];
                }
            } else {// 向下
                // 找到下面那个
                if (idx < this.columnListForView.length -1) {
                    target = this.columnListForView[idx+1];
                }
            }
            // 互换展示顺序属性
            if (target) {
                var temp = item.listColumnOrder;
                item.listColumnOrder = target.listColumnOrder;
                target.listColumnOrder = temp;
            }
        },
        // 选中字段
        colDetail(col) {
            this.curCol = col;
        },
        // 选中表格配置列字段
        tableColDetail(col) {
            this.curCol = col;
            this.viewConfigTab = 'tableColumnConfig';
        },
        // 新增列表页按钮
        addListBtn(type) {
            this.btnFormType = type;
            if (type === 1) {
                this.btnFormData = {size: 'default', type: 'primary', category: 'plain'};
                this.btnFormShowData = {size: 'default', type: 'primary', category: 'plain'};
            } else if (type === 2) {
                this.btnFormData = {size: 'default', type: 'primary', category: 'link'};
                this.btnFormShowData = {size: 'default', type: 'primary', category: 'link'};
            }
            this.btnFormDialog = true;
        },
        // 编辑按钮
        editListBtn(btn, type) {
            this.btnFormType = type;
            this.btnFormData = btn;
            this.btnFormShowData = jo.copyObjectDeep(btn);
            this.btnFormDialog = true;
        },
        // 删除按钮
        deleteListBtn(btn, type) {
            var _this = this;
            jo.confirm('您确定要删除按钮【' + btn.name + '】嘛?', function () {
                var arr = [];
                var list = [];
                if (type === 1) {
                    list = _this.formData.extInfo.buttonBarConfig.buttonList;
                } else if (type === 2) {
                    list = _this.formData.extInfo.tableRowButtonConfig.buttonList;
                }
                jo.forEach(list, function (item) {
                    if (item.code != btn.code) {
                        arr.push(item);
                    }
                });
                if (type === 1) {
                    _this.formData.extInfo.buttonBarConfig.buttonList = arr;
                } else if (type === 2) {
                    _this.formData.extInfo.tableRowButtonConfig.buttonList = arr;
                }
            });
        },
        // 保存按钮
        saveListBtn() {
            // 校验
            if (!this.btnFormShowData.name || !this.btnFormShowData.size) {
                jo.showErrorMsg('存在必填值为空');
                return;
            }
            // 区分新增和修改
            if (this.btnFormData.code) {
                // code有效是更新
                for (var k in this.btnFormShowData) {
                    var newVal = this.btnFormShowData[k];
                    var oldVal = this.btnFormData[k];
                    // 值前后有变化则更新
                    if (newVal != oldVal) {
                        this.btnFormData[k] = newVal;
                        console.debug('[保存按钮] 更新按钮属性,字段=%s,新值=%s,老值=%s', k, newVal, oldVal);
                    }
                }
            } else {
                // 新增
                if (!this.formData.extInfo.buttonBarConfig) {
                    this.formData.extInfo.buttonBarConfig = {};
                }
                if (!this.formData.extInfo.buttonBarConfig.buttonList) {
                    this.formData.extInfo.buttonBarConfig.buttonList = [];
                }
                if (!this.formData.extInfo.tableRowButtonConfig) {
                    this.formData.extInfo.tableRowButtonConfig = {};
                }
                if (!this.formData.extInfo.tableRowButtonConfig.buttonList) {
                    this.formData.extInfo.tableRowButtonConfig.buttonList = [];
                }
                // 新增时赋值默认code
                this.btnFormShowData.code = jo.getUUID(16);
                if (this.btnFormType === 1) {
                    this.formData.extInfo.buttonBarConfig.buttonList.push(this.btnFormShowData);
                } else if (this.btnFormType === 2) {
                    this.formData.extInfo.tableRowButtonConfig.buttonList.push(this.btnFormShowData);
                }
            }
            // 关闭前清掉数据
            this.btnFormData = {};
            this.btnFormShowData = {};
            this.btnFormType = 0;
            // 关闭
            this.btnFormDialog = false;
        },
        // 表格配置行样式{ row, rowIndex }
        tableConfigRowStyle(obj) {
            if (obj.row && obj.row.listColumnShow == 0) {
                return {backgroundColor: '#FAFAFA', textDecoration: 'line-through'};
            }
        },
        // 查询条件选择确认
        searchConditionConfirm() {
            var _this = this;
            if (this.searchConditionChecked && this.searchConditionChecked.length > 0) {
                if (!this.formData.extInfo.searchConditionList) {
                    this.formData.extInfo.searchConditionList = [];
                }
                var map = jo.array2Object(this.columnListForViewCondition, function (item) {
                    return item.fieldJavaName;
                });
                jo.forEach(this.searchConditionChecked, function (item) {
                    var field = map[item];
                    if (!field) {
                        console.warn('[查询条件选择确认] 根据字段名查找字段失败,字段名:', item, map)
                        return;
                    }
                    _this.formData.extInfo.searchConditionList.push({
                        fieldJavaName: item,
                        listHeaderTitle: field.listHeaderTitle,
                        listQueryCondition: field.listQueryCondition
                    });
                });

            }
            this.searchConditionListForm = false;
            // 清空选项
            this.searchConditionChecked = [];
        },
        // 拖拽开始:被拖拽对象
        dragStart(e, item) {
            // 设置被拖拽按钮
            this.dragBtn = item;
        },
        // 拖拽中:被拖拽对象
        drag(e, item) {
        },
        // 拖拽结束:被拖拽对象
        dragEnd(e, item) {
            // 清空数据
            this.dragBtn = null;
            // 清空
            $('.btn-bar-item-left').removeClass('btn-bar-item-left');
            $('.btn-bar-item-right').removeClass('btn-bar-item-right');
        },
        // 拖拽进入:目标对象
        dragEnter(e, item) {
        },
        // 拖拽在目标对象上移动:目标对象
        dragOver(e, item) {
            e.preventDefault();
            this.dragOverCore(e, item, this.dragBtn, 'code');
        },
        // 拖拽离开:目标对象
        dragLeave(e, item) {
        },
        // 拖拽放置:目标对象
        drop(e, item, type) {
            if (!this.dragBtn) {
                console.warn('[按钮拖拽放置] 被拖拽按钮查找失败');
                return;
            }
            if (!item) {
                return;
            }
            // 被拖拽按钮配置
            var dragBtn = this.dragBtn;
            if (dragBtn.code == item.code) {
                console.log('[按钮拖拽放置] 被拖拽按钮与放置按钮疑似同一个,不做处理');
                return;
            }

            // 定位信息
            var position = jo.getPositionDetail(e, 'btn-bar-item')

            // 判断鼠标是否过了中线, 过了就放右边, 否则放左边
            var arr = [];
            var oldArr = [];
            if (type === 1) {
                oldArr = this.formData.extInfo.buttonBarConfig.buttonList;
            } else if (type === 2) {
                oldArr = this.formData.extInfo.tableRowButtonConfig.buttonList;
            }
            jo.forEach(oldArr, function (one) {
                if (one.code == dragBtn.code) {
                    // 自己不处理
                    return;
                }
                // 匹配到放置的按钮
                if (one.code == item.code) {
                    // 放置区按钮, 则放他后面or前面
                    if (position.atRight) {
                        arr.push(one);
                        arr.push(jo.copyObjectDeep(dragBtn));// 拷贝一份新对象, 支持跨按钮区域拖拽, 防止拖拽后修改发生联动
                    } else {
                        arr.push(jo.copyObjectDeep(dragBtn));
                        arr.push(one);
                    }
                } else {
                    arr.push(one);
                }
            });
            if (type === 1) {
                this.formData.extInfo.buttonBarConfig.buttonList = arr;
            } else if (type === 2) {
                this.formData.extInfo.tableRowButtonConfig.buttonList = arr;
            }
        },


        dragStart2(e, item) {
            // 设置被拖拽按钮
            this.dragData = item;
        },
        dragEnd2(e, item) {
            // 清空数据
            this.dragData = null;
            // 清空
            $('.btn-bar-item-left').removeClass('btn-bar-item-left');
            $('.btn-bar-item-right').removeClass('btn-bar-item-right');
        },
        dragOver2(e, item) {
            e.preventDefault();
            this.dragOverCore(e, item, this.dragData, 'fieldJavaName');
        },
        drop2(e, item, type) {
            if (!this.dragData) {
                console.warn('[拖拽放置] 被拖拽数据查找失败');
                return;
            }
            if (!item) {
                return;
            }
            // 被拖拽按钮配置
            var dragItem = this.dragData;
            if (dragItem.fieldJavaName == item.fieldJavaName) {
                console.log('[拖拽放置] 被拖拽数据与放置数据疑似同一个,不做处理');
                return;
            }

            // 定位信息
            var position = jo.getPositionDetail(e, 'btn-bar-item')

            // 判断鼠标是否过了中线, 过了就放右边, 否则放左边
            var arr = [];
            var oldArr = [];
            if (type === 1) {
                oldArr = this.formData.extInfo.searchConditionList;
            }
            jo.forEach(oldArr, function (one) {
                if (one.fieldJavaName == dragItem.fieldJavaName) {
                    // 自己不处理
                    return;
                }
                // 匹配到放置的按钮
                if (one.fieldJavaName == item.fieldJavaName) {
                    // 放置区按钮, 则放他后面or前面
                    if (position.atRight) {
                        arr.push(one);
                        arr.push(jo.copyObjectDeep(dragItem));
                    } else {
                        arr.push(jo.copyObjectDeep(dragItem));
                        arr.push(one);
                    }
                } else {
                    arr.push(one);
                }
            });
            if (type === 1) {
                this.formData.extInfo.searchConditionList = arr;
            }
        },

        // 拖拽在目标对象上移动, e:事件, item:目标数据, dragItem: 被拖拽数据, idAttrName:用来判断数据是否相等的字段
        dragOverCore(e, item, dragItem, idAttrName) {
            if (!item || !dragItem) {
                console.info('[拖拽在目标对象上移动] 目标数据和拖拽数据无效', item, dragItem)
                return;
            }
            if (dragItem[idAttrName] != item[idAttrName]) {
                var pos = jo.getPositionDetail(e, 'btn-bar-item');
                if ($(pos.target).hasClass('btn-bar-item-left') && pos.atLeft) {
                    // 已经是左侧提示的并且目前还在左侧的, 不处理
                } else if ($(pos.target).hasClass('btn-bar-item-right') && pos.atRight) {
                    // 已经是右侧提示的并且目前还在右侧的, 不处理
                } else {
                    // 清空
                    $('.btn-bar-item-left').removeClass('btn-bar-item-left');
                    $('.btn-bar-item-right').removeClass('btn-bar-item-right');
                    // 然后重新设置
                    if (pos.atLeft) {
                        $(pos.target).addClass('btn-bar-item-left');
                    } else {
                        $(pos.target).addClass('btn-bar-item-right');
                    }
                }
            } else {
                // 清空
                $('.btn-bar-item-left').removeClass('btn-bar-item-left');
                $('.btn-bar-item-right').removeClass('btn-bar-item-right');
            }
        },
    },
    mounted() {
        jo.postJsonAjax("{URL_CMS}lowcode/generate/getShowFormatList", {}).success((json) => {
            this.showFormatList = json.data;
        });
        jo.postJsonAjax("{URL_CMS}lowcode/generate/getQueryConditionTypeList", {}).success((json) => {
            this.asConditionTypeList = json.data;
        });
        console.info('[lcp-generate-view] mounted.');
    }
});