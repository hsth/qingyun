// 低代码设计拖拽处理
var LCP_DESIGN_DRAG_VUE = {
    data() {
        return {
            // dom树操作次数
            domTreeOperate: 1,
        };
    },
    methods: {
        // 左侧选项转实际元素对象(或者画布元素拖拽时)
        convertOptionToEle(option) {
            var obj = {
                id: this.buildComponentId(option),
                type: option.type,
                subType: option.subType,
                classList: jo.copyObjectDeep(option.classList || []),
                styleList: jo.copyObjectDeep(option.styleList || []),
                attrList: jo.copyObjectDeep(option.attrList || []), // 组件属性, 实际上为组件设置的属性和属性值, 下面的customAttrList是值后台设置了哪些可设置的属性
                eventList: jo.copyObjectDeep(option.eventList || []),
                childrenList: [],
                // -- 组件配置元数据放下面 --
                customAttrList: jo.copyObjectDeep(option.customAttrList || []), // 其他定制属性 {name,value,dataType(number,string,object,array)}
                canAsParent: !(option.canAsParent === 'no' || option.canAsParent === false),//是否可以作为父节点
            };
            // 默认组件属性
            if (option.attrMetaList) {
                // 当前已经配置了的属性{name,value}
                var curAttr = jo.array2Object(obj.attrList, item=>{
                    return item.name;
                });
                jo.forEach(option.attrMetaList, attr=>{
                    // 当前没有配置该属性且有默认值的,设置默认值
                    if (!curAttr[attr.attrName] && jo.isValid(attr.attrDefault)) {
                        obj.attrList.push({
                            name: attr.attrName,
                            value: attr.attrDefault,
                            valueType: lcpUtil.parseAttrValueType(attr.attrDataType, attr.attrDefault)
                        });
                    }
                });
            }
            // 子组件
            jo.forEach(option.childrenList, item=>{
                obj.childrenList.push(this.convertOptionToEle(item));
            });
            // if (option.childrenList && option.childrenList.length > 0) {
            //     for (let i = 0; i < option.childrenList.length; i++) {
            //         var item = option.childrenList[i];
            //         obj.childrenList.push(this.convertOptionToEle(item));
            //     }
            // }
            return obj;
        },
        // 拖放到表单配置容器
        dropFormConfigContainer(event) {
            // console.info('在表单容器拖放', event, this.dragContext);
            if (this.dragContext.dragType === this.formConfigDragType.option.code) {
                if (this.dragContext.dragEleOption) {
                    this.pageConfig.elementList.push(this.convertOptionToEle(this.dragContext.dragEleOption));
                    // dom树变更
                    this.domTreeOperate++;
                }
            } else {
                console.info('表单容器不支持此拖拽类型', this.dragContext);
            }
        },
        dragStartEleOption(event, ele) {
            console.info('开始拖拽Option', ele)
            this.dragContext = {
                dragType: this.formConfigDragType.option.code,
                dragEleOption: ele,
            };
        },
        // 拖拽左侧组件选项结束
        dragEndEleOption(event, ele) {
            console.info('结束拖拽Option', ele)
            this.dragContext = {};
            this.clearDropStyle();
        },
        // 组件包装器拖拽开始
        dragStartEleWrap(event, ele) {
            console.info('开始拖拽Wrap', ele)
            this.dragContext = {
                dragType: this.formConfigDragType.inner.code,
                dragEleOption: ele,
            };
        },
        // 组件包装器拖拽结束
        dragEndEleWrap(event, ele) {
            console.info('结束拖拽Wrap', ele)
            this.dragContext = {};
            this.clearDropStyle();
        },
        // 清除拖放样式
        clearDropStyle() {
            $('.form-config-ele-wrap').removeClass('form-config-ele-wrap-drop-center');
            $('.form-config-ele-wrap-drop-center').removeClass('form-config-ele-wrap-drop-center');
            $('.form-config-ele-wrap').removeClass('form-config-ele-wrap-drop-before');
            $('.form-config-ele-wrap-drop-before').removeClass('form-config-ele-wrap-drop-before');
            $('.form-config-ele-wrap').removeClass('form-config-ele-wrap-drop-after');
            $('.form-config-ele-wrap-drop-after').removeClass('form-config-ele-wrap-drop-after');
        },
        // 组件包装器在目标对象上拖动, ele: 目标对象
        dragOverEleWrap(event, ele) {
            if (!ele || !this.dragContext.dragEleOption) {
                console.info('[dragOverEleWrap] 拖拽和拖放的存在无效对象,pass');
                return;
            }
            if (!event.currentTarget) {
                console.info('[dragOverEleWrap] event.currentTarget无效,pass', event, ele);
                return;
            }

            if (this.eleIncludeAnotherEle(ele, this.dragContext.dragEleOption)) {
                // console.info('[dragOverEleWrap] 拖拖拽和拖放的是同一个对象或者包含拖放对象,pass', this.dragContext.dragEleOption, ele);
                return;
            }
            // 鼠标在目标对象的位置
            var position = jo.getPositionDetail(event, 'form-config-ele-wrap');
            var positionDesc = position.atCenterOf16 ? 'inner' : (position.atLeftOf16 || position.atUpOf16 ? 'before' : 'after');

            if (this.dragContext.overEle && this.dragContext.overEle.id === ele.id && this.dragContext.overPosition === positionDesc) {
                // console.debug('[dragOverEleWrap] 当前目标对象与上次一致且位置一致,pass', event, ele);
                return;
            }

            // 清空样式
            this.clearDropStyle();
            if (positionDesc === 'inner') {
                // 放到目标对象的子节点
                $(event.currentTarget).addClass('form-config-ele-wrap-drop-center');
            } else if (positionDesc === 'before') {
                // 左/上则放到目标前面
                $(event.currentTarget).addClass('form-config-ele-wrap-drop-before');
            } else {
                // 右/下放到目标后面
                // 定位失败默认放到目标后面
                $(event.currentTarget).addClass('form-config-ele-wrap-drop-after');
                // console.info('---', event, event.currentTarget, event.target, position.eventTarget);
            }

            this.dragContext.overEle = ele;
            this.dragContext.overPosition = positionDesc;
            // console.info('[dragOverEleWrap] 记录当前悬浮于目标对象:', ele);
        },
        // 组件包装器拖放, ele:拖放对象
        dropEleWrap(event, ele) {
            // console.info('在表单容器拖放', event, this.dragContext);
            if (!ele || !this.dragContext.dragEleOption) {
                console.info('拖拖拽和拖放的存在无效对象,pass');
                return;
            }
            if (this.eleIncludeAnotherEle(ele, this.dragContext.dragEleOption)) {
                console.info('拖拖拽和拖放的是同一个对象或者包含拖放对象,pass', this.dragContext.dragEleOption, ele);
                return;
            }
            var dragEle;
            if (this.dragContext.dragType === this.formConfigDragType.option.code) {
                dragEle = this.convertOptionToEle(this.dragContext.dragEleOption);
            } else if (this.dragContext.dragType === this.formConfigDragType.inner.code) {
                dragEle = this.dragContext.dragEleOption;
            } else {
                console.info('元素wrap不支持此拖拽类型', this.dragContext);
                return;
            }

            // 放到目标对象的对应位置
            var position = jo.getPositionDetail(event, 'form-config-ele-wrap');
            var positionDesc = position.atCenterOf16 ? 'inner' : (position.atLeftOf16 || position.atUpOf16 ? 'before' : 'after');
            // 禁止拖放规则, 哪些能放进去, 哪些不能放进去
            let dragDropRule = {
                'jo-el-table': {
                    // 哪些能放进去, 哪些不能放进去
                    inRule: lcpUtil.AllowAndRefuse().allow({type: 'table-column'}),
                    // 能和那些元素做邻居
                    neighborRule: {},
                    // 元素能放到哪里, 不能放到哪里
                    putRule: lcpUtil.AllowAndRefuse()
                },
                'table-column': {
                    putRule: lcpUtil.AllowAndRefuse().allow({type: 'jo-el-table', msg: '表格列仅支持拖放在表格中!'})
                }
            };
            // 拖拽元素的放置规则
            var dragRule = dragDropRule[dragEle.type];
            if (dragRule && dragRule.putRule && dragRule.putRule.notMatch(ele)) {
                jo.showErrorMsg('组件[' + dragEle.type + ']不支持放入组件[' + ele.type + ']内');
                return;
            }
            // 拖放元素的规则
            var dropRule = dragDropRule[ele.type];
            if (dropRule && dropRule.inRule && dropRule.inRule.notMatch(dragEle)) {
                jo.showErrorMsg('组件[' + dragEle.type + ']不支持放入组件[' + ele.type + ']内');
                return;
            }

            // 拖拽元素放下时的限制条件
            if (dragEle.type === 'table-column') {
                if (positionDesc === 'inner' && ele.type !== 'jo-el-table') {
                    jo.showErrorMsg('表格列仅支持拖放在表格中!');
                    return;
                }
                if (positionDesc !== 'inner' && ele.type !== 'table-column') {
                    jo.showErrorMsg('表格列仅支持拖放在表格列前后!');
                    return;
                }
            }
            // 移动元素
            this.moveComponentElePosition(dragEle, ele, positionDesc);

        },
        // 移动组件位置, moveEle:要移动的元素, targetEle:目标位置元素, position:移动后相对目标元素的位置（before、after、inner）
        moveComponentElePosition(moveEle, targetEle, position) {
            if (!moveEle || !targetEle || !position) {
                console.warn('[移动组件位置] 存在无效参数:', arguments);
                return;
            }
            if (position !== 'before' && position !== 'after' && position !== 'inner') {
                console.warn('[移动组件位置] 位置参数非法:', position);
                return;
            }
            // 先从原来的位置删除
            this.removeEleWrap(moveEle);
            console.info('[移动组件位置] 已从老位置删除,准备插入到新位置--');
            if (position === 'inner') {
                if (!targetEle.childrenList) {
                    targetEle.childrenList = [];
                }
                targetEle.childrenList.push(moveEle);
            } else {
                // 找到目标对象所处的集合, 然后在合适的地方插入
                var list = this.findEleOriginList(targetEle, this.pageConfig.elementList) || this.pageConfig.elementList;
                // 目标对象在集合的位置
                var idx = this.findEleIdx(targetEle, list);
                if (idx < 0) {
                    // 目标位置匹配失败则默认放到最后, 正常情况到不了这里
                    list.push(moveEle);
                    console.warn('[移动组件位置] 目标位置匹配失败,默认放到集合最后');
                } else {
                    if (position === 'before') {
                        list.splice(idx, 0, moveEle);
                    } else {//position === 'after'
                        list.splice(idx + 1, 0, moveEle);
                    }
                }
            }
            // dom树变更
            this.domTreeOperate++;
        },
        // 拖拽在目标对象上移动, 啥也不干, 没有这个事件则拖放事件不生效
        dragOverNothing(event) {
        },
        // dragOverEleOption(event, ele) {
        //     console.info('拖拽移动', ele)
        // },
        // dropEleOption(event, ele) {
        //     console.info('拖放', ele)
        // },


        // 判断元素是否在另一个元素中, 也就是是否相同或者是子节点
        eleIncludeAnotherEle(ele, another) {
            if (ele.id === another.id) {
                return true;
            }
            if (another.childrenList) {
                for (let i = 0; i < another.childrenList.length; i++) {
                    var item = another.childrenList[i];
                    if (this.eleIncludeAnotherEle(ele, item)) {
                        return true;
                    }
                }
            }
            return false;
        },
        // 查找元素在哪个集合中
        findEleOriginList(eleWrap, list) {
            if (list && list.length > 0) {
                // 判断在不在list
                for (let i = 0; i < list.length; i++) {
                    var item = list[i];
                    if (item.id == eleWrap.id) {
                        return list;
                    }
                }
                // 判断在不在孩子节点
                for (let i = 0; i < list.length; i++) {
                    var item = list[i];
                    if (item.childrenList) {
                        var list2 = this.findEleOriginList(eleWrap, item.childrenList);
                        if (list2) {
                            return list2;
                        }
                    }
                }
            }
            return null;
        },
        // 从表单配置的元素列表中删除指定元素
        removeEleWrap(eleWrap) {
            this.removeEleWrapFromList(eleWrap, this.pageConfig.elementList);
        },
        // 删除元素从一个列表中
        removeEleWrapFromList(eleWrap, eleList) {
            if (eleList) {
                var _this = this;
                var delIdx = this.findEleIdx(eleWrap, eleList);
                if (delIdx >= 0) {
                    eleList.splice(delIdx, 1);
                    console.info('[删除元素老位置] 位置:', delIdx);
                } else {
                    // 再从子节点中删除
                    jo.forEach(eleList, function (item, i) {
                        if (item.childrenList) {
                            _this.removeEleWrapFromList(eleWrap, item.childrenList);
                        }
                    });
                }
            }
        },
        // 获取元素在集合的位置
        findEleIdx(eleWrap, eleList) {
            var idx = -1;
            if (eleList) {
                jo.forEach(eleList, function (item, i) {
                    if (item.id == eleWrap.id) {
                        if (idx >= 0) {
                            console.error('[获取元素在集合的位置] 重复匹配到元素,上次匹配索引+本次索引+集合输出:', idx, i, eleList);
                        }
                        idx = i;
                    }
                });
            }
            return idx;
        },
    }
};




