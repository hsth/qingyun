// 组件选项, 左侧可选的组件
joEl.register('form-config-ele-option', {
    props: {
        ele: {
            type: Object,
            default: '',
        },
    },
    data: function () {
        return {};
    },
    template: `
        <div class="form-config-ele-option"
            draggable="true"
            @dragstart.stop.self="dragStartEle($event, ele)"
            @dragend.stop.self="dragEndEle($event, ele)"
        >
            <div class="form-config-icon">
                <i class="fa" :class="ele.icon"></i>
            </div>
            <div class="form-config-ele-name">{{ele.optionShowName}}</div>
        </div>
    `,
    methods: {
        dragStartEle(event, ele) {
            this.$root.dragStartEleOption(event, ele);
        },
        dragEndEle(event, ele) {
            this.$root.dragEndEleOption(event, ele);
        },
    },
    mounted() {

    }
});
// 画布上的组件包装器, 对组件做的一层包装, 提供一些共性的操作(比如:支持拖拽,公共样式)
joEl.register('form-config-ele-wrap', {
    props: {
        ele: {
            type: Object,
            default: {}
        },
    },
    data: function () {
        return {};
    },
    computed: {
        wrapActiveEleId() {
            return this.$root.formConfigActiveEleId;
        },
        // class
        classCompute() {
            var clz = {
                'form-config-ele-wrap-active': this.ele.id == this.wrapActiveEleId
            };
            jo.forEach(this.ele.classList, function (c) {
                if (c) {
                    clz[c] = true;
                }
            });
            return clz;
        },
        // 样式style
        styleCompute() {
            var style = {};
            jo.forEach(this.ele.styleList, function (item) {
                if (item.name && item.value) {
                    style[item.name] = item.value;
                }
            });
            return style;
        },
    },
    template: `
        <form-config-ele-instance class="form-config-ele-wrap"
            :ele="ele"
            :class="classCompute"
            :style="styleCompute"
            @click.stop.prevent="eleWrapChecked(ele, $event)"
            @mouseover.stop="eleWrapMouseOver($event, ele)"
            @mouseout.stop="eleWrapMouseOut($event, ele)"
            draggable="true"
            @dragstart.stop.self="dragStartEle($event, ele)"
            @dragend.stop.self="dragEndEle($event, ele)"
            @dragover.stop.prevent="dragOverEle($event, ele)"
            @drop.stop="dropEle($event, ele)"
        >
        </form-config-ele-instance>
    `,
    methods: {
        // 鼠标悬停
        eleWrapMouseOver(event, ele) {
            if (!$(event.currentTarget).hasClass('form-config-ele-wrap-hover')) {
                $(event.currentTarget).addClass('form-config-ele-wrap-hover');
            }
        },
        // 鼠标移出
        eleWrapMouseOut(event, ele) {
            if ($(event.currentTarget).hasClass('form-config-ele-wrap-hover')) {
                $(event.currentTarget).removeClass('form-config-ele-wrap-hover');
            }
        },
        // 页面配置中组件选中
        eleWrapChecked(ele, e) {
            console.info('组件选中:', ele);
            if (this.$root.formConfigActiveEle && this.$root.formConfigActiveEle.id === ele.id) {
                this.$root.formConfigActiveEle = null;
            } else {
                this.$root.formConfigActiveEle = ele;
            }
        },
        // 组件拖拽开始
        dragStartEle(event, ele) {
            this.$root.dragStartEleWrap(event, ele);
        },
        // 组件拖拽结束
        dragEndEle(event, ele) {
            this.$root.dragEndEleWrap(event, ele);
        },
        // 组件拖拽在目标对象拖动
        dragOverEle(event, ele) {
            this.$root.dragOverEleWrap(event, ele);
        },
        // 拖放组件
        dropEle(event, ele) {
            this.$root.dropEleWrap(event, ele);
        },
    }
});
// 画布中的组件实例, 每个被拖拽到面板上的组件都会转换为一个组件实例对象, 内含真正的组件html
joEl.register('form-config-ele-instance', {
    // inheritAttrs: false,
    props: {
        ele: {
            type: Object,
            default: {}
        },
        bindVal: null
    },
    data: function () {
        return {
            // 变量类型的属性预览占位对象
            varAttrPlaceholder: {}
        };
    },
    computed: {
        // 当前选中的组件id
        wrapActiveEleId() {
            return this.$root.formConfigActiveEleId;
        },
        // 有子组件
        hasChildren() {
            return this.ele.childrenList && this.ele.childrenList.length > 0;
        },
        // 当前实例所属组件的元信息
        componentMeta() {
            var comp;
            jo.forEach(this.$root.componentOptionList, item => {
                if (item.type === this.ele.type) {
                    comp = item;
                }
            });
            // if (this.componentMeta)
            return comp || {};
        },
        // 当前实例所属组件的属性元配置
        componentAttrMetaMap() {
            var map = {};
            if (this.componentMeta) {
                return jo.array2Object(this.componentMeta.attrMetaList, item => {
                    return item.attrName;
                });
            }
            return map;
        },
        // 组件属性集合, 用来批量绑定到画布组件(编辑态)上
        attrMap() {
            var map = {};
            for (let key in this.$attrs) {
                map[key] = this.$attrs[key];
            }
            jo.forEach(this.ele.attrList, item => {
                var val = this.convertActualAttrValue(item);
                if (item.name && (jo.isValid(val) || val === false)) {
                    map[item.name] = val;
                }
            });
            console.log('[attrMap] 当前组件[%s],属性:', this.ele.id, map);
            return map;
        },
        // 去掉公共拖放属性后的组件属性集
        attrMapNotCommon() {
            var map = {
                'class': {},
                style: {},
            };
            // class属性
            jo.forEach(this.ele.classList, function (c) {
                if (c) {
                    map.class[c] = true;
                }
            });
            // style属性
            jo.forEach(this.ele.styleList, function (item) {
                if (item.name && item.value) {
                    map.style[item.name] = item.value;
                }
            });
            // 其他组件属性
            jo.forEach(this.ele.attrList, item => {
                var val = this.convertActualAttrValue(item);
                if (item.name && (jo.isValid(val) || val === false)) {
                    map[item.name] = val;
                }
            });
            console.log('[attrMapNotCommon] 当前组件[%s],属性:', this.ele.id, map);
            return map;
        },
        // 样式对象
        styleMap() {
            var map = {};
            jo.forEach(this.ele.styleList, item => {
                if (item.name && jo.isValid(item.value)) {
                    map[item.name] = item.value;
                }
            });
            return map;
        },
    },
    methods: {
        // 转换为实际的属性值
        convertActualAttrValue(attrConfig) {
            // 属性对应的元配置
            var attrMeta = this.componentAttrMetaMap[attrConfig.name];
            // 匹配到了元配置
            if (jo.isValid(attrConfig.value) && attrMeta && attrMeta.attrDataType) {
                return lcpUtil.parseValueByActualType(attrConfig.value, attrMeta.attrDataType);
            } else {
                return lcpUtil.parseValueByActualType(attrConfig.value, attrConfig.valueType);
            }
        },
        // 组件选中
        eleInstanceChecked() {
            let ele = this.ele;
            if (this.$root.formConfigActiveEle && this.$root.formConfigActiveEle.id === ele.id) {
                this.$root.formConfigActiveEle = null;
            } else {
                this.$root.formConfigActiveEle = ele;
            }
        }

    },
    template: `
        <template v-if="ele.type=='row'">
            <el-row v-bind="attrMap"><!--:gutter="attrMap['gutter']"-->
                <template v-if="hasChildren">
                    <form-config-ele-wrap v-for="child in ele.childrenList" :ele="child"></form-config-ele-wrap>
                </template>
                <lcp-design-child-place v-else=""></lcp-design-child-place>
            </el-row>
        </template>
        <template v-else-if="ele.type=='col'">
            <el-col v-bind="attrMap">
                 <template v-if="hasChildren">
                    <form-config-ele-wrap v-for="child in ele.childrenList" :ele="child"></form-config-ele-wrap>
                </template>
                <lcp-design-child-place v-else=""></lcp-design-child-place>
            </el-col>
        </template>
        <template v-else-if="ele.type=='div'">
            <div v-bind="attrMap">
                <template v-if="hasChildren">
                    <form-config-ele-wrap v-for="child in ele.childrenList" :ele="child"></form-config-ele-wrap>
                </template>
                <lcp-design-child-place v-else=""></lcp-design-child-place>
            </div>
        </template>
        <template v-else-if="ele.type=='divider'">
            <el-divider v-bind="attrMap" class="ele-wrap-no-default-border" ></el-divider>
        </template>
        
        <template v-else-if="ele.type=='form'">
            <el-form v-bind="attrMap">
                <template v-if="hasChildren">
                    <form-config-ele-wrap v-for="child in ele.childrenList" :ele="child"></form-config-ele-wrap>
                </template>
                <lcp-design-child-place v-else=""></lcp-design-child-place>
            </el-form>
        </template>
        <template v-else-if="ele.type=='form_item'">
            <el-form-item v-bind="attrMap">
                <template v-if="hasChildren">
                    <form-config-ele-wrap v-for="child in ele.childrenList" :ele="child"></form-config-ele-wrap>
                </template>
            </el-form-item>
        </template>
        
        
        
        <!-- ===== 表单元素 start ===== -->
        <!-- ===== 表单元素 start ===== -->
        <!-- ===== 表单元素 start ===== -->
        <template v-else-if="ele.type=='button'">
            <el-button v-bind="attrMap"><span v-if="attrMap.qyIcon"><i class="fa" :class="attrMap.qyIcon"></i>&nbsp;</span>{{attrMap.showText}}</el-button>
        </template>
        <template v-else-if="ele.type=='input'">
<!--            <div style="display: inline-block;">-->
                <el-input v-bind="attrMap"></el-input>
<!--            </div>-->
        </template>
        <template v-else-if="ele.type=='textarea'">
            <el-input type="textarea" v-bind="attrMap"></el-input>
        </template>
        <template v-else-if="ele.type=='number'">
            <el-input-number v-bind="attrMap" ></el-input-number>
        </template>
        <template v-else-if="ele.type=='select'">
            <el-select placeholder="Select" v-bind="attrMap"></el-select>
        </template>
        <template v-else-if="ele.type=='radio'">
            <el-radio v-bind="attrMap">{{attrMap.showText}}</el-radio>
        </template>
        <template v-else-if="ele.type=='radio_group'">
            <el-radio-group v-bind="attrMap">
                <template v-if="hasChildren">
<!--                    <div style="width: 10px;height: 10px;"></div>-->
                    <form-config-ele-wrap v-for="child in ele.childrenList" :ele="child"></form-config-ele-wrap>
<!--                    <div style="width: 10px;height: 10px;"></div>-->
                </template>
                <lcp-design-child-place v-else=""><div style="width: 20px;height: 20px;"></div></lcp-design-child-place>
            </el-radio-group>
        </template>
        <template v-else-if="ele.type=='checkbox'">
            <el-checkbox v-bind="attrMap" >{{attrMap.showText}}</el-checkbox>
        </template>
        <template v-else-if="ele.type=='checkbox_group'">
            <el-checkbox-group v-bind="attrMap">
                <template v-if="hasChildren">
<!--                    <span style="display: inline-block;width: 10px;">&nbsp;&nbsp;&nbsp;&nbsp;</span>-->
                    <form-config-ele-wrap v-for="child in ele.childrenList" :ele="child"></form-config-ele-wrap>
<!--                    <span style="display: inline-block;width: 10px;">&nbsp;&nbsp;&nbsp;&nbsp;</span>-->
                </template>
                <lcp-design-child-place v-else=""><div style="width: 20px;height: 20px;"></div></lcp-design-child-place>
            </el-checkbox-group>
        </template>
        <template v-else-if="ele.type=='switch'">
            <el-switch v-bind="attrMap"></el-switch>
        </template>
        <template v-else-if="ele.type=='cascade'">
            <div style="display: inline-block;">
                <el-cascader v-bind="attrMap"></el-cascader>
            </div>
        </template>
        <template v-else-if="ele.type=='date'">
            <el-date-picker type="date" v-bind="attrMap"></el-date-picker>
        </template>
        <template v-else-if="ele.type=='time'">
            <el-date-picker type="datetime" v-bind="attrMap"></el-date-picker>
        </template>
        <template v-else-if="ele.type=='color'">
            <el-color-picker v-bind="attrMap"></el-color-picker>
        </template>
        <template v-else-if="ele.type=='rate'">
            <el-rate v-bind="attrMap"></el-rate>
        </template>
        <template v-else-if="ele.type=='slider'">
            <el-slider v-bind="attrMap"></el-slider>
        </template>
        <template v-else-if="ele.type=='upload'">
             <jo-el-form-upload v-bind="attrMap"></jo-el-form-upload>
        </template>
        <!-- ===== 表单元素 end ===== -->
        <!-- ===== 表单元素 end ===== -->
        <!-- ===== 表单元素 end ===== -->
        
        
        <!-- ===== 数据组件 start ===== -->
        <!-- ===== 数据组件 start ===== -->
        <!-- ===== 数据组件 start ===== -->
        <template v-else-if="ele.type=='text'">
            <span>{{attrMap.value}}</span>
        </template>
        <template v-else-if="ele.type=='img'">
            <img v-bind="attrMap"/>
        </template>
        <template v-else-if="ele.type=='jo-el-table'">
            <div>
                <jo-el-table-v2 v-bind="attrMapNotCommon" :table-data="[]">
                    
                    <template #table="">
                        <template v-if="hasChildren">
                            <div style="display: flex;border: solid 1px rgb(235, 238, 245);">
                                <form-config-ele-wrap v-for="child in ele.childrenList" :ele="child"></form-config-ele-wrap>
                            </div>
                        </template>
                        <div v-else="" style="display: flex;border: solid 1px rgb(235, 238, 245);align-items: center;justify-content: center;padding: 10px;">
                            待添加表格列
                        </div>
                    </template>
                </jo-el-table-v2>
            </div>
        </template>
        <template v-else-if="ele.type=='table-column'">
            <div v-bind="attrMap" style="flex: 1;margin-right: 1px;padding: 2px;">
                <div style="background-color: #FAFAFA;border-bottom: solid 1px rgb(235, 238, 245);padding: 8px 12px;">{{attrMap.label}}</div>
                <div style="padding: 8px 12px;">
                    <template v-if="hasChildren">
                        <form-config-ele-wrap v-for="child in ele.childrenList" :ele="child"></form-config-ele-wrap>
                    </template>
                    <lcp-design-child-place v-else="">{{attrMap.label}}</lcp-design-child-place>
                </div>
            </div>
        </template>
        <!-- ===== 数据组件 end ===== -->
        <!-- ===== 数据组件 end ===== -->
        <!-- ===== 数据组件 end ===== -->
        
        
        <template v-else="">
            <div>{{ele.type}}</div>
        </template>
    `,
    mounted() {

    }
});
// 子元素占位符
joEl.register('lcp-design-child-place', {
    props: {},
    data: function () {
        return {};
    },
    computed: {},
    template: `
        <div>
            <slot>
                &nbsp;&nbsp;&nbsp;&nbsp;
            </slot>
        </div>
    `,
    methods: {},
    mounted() {

    }
});