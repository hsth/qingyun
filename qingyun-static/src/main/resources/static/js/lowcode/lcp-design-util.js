/*
 * 在线设计工具方法汇总
 */
window.lcpUtil = {};
(function (lcpUtil) {
    /**
     * 解析组件属性配置值的类型(数字?字符串?布尔?或者是vue变量名?)
     *
     * 在配置组件属性值的时候, 为了在生成代码或渲染的时候知道该用普通的赋值(a=b), 还是该用vue变量(:a=b,vue属性值是数字或布尔时也需要带冒号)
     * 于是在配置的时候需要用户手动选择配置的这个值是什么类型(数字?字符串?布尔?或者是vue变量?)
     * 通过此方法可以在不知道选择的类型时(元信息配的一些默认值是没有设置类型的), 用该方法对值进行解析, 得到该值是哪种类型
     * @param allowDataType 组件允许的值类型, 枚举参考 ELcpComponentAttrDataTypeEnum
     * @param val 组件某属性配置的值
     * @returns {string} 配置值的类型
     */
    lcpUtil.parseAttrValueType = function (allowDataType, val) {
        var types = allowDataType ? allowDataType.split(',') : ['string', 'number', 'boolean', 'object', 'array', 'time', 'function'];
        // 数字
        if (jo.arrayContains(types, 'number') && jo.isNumber(val)) {
            return 'number';
        }
        // 布尔
        if (jo.arrayContains(types, 'boolean') && (typeof val == 'boolean' || val === 'true' || val === 'false')) {
            return 'boolean';
        }
        // 变量
        if (typeof val == 'string' && val.startsWith(':')) {
            return 'var'
        }
        return 'string';
    };
    /**
     * 值类型转换
     * 给组件配置的值数据库中通常存的是字符串, 通过该方法将字符串值转为合适的数据类型
     * @param val 值
     * @param allowDataType 要转换的类型, 多种类型用逗号拼接
     * @returns {*|number|boolean} 对应类型的值, 若传入多个类型, 返回第一个转成功的类型的值
     */
    lcpUtil.parseValueByActualType = function (val, allowDataType) {
        if (allowDataType) {
            var arr = allowDataType.split(",");
            for (let i = 0; i < arr.length; i++) {
                var type = arr[i];
                if (type === 'number' && jo.isNumber(val)) {
                    return Number(val);
                } else if (type === 'boolean') {
                    if (val === 'true' || val === '1') {
                        return true;
                    } else if (val === 'false' || val === '0') {
                        return false;
                    } else {
                        console.info('[类型转换] 不支持的boolean类型:', val);
                    }
                } else if (type === 'string') {
                    return val;
                } else if (type === 'object' && jo.isObjJsonStr(val)) {
                    return JSON.parse(val);
                } else if (type === 'array' && jo.isArrayStr(val)) {
                    return JSON.parse(val);
                } else {
                    console.debug('[类型转换] 不适合转换的类型: %s, 传入值: %s, 传入类型: %s', type, val, allowDataType)
                }
            }
        }
        console.info('[类型转换] 转换未成功,原类型返回,传入值: %s, 传入类型: %s', val, allowDataType);
        return val;
    };
    // 在线设计组件集
    lcpUtil.COMPONENT_MAP = {};
    lcpUtil.component = lcpUtil.install = function (app) {
        // 注册组件
        for (var k in lcpUtil.COMPONENT_MAP) {
            app.component(k, lcpUtil.COMPONENT_MAP[k]);
        }
    };
    lcpUtil.register = function (componentName, componentConfig) {
        lcpUtil.COMPONENT_MAP[componentName] = componentConfig;
    };
    // 拖放规则, 支持哪些元素不支持哪些元素
    lcpUtil.AllowAndRefuse = function () {
        return {
            refuseAll: false,//拒绝所有元素
            allowAll: false,//允许所有元素
            refuseEle: [],//不允许那些元素{type: 'jo-el-table', msg: '表格列仅支持拖放在表格中!'}
            allowEle: [],//元素那些元素{type: 'jo-el-table', msg: '表格列仅支持拖放在表格中!'}
            match (ele) {
                if (this.refuseAll) {
                    return false;
                }
                if (this.allowAll) {
                    return true;
                }
                if (jo.arrayIsNotEmpty(this.refuseEle)) {
                    for (let i = 0; i < this.refuseEle.length; i++) {
                        let item = this.refuseEle[i];
                        if (item.type === ele.type) {
                            return false;
                        }
                    }
                    return true;
                } else if (jo.arrayIsNotEmpty(this.allowEle)) {
                    for (let i = 0; i < this.allowEle.length; i++) {
                        let item = this.allowEle[i];
                        if (item.type === ele.type) {
                            return true;
                        }
                    }
                    return false;
                }
                return true;
            },
            notMatch (ele) {
                return !this.match(ele);
            },
            // 允许某个元素
            allow (eles) {
                if (jo.isArray(eles)) {
                    jo.forEach(eles, item => {
                        this.allowEle.push(item);
                    });
                } else if (eles) {
                    this.allowEle.push(eles);
                }
                return this;
            },
            // 拒绝某个元素
            refuse (eles) {
                jo.forEach(eles, item => {
                    this.refuseEle.push(item);
                });
                return this;
            }
        };
    }
})(window.lcpUtil);
