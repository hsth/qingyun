joEl.register('lcp-generate-form', {
    emits: [],
    props: {
        formData: {
            type: Object,
            default: function () {
                return {
                    extInfo: {}
                    , fieldList: []
                };
            }
        }
    },
    data() {
        return {
            // 当前选中字段
            curCol: null,
            // 控件类型
            controlTypeList: [],
            // 控件表单校验规则
            controlCheckList: [],
            // 控件数据源快捷方式
            controlDataSourceShortcut: [
                {name: '组织机构树', dataUrl: '/ums/umsOrg/getTree', keyField: 'id', valueField: 'name'},
                {name: '单位树', dataUrl: '/ums/umsOrg/getCompanyTree', keyField: 'id', valueField: 'name'},
                {name: '资源树', dataUrl: '/ums/umsResource/getTree', keyField: 'id', valueField: 'name'},
            ],
            // 字典选项
            dictCodeOptions: [],
        };
    },
    computed: {
        // 字段列集合
        columnList() {
            if (this.formData) {
                return this.formData.fieldList;
            }
            return [];
        },
        allWidthIs24() {
            var flag = true;
            jo.forEach(this.columnList, function (item) {
                if (item.formColumnGridWidth != 24) {
                    flag = false;
                }
            });
            return flag;
        },
        allWidthIs12() {
            var flag = true;
            jo.forEach(this.columnList, function (item) {
                if (item.formColumnGridWidth != 12) {
                    flag = false;
                }
            });
            return flag;
        },
        allWidthIs8() {
            var flag = true;
            jo.forEach(this.columnList, function (item) {
                if (item.formColumnGridWidth != 8) {
                    flag = false;
                }
            });
            return flag;
        },
        // 校验规则map格式
        controlCheckMap() {
            return jo.array2Object(this.controlCheckList, function (item) {
                return item.key;
            }, function (item) {
                return item.value;
            });
        },
        // 表单配置
        columnListForForm() {
            var arr = [];
            jo.forEach(this.columnList, function (item, i) {
                arr.push(item);
            });
            // 排序
            jo.sort(arr, function (a, b) {
                var one = jo.getDefVal(a.formColumnOrder, 0);
                var two = jo.getDefVal(b.formColumnOrder, 0);
                return one < two;
            });
            return arr;
        },
        // 拖拽版表单配置
        columnListForFormDrag() {
            var arr = [];
            jo.forEach(this.columnList, function (item, i) {
                arr.push(item);
                // 处理虚拟字段, 用于交互使用, 后端不存储
                if (item.formControlCheckRule && item.formControlCheckRule.checkRuleList) {
                    if (!item.v_checkRuleTypeList || item.v_checkRuleTypeList.length === 0) {
                        item.v_checkRuleTypeList = [];
                        // 只有第一次才push
                        jo.forEach(item.formControlCheckRule.checkRuleList, function (rule) {
                            item.v_checkRuleTypeList.push(rule.checkType);
                        });
                    }
                }
            });
            // 排序
            jo.sort(arr, function (a, b) {
                var one = jo.getDefVal(a.formColumnOrder, 0);
                var two = jo.getDefVal(b.formColumnOrder, 0);
                return one < two;
            });
            return arr;
        },
        // 判断是否显示控件数据源
        showOptionDataSource() {
            var controlType = this.curCol.formControlType;
            return controlType === 'select' || controlType === 'treeSelect'
                || controlType === 'radio'
                || controlType === 'checkbox';
        },
    },
    template: `
    <el-container style="height:100%;padding: 0px;border: 1px solid #eee">
        <el-aside>
            <el-scrollbar>
                <div style="padding: 0px 15px 15px 15px;">
                    <div style="margin-bottom: 15px;">
                        <el-divider border-style="dashed">快捷方式</el-divider>
                        <el-button type="primary" @click="batchSetFormColumnWidth(24)" :plain="!allWidthIs24" size="small">一行一列</el-button>
                        <el-button type="success" @click="batchSetFormColumnWidth(12)" :plain="!allWidthIs12" size="small">一行二列</el-button>
                        <el-button type="warning" @click="batchSetFormColumnWidth(8)" :plain="!allWidthIs8" size="small">一行三列</el-button>
                    </div>
                    <template v-for="item in columnListForForm">
                        <div class="form-config-field-option" v-if="item.formColumnShow == 0">
                            <el-tag effect="plain">{{item.fieldJavaName}} : {{item.formControlTitle}}</el-tag>
                            <el-switch v-model="item.formColumnShow" :active-value="1" :inactive-value="0" active-text="√" inactive-text="×" inline-prompt ></el-switch>
                        </div>
                    </template>
                </div>
            </el-scrollbar>
        </el-aside>
        <el-main style="border-left: 1px solid #eee;border-right: 1px solid #eee;">
            <el-scrollbar wrap-class="previewBoxScroll">
                <el-form :label-width="formData.formLabelWidth" :label-position="formData.formLabelPosition">
                    <div style="padding: 7.5px;">
                        <el-row :gutter="10" class="jo-el-row-vertical-gutter">
                            <template v-for="item in columnListForFormDrag">
                                <el-col :span="item.formColumnGridWidth" v-if="item.formColumnShow == 1">
                                    <div draggable="true"
                                         @dragstart="moveColumnStart($event)"
                                         @drag="moveColumnIng($event)"
                                         @dragend="moveColumnEnd($event)"
                                         class="preview-col-item"
                                         :class="{'preview-col-item-active' : curCol && curCol.fieldDbName == item.fieldDbName}"
                                         @click="colDetail(item)"
                                         :dataColId="item.fieldDbName">
                                        <div style="padding: 10px;padding-bottom: 0px;">
                                            <el-form-item :label="item.formControlTitle">
                                                <template v-if="item.formControlType == 'input'">
                                                    <el-input :placeholder="item.formControlPlaceholder"></el-input>
                                                </template>
                                                <template v-else-if="item.formControlType == 'number'">
                                                    <el-input-number :placeholder="item.formControlPlaceholder"></el-input-number>
                                                </template>
                                                <template v-else-if="item.formControlType == 'date'">
                                                    <el-date-picker type="date" :placeholder="item.formControlPlaceholder"></el-date-picker>
                                                </template>
                                                <template v-else-if="item.formControlType == 'time'">
                                                    <el-date-picker type="datetime" :placeholder="item.formControlPlaceholder"></el-date-picker>
                                                </template>
                                                <template v-else-if="item.formControlType == 'select'">
                                                    <el-select :placeholder="item.formControlPlaceholder">
                                                        <el-option value="" label=""></el-option>
                                                    </el-select>
                                                </template>
                                                <template v-else-if="item.formControlType == 'treeSelect'">
                                                    <el-select :placeholder="item.formControlPlaceholder">
                                                        <el-option value="" label=""></el-option>
                                                    </el-select>
                                                </template>
                                                <template v-else-if="item.formControlType == 'iconSelect'">
                                                    <el-select :placeholder="item.formControlPlaceholder">
                                                        <el-option value="" label=""></el-option>
                                                    </el-select>
                                                </template>
                                                <template v-else-if="item.formControlType == 'textarea'">
                                                    <el-input :rows="2" type="textarea" :placeholder="item.formControlPlaceholder"></el-input>
                                                </template>
                                                <template v-else-if="item.formControlType == 'switch'">
                                                    <el-switch :model-value="true"></el-switch>
                                                </template>
                                                <template v-else-if="item.formControlType == 'radio'">
                                                    <el-radio-group>
                                                        <el-radio :label="3">Option A</el-radio>
                                                        <el-radio :label="6">Option B</el-radio>
                                                    </el-radio-group>
                                                </template>
                                                <template v-else-if="item.formControlType == 'checkbox'">
                                                    <el-checkbox-group>
                                                        <el-checkbox label="Option A"></el-checkbox>
                                                        <el-checkbox label="Option B"></el-checkbox>
                                                    </el-checkbox-group>
                                                </template>
                                                <template v-else-if="item.formControlType == 'label'">
                                                    <el-tag>label</el-tag>
                                                </template>
                                                <template v-else-if="item.formControlType == 'upload'">
                                                    <el-button type="success" :plain="true">上传</el-button>
                                                </template>
                                                <template v-else-if="item.formControlType == 'editor_md'">
                                                    ###Markdown富文本编辑器###
                                                </template>
                                                <template v-else="">
                                                    <el-input :placeholder="item.formControlPlaceholder"></el-input>
                                                </template>
                                            </el-form-item>
                                        </div>
                                        <div class="preview-col-oper">
                                            <el-form :inline="true" label-position="left" size="small">
                                                <el-form-item label="">
                                                    <el-switch v-model="item.formColumnShow" :active-value="1" :inactive-value="0"></el-switch>
                                                </el-form-item>
                                                <el-form-item label="">
                                                    <el-input-number v-model="item.formColumnGridWidth" size="small" :min="1" :max="24"></el-input-number>
                                                </el-form-item>
                                                <el-form-item label="">
                                                    <el-select v-model="item.formControlType" size="small">
                                                        <el-option v-for="item in controlTypeList" :value="item.key" :label="item.value"></el-option>
                                                    </el-select>
                                                </el-form-item>
                                            </el-form>
                                        </div>
                                    </div>
                                </el-col>
                            </template>
                        </el-row>
                    </div>
                </el-form>
            </el-scrollbar>
        </el-main>
        <el-aside>
            <div style="padding: 0px 10px;height: 100%;">
                <el-tabs model-value="first" class="qy-full-tabs">
                    <el-tab-pane label="表单项配置" name="first">
                        <el-scrollbar>
                            <el-form label-width="120px" v-if="curCol" label-position="top">
                                <lcp-config-item title="属性名">&nbsp;{{curCol.fieldJavaName}}</lcp-config-item>
                                <lcp-config-item title="类型">&nbsp;{{curCol.fieldJavaType}}</lcp-config-item>
                                <lcp-config-item title="标题">
                                    <el-input v-model="curCol.formControlTitle"></el-input>
                                </lcp-config-item>
                                <lcp-config-item title="宽度" style="padding-bottom: 20px;">
                                    <div style="padding-right: 20px;">
                                        <el-slider v-model="curCol.formColumnGridWidth" :min="1"
                                                   :max="24" size="small"
                                                   :marks="{1:'1',6:'6',12:'12',18:'18',24:'24'}"></el-slider>
                                    </div>
                                </lcp-config-item>
                                <lcp-config-item title="占位提示">
                                    <el-input v-model="curCol.formControlPlaceholder" placeholder="placeholder"></el-input>
                                </lcp-config-item>
                                <lcp-config-item title="默认值">
                                    <el-input v-model="curCol.formControlDefault"></el-input>
                                </lcp-config-item>
                                <lcp-config-item title="控件类型">
                                    <el-select v-model="curCol.formControlType">
                                        <el-option v-for="item in controlTypeList" :value="item.key" :label="item.value"></el-option>
                                    </el-select>
                                </lcp-config-item>
                              

                                <!--表单项配置-->
                                <div v-if="curCol && curCol.formControlConfig" style="margin-bottom: 10px;">
                                    <div v-if="curCol.formControlType == 'treeSelect'" class="lcp-config-item-group">
                                        <lcp-config-item title="可选任意层级" width="6em" size="small">
                                            <el-switch v-model="curCol.formControlConfig.treeSupportCheckAny" size="small" :active-value="1" :inactive-value="0"></el-switch>
                                        </lcp-config-item>
                                        <lcp-config-item title="子数据字段名" width="6em" size="small">
                                            <el-input v-model="curCol.formControlConfig.childrenField" size="small" placeholder="子数据字段名,默认children"></el-input>
                                        </lcp-config-item>
                                    </div>
                                    <div v-else-if="curCol.formControlType == 'number'" class="lcp-config-item-group">
                                        <lcp-config-item title="最小值" size="small">
                                            <el-input-number v-model="curCol.formControlConfig.min" size="small" :controls="false" placeholder="最小值"></el-input-number>
                                        </lcp-config-item>
                                        <lcp-config-item title="最大值" size="small">
                                            <el-input-number v-model="curCol.formControlConfig.max" size="small" :controls="false" placeholder="最大值"></el-input-number>
                                        </lcp-config-item>
                                        <lcp-config-item title="步长" size="small">
                                            <el-input-number v-model="curCol.formControlConfig.step" size="small" :controls="false" placeholder="步长"></el-input-number>
                                        </lcp-config-item>
                                    </div>
                                    
                                    <div v-else-if="curCol.formControlType == 'textarea'" class="lcp-config-item-group">
                                        <lcp-config-item title="行数" size="small">
                                            <el-input-number v-model="curCol.formControlConfig.rows" size="small" :controls="false" placeholder="文本域行数"></el-input-number>
                                        </lcp-config-item>
                                        <lcp-config-item title="自适应" size="small">
                                            <el-switch v-model="curCol.formControlConfig.autosize" size="small" :active-value="1" :inactive-value="0"></el-switch>
                                        </lcp-config-item>
                                    </div>
                                    
                                    <div v-else-if="curCol.formControlType == 'switch'" class="lcp-config-item-group">
                                        <lcp-config-item title="选中值" size="small">
                                            <div class="flex-x">
                                                <div class="qy-flex-1 margin-right-10">
                                                    <el-input v-model="curCol.formControlConfig.yesValue" size="small" placeholder="选中值"></el-input>
                                                </div>
                                                <div class="qy-flex-1">
                                                    <el-input v-model="curCol.formControlConfig.yesText" size="small" placeholder="选中文案"></el-input>
                                                </div>
                                            </div>
                                        </lcp-config-item>
                                        <lcp-config-item title="未选中值" size="small">
                                            <div class="flex-x">
                                                <div class="qy-flex-1 margin-right-10">
                                                    <el-input v-model="curCol.formControlConfig.noValue" size="small" placeholder="未选中值"></el-input>
                                                </div>
                                                <div class="qy-flex-1">
                                                    <el-input v-model="curCol.formControlConfig.noText" size="small" placeholder="未选中文案"></el-input>
                                                </div>
                                            </div>
                                        </lcp-config-item>
                                    </div>
                                    
                                    <div v-else-if="curCol.formControlType == 'upload'" class="lcp-config-item-group">
                                        <lcp-config-item title="存储方式" size="small">
                                            <el-select v-model="curCol.formControlConfig.uploadStorage" size="small">
                                                <el-option value="fs" label="本地存储"></el-option>
                                                <el-option value="cos" label="腾讯对象存储cos"></el-option>
                                            </el-select>
                                        </lcp-config-item>
                                        <lcp-config-item title="文件夹" size="small">
                                            <el-input v-model="curCol.formControlConfig.uploadFolderId" size="small" placeholder="为空时默认:root"></el-input>
                                        </lcp-config-item>
                                        <lcp-config-item title="绑定字段" size="small">
                                            <el-select v-model="curCol.formControlConfig.uploadBindAttr" size="small">
                                                <el-option value="id" label="文件ID"></el-option>
                                                <el-option value="path" label="文件URL"></el-option>
                                            </el-select>
                                        </lcp-config-item>
                                        <lcp-config-item title="上传地址" size="small" v-if="curCol.formControlConfig.uploadStorage == 'fs'">
                                            <el-input v-model="curCol.formControlConfig.uploadUrl" size="small" placeholder="为空时默认:/fs/file/upload"></el-input>
                                        </lcp-config-item>
                                        
                                        <div v-if="curCol.formControlConfig.uploadStorage == 'cos'">
                                            <lcp-config-item title="COS签名" size="small">
                                                <el-input v-model="curCol.formControlConfig.cosStsUrl" size="small" placeholder="为空时默认:/fs/cos/sts"></el-input>
                                            </lcp-config-item>
                                            <lcp-config-item title="COS分桶" size="small">
                                                <el-input v-model="curCol.formControlConfig.cosBucket" size="small" placeholder="选择COS对象存储时必填"></el-input>
                                            </lcp-config-item>
                                            <lcp-config-item title="COS区域" size="small">
                                                <el-input v-model="curCol.formControlConfig.cosRegion" size="small" placeholder="选择COS对象存储时必填"></el-input>
                                            </lcp-config-item>
                                        </div>
                                    </div>
                                     <div v-else-if="curCol.formControlType == 'editor_md'" class="lcp-config-item-group">
                                        <lcp-config-item title="编辑器高度" width="5em" size="small">
                                            <el-input v-model="curCol.formControlConfig.editorMdHeight" size="small" placeholder="编辑器高度,例如:200"></el-input>
                                        </lcp-config-item>
                                        <lcp-config-item title="工具栏图标" width="5em" size="small">
                                            <el-select v-model="curCol.formControlConfig.editorMdToolbar" size="small">
                                                <el-option value="default" label="默认"></el-option>
                                                <el-option value="core" label="核心版"></el-option>
                                                <el-option value="full" label="全量版"></el-option>
                                                <el-option value="simple" label="常规版"></el-option>
                                                <el-option value="mini" label="极简版"></el-option>
                                            </el-select>
                                        </lcp-config-item>
                                        <lcp-config-item title="html绑定" width="5em" size="small">
                                            <el-input v-model="curCol.formControlConfig.editorHtmlBind" size="small" placeholder="编辑器html值绑定变量名,不需要保存html代码的可以为空"></el-input>
                                        </lcp-config-item>
                                    </div>
                                </div>
                                
                                <div v-if="curCol && curCol.formControlDataSource">
                                    <lcp-config-item v-if="showOptionDataSource" title="选项数据源" width="5em">
                                        <el-select v-model="curCol.formControlDataSource.dataType">
                                            <el-option value="no" label="无">无</el-option>
                                            <el-option value="static" label="静态数据">静态数据</el-option>
                                            <el-option value="url" label="URL">URL</el-option>
                                            <el-option value="dict" label="字典">字典</el-option>
                                        </el-select>
                                    </lcp-config-item>
                                    <div class="lcp-config-item-group" v-if="curCol.formControlDataSource.dataType != 'no'">
                                        <div v-if="curCol.formControlDataSource.dataType == 'static'">
                                            <el-row v-for="(op, opIdx) in curCol.formControlDataSource.optionItemList" style="margin-bottom: 5px;" :gutter="10">
                                                <el-col :span="5">
                                                    <el-input v-model="op.key" placeholder="值" size="small"></el-input>
                                                </el-col>
                                                <el-col :span="8">
                                                    <el-input v-model="op.value" placeholder="文案" size="small"></el-input>
                                                </el-col>
                                                <el-col :span="8">
                                                    <el-select v-model="op.styleType" placeholder="样式类型" size="small" clearable filterable allow-create>
                                                        <el-option value="primary">primary</el-option>
                                                        <el-option value="success">success</el-option>
                                                        <el-option value="info">info</el-option>
                                                        <el-option value="warning">warning</el-option>
                                                        <el-option value="danger">danger</el-option>
                                                    </el-select>
                                                </el-col>
                                                <el-col :span="3">
                                                    <el-button type="danger" link size="small" @click="deleteOptionItem(opIdx)">
                                                        <i class="fa fa-minus-square-o" aria-hidden="true"></i>
                                                    </el-button>
                                                </el-col>
                                            </el-row>
                                            <div style="clear: both;display: block;width: 200px;">
                                                &nbsp;<el-button type="primary" link size="small" @click="addOptionItem">添加选项</el-button>
                                            </div>
                                        </div>

                                        <div v-if="curCol.formControlDataSource.dataType == 'url'">
                                            <lcp-config-item title="URL" size="small">
                                                <el-input v-model="curCol.formControlDataSource.dataUrl" placeholder="数据Url" size="small">
                                                    <template #append>
                                                        <el-dropdown>
                                                            <span class="el-dropdown-link"><i class="fa fa-angle-down"></i></span>
                                                            <template #dropdown>
                                                                <el-dropdown-menu>
                                                                    <el-dropdown-item v-for="item in controlDataSourceShortcut" @click="controlDataSourceClick(item)">{{item.name}}</el-dropdown-item>
                                                                </el-dropdown-menu>
                                                            </template>
                                                        </el-dropdown>
                                                    </template>
                                                </el-input>
                                            </lcp-config-item>
                                            <lcp-config-item title="值字段" size="small">
                                                <el-input v-model="curCol.formControlDataSource.keyField" placeholder="值字段" size="small"></el-input>
                                            </lcp-config-item>
                                            <lcp-config-item title="文本字段" size="small">
                                                <el-input v-model="curCol.formControlDataSource.valueField" placeholder="文本字段" size="small"></el-input>
                                            </lcp-config-item>
                                        </div>
                                        <div v-if="curCol.formControlDataSource.dataType == 'dict'">
                                            <el-select v-model="curCol.formControlDataSource.dictCode" :filterable="true" :clearable="true">
                                                <el-option v-for="item in dictCodeOptions" :value="item.code" :label="item.name">{{item.name}} ({{item.code}})</el-option>
                                            </el-select>
                                            &nbsp;
                                            <el-button link type="primary" @click="loadDictCodeOptions" title="点击刷新字典选项"><i class="fa fa-refresh"></i></el-button>
                                        </div>
                                    </div>
                                </div>
                                
                                <div v-if="curCol.formControlCheckRule">
                                    <lcp-config-item title="表单校验">
                                        <el-select v-model="curCol.v_checkRuleTypeList" multiple collapse-tags collapse-tags-tooltip @change="checkRuleListChange">
                                            <el-option :value="item.key" :label="item.value" v-for="item in controlCheckList"></el-option>
                                        </el-select>
                                    </lcp-config-item>
                                    <div class="lcp-config-item-group">
                                        <div v-for="check in curCol.formControlCheckRule.checkRuleList" style="margin-bottom: 8px;border: #ccc dashed 1px;padding: 8px;line-height: 1.7;">
                                            <el-tag style="margin-bottom: 5px;">
                                                {{getCheckRuleName(check.checkType)}}
                                            </el-tag>
                                            <lcp-config-item title="错误提示" size="small">
                                                <el-input v-model="check.errorTips" placeholder="错误提示文案" size="small"></el-input>
                                            </lcp-config-item>
                                            <lcp-config-item title="正则表达" size="small" v-if="check.checkType=='ErrReg'">
                                                <el-input v-model="check.checkReg" :row="2" placeholder="正则表达式" size="small"></el-input>
                                            </lcp-config-item>
                                            <lcp-config-item title="范围区间" size="small" v-if="check.checkType=='ErrLength' || check.checkType=='ErrNumber'">
                                                <div class="qy-flex flex-center-vertical">
                                                    <el-input v-model="check.min" placeholder="下限" size="small" style="flex: 1;"></el-input>
                                                    ~
                                                    <el-input v-model="check.max" placeholder="上限" size="small" style="flex: 1;"></el-input>
                                                </div>
                                            </lcp-config-item>
                                        </div>
                                    </div>
                                </div>
                                

                            </el-form>
                        </el-scrollbar>
                    </el-tab-pane>
                    
                    

                    <el-tab-pane label="显隐控制" name="show">
                        <el-scrollbar>
                            <el-form v-if="curCol && curCol.formColumnShowConfig" label-position="top" label-width="140px">
                                <el-form-item label="隐藏规则">
                                    <template #label="scope">
                                        <el-divider content-position="left"><span class="item-attr-title-style">隐藏规则</span></el-divider>
                                    </template>
                                    <el-radio-group v-model="curCol.formColumnShowConfig.showType">
                                        <jo-el-data url="/lowcode/generate/getShowTypeList">
                                            <template #default="scope">
                                                <el-radio v-for="item in scope.data" :label="item.key" style="width: 95px;" >{{item.value}}</el-radio>
                                            </template>
                                        </jo-el-data>
                                    </el-radio-group>
                                    <div v-if="curCol.formColumnShowConfig.showType == 4" class="flex-x">
                                        <div>隐藏条件：</div>
                                        <div class="qy-flex-1">
                                            <el-input v-model="curCol.formColumnShowConfig.hideCondition" placeholder="隐藏条件布尔表达式,例如:formData.id==1"></el-input>
                                        </div>
                                        <div style="padding: 0px 10px;color: #E6A33F;">
                                            <el-tooltip effect="dark" placement="top-start" 
                                                content="变量提示,formData:表单数据,addFlag:新增表单标识,editFlag:编辑表单标识,以及其他">
                                                <i class="fa fa-question-circle-o"></i>
                                                <el-button>top-start</el-button>
                                            </el-tooltip>
                                        </div>
                                    </div>
                                </el-form-item>

                                <el-form-item label="禁用规则">
                                    <template #label="scope">
                                        <el-divider content-position="left"><span class="item-attr-title-style">禁用规则</span></el-divider>
                                    </template>
                                    <el-radio-group v-model="curCol.formColumnShowConfig.disableType">
                                        <jo-el-data url="/lowcode/generate/getDisableTypeList">
                                            <template #default="scope">
                                                <el-radio v-for="item in scope.data" :label="item.key" style="width: 95px;">{{item.value}}</el-radio>
                                            </template>
                                        </jo-el-data>
                                    </el-radio-group>
                                    <div v-if="curCol.formColumnShowConfig.disableType == 4" class="flex-x">
                                        <div>禁用条件：</div>
                                        <div class="qy-flex-1">
                                            <el-input v-model="curCol.formColumnShowConfig.disableCondition" placeholder="禁用条件布尔表达式,例如:formData.id==1"></el-input>
                                        </div>
                                        <div style="padding: 0px 10px;color: #E6A33F;">
                                            <el-tooltip effect="dark" placement="top-start"
                                                        content="变量提示,formData:表单数据,addFlag:新增表单标识,editFlag:编辑表单标识,以及其他">
                                                <i class="fa fa-question-circle-o"></i>
                                                <el-button>top-start</el-button>
                                            </el-tooltip>
                                        </div>
                                    </div>
                                </el-form-item>
                            </el-form>
                        </el-scrollbar>
                    </el-tab-pane>

                    <el-tab-pane label="全局配置" name="second">
                        <el-scrollbar>
                            <el-form label-position="top" label-width="140px">
                                <el-form-item label="表单项标题位置">
                                    <el-radio-group v-model="formData.formLabelPosition">
                                        <el-radio-button label="left"></el-radio-button>
                                        <el-radio-button label="top"></el-radio-button>
                                        <el-radio-button label="right"></el-radio-button>
                                    </el-radio-group>
                                </el-form-item>
                                <lcp-config-item title="">
                                    <div style="color: #606266;">
                                        隐藏表单更新按钮：
                                        <el-switch v-model="formData.extInfo.hideFormUpdateFlag" :active-value="1" :inactive-value="0"></el-switch>
                                    </div>
                                </lcp-config-item>
                                <el-form-item label="表单窗口类型">
                                    <el-radio-group v-model="formData.extInfo.formDialogType">
                                        <el-radio-button label="dialog">对话框</el-radio-button>
                                        <el-radio-button label="drawer">抽屉</el-radio-button>
                                    </el-radio-group>
                                </el-form-item>
                                <el-form-item label="表单宽度">
                                    <el-input v-model="formData.extInfo.formWidth" placeholder="表单宽度"></el-input>
                                </el-form-item>
                                <el-form-item label="表单项标题宽度">
                                    <el-input v-model="formData.formLabelWidth"></el-input>
                                </el-form-item>
                                <el-form-item label="表单详情URL">
                                    <el-input v-model="formData.extInfo.detailUrl" placeholder="非必要可不填，为空会自动生成url"></el-input>
                                </el-form-item>
                                <el-form-item label="数据插入URL">
                                    <el-input v-model="formData.extInfo.insertUrl" placeholder="非必要可不填，为空会自动生成url"></el-input>
                                </el-form-item>
                                <el-form-item label="数据更新URL">
                                    <el-input v-model="formData.extInfo.updateUrl" placeholder="非必要可不填，为空会自动生成url"></el-input>
                                </el-form-item>
                            </el-form>
                        </el-scrollbar>
                    </el-tab-pane>
                </el-tabs>
            </div>
        </el-aside>
    </el-container>
    `,
    methods: {
        // 选中字段
        colDetail(col) {
            this.curCol = col;
        },
        // 加载字典选项
        loadDictCodeOptions() {
            jo.postJsonAjax("{URL_CMS}lowcode/lcpDict/getList", {}).success((json) => {
                this.dictCodeOptions = json.data || [];
            });
        },
        // 控件数据源快捷方式选择
        controlDataSourceClick(item) {
            if (this.curCol) {
                if (!this.curCol.formControlDataSource) {
                    this.curCol.formControlDataSource = {};
                }
                this.curCol.formControlDataSource.dataUrl = item.dataUrl;
                this.curCol.formControlDataSource.keyField = item.keyField;
                this.curCol.formControlDataSource.valueField = item.valueField;
            }
        },
        // 批量设置表单列数
        batchSetFormColumnWidth(span) {
            jo.forEach(this.formData.fieldList, function (item) {
                item.formColumnGridWidth = span;
            });
        },
        // 校验规则多选框变更
        checkRuleListChange(val) {
            this.curCol.v_checkRuleTypeList = val;
            // 原先的规则映射
            var oldRuleMap = {};
            jo.forEach(this.curCol.formControlCheckRule.checkRuleList, function (item) {
                oldRuleMap[item.checkType] = item;
            });

            // 新校验规则对象集合
            var arr = [];
            jo.forEach(this.curCol.v_checkRuleTypeList, function (item) {
                var old = oldRuleMap[item];
                if (old) {
                    arr.push(old);
                } else {
                    var check = {checkType: item, checkReg: '', errorTips: ''};
                    if (item === 'ErrMail') {
                        check.errorTips = '请输入正确的邮箱格式';
                    } else if (item === 'ErrPhone') {
                        check.errorTips = '请输入正确的手机号格式';
                    } else {
                        check.errorTips = '请输入符合要求的内容';
                    }
                    arr.push(check);
                }
            });
            this.curCol.formControlCheckRule.checkRuleList = arr;
        },
        // 获取规则名称
        getCheckRuleName(checkType) {
            var name = this.controlCheckMap[checkType];
            return name ? name : checkType;
        },
        // 删除数据项
        deleteOptionItem(idx) {
            var arr = [];
            jo.forEach(this.curCol.formControlDataSource.optionItemList, function (item, i) {
                if (idx === i) {
                    return;
                }
                arr.push(item);
            });
            this.curCol.formControlDataSource.optionItemList = arr;
        },
        // 添加数据项
        addOptionItem() {
            if (!this.curCol.formControlDataSource.optionItemList) {
                this.curCol.formControlDataSource.optionItemList = [];
            }
            this.curCol.formControlDataSource.optionItemList.push({key: '', value: ''});
        },
        // 拖拽表单项开始
        moveColumnStart(event) {
            // 当前拖拽对象
            var target = event.target;
        },
        // 拖拽表单项过程中
        moveColumnIng(event) {
            this.moveCol(event);
        },
        // 拖拽表单项结束
        moveColumnEnd(event) {
            // 清空位置提示
            $('.preview-col-item').removeClass('move-col-before');
            $('.preview-col-item').removeClass('move-col-after');
            this.moveCol(event, (target, item, type) => {
                this.dragPosition(target, item, type);
                // 清空位置提示
                $('.preview-col-item').removeClass('move-col-before');
                $('.preview-col-item').removeClass('move-col-after');
            });
        },
        // 拖拽结束后调整位置, 参数: 被拖拽对象, 锚定对象, 移动到锚定对象的后面after或者前面before
        dragPosition(target, posItem, type) {
            var num = 0;
            jo.forEach(this.columnListForForm, function (item, i) {
                item.formColumnOrder = (i + 1) * 10;
                // 找到锚定对象
                if ($(posItem).attr('dataColId') == item.fieldDbName) {
                    if (type === 'after') {
                        num = item.formColumnOrder + 5;
                    } else {
                        num = item.formColumnOrder - 5;
                    }
                }
            });
            if (num) {
                jo.forEach(this.columnList, function (item, i) {
                    // 找到锚定对象
                    if ($(target).attr('dataColId') == item.fieldDbName) {
                        item.formColumnOrder = num;
                    }
                });
            }
        },
        // 移动表单配置字段列
        moveCol(event, endFunc) {
            // 获取鼠标位置(窗口绝对位置)
            var x = event.pageX;
            var y = event.pageY;

            // 无效数据过滤
            if (!x || !y) {
                jo.log('鼠标坐标无效', [x, y, event]);
                return;
            }

            // 校准鼠标位置, 加上滚动条高度就是内容位置
            var scrollTop = $('.previewBoxScroll')[0].scrollTop;
            var scrollLeft = $('.previewBoxScroll')[0].scrollLeft;
            if (scrollTop) {
                y += scrollTop;
            }
            if (scrollLeft) {
                x += scrollLeft;
            }

            // 当前拖拽对象
            var target = event.target;

            // 计算每个表单项的位置
            var left = 320;// 父容器左侧距离
            var top = 70;// 父容器顶部距离
            // 所有的表单项
            var colArr = $('.preview-col-item');
            // 为每个表单项范围
            var rangeArr = [];
            jo.forEach(colArr, function (item, idx) {
                // 获取宽高, 计算中心点
                var width = item.clientWidth;
                var height = item.clientHeight;
                var absX = item.offsetLeft + left;
                var absY = item.offsetTop + top;
                // 表单项区域范围, 左上角点的xy坐标和右下角点的xy坐标
                rangeArr.push([absX, absY, absX + width, absY + height]);
            });

            // console.info(rangeArr);

            for (var i = 0; i < colArr.length; i++) {
                var item = colArr[i];
                var range = rangeArr[i];
                var x1 = range[0];
                var y1 = range[1];
                var x2 = range[2];
                var y2 = range[3];
                // 判断鼠标是否在当前表单项范围内
                if (x > x1 && x < x2 && y > y1 && y < y2 && target != item) {
                    // 先清空位置提示
                    $('.preview-col-item').removeClass('move-col-before');
                    $('.preview-col-item').removeClass('move-col-after');
                    // 判断鼠标在中线左边还是右边
                    var centerLineX = (x1 + x2) / 2;
                    if (x > centerLineX) {
                        if (typeof endFunc == 'function') {
                            endFunc(target, item, 'after');
                        } else {
                            $(item).addClass('move-col-after');
                        }
                    } else {
                        if (typeof endFunc == 'function') {
                            endFunc(target, item, 'before');
                        } else {
                            $(item).addClass('move-col-before');
                        }
                    }
                }
            }
        },
    },
    mounted() {
        jo.postJsonAjax("{URL_CMS}lowcode/generate/getControlTypeList", {}).success((json) => {
            this.controlTypeList = json.data;
        });
        jo.postJsonAjax("{URL_CMS}lowcode/generate/getControlCheckList", {}).success((json) => {
            this.controlCheckList = json.data;
        });
        this.loadDictCodeOptions();
        console.info('[lcp-generate-form] mounted.');
    }
});