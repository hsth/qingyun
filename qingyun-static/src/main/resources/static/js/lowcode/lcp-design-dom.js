// 低代码设计dom树处理
var LCP_DESIGN_DOM_VUE = {
    methods: {
        // 重新渲染dom树
        refreshDomTree() {
            this.domTreeShow = false;
            window.setTimeout(()=>{
                this.domTreeShow = true;
            }, 100);
            console.info('[重新渲染dom树] over!');
        },
        // 从dom树移除节点
        removeEleForDomTree(ele) {
            // 移除元素
            this.removeEleWrap(ele);
            // dom树变更
            this.domTreeOperate++;
        },
        // 从dom树拷贝节点
        copyEleForDomTree(ele) {
            // 拷贝节点对象
            var copyEle = this.convertOptionToEle(ele);
            // 移动新节点到原节点后面
            this.moveComponentElePosition(copyEle, ele, 'after');
        },
        // dom树节点是否可以被拖拽
        domTreeAllowDrag() {
            return true;
        },
        // dom树节点是否可以被拖放, prev/next/inner
        domTreeAllowDrop(dragNode, dropNode, position) {
            if (position === 'inner' && !dropNode.data.canAsParent) {
                // console.info('[dom树节点是否可以被拖放] 当前拖放节点不支持作为父节点', dropNode)
                return false;
            }
            return true;
        },
        // dom树节点拖放, 共四个参数，依次为：被拖拽节点对应的 Node、结束拖拽时最后进入的节点、被拖拽节点的放置位置（before、after、inner）、event
        domTreeDrop(dragNode, dropNode, position, event) {
            // this.moveComponentElePosition(dragNode.data, dropNode.data, position);
        },
        // 页面配置中组件选中
        domTreeClick(clickItem, clickNode, tree, event) {
            if (!this.formConfigActiveEle || this.formConfigActiveEle.id !== clickItem.id) {
                this.formConfigActiveEle = clickItem;
            }
        },
    },
    watch: {
        // dom树变更监听
        domTreeOperate() {
            this.refreshDomTree();
        }
    }
};




