$(window).resize(function () {
    if (appVM) {
        appVM.windowHeight = $(window).height();
    }
});
joEl.register('lcp-config-item', {
    props: {
        // 标题
        title: {
            type: String,
            default: ''
        },
        // 标题与内容垂直
        y: {
            type: Boolean,
            default: false
        },
        width: {
            type: String,
            default: '4em'
        },
        align: {
            type: String,
            default: 'left'
        },
        size: {
            type: String,
            default: 'default'
        }
    },
    data: function () {
        return {};
    },
    computed: {
        // 容器样式
        classArr() {
            var arr = [];
            if (this.y) {
                arr.push('flex-y');
            } else {
                arr.push('flex-x');
                arr.push('flex-center-vertical');
            }
            return arr;
        },
        // 标题样式
        titleClass() {
            var arr = [];
            if (this.y) {
                arr.push('column-config-title-y');
            } else {
                arr.push('column-config-title');
            }
            return arr;
        },
        titleStyle() {
            var style = {
                width: this.width
            };
            if (this.size === 'small') {
                style.fontSize = '12px';
            } else if (this.size === 'large') {
                style.fontSize = '16px';
            }
            return style;
        },
        contentStyle() {
            var style = {
                textAlign: this.align
            };
            if (this.size === 'small') {
                style.fontSize = '12px';
            } else if (this.size === 'large') {
                style.fontSize = '16px';
            }
            return style;
        }
    },
    template: `
        <div class="lcp-config-item" :class="classArr">
            <div v-if="title" :class="titleClass" :style="titleStyle">
                <slot name="title">{{title}}</slot>
            </div>
            <div class="qy-flex-1" :style="contentStyle">
                <slot></slot>
            </div>
        </div>
    `,
    methods: {},
    mounted() {
        console.info('[lcp-config-item] mounted');
    }
});

const app = Vue.createApp(joEl.buildVueAppParam({
    data: function () {
        return {
            windowHeight: $(window).height(),
            id: jo.getUrlParam('id'),
            // 表单详情
            formData: {
                extInfo: {}
                , fieldList: []
            },
        };
    }
    , computed: {
        defaultHeight() {
            return this.windowHeight - 160;
        },
        tableHeight() {
            return this.windowHeight - 80;
        },
        contentHeight() {
            return this.windowHeight - 85;
        },
        // 字段列集合
        columnList() {
            if (this.formData) {
                return this.formData.fieldList;
            }
            return [];
        },
    }
    , methods: {
        // 添加字段查询配置
        appendFieldSearchConfig() {
            if (!this.formData.extInfo.fieldSearchConfigList) {
                this.formData.extInfo.fieldSearchConfigList = [];
            }
            this.formData.extInfo.fieldSearchConfigList.push({fieldJavaNameList: [], selectOneFlag: true});
        },
        // 表选择
        tableChange(val) {
            var _this = this;
            jo.postJson('{URL_CMS}lowcode/generate/initGenerateByTableName', {body: val}).success(function (json) {
                if (json && json.code == 0) {
                    _this.formData = json.data;
                    _this.curCol = null;
                }
            });
        },
        // 枚举数据初始化
        init() {
            var _this = this;
            if (this.id) {
                jo.postJsonAjax('{URL_CMS}lowcode/generate/getGenerateDetail', {body: this.id}).success(function (json) {
                    _this.formData = json.data;
                    if (!_this.formData.formPageConfig) {
                        _this.formData.formPageConfig = {};
                    }
                    _this.formPageConfig = _this.formData.formPageConfig;
                    if (!_this.formPageConfig.elementList) {
                        _this.formPageConfig.elementList = [];
                    }
                }).error(function (json) {
                    jo.showErrorMsg('查询详情失败');
                });
            }
        },
        // 新增
        add() {
            jo.postJsonAjax('/lowcode/generate/addGenerateConfig', {body: this.formData}).success(function (json) {
                parent.jo.showSuccessMsg('保存成功~');
                // 刷新表格数据
                parent.appVM.list_refresh();
                // 关闭弹窗
                jo.closeSelf();
            }).error(function (json) {
                parent.jo.showErrorMsg('保存失败~');
            });
        },
        // 更新
        update() {
            jo.postJsonAjax('/lowcode/generate/update', {body: this.formData}).success(function (json) {
                parent.jo.showSuccessMsg('更新成功~');
                // 刷新表格数据
                parent.appVM.list_refresh();
                // 关闭弹窗
                // jo.closeSelf();
                window.location.reload();
            }).error(function (json) {
                parent.jo.showErrorMsg('更新失败~');
            });
        },
        // 同步字段变更
        syncFieldChange() {
            var _this = this;
            jo.postJsonAjax('/lowcode/generate/syncFieldChange', {body: this.id}).success(function (json) {
                parent.jo.showSuccessMsg('同步成功~');
                _this.formData = json.data;
                _this.curCol = null;
            }).error(function (json) {
                parent.jo.showErrorMsg('同步失败~');
            });
        },
        // 删除
        del(func) {
            jo.confirm('您确定要删除嘛?', function (idx) {
                jo.postJsonAjax('/lowcode/generate/delete', {body: this.id}).success(function (json) {
                    if (typeof func == 'function') {
                        func(json);
                    } else {
                        parent.jo.showSuccessMsg('删除成功~');
                        // 刷新表格数据
                        parent.appVM.list_refresh();
                        // 关闭弹窗
                        jo.closeSelf();
                    }
                }).error(function (json) {
                    parent.jo.showErrorMsg('删除失败~');
                });
                jo.close(idx);
            });

        },
    }
    , watch: {
        // 选中组件发生变化时的监听
        formConfigActiveEle() {
            var ele = this.formConfigActiveEle;
            if (ele) {
                if (!ele.styleList) {
                    ele.styleList = [];
                }
                if (ele.styleList.length === 0) {
                    ele.styleList.push({});
                }
                if (!ele.attrList) {
                    ele.attrList = [];
                }
                if (ele.attrList.length === 0) {
                    ele.attrList.push({});
                }
            }
        }
    }
    , mounted() {
        // 页面加载完成
        this.qy_page_loaded();
        this.init();
    }
    , setup() {

    }
}, VUE_LOW_CODE_FORM_CONFIG_V2));
app.use(ElementPlus, {locale: ElementPlusLocaleZhCn});
joEl.component(app);

var appVM = app.mount("#app");


//大写格式转驼峰,首字母小写
function up2Tuo(str) {
    if (jo.isValid(str)) {
        var arr = str.split("_");
        if (arr.length > 1) {
            var s = arr[0].toLowerCase();
            for (var i = 1; i < arr.length; i++) {
                s += arr[i].substring(0, 1).toUpperCase() + arr[i].substring(1).toLowerCase();
            }
            return s;
        } else {
            return str.toLowerCase();
        }
    } else {
        return "";
    }
}

//单词首字母大写
function wordOne2Up(str) {
    return str.substring(0, 1).toUpperCase() + str.substring(1);
}

//单词首字母小写
function wordOne2Low(str) {
    return str.substring(0, 1).toLowerCase() + str.substring(1);
}

