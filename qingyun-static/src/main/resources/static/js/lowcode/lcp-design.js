$(window).resize(function () {
    if (appVM) {
        appVM.windowHeight = $(window).height();
    }
});

const app = Vue.createApp(joEl.buildVueAppParam({
    data: function () {
        return {
            windowHeight: $(window).height(),
            designHeight: $(window).height() - 30,


            // 组件列表
            componentOptionList: [],


            // 表单配置当前选中面板, fields/component/highComponent/chart
            formConfigCollapse: 'layout',
            formConfigLeftTab: 'component',
            formConfigRightTab: 'detail',
            // 表单配置拖拽类型
            formConfigDragType: {
                option: {code: 'option', text: '被拖拽的是左侧组件选项'},
                inner: {code: 'inner', text: '被拖拽的是表单配置容器中已有的元素'},
            },
            // 当前正在拖拽的组件
            dragContext: {
                // 拖拽类型, formConfigDragType
                dragType: '',
                dragEleOption: null,
            },
            // 表单(页面)配置对象
            pageConfig: {
                // 元素(组件)集合
                elementList: []
            },
            // 代码生成配置
            formData: {},
            // 当前选中的组件
            formConfigActiveEle: null,

            // dom树展示
            domTreeShow: true,

            // 上次缓存内容的摘要,防止重复缓存
            cachePageDigest: '',

            // 组件分类
            compCategoryOptions: PAGE_CONFIG_COMPONENT_CATEGORY,


            // 元素全局递增序号
            seqId: 0,

            // 组件属性数据类型选项
            attrDataTypeOptions: jo.mergeArray([{key: 'var', value: 'vue变量', styleType: ''}], LCP_COMPONENT_ATTR_DATA_TYPE),
            // 组件属性的数据类型选项, 具体到某个属性, key是组件名, value是{属性:可选类型}
            compAttrDataTypeOptions: {

            },
            // 图标选择数据集
            joEl_constants_iconOptions: joEl.constants.iconOptions,
        };
    }
    , computed: {
        // 组件元数据map类型
        componentMetaMap() {
            return jo.array2Object(this.componentOptionList, item => {
                return item.type;
            });
        },
        // 分组后的左侧可选组件
        componentOptionGroupList() {
            var arr = [];
            jo.forEach(this.compCategoryOptions, item => {
                var group = jo.copyObject(item);
                group.componentList = [];
                jo.forEach(this.componentOptionList, comp => {
                    if (comp.showInOptional === 'yes') {
                        if (comp.category === group.name) {
                            // 复合型组件, 配置以groupContent字段为准
                            if (comp.metaType === 'group') {
                                if (comp.groupContent) {
                                    try {
                                        var groupComp = JSON.parse(comp.groupContent);
                                        groupComp.optionShowName = comp.name;
                                        groupComp.icon = comp.icon;
                                        group.componentList.push(groupComp);
                                    } catch (e) {
                                        console.error('[初始化组件元数据] 复合组件配置解析异常:', comp.groupContent, comp, e);
                                    }
                                } else {
                                    console.warn('[初始化组件元数据] 复合组件配置有误:', comp);
                                }
                            } else {
                                group.componentList.push(comp);
                            }
                        }
                    }
                });
                arr.push(group);
            });
            return arr;
        },
        // 可选的基础组件列表
        baseComponentOptionList() {
            var arr = [];
            jo.forEach(this.componentOptionList, item => {
                if (item.metaType === 'base') {
                    arr.push(item);
                }
            });
            return arr;
        },
        // dom树
        domTree() {
            var arr = [];
            if (this.pageConfig) {
                // jo.forEach(this.pageConfig.elementList, function (item) {
                //     arr.push(item);
                // });
                return jo.copyObjectDeep(this.pageConfig.elementList);
            }
            return arr;
        },
        // 表单配置选中项
        formConfigActiveEleWrap() {
            if (!this.formConfigActiveEle) {
                return {};
            }
        },
        // 当前选中组件的id
        formConfigActiveEleId() {
            return this.formConfigActiveEle ? this.formConfigActiveEle.id : '';
        },
        // 当前选中组件的scheme
        formConfigActiveEleSchemeShow() {
            var list = [];
            var push = function (s, indent, target) {
                target = target || list;
                target.push({
                    text: s,
                    indent: indent || 0
                });
            }
            var pushObject = function (obj, indent, pre, lastFlag, target) {
                var arrayFlag = Array.isArray(obj);
                var douHao = lastFlag ? '' : ',';
                // 空对象处理
                if (arrayFlag && jo.arrayIsEmpty(obj)) {
                    push((pre || '') + '[]' + douHao, indent, target);
                    return;
                } else if (typeof obj == 'object' && jo.objIsEmpty(obj)) {
                    push((pre || '') + '{}' + douHao, indent, target);
                    return;
                }

                push((pre || '') + (arrayFlag ? '[' : '{'), indent, target);
                var fields = [];
                for (var k in obj) {
                    fields.push(k);
                }
                for (let i = 0; i < fields.length; i++) {
                    var k = fields[i];
                    var val = obj[k];
                    var key = arrayFlag ? '' : '"' + k + '"' + ':';
                    var end = i === fields.length - 1;
                    if (typeof val == 'object' || Array.isArray(val)) {
                        pushObject(val, indent + 1, key, end, target);
                    } else {
                        push(key + JSON.stringify(val) + (end ? '' : ','), indent + 1, target);
                    }
                }
                push((arrayFlag ? ']' : '}') + douHao, indent, target);
            }

            if (this.formConfigActiveEle) {
                pushObject(this.formConfigActiveEle, 0, '', true, list);
            }
            return list;
        },

        // 可选的样式表集合
        classOptionList() {
            return LCP_DESIGN_CLASS_META;
        },
        // 可选的内联样式集合
        styleOptionList() {
            var arr = [];
            var _this = this;
            jo.forEach(PAGE_CONFIG_STYLE_META, function (item) {
                arr.push(_this.convertStyleMeta(item));
            });
            return arr;
        },
        // 可选的组件属性
        activeEleAttrOptionList() {
            var arr = [];
            var _this = this;
            if (this.formConfigActiveEle) {
                var config = this.componentMetaMap[this.formConfigActiveEle.type]
                if (config) {
                    jo.forEach(config.attrMetaList, function (item) {
                        arr.push(item);
                    });
                }
            }
            return arr;
        },
        // 当前选中组件 可选的组件属性类型
        activeEleAttrDataTypeOptions() {
            var map = {};
            if (this.formConfigActiveEle) {
                var typeMeta = jo.array2Object(LCP_COMPONENT_ATTR_DATA_TYPE, item=>{
                    return item.key;
                });
                // 组件元数据
                var compMeta = this.componentMetaMap[this.formConfigActiveEle.type];
                jo.forEach(compMeta.attrMetaList, attr=>{
                    let arr = attr.attrDataType ? attr.attrDataType.split(',') : [];
                    let options = [];
                    if (attr.supportVueBind === 'yes') {
                        options.push( {key: 'var', value: 'vue变量', styleType: ''});
                    }
                    jo.forEach(arr, t=>{
                        if (typeMeta[t]) {
                            options.push(typeMeta[t]);
                        }
                    })
                    map[attr.attrName] = options;
                });
            }
            return map;
        }
    }
    , methods: {
        // 删除当前节点
        removeActiveEle() {
            if (this.formConfigActiveEle) {
                this.removeEleWrap(this.formConfigActiveEle);
                this.formConfigActiveEle = null;
                // dom树变更
                this.domTreeOperate++;
            }
        },
        // 拷贝当前节点
        copyActiveEle() {
            if (this.formConfigActiveEle) {
                // 拷贝节点对象
                var copyEle = this.convertOptionToEle(this.formConfigActiveEle);
                // 移动新节点到原节点后面
                this.moveComponentElePosition(copyEle, this.formConfigActiveEle, 'after');
            }
        },
        // 转换样式元数据, 转为选择器需要的格式
        convertStyleMeta(item) {
            var _this = this;
            var obj = {
                value: item.name,
                label: item.text ? item.name + '：' + item.text : item.name
            }
            if (item.children) {
                // obj.label = item.text ? item.text + '（'+item.name+'）' : item.name;
                obj.children = [];
                jo.forEach(item.children, function (child) {
                    obj.children.push(_this.convertStyleMeta(child));
                });
            }
            return obj;
        },
        // 添加内联样式
        addStyle() {
            if (!this.formConfigActiveEle.styleList) {
                this.formConfigActiveEle.styleList = [];
            }
            this.formConfigActiveEle.styleList.push({});
        },
        // 添加常用内联样式
        addCommonlyUsedStyle() {
            if (!this.formConfigActiveEle.styleList) {
                this.formConfigActiveEle.styleList = [];
            }
            var arr = [
                {name: "width", value: ""},
                {name: "height", value: ""},
                {name: "background-color", value: ""},
                {name: "border", value: ""},
                {name: "display", value: ""}
            ];
            if (this.formConfigActiveEle.styleList.length === 1
                && !this.formConfigActiveEle.styleList[0].name
                && !this.formConfigActiveEle.styleList[0].value) {
                this.formConfigActiveEle.styleList = [];
            }
            var map = jo.array2Object(this.formConfigActiveEle.styleList, item => item.name);
            jo.forEach(arr, item => {
                if (!map[item.name]) {
                    this.formConfigActiveEle.styleList.push(item);
                }
            });
        },
        removeStyle(idx) {
            this.formConfigActiveEle.styleList.splice(idx, 1);
        },
        removeAllStyle() {
            this.formConfigActiveEle.styleList = [];
        },
        // 添加属性
        addAttr() {
            if (!this.formConfigActiveEle.attrList) {
                this.formConfigActiveEle.attrList = [];
            }
            this.formConfigActiveEle.attrList.push({valueType: 'string'});
        },
        removeAttr(idx) {
            this.formConfigActiveEle.attrList.splice(idx, 1);
        },

        // 初始化元数据
        initMeta() {
            jo.postJson('/lowcode/lcpMetaComponent/getDetailList').success(json => {
                // 历史原因, 前端用type标识组件, 后端用code标识, 这里将code转为type, 后端的type转metaType
                jo.forEach(json.data, item => {
                    item.metaType = item.type;
                    item.type = item.code;
                    item.optionShowName = item.name;
                });
                // 组件列表
                this.componentOptionList = json.data;
            });
        },
        // 定时缓存配置内容, 下次进来回显
        cachePageStart() {
            // 定时缓存起来
            window.setInterval(() => {
                var str = JSON.stringify(this.pageConfig);
                if (this.cachePageDigest !== str) {
                    this.cachePageDigest = str;
                    console.info('[定时缓存配置内容] 缓存配置内容:', this.pageConfig);
                    jo.cache.set('lcp-design-cache', str, 1000 * 60 * 60 * 24 * 30);
                } else {
                    console.info("[定时缓存配置内容] 内容没有发生变化,不缓存");
                }
            }, 3000);
        },
        // 回显缓存内容
        cachePageLoad() {
            var cache = jo.cache.get('lcp-design-cache');
            if (cache) {
                this.pageConfig = JSON.parse(cache) || {elementList: []};
                this.cachePageDigest = cache;
                console.info('回显缓存配置:', this.pageConfig);
            }
            // 初始化完成回调
            this.eleInitFinishCall();
        },
        // 画布初始化后回调
        eleInitFinishCall() {
            // 重置全局组件序号
            this.resetComponentSeqId();
        },
        // 构建组件id, 入参:左侧组件选项
        buildComponentId(option) {
            try {
                let seq = ++this.seqId;
                return option.type + '_s' + seq;
            } catch (err) {
                console.error('构建组件id异常', option, err)
                return option.type + '_' + jo.getUUID(6).toLowerCase();
            }
        },
        // 重置组件id序号
        resetComponentSeqId() {
            this.resetComponentSeqIdChild(this.pageConfig.elementList);
        },
        resetComponentSeqIdChild(eles) {
            jo.forEach(eles, item=>{
                if (item.id) {
                    let arr = item.id.split('_');
                    let id = arr[arr.length - 1];
                    let seq = id.indexOf('s') === 0 ? id.substring(1) : id;
                    if (jo.isNumber(seq)) {
                        this.seqId = Math.max(this.seqId, parseInt(seq));
                    }
                    // 遍历子节点的序号
                    if (jo.arrayIsNotEmpty(item.childrenList)) {
                        this.resetComponentSeqIdChild(item.childrenList);
                    }
                }
            });
        }
    }
    , watch: {
        // 选中组件发生变化时的监听
        formConfigActiveEle() {
            var ele = this.formConfigActiveEle;
            if (ele) {
                if (!ele.styleList) {
                    ele.styleList = [];
                }
                if (ele.styleList.length === 0) {
                    ele.styleList.push({});
                }
                if (!ele.attrList) {
                    ele.attrList = [];
                }
                if (ele.attrList.length === 0) {
                    ele.attrList.push({valueType: 'string'});
                }
            }
        },
    }
    , mounted() {
        // 页面加载完成
        this.qy_page_loaded();
        // 初始化元数据
        this.initMeta();
        this.cachePageLoad();
        this.cachePageStart();
    }
    , setup() {

    }
}, LCP_DESIGN_DRAG_VUE, LCP_DESIGN_DOM_VUE));
app.use(ElementPlus, {locale: ElementPlusLocaleZhCn});
joEl.component(app);
lcpUtil.component(app);

var appVM = app.mount("#app");



