joEl.register('lcp-db-data-source', joEl.buildVueAppParam({
    emits: ['change', 'openSearchDrawer'],
    props: {},
    data: function () {
        return {
            // 表格高度
            tableHeight: 'auto',
            // 表单宽度
            formWidth: '60%',
            // 表单项标题宽度
            formLabelWidth: '120px',
            // 是否支持更新保存按钮展示
            updateSaveBtnShow: true,
            // 是否支持新增保存按钮展示
            addSaveBtnShow: true,
            // 查询参数
            searchCondition: {},
            // 表格数据url
            tableDataUrl: '/lowcode/lcpDbDataSource/getPage',
            // 表单详情查询url
            formQueryUrl: '/lowcode/lcpDbDataSource/get',
            // 新增url
            formInsertUrl: '/lowcode/lcpDbDataSource/insert',
            // 修改url
            formUpdateUrl: '/lowcode/lcpDbDataSource/update',
            // 删除url
            deleteUrl: '/lowcode/lcpDbDataSource/delete',
            // 模板下载url
            templateUrl: '/lowcode/lcpDbDataSource/templateDownload',
            // 导入url
            importUrl: '/lowcode/lcpDbDataSource/import',
            // 导出url
            exportUrl: '/lowcode/lcpDbDataSource/export',
            // 校验规则
            checkRules: {
                code: [
                    joEl.rules.required('数据源编码不允许为空', 'blur'),
                    joEl.rules.length('数据源编码内容长度限制为1~64', 'blur', 64),
                ],
                name: [
                    joEl.rules.required('数据源名称不允许为空', 'blur'),
                    joEl.rules.length('数据源名称内容长度限制为1~64', 'blur', 64),
                ],
                dbType: [
                    joEl.rules.required('数据库类型不允许为空', 'blur'),
                    joEl.rules.length('数据库类型内容长度限制为1~32', 'blur', 32),
                ],
                driverClass: [
                    joEl.rules.required('驱动类不允许为空', 'blur'),
                    joEl.rules.length('驱动类内容长度限制为1~256', 'blur', 256),
                ],
                dbUrl: [
                    joEl.rules.required('数据库连接不允许为空', 'blur'),
                    joEl.rules.length('数据库连接内容长度限制为1~512', 'blur', 512),
                ],
                username: [
                    joEl.rules.required('用户名不允许为空', 'blur'),
                    joEl.rules.length('用户名内容长度限制为1~128', 'blur', 128),
                ],
                password: [
                    joEl.rules.required('密码不允许为空', 'blur'),
                    joEl.rules.length('密码内容长度限制为1~128', 'blur', 128),
                ],
                remark: [
                    joEl.rules.length('备注内容长度限制为1~256', 'blur', 256),
                ],
            },
            // 数据选项: 数据库类型
            dbTypeOptions: [
                {key: 'MYSQL', value: 'MySQL', styleType: 'primary'},
                {key: 'ORACLE', value: 'Oracle', styleType: 'success'},
                {key: 'POSTGRESQL', value: 'PgSQL', styleType: 'warning'},
            ],


            // 选中的数据源
            activeSource: {},
            // 默认数据源
            defaultSource: {
                id: -1,
                code: '_current',
                name: '当前开发平台库',
                remark: '当前开发平台所用的数据库',
                dbType: 'MYSQL'
            },
        }
    }
    , computed: {
        activeId() {
            return this.activeSource ? this.activeSource.id : null;
        }
    }
    , template: `
    <div class="qy-view-page-content">
        <!-- 查询条件 -->
        <div id="qy-view-search-bar2" class="qy-view-search-bar ">
            <div class="qy-view-search-condition-bar">
                <el-row :gutter="10">
                    <el-col :span="12">
                        <el-form-item label=""  style="margin-bottom: 10px;" >
                            <el-input v-model="searchCondition.code" placeholder="数据源编码"></el-input>
                        </el-form-item>
                    </el-col>
                    <el-col :span="12">
                        <el-form-item label=""  style="margin-bottom: 10px;" >
                            <el-input v-model="searchCondition.name" placeholder="数据源名称"></el-input>
                        </el-form-item>
                    </el-col>
                    <el-col :span="12">
                        <el-form-item label=""  style="margin-bottom: 0px;" >
                            <el-select v-model="searchCondition.dbType" :clearable="true" placeholder="数据源类型">
                                <el-option v-for="option in dbTypeOptions" :value="option.key" :label="option.value"></el-option>
                            </el-select>
                        </el-form-item>
                    </el-col>
                    <el-col :span="12">
                        <el-button type="primary" @click="list_search"><i class="fa fa-search" aria-hidden="true"></i>&nbsp;查询</el-button>
                    </el-col>
                </el-row>
            </div>
        </div>
        <!-- 按钮区 -->
        <div id="qy-view-button-bar2" class="qy-view-button-bar ">
            <el-button type="primary" plain
                       size="default"
                       @click="list_add"

            >新增</el-button>
        </div>
        <!-- 数据表格 -->
        <div id="qy-view-data-bar2" class="qy-view-data-bar ">
            <jo-el-table :url="tableDataUrl" :ref="joTableRef" :search-param="searchCondition" 
                first-column="radio" :radio-init-value="0"
                :page-size="10" :enable-loading="true" >
                <template #default="scope">
                    <el-card shadow="hover" body-style="padding:15px;"
                        style="margin-bottom: 10px;" @click="dataSourceClick(defaultSource)"
                        :class="{ 'card-active' : activeId == defaultSource.id, 'ds-card' : true }">
                        <div>
                            <div class="flex-x" style="margin-bottom: 5px;">
                                <div class="qy-flex-1">
                                    <jo-el-tag :value="defaultSource.dbType" :options="dbTypeOptions" code-field="key" text-field="value"></jo-el-tag>
                                    {{defaultSource.name}}（{{defaultSource.code}}）
                                </div>
                                <div>
                                    <el-button type="success" link size="default" title="打开查询窗口" @click.stop="openSearchDrawerInner(defaultSource)">
                                        <i class="fa fa-search"></i>
                                    </el-button>
                                </div>
                            </div>
                            <div style="color: #888888;">
                                {{defaultSource.remark}}
                            </div>
                        </div>
                    </el-card>
                    <el-card :key="item.id" shadow="hover" v-for="item in scope.data" 
                        body-style="padding:15px;"
                        style="margin-bottom: 10px;" @click="dataSourceClick(item)"
                        :class="{ 'card-active' : activeId == item.id, 'ds-card' : true }"
                        >
                        <div>
                            <div class="flex-x" style="margin-bottom: 5px;">
                                <div class="qy-flex-1">
                                    <jo-el-tag :value="item.dbType" :options="dbTypeOptions" code-field="key" text-field="value"></jo-el-tag>
                                    {{item.name}}（{{item.code}}）
                                </div>
                                <div>
                                    <el-button type="primary" link size="default" title="编辑" @click.stop="list_edit(item)">
                                        <i class="fa fa-edit"></i>
                                    </el-button>
                                    <el-divider direction="vertical"></el-divider>
                                    <el-button type="success" link size="default" title="打开查询窗口" @click.stop="openSearchDrawerInner(item)">
                                        <i class="fa fa-search"></i>
                                    </el-button>
                                </div>
                            </div>
                            <div class="flex-x">
                                <div class="qy-flex-1" style="color: #888888;">
                                    {{item.remark}}
                                </div>
                                <div>
                                    <el-button type="danger" link size="default" title="删除" @click.stop="list_delete(item)">
                                        <i class="fa fa-trash-o"></i>
                                    </el-button>
                                </div>
                            </div>
                        </div>
                    </el-card>
                </template>
            </jo-el-table>
        </div>
    </div>
    <!-- 表单 -->
    <el-dialog v-model="formShow" :title="formTitle" :width="formWidth" :draggable="true">
        <el-form :ref="formRef" :model="formData" label-position="right" :rules="checkRules" :label-width="formLabelWidth">
            <el-row :gutter="15">
                <el-col :span="24"  >
                    <el-form-item label="数据源编码" prop="code">
                        <el-input v-model="formData.code"
                                  placeholder=""></el-input>
                    </el-form-item>
                </el-col>
                <el-col :span="24"  >
                    <el-form-item label="数据源名称" prop="name">
                        <el-input v-model="formData.name"
                                  placeholder=""></el-input>
                    </el-form-item>
                </el-col>
                <el-col :span="24"  >
                    <el-form-item label="数据库类型" prop="dbType">
                        <el-select v-model="formData.dbType"
                                   placeholder="">
                            <el-option v-for="option in dbTypeOptions" :value="option.key" :label="option.value"></el-option>
                        </el-select>
                    </el-form-item>
                </el-col>
                <el-col :span="24"  >
                    <el-form-item label="驱动类" prop="driverClass">
                        <el-input v-model="formData.driverClass"
                                  placeholder=""></el-input>
                    </el-form-item>
                </el-col>
                <el-col :span="24"  >
                    <el-form-item label="数据库连接" prop="dbUrl">
                        <el-input type="textarea" v-model="formData.dbUrl"
                                  placeholder="" :rows="3"  ></el-input>
                    </el-form-item>
                </el-col>
                <el-col :span="12"  >
                    <el-form-item label="用户名" prop="username">
                        <el-input v-model="formData.username"
                                  placeholder=""></el-input>
                    </el-form-item>
                </el-col>
                <el-col :span="12"  >
                    <el-form-item label="密码" prop="password">
                        <el-input v-model="formData.password"
                                  placeholder=""></el-input>
                    </el-form-item>
                </el-col>
                <el-col :span="24"  >
                    <el-form-item label="备注" prop="remark">
                        <el-input type="textarea" v-model="formData.remark"
                                  placeholder="" :rows="3"  ></el-input>
                    </el-form-item>
                </el-col>
            </el-row>
        </el-form>
        <template #footer>
        <span class="dialog-footer">
            <el-button @click="close_form">关闭</el-button>
            <el-button type="primary" @click="form_save" v-if="addSaveBtnShow && addFlag">确认</el-button>
            <el-button type="primary" @click="form_save" v-else-if="updateSaveBtnShow && editFlag">确认</el-button>
        </span>
        </template>
    </el-dialog>
    `
    , methods: {
        // 新增表单数据初始化前的处理
        handle_form_init_data_add(data) {
            // 默认值
            data.dbType = 'MYSQL';
            data.driverClass = 'com.mysql.cj.jdbc.Driver';
        },
        // 编辑表单数据初始化前的处理
        handle_form_init_data_edit(data) {
        },

        // 数据区高度适应
        dataBarResize() {
            var total = 0;
            jo.forEach($('.qy-view-data-bar').siblings('.qy-height-resize'), (item) => {
                var id = item.id;
                if (id) {
                    total += jo.getDefVal($('#' + id).outerHeight(true), 0);
                }
            });
            var boxPadding = $('.qy-view-page-content').outerHeight(true) - $('.qy-view-page-content').height();
            var h = $('.qy-view-page-app').height() - boxPadding - total - jo.getDefVal($('.jo-el-table-page-bar').outerHeight(true));
            console.info('更新数据表格高度: %s -> %s', this.tableHeight, h);
            this.tableHeight = h;
        },
        // 数据区监听
        dataBarResizeListener() {
            // 初始计算
            this.dataBarResize();
            // 监听注册
            var arr = $('.qy-view-data-bar').siblings('.qy-height-resize');
            jo.forEach(arr, (item) => {
                var id = item.id;
                if (id) {
                    this.registerElementHeightResize('#' + item.id, this.dataBarResize);
                } else {
                    console.warn('注册数据区高度监听需要兄弟节点有id属性:', item);
                }
            });
            // 数据区高度变化
            // this.registerElementHeightResize('#qy-view-data-bar', this.dataBarResize);
        },

        // 点击选中数据源
        dataSourceClick(item) {
            if (this.activeSource && this.activeSource.id === item.id) {

            } else {
                var old = this.activeSource;
                this.activeSource = item;
                this.$emit('change', item, old);
            }
        },
        // 打开查询弹层
        openSearchDrawerInner(item) {
            this.$emit('openSearchDrawer', item);
        }
    }
    , mounted() {
        // 数据区大小监听
        // this.dataBarResizeListener();
        this.dataSourceClick(this.defaultSource);
    }
}, joEl.VUE_COMMON_VIEW, joEl.VUE_COMMON));

joEl.register('lcp-db-query', joEl.buildVueAppParam({
    emits: ['change'],
    props: {
        width: {
            type: String,
            default: '80%'
        }
    },
    data: function () {
        return {
            // 查询抽屉打开状态
            drawerOpen: false,
            // sql输入内容
            sqlInput: '',
            // 数据源
            dataSource: {},
            // 查询参数
            searchCondition: {},
            // 查询结果
            searchResult: {},
            // 加载中标识
            loading: false,
            // 常用sql记录
            hotSqlMark: {
                code: '',//数据源code
                sqlDescList: [
                    // {id: '', title: '', sql: ''},
                ]
            },
            // 常用sql标题输入
            hotSqlTitleInput: '',
            hotSqlTitleInputShow: false,
            // 选中的常用sql
            activeHotSql: ''
        };
    },
    computed: {
        drawerTitle() {
            return '查询窗口【' + this.dataSource.name + '（' + this.dataSource.code + '）】';
        },
        // 表格列
        tableColumns() {
            var arr = [];
            jo.forEach(this.searchResult.headers, item => {
                arr.push({field: item, remark: item});
            });
            if (jo.arrayIsNotEmpty(arr)) {
                return arr;
            }
            return [{field: 'field', remark: 'field'}];
        },
        // 表格数据
        tableData() {
            return this.searchResult.dataList || [];
        },
        // 常用sql集合
        hotSqlList() {
            return this.hotSqlMark.sqlDescList || [];
        },
        // 常用sql缓存key
        hotSqlCacheKey() {
            return 'hotSqlMark_' + this.dataSource.code;
        },
    },
    template: `
        <el-drawer v-model="drawerOpen" :title="drawerTitle" :before-close="beforeClose" :size="width">
            <div v-loading="loading" class="qy-flex-y" style="height: 100%;">
                <div style="margin-bottom: 15px;">
                    <el-button type="primary" @click="runSql">
                        <i class="fa fa-play"></i>&nbsp;执行
                    </el-button>
                </div>
                <div style="margin-bottom: 5px;display: flex;">
                    <el-tag v-for="(item, idx) in hotSqlList" :key="item.id" :closable="true"
                        @close="handleRemoveHotSql(item, idx)"
                        @click="hotSqlClick(item)"
                        style="margin-right: 10px;margin-bottom: 5px;cursor: pointer;"
                    >{{ item.title }}</el-tag>
                      <el-input
                        v-if="hotSqlTitleInputShow"
                        ref="hotSqlTitleInputRef"
                        v-model="hotSqlTitleInput"
                        size="small"
                        @keyup.enter="handleHotSqlTitleInputConfirm"
                        @blur="handleHotSqlTitleInputConfirm"
                        style="width: 80px;margin-bottom: 5px;"
                      />
                      <el-button v-else="" style="width: 80px;margin-bottom: 5px;" size="small" @click="showHotSqlTitleInput">
                        +&nbsp;添加常用
                      </el-button>
                </div>
                <div style="margin-bottom: 15px;">
                    <el-input v-model="sqlInput" :rows="5" type="textarea" placeholder="此处编写查询sql后点击执行即可"></el-input>
                </div>
                <div style="margin-bottom: 15px;">
                    <el-link type="primary" @click="this.sqlInput='show tables'">show tables</el-link>
                    &nbsp;&nbsp;
                    <el-link type="primary" @click="this.sqlInput='desc table_name'">表结构</el-link>
                    &nbsp;&nbsp;
                    <el-link type="primary" @click="this.sqlInput='select * from table_name'">select单表</el-link>
                    &nbsp;&nbsp;
                    <el-link type="primary" @click="this.sqlInput='show index from table_name'">查看索引</el-link>
                </div>
                <div style="border: #dddddd solid 0px;flex: 1;min-height: 1px;">
                    <el-table :data="tableData" header-cell-class-name="jo-el-table-header" height="100%" :border="true" tooltip-effect="dark">
                        <el-table-column prop="tableName" label="#" width="50" fixed="left">
                            <template #default="scope">
                                {{scope.$index + 1}}
                            </template>
                        </el-table-column>
                        <el-table-column v-for="item in tableColumns" :prop="item.field" :label="item.remark" min-width="150" :show-overflow-tooltip="true"></el-table-column>
                    </el-table>
                </div>
            </div>
        </el-drawer>
    `,
    methods: {
        // 常用sql点击
        hotSqlClick(sqlDesc) {
            this.sqlInput = sqlDesc.sql;
            this.activeHotSql = sqlDesc.id;
        },
        // 删除常用sql
        handleRemoveHotSql(sqlDesc, idx) {
            this.hotSqlMark.sqlDescList.splice(idx, 1);
            // 更新缓存
            jo.cache.set(this.hotSqlCacheKey, this.hotSqlMark);
        },
        // 常用sql标题输入后确认
        handleHotSqlTitleInputConfirm() {
            if (this.hotSqlTitleInput && this.sqlInput) {
                this.hotSqlMark.sqlDescList.push({id: jo.getUUID(16), title: this.hotSqlTitleInput, sql: this.sqlInput});
                // 缓存
                jo.cache.set(this.hotSqlCacheKey, this.hotSqlMark);
            }
            this.hotSqlTitleInputShow = false;
            this.hotSqlTitleInput = '';
        },
        // 展示常用sql标题输入
        showHotSqlTitleInput() {
            this.hotSqlTitleInput = '';
            this.hotSqlTitleInputShow = true;
            Vue.nextTick(() => {
                this.$refs.hotSqlTitleInputRef.focus();
            });
        },
        // 执行sql
        runSql() {
            this.loading = true;
            jo.postJson('/lowcode/db/' + this.dataSource.code + '/executeSearchSql', {sql: this.sqlInput}).success(json => {
                this.searchResult = json.data || {};
                this.loading = false;
            }).error(json => {
                this.loading = false;
                jo.showErrorMsg(json.info || '操作失败');
            });
        },
        beforeClose(done) {
            jo.confirm('您确定要关闭此查询窗口嘛?', () => {
                this.invokeBeforeClose();
                done();
            });
        },
        open(dataSource) {
            if (!dataSource || !dataSource.code) {
                console.error('[SQL查询窗口] 打开窗口传入数据源无效', dataSource);
                return;
            }
            this.dataSource = dataSource;
            this.drawerOpen = true;
            // 初始化常用sql
            this.hotSqlMark = jo.cache.get(this.hotSqlCacheKey) || {code: '', sqlDescList: []};
        },
        close() {
            this.invokeBeforeClose();
            this.drawerOpen = false;
        },
        invokeBeforeClose() {
            // 清空查询结果
            this.searchResult = {};
            // 清空查询sql
            this.sqlInput = '';
            // 清空常用sql
            this.hotSqlMark = {code: '', sqlDescList: []};
        }
    },
    mounted() {
        this.initInstance();
        console.info('[lcp-db-query] mounted');
    }
}, joEl.VUE_COMPONENT_BASE_V1));