/*
 * 代码设计器 - 工作态组件
 * 用来定义各个组件在画布区时的表现
 *
 * 命名: lcp-comp-work-组件的code
 */
lcpUtil.register('lcp-comp-work-form_item_input', {
    props: {
        value: {
            type: [String, Number],
            default: ''
        },
    },
    data: function () {
        return {};
    },
});
