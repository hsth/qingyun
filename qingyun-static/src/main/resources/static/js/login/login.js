// 执行登录
function loginProcess(param) {
    if (!param.account) {
        jo.showTipsMsg('请输入账号!');
        return;
    }
    if (!param.password) {
        jo.showTipsMsg('请输入密码!');
        return;
    }
    var encrypt = new JSEncrypt();
    encrypt.setPublicKey(SSO_RSA_PUBLIC_KEY);//公钥
    var account = param.account;//encrypt.encrypt(base64Util.encode(this.account));
    var password = encrypt.encrypt(base64Util.encode(param.password));
    // 发起登录
    jo.postAjax(URL_LOGIN || '/sso/login', {
        account: account,
        password: password,
        redirectTo: decodeURIComponent(jo.getUrlParam('fromUrl'))
    }, function (json) {
        console.info('登录返回结果:', json)
        if (json && json.code == 0) {
            // 保存token令牌
            window.sessionStorage.setItem(LOCAL_STORAGE_KEY_TOKEN, json.data.token);
            // 登录后回调
            if (typeof param.onSuccess == 'function') {
                param.onSuccess(json);
            } else {
                jo.showSuccessMsg('登录成功');
                if (json.data.redirectTo) {
                    console.info('登录返回重定向地址:', json.data.redirectTo);
                    window.location.href = json.data.redirectTo;
                } else {
                    window.location.href = '/?' + REQUEST_DATA_KEY_TOKEN + '=' + json.data.token;
                }
            }
        } else {
            if (typeof param.onError == 'function') {
                param.onError(json);
            } else {
                jo.showErrorMsg(json.info || '登录失败');
            }
        }
    }, true);
}

// 执行注册
function registerProcess(param) {
    if (!param.account) {
        jo.showTipsMsg('请输入账号!');
        return;
    }
    if (!param.password) {
        jo.showTipsMsg('请输入密码!');
        return;
    }
    if (!param.password2) {
        jo.showTipsMsg('请输入确认密码!');
        return;
    }
    if (param.password !== param.password2) {
        jo.showTipsMsg('两次密码输入不一致!');
        return;
    }
    var encrypt = new JSEncrypt();
    encrypt.setPublicKey(SSO_RSA_PUBLIC_KEY);//公钥
    var password = encrypt.encrypt(base64Util.encode(param.password));
    var body = param.user || {};// 用户信息, 用来放除账号密码外的其他字段
    body.account = param.account;
    body.password = password;
    // 发起注册
    jo.postJson('/sso/register', {body: body}).success(function (json) {
        console.info('注册返回结果:', json);
        if (typeof param.onSuccess == 'function') {
            param.onSuccess(json);
        } else {
            jo.showSuccessMsg('注册成功');
        }
    }).error(function (json) {
        if (typeof param.onError == 'function') {
            param.onError(json);
        } else {
            jo.showErrorMsg(json.info || '注册失败');
        }
    });
}

// 执行重置
function resetPwdProcess(param) {
    if (!param.password || !param.password2) {
        return;
    }
    if (param.password !== param.password2) {
        jo.showErrorMsg('两次密码输入不一致!');
        return;
    }
    if (!param.key) {
        return;
    }
    var encrypt = new JSEncrypt();
    encrypt.setPublicKey(SSO_RSA_PUBLIC_KEY);//公钥
    var password = encrypt.encrypt(base64Util.encode(param.password));
    jo.postJson('/sso/resetPwd', {key: param.key, password: password}).success(json => {
        jo.alert('密码重置成功，点击确定去登录！', function () {
            window.location.href = '/page/login.html';
        });
    }).error(json => {
        jo.showErrorMsg(json.info || '操作失败');
    });
}


/**
 * Base64 encode / decode
 * @constructor
 */
function Base64() {

    // private property
    _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

    // public method for encoding
    this.encode = function (input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;
        input = _utf8_encode(input);
        while (i < input.length) {
            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);
            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;
            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }
            output = output +
                _keyStr.charAt(enc1) + _keyStr.charAt(enc2) +
                _keyStr.charAt(enc3) + _keyStr.charAt(enc4);
        }
        return output;
    }

    // public method for decoding
    this.decode = function (input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;
        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
        while (i < input.length) {
            enc1 = _keyStr.indexOf(input.charAt(i++));
            enc2 = _keyStr.indexOf(input.charAt(i++));
            enc3 = _keyStr.indexOf(input.charAt(i++));
            enc4 = _keyStr.indexOf(input.charAt(i++));
            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;
            output = output + String.fromCharCode(chr1);
            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }
        }
        output = _utf8_decode(output);
        return output;
    }

    // private method for UTF-8 encoding
    _utf8_encode = function (string) {
        string = string.replace(/\r\n/g, "\n");
        var utftext = "";
        for (var n = 0; n < string.length; n++) {
            var c = string.charCodeAt(n);
            if (c < 128) {
                utftext += String.fromCharCode(c);
            } else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            } else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }
        return utftext;
    }

    // private method for UTF-8 decoding
    _utf8_decode = function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;
        while (i < utftext.length) {
            c = utftext.charCodeAt(i);
            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            } else if ((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i + 1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            } else {
                c2 = utftext.charCodeAt(i + 1);
                c3 = utftext.charCodeAt(i + 2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }
        }
        return string;
    }
}

var base64Util = new Base64();