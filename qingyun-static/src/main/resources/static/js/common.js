/**
 * 常量相关的属性或者方法放在前面,方便修改
 * Created by Administrator on 2017/6/10.
 */
//常量
var COMMON_CONST = {
    HTML_TREE_LOADING: '<span><i class="fa fa-spinner fa-pulse fa-fw" style="font-family: FontAwesome;"></i> 正在加载...</span>' //树的正在加载html
    ,
    HTML_TREE_LOAD_FAIL: '<span style="color: red;font-weight: 700;"><i class="fa fa-frown-o fa-fw" style="font-family: FontAwesome;"></i> 加载失败！</span>' //树加载失败html
};

/**
 * 替换url常量
 * @param sUrl
 */
function replaceUrlConstants(sUrl) {
    if (!sUrl) {
        return '';
    }
    sUrl = sUrl.replace("{URL_UMS}", URL_UMS);//替换ums域名常量
    sUrl = sUrl.replace("{URL_PORTAL}", URL_PORTAL);//替换门户域名常量
    sUrl = sUrl.replace("{URL_STATIC}", URL_STATIC);//替换静态域名
    sUrl = sUrl.replace("{URL_FS}", URL_FS);//文件系统域名
    sUrl = sUrl.replace("{URL_CMS}", URL_CMS);//内容管理系统域名
    sUrl = sUrl.replace("{URL_CONFIG}", URL_CONFIG);//
    sUrl = sUrl.replace("{URL_MONITOR}", URL_MONITOR);//
    sUrl = sUrl.replace("{contextPath}", contextPath);//当前上下文地址
    sUrl = sUrl.replace("{SSO_TOKEN}", REQUEST_DATA_KEY_TOKEN + '=' + (SSO_TOKEN ? encodeURIComponent(SSO_TOKEN) : ''));//sso令牌

    //在Url后追加时间戳,防止浏览器缓存
    sUrl = sUrl + jo.getLinkSign(sUrl) + "_t=" + new Date().getTime();
    return sUrl;
}

/**
 * 加工请求数据,jo.postAjax和jo.post在发送请求前会调用此方法对传输的数据做处理
 * @param data 待处理数据
 * @param type 类型:post/postAjax
 * @return 处理后的数据
 */
function processRequestData(data, type) {
    if (typeof (data) == "object") {
        //单点参数追加
        if (!data[REQUEST_DATA_KEY_TOKEN] && SSO_TOKEN) {//不存在_token参数时且存在令牌,将sso令牌存入
            data[REQUEST_DATA_KEY_TOKEN] = SSO_TOKEN;
        }
    }
    return data;
}


/**
 * 下载文件
 * @param fileId 文件编号
 */
function downloadFile(fileId) {
    if (jo.isValid(fileId)) {
        jo.confirm("您确定要下载该文件吗?", {title: "下载确认"}, function (idx) {
            jo.post("{URL_FS}fs/file/download", {"fileId": fileId});
            jo.close(idx);
        });
    } else {
        console.error("文件编号无效,无法下载!");
    }
}

/**
 * 删除文件
 * @param fileId 文件编号
 */
function dropFile(fileId) {
    if (jo.isValid(fileId)) {
        jo.confirm("您确定要删除该文件吗?", {title: "删除确认"}, function () {
            jo.postAjax("{URL_FS}fs/file/dropFile", {"fileId": fileId}, function (json) {
                if (json && json.code == "0") {
                    jo.showMsg("成功删除文件!");
                    if (joView.inited) {
                        joView.reloadCurrentPage();
                    }
                }
                if (json && json.code == "-1") {
                    jo.showMsg(json.info);
                }
            });
        });
    } else {
        console.error("文件编号无效,无法删除!");
    }
}

/**
 * 预览文件
 * @param fileId
 */
function previewFile(fileId) {
    if (jo.isValid(fileId)) {
        jo.newWindow("{URL_FS}fs/file/preview/" + fileId);
    } else {
        console.error("文件编号无效,无法预览!");
    }
}

/**
 * 格式化状态显示内容
 * @param state 状态,1/0
 * @returns {*}
 */
function formatYesOrNo(state) {
    if (state == '1') {
        return '<span class="label label-success">是</span>';
    } else if (state == '0') {
        return '<span class="label label-danger">否</span>';
    } else {
        return state;
    }
}


/**
 * 将内容包装为label展示
 * @param val 值
 * @param style 样式,label-primary,label-info,label-success,label-warning,label-danger,label-default
 * @param clickFunc 点击事件
 * @returns {string}
 */
function htmlWrap4Label(val, style, clickFunc) {
    if (clickFunc) {
        clickFunc = clickFunc.replace(/\"/ig, '&quot;').replace(/\'/ig, '&#x27;');
    }
    return '<span class="label' + (clickFunc ? ' cursor-hand' : '') + ' ' + jo.getDefVal(style, 'label-default') + '" '
        + (clickFunc ? (' onclick="' + clickFunc + '"') : '')
        + '>' + val + '</span>';
}

/**
 * 按钮html
 * @param param 具体参数看代码
 * @returns {string}
 */
function htmlWrap4Button(param) {
    var btnTemp = '<button type="button" class="btn ${class}" onclick="${click}"><i class="fa ${icon}" aria-hidden="true"></i>&nbsp;${val}</button>';
    var clickPart = param.click ? param.click.replace(/\"/ig, '&quot;').replace(/\'/ig, '&#x27;') : '';
    return btnTemp
        .replace('${class}', jo.getDefVal(param.class, 'btn-primary'))
        .replace('${click}', jo.getDefVal(clickPart, ''))
        .replace('${icon}', jo.getDefVal(param.icon, 'fa-plus'))
        .replace('${val}', jo.getDefVal(param.val, '--'))
        ;
    // return '<button type="button" class="btn '+param.class+'" '+clickPart+'><i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp;'+param.val+'</button>';
}

/**
 * 枚举类获取枚举数组
 * @returns {*[]}
 */
function enumItems() {
    var arr = [];
    for (var k in this) {
        if (typeof this[k] == 'function') {
            continue;
        }
        arr.push(this[k]);
    }
    return arr;
}
//枚举值生成label
function enumLabelHtml(id) {
    var item = this.of(id);
    if (item.id) {
        return htmlWrap4Label(item.name, item.label);
    }
    return htmlWrap4Label(id);
}
/**
 * 枚举类根据id匹配枚举值
 * @param id
 * @returns {{}|*}
 */
function enumOf(id) {
    var arr = this.items();
    for (var i = 0; i < arr.length; i++) {
        var item = arr[i];
        if (item.id == id) {
            return item;
        }
    }
    return {};
}
// 枚举声明举例, 注意items和of要有
const TEST_ENUM = {
    ON: {id: 1, name: '上线'}
    , items: enumItems
    , of: enumOf
};