var QY_VERSION = "1.0.0";//版本号
var loginUser = "";//current login user info
var contextPath = window.location.protocol + '//' + window.location.host + '/';
contextPath = ('file:///' == contextPath ? '' : contextPath);
var URL_UMS = contextPath;//统一用户域名
var URL_PORTAL = contextPath;//门户域名
var URL_STATIC = contextPath;//静态域名
var URL_FS = contextPath;//文件系统域名
var URL_CMS = contextPath;//内容管理系统域名
var URL_MONITOR = contextPath;//统一监控平台
var URL_CONFIG = contextPath;//统一配置中心
//系统url对象
var SYSURL = {
    contextPath: contextPath
    /*,UMS : URL_UMS
    ,PORTAL : URL_PORTAL
    ,STATIC : URL_STATIC
    ,FS : URL_FS
    ,CMS : URL_CMS
    ,MONITOR : URL_MONITOR
    ,CONFIG : URL_CONFIG*/
    ,URL_UMS : URL_UMS
    ,URL_PORTAL : URL_PORTAL
    ,URL_STATIC : URL_STATIC
    ,URL_FS : URL_FS
    ,URL_CMS : URL_CMS
    ,URL_MONITOR : URL_MONITOR
    ,URL_CONFIG : URL_CONFIG
};
window.UEDITOR_HOME_URL = URL_STATIC + "static/plugin/UEditor/";//富文本编辑器地址
var URL_MENU = "{URL_UMS}ums/navigate/getMenu";//菜单地址
var URL_LOGIN = "{URL_UMS}sso/login";//登录地址
var URL_LOGOUT = "{URL_UMS}sso/logout";//退出登录地址
var URL_SECURED = "{URL_UMS}sso/secured";//安全认证地址
var URL_SECURITY_CODE = "{URL_UMS}sso/securityCode";//刷新验证码地址
var SSO_RSA_PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCfm68MAkMmCfXPmPz/SSwaOrEQo7lxDKTtm21zgTRoKY5+NTeog9rpUUbpEfKaOxRy7KwF/geOfcR0AWSBJHQkt69d+SuUORIty0pchIjUNGclbxj+LCUlZwyCIuGjBk5CklBdi4NRobHgx2sK4Vkn9oZVsBaQ0yLQ+9OJFva5iQIDAQAB";//单点登录RSA加密公钥,用于加密账号密码等敏感数据
var LOCAL_STORAGE_KEY_TOKEN = "SSO_KEY_TOKEN";//单点登录的认证token在本地存储的key
var SSO_TOKEN = '';//令牌,向后端发送请求时携带上:_token=xxxx
var REQUEST_DATA_KEY_TOKEN = 'qy_token';//向后端传输令牌时的key
if(window.sessionStorage && sessionStorage.getItem(LOCAL_STORAGE_KEY_TOKEN)){
    SSO_TOKEN = sessionStorage.getItem(LOCAL_STORAGE_KEY_TOKEN);//取令牌
}
