// tab栏相关属性
var vue_param_tab = {
    data: function () {
        return {
            // main区的页面集合
            mainPageArr: [{
                code: '_home_',
                name: '首页',
                navId: -1,
                href: window.INDEX_HOME_URL || '',
                notClose: true
            }],
            // main区当前打开的页面code
            mainPageSelectedCode: '',
            // tab栏伸缩大小
            tabBarMarginLeft: 0,
        };
    },
    computed: {
        // tab栏展示菜单集合
        mainPageArrComputed() {
            var _this = this;
            var hasSelected = false;
            var arr = jo.array2array(this.mainPageArr, function (item) {
                var one = jo.copyObjectDeep(item);
                // 若没有选中项, 则判断当前项是否选中, 已有选中项时不再判断后续是否选中
                if (!hasSelected) {
                    one.selected = _this.mainPageSelectedCode === one.code;
                }
                // 判断是否有选中项
                if (one.selected) {
                    hasSelected = true;
                }
                return one;
            });
            // 若没有选中项并且存在页面, 则默认选中第一个
            if (!hasSelected && this.mainPageArr.length > 0) {
                this.mainPageArr[0].selected = true;
                arr[0].selected = true;
                this.mainPageSelectedCode = this.mainPageArr[0].code;
            }
            return arr;
        },
        // tab栏伸缩样式
        tabBarStyle() {
            return {'margin-left': (this.tabBarMarginLeft || 0) + 'px'};
        },
        // 当前展示的是第一个tab
        firstTabFlag() {
            return this.mainPageSelectedCode === this.mainPageArr[0].code;
        },
        // 当前展示的是最后一个tab
        lastTabFlag() {
            return this.mainPageSelectedCode === this.mainPageArr[this.mainPageArr.length - 1].code;
        },
    },
    watch: {
        // 选中tab发生变化时, tab栏重新计算伸缩大小
        mainPageSelectedCode() {
            this.triggerTabBarResize();
        }
    },
    methods: {
        // 触发tab伸缩计算
        triggerTabBarResize() {
            this.$nextTick(() => {
                this.tabBarResize();
            });
        },
        // 计算tab栏伸缩尺寸
        tabBarResize() {
            var marginLeft = 0;
            // tab栏可视区域宽度
            var tabBarWidth = $('.qy-index-main-tabs').outerWidth();
            // tab栏内容宽度
            var tabFlexWidth = $('.qy-index-main-tabs-flex').outerWidth();
            // tab栏内容带margin宽度, 如果margin是负数, 会比tabFlexWidth少对应数值
            // var tabFlexMarginWidth = $('.qy-index-main-tabs-flex').outerWidth(true);
            if (tabFlexWidth > tabBarWidth) {
                // 当前选中tab距离左边的距离
                var activeLeft = 0;
                var tabs = $('.qy-index-main-tab-item');
                for (let i = 0; i < tabs.length; i++) {
                    var e = tabs[i];
                    activeLeft += $(e).outerWidth(true) || 0;
                    if ($(e).hasClass('qy-index-main-tab-item-active')) {
                        break;
                    }
                }
                // 当前tab需要的空间大于可视区域的话, 则需要进行位移
                if (activeLeft > tabBarWidth) {
                    for (let i = 0; i < tabs.length; i++) {
                        var e = tabs[i];
                        marginLeft += $(e).outerWidth(true) || 0;
                        if (activeLeft - marginLeft <= tabBarWidth) {
                            break;
                        }
                    }
                }
            }
            console.info('[tab栏伸缩计算] 当前伸缩与重新计算后伸缩:', this.tabBarMarginLeft, -marginLeft);
            if (-marginLeft !== this.tabBarMarginLeft) {
                this.tabBarMarginLeft = -marginLeft;
            }
        },
        // 关闭左侧tab
        closeLeftTab() {
            var idx = this.tabIndex();
            if (idx > 1) {
                this.mainPageArr.splice(1, idx - 1);
                this.triggerTabBarResize();
            }
        },
        // 关闭右侧tab
        closeRightTab() {
            var idx = this.tabIndex();
            if (idx < this.mainPageArr.length - 1) {
                this.mainPageArr.splice(idx + 1, this.mainPageArr.length - idx);
                this.triggerTabBarResize();
            }
        },
        // 关闭其他tab
        closeOtherTab() {
            // 关闭右侧
            this.closeRightTab();
            // 关闭左侧
            this.closeLeftTab();
        },
        // 关闭全部tab
        closeAllTab() {
            if (this.mainPageArr.length > 1) {
                if (!this.firstTabFlag) {
                    // 选中第一个
                    this.tabSelected(this.mainPageArr[0]);
                }
                // 删除第一个之外的所有
                this.mainPageArr.splice(1, this.mainPageArr.length - 1);
                this.triggerTabBarResize();
            }
        },
        // 切换上一个tab
        lastTab() {
            if (!this.firstTabFlag) {
                var idx = this.tabIndex();
                if (idx > 0) {
                    this.tabSelected(this.mainPageArr[idx - 1]);
                }
            }
        },
        // 切换下一个tab
        nextTab() {
            if (!this.lastTabFlag) {
                var idx = this.tabIndex();
                if (idx < this.mainPageArr.length - 1) {
                    this.tabSelected(this.mainPageArr[idx + 1]);
                }
            }
        },
        // 计算当前tab的下标
        tabIndex() {
            for (let i = 0; i < this.mainPageArr.length; i++) {
                var tab = this.mainPageArr[i];
                if (tab.code === this.mainPageSelectedCode) {
                    return i;
                }
            }
            return 0;
        },
        // tab点击
        tabClick(tab) {
            this.tabSelected(tab);
        },
        // tab选中
        tabSelected(tab) {
            this.mainPageSelectedCode = tab.code;
            // 当tab关联菜单id有效时, 设置选中菜单
            if (tab.navId) {
                this.activeNav.value = tab.navId;
            }
        },
        // 刷新tab页面
        refreshTabPage(tab) {
            var tabWin = window[tab.code];
            if (tabWin) {
                tabWin.location.reload();
            } else {
                console.warn('刷新tab页面失败,找不到对应的window对象:', tab);
            }
        },
        // tab关闭
        tabClose(tab) {
            var idx = -1;// 被关闭tab在集合中的位置
            for (let i = 0; i < this.mainPageArr.length; i++) {
                var item = this.mainPageArr[i];
                if (item.code === tab.code) {
                    idx = i;
                    break;
                }
            }

            if (idx > -1) {
                // 从main区页面集合中移除
                this.mainPageArr.splice(idx, 1);
                // 联动左侧菜单栏取消高亮
                if (this.activeNav.value === tab.navId) {
                    this.activeNav.value = '';
                }
                // 若关闭的是当前正在显示的, 则关闭后默认打开后面相邻的页面(后面没有则打开前面)
                if (tab.code === this.mainPageSelectedCode) {
                    if (this.mainPageArr.length > 0) {
                        if (this.mainPageArr.length > idx) {
                            // 集合最大索引大于等于被删索引, 说明有下一个tab
                            this.tabSelected(this.mainPageArr[idx]);
                        } else if (this.mainPageArr.length > (idx - 1)) {
                            // 选中上一个tab
                            this.tabSelected(this.mainPageArr[idx - 1]);
                        } else {
                            this.tabSelected(this.mainPageArr[0]);
                            console.warn('tab关闭后的默认打开tab选取异常(理论上走不到这里),降级选择第一个tab:', tab);
                        }
                    } else {
                        console.info('最后一个tab被关闭:', tab);
                    }
                } else {
                    this.triggerTabBarResize();
                }
            } else {
                console.warn('找不到待关闭的tab:', tab);
            }
        },
    }
};
// 菜单栏相关属性
var vue_param_menu = {
    data: function () {
        return {
            // 菜单集合
            navs: [
                {},
            ],
            // 选中的菜单项, value: 选中的菜单id, openNavMap: 节点的子菜单展开状态,key是id,value是布尔值(是否展开)
            activeNav: {
                value: '',
                openNavMap: {}
            },

            // 左侧边栏展示标识
            leftBarShow: true,
        };
    },
    computed: {
        // 左边栏样式
        leftBarStyle() {
            var style = {};
            if (!this.leftBarShow) {
                style.width = '0px';
            }
            return style;
        },
    },
    methods: {
        // 左侧栏控制
        leftBarControl() {
            this.leftBarShow = !this.leftBarShow;
        },
        // 加载菜单
        loadMenu() {
            var _this = this;
            jo.postJson('/ums/menu/getMenuTree', {}).success(function (json) {
                _this.navs = json.data || [];
            });
        },
        // 菜单点击打开
        openPageOnMain(nav) {
            if (nav.href) {
                var page = {code: nav.code, name: nav.name, navId: nav.id, href: nav.href};
                if (!page.code) {
                    page.code = 'page_' + jo.getUUID();
                }
                var map = jo.array2Object(this.mainPageArr, function (item) {
                    return item.code;
                });
                // 若没有打开则添加新页面
                if (!map[page.code]) {
                    this.mainPageArr.push(page);
                }
                // 选中
                this.mainPageSelectedCode = page.code;
            }
        },
        // 菜单栏展开控制
        navOpenControl(nav) {
            if (nav) {
                // 兄弟节点
                var broList = [];
                if (nav.parentId === 0) {
                    // 父节点为0时, 关闭同级根节点
                    broList = this.navs;
                } else {
                    // 菜单map
                    var map = jo.array2Object(this.navs, function (item) {
                        return item.id;
                    });
                    // 找到父节点
                    var parent = map[nav.parentId];
                    // 父节点下的其他兄弟节点关闭
                    if (parent) {
                        broList = parent.children;
                    }
                }
                if (broList) {
                    jo.forEach(broList, (child) => {
                        if (child.id !== nav.id && this.activeNav.openNavMap[child.id]) {
                            this.activeNav.openNavMap[child.id] = false;
                            // console.info('收起同级兄弟节点:', child);
                        }
                    });
                }
            }
        },
    }
};
// 全局配置相关属性
var vue_param_config = {
    data: function () {
        return {
            // 全局配置实例
            globalConfigIns: {},
            // 全局配置数据对象
            globalConfigData: {},
        };
    },
    computed: {},
    methods: {
        // 打开全局配置
        openGlobalConfig() {
            this.globalConfigIns.command.open();
        }
    }
};
// 首页main页面tab
joEl.register('qy-index-main-tab', {
    emits: ['tabClick', 'tabClose'],
    props: {
        tab: {
            type: Object,
            required: true
        }
    },
    data: function () {
        return {};
    },
    computed: {},
    template: `
        <div class="qy-index-main-tab-item" :class="{ 'qy-index-main-tab-item-active' : tab.selected }" @click="tabClickInner">
            <div class="qy-index-main-tab-name">{{tab.name}}</div>
            <div v-if="!tab.notClose" class="qy-index-main-tab-close" @click.stop="tabCloseInner"><i class="fa fa-close"></i></div>
        </div>
    `,
    methods: {
        // 点击
        tabClickInner() {
            // console.info('tab 点击:', this.tab);
            this.$emit('tabClick', this.tab);
        },
        // 关闭tab
        tabCloseInner() {
            this.$emit('tabClose', this.tab);
        }
    },
    mounted() {
    }
});
// 菜单组件
joEl.register('qy-index-nav', {
    emits: ['navClick', 'navOpen'],
    props: {
        nav: {
            type: Object,
            default: function () {
                return {};
            }
        },
        level: {
            type: Number,
            default: 1
        },
        // 选择菜单id
        activeIdRef: {
            type: Object,
            default: function () {
                return {value: '', openNavMap: {}};
            }
        },
        // 菜单项高度, 用来计算子菜单栏最大高度
        navHeight: {
            type: Number,
            default: 42
        },
    },
    data: function () {
        return {};
    },
    computed: {
        // 是否有子菜单
        hasChildFlag() {
            return this.nav.children && this.nav.children.length > 0
        },
        // 是否展开子菜单
        openChildFlag() {
            var open = this.activeIdRef.openNavMap || {};
            return !!open[this.nav.id];
        },
        // 子菜单等级
        childLevel() {
            return this.level + 1;
        },
        // 子节点最大高度, 为了实现展开收起的动画, 使用max-height, +20是为了容错部分高度误差
        childMaxHeight() {
            return this.openChildFlag ? (this.countNav(this.nav.children) * this.navHeight + 20) + 'px' : '0px';
        },
        // 菜单等级对应的样式, 主要是左侧缩进
        levelClass() {
            return 'qy-nav-item-n' + this.level;
        },
        // 是否选中
        activeFlag() {
            return this.nav.id === this.activeIdRef.value;
        },
        // 菜单class属性
        navClass() {
            var c = [this.levelClass];
            if (this.activeFlag) {
                c.push('qy-nav-item-active');
            } else {
                // 判断选中节点是否子孙节点
                var activeId = this.activeIdRef.value;
                var activeIsChild = function (childList) {
                    if (childList && childList.length > 0) {
                        for (let i = 0; i < childList.length; i++) {
                            var item = childList[i];
                            if (item.id === activeId) {
                                return true;
                            }
                            if (activeIsChild(item.children)) {
                                return true;
                            }
                        }
                    }
                    return false;
                }
                if (activeIsChild(this.nav.children)) {
                    // 高亮选中节点的祖先节点
                    c.push('qy-nav-item-active-parent');
                }
            }
            return c;
        },
    },
    template: `
        <div class="qy-nav-wrap">
            <div class="qy-nav-item" :class="navClass" @click="navClickInner">
                <div class="qy-nav-item-icon">
                    <i v-if="nav.icon" class="fa" :class="nav.icon"></i>
                    <i v-else="" class="fa fa-list-ul"></i>
                </div>
                <div class="qy-nav-item-name">{{nav.name}}</div>
                <div v-if="hasChildFlag" class="qy-nav-item-arrow">
                    <i v-if="openChildFlag" class="fa fa-angle-up"></i>
                    <i v-else="" class="fa fa-angle-down"></i>
                </div>
            </div>
            <div v-if="hasChildFlag" class="qy-nav-child-wrap" :class="{ 'qy-nav-child-wrap-close' : !openChildFlag }" :style="{ 'maxHeight' : childMaxHeight }">
                <qy-index-nav v-for="child in nav.children" :nav="child" :level="childLevel" :active-id-ref="activeIdRef" 
                    :nav-height="navHeight" @nav-click="navClickDispatch" @nav-open="navOpenDispatch"></qy-index-nav>
            </div>
        </div>
    `,
    methods: {
        // 菜单点击 v-show="openChildFlag"
        navClickInner() {
            // console.debug('菜单点击:', this.nav);
            if (this.hasChildFlag) {
                // 当有子节点时, 控制展开/收起
                if (this.openChildFlag) {
                    this.childClose(this.nav);
                } else {
                    this.childOpen(this.nav);
                }
            } else {
                // 链接有效, 设置选中项, 不支持反选
                if (this.nav.href) {
                    this.activeIdRef.value = this.nav.id;
                    this.navClickDispatch(this.nav);
                }
            }
        },
        // 菜单点击事件转发
        navClickDispatch(nav) {
            this.$emit('navClick', nav);
        },
        // 展开子节点
        childOpen() {
            if (!this.activeIdRef.openNavMap) {
                this.activeIdRef.openNavMap = {};
            }
            this.activeIdRef.openNavMap[this.nav.id] = true;

            // 当展开一个节点时, 关闭其他同级节点
            this.navOpenDispatch(this.nav);
        },
        // 菜单打开事件转发
        navOpenDispatch(nav) {
            this.$emit('navOpen', nav);
        },
        // 关闭子节点
        childClose() {
            if (!this.activeIdRef.openNavMap) {
                this.activeIdRef.openNavMap = {};
            }
            this.activeIdRef.openNavMap[this.nav.id] = false;
        },
        // 菜单数量
        countNav(navs) {
            var count = function (list) {
                var num = 0;
                jo.forEach(list, function (item) {
                    num++;
                    if (item.children && item.children.length > 0) {
                        num += count(item.children);
                    }
                });
                return num;
            }
            return Array.isArray(navs) ? count(navs) : count(navs.children) + 1;
        }
    },
    mounted() {
        console.info('[qy-index-nav] mounted');
    }
});
joEl.register('qy-index-global-config', joEl.buildVueAppParam({
    emits: [],
    props: {
        config: {
            type: Object,
            default: function () {
                return {};
            }
        }
    },
    data: function () {
        return {
            // 加载中状态
            loading: false,
            // 弹层打开状态
            drawerOpen: false,
            // 弹层标题
            drawerTitle: '全局风格设置',
            // 弹层宽度
            width: '280px',
            // 主题
            skins: [
                {name: '动感主题', code: 'qy-skin-blue'},
                {name: '明亮主题', code: 'qy-skin-default'},
                {name: '暗黑主题', code: 'qy-skin-dark'},
            ],
            // 当前主题
            activeSkin: document.body.getAttribute('skin')
        };
    },
    computed: {
        skinsComputed() {
            var arr = jo.array2array(this.skins, item => {
                var skin = {
                    name: item.name,
                    code: item.code,
                    demoClass: [item.code],
                    active: this.activeSkin === item.code
                };
                if (skin.active) {
                    skin.demoClass.push('qy-skin-option-item-active');
                }
                return skin;
            });
            return arr;
        }
    },
    template: `
        <el-drawer v-model="drawerOpen" :title="drawerTitle" :before-close="beforeClose" :size="width">
            <div v-loading="loading" class="qy-flex-y" style="height: 100%;">
                <div class="qy-skin-options">
                    <div v-for="skin in skinsComputed" @click="selectedSkin(skin)" :title="skin.name" :key="skin.code" class="qy-skin-option-item" :class="skin.demoClass">
                        <div class="qy-skin-demo-header"></div>
                        <div class="qy-skin-demo-body">
                            <div class="qy-skin-demo-left"></div>
                            <div class="qy-skin-demo-right">
                                <i class="fa fa-check" v-if="skin.active"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </el-drawer>
    `,
    methods: {
        // 选择主题
        selectedSkin(skin) {
            if (skin && skin.code !== this.activeSkin) {
                this.activeSkin = skin.code;
                document.body.setAttribute('skin', skin.code);
            }
        },
        // 打开弹层
        open() {
            this.drawerOpen = true;
        },
        // 关闭弹层
        close() {
            this.invokeBeforeClose();
            this.drawerOpen = false;
        },
        // 弹层关闭钩子
        beforeClose(done) {
            // jo.confirm('您确定要关闭此窗口嘛?', () => {
            this.invokeBeforeClose();
            done();
            // });
        },
        // 执行关闭前处理
        invokeBeforeClose() {

        }
    },
    mounted() {
        this.initInstance();
        console.info('[qy-index-global-config] mounted');
    }
}, joEl.VUE_COMPONENT_BASE_V1));
const app = Vue.createApp(joEl.buildVueAppParam({
    data: function () {
        return {
            // 修改密码弹层实例对象
            updatePwdLayerIns: {},
        };
    }
    , computed: {
        // 语言选项
        langOptions() {
            var arr = [];
            for (var k in joEl.i18n_map) {
                arr.push({key: k, name: joEl.i18n_map[k].name || k});
            }
            return arr;
        },
        // 当前选中的语言
        activeLangKey() {
            return this.qy_i18n_key;
        }
    }
    , methods: {
        // 语言切换
        langChange(lang) {
            if (lang && lang.key) {
                // joEl.useI18n(lang.key);
                this.qy_use_i18n(lang.key);
            }
        },
        // 全屏
        fullScreen() {
            if (document.fullscreen) {
                jo.exitScreen();
            } else {
                jo.fullScreen();
            }
        },
        // 修改密码
        modifyPassword() {
            this.updatePwdLayerIns.command.open();
        },
        // 退出登录
        logout() {
            jo.confirm('您是否确定要退出登录?', ()=>{
                jo.postAjax(URL_LOGOUT || '/sso/logout', {fromUrl: decodeURI(window.location.href)}, json => {
                    if (json.code == 0) {
                        window.location.href = json.data.redirectTo;
                    } else {
                        jo.showErrorMsg(json.info || '操作失败');
                    }
                }, true);
            });
        }
    }
    , mounted() {
        this.qy_page_loaded();
        // 加载菜单
        this.loadMenu();
    }
}, vue_param_menu, vue_param_tab, vue_param_config));
// ElementPlus组件
app.use(ElementPlus, {locale: ElementPlusLocaleZhCn});
// 平台组件
app.use(joEl);
// 实例化
var appVM = app.mount("#app");