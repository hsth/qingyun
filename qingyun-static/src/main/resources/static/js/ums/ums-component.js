(function () {
    // 给用户分配角色弹层
    joEl.register('ums-user-bind-role-layer', joEl.buildVueAppParam({
        emits: [],
        props: {},
        data: function () {
            return {
                showDialog: false,
                dialogWidth: '400px',
                title: '授权',

                treeData: [],
                userId: '',
                defaultCheckedKeys: [],
            };
        },
        computed: {},
        template: `
            <el-dialog v-model="showDialog" :title="title" :width="dialogWidth"
            :before-close="close">
                <el-tree ref="userBindRoleTree"
                         :data="treeData"
                         :props="{ label : 'name' , children : 'children' }"
                         :show-checkbox="true"
                         :check-on-click-node="true"
                         node-key="id"
                         :expand-on-click-node="true"
                         :check-strictly="true"
                         @check="check"
                         @check-change="checkChange"
                         :default-checked-keys="defaultCheckedKeys"
                >
                    <template #default="scope">
                        {{scope.data.name}}（{{scope.data.code}}）   
                    </template>
                </el-tree>
                <template #footer>
                    <div>
                        <slot name="footer">
                            <el-button @click="close">取消</el-button>
                            <el-button type="primary" @click="ok">确认</el-button>
                        </slot>
                    </div>
                </template>
            </el-dialog>
        `,
        methods: {
            check(nodeData, status) {
            },
            checkChange(nodeData, checked, hasCheckedChildren) {
            },
            // 打开授权弹层
            open(userId) {
                if (!userId) {
                    return;
                }
                this.userId = userId;
                this.treeData = [];
                this.showDialog = true;
                this.loadData(userId);
            },
            // 关闭弹层
            close() {
                this.userId = '';
                this.defaultCheckedKeys = [];
                this.showDialog = false;
            },
            // 确认授权
            ok() {
                var tree = this.$refs['userBindRoleTree'];
                var roles = tree.getCheckedKeys();
                jo.confirm('您确定要分配选中权限吗?', () => {
                    jo.postJson('/ums/umsAuth/userBindRole', {userId: this.userId, roleIdList: roles}).success(json=>{
                        jo.showSuccessMsg('分配成功~');
                        // 关闭弹层
                        this.close();
                    });
                });
            },
            // 加载选中角色的资源树数据
            loadData(roleId) {
                jo.postJson('/ums/umsAuth/getUserBindRoleList', roleId).success((json) => {
                    this.treeData = json.data || [];
                    // 默认选中
                    var checkedArr = [];
                    jo.forEach(json.data, item=>{
                        if (item.checked) {
                            checkedArr.push(item.id);
                        }
                    });
                    this.defaultCheckedKeys = checkedArr;
                });
            }
        },
        mounted() {
            // 初始化实例对象
            this.initInstance();
            console.info('[ums-user-bind-role-layer] mounted');
        }
    }, joEl.VUE_COMPONENT_BASE_V1));

    // 角色授权弹层
    joEl.register('ums-auth-to-role-layer', joEl.buildVueAppParam({
        emits: [],
        props: {},
        data: function () {
            return {
                showDialog: false,
                dialogWidth: '400px',
                title: '授权',
                resourceTreeData: [],
                roleId: '',
                defaultCheckedKeys: [],
            };
        },
        computed: {},
        template: `
            <el-dialog v-model="showDialog" :title="title" :width="dialogWidth"
            :before-close="closeBefore">
                <el-tree ref="authToRoleTree"
                         :data="resourceTreeData"
                         :props="{ label : 'name' , children : 'children' }"
                         :show-checkbox="true"
                         :check-on-click-node="false"
                         node-key="id"
                         :expand-on-click-node="true"
                         :check-strictly="true"
                         @check="check"
                         @check-change="checkChange"
                         :default-checked-keys="defaultCheckedKeys"
                >
                </el-tree>
                <template #footer>
                    <div>
                        <slot name="footer">
                            <el-button @click="close">取消</el-button>
                            <el-button type="primary" @click="ok">确认</el-button>
                        </slot>
                    </div>
                </template>
            </el-dialog>
        `,
        methods: {
            check(nodeData, status) {
                // console.info('check', arguments)
            },
            checkChange(nodeData, checked, hasCheckedChildren) {
                var tree = this.$refs['authToRoleTree'];
                if (nodeData.children && nodeData.children.length > 0) {
                    jo.forEach(nodeData.children, item => {
                        tree.setChecked(item.id, checked, true);
                    });
                }
                console.debug('checkChange', nodeData);
            },
            // 打开授权弹层
            open(roleId) {
                if (!roleId) {
                    return;
                }
                this.resourceTreeData = [];
                this.showDialog = true;
                this.loadResourceTreeData(roleId);
            },
            // 关闭弹层
            close(done) {
                this.showDialog = false;
                this.closeClean();
            },
            closeBefore(done) {
                done();
                this.closeClean();
            },
            closeClean() {
                this.roleId = '';
                // this.resourceTreeData = [];
                this.defaultCheckedKeys = [];
            },
            // 确认授权
            ok() {
                var tree = this.$refs['authToRoleTree'];
                var resources = tree.getCheckedKeys();
                jo.confirm('您确定要分配选中权限吗?', () => {
                    jo.postJson('/ums/umsAuth/authToRole', {roleId: this.roleId, resourceIdList: resources}).success(json=>{
                        jo.showSuccessMsg('授权成功~');
                        // 关闭弹层
                        this.close();
                    });
                });
            },
            // 加载选中角色的资源树数据
            loadResourceTreeData(roleId) {
                jo.postJson('/ums/umsAuth/getRoleAuthResourceTree', roleId).success((json) => {
                    this.resourceTreeData = json.data || [];
                    this.roleId = roleId;
                    // 默认选中
                    var checkedArr = [];
                    var func = function (list, arr) {
                        jo.forEach(list, item=>{
                            if (item.checked) {
                                arr.push(item.id);
                            }
                            if (item.children) {
                                func(item.children, arr);
                            }
                        });
                    }
                    func(json.data, checkedArr);
                    this.defaultCheckedKeys = checkedArr;
                });
            }
        },
        mounted() {
            // 初始化实例对象
            this.initInstance();
            console.info('[ums-auth-to-role-layer] mounted');
        }
    }, joEl.VUE_COMPONENT_BASE_V1));

    // 角色绑定用户弹层
    joEl.register('ums-role-bind-user-layer', joEl.buildVueAppParam({
        emits: [],
        props: {},
        data: function () {
            return {
                showDialog: false,
                dialogWidth: '80%',
                title: '关联用户',
                roleId: '',
                tableDataUrl: '/ums/umsUser/getPage',
                joTableRef: 'bindUserTable',
                searchCondition: {},
                companyIdOptions: [],
                moreConditionFlag: false,
            };
        },
        computed: {},
        template: `
            <el-dialog v-model="showDialog" :title="title" :width="dialogWidth" 
            :before-close="closeBefore">
                <div>
                    <el-form :inline="true" :model="searchCondition">
                        <el-form-item label="用户ID"  >
                            <el-input v-model="searchCondition.id" placeholder=""></el-input>
                        </el-form-item>
                        <el-form-item label="姓名"  >
                            <el-input v-model="searchCondition.name" placeholder=""></el-input>
                        </el-form-item>
                        <el-form-item label="所属单位"  v-if="moreConditionFlag" >
                            <el-tree-select v-model="searchCondition.companyId" :clearable="true"
                                            :data="companyIdOptions" node-key="id" :props="{label:'name',children:'children'}"
                                    :check-strictly="true"
                                            :render-after-expand="false">
                            </el-tree-select>
                        </el-form-item>
                        <el-form-item>
                            <el-button type="primary" link @click="moreConditionFlag = true" v-if="!moreConditionFlag">更多</el-button>
                            <el-button type="primary" link @click="moreConditionFlag = false" v-if="moreConditionFlag">收起</el-button>
                        </el-form-item>
                        <el-form-item>
                            <el-button type="primary" @click="list_search"><i class="fa fa-search" aria-hidden="true"></i>&nbsp;查询</el-button>
                        </el-form-item>
                    </el-form>
                </div>
                <div>
                    <jo-el-table :url="tableDataUrl" ref="bindUserJoTable" table-ref="bindUserTable"
                    :search-param="searchCondition" first-column="checkbox" :page-size="10" :enable-loading="true" >
                        <template #default="scope">
                            <el-table ref="bindUserTable" :data="scope.data" header-cell-class-name="jo-el-table-header"
                                      :tree-props="{ children: 'children' }"
                                      @sort-change="list_table_sort">
                                <el-table-column type="selection" prop="selection" width="50" fixed="left" label="#" header-align="center" align="center"></el-table-column>
                                <el-table-column prop="id" label="用户ID" min-width="100" header-align="left" align="left"  sortable="custom" ></el-table-column>
                                <el-table-column prop="name" label="姓名" min-width="180" header-align="left" align="left"   ></el-table-column>
                                <el-table-column prop="account" label="账号" min-width="180" header-align="left" align="left"   ></el-table-column>
                                <el-table-column prop="createTime" label="创建时间" min-width="155" header-align="left" align="left"  sortable="custom" ></el-table-column>
                            </el-table>
                        </template>
                    </jo-el-table>
                </div>
                
                <template #footer>
                    <div>
                        <slot name="footer">
                            <el-button @click="close">取消</el-button>
                            <el-button type="primary" @click="ok">确认</el-button>
                        </slot>
                    </div>
                </template>
            </el-dialog>
        `,
        methods: {
            // 列表查询
            list_search() {
                this.$refs['bindUserJoTable'].goPage();
            },
            // 排序{ column, prop, order }
            list_table_sort(sort) {
                this.$refs['bindUserJoTable'].tableSortChange(sort);
            },
            // 打开授权弹层
            open(roleId) {
                if (!roleId) {
                    return;
                }
                this.showDialog = true;
                this.roleId = roleId;
                if (this.$refs['bindUserJoTable']) {
                    this.$refs['bindUserJoTable'].goPage();
                }
                // 单位树
                if (jo.arrayIsEmpty(this.companyIdOptions)) {
                    jo.postJson('/ums/umsOrg/getCompanyTree').success(json=>{
                        this.companyIdOptions = json.data||[];
                    });
                }
            },
            // 关闭弹层
            close() {
                this.showDialog = false;
                this.closeClean();
            },
            closeBefore(done) {
                this.closeClean();
                done();
            },
            closeClean() {
                this.roleId = '';
                this.searchCondition = {};
            },
            // 确认授权
            ok() {
                var userIds = [];
                jo.forEach(this.$refs['bindUserTable'].getSelectionRows(), item=>{
                    userIds.push(item.id);
                });
                if (jo.arrayIsEmpty(userIds)) {
                    jo.showTipsMsg('请选择待关联用户!');
                    return;
                }
                jo.confirm('您确定关联选中用户吗?', () => {
                    jo.postJson('/ums/umsAuth/roleBindUser', {roleId: this.roleId, userIdList: userIds}).success(json=>{
                        jo.showSuccessMsg('关联成功~');
                        // 关闭弹层
                        this.close();
                    });
                });
            },
        },
        mounted() {
            // 初始化实例对象
            this.initInstance();
            console.info('[ums-role-bind-user-layer] mounted');
        }
    }, joEl.VUE_COMPONENT_BASE_V1));

    // 修改密码弹层
    joEl.register('ums-sso-update-pwd-layer', joEl.buildVueAppParam({
        emits: [],
        props: {},
        data: function () {
            return {
                modifyPwdDialogIns: {},
                modifyPwdRules: {
                    oldPwd: [
                        joEl.rules.required('原密码不允许为空', 'blur'),
                    ],
                    newPwd: [
                        joEl.rules.required('新密码不允许为空', 'blur'),
                        joEl.rules.range('密码长度限制为6~24', 'blur', 6, 24),
                    ],
                    newPwd2: [
                        joEl.rules.required('确认密码不允许为空', 'blur'),
                        joEl.rules.range('密码长度限制为6~24', 'blur', 6, 24),
                    ],
                }
            };
        },
        computed: {},
        template: `
            <jo-el-dialog-form :instance="modifyPwdDialogIns"
                           title="修改密码"
                           width="450px"
                           label-position="right"
                           label-width="100px"
                           :rules="modifyPwdRules"
                           @ok="modifyPasswordSubmit"
            >
                <template #default="scope">
                    <el-row :gutter="15">
                        <el-col :span="24">
                            <el-form-item label="旧密码" prop="oldPwd">
                                <el-input v-model="scope.data.oldPwd" type="password" placeholder="请输入旧密码"></el-input>
                            </el-form-item>
                        </el-col>
                        <el-col :span="24">
                            <el-form-item label="新密码" prop="newPwd">
                                <el-input v-model="scope.data.newPwd" type="password" placeholder="请输入新密码"></el-input>
                            </el-form-item>
                        </el-col>
                        <el-col :span="24">
                            <el-form-item label="确认密码" prop="newPwd2">
                                <el-input v-model="scope.data.newPwd2" type="password" placeholder="请再次输入新密码"></el-input>
                            </el-form-item>
                        </el-col>
                    </el-row>
                </template>
            </jo-el-dialog-form>
        `,
        methods: {
            open() {
                this.modifyPwdDialogIns.command.open();
            },
            // 确认修改密码
            modifyPasswordSubmit(data, command) {
                if (jo.assert.valid(data.oldPwd, '请输入旧密码')
                    && jo.assert.valid(data.newPwd, '请输入新密码')
                    && jo.assert.valid(data.newPwd2, '请输入确认密码')
                    && jo.assert.valid(data.newPwd === data.newPwd2, '两次密码输入不一致')) {

                    jo.postJson('/sso/updatePassword', {oldPwd: jo.rsa_public_encrypt(data.oldPwd), newPwd: jo.rsa_public_encrypt(data.newPwd)})
                        .success(json => {
                            jo.showSuccessMsg('修改成功!');
                            command.close();
                        })
                        .error(json => {
                            jo.showErrorMsg(json.info || '操作失败');
                        });
                }
            }
        },
        mounted() {
            // 初始化实例对象
            this.initInstance();
            console.info('[ums-sso-update-pwd-dialog] mounted');
        }
    }, joEl.VUE_COMPONENT_BASE_V1));
})();