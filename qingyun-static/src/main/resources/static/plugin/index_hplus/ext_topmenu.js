
//通过遍历给菜单项加上data-index属性
function loadMenuIndex(){
    $(".J_menuItem").each(function (index) {
        if (!$(this).attr('data-index')) {
            $(this).attr('data-index', index);
        }
    });
}

//根据菜单层级获取类样式
function getClassByLevel(level){
    if(level == 1){
        console.warn("[菜单初始化] 菜单层级存在异常(通常不存在1级子菜单): "+level);
        return "nav-second-level";
    }else if(level == 2){
        return "nav-second-level";
    }else if(level == 3){
        return "nav-third-level";
    }else if(level == 4){
        return "nav-fourth-level";
    }else if(level == 5){
        return "nav-fifth-level";
    }
    console.warn("[菜单初始化] 菜单层级存在异常(通常在2~5): "+level);
    //默认返回五级菜单样式
    return "nav-fifth-level";
}
//将平台带函数的链接转为纯url
function parseHref(href){
    if(href){
        href = href.trim();//去除两边空格
        if(href.indexOf("openPageOnMain") == 0){//右侧打开方法
            href = href.replace("openPageOnMain('", "");
            href = href.replace("')", "");
        }
        if(href.indexOf("newWindow") == 0){//新窗口方法
            href = href.replace("newWindow('", "");
            href = href.replace("')", "");
        }
        return href;
    }else{
        return "";
    }
}
//生成菜单元html,参数1:菜单对象;参数2:菜单层级
function getNavUnitHtml(menu, level, liClass, notChild, menuId){
    var _html = '';
    if(menu){
        _html += '<li'+(liClass ? ' class="' + liClass + '"' : '')+''+(menuId ? ' menuId="' + menuId + '"' : '')+'>';
        if(menu.href){//链接有效,则使用J_menuItem,可点击状态
            if(menu.href.indexOf("newWindow") == 0){//新窗口打开
                _html += '<a target="_blank" href="'+parseHref(menu.href)+'">';
            }else{
                _html += '<a class="J_menuItem" href="'+parseHref(menu.href)+'">';
            }
        }else{//链接无效
            _html += '<a href="javascript:;">';
        }
        _html += '<i class="fa '+menu.icon+' fa-fw"></i> ' +
            '<span class="nav-label">'+menu.name+'</span>';
        if(!notChild && menu.children && menu.children.length > 0){//存在子菜单
            _html += '<span class="fa arrow"></span>' +
                '</a>';
            _html += '<ul class="nav '+getClassByLevel(level+1)+'">';
            for(var i=0;i<menu.children.length;i++){
                _html += getNavUnitHtml(menu.children[i], level+1);
            }
            _html += '</ul>';
        }else{//没有子菜单
            _html += '</a>';
        }
        _html += '</li>';
    }
    return _html;
}

//tab方式打开导航
function openPageOnTabs(sUrl, sName, sIndex) {
    // 获取标识数据
    var dataUrl = sUrl,
        dataIndex = sIndex || new Date().getTime(),
        menuName = sName,
        flag = true;
    if (dataUrl == undefined || $.trim(dataUrl).length == 0)return false;

    // 选项卡菜单已存在,则将其显示
    $('.J_menuTab').each(function () {
        if ($(this).data('id') == dataUrl) {
            if (!$(this).hasClass('active')) {
                $(this).addClass('active').siblings('.J_menuTab').removeClass('active');
                scrollToTab(this);
                // 显示tab对应的内容区
                $('.J_mainContent .J_iframe').each(function () {
                    if ($(this).data('id') == dataUrl) {
                        $(this).show().siblings('.J_iframe').hide();
                        return false;
                    }
                });
            }
            flag = false;
            return false;
        }
    });

    // 选项卡菜单不存在,则创建iframe
    if (flag) {
        var str = '<a href="javascript:;" class="active J_menuTab" data-id="' + dataUrl + '">' + menuName + ' <i class="fa fa-times-circle"></i></a>';
        $('.J_menuTab').removeClass('active');
        var height = $(window).height() - 110;
        //为了兼容平台,在这里对url做解析处理,因为url用来作为data-id,所以不能提前进行解析,在生产iframe时使用解析后的url
        var dataUrl2 = dataUrl;
        if(typeof (replaceUrlConstants) == "function"){
            dataUrl2 = replaceUrlConstants(dataUrl);
        }
        // 添加选项卡对应的iframe
        height = '100%';
        var str1 = '<iframe class="J_iframe" name="iframe' + dataIndex + '" width="100%" height="'+height+'" src="' + dataUrl2 + '" frameborder="0" data-id="' + dataUrl + '" seamless></iframe>';
        $('.J_mainContent').find('iframe.J_iframe').hide().parents('.J_mainContent').append(str1);

        // 添加选项卡
        $('.J_menuTabs .page-tabs-content').append(str);
        scrollToTab($('.J_menuTab.active'));
    }
    return false;
}
//滚动到当前页
function scrollToTab(element) {
    refreshMainDom();
    var marginLeftVal = calSumWidth($(element).prevAll()), marginRightVal = calSumWidth($(element).nextAll());
    // 可视区域非tab宽度
    var tabOuterWidth = calSumWidth($(".content-tabs").children().not(".J_menuTabs"));
    //可视区域tab宽度
    var visibleWidth = $(".content-tabs").outerWidth(true) - tabOuterWidth;
    //实际滚动宽度
    var scrollVal = 0;
    if ($(".page-tabs-content").outerWidth() < visibleWidth) {
        scrollVal = 0;
    } else if (marginRightVal <= (visibleWidth - $(element).outerWidth(true) - $(element).next().outerWidth(true))) {
        if ((visibleWidth - $(element).next().outerWidth(true)) > marginRightVal) {
            scrollVal = marginLeftVal;
            var tabElement = element;
            while ((scrollVal - $(tabElement).outerWidth()) > ($(".page-tabs-content").outerWidth() - visibleWidth)) {
                scrollVal -= $(tabElement).prev().outerWidth();
                tabElement = $(tabElement).prev();
            }
        }
    } else if (marginLeftVal > (visibleWidth - $(element).outerWidth(true) - $(element).prev().outerWidth(true))) {
        scrollVal = marginLeftVal - $(element).prev().outerWidth(true);
    }
    $('.page-tabs-content').animate({
        marginLeft: 0 - scrollVal + 'px'
    }, "fast");
}
//刷新主dom,即更新当前活动页为main,兼容单iframe首页
function refreshMainDom(){
    //当前活动的窗口id
    var activeId = $('.J_menuTab.active').data('id');
    $('.J_mainContent .J_iframe').each(function () {
        if ($(this).data('id') == activeId) {//匹配到该iframe
            window.main = this.contentWindow;//将main指向当前活动窗口dom
            return false;
        }
    });
}
//计算元素集合的总宽度
function calSumWidth(elements) {
    var width = 0;
    $(elements).each(function () {
        width += $(this).outerWidth(true);
    });
    return width;
}