/*
 * jo:基于jQuery封装,引入jo前要引入jQuery包;
 */
window.jo = {};
//jo初始化
(function () {
    //[Attr] 域名集合
    jo.address = {
        "contextPath": typeof (contextPath) == 'string' ? contextPath : "/",
        "BAM": "/",
        "UMS": "/",
        "PORTAL": "/",
        "STATIC": "/",
        "FS": "/"
    };
    //jo常量
    jo.Const = {
        URI_LOGOUT: "sso/logout"//用户注销uri
        , REG_MAIL: /^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.[a-zA-Z0-9]{2,6}$/	//邮箱验证的正则表达式
        , REG_MOBILEPHONE: /^1[3,4,5,6,7,8,9]\d{9}$/	//手机号的正则表达式
    };
    jo.params = {};//页面参数集
    //[Attr] jt.bIE###是否是IE
    var ie_upto10 = /MSIE \d/.test(navigator.userAgent);
    var ie_11up = /Trident\/(?:[7-9]|\d{2,})\..*rv:(\d+)/.exec(navigator.userAgent);
    //[Attr] 是不是IE
    jo.bIE = ie_upto10 || ie_11up;
    //[Attr] IE版本
    jo.IEVersion = jo.bIE && (ie_upto10 ? document.documentMode || 6 : ie_11up[1]);
    //[Attr] 是否是Edge
    jo.bEdge = /Edge\//i.test(navigator.userAgent); //Edge
    //[Attr] 是否是IE6
    jo.bIE6 = /msie 6/i.test(navigator.userAgent);
    //[Attr] 是否是Chrome
    jo.bChrome = /chrome/i.test(navigator.userAgent);
    //[Attr] 是否是FireFox
    jo.bFireFox = /firefox/i.test(navigator.userAgent);
    //[Attr] 是否是Opera
    jo.bOpera = /Opera/i.test(navigator.userAgent);
    //[Func] 判断val是否有效(即:!=null,!='null',!='NULL',!='',!='undefined')
    jo.isValid = function (val) {
        if (typeof (val) == "undefined") {
            return false;
        } else {
            if (val != null && val != "null" && val != "undefined") {
                if (val != "") {
                    return true;
                }
                if (val === 0) {//因为0==""返回的是true,所以在这要对0做处理
                    return true;
                }
            }
        }
        return false;
    };
    //[Func] 判断参数不是undefined
    jo.isNotUndefined = function (val) {
        if (typeof (val) == "undefined") {
            return false;
        }
        return true;
    };
    //[Func] 获取默认值,val有效返回val,def返回def(def无效返回"")
    jo.getDefVal = function (val, def) {
        if (typeof (val) == "number") {
            return val;
        }
        if (typeof (val) == "boolean") {
            return val;
        }
        return jo.isValid(val) ? val : (jo.isValid(def) ? def : "");
    };
    //[Func] 获取jo策略的UUID
    jo.getUUID = function (size) {
        var UUIDLen = size || 32;//UUID长度
        // var mm = new Date().getTime().toString();//时间戳
        var mm = "";
        var str = "AbC13DE2F56g4hi79JKL0MN8opqR9STU8VwxYZ7aBc6def5GHI4jkl3mnO2PQr1stu0vWXyz";//72个字符
        var len = UUIDLen - mm.length;//计算还差几个长度
        for (var i = 0; i < len; i++) {//循环生成
            mm += str.charAt(Math.floor(Math.random() * 71));//生成0-71的随机数,根据随机数获取对应索引的字符
        }
        return mm;
    };
    //[Func] 获取对象
    jo._ = function (finder) {
        return $(finder);
    };
    //[Func] 获取对象
    window.jo_ = function (finder) {
        return $(finder);
    };
    //[Func] 获取尺寸数字,例如:输入5px,输出5
    jo.getSizeNum = function (nPX) {
        return parseInt(nPX.substring(0, nPX.length - 2));
    };
    //[Func] 解析URL,例如:输入{umsPath}/ums/getXX.action,输出http://www.rookie.com/rookie/ums/getXX.action
    jo.parseUrl = function (sUrl) {
        return jo.getCode(sUrl, jo.address);//sUrl.replace(/\{[a-zA-Z]*Path\}/ig,"http://www.rookie.com/rookie");
    };
    //[Func] 对象转为json字符串
    jo.obj2JsonStr = function (obj) {
        if (!jo.isValid(obj)) {
            if (obj === '') {
                return '""';//空字符串返回空字符串,不返回null
            }
            return null;
        }
        if (typeof (obj) == "object" && !(obj instanceof Array)) {
            var arr = new Array();
            for (var e in obj) {
                if (typeof (obj[e]) != "function") {//排除方法
                    var v = jo.obj2JsonStr(obj[e]);
                    arr.push('"' + e + '":' + v + '');
                }
            }
            return '{' + arr.join(",") + '}';
        } else if (typeof (obj) == "string") {
            return '"' + obj + '"';
        } else if (typeof (obj) == "number") {
            return obj + "";
        } else if (typeof (obj) == "boolean") {//布尔类型
            return obj + "";
        } else if (typeof (obj) == "object" && (obj instanceof Array)) {//数组
            if (obj.length > 0) {
                var str = '[';
                for (var i = 0; i < obj.length; i++) {
                    if (i == 0) {
                        str += jo.obj2JsonStr(obj[i]);
                    } else {
                        str += ',' + jo.obj2JsonStr(obj[i]);
                    }
                }
                str += ']';
                return str;
            } else {
                return "[]";
            }
        } else {
            return {};
        }
    };
    //[Func] xml对象转json
    jo.xmlDocument2Json = function (xmlDoc) {
        var xmlObj = $(xmlDoc);//jq对象化
        var child = xmlObj.children();//一级根节点
        var json = {};
        if (child && child.length > 0) {
            for (var i = 0; i < child.length; i++) {
                var ele = child[i];//节点
                var tagName = ele.tagName;//节点名称
                if (typeof (json[tagName]) == "object" && (json[tagName] instanceof Array)) {//该节点名已存在数组类型的值,添加进去
                    json[tagName].push(jo.xmlDocument2Json(ele));
                } else if (typeof (json[tagName]) != "undefined") {//已经存在该节点名的对象,则改装为数组形式
                    var _arr = new Array();
                    _arr.push(json[tagName]);//将原先的放到数组中
                    _arr.push(jo.xmlDocument2Json(ele));//将当前的也放到数组中
                    json[tagName] = _arr;//更新为数组
                } else {
                    json[tagName] = jo.xmlDocument2Json(ele);
                }
            }
            //读取节点属性
            if (xmlDoc.attributes && xmlDoc.attributes.length && xmlDoc.attributes.length > 0) {
                //var attrsArray = Array.prototype.slice.call(xmlDoc.attributes);
                for (var i = 0; i < xmlDoc.attributes.length; i++) {
                    var attr = xmlDoc.attributes[i];
                    if (typeof (json[attr.name]) == "undefined") {
                        json[attr.name] = attr.value;
                    } else {
                        json["_" + attr.name] = attr.value;//节点中存在同名节点,则以_为前缀
                    }
                }
            }
        } else if (xmlDoc.attributes && xmlDoc.attributes.length && xmlDoc.attributes.length > 0) {
            for (var i = 0; i < xmlDoc.attributes.length; i++) {
                var attr = xmlDoc.attributes[i];
                if (typeof (json[attr.name]) == "undefined") {
                    json[attr.name] = attr.value;
                } else {
                    json["_" + attr.name] = attr.value;//节点中存在同名节点,则以_为前缀
                }
            }
        } else {//不存在子节点和属性,则返回节点内容
            return $(xmlDoc).text();
        }
        return json;
    };
    //[Func] post类型的普通请求:虚拟表单实现
    jo.post = function (sUrl, oData) {
        var temp = document.createElement("form");
        temp.action = jo.parseUrl(sUrl);//解析url
        if (typeof (processRequestData) == "function") {
            oData = processRequestData(oData, "post");//数据加工
        }
        temp.method = "post";
        temp.style.display = "none";
        for (var x in oData) {
            var opt = document.createElement("textarea");
            opt.name = x;
            opt.value = oData[x];
            temp.appendChild(opt);
        }
        document.body.appendChild(temp);
        temp.submit();
        return temp;
    };
    // 上传文件
    jo.postFile = function (url, file, data, successCall, errorCall) {
        if (typeof url == 'object') {
            console.debug('[上传文件] 兼容默认URL模式,原入参:', [url, file, data, successCall, errorCall]);
            errorCall = successCall;
            successCall = data;
            data = file;
            file = url;
            url = '/fs/file/upload';
            console.debug('[上传文件] 兼容默认URL模式,现入参:', [url, file, data, successCall, errorCall]);
        }
        var formData = new FormData();
        formData.append('file', file);
        // formData.append(REQUEST_DATA_KEY_TOKEN, SSO_TOKEN);
        if (data) {
            for (var k in data) {
                formData.append(k, data[k]);
            }
        }
        $.ajax({
            url: url,
            type: 'POST',
            cache: false,
            data: formData,
            async: true,
            processData: false,
            contentType: false,
            dataType: "json",
            beforeSend: function (request) {//加载前执行函数
                // console.info('设置请求头,key=%s,value=%s', REQUEST_DATA_KEY_TOKEN, SSO_TOKEN);
                request.setRequestHeader(REQUEST_DATA_KEY_TOKEN, SSO_TOKEN);
            },
            success: function (json) {
                if (json && json.code == 0) {
                    if (typeof successCall == 'function') {
                        successCall(json, file, data);
                    }
                } else {
                    if (typeof errorCall == 'function') {
                        errorCall(json || {}, file, data);
                    }
                }
            },
            error: function (jqXHR, textStatus, err) {
                console.error("[postJson] --ajax请求出错-参数打印...--" + "\n" +
                    "【请求地址】" + this.url + "\n" +
                    "【请求类型】" + this.type + "\n" +
                    "【请求参数】" + this.data + "\n" +
                    "【返回状态】" + jqXHR.status + "\n" +
                    "【返回类型】" + this.datatype + "\n");
                var re = '';
                if (jo.isValid(jqXHR.responseText)) {//后台传的json字符串
                    try {
                        re = JSON.parse(jqXHR.responseText);
                    } catch (err) {
                        console.error('[postFile] 解析返回结果异常', err);
                    }
                } else {

                }
                if (typeof errorCall == 'function') {
                    errorCall(re || {}, file, data);
                }
            },
            complete: function () {
            }
        });
    };
    //适配老版本的result结果
    jo.adaptOldResult = function (result) {
        // if (result && typeof (result) == 'object' && jo.isValid(result.data) && !Array.isArray(result.data)) {
        //     result._data = result.data;
        //     result.data = [result.data];
        // }
        return result;
    };
    // 是否空对象
    jo.objIsEmpty = function (obj) {
        if (obj) {
            for (var k in obj) {
                return false;
            }
        }
        return true;
    };
    // 是否非空对象
    jo.objIsNotEmpty = function (obj) {
        return !jo.objIsEmpty(obj);
    };
    // 是否空数组
    jo.arrayIsEmpty = function (arr) {
        return !jo.arrayIsNotEmpty(arr);
    };
    // 是否非空数组
    jo.arrayIsNotEmpty = function (arr) {
        return Array.isArray(arr) && arr && arr.length > 0;
    };
    // postJson
    jo.postJsonAjax = jo.postJson = function (sUrl, data, successFunc, errorFunc) {
        data = data || {};
        if (typeof data != 'object' || Array.isArray(data)) {
            console.debug('[postJson] 参数非object类型,自动包装为{body:参数}格式,原参数:', data);
            data = {body: data};
        } else if (jo.objIsNotEmpty(data) && !data.body) {
            console.debug('[postJson] 参数对象无body属性,自动包装为{body:参数}格式,原参数:', data);
            data = {body: data};
        }
        var promise = {
            url: jo.parseUrl(sUrl),
            data: data || {},
            successFunc: successFunc,
            successFuncExec: false,
            errorFunc: errorFunc,
            errorFuncExec: false,
            ajaxFinish: false,
            ajaxFail: false,
            ajaxSuccess: false,
            jqXHR: null,
            textStatus: null,
            err: null,
            result: null,
            // 注册失败回调
            error: function (func) {
                this.errorFunc = func;
                this.callbackError();
                return this;
            },
            // 注册成功回调
            success: function (func) {
                this.successFunc = func;
                this.callbackSuccess();
                return this;
            },
            // 失败回调
            callbackError() {
                if (this.ajaxFail) {// ajax失败
                    if (!this.errorFuncExec) {// 未执行失败回调时才执行, 保证只执行一次
                        console.debug('[postJson] 返回失败,触发error回调', this.result);
                        if (typeof this.errorFunc == 'function') {
                            this.errorFunc(this.result || {}, this.textStatus, this.jqXHR, this.err);
                            this.errorFuncExec = true;
                        } else {
                            console.info('[postJson] error回调函数无效');
                        }
                    } else {
                        console.info('[postJson] error回调函数被重复调用');
                    }
                }
            },
            // 成功回调
            callbackSuccess() {
                if (this.ajaxSuccess) {// ajax成功
                    if (!this.successFuncExec) {// 未执行回调时才执行, 保证只执行一次
                        if (typeof this.successFunc == 'function') {
                            this.successFunc(this.result, this.textStatus, this.jqXHR);
                            this.successFuncExec = true;
                        } else {
                            console.info('[postJson] success回调函数无效');
                        }
                    } else {
                        console.info('[postJson] success回调函数被重复调用');
                    }
                }
            },
            // 执行
            run: function () {
                var _this = this;
                $.ajax({
                    type: "POST",
                    url: _this.url, data: typeof (_this.data) == 'object' ? JSON.stringify(_this.data) : _this.data,
                    async: true, cache: false, processData: false,
                    contentType: 'application/json',//数据提交类型
                    datatype: "json", //返回值类型
                    beforeSend: function (request) {//加载前执行函数
                        // console.info('设置请求头,key=%s,value=%s',REQUEST_DATA_KEY_TOKEN, SSO_TOKEN);
                        request.setRequestHeader(REQUEST_DATA_KEY_TOKEN, SSO_TOKEN);
                    },
                    success: function (result, textStatus, jqXHR) {
                        // 处理
                        _this.jqXHR = jqXHR;
                        _this.textStatus = textStatus;
                        _this.result = result;
                        // 成功回调
                        if (result && result.code == 0) {
                            _this.ajaxSuccess = true;
                            _this.callbackSuccess();
                        } else {
                            _this.ajaxFail = true;
                            _this.callbackError();
                        }

                    },
                    complete: function (jqXHR, textStatus) {
                        if (jo.isValid(jqXHR.responseText)) {//后台传的json字符串
                            var re = '';
                            try {
                                re = JSON.parse(jqXHR.responseText);
                            } catch (err) {
                                console.error('[postJson] 解析返回结果异常', err);
                            }
                            if (jo.isValid(re) && re.code == '-101') {//session过期异常代码
                                console.info('[postJson] 异常代码[-101]:会话过期,url=%s,返回结果=%o', sUrl, re);
                                if (re.redirectTo) {
                                    top.window.location.href = re.redirectTo;
                                } else {
                                    var logoutUrl = jo.getDefVal(URL_UMS, contextPath) + jo.Const.URI_LOGOUT;//登出
                                    logoutUrl += jo.getLinkSign(logoutUrl) + "fromUrl=" + encodeURIComponent(top.location.href);
                                    top.window.location.href = logoutUrl;//登出
                                }
                            }
                        } else {

                        }
                    },
                    error: function (jqXHR, textStatus, err) {
                        //jqXHR:jQuery增强的XHR
                        //textStatus:请求完成状态
                        //err:底层通过throw抛出的异常对象，类型与值与错误类型有关
                        console.error("[postJson] --ajax请求出错-参数打印...--" + "\n" +
                            "【请求地址】" + this.url + "\n" +
                            "【请求类型】" + this.type + "\n" +
                            "【请求参数】" + this.data + "\n" +
                            "【返回状态】" + jqXHR.status + "\n" +
                            "【返回类型】" + this.datatype + "\n");
                        // 处理失败
                        _this.ajaxFail = true;
                        _this.jqXHR = jqXHR;
                        _this.textStatus = textStatus;
                        _this.err = err;
                        // 调用失败回调
                        _this.callbackError();
                    }
                });
                return _this;
            }
        };
        return promise.run();
    };
    //[Func] post类型的ajax请求
    jo.postAjax = jo.postForm = function (sUrl, sData, func, isAsync) {
        // console.error('postAjax已过时,推荐使用postJson');
        sUrl = jo.parseUrl(sUrl);//解析url
        if (typeof (processRequestData) == "function") {
            sData = processRequestData(sData, "postAjax");//数据加工
        }
        //判断是否跨域
        /*if(sUrl.indexOf("http://") == 0 || sUrl.indexOf("https://") == 0){//url为完整形式时继续判断是否跨域
			var cur = location.protocol + "//" + location.host;//当前页面的域名
			if(sUrl.indexOf(cur) != 0){//如果请求地址不是以当前页面的域名开头,则认为是跨域请求
				jo.postJSONP(sUrl, sData, func);//调用跨域请求
				return;
			}
		}*/
        if (!jo.isValid(isAsync)) {
            isAsync = false;//默认非异步加载
        }
        if (!sData) {
            sData = null;
        } else {
            if (typeof (sData) == "string") {//字符串,key=value形式

            } else if (typeof (sData) == "object") {//对象,{key:value,key:value}形式
                //转换为1=1&2=2形式字符串
                var str = "";

                for (var k in sData) {
                    //console.info(sData[k]+"----"+encodeURIComponent(sData[k]))
                    str += "&" + k + "=" + encodeURIComponent(sData[k]);
                }
                //replace(/\+/g, '%2B') 处理加号传到服务端变成空格的问题
                sData = str.substring(1);//.replace(/\+/g, '%2B');
            }
        }
        var res = null;
        $.ajax({
            type: "POST",
            url: sUrl,
            data: sData,
            async: isAsync,
            cache: false,
            contentType: 'application/x-www-form-urlencoded',//'application/json',数据提交类型
            processData: false,
            datatype: "json", //返回值类型
            beforeSend: function (request) {//加载前执行函数
                // console.info('设置请求头,key=%s,value=%s',REQUEST_DATA_KEY_TOKEN, SSO_TOKEN);
                request.setRequestHeader(REQUEST_DATA_KEY_TOKEN, SSO_TOKEN);
            },
            success: function (result, textStatus, jqXHR) {
                if (jo.isValid(result) && result.code != '-101' /*&& result.code != '-102' && result.code != '-103' && result.code != '-901' && result.code != '-999'*/) {
                    result = jo.adaptOldResult(result);
                    if (typeof (func) == "function") {
                        try {
                            func(result)
                        } catch (err) {
                            console.error(err);
                        }
                        ;
                    }
                    res = result;
                } else {
                    res = null;
                }
            },
            complete: function (jqXHR, textStatus) {
                if (jo.isValid(jqXHR.responseText)) {//后台传的json字符串
                    var re = '';
                    try {
                        re = JSON.parse(jqXHR.responseText);
                    } catch (err) {
                        if (textStatus == "error") {

                        }
                    }
                    if (jo.isValid(re) && re.code == '-101') {//session过期异常代码
                        console.info('异常代码[-101]:会话过期,url=%s,返回结果=%o', sUrl, re);
                        /*jo.confirm(re.info,function(){

    		        	});*/
                        if (re.redirectTo) {
                            top.window.location.href = re.redirectTo;
                        } else {
                            var logoutUrl = jo.getDefVal(URL_UMS, contextPath) + jo.Const.URI_LOGOUT;//登出
                            /*var fromUrl = window.location.href;
							var win = window;
							while(win != top){
								var parentUrl = win.parent.location.href;//父页面的url
								fromUrl = parentUrl + jo.getLinkSign(parentUrl) + "childUrl=" + encodeURIComponent(fromUrl);
								win = win.parent;
							}*/
                            logoutUrl += jo.getLinkSign(logoutUrl) + "fromUrl=" + encodeURIComponent(top.location.href);
                            top.window.location.href = logoutUrl;//登出
                        }
                        //top.window.location.href ="page/jsp/login.jsp";  //跳转页面
                    }/*else if(jo.isValid(re) && re.code == '-102'){//平台业务异常代码
						jo.showMsg(jo.getDefVal(re.info,"非法操作!"));
					}else if(jo.isValid(re) && re.code == '-801'){//参数非法异常
						jo.showMsg(jo.getDefVal(re.info,"参数异常!"));
					}else if(jo.isValid(re) && re.code == '-901'){//平台业务异常代码
    		    		jo.showMsg(jo.getDefVal(re.info,"*系统异常!"));
    		    	}else if(jo.isValid(re) && re.code == '-999'){//系统异常代码
    		    		jo.showMsg("*系统出现异常,请与管理员联系!");
    		    	}else{

    		    	}*/
                } else {

                }
            },
            error: function (jqXHR, textStatus, err) {
                //jqXHR:jQuery增强的XHR
                //textStatus:请求完成状态
                //err:底层通过throw抛出的异常对象，类型与值与错误类型有关
                console.log(arguments);
                console.error(" --ajax请求出错-参数打印...--" + "\n" +
                    "【请求地址】" + this.url + "\n" +
                    "【请求类型】" + this.type + "\n" +
                    "【请求参数】" + this.data + "\n" +
                    "【返回状态】" + jqXHR.status + "\n" +
                    "【返回类型】" + this.datatype + "\n");
                if (typeof (func) == "function") {
                    try {
                        func({code: -1, info: 'error:' + jqXHR.status});
                    } catch (err) {
                        console.error(err);
                    }
                }
            },
            statusCode: {
                '403': function (jqXHR, textStatus, err) {
                    //jqXHR:jQuery增强的XHR
                    //textStatus:请求完成状态
                    //err:底层通过throw抛出的异常对象，类型与值与错误类型有关
                    console.log(arguments);
                    console.log(403);
                },
                '400': function () {
                    console.log(400);
                },
                '404': function () {
                    console.log(404 + ":阿偶,无法链接到二次元资源:" + this.url);
                }
            }
        });
        return res;
    };
    //[Func] 获取地址栏参数
    jo.getUrlParam = function (key) {
        var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)", "i");
        var r = window.location.search.substr(1).match(reg);
        //decodeURI(escape(unescape(r[2])))处理中文乱码
        if (r != null) {
            if (jo.bIE) {
                return r[2];
            } else {
                return decodeURI(escape(unescape(r[2])));
            }
        }
        return "";
    };
    //[Func] 获取所有地址栏参数
    jo.getUrlParams = function (sUrl) {
        var searchStr = jo.getDefVal(sUrl, window.location.search);
        if (searchStr.indexOf("?") > -1) {//有参数
            searchStr = searchStr.substring(1);
            var searchs = searchStr.split("&");
            var res = {};
            for (var i = 0; i < searchs.length; i++) {
                var j = searchs[i].indexOf("=");
                var skey = searchs[i].substring(0, j);
                var svalue = searchs[i].substring(j + 1);
                res[skey] = svalue;
            }
            return res;
        } else {
            return {};
        }
    };
    //[Func] 页面弹出提示信息
    jo.showMsg = function (msg) {
        jo.closeMsg();
        var msg_box = "<div style='width:20%;height:auto;position:absolute;z-index:9999;top:40%;left:40%;background-color:#333333;line-height:25px; letter-spacing:1px;padding:3px 5px;font-size:16px;color:#FFFFFF;text-align:center;display:none;' class='showMsg_style' id='showMsg' onclick='jo.closeMsg()'>" + msg + "</div>";
        $("body").append(msg_box);
        $("#showMsg").fadeIn(1000).fadeOut(3000);
    };
    //[Func] 关闭提示信息
    jo.closeMsg = function () {
        if ($("#showMsg")) {//存在提示消息窗
            $("#showMsg").remove();
        }
    };
    //[Func] 正在加载提示框,当传入false表示关闭
    jo.showLoading = function (msgOrClose) {
        //当参数为false时,关闭提示
        if (typeof (msgOrClose) == "boolean" && !msgOrClose) {
            jo.remove(document.getElementById("joMsg_loading"));
        } else {
            var obj = document.getElementById("joMsg_loading");
            if (jo.isValid(obj)) {
                $(obj).html(jo.getDefVal(msgOrClose, "正在加载..."));
            } else {
                var _html = '<div class="joMsg_loading" id="joMsg_loading">';
                _html += jo.getDefVal(msgOrClose, "正在加载...");
                _html += '</div>';
                $("body").append(_html);
            }
        }
    };
    //[Func] 将表单数据转为json对象
    jo.form2Json = function (id) {
        var arr = null;
        if (typeof (id) == "string") {//传入字符串,说明是表单的ID
            if (!($("#" + id).attr("id"))) {
                return null;
            }
            arr = $("#" + id).serializeArray();
        } else if (typeof (id) == "object") {
            arr = id.serializeArray();
        } else {
            return null;
        }

        //直接返回js对象
        var json = {};
        for (var i = 0; i < arr.length; i++) {
            var val = arr[i].value;
            //在这里不需要对换行做处理,否则会将换行符变为\r\n字符串
            /*if(val){
				val = val.replace(/\r/ig, "\\r");
				val = val.replace(/\n/ig, "\\n");
			}*/
            if (json[arr[i].name]) {//已存在该name,例如:多选框,默认逗号拼接
                json[arr[i].name] = json[arr[i].name] + ',' + val;
            } else {
                json[arr[i].name] = val;
            }
        }
        return json;
    };
    //[Func] 将表单数据转为json字符串
    jo.form2JsonStr = function (id) {
        return JSON.stringify(jo.form2Json(id));
    };
    //[Func] 将表单数据转为key1=value1&key2=value2..形式
    jo.form2ParamStr = function (id) {
        if (!($("#" + id).attr("id"))) {
            return "";
        }
        return $("#" + id).serialize();
    };
    //[Func] 重置表单,传入表单id
    jo.reset = function (id) {
        document.getElementById(id).reset();
    };
    //[Func] 全选/取消全选,全选框的id为checkAll,参数为复选框的name(默认是ID);参数obj表示全选框对象,通常用this;示例:<input type="checkbox" onclick="jo.checkAll('ID',this)">
    jo.checkAll = function (name, obj) {
        var _id = name ? name : "ID";
        if (typeof (obj) == "object") {
            $("input[name='" + _id + "']").prop("checked", jo.getDefVal($(obj).prop("checked"), false));//1.9以上jquery使用
        } else {
            $("input[name='" + _id + "']").prop("checked", jo.getDefVal($("#checkAll").prop("checked"), false));//1.9以上jquery使用
        }
    };
    //[Func] 得到所有选中复选框的值:参数为checkbox的name;返回以逗号间隔的字符串
    jo.getCheckBoxValue = function (boxName) {
        if (!boxName) {
            boxName = "ID";
        }
        var ids = $("input[name='" + boxName + "']" + ":checked");
        idStr = "";
        //获取勾选集
        for (var i = 0; i < ids.length; i++) {
            idStr += "," + ids[i].value;
        }
        idStr = idStr.substring(1);
        return idStr;
    };
    // 获取元素定位, 原生元素
    jo.getXY = function (ele) {
        var left = ele.offsetLeft;
        var top = ele.offsetTop;
        // 取得元素的offsetParent
        var parent = ele.offsetParent;

        // 一直循环直到根元素
        while (parent) {
            left += parent.offsetLeft;
            top += parent.offsetTop;
            parent = parent.offsetParent;
        }
        return {
            x: left,
            y: top
        };
    };
    // 获取定位详情
    jo.getPositionDetail = function (e, classNameOrFunc) {
        // 鼠标位置
        var x = e.pageX;
        var y = e.pageY;
        // 找到按钮放置盒子
        var target = e.target;
        if (classNameOrFunc) {
            if (typeof classNameOrFunc == 'function') {
                while (target && !classNameOrFunc(target)) {
                    target = target.parentNode;
                }
            } else {
                while (target && !$(target).hasClass(classNameOrFunc)) {
                    target = target.parentNode;
                }
            }
        }
        // console.log('[获取事件坐标详情] 查找真正的事件节点:', target)
        // 元素的位置
        var xy = jo.getXY(target);
        var eleX = xy.x;
        var eleY = xy.y;
        // 元素的宽高
        var w = target.offsetWidth;
        var h = target.offsetHeight;

        // 判断是否放到右面
        var right = x > (eleX + w / 2);
        // 是否下面
        var down = y > (eleY + h / 2);
        return {
            // 预期事件对象, 比如事件在父亲上挂着, 操作孩子时也会触发, 此时eventTarget是孩子(event事件对象的target), target是父亲, 也就是我们预期的那个元素
            target: target,
            // 事件触发对象
            eventTarget: e.target,
            // 预期对象的x坐标
            targetX: eleX,
            // 预期对象的y坐标
            targetY: eleY,
            // 预期对象的宽度, 含width,padding,border
            targetWidth: w,
            // 预期对象的高度, 含height,padding,border
            targetHeight: h,
            // 触发事件时鼠标x坐标
            x: x,
            // 触发事件时鼠标y坐标
            y: y,

            // 鼠标是否在预期对象左边
            atLeft: !right,
            // 鼠标是否在预期对象右边
            atRight: right,
            // 鼠标是否在预期对象上边
            atUp: !down,
            // 鼠标是否在预期对象下边
            atDown: down,
            // 鼠标是否在目标对象内部
            atInner: x >= eleX && x <= eleX + w && y >= eleY && y <= eleY + h,

            // 鼠标是否在预期对象中央(九宫格中间那一格)
            atCenterOf9: x >= (eleX + w / 3) && x <= (eleX + 2 * w / 3) && y >= (eleY + h / 3) && y <= (eleY + 2 * h / 3),
            // 鼠标是否在预期对象左侧1/3
            atLeftOf9: x < (eleX + w / 3),
            // 鼠标是否在预期对象右侧1/3
            atRightOf9: x > (eleX + 2 * w / 3),
            // 鼠标是否在预期对象上边1/3
            atUpOf9: y < (eleY + h / 3),
            // 鼠标是否在预期对象下边1/3
            atDownOf9: y > (eleY + 2 * h / 3),

            // 鼠标是否在预期对象中央(16宫格中间那一格)
            atCenterOf16: x >= (eleX + w / 4) && x <= (eleX + w * 3 / 4) && y >= (eleY + h / 4) && y <= (eleY + h * 3 / 4),
            // 鼠标是否在预期对象左侧1/4
            atLeftOf16: x < (eleX + w / 4),
            // 鼠标是否在预期对象右侧1/4
            atRightOf16: x > (eleX + w * 3 / 4),
            // 鼠标是否在预期对象上边1/4
            atUpOf16: y < (eleY + h / 4),
            // 鼠标是否在预期对象下边1/4
            atDownOf16: y > (eleY + h * 3 / 4),
        };
    };
    //[Func] 获得url添加条件的连接符,即判断传入的URL后方链接新参数应该用'?'还是用'&'
    jo.getLinkSign = function (str) {
        if (str.indexOf("?") >= 0) {//传进来的URL带有'?'
            if (str.indexOf("?") + 1 == str.length) {//当url以?结尾时,返回''
                return "";
            }
            if (str.indexOf("&") + 1 == str.length) {//当url以&结尾,返回''
                return '';
            } else {
                if (str.split("?").length > 2) {//有且只有1个'?'
                    console.info("URL格式可能存在异常:" + str);
                }
                return "&";
            }
        } else {
            return "?";
        }
    };
    //[Func] 格式化日期 : 毫秒数/date==>yyyy-MM-dd
    jo.formatDate = function (ms) {
        if (!jo.isValid(ms)) {
            return "";
        }
        if (typeof (ms) == "string" && !isNaN(ms)) {
            ms = parseInt(ms);
        }
        var _ms = jo.getDefVal(ms);
        var myDate = new Date(_ms);//生成时间类型
        var yyyy = myDate.getFullYear();//年份 xxxx
        var MM = (myDate.getMonth() + 1) < 10 ? "0" + (myDate.getMonth() + 1) : (myDate.getMonth() + 1);//月份 0-11,加1就是真实月份
        var dd = myDate.getDate() < 10 ? "0" + myDate.getDate() : myDate.getDate();//日期 1-31
        //myDate.getDay();//星期几 0-6 ,0代表星期天
        return yyyy + "-" + MM + "-" + dd;
    };
    //[Func] 格式化时间 : 毫秒数/date==>yyyy-MM-dd hh:mm:ss
    jo.formatTime = function (ms) {
        if (!jo.isValid(ms)) {
            return "";
        }
        if (typeof (ms) == "string" && !isNaN(ms)) {
            ms = parseInt(ms);
        }
        var _ms = jo.getDefVal(ms);
        var myDate = new Date(_ms);//生成时间类型
        var yyyy = myDate.getFullYear();//年份 xxxx
        var MM = (myDate.getMonth() + 1) < 10 ? "0" + (myDate.getMonth() + 1) : (myDate.getMonth() + 1);//月份 0-11,加1就是真实月份
        var dd = myDate.getDate() < 10 ? "0" + myDate.getDate() : myDate.getDate();//日期 1-31
        //时分秒小于10时在前补0
        var hh = myDate.getHours() < 10 ? "0" + myDate.getHours() : myDate.getHours();//小时数
        var mm = myDate.getMinutes() < 10 ? "0" + myDate.getMinutes() : myDate.getMinutes();//分钟数
        var ss = myDate.getSeconds() < 10 ? "0" + myDate.getSeconds() : myDate.getSeconds();//秒数
        return yyyy + "-" + MM + "-" + dd + " " + hh + ":" + mm + ":" + ss;
    };
    //[Func] 格式化时间戳 : 毫秒数/date==>yyyy-MM-dd hh:mm:ss.SSS
    jo.formatTimestamp = function (ms) {
        if (!jo.isValid(ms)) {
            return "";
        }
        if (typeof (ms) == "string" && !isNaN(ms)) {
            ms = parseInt(ms);
        }
        var _ms = jo.getDefVal(ms);
        var myDate = new Date(_ms);//生成时间类型
        var yyyy = myDate.getFullYear();//年份 xxxx
        var MM = (myDate.getMonth() + 1) < 10 ? "0" + (myDate.getMonth() + 1) : (myDate.getMonth() + 1);//月份 0-11,加1就是真实月份
        var dd = myDate.getDate() < 10 ? "0" + myDate.getDate() : myDate.getDate();//日期 1-31
        //时分秒小于10时在前补0
        var hh = myDate.getHours() < 10 ? "0" + myDate.getHours() : myDate.getHours();//小时数
        var mm = myDate.getMinutes() < 10 ? "0" + myDate.getMinutes() : myDate.getMinutes();//分钟数
        var ss = myDate.getSeconds() < 10 ? "0" + myDate.getSeconds() : myDate.getSeconds();//秒数
        var ms = myDate.getMilliseconds() < 10 ? "00" + myDate.getMilliseconds() : (myDate.getMilliseconds() < 100 ? "0" + myDate.getMilliseconds() : myDate.getMilliseconds());//毫秒数
        return yyyy + "-" + MM + "-" + dd + " " + hh + ":" + mm + ":" + ss + "." + ms;
    };
    //[Func] 执行代码,参数1:代码表达式string,参数2:表达式中的常量(类似于{URL_UMS}格式)映射,object类型
    jo.execCode = function (sCode, paramArr) {
        if (jo.isValid(sCode)) {//待执行代码的有效性
            return eval(jo.getCode(sCode, paramArr));
        } else {
            return false;
        }
    };
    //[Func] 解析代码
    jo.getCode = function (sCode, paramArr) {
        if (jo.isValid(sCode)) {//待执行代码的有效性
            if (jo.isValid(paramArr)) {//替换参数有效的话,执行替换,否则直接返回sCode
                //替换变量
                var reg = /\{[a-zA-Z0-9_]*\}/g;
                if (reg.test(sCode)) {
                    var a = sCode.match(reg);
                    if (!jo.isValid(paramArr)) {
                        //参数替换数组无效
                    } else {
                        for (var i = 0; i < a.length; i++) {
                            var x = a[i] + "";
                            var key = x.substring(1, x.length - 1);
                            sCode = sCode.replace(x, paramArr[key]);
                        }
                    }
                }
            }
            return sCode;
        } else {
            return "";
        }
    };
    //[Func] 解析表达式, 参数1:表达式(字段/{字段}/[=方法({字段})]/文字), 参数2:数据对象, 参数3:当前索引
    jo.parseExpression = function (expression, item, m) {
        var _val = expression;//
        //对可执行字段进行处理
        if (jo.isValid(_val)) {
            //以 [= 开头, ] 结尾的格式,表示调用函数对字段进行处理,其中的变量(即该行json的字段值)要使用花括号括起来,例如 [=jo.formatDate('{time}')],time为从数据库查询出来的字段
            if (_val && /^\[=(.)*\]$/.test(_val)) {
                //去掉无效的[=和]
                _val = _val.substring(2, _val.length - 1);
                //解析{xx}类型的变量
                _val = jo.getCode(_val, item);
                //执行该函数
                _val = eval(_val);
            } else if (/\{(.)*\}/g.test(_val)) {//解析{xx}类型的变量
                _val = jo.getCode(_val, item);
            } else if (_val && _val == "_seq") {//序号常量,自动生成序号
                _val = m + 1;
            } else {
                _val = jo.getDefVal(item[_val], "");//正常字段名则从json中取对应字段值
            }
        }
        return jo.getDefVal(_val, "");//最后对_val进行检查,无效则赋值空格
    };
    //[Func] 初始化按钮权限
    jo.initBtnAuth = function () {
        $(".joBtn[isShow]").each(function (index, element) {
            var isShow = $(this).attr("isShow");
            //当返回false或'false'时才隐藏,isShow为空时也隐藏
            if (jo.execCode(isShow) == false || jo.execCode(isShow) == 'false') {
                $(this).hide();
            } else {
                $(this).show();
            }
        });
    };
    //[Func] 加载css,参数sUrl为css地址
    jo.loadCSS = function (sUrl) {
        if (jo.isValid(sUrl)) {
            var link = document.createElement("link");
            link.setAttribute("rel", "stylesheet");
            link.setAttribute("href", sUrl);
            document.head.appendChild(link);
        }
    };

    //[Func] 加载JavaScript,参数sUrl为js地址
    jo.loadJS = function (sUrl) {
        if (jo.isValid(sUrl)) {
            var script = document.createElement("script");
            script.setAttribute("type", "text/javascript");
            script.setAttribute("charset", "utf-8");
            script.setAttribute("src", sUrl);
            document.head.appendChild(script);
        }
    };
    //JSONP回调方法数组
    window.joJSONP = new Array();
    //[Func] JSONP跨域请求
    jo.postJSONP = function (sUrl, sData, callback) {
        if (jo.isValid(sUrl)) {
            console.log("jsonp: " + sUrl);
            if (typeof (sData) == "string") {//字符串,key=value形式

            } else if (typeof (sData) == "object") {//对象,{key:value,key:value}形式
                //转换为1=1&2=2形式字符串
                var str = "";
                for (var k in sData) {
                    str += "&" + k + "=" + encodeURIComponent(sData[k]);//sData[k];
                }
                //replace(/\+/g, '%2B') 处理加号传到服务端变成空格的问题
                sData = str.substring(1);//.replace(/\+/g, '%2B');
            }
            var idx = joJSONP.push(callback) - 1;
            sUrl += jo.getLinkSign(sUrl) + "callback=joJSONP%5B" + idx + "%5D";
            if (sData) {
                sUrl += "&" + sData;
            }
            jo.loadJS(sUrl);
        }
    };

    //[Func] 移除某元素,参数为dom元素对象
    jo.remove = function (element) {
        if (jo.isValid(element)) {
            var pElement = element.parentNode;
            pElement.removeChild(element);
        }
    };

    //[Func] iframe加载完成事件处理
    jo.iframeLoaded = function (iframe, func) {
        if (iframe.attachEvent) {
            iframe.attachEvent("onload", func);
        } else {
            iframe.onload = func;
        }
    };

    //[Func] 生成遮罩层,返回遮罩层元素,引用的jo窗口相关css
    jo.getShade = function (zIndex) {
        var shadeElement = document.createElement("div");
        //遮罩层ID为窗口名字+Shade
        shadeElement.setAttribute("id", "joShade");
        //遮罩层样式为joWinShade
        shadeElement.setAttribute("class", "joWinShade");
        if (jo.isValid(zIndex)) {
            //z-index属性
            shadeElement.style.zIndex = zIndex;
        }
        document.body.appendChild(shadeElement);
        return shadeElement;
    };
    //[Func] 打开新窗口
    jo.newWindow = function (sUrl) {
        sUrl = jo.parseUrl(sUrl);//解析url
        window.open(sUrl);
    };

    /**
     * [Func] 表单验证,参数为表单jQuery对象
     * 1.ErrEmpty 为空提示,含有此属性说明该项为必填项,属性值为提示信息;
     * 2.ErrLength 长度验证,值为该项允许的最大长度;
     * 3.ErrInfo 错误提示,发生验证错误时的提示信息;
     * 4.ErrReg 验证的正则表达式,可以自定义;
     * 5.ErrMail 邮箱验证
     * 6.ErrPhone 手机号验证
     */
    jo.checkForm = function (oForm) {
        //必填/非空验证
        var emptys = oForm.find("input[ErrEmpty]");
        for (var i = 0; i < emptys.length; i++) {
            var e = emptys[i];
            var val = $(e).val();
            if (val == "") {
                jo.showMsg(jo.getDefVal($(e).attr("ErrEmpty"), "该项不可以为空！"));//提示信息
                $(e).focus();//获取焦点
                return false;
            }
        }
        //select下拉列表的非空验证
        emptys = oForm.find("select[ErrEmpty]");
        for (var i = 0; i < emptys.length; i++) {
            var e = emptys[i];
            var val = $(e).val();
            if (val == "") {
                jo.showMsg(jo.getDefVal($(e).attr("ErrEmpty"), "该项不可以为空！"));//提示信息
                $(e).focus();//获取焦点
                return false;
            }
        }
        //长度验证
        var lengths = oForm.find("input[ErrLength]");
        for (var i = 0; i < lengths.length; i++) {
            var e = lengths[i];
            var len = $(e).attr("ErrLength");//限制长度
            var val = $(e).val();//值
            //alert(len+"--"+val);
            if (jo.isValid(len) && jo.isValid(val)) {//长度和值均有效时,判断是否超过长度
                if (val.length > parseInt(len)) {//长度超过
                    jo.showMsg(jo.getDefVal($(e).attr("ErrInfo"), "该项超过最大长度[" + len + "]！"));//提示信息
                    $(e).focus();//获取焦点
                    return false;
                }
            }
        }

        //数字验证
        var numbers = oForm.find("input[ErrNumber]");
        for (var i = 0; i < numbers.length; i++) {
            var e = numbers[i];
            //var len = $(e).attr("ErrLength");//限制长度
            var val = $(e).val();//值
            if (jo.isValid(val)) {//值有效时,判断是否是数字类型
                if (isNaN(val)) {
                    jo.showMsg(jo.getDefVal($(e).attr("ErrNumber"), "该值必须是数字类型！"));//提示信息
                    $(e).focus();//获取焦点
                    return false;
                }
            }
        }

        //邮箱验证
        var numbers = oForm.find("input[ErrMail]");
        for (var i = 0; i < numbers.length; i++) {
            var e = numbers[i];//表单元素
            var val = $(e).val();//值
            if (jo.isValid(val)) {//值有效时,进行验证
                if (!jo.Const.REG_MAIL.test(val)) {
                    jo.showMsg(jo.getDefVal($(e).attr("ErrMail"), "错误的邮箱格式！"));//提示信息
                    $(e).focus();//获取焦点
                    return false;
                }
            }
        }

        //手机号验证
        var numbers = oForm.find("input[ErrPhone]");
        1
        for (var i = 0; i < numbers.length; i++) {
            var e = numbers[i];//表单元素
            var val = $(e).val();//值
            if (jo.isValid(val)) {//值有效时,进行验证
                if (!jo.Const.REG_MOBILEPHONE.test(val)) {
                    jo.showMsg(jo.getDefVal($(e).attr("ErrPhone"), "错误的手机号格式！"));//提示信息
                    $(e).focus();//获取焦点
                    return false;
                }
            }
        }

        //自定义正则表达式
        var numbers = oForm.find("input[ErrReg]");
        for (var i = 0; i < numbers.length; i++) {
            var e = numbers[i];//表单元素
            var val = $(e).val();//值
            var reg = $(e).attr("ErrReg");
            if (jo.isValid(val) && jo.isValid(reg)) {//值有效时,进行验证
                if (!new RegExp(reg).test(val)) {
                    jo.showMsg(jo.getDefVal($(e).attr("ErrInfo"), "验证未通过！"));//提示信息
                    $(e).focus();//获取焦点
                    return false;
                }
            }
        }


        return true;
    };

    //[Func] confirm选择框
    jo.confirm = function (sMsg, funcOk) {
        if (window.confirm(sMsg)) {
            funcOk();
        }
    };

    /**
     * [Func] 数组排序,基于冒泡排序算法
     * @param arr 待排序数组
     * @param func 核心比较回调函数(参考java中Comparator比较器),回调函数有2参数,返回元素a和b的比较结果,返回true表示元素a在b前
     */
    jo.sort = function (arr, func) {
        for (var i = 0; i < arr.length; i++) {
            for (var j = i + 1; j < arr.length; j++) {
                //两者比较,返回true则a在b前面,返回false则b在a前面
                if (!func(arr[i], arr[j])) {
                    var temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
    };

    /**
     * 智能解析key,和execCode类似
     * 1.key可以是普通字符串,例如name,返回oObj对象的name属性
     * 2.key可以是多属性拼接表达式,例如{id}({name}),传入该值返回的是{id}和{name}分别被id和name属性替换后的值==> id的值(name的值)
     * 3.key可以是有返回值的方法表达式,例如:[=format({time})],返回的是time属性传入format方法后返回的值
     * @param oObj
     * @param sKey
     */
    jo.parseKey = function (oObj, sKey) {
        var _val = sKey;
        //对可执行字段进行处理
        if (jo.isValid(_val)) {
            //以 [= 开头, ] 结尾的格式,表示调用函数对字段进行处理,其中的变量(即该行json的字段值)要使用花括号括起来,例如 [=jo.formatDate('{time}')],time为从数据库查询出来的字段
            if (_val && /^\[=(.)*\]$/.test(_val)) {
                //去掉无效的[=和]
                _val = _val.substring(2, _val.length - 1);
                //解析{xx}类型的变量
                _val = jo.getCode(_val, oObj);
                //执行该函数
                _val = eval(_val);
            } else if (/\{(.)*\}/g.test(_val)) {//解析{xx}类型的变量
                _val = jo.getCode(_val, oObj);
            } else {
                _val = jo.getDefVal(oObj[_val], "");//正常字段名则从接送中取对应字段值
            }
        }
        return jo.getDefVal(_val, "");//最后对_val进行检查,无效则赋值空
    };
    /**
     * 将只读属性的表单元素转为文本
     * @param formId 表单id
     */
    jo.readonly2Label = function (formId) {
        formId = formId || "pageForm";
        var form = $("#" + formId);//表单
        //处理input
        form.find("input[type='text'][readonly]").each(function (idx, ele) {
            var val = $(ele).val();
            var label = '<span>' + val + '</span>';
            $(ele).after(label);//在元素后加文本
            jo.remove(ele);//删除元素
        });
        //处理select
        form.find("select[readonly]").each(function (idx, ele) {
            var text = "";
            if (typeof (ele.options[ele.options.selectedIndex]) != "undefined") {//避免因异步问题或空值带来的undefind错误
                text = ele.options[ele.options.selectedIndex].text;
            }
            var label = '<span>' + text + '</span>';
            $(ele).after(label);
            jo.remove(ele);
        });
        //处理textarea
        form.find("textarea[readonly]").each(function (idx, ele) {
            var val = $(ele).val();
            var label = '<span>' + val + '</span>';
            $(ele).after(label);
            jo.remove(ele);
        });
    };
    /**
     * 表单元素转label
     * @param formId
     */
    jo.form2Label = function (formId) {
        formId = formId || "pageForm";
        var form = $("#" + formId);//表单
        //处理input
        form.find("input[type='text']").each(function (idx, ele) {
            var val = $(ele).val();
            var label = '<span>' + val + '</span>';
            $(ele).after(label);//在元素后加文本
            jo.remove(ele);//删除元素
        });
        //处理select
        form.find("select").each(function (idx, ele) {
            var text = "";
            if (typeof (ele.options[ele.options.selectedIndex]) != "undefined") {//避免因异步问题或空值带来的undefind错误
                text = ele.options[ele.options.selectedIndex].text;
            }
            var label = '<span>' + text + '</span>';
            $(ele).after(label);
            jo.remove(ele);
        });
        //处理textarea
        form.find("textarea").each(function (idx, ele) {
            var val = $(ele).val();
            var label = '<span>' + val + '</span>';
            $(ele).after(label);
            jo.remove(ele);
        });
    };

    // 数组转对象
    jo.array2Object = function (arr, keyFunc, valFunc) {
        var obj = {};
        if (arr) {
            for (var i = 0; i < arr.length; i++) {
                var item = arr[i];
                var key = item;
                if (typeof keyFunc == 'function') {
                    key = keyFunc(item);
                }
                var val = item;
                if (typeof valFunc == 'function') {
                    val = valFunc(item);
                }
                obj[key] = val;
            }
        }
        return obj;
    };
    // 对象转数组
    jo.object2Array = function (obj, elementFunc) {
        var arr = [];
        if (obj) {
            for (var k in obj) {
                if (typeof elementFunc == 'function') {
                    arr.push(elementFunc(k, obj[k]));
                } else {
                    arr.push(k);
                }
            }
        }
        return arr;
    };
    // 拷贝对象
    jo.copyObject = function (obj) {
        var newObj = {};
        if (obj) {
            for (var k in obj) {
                newObj[k] = obj[k];
            }
        }
        return newObj;
    };
    // 深拷贝对象
    jo.copyObjectDeep = function (obj) {
        return JSON.parse(JSON.stringify(obj));
    };
    // 合并对象, 返回新对象
    jo.mergeObject = function (one, two) {
        var newObj = {};
        if (one) {
            for (var k in one) {
                newObj[k] = one[k];
            }
        }
        if (two) {
            for (var k in two) {
                newObj[k] = two[k];
            }
        }
        return newObj;
    };
    // 拷贝数组
    jo.copyArray = function (arr) {
        var newArr = [];
        if (arr) {
            for (var i = 0; i < arr.length; i++) {
                newArr.push(arr[i]);
            }
        }
        return newArr;
    };
    // 合并数组, 返回合并后的新数组
    jo.mergeArray = function (arrOne, arrTwo) {
        var arr = [];
        if (arrOne) {
            for (var i = 0; i < arrOne.length; i++) {
                arr.push(arrOne[i]);
            }
        }
        if (arrTwo) {
            for (var i = 0; i < arrTwo.length; i++) {
                arr.push(arrTwo[i]);
            }
        }
        return arr;
    };
    // 数组循环
    jo.forEach = function (arr, forFunc) {
        if (arr && arr.length > 0) {
            for (var i = 0; i < arr.length; i++) {
                var item = arr[i];
                forFunc(item, i);
            }
        }
    };
    // 数组转数组
    jo.array2array = function (arr, mapFunc) {
        var newArr = [];
        if (arr && arr.length > 0) {
            for (var i = 0; i < arr.length; i++) {
                var item = arr[i];
                newArr.push(mapFunc(item, i));
            }
        }
        return newArr;
    };
    // 日志输出
    jo.log = function (info, args) {
        console.log(info + ', 输出内容=%o', args);
    };
    // 断言
    jo.assert = {
        valid(val, msg) {
            if (!jo.isValid(val)) {
                if (msg) {
                    jo.showErrorMsg(msg);
                }
                return false;
            }
            return true;
        },
        main(val, msg) {
            if (!jo.Const.REG_MAIL.test(val)) {
                if (msg) {
                    jo.showErrorMsg(msg);
                }
                return false;
            }
            return true;
        },
        phone(val, msg) {
            if (!jo.Const.REG_MOBILEPHONE.test(val)) {
                if (msg) {
                    jo.showErrorMsg(msg);
                }
                return false;
            }
            return true;
        },
        reg(val, reg, msg) {
            if (!new RegExp(reg).test(val)) {
                if (msg) {
                    jo.showErrorMsg(msg);
                }
                return false;
            }
            return true;
        },
        notNaN(val, msg) {
            if (isNaN(val)) {
                if (msg) {
                    jo.showErrorMsg(msg);
                }
                return false;
            }
            return true;
        },
        object(val, msg) {
            if (typeof (val) != 'object') {
                if (msg) {
                    jo.showErrorMsg(msg);
                }
                return false;
            }
            return true;
        },
        number(val, msg) {
            if (typeof (val) != 'number') {
                if (msg) {
                    jo.showErrorMsg(msg);
                }
                return false;
            }
            return true;
        },
        string(val, msg) {
            if (typeof (val) != 'string') {
                if (msg) {
                    jo.showErrorMsg(msg);
                }
                return false;
            }
            return true;
        },
        array(val, msg) {
            if (!Array.isArray(val)) {
                if (msg) {
                    jo.showErrorMsg(msg);
                }
                return false;
            }
            return true;
        },
        notEmpty(val, msg) {
            if (!val || val.length === 0) {
                if (msg) {
                    jo.showErrorMsg(msg);
                }
                return false;
            }
            return true;
        },
    };
    // 判断是否数字或数字字符串
    jo.isNumber = function (val) {
        return !isNaN(parseFloat(val)) && isFinite(val);
    };
    // 判断是否数组
    jo.isArray = function (obj) {
        return typeof (obj) == "object" && obj instanceof Array;
    };
    // 是否数组字符串
    jo.isArrayStr = function (val) {
        if (val.indexOf('[') > -1) {
            try {
                return Array.isArray(JSON.parse(val));
            } catch (e) {
                console.warn('判断是否能转为array异常', val, e);
            }
        }
        return false;
    };
    // 是否对象json字符串
    jo.isObjJsonStr = function (val) {
        if (val.indexOf('{') > -1) {
            try {
                JSON.parse(val);
                return true;
            } catch (e) {
                console.warn('判断是否能转为json异常', val, e);
            }
        }
        return false;
    };
    // 判断数组是否包含某元素
    jo.arrayContains = function (arr, ele) {
        if (jo.arrayIsNotEmpty(arr)) {
            for (let i = 0; i < arr.length; i++) {
                if (arr[i] === ele) {
                    return true;
                }
            }
        }
        return false;
    };
    // 全屏
    jo.fullScreen = function (elem) {
        elem = elem || document.documentElement; // 获取整个文档根元素
        if (elem.requestFullscreen) {
            elem.requestFullscreen();       // 全屏显示
        } else if (elem.msRequestFullscreen) {
            elem.msRequestFullscreen();     // IE特有方法，全屏显示
        } else if (elem.mozRequestFullScreen) {
            elem.mozRequestFullScreen();    // Firefox特有方法，全屏显示
        } else if (elem.webkitRequestFullscreen) {
            elem.webkitRequestFullscreen(); // Chrome和Safari特有方法，全屏显示
        } else {
            console.warn('未找到全屏显示方法');
        }
    };
    // 退出全屏
    jo.exitScreen = function () {
        if (document.exitFullscreen) {
            document.exitFullscreen();       // 退出全屏
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();     // IE特有方法，退出全屏
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen(); // Firefox特有方法，退出全屏
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();// Chrome和Safari特有方法，退出全屏
        } else {
            console.warn('未找到退出全屏方法');
        }
    };
    // 本地缓存操作
    jo.cache = {
        lock: function (key, timeoutMills) {
            key = 'jo.cache.lock.' + key;
            if (!jo.cache.get(key)) {
                jo.cache.set(key, true, timeoutMills || 300);
                return true;
            }
            return false;
        },
        set: function (key, data, timeoutMills) {
            var param = {
                key: key,// 缓存key
                data: data,// 缓存数据
                timeout: timeoutMills && timeoutMills > 0 ? timeoutMills : 3153600000 * 1000,// 超时时间,默认100年
            };
            if (!param.key) {
                console.error('[cache.set] key无效', param);
                throw new Error("入参缓存key无效");
            }
            if (typeof param.data == 'undefined') {
                console.warn('[cache.set] data is undefined');
                return;
            } else if (typeof param.data == 'symbol') {
                console.error('[cache.set] 暂不支持数据类型[symbol]', param);
                throw new Error("暂不支持数据类型[symbol]");
                // } else if (typeof data == 'boolean') {
                //
                // } else if (typeof data == 'number') {
                //
                // } else if (typeof data == 'string') {
                //
                // } else if (typeof data == 'bigint') {
                //
                // } else if (typeof data == 'object') {
            }
            var now = new Date().getTime();
            var meta = {
                // 数据原始类型
                originalType: typeof param.data,
                // 序列化后存储的值
                value: param.data,
                // 首次保存时间
                firstTime: now,
                // 最近更新时间
                updateTime: now,
                // 过期时间
                expireTime: now + param.timeout,
                // 数据版本号
                dataVersion: 1,
                // 缓存组件版本号
                sdkVersion: 1,
            };
            window.localStorage.setItem(param.key, JSON.stringify(meta));
        },
        get: function (key) {
            var param = {
                key: key,
            };
            if (!param.key) {
                console.error('[cache.get] key无效', param);
                throw new Error("入参缓存key无效");
            }
            var result = window.localStorage.getItem(param.key);
            if (!result) {
                console.debug('[cache.get] 缓存[%s]未命中', param.key);
                return null;
            }
            // 反序列化为元对象
            var meta = JSON.parse(result);
            // 过期处理
            if (meta.expireTime < new Date().getTime()) {
                // 懒删除
                window.localStorage.removeItem(param.key);
                console.debug('[cache.get] 缓存[%s]过期删除:', param.key, meta);
                return null;
            }
            return meta.value;
        },
        del: function (key) {
            var param = {
                key: key,
            };
            if (!param.key) {
                console.error('[cache.del] key无效', param);
                throw new Error("入参缓存key无效");
            }
            var result = window.localStorage.getItem(param.key);
            window.localStorage.removeItem(param.key);
            console.debug('[cache.del] 缓存[%s]过期删除:', param.key, result);
        }
    };
    // base64编码
    jo.encodeBase64 = function (input) {
        var _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;
        input = jo.utf8_encode(input);
        while (i < input.length) {
            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);
            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;
            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }
            output = output +
                _keyStr.charAt(enc1) + _keyStr.charAt(enc2) +
                _keyStr.charAt(enc3) + _keyStr.charAt(enc4);
        }
        return output;
    };
    // base64解码
    jo.decodeBase64 = function (input) {
        var _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;
        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
        while (i < input.length) {
            enc1 = _keyStr.indexOf(input.charAt(i++));
            enc2 = _keyStr.indexOf(input.charAt(i++));
            enc3 = _keyStr.indexOf(input.charAt(i++));
            enc4 = _keyStr.indexOf(input.charAt(i++));
            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;
            output = output + String.fromCharCode(chr1);
            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }
        }
        output = jo.utf8_decode(output);
        return output;
    };
    // private method for UTF-8 encoding
    jo.utf8_encode = function (string) {
        string = string.replace(/\r\n/g, "\n");
        var utftext = "";
        for (var n = 0; n < string.length; n++) {
            var c = string.charCodeAt(n);
            if (c < 128) {
                utftext += String.fromCharCode(c);
            } else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            } else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }
        return utftext;
    };
    // private method for UTF-8 decoding
    jo.utf8_decode = function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;
        while (i < utftext.length) {
            c = utftext.charCodeAt(i);
            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            } else if ((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i + 1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            } else {
                c2 = utftext.charCodeAt(i + 1);
                c3 = utftext.charCodeAt(i + 2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }
        }
        return string;
    };
    // rsa公钥加密
    jo.rsa_public_encrypt = function (input, publicKey) {
        var encrypt = new JSEncrypt();
        encrypt.setPublicKey(publicKey || SSO_RSA_PUBLIC_KEY);//公钥
        return encrypt.encrypt(jo.encodeBase64(input));
    };
    // 防抖
    jo.preventShake = function (key, mills, call) {
        var timer = window['jo_preventShake_' + key];
        if (timer) {
            console.info('[防抖] 移除上一次操作', key, timer);
            window.clearTimeout(timer);
        }
        window['jo_preventShake_' + key] = window.setTimeout(() => {
            call();
        }, mills || 1000);
    };
    // 腾讯云cos对象存储
    jo.cos = {
        config: {
            bucket: window.CONST_COS_BUCKET || '',
            region: window.CONST_COS_REGION || '',
        },
        client: null,
        initClient: function (url, successFunc, errorFunc) {
            var data;
            var credentials;
            url = url || '/fs/cos/sts';
            jo.postAjax(url, {}, json => {
                if (json.code != 0) {
                    console.error('初始化COS认证失败', json);
                    if (typeof errorFunc == 'function') {
                        errorFunc(new Error('authorization error'));
                        return;
                    }
                    throw new Error('authorization error');
                }
                console.debug('[cos初始化sts] cos认证成功:', json);
                data = json.data;
                credentials = data.credentials;
                if (!data || !credentials) {
                    console.error('初始化COS认证失败', json);
                    if (typeof errorFunc == 'function') {
                        errorFunc(new Error('credentials invalid'));
                        return;
                    }
                    throw new Error('credentials invalid');
                }


                try {
                    jo.cos.client = new COS({
                        getAuthorization: function (options, callback) {
                            callback({
                                TmpSecretId: credentials.tmpSecretId,
                                TmpSecretKey: credentials.tmpSecretKey,
                                XCosSecurityToken: credentials.sessionToken,
                                StartTime: data.startTime, // 时间戳，单位秒，如：1580000000，建议返回服务器时间作为签名的开始时间，避免用户浏览器本地时间偏差过大导致签名错误
                                ExpiredTime: data.expiredTime, // 时间戳，单位秒，如：1580000900
                            });
                        }
                    });
                } catch (e) {
                    console.error('创建COS实例异常', e);
                    if (typeof errorFunc == 'function') {
                        errorFunc(new Error('create COS instance error'));
                        return;
                    }
                    throw e;
                }


                if (typeof successFunc == 'function') {
                    successFunc();
                }
            });
        },
        upload: function (param) {
            if (!param.file) {
                throw new Error('文件信息无效!');
            }
            console.debug('[cos上传] 参数:', param);
            if (!jo.cos.client) {
                console.debug('[cos上传] 初始化临时令牌认证:', param);
                jo.cos.initClient(param.stsUrl, () => {
                    jo.cos.do_upload_inner(param);
                }, (e) => {
                    console.error('[cos上传] 初始化COS客户端异常', e);
                    if (typeof param.error == 'function') {
                        param.error(e, {});
                    }
                });

            } else {
                jo.cos.do_upload_inner(param);
            }
        },
        do_upload_inner: function ({file, fileId, bucket, region, stsUrl, success, error}) {
            var _bucket = jo.getDefVal(bucket, jo.cos.config.bucket);
            var _region = jo.getDefVal(region, jo.cos.config.region);
            var _key = fileId || file.size + '_' + file.lastModified + '_' + file.name;
            if (!_bucket) {
                throw new Error('参数无效:bucket');
            }
            if (!_region) {
                throw new Error('参数无效:region');
            }
            console.info('[cos上传] 最终上传cos参数bucket/region/key/file:', [_bucket, _region, _key, file]);
            jo.cos.client.uploadFile({
                Bucket: _bucket,
                Region: _region,
                Key: _key,
                Body: file,
                onHashProgress: function (progressData) {
                    console.log('[cos上传] 校验中:', progressData);
                },
                onProgress: function (progressData) {
                    console.log('[cos上传] 上传中:', progressData);
                },
            }, (err, data) => {
                console.log("[cos上传] 结果:", err, data);
                if (data && data.statusCode == 200) {
                    if (typeof success == 'function') {
                        success(err, data);
                    }
                } else {
                    if (typeof error == 'function') {
                        error(err, data);
                    }
                }
            });
        },
    };
})();
//jo初始化完成