/*element plus 适配器*/
(function () {
    if (typeof (ElementPlus) == "undefined" || !ElementPlus) {
        console.error("jo-adapt-element-plus初始化失败:ElementPlus依赖无效!");
        return;
    }
    /**
     * [Func] alert提示
     * @param sMsg
     * @param options
     * @param yesCall
     * @returns {*}
     */
    jo.alert = function(sMsg, options, yesCall) {
        var op = {};
        if (jo.isValid(options)) {
            if (typeof options == 'object') {
                op = options;
            } else if (typeof options == 'string') {
                op.title = options;
            } else if (typeof options == 'function') {
                op.yesCall = options;
            }
        }
        if (typeof yesCall == 'function') {
            op.yesCall = options;
        }
        return ElementPlus.ElMessageBox.alert(sMsg, op.title || '信息', {
            confirmButtonText: op.confirmButtonText || '确定',
            callback: function (action) {
                if (typeof op.yesCall == 'function') {
                    op.yesCall(action);
                }
            }
        });
    };
    /**
     * [Func] confirm选择框,参数会自动向左合并,即当options为function时表示该参数为yesCall
     * @param sMsg
     * @param yesCall
     * @param noCall
     * @param options
     * @returns {*}
     */
    jo.confirm = function(sMsg, options, yesCall, noCall){
        var title = jo.getDefVal(typeof options == 'object' ? options.title : '', '确认');
        if (typeof options == 'function') {
            noCall = yesCall;
            yesCall = options;
        }
        return ElementPlus.ElMessageBox.confirm(
            sMsg,
            title,
            {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning',
            }
        ).then(() => {
            if (typeof yesCall == 'function') {
                yesCall();
            }
        }).catch(() => {
            if (typeof noCall == 'function') {
                noCall();
            }
        });
    };
    /**
     * 输入层
     * @param options 例如:{title:'标题',msg:'输入提示文案'}
     * @param yesCall 成功回调,包含3个参数value, index, elem
     * @param noCall 取消回调
     */
    jo.prompt = function(options, yesCall, noCall){
        return ElementPlus.ElMessageBox.prompt(
            jo.getDefVal(options.msg, ''),
            jo.getDefVal(options.title, '请输入'),
            {
                confirmButtonText: '确定',
                cancelButtonText: '取消'
            }
        ).then((res) => {
            if (typeof yesCall == 'function') {
                yesCall(res.value);
            }
        }).catch(() => {
            if (typeof noCall == 'function') {
                noCall();
            }
        });
    };
    /**
     * [Func] 加载提示
     * @param msgOrClose
     * @returns {*}
     */
    jo.showLoading = function(show){
        if(show === false && window.elLoadingObj){
            return window.elLoadingObj.close();//关闭
        }
        var loading = ElementPlus.ElLoading.service({
            lock: true,
            text: 'Loading',
            background: 'rgba(0, 0, 0, 0.7)',
        });
        if (window.elLoadingObj) {
            console.warn('[showLoading] 存在未关闭的loading,请检查是否重复loading,旧loading对象:', window.elLoadingObj);
        }
        window.elLoadingObj = loading;
        return loading;
    };
    /**
     * [Func] 消息提示,参数2为function时,表示为结束回调
     * @param sMsg 内容
     * @param options
     * @param endCall
     * @returns {*}
     */
    jo.showMsg = function (sMsg, options, endCall) {
        return ElementPlus.ElMessage({message: sMsg, showClose: true});
    };
    /**
     * [Func] 展示提示消息
     * @param sMsg
     * @param options
     * @param endCall
     * @returns {*}
     */
    jo.showTipsMsg = function (sMsg, options, endCall) {
        return ElementPlus.ElMessage({message: sMsg, showClose: true});
    };
    /**
     * [Func] 提示成功消息
     * @param sMsg
     * @param options
     * @param endCall
     * @returns {*}
     */
    jo.showSuccessMsg = function (sMsg, options, endCall) {
        return ElementPlus.ElMessage({
            message: sMsg,
            type: 'success',
            showClose: true
        });
    };
    /**
     * [Func] 提示错误消息
     * @param sMsg 内容
     * @param options
     * @param endCall
     * @returns {*}
     */
    jo.showErrorMsg = function (sMsg, options, endCall) {
        return ElementPlus.ElMessage({
            message: sMsg,
            type: 'error',
            showClose: true
        });
    };
    /**
     * [Func] 提示疑问消息
     * @param sMsg
     * @param options
     * @param endCall
     * @returns {*}
     */
    jo.showDoubtMsg = function (sMsg, options, endCall) {
        return ElementPlus.ElMessage({message: sMsg, showClose: true});
    };
    /**
     * [Func] 提示锁定消息
     * @param sMsg
     * @param options
     * @param endCall
     * @returns {*}
     */
    jo.showLockMsg = function (sMsg, options, endCall) {
        return ElementPlus.ElMessage({message: sMsg, showClose: true});
    };
    /**
     * [Func] 提示哭脸表情的消息
     * @param sMsg
     * @param options
     * @param endCall
     * @returns {*}
     */
    jo.showCryMsg = function (sMsg, options, endCall) {
        return ElementPlus.ElMessage({
            message: sMsg,
            type: 'error',
            showClose: true
        });
    };
    /**
     * [Func] 提示笑脸表情的消息
     * @param sMsg
     * @param options
     * @param endCall
     * @returns {*}
     */
    jo.showSmileMsg = function (sMsg, options, endCall) {
        return ElementPlus.ElMessage({
            message: sMsg,
            type: 'success',
            showClose: true
        });
    };
})();