// 基于element-plus的视图组件
window.joel = window.joEl = {};

// 国际化
(function () {
    // 国际化
    joEl.i18n = {};
    // 国际化内容集合
    joEl.i18n_map = {
        cn: {
            key: 'cn',
            name: '简体中文',
            test: '国际化测试',
        },
        en: {
            key: 'en',
            name: 'English',
            test: 'i18n test',
        }
    };
    // 注册国际化对象
    joEl.registerI18n = function (key, i18n) {
        joEl.i18n_map[key] = i18n;
        console.debug('注册国际化对象', key, i18n);
    };
    // 切换国际化对象
    joEl.useI18n = function (key) {
        var i18n = joEl.i18n_map[key];
        if (i18n) {
            // 覆盖国际化对象
            for (var k in i18n) {
                joEl.i18n[k] = i18n[k];
            }
            console.info('[切换国际化] key、被选中的国际化对象、合并后最终国际化对象：', [key, i18n, joEl.i18n]);
        }
    };
    // 默认使用中文
    joEl.useI18n('cn');
})();

// joEl组件
(function () {
    joEl.component = joEl.install = function (app) {
        // 注册组件
        for (var k in joEl.COMPONENT_MAP) {
            app.component(k, joEl.COMPONENT_MAP[k]);
        }
    };
    // 临时组件
    joEl.register = function (componentName, componentConfig) {
        joEl.COMPONENT_MAP[componentName] = componentConfig;
    };
    /**
     * 获取ref对象
     * @param _this vue实例,直接传this
     * @param refName ref名字
     * @returns {*|null}
     */
    joEl.getRef = function (_this, refName) {
        // 先获取子ref
        var ref = _this.$refs[refName];
        // ref无效则从根节点开始找
        if (!ref) {
            ref = joEl.findRefRecursion(_this.$root, refName);
        }
        return ref;
    };
    /**
     * 递归查找ref
     * @param refIns ref实例
     * @param refName 要查找的ref名字
     * @returns {null|*}
     */
    joEl.findRefRecursion = function (refIns, refName) {
        if (!refIns || !refIns.$refs) {
            return null;
        }
        var ref = refIns.$refs[refName];
        if (!ref) {
            for (var k in refIns.$refs) {
                ref = joEl.findRefRecursion(refIns.$refs[k], refName);
                if (ref) {
                    break;
                }
            }
        }
        return ref;
    };
    // el表单校验规则
    joEl.rules = {
        required: function (msg, trigger) {
            return {required: true, message: msg || '必填项', trigger: trigger || 'blur'};
        },
        range: function (msg, trigger, min, max) {
            return {min: min, max: max, message: msg || '超出范围', trigger: trigger || 'blur'};
        },
        length: function (msg, trigger, len) {
            return {
                validator: function (rule, value, callback) {
                    if (value && value.length > len) {
                        callback(new Error(msg || ('长度超出限制:' + len)));
                    } else {
                        callback();
                    }
                }
                , trigger: trigger || 'blur'
            };
        },
        // 是否在枚举范围内
        enums: function (msg, trigger, enumArr) {
            return {
                validator: function (rule, value, callback) {
                    var flag = false;
                    for (let i = 0; i < enumArr.length; i++) {
                        var e = enumArr[i];
                        if (e == value) {
                            flag = true;
                            break;
                        }
                    }
                    if (flag) {
                        callback();
                    } else {
                        callback(new Error(msg || '不在枚举范围内'));
                    }
                }
                , trigger: trigger || 'blur'
            };
        },
        mail: function (msg, trigger) {
            return {
                validator: function (rule, value, callback) {
                    if (value) {
                        if (!jo.Const.REG_MAIL.test(value)) {
                            callback(new Error(msg || '邮箱格式错误'));
                            return;
                        }
                    }
                    callback();
                }
                , trigger: trigger || 'blur'
            };
        },
        phone: function (msg, trigger) {
            return {
                validator: function (rule, value, callback) {
                    if (value) {
                        if (!jo.Const.REG_MOBILEPHONE.test(value)) {
                            callback(new Error(msg || '手机号格式错误'));
                            return;
                        }
                    }
                    callback();
                }
                , trigger: trigger || 'blur'
            };
        },
        reg: function (msg, trigger, reg) {
            return {
                validator: function (rule, value, callback) {
                    if (value) {
                        if (!new RegExp(reg).test(value)) {
                            callback(new Error(msg || '格式错误'));
                            return;
                        }
                    }
                    callback();
                }
                , trigger: trigger || 'blur'
            };
        },
        notNaN: function (msg, trigger) {
            return {
                validator: function (rule, value, callback) {
                    if (value) {
                        if (isNaN(value)) {
                            callback(new Error(msg || '非数错误'));
                            return;
                        }
                    }
                    callback();
                }
                , trigger: trigger || 'blur'
            };
        },


        date: function (msg, trigger) {
            return {type: 'date', message: msg || '必须是时间类型', trigger: trigger || 'blur'};
        },
        array: function (msg, trigger) {
            return {type: 'array', message: msg || '必须是数组类型', trigger: trigger || 'blur'};
        },
        object: function (msg, trigger) {
            return {type: 'object', message: msg || '必须是对象类型', trigger: trigger || 'blur'};
        },
        number: function (msg, trigger) {
            return {type: 'number', message: msg || '必须是数字类型', trigger: trigger || 'blur'};
        },
        string: function (msg, trigger) {
            return {type: 'string', message: msg || '必须是字符串类型', trigger: trigger || 'blur'};
        },
    };
    // joEl常量
    joEl.constants = {
        // 图标选项
        iconOptions: [{
            "name": "Web Application Icons",
            "icons": ["fa-address-book", "fa-address-book-o", "fa-address-card", "fa-address-card-o", "fa-adjust", "fa-american-sign-language-interpreting", "fa-anchor", "fa-archive", "fa-area-chart", "fa-arrows", "fa-arrows-h", "fa-arrows-v", "fa-asl-interpreting", "fa-assistive-listening-systems", "fa-asterisk", "fa-at", "fa-audio-description", "fa-automobile", "fa-balance-scale", "fa-ban", "fa-bank", "fa-bar-chart", "fa-bar-chart-o", "fa-barcode", "fa-bars", "fa-bath", "fa-bathtub", "fa-battery", "fa-battery-0", "fa-battery-1", "fa-battery-2", "fa-battery-3", "fa-battery-4", "fa-battery-empty", "fa-battery-full", "fa-battery-half", "fa-battery-quarter", "fa-battery-three-quarters", "fa-bed", "fa-beer", "fa-bell", "fa-bell-o", "fa-bell-slash", "fa-bell-slash-o", "fa-bicycle", "fa-binoculars", "fa-birthday-cake", "fa-blind", "fa-bluetooth", "fa-bluetooth-b", "fa-bolt", "fa-bomb", "fa-book", "fa-bookmark", "fa-bookmark-o", "fa-braille", "fa-briefcase", "fa-bug", "fa-building", "fa-building-o", "fa-bullhorn", "fa-bullseye", "fa-bus", "fa-cab", "fa-calculator", "fa-calendar", "fa-calendar-check-o", "fa-calendar-minus-o", "fa-calendar-o", "fa-calendar-plus-o", "fa-calendar-times-o", "fa-camera", "fa-camera-retro", "fa-car", "fa-caret-square-o-down", "fa-caret-square-o-left", "fa-caret-square-o-right", "fa-caret-square-o-up", "fa-cart-arrow-down", "fa-cart-plus", "fa-cc", "fa-certificate", "fa-check", "fa-check-circle", "fa-check-circle-o", "fa-check-square", "fa-check-square-o", "fa-child", "fa-circle", "fa-circle-o", "fa-circle-o-notch", "fa-circle-thin", "fa-clock-o", "fa-clone", "fa-close", "fa-cloud", "fa-cloud-download", "fa-cloud-upload", "fa-code", "fa-code-fork", "fa-coffee", "fa-cog", "fa-cogs", "fa-comment", "fa-comment-o", "fa-commenting", "fa-commenting-o", "fa-comments", "fa-comments-o", "fa-compass", "fa-copyright", "fa-creative-commons", "fa-credit-card", "fa-credit-card-alt", "fa-crop", "fa-crosshairs", "fa-cube", "fa-cubes", "fa-cutlery", "fa-dashboard", "fa-database", "fa-deaf", "fa-deafness", "fa-desktop", "fa-diamond", "fa-dot-circle-o", "fa-download", "fa-drivers-license", "fa-drivers-license-o", "fa-edit", "fa-ellipsis-h", "fa-ellipsis-v", "fa-envelope", "fa-envelope-o", "fa-envelope-open", "fa-envelope-open-o", "fa-envelope-square", "fa-eraser", "fa-exchange", "fa-exclamation", "fa-exclamation-circle", "fa-exclamation-triangle", "fa-external-link", "fa-external-link-square", "fa-eye", "fa-eye-slash", "fa-eyedropper", "fa-fax", "fa-feed", "fa-female", "fa-fighter-jet", "fa-file-archive-o", "fa-file-audio-o", "fa-file-code-o", "fa-file-excel-o", "fa-file-image-o", "fa-file-movie-o", "fa-file-pdf-o", "fa-file-photo-o", "fa-file-picture-o", "fa-file-powerpoint-o", "fa-file-sound-o", "fa-file-video-o", "fa-file-word-o", "fa-file-zip-o", "fa-film", "fa-filter", "fa-fire", "fa-fire-extinguisher", "fa-flag", "fa-flag-checkered", "fa-flag-o", "fa-flash", "fa-flask", "fa-folder", "fa-folder-o", "fa-folder-open", "fa-folder-open-o", "fa-frown-o", "fa-futbol-o", "fa-gamepad", "fa-gavel", "fa-gear", "fa-gears", "fa-gift", "fa-glass", "fa-globe", "fa-graduation-cap", "fa-group", "fa-hand-grab-o", "fa-hand-lizard-o", "fa-hand-paper-o", "fa-hand-peace-o", "fa-hand-pointer-o", "fa-hand-rock-o", "fa-hand-scissors-o", "fa-hand-spock-o", "fa-hand-stop-o", "fa-handshake-o", "fa-hard-of-hearing", "fa-hashtag", "fa-hdd-o", "fa-headphones", "fa-heart", "fa-heart-o", "fa-heartbeat", "fa-history", "fa-home", "fa-hotel", "fa-hourglass", "fa-hourglass-1", "fa-hourglass-2", "fa-hourglass-3", "fa-hourglass-end", "fa-hourglass-half", "fa-hourglass-o", "fa-hourglass-start", "fa-i-cursor", "fa-id-badge", "fa-id-card", "fa-id-card-o", "fa-image", "fa-inbox", "fa-industry", "fa-info", "fa-info-circle", "fa-institution", "fa-key", "fa-keyboard-o", "fa-language", "fa-laptop", "fa-leaf", "fa-legal", "fa-lemon-o", "fa-level-down", "fa-level-up", "fa-life-bouy", "fa-life-buoy", "fa-life-ring", "fa-life-saver", "fa-lightbulb-o", "fa-line-chart", "fa-location-arrow", "fa-lock", "fa-low-vision", "fa-magic", "fa-magnet", "fa-mail-forward", "fa-mail-reply", "fa-mail-reply-all", "fa-male", "fa-map", "fa-map-marker", "fa-map-o", "fa-map-pin", "fa-map-signs", "fa-meh-o", "fa-microchip", "fa-microphone", "fa-microphone-slash", "fa-minus", "fa-minus-circle", "fa-minus-square", "fa-minus-square-o", "fa-mobile", "fa-mobile-phone", "fa-money", "fa-moon-o", "fa-mortar-board", "fa-motorcycle", "fa-mouse-pointer", "fa-music", "fa-navicon", "fa-newspaper-o", "fa-object-group", "fa-object-ungroup", "fa-paint-brush", "fa-paper-plane", "fa-paper-plane-o", "fa-paw", "fa-pencil", "fa-pencil-square", "fa-pencil-square-o", "fa-percent", "fa-phone", "fa-phone-square", "fa-photo", "fa-picture-o", "fa-pie-chart", "fa-plane", "fa-plug", "fa-plus", "fa-plus-circle", "fa-plus-square", "fa-plus-square-o", "fa-podcast", "fa-power-off", "fa-print", "fa-puzzle-piece", "fa-qrcode", "fa-question", "fa-question-circle", "fa-question-circle-o", "fa-quote-left", "fa-quote-right", "fa-random", "fa-recycle", "fa-refresh", "fa-registered", "fa-remove", "fa-reorder", "fa-reply", "fa-reply-all", "fa-retweet", "fa-road", "fa-rocket", "fa-rss", "fa-rss-square", "fa-s15", "fa-search", "fa-search-minus", "fa-search-plus", "fa-send", "fa-send-o", "fa-server", "fa-share", "fa-share-alt", "fa-share-alt-square", "fa-share-square", "fa-share-square-o", "fa-shield", "fa-ship", "fa-shopping-bag", "fa-shopping-basket", "fa-shopping-cart", "fa-shower", "fa-sign-in", "fa-sign-language", "fa-sign-out", "fa-signal", "fa-signing", "fa-sitemap", "fa-sliders", "fa-smile-o", "fa-snowflake-o", "fa-soccer-ball-o", "fa-sort", "fa-sort-alpha-asc", "fa-sort-alpha-desc", "fa-sort-amount-asc", "fa-sort-amount-desc", "fa-sort-asc", "fa-sort-desc", "fa-sort-down", "fa-sort-numeric-asc", "fa-sort-numeric-desc", "fa-sort-up", "fa-space-shuttle", "fa-spinner", "fa-spoon", "fa-square", "fa-square-o", "fa-star", "fa-star-half", "fa-star-half-empty", "fa-star-half-full", "fa-star-half-o", "fa-star-o", "fa-sticky-note", "fa-sticky-note-o", "fa-street-view", "fa-suitcase", "fa-sun-o", "fa-support", "fa-tablet", "fa-tachometer", "fa-tag", "fa-tags", "fa-tasks", "fa-taxi", "fa-television", "fa-terminal", "fa-thermometer", "fa-thermometer-0", "fa-thermometer-1", "fa-thermometer-2", "fa-thermometer-3", "fa-thermometer-4", "fa-thermometer-empty", "fa-thermometer-full", "fa-thermometer-half", "fa-thermometer-quarter", "fa-thermometer-three-quarters", "fa-thumb-tack", "fa-thumbs-down", "fa-thumbs-o-down", "fa-thumbs-o-up", "fa-thumbs-up", "fa-ticket", "fa-times", "fa-times-circle", "fa-times-circle-o", "fa-times-rectangle", "fa-times-rectangle-o", "fa-tint", "fa-toggle-down", "fa-toggle-left", "fa-toggle-off", "fa-toggle-on", "fa-toggle-right", "fa-toggle-up", "fa-trademark", "fa-trash", "fa-trash-o", "fa-tree", "fa-trophy", "fa-truck", "fa-tty", "fa-tv", "fa-umbrella", "fa-universal-access", "fa-university", "fa-unlock", "fa-unlock-alt", "fa-unsorted", "fa-upload", "fa-user", "fa-user-circle", "fa-user-circle-o", "fa-user-o", "fa-user-plus", "fa-user-secret", "fa-user-times", "fa-users", "fa-vcard", "fa-vcard-o", "fa-video-camera", "fa-volume-control-phone", "fa-volume-down", "fa-volume-off", "fa-volume-up", "fa-warning", "fa-wheelchair", "fa-wheelchair-alt", "fa-wifi", "fa-window-close", "fa-window-close-o", "fa-window-maximize", "fa-window-minimize", "fa-window-restore", "fa-wrench"]
        }, {
            "name": "Accessibility Icons",
            "icons": ["fa-american-sign-language-interpreting", "fa-asl-interpreting", "fa-assistive-listening-systems", "fa-audio-description", "fa-blind", "fa-braille", "fa-cc", "fa-deaf", "fa-deafness", "fa-hard-of-hearing", "fa-low-vision", "fa-question-circle-o", "fa-sign-language", "fa-signing", "fa-tty", "fa-universal-access", "fa-volume-control-phone", "fa-wheelchair", "fa-wheelchair-alt"]
        }, {
            "name": "Hand Icons",
            "icons": ["fa-hand-grab-o", "fa-hand-lizard-o", "fa-hand-o-down", "fa-hand-o-left", "fa-hand-o-right", "fa-hand-o-up", "fa-hand-paper-o", "fa-hand-peace-o", "fa-hand-pointer-o", "fa-hand-rock-o", "fa-hand-scissors-o", "fa-hand-spock-o", "fa-hand-stop-o", "fa-thumbs-down", "fa-thumbs-o-down", "fa-thumbs-o-up", "fa-thumbs-up"]
        }, {
            "name": "Transportation Icons",
            "icons": ["fa-ambulance", "fa-automobile", "fa-bicycle", "fa-bus", "fa-cab", "fa-car", "fa-fighter-jet", "fa-motorcycle", "fa-plane", "fa-rocket", "fa-ship", "fa-space-shuttle", "fa-subway", "fa-taxi", "fa-train", "fa-truck", "fa-wheelchair", "fa-wheelchair-alt"]
        }, {
            "name": "Gender Icons",
            "icons": ["fa-genderless", "fa-intersex", "fa-mars", "fa-mars-double", "fa-mars-stroke", "fa-mars-stroke-h", "fa-mars-stroke-v", "fa-mercury", "fa-neuter", "fa-transgender", "fa-transgender-alt", "fa-venus", "fa-venus-double", "fa-venus-mars"]
        }, {
            "name": "File Type Icons",
            "icons": ["fa-file", "fa-file-archive-o", "fa-file-audio-o", "fa-file-code-o", "fa-file-excel-o", "fa-file-image-o", "fa-file-movie-o", "fa-file-o", "fa-file-pdf-o", "fa-file-photo-o", "fa-file-picture-o", "fa-file-powerpoint-o", "fa-file-sound-o", "fa-file-text", "fa-file-text-o", "fa-file-video-o", "fa-file-word-o", "fa-file-zip-o"]
        }, {
            "name": "Spinner Icons",
            "icons": ["fa-circle-o-notch", "fa-cog", "fa-gear", "fa-refresh", "fa-spinner"]
        }, {
            "name": "Form Control Icons",
            "icons": ["fa-check-square", "fa-check-square-o", "fa-circle", "fa-circle-o", "fa-dot-circle-o", "fa-minus-square", "fa-minus-square-o", "fa-plus-square", "fa-plus-square-o", "fa-square", "fa-square-o"]
        }, {
            "name": "Payment Icons",
            "icons": ["fa-cc-amex", "fa-cc-diners-club", "fa-cc-discover", "fa-cc-jcb", "fa-cc-mastercard", "fa-cc-paypal", "fa-cc-stripe", "fa-cc-visa", "fa-credit-card", "fa-credit-card-alt", "fa-google-wallet", "fa-paypal"]
        }, {
            "name": "Chart Icons",
            "icons": ["fa-area-chart", "fa-bar-chart", "fa-bar-chart-o", "fa-line-chart", "fa-pie-chart"]
        }, {
            "name": "Currency Icons",
            "icons": ["fa-bitcoin", "fa-btc", "fa-cny", "fa-dollar", "fa-eur", "fa-euro", "fa-gbp", "fa-gg", "fa-gg-circle", "fa-ils", "fa-inr", "fa-jpy", "fa-krw", "fa-money", "fa-rmb", "fa-rouble", "fa-rub", "fa-ruble", "fa-rupee", "fa-shekel", "fa-sheqel", "fa-try", "fa-turkish-lira", "fa-usd", "fa-won", "fa-yen"]
        }, {
            "name": "Text Editor Icons",
            "icons": ["fa-align-center", "fa-align-justify", "fa-align-left", "fa-align-right", "fa-bold", "fa-chain", "fa-chain-broken", "fa-clipboard", "fa-columns", "fa-copy", "fa-cut", "fa-dedent", "fa-eraser", "fa-file", "fa-file-o", "fa-file-text", "fa-file-text-o", "fa-files-o", "fa-floppy-o", "fa-font", "fa-header", "fa-indent", "fa-italic", "fa-link", "fa-list", "fa-list-alt", "fa-list-ol", "fa-list-ul", "fa-outdent", "fa-paperclip", "fa-paragraph", "fa-paste", "fa-repeat", "fa-rotate-left", "fa-rotate-right", "fa-save", "fa-scissors", "fa-strikethrough", "fa-subscript", "fa-superscript", "fa-table", "fa-text-height", "fa-text-width", "fa-th", "fa-th-large", "fa-th-list", "fa-underline", "fa-undo", "fa-unlink"]
        }, {
            "name": "Directional Icons",
            "icons": ["fa-angle-double-down", "fa-angle-double-left", "fa-angle-double-right", "fa-angle-double-up", "fa-angle-down", "fa-angle-left", "fa-angle-right", "fa-angle-up", "fa-arrow-circle-down", "fa-arrow-circle-left", "fa-arrow-circle-o-down", "fa-arrow-circle-o-left", "fa-arrow-circle-o-right", "fa-arrow-circle-o-up", "fa-arrow-circle-right", "fa-arrow-circle-up", "fa-arrow-down", "fa-arrow-left", "fa-arrow-right", "fa-arrow-up", "fa-arrows", "fa-arrows-alt", "fa-arrows-h", "fa-arrows-v", "fa-caret-down", "fa-caret-left", "fa-caret-right", "fa-caret-square-o-down", "fa-caret-square-o-left", "fa-caret-square-o-right", "fa-caret-square-o-up", "fa-caret-up", "fa-chevron-circle-down", "fa-chevron-circle-left", "fa-chevron-circle-right", "fa-chevron-circle-up", "fa-chevron-down", "fa-chevron-left", "fa-chevron-right", "fa-chevron-up", "fa-exchange", "fa-hand-o-down", "fa-hand-o-left", "fa-hand-o-right", "fa-hand-o-up", "fa-long-arrow-down", "fa-long-arrow-left", "fa-long-arrow-right", "fa-long-arrow-up", "fa-toggle-down", "fa-toggle-left", "fa-toggle-right", "fa-toggle-up"]
        }, {
            "name": "Video Player Icons",
            "icons": ["fa-arrows-alt", "fa-backward", "fa-compress", "fa-eject", "fa-expand", "fa-fast-backward", "fa-fast-forward", "fa-forward", "fa-pause", "fa-pause-circle", "fa-pause-circle-o", "fa-play", "fa-play-circle", "fa-play-circle-o", "fa-random", "fa-step-backward", "fa-step-forward", "fa-stop", "fa-stop-circle", "fa-stop-circle-o", "fa-youtube-play"]
        }, {
            "name": "Brand Icons",
            "icons": ["fa-500px", "fa-adn", "fa-amazon", "fa-android", "fa-angellist", "fa-apple", "fa-bandcamp", "fa-behance", "fa-behance-square", "fa-bitbucket", "fa-bitbucket-square", "fa-bitcoin", "fa-black-tie", "fa-bluetooth", "fa-bluetooth-b", "fa-btc", "fa-buysellads", "fa-cc-amex", "fa-cc-diners-club", "fa-cc-discover", "fa-cc-jcb", "fa-cc-mastercard", "fa-cc-paypal", "fa-cc-stripe", "fa-cc-visa", "fa-chrome", "fa-codepen", "fa-codiepie", "fa-connectdevelop", "fa-contao", "fa-css3", "fa-dashcube", "fa-delicious", "fa-deviantart", "fa-digg", "fa-dribbble", "fa-dropbox", "fa-drupal", "fa-edge", "fa-eercast", "fa-empire", "fa-envira", "fa-etsy", "fa-expeditedssl", "fa-fa", "fa-facebook", "fa-facebook-f", "fa-facebook-official", "fa-facebook-square", "fa-firefox", "fa-first-order", "fa-flickr", "fa-font-awesome", "fa-fonticons", "fa-fort-awesome", "fa-forumbee", "fa-foursquare", "fa-free-code-camp", "fa-ge", "fa-get-pocket", "fa-gg", "fa-gg-circle", "fa-git", "fa-git-square", "fa-github", "fa-github-alt", "fa-github-square", "fa-gitlab", "fa-gittip", "fa-glide", "fa-glide-g", "fa-google", "fa-google-plus", "fa-google-plus-circle", "fa-google-plus-official", "fa-google-plus-square", "fa-google-wallet", "fa-gratipay", "fa-grav", "fa-hacker-news", "fa-houzz", "fa-html5", "fa-imdb", "fa-instagram", "fa-internet-explorer", "fa-ioxhost", "fa-joomla", "fa-jsfiddle", "fa-lastfm", "fa-lastfm-square", "fa-leanpub", "fa-linkedin", "fa-linkedin-square", "fa-linode", "fa-linux", "fa-maxcdn", "fa-meanpath", "fa-medium", "fa-meetup", "fa-mixcloud", "fa-modx", "fa-odnoklassniki", "fa-odnoklassniki-square", "fa-opencart", "fa-openid", "fa-opera", "fa-optin-monster", "fa-pagelines", "fa-paypal", "fa-pied-piper", "fa-pied-piper-alt", "fa-pied-piper-pp", "fa-pinterest", "fa-pinterest-p", "fa-pinterest-square", "fa-product-hunt", "fa-qq", "fa-quora", "fa-ra", "fa-ravelry", "fa-rebel", "fa-reddit", "fa-reddit-alien", "fa-reddit-square", "fa-renren", "fa-resistance", "fa-safari", "fa-scribd", "fa-sellsy", "fa-share-alt", "fa-share-alt-square", "fa-shirtsinbulk", "fa-simplybuilt", "fa-skyatlas", "fa-skype", "fa-slack", "fa-slideshare", "fa-snapchat", "fa-snapchat-ghost", "fa-snapchat-square", "fa-soundcloud", "fa-spotify", "fa-stack-exchange", "fa-stack-overflow", "fa-steam", "fa-steam-square", "fa-stumbleupon", "fa-stumbleupon-circle", "fa-superpowers", "fa-telegram", "fa-tencent-weibo", "fa-themeisle", "fa-trello", "fa-tripadvisor", "fa-tumblr", "fa-tumblr-square", "fa-twitch", "fa-twitter", "fa-twitter-square", "fa-usb", "fa-viacoin", "fa-viadeo", "fa-viadeo-square", "fa-vimeo", "fa-vimeo-square", "fa-vine", "fa-vk", "fa-wechat", "fa-weibo", "fa-weixin", "fa-whatsapp", "fa-wikipedia-w", "fa-windows", "fa-wordpress", "fa-wpbeginner", "fa-wpexplorer", "fa-wpforms", "fa-xing", "fa-xing-square", "fa-y-combinator", "fa-y-combinator-square", "fa-yahoo", "fa-yc", "fa-yc-square", "fa-yelp", "fa-yoast", "fa-youtube", "fa-youtube-play", "fa-youtube-square"]
        }, {
            "name": "Medical Icons",
            "icons": ["fa-ambulance", "fa-h-square", "fa-heart", "fa-heart-o", "fa-heartbeat", "fa-hospital-o", "fa-medkit", "fa-plus-square", "fa-stethoscope", "fa-user-md", "fa-wheelchair", "fa-wheelchair-alt"]
        }],
    };
    // 用户拥有的权限code容器
    joEl.auth_code_map = {};
})();

// 事件总线
(function () {
    joEl.bus = {
        listener: {
            //     '事件名': {
            //         '消费方': '事件callback'
            //     }
        },
        // 发布事件
        emit: function (eventName, data) {
            let callMap = this.listener[eventName] || {};
            for (let k in callMap) {
                let callback = callMap[k];
                if (typeof callback == 'function') {
                    try {
                        callback(data);
                    } catch (e) {
                        console.error('事件回调时异常,事件名:%s、消费方:%s、异常信息:', eventName, k, e);
                    }
                } else {
                    console.error('事件回调未找到,事件名:%s、消费方:%s、callback:', eventName, k, callback);
                }
            }
        },
        // 监听事件
        on: function (eventName, consumer, callback) {
            let callMap = this.listener[eventName] || {};
            callMap[consumer] = callback;
            this.listener[eventName] = callMap;
            console.info('监听事件,事件名:%s、消费方:%s', eventName, consumer);
        },
        // 取消监听
        off: function (eventName, consumer) {
            let callMap = this.listener[eventName];
            if (callMap && callMap[consumer]) {
                delete callMap[consumer];
                console.info('取消监听事件,事件名:%s、消费方:%s', eventName, consumer);
                return;
            }
            console.warn('取消监听事件失败,未找到对应事件监听,事件名:%s、消费方:%s', eventName, consumer);
        }
    };
})();

// joEl方法库
(function () {
    // 当key不存在时赋值
    joEl.putIfNoValue = function (obj, key, value) {
        if (!jo.isValid(key)) {
            console.warn('当前key无效:' + key);
        } else if (jo.isNotUndefined(obj[key])) {
            console.info('当前key已存在:' + key + ', 不替换原属性,原属性和本次未生效属性分别为:%o', [obj[key], value]);
        } else {
            obj[key] = value;
        }
    };

    /**
     * 构造vue参数, 多个参数合并为一个参数, 优先级为入参顺序
     * @param param 定制参数
     * @param commonParam 合并参数
     * @returns {*}
     */
    joEl.buildVueAppParam = function (param, commonParam) {
        if (arguments.length > 2) {
            for (var i = 1; i < arguments.length; i++) {
                var p = arguments[i];
                param = joEl.buildVueAppParamInvoke(param, p, i);
            }
        } else if (commonParam) {
            param = joEl.buildVueAppParamInvoke(param, commonParam);
        }
        // 全局参数
        var global = {
            data: function () {
                return {
                    qy_global_i18n: joEl.i18n,
                    qy_version: 1
                };
            },
            computed: {
                // 国际化
                qy_i18n() {
                    return this.qy_global_i18n;
                },
                // 国际化key
                qy_i18n_key() {
                    return this.qy_global_i18n.key;
                },
            },
            methods: {
                // 切换国际化
                qy_use_i18n(key) {
                    var i18n = joEl.i18n_map[key];
                    if (i18n) {
                        // 覆盖国际化对象
                        for (var k in i18n) {
                            this.qy_global_i18n[k] = i18n[k];
                        }
                        console.info('[切换国际化] key、被选中的国际化对象、合并后最终国际化对象：', [key, i18n, this.qy_global_i18n]);
                    }
                },
                // 页面加载完成
                qy_page_loaded() {
                    document.body.classList.add('qy-loaded');
                }
            }
        };
        param = joEl.buildVueAppParamInvoke(param, global, '__global');
        return param;
    };

    // 实际执行vue参数合并函数, idx表示序号参数合并序号, 从1开始
    joEl.buildVueAppParamInvoke = function (param, commonParam, idx) {
        if (!commonParam) {
            return param;
        }
        // 初始化方法
        if (!param.methods) {
            param.methods = {};
        }
        idx = jo.getDefVal(idx, 1);
        var preData = 'data__backup';
        var preData_idx = 'data__backup' + idx;
        var preCommonData = 'common__data__backup';
        var preCommonData_idx = 'common__data__backup' + idx;
        // 暂存定制data方法
        param.methods[preData_idx] = param.data || function () {
            // console.debug('默认空data:' + preData_idx);
            return {};
        };
        param.methods[preCommonData_idx] = commonParam.data || function () {
            // console.debug('默认空data:' + preCommonData_idx);
            return {};
        };
        // 重写data
        param.data = function () {
            var common = typeof this[preCommonData_idx] == 'function' ? this[preCommonData_idx]() : {};
            var data = typeof this[preData_idx] == 'function' ? this[preData_idx]() : {};
            var result = jo.mergeObject(common, data);
            // console.debug('当前data的序号为=%s,有效data/待合并data/合并后data分别为:%o', idx, [data, common, result]);
            return result;
        };

        // 初始化计算变量
        if (!param.computed) {
            param.computed = {};
        }
        if (commonParam.computed) {
            for (var k in commonParam.computed) {
                // 不存在则填充计算变量
                joEl.putIfNoValue(param.computed, k, commonParam.computed[k]);
            }
        }

        // 填充方法
        if (commonParam.methods) {
            for (var k in commonParam.methods) {
                // 不存在则填充方法
                joEl.putIfNoValue(param.methods, k, commonParam.methods[k]);
            }
        }

        // mounted
        if (!param.mounted) {
            param.mounted = commonParam.mounted;
        }

        // watch
        if (commonParam.watch) {
            if (!param.watch) {
                param.watch = {};
            }
            for (var k in commonParam.watch) {
                // 不存在则填充
                joEl.putIfNoValue(param.watch, k, commonParam.watch[k]);
            }
        }

        // props
        if (commonParam.props) {
            if (!param.props) {
                param.props = {};
            }
            for (var k in commonParam.props) {
                // 不存在则填充
                joEl.putIfNoValue(param.props, k, commonParam.props[k]);
            }
        }

        return param;
    };
    // 构造公共组件参数
    joEl.buildCommonComponentParam = function (param) {
        return joEl.buildVueAppParam(param, joEl.VUE_COMPONENT_BASE_V1);
    };
})();

// joEl基础app参数
(function () {
    // 公共参数
    joEl.VUE_COMMON = {
        data() {
            return {
                // 尺寸监听开启状态
                qy_resizeListenerEnable: false,
                // 尺寸监听处理集合, key是元素选择器, value是回调函数
                qy_resizeListenerMap: {},
                // 尺寸监听对于元素的高度, 用来判断是否需要触发
                qy_resizeElementHeight: {},
            };
        },
        methods: {
            // 获取组件ref
            getRef(refName) {
                return joEl.getRef(this, refName);
            },
            // 刷新页面
            reloadPage() {
                window.location.reload();
            },
            // 元素高度变化监听
            registerElementHeightResize(selector, resizeFunc) {
                // 添加到监听集合
                this.qy_resizeListenerMap[selector] = resizeFunc;
                console.debug('注册元素高度监听:', selector, this.qy_resizeListenerMap);
                // 监听没开启,则开启监听
                if (!this.qy_resizeListenerEnable) {
                    // 更新开启状态, 避免重复开启
                    this.qy_resizeListenerEnable = true;
                    const resizeCall = (k, func) => {
                        var height = this.qy_resizeElementHeight[k];
                        var newHeight = $(k).height();
                        if (height !== newHeight) {
                            // 更新元素高度
                            this.qy_resizeElementHeight[k] = newHeight;
                            try {
                                func();
                            } catch (e) {
                                console.error('元素高度监听回调函数执行异常: ', k, e);
                            }
                            console.info('resize: ', k);
                        }
                    }
                    // 窗口大小监听
                    $(window).resize(() => {
                        for (var k in this.qy_resizeListenerMap) {
                            var func = this.qy_resizeListenerMap[k];
                            if (typeof func == 'function') {
                                resizeCall(k, func);
                            }
                        }
                    });

                    // 定时轮训, 当窗口大小没变, 但是元素内容改变时, 不会触发window.resize, 通过轮训兜底
                    window.setInterval(() => {
                        for (var k in this.qy_resizeListenerMap) {
                            var func = this.qy_resizeListenerMap[k];
                            if (typeof func == 'function') {
                                resizeCall(k, func);
                            }
                        }
                    }, 500);
                }
            },
            // 数据区高度适应, 入参dataBarEl: 数据区元素, 字符串表示数据区选择器, 数字表示第几个数据区(.qy-view-data-bar)
            calcDataBarHeight(dataBarEl) {
                if (!dataBarEl || dataBarEl === 0) {
                    dataBarEl = document.getElementsByClassName('qy-view-data-bar')[0];
                } else if (typeof dataBarEl == 'string') {
                    dataBarEl = $(dataBarEl)[0];
                } else if (typeof dataBarEl == 'number') {
                    dataBarEl = document.getElementsByClassName('qy-view-data-bar')[dataBarEl];
                }
                var dataBarJq = $(dataBarEl);
                var appJq = $('.qy-view-page-app');
                var pageContentJq = dataBarJq.parent('.qy-view-page-content');
                var pageBarJq = dataBarJq.find('.jo-el-table-page-bar');

                var total = 0;
                jo.forEach(dataBarJq.siblings(), (item) => {
                    total += $(item).outerHeight(true) || 0;
                });
                // 内容容器非height总高度. 下面为啥不直接用容器的高度减去其他区域的高度进行计算?因为容器的高度是会随着内容变化的,不是一个定值
                var boxPadding = pageContentJq.outerHeight(true) - pageContentJq.height();
                // 分页条高度
                var pageBarHeight = jo.getDefVal(pageBarJq.outerHeight(true), 0);
                // 页面总高度(.qy-view-page-app需显式指定高度,不能是auto)
                var appHeight = appJq.height();
                // 页面应用容器(.qy-view-page-app)到内容容器(.qy-view-page-content)中间夹层的高度
                var middleHeight = 0;
                var p = pageContentJq.parent();
                while (p && !p.hasClass('qy-view-page-app')) {
                    middleHeight += p.outerHeight(true) - p.height();
                    p = p.parent();
                }
                var h = appHeight - middleHeight - boxPadding - total - pageBarHeight;
                console.debug('[数据区高度适应] 应用高度、夹层高度、内容区边距高度、兄弟总高度、分页条高度、计算结果:', [appHeight, middleHeight, boxPadding, total, pageBarHeight, h]);
                return h;
            },
            // 格式化日期
            formatDate(t) {
                if (t) {
                    try {
                        return jo.formatDate(t);
                    } catch (e) {
                        console.error('格式化日期异常:', t, e);
                    }
                }
                return t;
            },
            // 格式化时间
            formatTime(t) {
                if (t) {
                    try {
                        return jo.formatTime(t);
                    } catch (e) {
                        console.error('格式化时间异常:', t, e);
                    }
                }
                return t;
            }
        }
    };
    // 视图页参数
    joEl.VUE_COMMON_VIEW = {
        data() {
            return {
                // 查询参数
                searchCondition: {},
                // 更多条件
                moreConditionFlag: false,
                // 表单校验规则
                checkRules: {},
                // 表单详情查询url
                formQueryUrl: '',
                // 新增url
                formInsertUrl: '',
                // 修改url
                formUpdateUrl: '',
                // 删除url
                deleteUrl: '',
                // 模板下载url
                templateUrl: '',
                // 导入url
                importUrl: '',
                // 导出url
                exportUrl: '',
                // 新增表单ref
                addFormRef: 'formRef',
                // 编辑表单ref
                editFormRef: 'formRef',

                // 新增表单标题
                addFormTitle: '新增',
                // 新增表单宽度
                addFormWidth: '75%',
                // 编辑表单标题
                editFormTitle: '编辑',
                // 编辑表单宽度
                editFormWidth: '75%',

                // 主键属性名
                primaryKey: 'id',
                // jo表格ref, 无特殊情况不需要改动
                joTableRef: 'joTableRef',
                // el表格ref, 无特殊情况不需要改动
                tableRef: 'tableRef',
                // 表格单选框值
                defaultTableRadio: '',
                // 表单数据
                formData: {},
                // 表单显示开关
                formShow: false,
                // 新增表单标识, 用来区分表单弹层是新增表单(1)还是编辑表单(2)
                formType: 1,
                // 打开弹层前先清空数据
                cleanDataBeforeOpen: true
            };
        },
        computed: {
            // 新增表单标识
            addFlag() {
                return this.formType == 1;
            },
            // 编辑表单标识
            editFlag() {
                return this.formType == 2;
            },
            // 表单ref
            formRef() {
                if (this.formType === 1) {
                    return this.addFormRef;
                } else if (this.formType === 2) {
                    return this.editFormRef;
                } else {
                    return 'formRef';
                }
            },
            // 表单标题
            formTitle() {
                if (this.formType === 1) {
                    return this.addFormTitle;
                } else if (this.formType === 2) {
                    return this.editFormTitle;
                } else {
                    return '';
                }
            },
            // 表单宽度
            formWidth() {
                if (this.formType === 1) {
                    return this.addFormWidth;
                } else if (this.formType === 2) {
                    return this.editFormWidth;
                } else {
                    return '75%';
                }
            },
        },
        methods: {
            // ************ 回调函数 start ************
            // ************ 回调函数 start ************
            // ************ 回调函数 start ************
            // 新增表单数据初始化前的处理
            handle_form_init_data_add(data) {

            },
            // 编辑表单数据初始化前的处理, ajax取到数据后
            handle_form_init_data_edit(data) {

            },
            // 新增表单提交前数据处理, 表单校验通过后,ajax请求前
            handle_form_data_before_submit_add(data) {

            },
            // 编辑表单提交前数据处理, 表单校验通过后,ajax请求前
            handle_form_data_before_submit_edit(data) {

            },
            // 关闭新增表单后的回调
            close_add_form_after() {

            },
            // 关闭编辑表单后的回调
            close_edit_form_after() {

            },
            // 新增成功后处理, 参数为后端返回结果
            insert_success_after(json) {
                jo.showSuccessMsg('保存成功~');
                // 刷新列表页
                this.list_refresh();
                // 关闭弹层
                this.close_add_form();
            },
            // 新增成功后处理, 参数为后端返回结果
            update_success_after(json) {
                jo.showSuccessMsg('更新成功~');
                // 刷新列表页
                this.list_refresh();
                // 关闭弹层
                this.close_add_form();
            },
            // 删除成功后处理
            delete_success_after(json) {
                jo.showSuccessMsg('删除成功~');
                // 刷新列表页
                this.list_refresh();
            },
            // 导入成功后处理
            import_success_after(json, file, data) {
                jo.showSuccessMsg('导入成功~');
                // 刷新列表页
                this.list_refresh();
            },
            // 编辑表单数据查询url
            get_edit_form_data_url() {
                return this.formQueryUrl;
            },
            // 导出excel的url构造
            get_list_export_excel_url() {
                return this.exportUrl;
            },
            // ************ 回调函数 end ************
            // ************ 回调函数 end ************
            // ************ 回调函数 end ************
            // 事件发布
            emit_command(command, data) {
                console.info(arguments)
                joEl.bus.emit(command, data);
            },
            // jo表格ref
            list_jo_table_ref(joTableRef) {
                return this.getRef(joTableRef || this.joTableRef);
            },
            // 表格ref
            // list_table_ref() {
            //     return this.getRef(this.tableRef);
            // },
            // 新增表单ref
            add_form_ref() {
                return this.getRef(this.addFormRef);
            },
            // 编辑表单ref
            edit_form_ref() {
                return this.getRef(this.editFormRef);
            },
            // 获取选中项
            getCheckedItemArr(joTableRef) {
                // 拿到jo表格实例
                var joTable = joTableRef ? this.getRef(joTableRef) : this.list_jo_table_ref();
                if (!joTable) {
                    console.error('[列表页组件] 获取jo表格ref失败.', joTableRef);
                    return [];
                }
                return joTable.getCheckedItemArr();
            },
            // 获取选中项id集合
            getCheckedItemIdArr(joTableRef, idFieldName) {
                var arr = [];
                var _this = this;
                jo.forEach(this.getCheckedItemArr(joTableRef), function (item) {
                    arr.push(item[idFieldName || _this.primaryKey]);
                });
                return arr;
            },
            // [过时] 排序{ column, prop, order }, 使用jo-el-table-v2可以忽略该方法
            list_table_sort(sort) {
                this.list_jo_table_ref().tableSortChange(sort);
            },
            // 列表查询
            list_search() {
                this.list_jo_table_ref().goPage();
            },
            // 列表刷新
            list_refresh(joTableRef) {
                this.list_jo_table_ref(joTableRef).refresh();
            },
            // 打开新增弹层
            open_add_form() {
                this.formShow = true;
                // 重置该表单项，将其值重置为初始值，并移除校验结果
                var ref = this.add_form_ref();
                if (ref) {
                    ref.resetFields();
                }
            },
            // 打开编辑弹层
            open_edit_form() {
                this.formShow = true;
                // 重置该表单项，将其值重置为初始值，并移除校验结果
                var ref = this.edit_form_ref();
                if (ref) {
                    ref.resetFields();
                }
            },
            // 关闭新增弹层
            close_add_form() {
                this.formShow = false;
                // 关闭弹层后的处理
                if (typeof this.close_add_form_after == 'function') {
                    this.close_add_form_after();
                }
            },
            // 关闭编辑弹层
            close_edit_form() {
                this.formShow = false;
                // 关闭弹层后的处理
                if (typeof this.close_edit_form_after == 'function') {
                    this.close_edit_form_after();
                }
            },
            // 关闭表单弹层
            close_form() {
                if (this.formType === 1) {
                    this.close_add_form();
                } else if (this.formType === 2) {
                    this.close_edit_form();
                } else {
                    console.error('未知的表单类型(formType):' + this.formType);
                }
            },
            // 初始化新增表单数据
            init_form_data_add() {
                var data = {};
                if (typeof this.handle_form_init_data_add == 'function') {
                    this.handle_form_init_data_add(data);
                }
                this.formData = data;
            },
            // 初始化编辑表单数据
            init_form_data_edit(item) {
                var url = this.get_edit_form_data_url();
                if (!url) {
                    console.error('表单详情查询URL无效,请检查配置项[formQueryUrl or get_edit_form_data_url()]');
                    return;
                }
                var _this = this;
                // 先清空, 在查询, 避免错乱
                this.formData = {};
                this.$nextTick(() => {
                    jo.showLoading();
                    jo.postJson(url, {body: item[this.primaryKey]}).success(function (json) {
                        jo.showLoading(false);
                        if (typeof _this.handle_form_init_data_edit == 'function') {
                            _this.handle_form_init_data_edit(json.data);
                        }
                        _this.formData = json.data || {};
                    }).error(function (json) {
                        jo.showLoading(false);
                        jo.showErrorMsg(json.info || '详情查询失败');
                    });
                })
            },

            // 清空表单数据
            clean_form_data() {
                this.formData = {};
            },
            // 新增
            list_add() {
                this.formType = 1;
                if (this.cleanDataBeforeOpen) {
                    this.clean_form_data();
                }
                // 打开弹层
                this.open_add_form();
                // 初始化表单数据
                this.init_form_data_add();
            },
            // 编辑
            list_edit(item) {
                this.formType = 2;
                if (this.cleanDataBeforeOpen) {
                    this.clean_form_data();
                }
                // 打开弹层
                this.open_edit_form();
                // 初始化表单数据
                this.init_form_data_edit(item);
            },
            // 删除
            list_delete(item) {
                var id = typeof item == 'object' ? item[this.primaryKey] : item;
                if (!id) {
                    console.error('[list_delete] 入参待删除项无效');
                    return;
                }
                this.delete_ids(id);
            },
            delete_ids(ids) {
                if (!this.deleteUrl) {
                    console.error('[delete_ids] 删除URL无效,请检查配置项[deleteUrl]');
                    return;
                }
                if (!ids) {
                    console.error('[delete_ids] 入参待删除项无效');
                    return;
                }
                var _this = this;
                jo.confirm('您确定要删除选中项嘛?', function (json) {
                    jo.postJson(_this.deleteUrl, {body: ids}).success(function (json) {
                        _this.delete_success_after(json);
                    }).error(function (json) {
                        jo.showErrorMsg(json.info || '删除失败');
                    });
                });
            },
            // 批量删除
            list_delete_batch() {
                var ids = this.getCheckedItemIdArr().join(',');
                if (!ids) {
                    jo.showTipsMsg('请选择待删除项~');
                    return;
                }
                this.delete_ids(ids);
            },
            // 批量删除模板方法,
            // table:(必填)表格ref属性, deleteUrl:(必填)删除url, idField:id字段名
            // noCheckedMsg:没有选中项时提示文案, confirmMsg:删除前确认文案
            // successCall:删除成功回调, errorCall:删除失败回调
            list_delete_template(param) {
                param.noCheckedMsg = param.noCheckedMsg || '请选择待删除项~';
                param.confirmMsg = param.confirmMsg || '您确定要删除选中项嘛?';
                param.successMsg = param.successMsg || '删除成功~';
                param.errorMsg = param.errorMsg || '删除失败';
                var ids = param.ids;
                if (!ids) {
                    if (!param.table) {
                        console.error('待删除ids或表格ref无效,请检查配置项[table]或[ids]');
                        return;
                    }
                    // 获取待删除id集合
                    ids = this.getCheckedItemIdArr(param.table, param.idField).join(',');
                    if (!ids) {
                        jo.showTipsMsg(param.noCheckedMsg);
                        return;
                    }
                }
                if (!param.deleteUrl) {
                    console.error('删除URL无效,请检查配置项[deleteUrl]');
                    return;
                }
                jo.confirm(param.confirmMsg, () => {
                    jo.postJson(param.deleteUrl, {body: ids}).success((json) => {
                        if (typeof param.successCall == 'function') {
                            param.successCall(json);
                        } else {
                            jo.showSuccessMsg(param.successMsg);
                            // 刷新表格
                            this.list_refresh(param.table);
                        }
                    }).error((json) => {
                        if (typeof param.errorCall == 'function') {
                            param.errorCall(json);
                        } else {
                            jo.showErrorMsg(json.info || param.errorMsg);
                        }
                    });
                });
            },
            // 表单保存
            form_save() {
                if (this.formType === 1) {
                    this.form_insert();
                } else if (this.formType === 2) {
                    this.form_update();
                } else {
                    console.error('未知的表单类型(formType):' + this.formType);
                }
            },
            // 表单保存模板方法
            // data:(必填)表单数据, url:(必填)保存url, formRef:表单ref属性
            // successMsg:成功提示文案, errorMsg:失败提示文案
            // successCall:删除成功回调, errorCall:删除失败回调
            form_save_template(param) {
                if (!jo.assert.object(param.data, '待保存数据为空')
                    || !jo.assert.valid(param.url, '表单保存URL为空')) {
                    return;
                }
                param.successMsg = param.successMsg || '保存成功~';
                param.errorMsg = param.errorMsg || '保存失败';
                var run = function () {
                    jo.postJson(param.url, {body: param.data}).success((json) => {
                        if (typeof param.successCall == 'function') {
                            param.successCall(json);
                        } else {
                            jo.showSuccessMsg(param.successMsg);
                        }
                    }).error((json) => {
                        if (typeof param.errorCall == 'function') {
                            param.errorCall(json);
                        } else {
                            jo.showErrorMsg(json.info || param.errorMsg);
                        }

                    });
                };
                if (param.formRef) {// ref有效则表单校验
                    var ref = typeof param.formRef == 'object' ? param.formRef : this.getRef(param.formRef);
                    if (!ref) {
                        console.error('[form_save_template] 获取表单失败:', param.formRef);
                        jo.showErrorMsg('获取表单失败');
                        return;
                    }
                    ref.validate((success, invalidFields) => {
                        if (success) {
                            run();
                        } else {
                            console.info('[form_save_template] 表单[%s]校验失败,非法字段:', param.formRef, invalidFields);
                        }
                    });
                } else {// 直接保存
                    run();
                }
            },
            // 表单新增
            form_insert() {
                var _this = this;
                var param = JSON.parse(JSON.stringify(this.formData));
                this.add_form_ref().validate(function (success, invalidFields) {
                    // invalidFields参数示例: [{field: "code",fieldValue: "",message: "职位编号必填"}]
                    if (success) {
                        if (typeof _this.handle_form_data_before_submit_add == 'function') {
                            _this.handle_form_data_before_submit_add(param);
                        }
                        jo.postJson(_this.formInsertUrl, {body: param}).success(function (json) {
                            _this.insert_success_after(json);
                        }).error(function (json) {
                            jo.showErrorMsg(json.info || '保存失败');
                        });
                    } else {
                        console.info('[form_insert] 表单校验失败,非法字段:', invalidFields);
                    }
                });
            },
            // 表单修改
            form_update() {
                var _this = this;
                var param = JSON.parse(JSON.stringify(this.formData));
                this.edit_form_ref().validate(function (success, invalidFields) {
                    // invalidFields参数示例: [{field: "code",fieldValue: "",message: "职位编号必填"}]
                    if (success) {
                        if (typeof _this.handle_form_data_before_submit_edit == 'function') {
                            _this.handle_form_data_before_submit_edit(param);
                        }
                        jo.postJson(_this.formUpdateUrl, {body: param}).success(function (json) {
                            _this.update_success_after(json);
                        }).error(function (json) {
                            jo.showErrorMsg(json.info || '更新失败');
                        });
                    } else {
                        console.info('[form_update] 表单校验失败,非法字段:', invalidFields);
                    }
                });
            },
            // 导入excel
            list_import_excel(files, closeFunc) {
                if (this.importUrl) {
                    jo.postFile(this.importUrl, files[0], {}, (json, file, data) => {
                        this.import_success_after(json, file, data);
                        closeFunc();
                    }, (json, file, data) => {
                        jo.showErrorMsg(json.info || '导入失败!');
                        console.warn('[list_import_excel] 导入失败', json, file, data);
                    });
                } else {
                    jo.showErrorMsg('导入URL配置[importUrl]无效!');
                }
            },
            // 导出excel
            list_export_excel() {
                var url = this.get_list_export_excel_url();
                if (url) {
                    jo.newWindow(url);
                } else {
                    jo.showErrorMsg('导出URL配置[exportUrl/get_list_export_excel_url()]无效!');
                }
            },

        }
    };
    // 组件公共属性v1, 自定义组件可以复用, 提供基础的组件能力
    joEl.VUE_COMPONENT_BASE_V1 = {
        data: function () {
            return {
                vue_component_base_version: 1
            };
        },
        props: {
            instance: {
                type: Object,
                default: function () {
                    return {
                        command: {},
                        call: {},
                    };
                }
            },
        },
        computed: {
            // 组件实例操作指令对象, 方便调用组件实例提供的方法
            command() {
                return this.instance.command;
            },
            // 钩子函数
            call() {
                return this.instance.call;
            },
        },
        methods: {
            // 初始化instance属性
            initInstance() {
                if (!this.instance.command) {
                    this.instance.command = {};
                }
                if (!this.instance.call) {
                    this.instance.call = {};
                }

                // 绑定命令对象
                if (this.$options && this.$options.methods) {
                    for (let k in this.$options.methods) {
                        if (typeof this[k] == "function") {
                            if (k === 'initInstance') {
                                continue;
                            }
                            if (k && k.indexOf('common__data__backup') > -1) {
                                continue;
                            }
                            this.instance.command[k] = this[k];
                        }
                    }
                }
                // console.debug('[initInstance] 命令对象:', this.instance);
            },
        },
    };
})();

// joEl组件
(function () {
    joEl.COMPONENT_MAP = {
        'jo-el-tag': {
            props: {
                // tag值
                value: {
                    type: [String, Number],
                    default: ''
                },
                // 枚举选项
                options: {
                    type: [Array, Object],
                    default: function () {
                        return {};
                    }
                },
                // 枚举选项中的code字段名
                codeField: {
                    type: String,
                    default: 'code'
                },
                // 枚举选项中的展示字段名
                textField: {
                    type: String,
                    default: 'text'
                },
                // 枚举选项中的样式字段
                typeField: {
                    type: String,
                    default: 'styleType'
                },
                // el-tag样式effect
                effect: {
                    type: String,
                    default: 'plain'
                },
                // el-tag皮肤样式type, '',success,info,warning,danger
                type: {
                    type: String,
                    default: 'primary'
                },
            },
            data: function () {
                return {};
            },
            computed: {
                // 是否显示
                showFlag() {
                    return jo.isValid(this.contentValue);
                },
                // tag展示内容
                contentValue() {
                    if (this.options) {
                        if (Array.isArray(this.options) && this.options.length > 0) {
                            for (let i = 0; i < this.options.length; i++) {
                                var item = this.options[i];
                                if (item[this.codeField] == this.value) {
                                    return item[this.textField];
                                }
                            }
                        } else if (typeof this.options == 'object') {
                            if (this.value in this.options) {
                                return this.options[this.value];
                            }
                        }
                    }
                    return this.value;
                },
                // tag的type值, 从枚举选项中拿
                typeValue() {
                    var type = this.type;
                    if (!this.type) {
                        // 没有指定样式, 则到枚举里面找
                        if (Array.isArray(this.options) && this.options.length > 0) {
                            for (let i = 0; i < this.options.length; i++) {
                                var item = this.options[i];
                                if (item[this.codeField] == this.value) {
                                    type = jo.getDefVal(item[this.typeField], this.type);
                                    break;
                                }
                            }
                        }
                    }
                    // 在tag组件里面, primary要设置空, el升级到2.8之后就是正常的primary了
                    // return type === 'primary' ? '' : type;
                    return type;
                }
            },
            template: `
                <el-tag v-if="showFlag" :type="typeValue" :effect="effect">{{ contentValue }}</el-tag>
            `,
            methods: {},
            mounted() {
                console.info('[jo-el-tag] mounted');
            }
        },
        'jo-el-table-v2': {
            inheritAttrs: false,
            // 可以接受的属性
            props: {
                tableRef: {
                    type: String,
                    default: 'tableRef'
                },
                // 数据url
                url: {
                    type: String
                },
                // 是否开启分页
                pageEnable: {
                    type: [String, Number, Boolean],
                    default: true
                },
                // 查询参数对象
                searchParam: {
                    type: Object,
                    default: function () {
                        return {};
                    }
                },
                // 渲染表格数据前通过该方法处理数据项
                handleItem: {
                    type: Function,
                },
                // 查询参数处理, 查询前对参数的校准
                handleParam: {
                    type: Function,
                },
                // 查询参数处理, 查询前对参数的校准
                buildSearchUrl: {
                    type: Function,
                },
                pageSize: {
                    type: Number,
                    default: 10
                },
                pageSizes: {
                    type: Array,
                    default: [10, 20, 50, 100]
                },
                // 分页布局
                pageLayout: {
                    type: String,
                    default: 'total, sizes, prev, pager, next'
                },
                // 首列类型, radio,checkbox,index,none
                firstColumn: {
                    type: String,
                    default: 'none'
                },
                // 是否开启加载提示
                enableLoading: {
                    type: Boolean,
                    default: false
                },
                // radio首列初始化值
                radioInitValue: {
                    type: [String, Number],
                    default: null
                },
                // 数据加载后回调
                dataLoadAfter: {
                    type: Function,
                    default: null
                },
                // 表格刷新指令监听
                commandRefresh: {
                    type: [String, Array],
                    default: null
                },
                // 表格查询指令监听
                commandSearch: {
                    type: [String, Array],
                    default: null
                },
                // 加载后是否默认初始化数据
                defaultInit: {
                    type: [Boolean, String, Number],
                    default: true
                }
            },
            data: function () {
                return {
                    tableData: [],
                    page: {
                        pageNumber: 1,
                        pageSize: this.pageSize,
                        pageSizes: this.pageSizes,
                        total: 0,
                        layout: this.pageLayout,   //, jumper
                        hideOnSinglePage: false
                    },
                    // 排序字段
                    sortBy: '',
                    // 排序类型, asc,desc
                    sortType: '',

                    // radio单选框值对象引用
                    radioValue: {
                        value: this.radioInitValue
                    },

                    // 数据加载中状态
                    dataLoading: false,
                };
            },
            computed: {
                // 首列是单选框
                isRadio() {
                    return this.firstColumn === 'radio';
                },
                // 是否分页展示
                showPageBar() {
                    if (this.pageEnable === false
                        || this.pageEnable === 'false'
                        || this.pageEnable === 0) {
                        return false;
                    }
                    return true;
                },
                // loading状态
                realLoadingStatus() {
                    // 开启loading后生效
                    return this.enableLoading && this.dataLoading;
                },
                // 参数绑定
                otherBindAttr() {
                    // return this,
                }
            },
            template: `
            <div class="jo-el-table-bar" v-loading="realLoadingStatus">
                <div class="jo-el-table-data-bar">
                    <slot name="table">
                        <el-table :ref="tableRef" :data="tableData" 
                            header-cell-class-name="jo-el-table-header" 
                            @sort-change="tableSortChange" 
                            v-bind="$attrs">
                            <slot></slot>
                        </el-table>
                    </slot>
                </div>
                <div class="jo-el-table-page-bar" v-if="showPageBar">
                    <slot name="page" :page="page">
                          <el-pagination
                            :currentPage="page.pageNumber"
                            :page-sizes="page.pageSizes"
                            :page-size="page.pageSize"
                            :total="page.total"
                            :layout="page.layout"
                            :background="true"
                            :hide-on-single-page="page.hideOnSinglePage"
                            @size-change="handleSizeChange"
                            @current-change="goPage"
                            class=""
                            small
                            >
                          </el-pagination>
                    </slot>
                </div>
            </div>
            `,
            methods: {
                // 排序{ column, prop, order }
                tableSortChange(sort) {
                    if (sort) {
                        this.sortBy = jo.getDefVal(sort.prop, '');
                        if (sort.order === 'ascending') {
                            // 升序
                            this.sortType = 'asc';
                        } else if (sort.order === 'descending') {
                            // 降序
                            this.sortType = 'desc';
                        } else {
                            this.sortType = '';
                        }
                        // 刷新表格数据
                        this.refresh();
                    }
                },
                // 获取表格ref
                getTableRef() {
                    return joEl.getRef(this, this.tableRef);
                },
                // 获取radio选中项
                getCheckedRadioItem() {
                    if (jo.isValid(this.radioValue.value)) {
                        return this.tableData[this.radioValue.value];
                    }
                    return null;
                },
                // 获取选中项
                getCheckedItemArr() {
                    if (this.isRadio) {
                        var item = this.getCheckedRadioItem();
                        return item ? [item] : [];
                    } else {
                        var table = this.getTableRef();
                        if (!table) {
                            console.error('[jo-el-table-v2] 获取el表格ref为空,tableRef参数=' + this.tableRef);
                            return [];
                        }
                        var arr = table.getSelectionRows();
                        return arr ? arr : [];
                    }
                },
                // 获取选中项某字段集合
                getCheckedItemFieldArr(field) {
                    var arr = [];
                    field = jo.getDefVal(field, 'id');
                    jo.forEach(this.getCheckedItemArr(), function (item) {
                        arr.push(item[field]);
                    });
                    return arr;
                },
                // 页大小变更
                handleSizeChange(pageSize) {
                    this.searchInvoke({pageNumber: 1, pageSize: pageSize});
                },
                // 去第n页
                goPage(pageNumber) {
                    if (!pageNumber) {
                        pageNumber = 1;
                    }
                    this.searchInvoke({pageNumber: pageNumber});
                },
                // 刷新数据, 也就是重新加载当前页
                refresh() {
                    this.goPage(this.page.pageNumber);
                },
                // 查询url
                buildSearchUrlInvoke() {
                    if (typeof this.buildSearchUrl == 'function') {
                        return this.buildSearchUrl();
                    }
                    return this.url;
                },
                // 数据查询执行核心方法
                searchInvoke(extParam) {
                    var _this = this;
                    // 设置加载中状态
                    this.dataLoading = true;
                    try {
                        // 构造查询参数, 包括表单参数和分页参数等
                        var param = _this.buildSearchParam(extParam);
                        var url = _this.buildSearchUrlInvoke();// + jo.getLinkSign(_this.url) + 'pageNumber=' + jo.getDefVal(param.pageNumber, '') + '&pageSize=' + jo.getDefVal(param.pageSize, '')
                        if (!url) {
                            console.error('[jo-el-table-v2] 数据查询url为空,请检查配置项[url or buildSearchUrl()]');
                            return;
                        }
                        jo.postJsonAjax(url, param).success(function (json) {
                            var list = json.data;
                            jo.forEach(list, function (item, i) {
                                if (typeof _this.handleItem == 'function') {
                                    _this.handleItem(item, i);
                                }
                            });
                            _this.tableData = json.data || [];
                            _this.page.pageNumber = json.pageNumber;
                            _this.page.total = json.total;
                            // 清空loading状态
                            _this.dataLoading = false;
                            // 数据加载后回调
                            if (typeof _this.dataLoadAfter == 'function') {
                                _this.dataLoadAfter(json, _this);
                            }
                        }).error(function (json) {
                            // 清空loading状态
                            _this.dataLoading = false;
                            _this.tableData = [];
                            if (json.info) {
                                jo.showErrorMsg(json.info);
                            }
                            console.warn('[jo-el-table-v2] query error', json);
                        });
                    } catch (e) {
                        // 清空loading状态
                        _this.dataLoading = false;
                        console.warn('[jo-el-table-v2] 数据查询searchInvoke执行异常:', e);
                    }
                },
                // 构造查询参数
                buildSearchParam(extParam) {
                    // 输入的查询条件
                    var param = {
                        body: this.searchParam ? JSON.parse(JSON.stringify(this.searchParam)) : {}
                    };
                    // 分页参数
                    param.pageNumber = this.page.pageNumber;
                    param.pageSize = this.page.pageSize;
                    // 排序参数
                    if (this.sortBy) {
                        param.body.dbSortBy = this.sortBy;
                    }
                    if (this.sortType) {
                        param.body.dbSortType = this.sortType;
                    }
                    // 指定的扩展参数, 和上面参数重复的, 以扩展参数为准, 可以用来参数校准
                    if (extParam) {
                        param = jo.mergeObject(param, extParam);
                    }
                    // 参数处理钩子
                    if (typeof this.handleParam == 'function') {
                        this.handleParam(param);
                    }
                    return param;
                },
                initCommandHandler() {
                    this.bindCommandHandler(this.commandSearch, (data) => {
                        this.goPage();
                    });
                    this.bindCommandHandler(this.commandRefresh, (data) => {
                        this.refresh();
                    });
                },
                bindCommandHandler(command, handler, consumer) {
                    if (command && typeof handler == 'function') {
                        if (typeof command == 'string') {
                            joEl.bus.on(command, consumer || 'GLOBAL', handler);
                        } else if (Array.isArray(command)) {
                            command.forEach(cmd => {
                                joEl.bus.on(cmd, consumer || 'GLOBAL', handler);
                            });
                        }
                    }
                }
            },
            mounted() {
                if (this.defaultInit) {
                    this.goPage(1);
                }
                this.initCommandHandler();
                console.info('[jo-el-table-v2] mounted.');
            }
        },
        'jo-el-table': {
            // 可以接受的属性
            props: {
                tableRef: {
                    type: String,
                    default: 'tableRef'
                },
                // 数据url
                url: {
                    type: String
                },
                // 列配置
                cols: {
                    type: Array
                },
                // 是否开启分页
                pageEnable: {
                    type: [String, Number, Boolean],
                    default: true
                },
                // 边框
                border: {
                    type: Boolean,
                    default: false
                },
                // 查询参数对象
                searchParam: {
                    type: Object,
                    default: function () {
                        return {};
                    }
                },
                // 渲染表格数据前通过该方法处理数据项
                handleItem: {
                    type: Function,
                },
                // 查询参数处理, 查询前对参数的校准
                handleParam: {
                    type: Function,
                },
                // 查询参数处理, 查询前对参数的校准
                buildSearchUrl: {
                    type: Function,
                },
                pageSize: {
                    type: Number,
                    default: 10
                },
                pageSizes: {
                    type: Array,
                    default: [10, 20, 50, 100]
                },
                // 分页布局
                pageLayout: {
                    type: String,
                    default: 'total, sizes, prev, pager, next'
                },
                // 首列类型, radio,checkbox,index,none
                firstColumn: {
                    type: String,
                    default: 'none'
                },
                // 是否开启加载提示
                enableLoading: {
                    type: Boolean,
                    default: false
                },
                // radio首列初始化值
                radioInitValue: {
                    type: [String, Number],
                    default: null
                },
                // 数据加载后回调
                dataLoadAfter: {
                    type: Function,
                    default: null
                },
            },
            data: function () {
                return {
                    tableData: [],
                    page: {
                        pageNumber: 1,
                        pageSize: this.pageSize,
                        pageSizes: this.pageSizes,
                        total: 0,
                        layout: this.pageLayout,   //, jumper
                        hideOnSinglePage: false
                    },
                    // 排序字段
                    sortBy: '',
                    // 排序类型, asc,desc
                    sortType: '',

                    // radio单选框值对象引用
                    radioValue: {
                        value: this.radioInitValue
                    },

                    // 数据加载中状态
                    dataLoading: false,
                };
            },
            computed: {
                // 首列是单选框
                isRadio() {
                    return this.firstColumn === 'radio';
                },
                // 是否分页展示
                showPageBar() {
                    if (this.pageEnable === false
                        || this.pageEnable === 'false'
                        || this.pageEnable === 0) {
                        return false;
                    }
                    return true;
                },
                // loading状态
                realLoadingStatus() {
                    // 开启loading后生效
                    return this.enableLoading && this.dataLoading;
                }
            },
            template: `
            <div class="jo-el-table-bar" v-loading="realLoadingStatus">
                <div class="jo-el-table-data-bar">
                    <slot :cols="cols" :data="tableData" :radio="radioValue" :ref="tableRef">
                        <el-table :ref="tableRef" :data="tableData" :border="border" header-cell-class-name="jo-el-table-header" 
                            @sort-change="tableSortChange">
                            <slot name="columns" :cols="cols">
                                   <el-table-column v-for="col in cols" :type="col.type" :prop="col.field" :label="col.label"
                                    :width="col.width" :header-align="col.headerAlign" :align="col.align" :formatter="col.formatter">
                                    </el-table-column>
                                    <slot name="oper"></slot>
                            </slot>
                        </el-table>
                    </slot>
                </div>
                <div class="jo-el-table-page-bar" v-if="showPageBar">
                    <slot name="page" :page="page">
                          <el-pagination
                            :currentPage="page.pageNumber"
                            :page-sizes="page.pageSizes"
                            :page-size="page.pageSize"
                            :total="page.total"
                            :layout="page.layout"
                            :background="true"
                            :hide-on-single-page="page.hideOnSinglePage"
                            @size-change="handleSizeChange"
                            @current-change="goPage"
                            class=""
                            small
                            >
                          </el-pagination>
                    </slot>
                </div>
            </div>
            `,
            methods: {
                // 排序{ column, prop, order }
                tableSortChange(sort) {
                    if (sort) {
                        this.sortBy = jo.getDefVal(sort.prop, '');
                        if (sort.order === 'ascending') {
                            // 升序
                            this.sortType = 'asc';
                        } else if (sort.order === 'descending') {
                            // 降序
                            this.sortType = 'desc';
                        } else {
                            this.sortType = '';
                        }
                        // 刷新表格数据
                        this.refresh();
                    }
                },
                // 获取表格ref
                getTableRef() {
                    return joEl.getRef(this, this.tableRef);
                },
                // 获取radio选中项
                getCheckedRadioItem() {
                    if (jo.isValid(this.radioValue.value)) {
                        return this.tableData[this.radioValue.value];
                    }
                    return null;
                },
                // 获取选中项
                getCheckedItemArr() {
                    if (this.isRadio) {
                        var item = this.getCheckedRadioItem();
                        return item ? [item] : [];
                    } else {
                        var table = this.getTableRef();
                        if (!table) {
                            console.error('[jo-el-table] 获取el表格ref为空,tableRef参数=' + this.tableRef);
                            return [];
                        }
                        var arr = table.getSelectionRows();
                        return arr ? arr : [];
                    }
                },
                // 获取选中项某字段集合
                getCheckedItemFieldArr(field) {
                    var arr = [];
                    field = jo.getDefVal(field, 'id');
                    jo.forEach(this.getCheckedItemArr(), function (item) {
                        arr.push(item[field]);
                    });
                    return arr;
                },
                // 页大小变更
                handleSizeChange(pageSize) {
                    this.searchInvoke({pageNumber: 1, pageSize: pageSize});
                },
                // 去第n页
                goPage(pageNumber) {
                    if (!pageNumber) {
                        pageNumber = 1;
                    }
                    this.searchInvoke({pageNumber: pageNumber});
                },
                // 刷新数据, 也就是重新加载当前页
                refresh() {
                    this.goPage(this.page.pageNumber);
                },
                // 查询url
                buildSearchUrlInvoke() {
                    if (typeof this.buildSearchUrl == 'function') {
                        return this.buildSearchUrl();
                    }
                    return this.url;
                },
                // 数据查询执行核心方法
                searchInvoke(extParam) {
                    var _this = this;
                    // 设置加载中状态
                    this.dataLoading = true;
                    try {
                        // 构造查询参数, 包括表单参数和分页参数等
                        var param = _this.buildSearchParam(extParam);
                        var url = _this.buildSearchUrlInvoke();// + jo.getLinkSign(_this.url) + 'pageNumber=' + jo.getDefVal(param.pageNumber, '') + '&pageSize=' + jo.getDefVal(param.pageSize, '')
                        if (!url) {
                            console.error('[jo-el-table] 数据查询url为空,请检查配置项[url or buildSearchUrl()]');
                            return;
                        }
                        jo.postJsonAjax(url, param).success(function (json) {
                            var list = json.data;
                            jo.forEach(list, function (item, i) {
                                if (typeof _this.handleItem == 'function') {
                                    _this.handleItem(item, i);
                                }
                            });
                            _this.tableData = json.data || [];
                            _this.page.pageNumber = json.pageNumber;
                            _this.page.total = json.total;
                            // 清空loading状态
                            _this.dataLoading = false;
                            // 数据加载后回调
                            if (typeof _this.dataLoadAfter == 'function') {
                                _this.dataLoadAfter(json, _this);
                            }
                        }).error(function (json) {
                            // 清空loading状态
                            _this.dataLoading = false;
                            _this.tableData = [];
                            if (json.info) {
                                jo.showErrorMsg(json.info);
                            }
                            console.warn('[jo-el-table] query error', json);
                        });
                    } catch (e) {
                        // 清空loading状态
                        _this.dataLoading = false;
                        console.warn('[jo-el-table] 数据查询searchInvoke执行异常:', e);
                    }
                },
                // 构造查询参数
                buildSearchParam(extParam) {
                    // 输入的查询条件
                    var param = {
                        body: this.searchParam ? JSON.parse(JSON.stringify(this.searchParam)) : {}
                    };
                    // 分页参数
                    param.pageNumber = this.page.pageNumber;
                    param.pageSize = this.page.pageSize;
                    // 排序参数
                    if (this.sortBy) {
                        param.body.dbSortBy = this.sortBy;
                    }
                    if (this.sortType) {
                        param.body.dbSortType = this.sortType;
                    }
                    // 指定的扩展参数, 和上面参数重复的, 以扩展参数为准, 可以用来参数校准
                    if (extParam) {
                        param = jo.mergeObject(param, extParam);
                    }
                    // 参数处理钩子
                    if (typeof this.handleParam == 'function') {
                        this.handleParam(param);
                    }
                    return param;
                }
            },
            mounted() {
                this.goPage(1);
                console.info('[jo-el-table] mounted');
            }
        },
        'jo-el-data': {
            emits: ['update:modelValue'],
            props: {
                modelValue: {
                    type: [String, Number, Boolean, Object, Array, Date, Function, Symbol],
                    default: ''
                },
                url: {
                    type: String,
                    default: ''
                },
                // 绑定根属性名
                bindRootName: {
                    type: [String],
                    default: ''
                },
                // 数据查询参数
                param: {
                    type: [String, Number, Object, Array],
                    default: {}
                }
            },
            data: function () {
                return {
                    data: []
                };
            },
            // template: `<div class="jo-el-data-bar"><slot :data="data" :url="url"></slot></div>`,
            template: `<slot :data="data" :url="url"></slot>`,
            mounted() {
                var _this = this;
                if (this.url) {
                    jo.postJsonAjax(this.url, {body: this.param || {}}).success(function (json) {
                        _this.data = json.data;
                        if (_this.bindRootName) {
                            _this.$root[_this.bindRootName] = json.data;
                        }
                        _this.$emit('update:modelValue', json.data);
                    }).error(function (json) {
                        console.warn('[jo-el-data] error, url: ' + _this.url, json);
                    });
                }
                console.info('[jo-el-data] mounted, url: ' + this.url);
            }
        },
        'jo-el-dialog-form': joEl.buildCommonComponentParam({
            emits: ['ok'],
            props: {
                // 表单ref
                formRef: {
                    type: String,
                    default: 'joElDialogFormRef'
                },
                // 弹层标题
                title: {
                    type: String,
                    default: ''
                },
                // 弹层宽度
                dialogWidth: {
                    type: [Number, String],
                    default: '60%'
                },
                // 表单项标题位置
                labelPosition: {
                    type: String,
                    default: 'right'
                },
                // 表单项标题宽度
                labelWidth: {
                    type: [Number, String],
                    default: '100px'
                },
                // 表单校验规则
                rules: {
                    type: Object,
                    default: function () {
                        return {};
                    }
                },
            },
            data: function () {
                return {
                    // 弹层展示标识
                    showDialog: false,
                    // 表单数据
                    data: {},
                };
            },
            computed: {},
            template: `
            <el-dialog v-model="showDialog" :title="title" :width="dialogWidth">
                <el-form :ref="formRef" :model="data" :label-position="labelPosition" :rules="rules" :label-width="labelWidth">
                    <slot :data="data"></slot>
                </el-form>
                <template #footer>
                    <div>
                        <slot name="footer">
                            <el-button @click="close">取消</el-button>
                            <el-button type="primary" @click="ok">确认</el-button>
                        </slot>
                    </div>
                </template>
            </el-dialog>
            `,
            methods: {
                // 获取实例对象
                getRef() {
                    return this;
                },
                // 获取表单ref对象
                getFormRef() {
                    return this.$refs[this.formRef];
                },
                // 获取表单数据
                getData() {
                    return this.data;
                },
                // 打开弹层
                open(data, openedFunc) {
                    // 弹层打开前构造表单数据
                    if (typeof this.call.buildData == 'function') {
                        data = this.call.buildData(data || {});
                    }
                    // 设置表单数据
                    this.data = data || {};
                    // 展示
                    this.showDialog = true;
                    // 打开后回调
                    if (typeof openedFunc == 'function') {
                        this.$nextTick(() => {
                            openedFunc(data, this.command);
                        });
                    }
                },
                // 关闭弹层
                close() {
                    // 关闭弹层前清理数据
                    if (typeof this.call.cleanData == 'function') {
                        this.call.cleanData(this.data);
                    }
                    // 关闭
                    this.showDialog = false;
                },
                // 确定按钮
                ok() {
                    this.getFormRef().validate((success, invalidFields) => {
                        if (success) {
                            // 触发ok事件
                            this.$emit('ok', this.data, this.command);
                        } else {
                            console.warn('表单校验失败:', invalidFields);
                        }
                    });
                },
            },
            mounted() {
                // 初始化实例对象
                this.initInstance();
                console.info('[jo-el-dialog-form] mounted', this.command);
            }
        }),
        // 文件上传按钮
        'jo-el-upload-btn': {
            emits: ['fileChange', 'submitUpload', 'uploadSuccess', 'uploadError'],
            props: {
                // 按钮plain样式
                plain: {
                    type: Boolean,
                    default: true
                },
                // 按钮type属性
                type: {
                    type: String,
                    default: 'success'
                },
                // title
                title: {
                    type: String,
                    default: '上传'
                },
                // 宽度
                width: {
                    type: String,
                    default: '500px'
                },
                // 是否可以多选文件
                multiple: {
                    type: Boolean,
                    default: true
                },
                // 模板下载url
                templateUrl: {
                    type: String,
                    default: ''
                },
                // 是否显示进度
                progress: {
                    type: Boolean,
                    default: false
                },
                // 存储方式, fs:fs服务端本地文件系统,cos:腾讯云存储,为空则通过submitUpload事件交由调用方处理
                storage: {
                    type: String,
                    default: 'fs'
                },
                // 上传地址
                url: {
                    type: String,
                    default: '/fs/file/upload'
                },
                // 对象存储临时签名
                cosStsUrl: {
                    type: String,
                    default: '/fs/cos/sts'
                },
                // cos分桶
                cosBucket: {
                    type: String,
                    default: ''
                },
                // cos区域
                cosRegion: {
                    type: String,
                    default: ''
                },
                // 保存文件夹
                folderId: {
                    type: String,
                    default: ''
                },
                // 业务code
                businessCode: {
                    type: String,
                    default: ''
                },
                // 构造上传参数函数
                buildUploadData: {
                    type: Function,
                    default: (file) => {
                        return {};
                    }
                },
            },
            data: function () {
                return {
                    // 文件信息,{file,uploadStatus,errorMsg,key}
                    fileDescList: [],
                    // 上传弹层
                    layerShow: false,
                    // id
                    fileInputId: 'file_upload_' + jo.getUUID(16),
                    // cos客户端
                    cosClient: null
                };
            },
            computed: {
                // 选中的文件
                files() {
                    var arr = [];
                    jo.forEach(this.fileDescList, item => {
                        arr.push(item.file);
                    });
                    return arr;
                },
                // 待上传文件
                toBeUploadList() {
                    var arr = [];
                    jo.forEach(this.fileDescList, item => {
                        if (item.uploadStatus === 10) {
                            arr.push(item);
                        }
                    });
                    return arr;
                },
                // 是否可以确认上传
                canUpload() {
                    return jo.arrayIsNotEmpty(this.toBeUploadList);
                },
                // 上传按钮加载中显示
                uploadLoading() {
                    for (let i = 0; i < this.fileDescList.length; i++) {
                        var item = this.fileDescList[i];
                        if (item.uploadStatus === 20) {
                            return true;
                        }
                    }
                    return false;
                }
            },
            template: `
            <el-button :type="type" @click="uploadClick" :plain="plain">
                <slot>上传</slot>
                <el-dialog v-model="layerShow" :width="width">
                    <template #header>
                        <div style="text-align: left;color: #333333;font-size: 16px;">{{title}}</div>
                    </template>
                    <div style="text-align: left;">
                        <div v-for="(item, idx) in fileDescList" style="padding: 5px;">
                            <div style="display: flex;align-items: center;">
                                <div style="flex: 1;"><i class="fa fa-file-text-o"></i> {{item.name}}</div>
                                <div><i class="fa fa-close" style="color: #f56c6c;" @click="removeInner(idx)"></i></div>
                            </div>
                            <div v-if="progress" style="margin-top: 2px;height: 15px;box-sizing: border-box;">
                                <el-progress v-if="item.uploadStatus===30" :percentage="item.percentage" :stroke-width="6" status="success">
                                    <i class="fa fa-check-circle-o" style="color: #67C23A;"></i>
                                </el-progress>
                                <el-progress v-else-if="item.uploadStatus===40" :percentage="item.percentage" :stroke-width="6" status="exception">
                                    <i class="fa fa-exclamation-triangle" style="color: #F56C6C;"></i>
                                </el-progress>
                                <el-progress v-else="" :percentage="item.percentage" :stroke-width="6">
                                    <span>&nbsp;</span>
                                </el-progress>
                            </div>
                        </div>
                    </div>
                    <template #footer>
                        <span class="dialog-footer">
                            <el-button v-if="templateUrl" type="info" @click="templateDownloadInner">模板下载</el-button>
                            <el-button type="success" @click="selectFileClick">选择文件</el-button>
                            <el-button type="primary" :loading="uploadLoading" :disabled="!canUpload" @click="submitUploadInner">确认</el-button>
                        </span>
                        <input :id="fileInputId" type="file" @change="fileChangeInner" :multiple="multiple" style="width: 0px;height: 0px;">
                    </template>
                </el-dialog>
            </el-button>
            `,
            methods: {
                // 上传点击
                uploadClick() {
                    if (!this.layerShow) {
                        // 清空上一次选项
                        this.fileDescList = [];
                        this.layerShow = true;
                    }
                },
                // 关闭
                closeLayer() {
                    this.layerShow = false;
                },
                // 确认
                submitUploadInner() {
                    // 待上传文件
                    var fileDescList = this.toBeUploadList;
                    if (jo.arrayIsEmpty(fileDescList)) {
                        jo.showTipsMsg('请选择待上传文件~');
                        return;
                    }
                    if (this.storage === 'fs' || this.storage === 'cos') {
                        if (this.multiple) {
                            jo.forEach(fileDescList, item => {
                                this.uploadOneInner(item);
                            });
                        } else {
                            this.uploadOneInner(fileDescList[0]);
                        }
                    } else {
                        this.$emit('submitUpload', this.files, () => {
                            this.closeLayer();
                        });
                    }
                },
                // 进度处理
                handlePercentage(fileDesc) {
                    // 预计上传需要的毫秒数, 按每秒512kb的速度算
                    var totalMills = parseInt(jo.getDefVal(fileDesc.size, 0) / 1024 / 512 * 1000);
                    if (totalMills < 1000) {
                        fileDesc.percentage = 50;
                    } else {
                        // 每个进度间隔多少毫秒
                        var stepMills = parseInt(totalMills / 100);
                        var go = () => {
                            if (fileDesc.uploadStatus === 20 && fileDesc.percentage < 99) {
                                fileDesc.percentage++;
                                window.setTimeout(go, stepMills);
                            }
                        }
                        go();
                    }
                },
                // 上传单文件
                uploadOneInner(fileDesc) {
                    try {
                        // 上传中状态
                        fileDesc.uploadStatus = 20;
                        // 处理进度
                        this.handlePercentage(fileDesc);
                        // 上传参数
                        var param = this.buildUploadData(fileDesc.file) || {};
                        if (this.storage === 'fs') {
                            this.upload_file_fs(fileDesc, param);
                        } else if (this.storage === 'cos') {
                            this.upload_file_cos(fileDesc, param);
                        } else {
                            throw new Error('未知的存储类型:' + this.storage);
                        }
                    } catch (e) {
                        console.error('[jo-el-upload-btn] 上传发生异常:', fileDesc, e);
                    }
                },
                // fs文件上传
                upload_file_fs(fileDesc, param) {
                    var _param = {
                        folderId: this.folderId || '',
                        businessCode: this.businessCode || '',
                    };
                    for (var k in param) {
                        if (_param[k]) {
                            console.warn('[jo-el-upload-btn] 自定义参数与内置参数重复,属性/内置参数/自定义参数为:', [k, _param, param]);
                        }
                        _param[k] = param[k];
                    }
                    jo.postFile(this.url, fileDesc.file, _param, (json, file, data) => {
                        console.info('[jo-el-upload-btn] 上传成功:%o', [json, file, data]);
                        this.handle_upload_success(fileDesc, json, data);
                    }, (json, file, data) => {
                        console.warn('[jo-el-upload-btn] 上传失败:%o', [json, file, data]);
                        this.handle_upload_error(fileDesc, json, data);
                    });
                },
                // cos文件上传
                upload_file_cos(fileDesc, param) {
                    var fileId = this.buildCosFileId(fileDesc.file);
                    jo.cos.upload({
                        file: fileDesc.file,
                        fileId: fileId,
                        bucket: this.cosBucket,
                        region: this.cosRegion,
                        stsUrl: this.cosStsUrl,
                        success: (err, data) => {
                            console.info('[jo-el-upload-btn] cos上传成功:', err, data);
                            var _param = {
                                id: fileId,
                                name: fileDesc.file.name,
                                saveName: fileId,
                                fileType: 'FILE',
                                fileSize: fileDesc.file.size || 0,
                                addr: data.Location.indexOf('http://') === 0 || data.Location.indexOf('https://') === 0 ? data.Location : 'http://' + data.Location,
                                path: data.Location.indexOf('http://') === 0 || data.Location.indexOf('https://') === 0 ? data.Location : 'http://' + data.Location,
                                storageMode: 'cos',
                                storageConfig: JSON.stringify({
                                    bucket: this.cosBucket,
                                    region: this.cosRegion,
                                    key: fileId,
                                    etag: data.ETag,
                                    requestId: data.RequestId
                                }),
                                folderId: this.folderId || '',
                                businessCode: this.businessCode || '',
                            }
                            for (var k in param) {
                                if (_param[k]) {
                                    console.info('[jo-el-upload-btn] 自定义参数与内置参数重复,属性/内置参数/自定义参数为:', [k, _param, param]);
                                }
                                _param[k] = param[k];
                            }
                            jo.postJson('/fs/fsFile/insertAfterCosUpload', _param).success(json => {
                                console.warn('[jo-el-upload-btn] cos结果写入fs成功:', err, data);
                                this.handle_upload_success(fileDesc, json, _param);
                            }).error(json => {
                                console.warn('[jo-el-upload-btn] cos结果写入fs失败:', err, data);
                                this.handle_upload_error(fileDesc, json, _param);
                            });
                        },
                        error: (err, data) => {
                            console.warn('[jo-el-upload-btn] cos上传失败:', err, data);
                            this.handle_upload_error(fileDesc, {code: -1, info: 'cos error!'}, param);
                        },
                    });
                },
                // 处理上传成功, fileDesc:文件描述, json:上传结果, param:上传参数
                handle_upload_success(fileDesc, json, param) {
                    // 上传成功
                    fileDesc.uploadStatus = 30;
                    // 上传进度
                    fileDesc.percentage = 100;
                    this.$emit('uploadSuccess', json, fileDesc.file, param, () => {
                        this.closeLayer();
                    });
                },
                // 处理上传失败
                handle_upload_error(fileDesc, json, param) {
                    // 上传成功
                    fileDesc.uploadStatus = 40;
                    // 上传进度
                    fileDesc.percentage = 100;
                    // 错误信息
                    fileDesc.errorMsg = json && json.info ? json.info : 'error!';
                    this.$emit('uploadError', json, fileDesc.file, param, () => {
                        this.closeLayer();
                    });
                },
                // 构造cos文件id
                buildCosFileId(file) {
                    var name = file.name;
                    var suffix = name.substring(name.lastIndexOf('.'));
                    if (suffix && suffix.length > 12) {
                        return 'cos_' + jo.getUUID(20).toLowerCase();
                    } else {
                        return 'cos_' + jo.getUUID(16).toLowerCase() + suffix;
                    }
                },
                // 选择文件
                selectFileClick() {
                    document.getElementById(this.fileInputId).click();
                },
                // 追加文件
                addFileInner(file, clean) {
                    if (clean) {
                        console.info('[jo-el-upload-btn] 清空文件列表', this.fileDescList);
                        this.fileDescList = [];
                    }
                    this.fileDescList.push({
                        // 文件对象
                        file: file,
                        // 上传状态,10:初始化,20:上传中,30:成功,40:失败
                        uploadStatus: 10,
                        // 错误信息
                        errorMsg: '',
                        // 上传进度
                        percentage: 0,
                        // 文件标识key
                        key: this.buildFileKey(file),
                        // 文件名
                        name: file.name,
                    });
                },
                // 构造文件标识key
                buildFileKey(file) {
                    return file.name + '_' + file.size + '_' + file.lastModified;
                },
                // 文件发生变化, 说明用户重新选了文件
                fileChangeInner(event) {
                    if (event && event.target && event.target.files && event.target.files.length > 0) {
                        // 当前已有文件map,用来去重
                        var map = jo.array2Object(this.fileDescList, function (item) {
                            return item.key;
                        });
                        var changeFlag = false;
                        var changeFiles = [];
                        if (this.multiple) {
                            jo.forEach(event.target.files, (item, idx) => {
                                if (!map[this.buildFileKey(item)]) {
                                    this.addFileInner(item);
                                    changeFlag = true;
                                    changeFiles.push(item);
                                } else {
                                    console.warn('[jo-el-upload-btn] 当前文件已被选择', item);
                                }
                            });
                        } else {
                            var oldFile = jo.arrayIsNotEmpty(this.fileDescList) ? this.fileDescList[0].file : null;
                            var newFile = event.target.files[0];
                            if (!map[this.buildFileKey(newFile)]) {
                                this.addFileInner(newFile, true);
                                changeFlag = true;
                                if (oldFile) {
                                    changeFiles.push(oldFile);
                                }
                            } else {
                                console.warn('[jo-el-upload-btn] 当前文件已被选择', newFile);
                            }
                        }
                        console.info('[jo-el-upload-btn] 当前所有文件', this.fileDescList);
                        if (changeFlag) {
                            this.$emit('fileChange', this.files, changeFiles);
                        }
                        // 清空file框, 不影响下次选择
                        document.getElementById(this.fileInputId).value = null;
                    } else {
                        console.warn('[jo-el-upload-btn] 文件变更获取文件失败', event);
                    }
                },
                // 移除文件
                removeInner(idx) {
                    var fileDesc = this.fileDescList[idx];
                    this.fileDescList.splice(idx, 1);
                    this.$emit('fileChange', this.files, [fileDesc.file]);
                },
                // 模板下载
                templateDownloadInner() {
                    if (this.templateUrl) {
                        jo.newWindow(this.templateUrl);
                    }
                }
            },
            mounted() {
                console.info('[jo-el-upload-btn] mounted');
            }
        },
        // 权限控制
        'jo-el-auth': joEl.buildCommonComponentParam({
            emits: [],
            props: {
                code: {
                    type: String,
                    default: ''
                },
            },
            data: function () {
                return {
                    ownedAuth: joEl.auth_code_map
                };
            },
            computed: {
                showFlag() {
                    if (this.code) {
                        return this.ownedAuth[this.code];
                    }
                    return true;
                }
            },
            template: `
                <slot v-if="showFlag"></slot>
            `,
            methods: {
                // 加载
                loadUserAuthCodes() {
                    // 先从缓存取
                    var cacheCodes = jo.cache.get('jo-el-auth.codes');
                    // console.info('[权限控制] 从缓存取出用户权限:', cacheCodes);
                    if (!cacheCodes) {
                        // 缓存没有再请求后端, 加锁, 减少重复请求
                        if (jo.cache.lock('jo-el-auth.lock')) {
                            jo.postJson('/ums/umsAuth/getUserOwnedResourceCodes').success(json => {
                                cacheCodes = json.data || [];
                                // 刷新权限集
                                this.refreshAuthCode(cacheCodes);
                                // 写缓存
                                jo.cache.set('jo-el-auth.codes', cacheCodes, 60 * 30 * 1000);
                            }).error(json => {
                                console.error('加载用户权限失败', json);
                            });
                        }
                    } else {
                        // 刷新权限集
                        this.refreshAuthCode(cacheCodes);
                    }
                },
                // 刷新权限集
                refreshAuthCode(codes) {
                    // console.debug('[刷新用户权限code集] 原权限:', joEl.auth_code_map);
                    for (var k in this.ownedAuth) {
                        this.ownedAuth[k] = false;
                    }
                    jo.forEach(codes, item => {
                        this.ownedAuth[item] = true;
                    });
                    // console.debug('[刷新用户权限code集] 刷新后权限:', joEl.auth_code_map);
                }
            },
            mounted() {
                // 初始化实例对象
                this.initInstance();
                // 加载用户权限
                this.loadUserAuthCodes();
                console.info('[jo-el-auth] mounted');
            }
        }),
        // 上传组件
        'jo-el-form-upload': joEl.buildCommonComponentParam({
            emits: ['update:modelValue'],
            props: {
                modelValue: {
                    type: [String, Number],
                    default: ''
                },
                // 文件上传成功后参数绑定的文件属性
                bindFileAttr: {
                    type: String,
                    default: 'id'
                },
                downloadText: {
                    type: [String],
                    default: '下载'
                },
                placeholder: {
                    type: [String],
                    default: '待上传'
                },
                // 是否显示进度
                progress: {
                    type: Boolean,
                    default: true
                },
                // 存储方式, fs:fs服务端本地文件系统,cos:腾讯云存储,为空则通过submitUpload事件交由调用方处理
                storage: {
                    type: String,
                    default: 'fs'
                },
                // 上传地址
                url: {
                    type: String,
                    default: '/fs/file/upload'
                },
                // 对象存储临时签名
                cosStsUrl: {
                    type: String,
                    default: '/fs/cos/sts'
                },
                // cos分桶
                cosBucket: {
                    type: String,
                    default: ''
                },
                // cos区域
                cosRegion: {
                    type: String,
                    default: ''
                },
                // 保存文件夹
                folderId: {
                    type: String,
                    default: ''
                },
                // 业务code
                businessCode: {
                    type: String,
                    default: ''
                },
                // 构造上传参数函数
                buildUploadData: {
                    type: Function,
                    default: (file) => {
                        return {};
                    }
                },
            },
            data: function () {
                return {};
            },
            computed: {
                downloadShow() {
                    return jo.isValid(this.modelValue);
                },
                // 下载链接
                downloadHref() {
                    if (!this.downloadShow) {
                        return '';
                    }
                    var fileId = this.modelValue;
                    if (this.bindFileAttr === 'path') {
                        // 文件名
                        var name = this.modelValue.substring(this.modelValue.lastIndexOf('/') + 1)
                        if (name.indexOf('cos_') === 0) {
                            // cos存储, 文件名就是id
                            fileId = name;
                        } else {
                            // 本地存储, 文件名去掉后缀就是id
                            fileId = name.substring(0, name.lastIndexOf('.'));
                        }
                    }
                    return '/fs/file/download?fileId=' + encodeURIComponent(fileId);
                }
            },
            template: `
            <div style="display: flex;width: 100%;">
                <div style="flex: 1;">
                    <slot>
                        <el-link v-if="downloadShow" type="primary" :href="downloadHref" target="_blank">{{downloadText}}</el-link>
                        <el-text v-else="" type="info">{{placeholder}}</el-text>
                    </slot>
                </div>
                <div style="margin-left: 10px;">
                    <jo-el-upload-btn ref="uploadBtnRef" :url="url" :progress="progress" :storage="storage" 
                        :cos-sts-url="cosStsUrl" :cos-bucket="cosBucket" :cos-region="cosRegion" 
                        :folder-id="folderId" :business-code="businessCode" :build-upload-data="buildUploadData" 
                        @upload-success="uploadSuccessInner"></jo-el-upload-btn>
                </div>
            </div>
            `,
            methods: {
                // 上传成功
                uploadSuccessInner(json, file, data, closeFunc) {
                    this.updateModelValue(json.data[this.bindFileAttr || 'id']);
                    closeFunc();
                },
                // 上传处理 @submit-upload="uploadHandle"
                uploadHandle(files, closeCall) {
                    if (files[0]) {
                        jo.postFile(files[0], {}, (json, file, data) => {
                            this.updateModelValue(json.data[this.bindFileAttr || 'id']);
                            jo.showSuccessMsg('上传成功');
                            closeCall();
                        }, (json, file, data) => {
                            jo.showErrorMsg(json.info || '上传失败!');
                            console.warn('[file_upload] 上传失败', json, file, data);
                        });
                    }

                },
                updateModelValue(newVal) {
                    // 当内部数据发生变化时，发送更新事件
                    this.$emit('update:modelValue', newVal);
                },
            },
            mounted() {
                // 初始化实例对象
                this.initInstance();
                console.info('[jo-el-form-upload] mounted', this.command);
            }
        }),
        // 文件展示
        'jo-el-file-show': joEl.buildCommonComponentParam({
            props: {
                fileId: {
                    type: [String, Number],
                    default: ''
                },
                downloadText: {
                    type: [String],
                    default: '下载'
                }
            },
            data: function () {
                return {};
            },
            computed: {
                downloadShow() {
                    return jo.isValid(this.fileId);
                },
                // 下载链接
                downloadHref() {
                    return '/fs/file/download?fileId=' + encodeURIComponent(this.fileId);
                }
            },
            template: `
            <el-link v-if="downloadShow" type="primary" :href="downloadHref" target="_blank">{{downloadText}}</el-link>
            `,
            methods: {},
            mounted() {
                // 初始化实例对象
                this.initInstance();
                console.info('[jo-el-file-show] mounted', this.command);
            }
        }),
        // markdown富文本组件
        'jo-el-editor': joEl.buildCommonComponentParam({
            emits: ['update:modelValue', 'update:htmlCode'],
            props: {
                // 双向数据绑定:md文本
                modelValue: {
                    type: [String],
                    default: ''
                },
                // 双向数据绑定:html代码
                htmlCode: {
                    type: [String],
                    default: ''
                },
                // 高度
                height: {
                    type: [Number, String],
                    default: 300
                },
                // 工具栏
                toolbar: {
                    type: [String, Object],
                    default: 'default'
                },
                // placeholder文案
                placeholder: {
                    type: [String],
                    default: 'Enjoy Markdown! coding now...'
                },
                // 是否禁用
                disabled: {
                    type: [Boolean],
                    default: false
                },
                // 文件夹id
                folderId: {
                    type: [String],
                    default: ''
                }
            },
            data() {
                return {
                    editorId: 'editor_' + jo.getUUID(6).toLowerCase(),
                    editor: null,
                    toolbarConfig: {
                        default: [
                            "undo", "redo", "|"
                            , "bold", "del", "italic", "quote", "ucwords", "uppercase", "lowercase", "|"
                            , "h1", "h2", "h3", "h4", "|"
                            , "list-ul", "list-ol", "hr", "|"
                            , "link", "image", "code-block", "table", "html-entities", "|"
                            , "watch", "preview", "fullscreen", "|"
                            , "help"
                        ],
                        core: [
                            "undo", "redo", "|",
                            "bold", "del", "italic", "|",
                            "h1", "h2", "h3",
                            "list-ul", "list-ol", "|",
                            "link", "image", "code-block", "table", "|",
                            "help"
                        ],
                        full: [
                            "undo", "redo", "|",
                            "bold", "del", "italic", "quote", "ucwords", "uppercase", "lowercase", "|",
                            "h1", "h2", "h3", "h4", "h5", "h6", "|",
                            "list-ul", "list-ol", "hr", "|",
                            "link", "reference-link", "image", "code", "preformatted-text", "code-block", "table", "datetime", "emoji", "html-entities", "pagebreak", "|",
                            "goto-line", "watch", "preview", "fullscreen", "clear", "search", "|",
                            "help"
                        ],
                        simple: [
                            "undo", "redo", "|",
                            "bold", "del", "italic", "quote", "|",
                            "h1", "h2", "h3", "h4", "|",
                            "list-ul", "list-ol", "|",
                            "link", "image", "code-block", "table", "|",
                            "watch", "preview", "fullscreen", "|",
                            "help"
                        ],
                        mini: [
                            "undo", "redo", "|",
                            "watch", "preview", "|",
                            "help"
                        ]
                    }
                };
            },
            computed: {},
            watch: {
                modelValue(newVal, oldVal) {
                    if (this.editor && this.editor.cm && newVal !== oldVal) {
                        console.debug('[jo-el-editor] modelValue值发生改变:', [newVal, oldVal]);
                        if (!oldVal && newVal) {
                            var md = this.editor.getMarkdown();
                            if (md !== newVal) {
                                this.editor.setMarkdown(newVal);
                                console.info('[jo-el-editor] modelValue值发生改变,设置markdown值:', newVal);
                            } else {
                                console.info('[jo-el-editor] modelValue值发生改变,不处理:当前markdown内容与新值相同:', [newVal, oldVal]);
                            }

                        } else {
                            console.debug('[jo-el-editor] modelValue值发生改变,不处理:老值有效,目前仅支持从无到有的处理:', [newVal, oldVal]);
                        }
                    } else {
                        console.debug('[jo-el-editor] modelValue值发生改变,不处理:编辑器还未初始化:', [newVal, oldVal, this.editor]);
                    }
                }
            },
            template: `
            <div style="width:100%;position: relative;">
                <div v-if="disabled" style="position: absolute;inset: 0;z-index: 80;background-color: rgba(245, 247, 250, 0.7);cursor: not-allowed;"></div>
                <div class="editormd" :key="editorId" :id="editorId">
                    <textarea class="editormd-markdown-textarea"></textarea>
                    <textarea class="editormd-html-textarea"></textarea>
                </div>
            </div>
            `,
            methods: {
                // 双向绑定值更新
                updateModelValue(newVal) {
                    // 当内部数据发生变化时，发送更新事件
                    this.$emit('update:modelValue', newVal);
                },
                // 双向绑定值更新
                updateHtmlCode(newVal) {
                    // 当内部数据发生变化时，发送更新事件
                    this.$emit('update:htmlCode', newVal);
                },
                // 工具栏
                buildToolbars() {
                    var arr = typeof this.toolbar == 'string' ? this.toolbarConfig[this.toolbar] : this.toolbar;
                    if (jo.arrayIsNotEmpty(arr)) {
                        arr = jo.copyArray(arr);
                    } else {
                        console.warn('[jo-el-editor] 工具栏配置无效:', this.toolbar);
                        arr = jo.copyArray(this.toolbarConfig.default);
                    }
                    return arr;
                },
                // 初始化
                initEditor() {
                    if (this.editor) {
                        console.info('[jo-el-editor] editor[%s]实例已存在:', this.editorId, this.editor);
                        return;
                    }
                    joEl[this.editorId] = this;

                    this.editor = editormd(this.editorId, {
                        width: "100%",
                        height: this.height,
                        markdown: this.modelValue,
                        toolbarIcons: this.buildToolbars(),
                        placeholder: this.placeholder,
                        path: '/static/plugin/editor_md/lib/',
                        saveHTMLToTextarea: true,  //将html保存到第二个textarea
                        //dialogLockScreen : false,   // 设置弹出层对话框不锁屏，全局通用，默认为 true
                        //dialogShowMask : false,     // 设置弹出层对话框显示透明遮罩层，全局通用，默认为 true
                        //dialogDraggable : false,    // 设置弹出层对话框不可拖动，全局通用，默认为 true
                        //dialogMaskOpacity : 0.4,    // 设置透明遮罩层的透明度，全局通用，默认值为 0.1
                        //dialogMaskBgColor : "#000", // 设置透明遮罩层的背景颜色，全局通用，默认为 #fff
                        imageUpload: true,
                        imageFormats: ["jpg", "jpeg", "gif", "png", "bmp", "webp"],
                        imageUploadURL: "/fs/editorMd/upload/image?folderId=" + this.folderId,
                        // 事件使用箭头函数时, 里面的this永远指向的是最后一个组件实例, 不知道为啥..
                        // onload: ()=> {
                        //     console.info('[jo-el-editor] editor[%s] 加载完毕,入参:', this.editorId, arguments);
                        //     this.editor.setMarkdown(val);
                        //
                        // },
                        // onchange: ()=> {
                        //     console.info('[jo-el-editor] editor[%s] onchange,入参:', this.editorId, arguments);
                        //     var md = this.editor.getMarkdown();
                        //     this.updateModelValue(md);
                        //     var html = this.editor.getHTML();
                        //     this.updateHtmlCode(html);
                        //     console.info('[jo-el-editor] editor[%s] onchange', this.editorId, this, md, html, this.editorId, this.editor)
                        // },
                        onload: function () {
                            var val = joEl[this.id].modelValue
                            this.setMarkdown(val);
                            console.debug('[jo-el-editor] editor[%s] 加载完毕:', this.id, val);

                        },
                        onchange: function () {
                            var md = this.getMarkdown();
                            var html = this.getHTML();
                            var _this = joEl[this.id];
                            _this.updateModelValue(md);
                            _this.updateHtmlCode(html);
                            console.debug('[jo-el-editor] editor[%s] onchange', _this.editorId, [md, html]);
                        },

                        /*
                         上传的后台只需要返回一个 JSON 数据，结构如下：
                         {
                         success : 0 | 1,           // 0 表示上传失败，1 表示上传成功
                         message : "提示的信息，上传成功或上传失败及错误信息等。",
                         url     : "图片地址"        // 上传成功时才返回
                         }
                         */
                    });
                }
            },
            mounted() {
                // 初始化实例对象
                this.initInstance();
                console.info('[jo-el-editor] mounted:', this.editorId, this.command);
                console.info('[jo-el-editor] editor[%s] 待初始化内容:', this.editorId, this.modelValue);
                // 稍微延时下, 解决偶发的光标问题, 暂时没找到原因
                window.setTimeout(() => {
                    this.initEditor();
                }, 50);
            }
        }),
        'jo-el-icon-select': joEl.buildCommonComponentParam({
            emits: ['update:modelValue'],
            props: {
                modelValue: {
                    type: [String],
                    default: ''
                },
            },
            data: function () {
                return {
                    // 内部绑定值
                    innerModelValue: this.modelValue,
                    // 图标枚举常量
                    joEl_constants_iconOptions: joEl.constants.iconOptions,
                };
            },
            computed: {},
            watch: {
                modelValue(newVal, oldVal) {
                    if (newVal !== this.innerModelValue) {
                        this.innerModelValue = newVal;
                    }
                }
            },
            template: `
                <el-select v-model="innerModelValue" placeholder="" 
                            @change="innerSelectChange"
                            v-bind="$attrs"
                            popper-class="qy-icon-selected" 
                            :filterable="true" 
                            :fit-input-width="true"
                            :allow-create="true"
                            :clearable="true">
                    <template #prefix="scope"><i class="fa" :class="[innerModelValue]"></i></template>
                    <el-option-group v-for="group in joEl_constants_iconOptions" :label="group.name">
                        <el-option v-for="icon in group.icons" :value="icon" :title="icon">
                            <i class="fa" :class="[icon]"></i>
                        </el-option>
                    </el-option-group>
                </el-select>
            `,
            methods: {
                // 双向绑定值更新
                innerSelectChange(newVal) {
                    // 当内部数据发生变化时，发送更新事件
                    this.$emit('update:modelValue', newVal);
                }
            },
            mounted() {
                // 初始化实例对象
                this.initInstance();
                console.info('[jo-el-icon-select] mounted');
            }
        }),
    };
})();