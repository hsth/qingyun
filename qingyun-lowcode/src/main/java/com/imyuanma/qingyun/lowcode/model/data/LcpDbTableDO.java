package com.imyuanma.qingyun.lowcode.model.data;

import lombok.Data;


/**
 * 数据库表模型
 *
 * @author wangjy
 * @date 2022/05/04 14:34:10
 */
@Data
public class LcpDbTableDO {
    /**
     * 表名
     */
    private String tableName;
    /**
     * 表注释
     */
    private String tableComment;
    /**
     * 表数据量
     */
    private Long tableRows;
    /**
     * 表数据大小(Byte)
     */
    private Long dataLength;
}
