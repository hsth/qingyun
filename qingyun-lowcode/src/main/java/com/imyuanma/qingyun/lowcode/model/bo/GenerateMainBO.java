package com.imyuanma.qingyun.lowcode.model.bo;

import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.interfaces.common.model.enums.EYesOrNoEnum;
import com.imyuanma.qingyun.lowcode.model.bo.ui.LcpPageConfig;
import com.imyuanma.qingyun.lowcode.model.enums.ELcpFormControlTypeEnum;
import com.imyuanma.qingyun.lowcode.model.enums.ELcpModelExtendsTypeEnum;
import com.imyuanma.qingyun.lowcode.model.enums.ELcpTraceTypeEnum;
import lombok.Data;

import java.util.List;
import java.util.Optional;

/**
 * 代码生成业务模型
 *
 * @author wangjy
 * @date 2022/05/04 16:15:38
 */
@Data
public class GenerateMainBO {
    /**
     * 主键
     */
    private Long id;
    /**
     * 名称
     */
    private String name;
    /**
     * 表名
     */
    private String tableName;
    /**
     * java类名
     */
    private String className;
    /**
     * 基础包路径,例如com.imyuanma.qingyun
     */
    private String packagePath;
    /**
     * 模块名
     */
    private String moduleName;
    /**
     * 代码文件保存路径
     */
    private String savePath;
    /**
     * 作者
     */
    private String author;
    /**
     * 表单项label位置配置,left,right,top
     */
    private String formLabelPosition;
    /**
     * 表单项label宽度,例如120px
     */
    private String formLabelWidth;
    /**
     * 全局css
     */
    private String globalCss;
    /**
     * 全局js
     */
    private String globalJs;

    /**
     * 扩展配置
     */
    private GenerateMainExtBO extInfo;
    /**
     * 表单页配置
     */
    private LcpPageConfig formPageConfig;

    /**
     * 字段配置明细
     */
    private List<GenerateFieldBO> fieldList;

    /**
     * 获取主键字段
     *
     * @return
     */
    public GenerateFieldBO findPrimaryKeyField() {
        GenerateFieldBO generateFieldBO = this.fieldList.stream().filter(item -> "PRI".equalsIgnoreCase(item.getFieldDbKey())).findFirst().orElse(null);
        if (generateFieldBO == null) {
            generateFieldBO = this.fieldList.stream().filter(item -> "id".equalsIgnoreCase(item.getFieldDbName())).findFirst().orElse(null);
        }
        return generateFieldBO;
    }

    /**
     * 是否继承BaseDO
     *
     * @return
     */
    public boolean isExtendsBaseDO() {
        return extInfo != null
                && (ELcpModelExtendsTypeEnum.BaseDO.getCode().equals(extInfo.getModelExtendsType())
                || ELcpModelExtendsTypeEnum.BaseDOAndFill.getCode().equals(extInfo.getModelExtendsType()));
    }

    /**
     * 是否继承DbSortDO
     *
     * @return
     */
    public boolean isExtendsDbSortDO() {
        return extInfo != null && ELcpModelExtendsTypeEnum.DbSortDO.getCode().equals(extInfo.getModelExtendsType());
    }

    /**
     * 是否支持完整的链路追踪
     *
     * @return
     */
    public boolean isSupportTraceAll() {
        return extInfo != null && ELcpTraceTypeEnum.ALL.getCode().equals(extInfo.getTraceType());
    }

    /**
     * 是否支持web入口的链路追踪
     *
     * @return
     */
    public boolean isSupportTraceWeb() {
        return extInfo != null && (ELcpTraceTypeEnum.ALL.getCode().equals(extInfo.getTraceType()) || ELcpTraceTypeEnum.ONLY_WEB.getCode().equals(extInfo.getTraceType()));
    }

    /**
     * 存在图标选择控件
     *
     * @return
     */
    public boolean isExistsIconSelectField() {
        if (fieldList != null) {
            for (GenerateFieldBO fieldBO : fieldList) {
                if (ELcpFormControlTypeEnum.ICON_SELECT.getType().equals(fieldBO.getFormControlType())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 是否支持导入
     * @return
     */
    public boolean isSupportImportFlag() {
        return extInfo != null && EYesOrNoEnum.YES.getCode().equals(extInfo.getSupportImportExcel());
    }

    /**
     * 是否支持导出
     * @return
     */
    public boolean isSupportExportFlag() {
        return extInfo != null && EYesOrNoEnum.YES.getCode().equals(extInfo.getSupportExportExcel());
    }

    /**
     * 是否依赖cos对象存储
     * @return
     */
    public boolean dependTencentCos() {
        if (CollectionUtil.isNotEmpty(this.fieldList)) {
            for (GenerateFieldBO fieldBO : this.fieldList) {
                if (fieldBO != null && fieldBO.useCosUploadControl()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 是否依赖md编辑器
     * @return
     */
    public boolean dependEditorMd() {
        if (CollectionUtil.isNotEmpty(this.fieldList)) {
            for (GenerateFieldBO fieldBO : this.fieldList) {
                if (fieldBO != null && fieldBO.useEditorMdControl()) {
                    return true;
                }
            }
        }
        return false;
    }
}
