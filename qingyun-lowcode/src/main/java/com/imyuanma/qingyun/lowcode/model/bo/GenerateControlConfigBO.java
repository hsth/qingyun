package com.imyuanma.qingyun.lowcode.model.bo;

import com.imyuanma.qingyun.common.util.StringUtil;
import lombok.Data;

/**
 * 表单项其他配置
 *
 * @author wangjy
 * @date 2022/05/04 17:46:48
 */
@Data
public class GenerateControlConfigBO {
    /**
     * 最小值
     */
    private Long min;
    /**
     * 最大值
     */
    private Long max;
    /**
     * 步长
     */
    private Long step;
    /**
     * 数字输入控件是否展示控制按钮(加减按钮)
     * 默认不要
     */
    private Integer numberControls;

    /**
     * 行数
     */
    private Integer rows;
    /**
     * 自适应文本域 1/0
     */
    private Integer autosize;

    /**
     * 选中值
     */
    private String yesValue;
    /**
     * 未选中值
     */
    private String noValue;
    /**
     * 选中文案
     */
    private String yesText;
    /**
     * 未选中文案
     */
    private String noText;

    /**
     * 树选择支持选择任意节点, 1表示可以选择任意节点, 0表示只可以选择叶子节点
     */
    private Integer treeSupportCheckAny;
    /**
     * 子节点数据的字段名
     * 例如: 树组件, 树表格, 树选择等有子节点的组件用
     */
    private String childrenField;
    /**
     * 数据表格自定义格式化代码
     */
    private String listValueFormatHtml = "";

    // ==============文件上传 start==============
    /**
     * 上传存储策略 fs:本地存储 cos:腾讯对象存储cos
     */
    private String uploadStorage = "fs";
    /**
     * 上传地址
     */
    private String uploadUrl = "";
    /**
     * COS临时签名
     */
    private String cosStsUrl = "";
    /**
     * COS分桶
     */
    private String cosBucket = "";
    /**
     * COS区域
     */
    private String cosRegion = "";
    /**
     * 上传文件夹ID
     */
    private String uploadFolderId = "";
    /**
     * 上传组件参数绑定的字段
     * path: 文件url
     */
    private String uploadBindAttr = "";

    /**
     * 判断是否使用cos存储
     * @return
     */
    public boolean useCosStorage() {
        return "cos".equals(this.uploadStorage);
    }

    /**
     * 上传组件配置的属性值
     *
     * @return
     */
    public String buildUploadAttrHtml() {
        StringBuilder sb = new StringBuilder("");
        sb.append(eleAttrHtml("folder-id", this.uploadFolderId));
        sb.append(eleAttrHtml("storage", this.uploadStorage));
        sb.append(eleAttrHtml("bind-file-attr", this.uploadBindAttr));
        if (this.useCosStorage()) {
            sb.append(eleAttrHtml("cos-sts-url", this.cosStsUrl));
            sb.append(eleAttrHtml("cos-bucket", this.cosBucket));
            sb.append(eleAttrHtml("cos-region", this.cosRegion));
        } else {
            sb.append(eleAttrHtml("url", this.uploadUrl));
        }
        return sb.toString();
    }

    private String eleAttrHtml(String attr, String value) {
        return StringUtil.isNotBlank(value) ? String.format(" %s=\"%s\"", attr, value) : "";
    }
    // ==============文件上传 end==============


    // ==============md编辑器 start==============
    /**
     * 编辑器高度
     */
    private String editorMdHeight = "";
    /**
     * 编辑器工具栏, 具体枚举值参考组件代码
     */
    private String editorMdToolbar = "default";
    /**
     * 编辑器html代码绑定变量
     */
    private String editorHtmlBind = "";
    // ==============md编辑器 end==============

}
