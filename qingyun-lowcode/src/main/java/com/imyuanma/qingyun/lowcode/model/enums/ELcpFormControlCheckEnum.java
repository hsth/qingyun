package com.imyuanma.qingyun.lowcode.model.enums;

/**
 * 表单校验
 *
 * @author wangjy
 * @date 2022/04/02 10:43:41
 */
public enum ELcpFormControlCheckEnum {
    ErrEmpty("ErrEmpty", "非空校验"),
    ErrLength("ErrLength", "长度校验"),
    ErrNumber("ErrNumber", "数字校验"),
    ErrMail("ErrMail", "邮箱校验"),
    ErrPhone("ErrPhone", "手机号校验"),
    ErrReg("ErrReg", "自定义校验"),
    ;
    private String type;
    private String text;

    ELcpFormControlCheckEnum(String type, String text) {
        this.type = type;
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public String getText() {
        return text;
    }
}
