package com.imyuanma.qingyun.lowcode.model.enums;

/**
 * 查询条件类型
 *
 * @author wangjy
 * @date 2022/03/20 00:23:59
 */
public enum ELcpQueryConditionTypeEnum {
    NO(1, "否")
    , EQ(2, "=")
    , LIKE(3, "like")
    , LEFT_LIKE(4, "left like")
    , RIGHT_LIKE(5, "right like")
    , GT(6, ">")
    , LT(7, "<")
    , GT_EQ(8, ">=")
    , LT_EQ(9, "<=")
    , BETWEEN_AND(10, "between and")
    ;
    private Integer type;
    private String text;

    ELcpQueryConditionTypeEnum(Integer type, String text) {
        this.type = type;
        this.text = text;
    }

    public Integer getType() {
        return type;
    }

    public String getText() {
        return text;
    }
}
