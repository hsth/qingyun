package com.imyuanma.qingyun.lowcode.service;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.lowcode.model.LcpMetaComponentAttr;

import java.util.List;

/**
 * 组件属性配置项服务
 *
 * @author YuanMaKeJi
 * @date 2022-12-03 21:45:01
 */
public interface ILcpMetaComponentAttrService {

    /**
     * 列表查询
     *
     * @param lcpMetaComponentAttr 查询条件
     * @return
     */
    List<LcpMetaComponentAttr> getList(LcpMetaComponentAttr lcpMetaComponentAttr);

    /**
     * 根据组件id查询属性配置
     *
     * @param componentId
     * @return
     */
    List<LcpMetaComponentAttr> getListByComponentId(Long componentId);

    /**
     * 分页查询
     *
     * @param lcpMetaComponentAttr 查询条件
     * @param pageQuery            分页参数
     * @return
     */
    List<LcpMetaComponentAttr> getPage(LcpMetaComponentAttr lcpMetaComponentAttr, PageQuery pageQuery);

    /**
     * 统计数量
     *
     * @param lcpMetaComponentAttr 查询条件
     * @return
     */
    int count(LcpMetaComponentAttr lcpMetaComponentAttr);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    LcpMetaComponentAttr get(Long id);

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    List<LcpMetaComponentAttr> getListByIds(List<Long> list);

    /**
     * 插入
     *
     * @param lcpMetaComponentAttr 参数
     * @return
     */
    int insert(LcpMetaComponentAttr lcpMetaComponentAttr);

    /**
     * 选择性插入
     *
     * @param lcpMetaComponentAttr 参数
     * @return
     */
    int insertSelective(LcpMetaComponentAttr lcpMetaComponentAttr);

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    int batchInsert(List<LcpMetaComponentAttr> list);

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    int batchInsertSelective(List<LcpMetaComponentAttr> list);

    /**
     * 修改
     *
     * @param lcpMetaComponentAttr 参数
     * @return
     */
    int update(LcpMetaComponentAttr lcpMetaComponentAttr);

    /**
     * 选择性修改
     *
     * @param lcpMetaComponentAttr 参数
     * @return
     */
    int updateSelective(LcpMetaComponentAttr lcpMetaComponentAttr);

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    int delete(Long id);

    /**
     * 根据组件id删除
     * @param componentId
     * @return
     */
    int deleteByComponentId(Long componentId);

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    int batchDelete(List<Long> list);

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param lcpMetaComponentAttr 参数
     * @return
     */
    int deleteByCondition(LcpMetaComponentAttr lcpMetaComponentAttr);

}
