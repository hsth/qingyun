package com.imyuanma.qingyun.lowcode.controller;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.model.request.WebRequest;
import com.imyuanma.qingyun.common.model.response.Page;
import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.common.util.StringUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.lowcode.model.bo.GenerateMainBO;
import com.imyuanma.qingyun.lowcode.service.ILcpCodeOnlineService;
import com.imyuanma.qingyun.lowcode.service.ILcpGenerateService;
import com.imyuanma.qingyun.lowcode.util.GenerateSerializeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 代码生成在线服务
 *
 * @author wangjy
 * @date 2022/06/27 22:40:29
 */
@RestController
@RequestMapping(value = "/lowcode/online")
public class LcpCodeOnlineController {

    @Autowired
    private ILcpGenerateService generateService;
    @Autowired
    private ILcpCodeOnlineService codeOnlineService;

    /**
     * 在线视图页
     *
     * @param id       代码生成配置id
     * @param response http响应
     */
    @Trace("在线视图页")
    @GetMapping(value = "/html/view/{id}")
    public void view(@PathVariable("id") Long id, HttpServletResponse response) {
        AssertUtil.notNull(id);
        // 代码生成配置详情
        GenerateMainBO generateMainBO = generateService.getGenerateDetail(id);
        AssertUtil.notNull(generateMainBO);
        // 代码模板参数
        Map<String, Object> map = GenerateSerializeUtil.buildCodeTemplateParam(generateMainBO);
        // 生成html写入输出流
        GenerateSerializeUtil.generateHtmlResponse(GenerateSerializeUtil.CODE_TEMPLATE_DIR_PATH, "html_list.ftl", map, response);
    }

    /**
     * 在线接口-分页查询
     *
     * @param id         代码生成配置id
     * @param webRequest 参数
     * @return
     */
    @Trace("在线接口-分页查询")
    @PostMapping(value = "/api/getPage/{id}")
    public Page<List<Map<String, Object>>> getPage(@PathVariable("id") Long id, @RequestBody WebRequest<Map<String, Object>> webRequest) {
        AssertUtil.notNull(id);
        PageQuery pageQuery = webRequest.buildPageQuery();
        List<Map<String, Object>> list = codeOnlineService.getPage(id, webRequest.getBody(), pageQuery);
        return Page.success(list, pageQuery);
    }

    @Trace("在线接口-列表查询")
    @PostMapping(value = "/api/getList/{id}")
    public Result<List<Map<String, Object>>> getList(@PathVariable("id") Long id, @RequestBody WebRequest<Map<String, Object>> webRequest) {
        AssertUtil.notNull(id);
        List<Map<String, Object>> list = codeOnlineService.getList(id, webRequest.getBody());
        return Result.success(list);
    }

    /**
     * 在线接口-单查询
     *
     * @param id         代码生成配置id
     * @param webRequest 参数
     * @return
     */
    @Trace("在线接口-详情查询")
    @PostMapping(value = "/api/get/{id}")
    public Result<Map<String, Object>> get(@PathVariable("id") Long id, @RequestBody WebRequest<Object> webRequest) {
        AssertUtil.notNull(id);
        AssertUtil.notNull(webRequest.getBody());
        return Result.success(codeOnlineService.get(id, webRequest.getBody()));
    }

    /**
     * 在线接口-插入
     *
     * @param id         代码生成配置id
     * @param webRequest 参数
     * @return
     */
    @Trace("在线接口-新增")
    @PostMapping(value = "/api/insert/{id}")
    public Result insert(@PathVariable("id") Long id, @RequestBody WebRequest<Map<String, Object>> webRequest) {
        AssertUtil.notNull(id);
        AssertUtil.notNull(webRequest.getBody());
        codeOnlineService.insertSelective(id, webRequest.getBody());
        return Result.success();
    }

    /**
     * 在线接口-更新
     *
     * @param id         代码生成配置id
     * @param webRequest 参数
     * @return
     */
    @Trace("在线接口-更新")
    @PostMapping(value = "/api/update/{id}")
    public Result update(@PathVariable("id") Long id, @RequestBody WebRequest<Map<String, Object>> webRequest) {
        AssertUtil.notNull(id);
        AssertUtil.notNull(webRequest.getBody());
        codeOnlineService.updateSelective(id, webRequest.getBody());
        return Result.success();
    }

    /**
     * 在线接口-删除
     *
     * @param id         代码生成配置id
     * @param webRequest 参数
     * @return
     */
    @Trace("在线接口-删除")
    @PostMapping(value = "/api/delete/{id}")
    public Result delete(@PathVariable("id") Long id, @RequestBody WebRequest<String> webRequest) {
        AssertUtil.notNull(id);
        AssertUtil.notBlank(webRequest.getBody());
        String ids = webRequest.getBody();
        if (ids.contains(",")) {
            codeOnlineService.batchDelete(id, Arrays.stream(ids.split(",")).filter(StringUtil::isNotBlank).collect(Collectors.toList()));
        } else {
            codeOnlineService.delete(id, ids);
        }
        return Result.success();
    }
}
