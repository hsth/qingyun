package com.imyuanma.qingyun.lowcode.model.bo.ui;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * 页面元素(组件)模型
 *
 * @author wangjy
 * @date 2022/08/19 23:07:13
 */
@Data
public class LcpPageElementBO {
    /**
     * 元素id
     */
    private String id;
    /**
     * 元素类型
     */
    private String type;
    /**
     * 二级类型
     */
    private String subType;
    /**
     * 样式表集合
     */
    private List<String> classList;
    /**
     * 元素属性
     */
    private List<LcpPageElementAttrBO> attrList;
    /**
     * 元素样式
     */
    private List<LcpPageElementStyleBO> styleList;
    /**
     * 元素事件
     */
    private List<LcpPageElementEventBO> eventList;
    /**
     * 子元素集合
     */
    private List<LcpPageElementBO> childrenList;
}
