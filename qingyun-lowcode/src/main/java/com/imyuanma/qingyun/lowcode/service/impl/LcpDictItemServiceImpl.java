package com.imyuanma.qingyun.lowcode.service.impl;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.lowcode.dao.ILcpDictItemDao;
import com.imyuanma.qingyun.lowcode.model.LcpDictItem;
import com.imyuanma.qingyun.lowcode.service.ILcpDictItemService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 字典数据项服务
 *
 * @author YuanMaKeJi
 * @date 2022-12-04 23:44:03
 */
@Slf4j
@Service
public class LcpDictItemServiceImpl implements ILcpDictItemService {

    /**
     * 字典数据项dao
     */
    @Autowired
    private ILcpDictItemDao lcpDictItemDao;

    /**
     * 列表查询
     *
     * @param lcpDictItem 查询条件
     * @return
     */
    @Trace("查询字典数据项列表")
    @Override
    public List<LcpDictItem> getList(LcpDictItem lcpDictItem) {
        return lcpDictItemDao.getList(lcpDictItem);
    }

    /**
     * 获取字典下的所有数据
     *
     * @param dictCode
     * @return
     */
    @Trace("获取字典下的所有数据")
    @Override
    public List<LcpDictItem> getListByDictCode(String dictCode) {
        AssertUtil.notBlank(dictCode);
        LcpDictItem lcpDictItem = new LcpDictItem();
        lcpDictItem.setDictCode(dictCode);
        return lcpDictItemDao.getList(lcpDictItem);
    }

    /**
     * 分页查询
     *
     * @param lcpDictItem 查询条件
     * @param pageQuery 分页参数
     * @return
     */
    @Trace("分页查询字典数据项")
    @Override
    public List<LcpDictItem> getPage(LcpDictItem lcpDictItem, PageQuery pageQuery) {
        return lcpDictItemDao.getList(lcpDictItem, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param lcpDictItem 查询条件
     * @return
     */
    @Trace("统计字典数据项数量")
    @Override
    public int count(LcpDictItem lcpDictItem) {
        return lcpDictItemDao.count(lcpDictItem);
    }

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    @Trace("根据主键查询字典数据项")
    @Override
    public LcpDictItem get(Long id) {
        return lcpDictItemDao.get(id);
    }

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    @Trace("根据主键批量查询字典数据项")
    @Override
    public List<LcpDictItem> getListByIds(List<Long> list) {
        return lcpDictItemDao.getListByIds(list);
    }

    /**
     * 插入
     *
     * @param lcpDictItem 参数
     * @return
     */
    @Trace("插入字典数据项")
    @Override
    public int insert(LcpDictItem lcpDictItem) {
        return lcpDictItemDao.insert(lcpDictItem);
    }

    /**
     * 选择性插入
     *
     * @param lcpDictItem 参数
     * @return
     */
    @Trace("选择性插入字典数据项")
    @Override
    public int insertSelective(LcpDictItem lcpDictItem) {
        return lcpDictItemDao.insertSelective(lcpDictItem);
    }

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量插入字典数据项")
    @Override
    public int batchInsert(List<LcpDictItem> list) {
        return lcpDictItemDao.batchInsert(list);
    }

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量选择性插入字典数据项")
    @Override
    public int batchInsertSelective(List<LcpDictItem> list) {
        return lcpDictItemDao.batchInsertSelective(list);
    }

    /**
     * 修改
     *
     * @param lcpDictItem 参数
     * @return
     */
    @Trace("修改字典数据项")
    @Override
    public int update(LcpDictItem lcpDictItem) {
        return lcpDictItemDao.update(lcpDictItem);
    }

    /**
     * 选择性修改
     *
     * @param lcpDictItem 参数
     * @return
     */
    @Trace("选择性修改字典数据项")
    @Override
    public int updateSelective(LcpDictItem lcpDictItem) {
        return lcpDictItemDao.updateSelective(lcpDictItem);
    }

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    @Trace("删除字典数据项")
    @Override
    public int delete(Long id) {
        return lcpDictItemDao.delete(id);
    }

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    @Trace("批量删除字典数据项")
    @Override
    public int batchDelete(List<Long> list) {
        return lcpDictItemDao.batchDelete(list);
    }

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param lcpDictItem 参数
     * @return
     */
    @Trace("根据条件删除字典数据项")
    @Override
    public int deleteByCondition(LcpDictItem lcpDictItem) {
        return lcpDictItemDao.deleteByCondition(lcpDictItem);
    }

}
