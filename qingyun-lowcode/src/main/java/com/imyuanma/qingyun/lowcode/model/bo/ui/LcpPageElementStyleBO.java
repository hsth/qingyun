package com.imyuanma.qingyun.lowcode.model.bo.ui;

import lombok.Data;

/**
 * 元素样式
 *
 * @author wangjy
 * @date 2022/09/03 11:40:32
 */
@Data
public class LcpPageElementStyleBO {
    /**
     * 属性名
     */
    private String name;
    /**
     * 属性值
     */
    private String value;
}
