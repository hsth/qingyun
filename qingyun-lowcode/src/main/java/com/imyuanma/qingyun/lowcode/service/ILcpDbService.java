package com.imyuanma.qingyun.lowcode.service;

import com.imyuanma.qingyun.lowcode.model.SqlExecuteCmd;
import com.imyuanma.qingyun.lowcode.model.data.LcpDbColumnDO;
import com.imyuanma.qingyun.lowcode.model.data.LcpDbTableDO;
import com.imyuanma.qingyun.lowcode.model.vo.LcpDbColumnVO;
import com.imyuanma.qingyun.lowcode.model.vo.LcpDbTableDetailVO;

import java.util.List;
import java.util.Map;

/**
 * 数据库操作服务
 *
 * @author wangjy
 * @date 2022/05/04 15:30:33
 */
public interface ILcpDbService {
    /**
     * 查询数据库所有表
     *
     * @return
     */
    List<LcpDbTableDO> getTableList(LcpDbTableDO lcpDbTableDO);

    /**
     * 查询数据库所有表
     *
     * @param dataSourceCode 数据源code
     * @param lcpDbTableDO   查询条件
     * @return
     */
    List<LcpDbTableDO> getTableListDynamic(String dataSourceCode, LcpDbTableDO lcpDbTableDO);

    /**
     * 根据表名查询表信息
     *
     * @param tableName 表名
     * @return
     */
    LcpDbTableDO getTableByTableName(String tableName);

    /**
     * 根据表名查询表信息
     *
     * @param dataSourceCode
     * @param tableName
     * @return
     */
    LcpDbTableDO getTableByTableNameDynamic(String dataSourceCode, String tableName);

    /**
     * 根据表名批量查询表信息
     *
     * @param tableNameList 表名集合
     * @return
     */
    List<LcpDbTableDO> getTableListByTableName(List<String> tableNameList);

    /**
     * 根据表名批量查询表信息
     *
     * @param dataSourceCode
     * @param tableNameList
     * @return
     */
    List<LcpDbTableDO> getTableListByTableNameDynamic(String dataSourceCode, List<String> tableNameList);

    /**
     * 根据表名查询字段信息
     *
     * @param tableName 表名
     * @return
     */
    List<LcpDbColumnDO> getColumnListByTableName(String tableName);

    /**
     * 查询表字段信息
     *
     * @param dataSourceCode
     * @param tableName
     * @return
     */
    List<LcpDbColumnDO> getColumnListByTableNameDynamic(String dataSourceCode, String tableName);

    /**
     * 修改字段
     *
     * @param columnVO
     */
    void modifyDbColumn(LcpDbColumnVO columnVO);

    /**
     * 修改字段
     *
     * @param dataSourceCode
     * @param columnVO
     */
    void modifyDbColumnDynamic(String dataSourceCode, LcpDbColumnVO columnVO);

    /**
     * 新增字段
     *
     * @param columnVO
     */
    void addDbColumn(LcpDbColumnVO columnVO);

    /**
     * 新增字段
     *
     * @param dataSourceCode
     * @param columnVO
     */
    void addDbColumnDynamic(String dataSourceCode, LcpDbColumnVO columnVO);

    /**
     * 创建数据库表
     *
     * @param tableDetailVO
     */
    void createTable(LcpDbTableDetailVO tableDetailVO);

    /**
     * 创建表
     *
     * @param dataSourceCode
     * @param tableDetailVO
     */
    void createTableDynamic(String dataSourceCode, LcpDbTableDetailVO tableDetailVO);

    /**
     * 执行查询sql
     *
     * @param sqlExecuteCmd
     * @return
     */
    List<Map<String, Object>> executeSearchSql(SqlExecuteCmd sqlExecuteCmd);

    /**
     * 执行查询sql
     *
     * @param dataSourceCode
     * @param sqlExecuteCmd
     * @return
     */
    List<Map<String, Object>> executeSearchSqlDynamic(String dataSourceCode, SqlExecuteCmd sqlExecuteCmd);
}
