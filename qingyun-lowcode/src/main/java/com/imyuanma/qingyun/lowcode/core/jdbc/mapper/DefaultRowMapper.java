package com.imyuanma.qingyun.lowcode.core.jdbc.mapper;

import org.springframework.jdbc.core.RowMapper;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * 默认的行转换器
 *
 * @author wangjy
 * @date 2023/04/21 22:29:48
 */
public class DefaultRowMapper<T> implements RowMapper<T> {
    /**
     * 转换的目标类型
     */
    private Class<T> targetType;

    public DefaultRowMapper(Class<T> targetType) {
        this.targetType = targetType;
    }

    @Override
    public T mapRow(ResultSet rs, int rowNum) throws SQLException {
        T target;
        try {
            target = targetType.newInstance();
            ResultSetMetaData rsMetaData = rs.getMetaData();
            // 结果列元信息
            Map<String, ResultSetColumnDesc> columnMap = new HashMap<>();
            for (int i = 0; i < rsMetaData.getColumnCount(); i++) {
                columnMap.put(rsMetaData.getColumnLabel(i + 1), new ResultSetColumnDesc(rsMetaData, i + 1));
            }
            // bean转换
            for (Field field : targetType.getDeclaredFields()) {
                // 判断返回列中包含字段才处理, 否则会跑出SQLException找不到列
                if (columnMap.containsKey(field.getName())) {
                    Object value = rs.getObject(field.getName());
                    Object typeValue = JdbcTypeToJavaTypeUtil.convertJdbcValueToJavaType(value, field.getType());
                    field.setAccessible(true);
                    field.set(target, typeValue);
                }
            }
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        return target;
    }
}
