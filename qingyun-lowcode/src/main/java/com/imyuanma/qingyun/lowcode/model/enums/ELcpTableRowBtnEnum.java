package com.imyuanma.qingyun.lowcode.model.enums;

/**
 * 表格行按钮枚举
 *
 * @author wangjy
 * @date 2022/07/02 23:52:17
 */
public enum ELcpTableRowBtnEnum {
    EDIT("tag_view_tr_edit", "编辑"),
    DELETE("tag_view_tr_del", "删除"),
    ;
    private String type;
    private String text;

    ELcpTableRowBtnEnum(String type, String text) {
        this.type = type;
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public String getText() {
        return text;
    }
}
