package com.imyuanma.qingyun.lowcode.model.enums;

/**
 * 表格列特殊类型
 *
 * @author wangjy
 * @date 2022/03/20 17:04:49
 */
public enum ELcpTableColumnTypeEnum {
    NONE("none", "无")
    , CHECKBOX("checkbox", "多选框")
    , RADIO("radio", "单选框")
    , INDEX("index", "序号")
    ;
    private String type;
    private String text;

    ELcpTableColumnTypeEnum(String type, String text) {
        this.type = type;
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public String getText() {
        return text;
    }
}
