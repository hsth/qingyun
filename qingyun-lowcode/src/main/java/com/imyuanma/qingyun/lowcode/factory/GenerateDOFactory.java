package com.imyuanma.qingyun.lowcode.factory;

import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.common.util.JsonUtil;
import com.imyuanma.qingyun.common.util.StringUtil;
import com.imyuanma.qingyun.interfaces.common.model.enums.ETrashFlagEnum;
import com.imyuanma.qingyun.lowcode.model.bo.GenerateFieldBO;
import com.imyuanma.qingyun.lowcode.model.bo.GenerateMainBO;
import com.imyuanma.qingyun.lowcode.model.bo.JsLinkBO;
import com.imyuanma.qingyun.lowcode.model.data.LcpGenerateFieldDO;
import com.imyuanma.qingyun.lowcode.model.data.LcpGenerateMainDO;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 代码生成数据模型工厂
 *
 * @author wangjy
 * @date 2022/05/05 23:42:47
 */
public class GenerateDOFactory {

    public static LcpGenerateMainDO buildGenerateMainDO(GenerateMainBO generateMainBO) {
        LcpGenerateMainDO lcpGenerateMainDO = new LcpGenerateMainDO();
        lcpGenerateMainDO.setId(generateMainBO.getId());
        lcpGenerateMainDO.setName(generateMainBO.getName());
        lcpGenerateMainDO.setTableName(generateMainBO.getTableName());
        lcpGenerateMainDO.setClassName(generateMainBO.getClassName());
        lcpGenerateMainDO.setPackagePath(generateMainBO.getPackagePath());
        lcpGenerateMainDO.setModuleName(generateMainBO.getModuleName());
        lcpGenerateMainDO.setSavePath(generateMainBO.getSavePath());
        lcpGenerateMainDO.setAuthor(generateMainBO.getAuthor());
        lcpGenerateMainDO.setFormLabelPosition(generateMainBO.getFormLabelPosition());
        lcpGenerateMainDO.setFormLabelWidth(generateMainBO.getFormLabelWidth());
        lcpGenerateMainDO.setGlobalCss(generateMainBO.getGlobalCss());
        lcpGenerateMainDO.setGlobalJs(generateMainBO.getGlobalJs());
        // 转json
        lcpGenerateMainDO.setExtInfo(JsonUtil.toJson(generateMainBO.getExtInfo()));
        lcpGenerateMainDO.setFormPageConfig(JsonUtil.toJson(generateMainBO.getFormPageConfig()));
        lcpGenerateMainDO.setCreateTime(new Date());
        lcpGenerateMainDO.setUpdateTime(new Date());
        lcpGenerateMainDO.setCreateUserId(1L);
        lcpGenerateMainDO.setUpdateUserId(1L);
        lcpGenerateMainDO.setDataSequence(1L);
        lcpGenerateMainDO.setDataVersion(1L);
        lcpGenerateMainDO.setTrashFlag(ETrashFlagEnum.VALID.getType());
        return lcpGenerateMainDO;
    }

    public static List<LcpGenerateFieldDO> buildGenerateFieldDOList(List<GenerateFieldBO> fieldBOList) {
        return fieldBOList.stream().map(GenerateDOFactory::buildGenerateFieldDO).collect(Collectors.toList());
    }

    public static LcpGenerateFieldDO buildGenerateFieldDO(GenerateFieldBO generateFieldBO) {
        LcpGenerateFieldDO lcpGenerateFieldDO = new LcpGenerateFieldDO();
        lcpGenerateFieldDO.setId(generateFieldBO.getId());
        lcpGenerateFieldDO.setGenerateId(generateFieldBO.getGenerateId());
        lcpGenerateFieldDO.setFieldDbName(generateFieldBO.getFieldDbName());
        lcpGenerateFieldDO.setFieldDbRemark(generateFieldBO.getFieldDbRemark());
        lcpGenerateFieldDO.setFieldDbType(generateFieldBO.getFieldDbType());
        lcpGenerateFieldDO.setFieldDbLength(generateFieldBO.getFieldDbLength());
        lcpGenerateFieldDO.setFieldDbNotNull(generateFieldBO.getFieldDbNotNull());
        lcpGenerateFieldDO.setFieldDbKey(generateFieldBO.getFieldDbKey());
        lcpGenerateFieldDO.setFieldJavaName(generateFieldBO.getFieldJavaName());
        lcpGenerateFieldDO.setFieldJavaType(generateFieldBO.getFieldJavaType());
        lcpGenerateFieldDO.setSupportImport(generateFieldBO.getSupportImport());
        lcpGenerateFieldDO.setSupportExport(generateFieldBO.getSupportExport());
        lcpGenerateFieldDO.setListColumnShow(generateFieldBO.getListColumnShow());
        lcpGenerateFieldDO.setListColumnOrder(generateFieldBO.getListColumnOrder());
        lcpGenerateFieldDO.setListColumnFixed(generateFieldBO.getListColumnFixed());
        lcpGenerateFieldDO.setListColumnWidth(generateFieldBO.getListColumnWidth());
        lcpGenerateFieldDO.setListColumnSort(generateFieldBO.getListColumnSort());
        lcpGenerateFieldDO.setListHeaderTitle(generateFieldBO.getListHeaderTitle());
        lcpGenerateFieldDO.setListHeaderAlign(generateFieldBO.getListHeaderAlign());
        lcpGenerateFieldDO.setListHeaderTips(generateFieldBO.getListHeaderTips());
        lcpGenerateFieldDO.setListValueAlign(generateFieldBO.getListValueAlign());
        lcpGenerateFieldDO.setListValueFormat(generateFieldBO.getListValueFormat());
        lcpGenerateFieldDO.setListQueryCondition(generateFieldBO.getListQueryCondition());
        lcpGenerateFieldDO.setFormColumnShow(generateFieldBO.getFormColumnShow());
        // 转json
        lcpGenerateFieldDO.setFormColumnShowConfig(JsonUtil.toJson(generateFieldBO.getFormColumnShowConfig()));
        lcpGenerateFieldDO.setFormColumnOrder(generateFieldBO.getFormColumnOrder());
        lcpGenerateFieldDO.setFormColumnGridWidth(generateFieldBO.getFormColumnGridWidth());
        lcpGenerateFieldDO.setFormControlType(generateFieldBO.getFormControlType());
        lcpGenerateFieldDO.setFormControlDefault(generateFieldBO.getFormControlDefault());
        lcpGenerateFieldDO.setFormControlTitle(generateFieldBO.getFormControlTitle());
        lcpGenerateFieldDO.setFormControlPlaceholder(generateFieldBO.getFormControlPlaceholder());
        lcpGenerateFieldDO.setFormControlTips(generateFieldBO.getFormControlTips());
        // 转json
        lcpGenerateFieldDO.setFormControlCheckRule(JsonUtil.toJson(generateFieldBO.getFormControlCheckRule()));
        lcpGenerateFieldDO.setFormControlDataSource(JsonUtil.toJson(generateFieldBO.getFormControlDataSource()));
        lcpGenerateFieldDO.setFormControlConfig(JsonUtil.toJson(generateFieldBO.getFormControlConfig()));
        lcpGenerateFieldDO.setCreateTime(new Date());
        lcpGenerateFieldDO.setUpdateTime(new Date());
        lcpGenerateFieldDO.setCreateUserId(1L);
        lcpGenerateFieldDO.setUpdateUserId(1L);
        lcpGenerateFieldDO.setDataSequence(1L);
        lcpGenerateFieldDO.setDataVersion(1L);
        lcpGenerateFieldDO.setTrashFlag(ETrashFlagEnum.VALID.getType());
        return lcpGenerateFieldDO;
    }
}
