package com.imyuanma.qingyun.lowcode.core.jdbc.dialect;

import com.imyuanma.qingyun.common.db.dal.EDBType;
import com.imyuanma.qingyun.common.util.StringUtil;
import com.imyuanma.qingyun.lowcode.model.data.LcpDbColumnDO;
import com.imyuanma.qingyun.lowcode.model.data.LcpDbTableDO;
import lombok.extern.slf4j.Slf4j;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * mysql
 *
 * @author wangjy
 * @date 2023/04/17 23:56:41
 */
@Slf4j
public class MysqlJdbcExecutor extends AbstractJdbcDialectExecutor {

    public MysqlJdbcExecutor(EDBType dbType, DataSource dataSource) {
        super(dbType, dataSource);
    }

    /**
     * 查询所有表
     *
     * @param lcpDbTableDO
     * @return
     */
    @Override
    public List<LcpDbTableDO> getTableList(LcpDbTableDO lcpDbTableDO) {
        StringBuilder sql = new StringBuilder("SELECT table_name AS \"tableName\", table_comment AS \"tableComment\", table_rows AS \"tableRows\", data_length AS \"dataLength\" FROM information_schema.TABLES WHERE table_schema IN (SELECT DATABASE()) ");
        List<Object> param = new ArrayList<>();
        if (lcpDbTableDO != null) {
            if (StringUtil.isNotBlank(lcpDbTableDO.getTableName())) {
                sql.append(" and table_name LIKE CONCAT('%', CONCAT(?, '%'))");
                param.add(lcpDbTableDO.getTableName());
            }
            if (StringUtil.isNotBlank(lcpDbTableDO.getTableComment())) {
                sql.append(" and table_comment LIKE CONCAT('%', CONCAT(?, '%'))");
                param.add(lcpDbTableDO.getTableComment());
            }
        }
        return this.selectList(sql.toString(), LcpDbTableDO.class, param.toArray());
    }

    /**
     * 根据表名查询表信息
     *
     * @param tableName 表名
     * @return
     */
    @Override
    public LcpDbTableDO getTableByTableName(String tableName) {
        String sql = "SELECT table_name AS \"tableName\", table_comment AS \"tableComment\", table_rows AS \"tableRows\", data_length AS \"dataLength\" FROM information_schema.TABLES WHERE table_schema IN (SELECT DATABASE()) and table_name = ?";
        return this.selectOne(sql, LcpDbTableDO.class, tableName);
    }

    /**
     * 根据表名批量查询表信息
     *
     * @param tableNameList
     * @return
     */
    @Override
    public List<LcpDbTableDO> getTableListByTableName(List<String> tableNameList) {
        StringBuilder sql = new StringBuilder("SELECT table_name AS \"tableName\", table_comment AS \"tableComment\", table_rows AS \"tableRows\", data_length AS \"dataLength\" FROM information_schema.TABLES WHERE table_schema IN (SELECT DATABASE())");
        sql.append("and table_name in (");
        for (int i = 0; i < tableNameList.size(); i++) {
            if (i > 0) {
                sql.append(",");
            }
            sql.append("?");
        }
        sql.append(")");
        return this.selectList(sql.toString(), LcpDbTableDO.class, tableNameList.toArray());
    }

    /**
     * 查询表字段信息
     *
     * @param tableName
     * @return
     */
    @Override
    public List<LcpDbColumnDO> getColumnListByTableName(String tableName) {
        StringBuilder sql = new StringBuilder("SELECT table_name AS \"tableName\" , column_name AS \"columnName\" , data_type AS \"dataType\" , character_maximum_length AS \"length\" , is_nullable = 'NO' AS \"notNull\" , column_comment AS \"remark\" , column_key AS \"columnKey\" , ordinal_position AS \"position\" , column_default AS \"columnDefault\" , extra = 'auto_increment' AS \"autoIncrement\"");
        sql.append(" FROM information_schema.COLUMNS");
        sql.append(" WHERE TABLE_NAME = ?");
        sql.append(" AND table_schema IN (SELECT DATABASE())");
        sql.append(" ORDER BY ordinal_position");
        return this.selectList(sql.toString(), LcpDbColumnDO.class, tableName);
    }

}
