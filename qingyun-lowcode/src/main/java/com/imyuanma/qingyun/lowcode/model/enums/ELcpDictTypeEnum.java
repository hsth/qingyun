package com.imyuanma.qingyun.lowcode.model.enums;

/**
 * 类型枚举
 *
 * @author YuanMaKeJi
 * @date 2022-12-04 23:42:56
 */
public enum ELcpDictTypeEnum {
    CODE_list("list", "普通字典"),
    CODE_tree("tree", "树结构字典"),
    ;

    /**
     * code
     */
    private String code;
    /**
     * 文案
     */
    private String text;

    ELcpDictTypeEnum(String code, String text) {
        this.code = code;
        this.text = text;
    }

    /**
     * 根据code获取枚举
     *
     * @param code code值
     */
    public static ELcpDictTypeEnum getByCode(String code) {
        for (ELcpDictTypeEnum e : ELcpDictTypeEnum.values()) {
            if (e.code != null && e.code.equals(code)) {
                return e;
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
