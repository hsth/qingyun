package com.imyuanma.qingyun.lowcode.model.vo;

import com.imyuanma.qingyun.lowcode.model.data.LcpDbColumnDO;
import lombok.Data;

/**
 * 数据库字段
 *
 * @author wangjy
 * @date 2022/11/15 23:46:07
 */
@Data
public class LcpDbColumnVO {
    /**
     * 表名
     */
    private String tableName;
    /**
     * 字段名
     */
    private String columnName;
    /**
     * 字段数据类型
     */
    private String dataType;
    /**
     * 字段长度
     */
    private Integer length;
    /**
     * 非空
     */
    private Integer notNull;
    /**
     * 备注,字段说明
     */
    private String remark;
    /**
     * 主键标识
     */
    private Boolean primaryKeyFlag;
    /**
     * 自增
     */
    private Integer autoIncrement;
    /**
     * 字段顺序
     */
    private Integer position;
    /**
     * 字段默认值
     */
    private String columnDefault;
    /**
     * 顺序在哪个字段后面
     * 新增或修改时用来调整字段顺序
     */
    private String afterColumn;

    public LcpDbColumnVO() {
    }

    public LcpDbColumnVO(String columnName) {
        this.columnName = columnName;
    }

    public LcpDbColumnVO(LcpDbColumnDO columnDO) {
        this.tableName = columnDO.getTableName();
        this.columnName = columnDO.getColumnName();
        this.dataType = columnDO.getDataType();
        this.length = columnDO.getLength();
        this.notNull = columnDO.getNotNull();
        this.remark = columnDO.getRemark();
        this.primaryKeyFlag = columnDO.isPrimaryKey();
        this.autoIncrement = columnDO.getAutoIncrement();
        this.position = columnDO.getPosition();
        this.columnDefault = columnDO.getColumnDefault();
    }
}
