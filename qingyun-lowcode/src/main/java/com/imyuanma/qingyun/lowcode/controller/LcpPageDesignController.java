package com.imyuanma.qingyun.lowcode.controller;

import com.imyuanma.qingyun.common.model.request.WebRequest;
import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.lowcode.model.LcpDictItem;
import com.imyuanma.qingyun.lowcode.model.bo.OptionItem;
import com.imyuanma.qingyun.lowcode.service.ILcpDictItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 低代码页面设计
 *
 * @author wangjy
 * @date 2024/08/03 16:08:09
 */
@RestController
@RequestMapping(value = "/lowcode/design")
public class LcpPageDesignController {

    /**
     * 字典数据项服务
     */
    @Autowired
    private ILcpDictItemService lcpDictItemService;

    @Trace("查询组件分类")
    @PostMapping("/dict/getComponentCategoryList")
    public Result<List<OptionItem>> getComponentCategoryList(@RequestBody WebRequest webRequest) {
        List<OptionItem> kvList = new ArrayList<>();
        List<LcpDictItem> list = lcpDictItemService.getListByDictCode("component_category");
        if (CollectionUtil.isNotEmpty(list)) {
            kvList = list.stream().map(item -> new OptionItem(item.getCode(), item.getName(), item.getStyle())).collect(Collectors.toList());
        }
        return Result.success(kvList);
    }
}
