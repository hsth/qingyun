package com.imyuanma.qingyun.lowcode.service.impl;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.lowcode.dao.ILcpDbDataSourceDao;
import com.imyuanma.qingyun.lowcode.model.LcpDbDataSource;
import com.imyuanma.qingyun.lowcode.service.ILcpDbDataSourceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * DB数据源服务
 *
 * @author YuanMaKeJi
 * @date 2023-04-22 11:18:10
 */
@Slf4j
@Service
public class LcpDbDataSourceServiceImpl implements ILcpDbDataSourceService {

    /**
     * DB数据源dao
     */
    @Autowired
    private ILcpDbDataSourceDao lcpDbDataSourceDao;

    /**
     * 列表查询
     *
     * @param lcpDbDataSource 查询条件
     * @return
     */
    @Trace("查询DB数据源列表")
    @Override
    public List<LcpDbDataSource> getList(LcpDbDataSource lcpDbDataSource) {
        return lcpDbDataSourceDao.getList(lcpDbDataSource);
    }

    /**
     * 分页查询
     *
     * @param lcpDbDataSource 查询条件
     * @param pageQuery 分页参数
     * @return
     */
    @Trace("分页查询DB数据源")
    @Override
    public List<LcpDbDataSource> getPage(LcpDbDataSource lcpDbDataSource, PageQuery pageQuery) {
        return lcpDbDataSourceDao.getList(lcpDbDataSource, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param lcpDbDataSource 查询条件
     * @return
     */
    @Trace("统计DB数据源数量")
    @Override
    public int count(LcpDbDataSource lcpDbDataSource) {
        return lcpDbDataSourceDao.count(lcpDbDataSource);
    }

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    @Trace("主键查询DB数据源")
    @Override
    public LcpDbDataSource get(Long id) {
        return lcpDbDataSourceDao.get(id);
    }

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    @Trace("主键批量查询DB数据源")
    @Override
    public List<LcpDbDataSource> getListByIds(List<Long> list) {
        return lcpDbDataSourceDao.getListByIds(list);
    }

    /**
     * 根据code查询
     *
     * @param code 数据源编码
     * @return
     */
    @Trace("根据code查询DB数据源")
    @Override
    public LcpDbDataSource getByCode(String code) {
        return lcpDbDataSourceDao.getByCode(code);
    }

    /**
     * 根据dbType查询
     *
     * @param dbType 数据库类型
     * @return
     */
    @Trace("根据dbType查询DB数据源")
    @Override
    public List<LcpDbDataSource> getListByDbType(String dbType) {
        return lcpDbDataSourceDao.getListByDbType(dbType);
    }

    /**
     * 插入
     *
     * @param lcpDbDataSource 参数
     * @return
     */
    @Trace("插入DB数据源")
    @Override
    public int insert(LcpDbDataSource lcpDbDataSource) {
        return lcpDbDataSourceDao.insert(lcpDbDataSource);
    }

    /**
     * 选择性插入
     *
     * @param lcpDbDataSource 参数
     * @return
     */
    @Trace("选择性插入DB数据源")
    @Override
    public int insertSelective(LcpDbDataSource lcpDbDataSource) {
        return lcpDbDataSourceDao.insertSelective(lcpDbDataSource);
    }

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量插入DB数据源")
    @Override
    public int batchInsert(List<LcpDbDataSource> list) {
        return lcpDbDataSourceDao.batchInsert(list);
    }

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量选择性插入DB数据源")
    @Override
    public int batchInsertSelective(List<LcpDbDataSource> list) {
        return lcpDbDataSourceDao.batchInsertSelective(list);
    }

    /**
     * 修改
     *
     * @param lcpDbDataSource 参数
     * @return
     */
    @Trace("修改DB数据源")
    @Override
    public int update(LcpDbDataSource lcpDbDataSource) {
        return lcpDbDataSourceDao.update(lcpDbDataSource);
    }

    /**
     * 选择性修改
     *
     * @param lcpDbDataSource 参数
     * @return
     */
    @Trace("选择性修改DB数据源")
    @Override
    public int updateSelective(LcpDbDataSource lcpDbDataSource) {
        return lcpDbDataSourceDao.updateSelective(lcpDbDataSource);
    }

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    @Trace("删除DB数据源")
    @Override
    public int delete(Long id) {
        return lcpDbDataSourceDao.delete(id);
    }

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    @Trace("批量删除DB数据源")
    @Override
    public int batchDelete(List<Long> list) {
        return lcpDbDataSourceDao.batchDelete(list);
    }

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param lcpDbDataSource 参数
     * @return
     */
    @Trace("根据条件删除DB数据源")
    @Override
    public int deleteByCondition(LcpDbDataSource lcpDbDataSource) {
        return lcpDbDataSourceDao.deleteByCondition(lcpDbDataSource);
    }

}
