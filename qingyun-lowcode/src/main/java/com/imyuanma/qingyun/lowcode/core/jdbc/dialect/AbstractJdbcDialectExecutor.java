package com.imyuanma.qingyun.lowcode.core.jdbc.dialect;

import com.imyuanma.qingyun.common.db.dal.EDBType;
import com.imyuanma.qingyun.common.db.dal.IPageSqlBuilder;
import com.imyuanma.qingyun.common.db.dal.PageSqlBuilderFactory;
import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.lowcode.model.data.LcpDbColumnDO;
import com.imyuanma.qingyun.lowcode.model.data.LcpDbTableDO;
import com.imyuanma.qingyun.lowcode.model.vo.LcpDbColumnVO;
import com.imyuanma.qingyun.lowcode.model.vo.LcpDbTableDetailVO;
import com.imyuanma.qingyun.lowcode.util.DdlFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.DataClassRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 方言执行抽象类
 *
 * @author wangjy
 * @date 2023/04/18 00:04:55
 */
@Slf4j
public abstract class AbstractJdbcDialectExecutor implements IJdbcDialectExecute {
    /**
     * 数据库类型
     */
    protected EDBType dbType;
    /**
     * jdbc模板
     */
    protected JdbcTemplate jdbcTemplate;

    public AbstractJdbcDialectExecutor(EDBType dbType, DataSource dataSource) {
        this.dbType = dbType;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    /**
     * 构造分页查询总数据量sql
     *
     * @param sql 普通查询sql
     * @return
     */
    protected String buildCountSql(String sql) {
        // 分页sql构造器
        IPageSqlBuilder pageSqlBuilder = PageSqlBuilderFactory.getBuilder(dbType);
        // 构造数据量查询sql
        return pageSqlBuilder.getCountSql(sql);
    }

    /**
     * 构造分页查询sql
     *
     * @param sql       普通查询sql
     * @param pageQuery 分页参数
     * @return
     */
    protected String buildPageSql(String sql, PageQuery pageQuery) {
        // 分页sql构造器
        IPageSqlBuilder pageSqlBuilder = PageSqlBuilderFactory.getBuilder(dbType);
        return pageSqlBuilder.getPageSql(sql, pageQuery.getPageNumber(), pageQuery.getPageSize());
    }

    /**
     * 查询所有表
     *
     * @param lcpDbTableDO
     * @return
     */
    @Override
    public List<LcpDbTableDO> getTableList(LcpDbTableDO lcpDbTableDO) {
        throw new UnsupportedOperationException();
    }

    /**
     * 根据表名查询表信息
     *
     * @param tableName 表名
     * @return
     */
    @Override
    public LcpDbTableDO getTableByTableName(String tableName) {
        throw new UnsupportedOperationException();
    }

    /**
     * 根据表名批量查询表信息
     *
     * @param tableNameList
     * @return
     */
    @Override
    public List<LcpDbTableDO> getTableListByTableName(List<String> tableNameList) {
        throw new UnsupportedOperationException();
    }

    /**
     * 查询表字段信息
     *
     * @param tableName
     * @return
     */
    @Override
    public List<LcpDbColumnDO> getColumnListByTableName(String tableName) {
        throw new UnsupportedOperationException();
    }

    /**
     * 修改字段
     *
     * @param columnVO
     */
    @Override
    public void modifyDbColumn(LcpDbColumnVO columnVO) {
        String sql = DdlFactory.buildModifyColumnSql(columnVO);
        log.info("[修改数据库字段] 待执行字段修改sql:{}", sql);
        this.executeDdl(sql);
    }

    /**
     * 新增字段
     *
     * @param columnVO
     */
    @Override
    public void addDbColumn(LcpDbColumnVO columnVO) {
        String sql = DdlFactory.buildAddColumnSql(columnVO);
        log.info("[新增数据库字段] 待执行字段新增sql:{}", sql);
        this.executeDdl(sql);
    }

    /**
     * 创建表
     *
     * @param tableDetailVO
     */
    @Override
    public void createTable(LcpDbTableDetailVO tableDetailVO) {
        String sql = DdlFactory.buildCreateTableSql(tableDetailVO);
        log.info("[创建数据库表] 待执行表创建sql:{}", sql);
        this.executeDdl(sql);
    }

    /**
     * 执行ddl
     *
     * @param sql
     */
    @Override
    public void executeDdl(String sql) {
        jdbcTemplate.execute(sql);
    }

    /**
     * 单查询
     *
     * @param sql    sql语句
     * @param params 参数
     * @return
     */
    @Override
    public Map<String, Object> selectOne(String sql, Object... params) {
        return jdbcTemplate.queryForMap(sql, params);
    }

    /**
     * 单查询
     *
     * @param sql         sql语句
     * @param elementType 返回值类型
     * @param params      参数
     * @return
     */
    @Override
    public <T> T selectOne(String sql, Class<T> elementType, Object... params) {
        if (elementType.isPrimitive() || elementType == Boolean.class
                || elementType == Character.class || Number.class.isAssignableFrom(elementType)) {
            return jdbcTemplate.queryForObject(sql, elementType, params);
        }
        return jdbcTemplate.queryForObject(sql, new DataClassRowMapper<>(elementType), params);
    }

    /**
     * 列表查询
     *
     * @param sql    查询sql
     * @param params 查询参数
     * @return
     */
    @Override
    public List<Map<String, Object>> selectList(String sql, Object... params) {
        return jdbcTemplate.queryForList(sql, params);
    }

    /**
     * 列表查询
     *
     * @param sql         查询sql
     * @param elementType 返回值类型
     * @param params      查询参数
     * @param <T>
     * @return
     */
    @Override
    public <T> List<T> selectList(String sql, Class<T> elementType, Object... params) {
        if (elementType.isPrimitive() || elementType == Boolean.class
                || elementType == Character.class || Number.class.isAssignableFrom(elementType)) {
            return jdbcTemplate.queryForList(sql, elementType, params);
        }
        return jdbcTemplate.query(sql, new DataClassRowMapper<>(elementType), params);
    }

    /**
     * 分页查询
     *
     * @param sql       查询sql
     * @param pageQuery 分页条件
     * @param params    查询参数
     * @return
     */
    @Override
    public List<Map<String, Object>> selectPage(String sql, PageQuery pageQuery, Object... params) {
        if (pageQuery.isAutoQueryTotal()) {
            // 数据量查询
            Integer total = this.selectOne(this.buildCountSql(sql), Integer.class, params);
            pageQuery.setTotal(total != null ? total : 0);
            int startIndex = (pageQuery.getPageNumber() - 1) * pageQuery.getPageSize();
            if (pageQuery.getTotal() > 0 && startIndex < pageQuery.getTotal()) {
                // 分页查询
                return this.selectList(this.buildPageSql(sql, pageQuery), params);
            }
            return new ArrayList<>();
        } else {
            return this.selectList(this.buildPageSql(sql, pageQuery), params);
        }
    }

    /**
     * 分页查询
     *
     * @param sql         查询sql
     * @param pageQuery   分页条件
     * @param elementType 返回值类型
     * @param params      查询参数
     * @param <T>
     * @return
     */
    @Override
    public <T> List<T> selectPage(String sql, PageQuery pageQuery, Class<T> elementType, Object... params) {
        if (pageQuery.isAutoQueryTotal()) {
            // 数据量查询
            Integer total = this.selectOne(this.buildCountSql(sql), Integer.class, params);
            pageQuery.setTotal(total != null ? total : 0);
            int startIndex = (pageQuery.getPageNumber() - 1) * pageQuery.getPageSize();
            if (pageQuery.getTotal() > 0 && startIndex < pageQuery.getTotal()) {
                // 分页查询
                return this.selectList(this.buildPageSql(sql, pageQuery), elementType, params);
            }
            return new ArrayList<>();
        } else {
            return this.selectList(this.buildPageSql(sql, pageQuery), elementType, params);
        }
    }

    /**
     * 插入
     *
     * @param sql    sql语句
     * @param params 参数
     * @return
     */
    @Override
    public int insert(String sql, Object... params) {
        return jdbcTemplate.update(sql, params);
    }

    /**
     * 修改
     *
     * @param sql    sql语句
     * @param params 参数
     * @return
     */
    @Override
    public int update(String sql, Object... params) {
        return jdbcTemplate.update(sql, params);
    }

    /**
     * 删除
     *
     * @param sql    sql语句
     * @param params 参数
     * @return
     */
    @Override
    public int delete(String sql, Object... params) {
        return jdbcTemplate.update(sql, params);
    }
}
