package com.imyuanma.qingyun.lowcode.service;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.lowcode.model.bo.GenerateMainBO;
import com.imyuanma.qingyun.lowcode.model.dto.LcpGenerateExportDTO;

import java.util.List;

/**
 * 代码生成服务
 *
 * @author wangjy
 * @date 2022/05/04 14:29:13
 */
public interface ILcpGenerateService {
    /**
     * 生成代码
     *
     * @param generateId     代码生成配置id
     * @param codeTemplateId 代码模板id
     */
    void generate(Long generateId, Long codeTemplateId);

    /**
     * 初始化代码生成配置
     *
     * @param tableName 表名
     * @return
     */
    GenerateMainBO initGenerateByTableName(String tableName);

    /**
     * 新增代码生成配置
     *
     * @param generateMainBO 代码生成配置
     */
    void addGenerateConfig(GenerateMainBO generateMainBO);

    /**
     * 更新代码生成配置
     *
     * @param generateMainBO 代码生成配置
     */
    void update(GenerateMainBO generateMainBO);

    /**
     * 查询代码生成列表
     *
     * @param generateMainBO 代码生成查询条件
     * @return
     */
    List<GenerateMainBO> getList(GenerateMainBO generateMainBO);

    /**
     * 分页查询
     *
     * @param generateMainBO 代码生成查询条件
     * @param pageQuery      分页参数
     * @return
     */
    List<GenerateMainBO> getPage(GenerateMainBO generateMainBO, PageQuery pageQuery);

    /**
     * 查询代码生成详情
     *
     * @param id 代码生成id
     * @return
     */
    GenerateMainBO getGenerateDetail(Long id);

    /**
     * 同步字段变更并返回最新详情
     *
     * @param id 代码生成id
     * @return
     */
    GenerateMainBO syncFieldChange(Long id);

    /**
     * 删除
     *
     * @param ids 代码生成id
     */
    void delete(Long[] ids);

    /**
     * 批量导入
     *
     * @param list
     */
    void batchImport(List<LcpGenerateExportDTO> list);
}
