package com.imyuanma.qingyun.lowcode.model;

import lombok.Data;
import java.util.Date;
import com.imyuanma.qingyun.interfaces.common.model.BaseDO;

/**
 * 组件属性配置项实体类
 *
 * @author YuanMaKeJi
 * @date 2022-12-03 21:45:01
 */
@Data
public class LcpMetaComponentAttr extends BaseDO {

    /**
     * 主键
     */
    private Long id;

    /**
     * 属性名,例如:span
     */
    private String attrName;

    /**
     * 组件ID
     */
    private Long componentId;

    /**
     * 属性说明
     */
    private String attrText;

    /**
     * 属性默认值
     */
    private String attrDefault;

    /**
     * 属性数据类型,多个逗号拼接,var,string,number,boolean,object,array
     */
    private String attrDataType;

    /**
     * 是否支持vue绑定属性,yes/no
     */
    private String supportVueBind;

    /**
     * 备注
     */
    private String remark;



}