package com.imyuanma.qingyun.lowcode.service.impl;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.lowcode.dao.ILcpMetaComponentDao;
import com.imyuanma.qingyun.lowcode.model.LcpMetaComponent;
import com.imyuanma.qingyun.lowcode.model.LcpMetaComponentAttr;
import com.imyuanma.qingyun.lowcode.service.ILcpMetaComponentAttrService;
import com.imyuanma.qingyun.lowcode.service.ILcpMetaComponentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 组件元数据服务
 *
 * @author YuanMaKeJi
 * @date 2022-12-03 21:44:59
 */
@Slf4j
@Service
@Transactional(rollbackFor = Throwable.class)
public class LcpMetaComponentServiceImpl implements ILcpMetaComponentService {

    /**
     * 组件元数据dao
     */
    @Autowired
    private ILcpMetaComponentDao lcpMetaComponentDao;
    /**
     * 组件属性元数据
     */
    @Autowired
    private ILcpMetaComponentAttrService lcpMetaComponentAttrService;

    /**
     * 列表查询
     *
     * @param lcpMetaComponent 查询条件
     * @return
     */
    @Trace("查询组件元数据列表")
    @Override
    public List<LcpMetaComponent> getList(LcpMetaComponent lcpMetaComponent) {
        return lcpMetaComponentDao.getList(lcpMetaComponent);
    }

    /**
     * 分页查询
     *
     * @param lcpMetaComponent 查询条件
     * @param pageQuery 分页参数
     * @return
     */
    @Trace("分页查询组件元数据")
    @Override
    public List<LcpMetaComponent> getPage(LcpMetaComponent lcpMetaComponent, PageQuery pageQuery) {
        return lcpMetaComponentDao.getList(lcpMetaComponent, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param lcpMetaComponent 查询条件
     * @return
     */
    @Trace("统计组件元数据数量")
    @Override
    public int count(LcpMetaComponent lcpMetaComponent) {
        return lcpMetaComponentDao.count(lcpMetaComponent);
    }

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    @Trace("根据主键查询组件元数据")
    @Override
    public LcpMetaComponent get(Long id) {
        return lcpMetaComponentDao.get(id);
    }

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    @Trace("根据主键批量查询组件元数据")
    @Override
    public List<LcpMetaComponent> getListByIds(List<Long> list) {
        return lcpMetaComponentDao.getListByIds(list);
    }

    /**
     * 插入
     *
     * @param lcpMetaComponent 参数
     * @return
     */
    @Trace("插入组件元数据")
    @Override
    public int insert(LcpMetaComponent lcpMetaComponent) {
        return lcpMetaComponentDao.insert(lcpMetaComponent);
    }

    /**
     * 选择性插入
     *
     * @param lcpMetaComponent 参数
     * @return
     */
    @Trace("选择性插入组件元数据")
    @Override
    public int insertSelective(LcpMetaComponent lcpMetaComponent) {
        // 插入
        int num = lcpMetaComponentDao.insertSelective(lcpMetaComponent);
        // 插入属性子表
        if (CollectionUtil.isNotEmpty(lcpMetaComponent.getAttrMetaList())) {
            for (LcpMetaComponentAttr componentAttr : lcpMetaComponent.getAttrMetaList()) {
                componentAttr.setComponentId(lcpMetaComponent.getId());
            }
            lcpMetaComponentAttrService.batchInsertSelective(lcpMetaComponent.getAttrMetaList());
        }
        return num;
    }

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量插入组件元数据")
    @Override
    public int batchInsert(List<LcpMetaComponent> list) {
        return lcpMetaComponentDao.batchInsert(list);
    }

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量选择性插入组件元数据")
    @Override
    public int batchInsertSelective(List<LcpMetaComponent> list) {
        return lcpMetaComponentDao.batchInsertSelective(list);
    }

    /**
     * 修改
     *
     * @param lcpMetaComponent 参数
     * @return
     */
    @Trace("修改组件元数据")
    @Override
    public int update(LcpMetaComponent lcpMetaComponent) {
        return lcpMetaComponentDao.update(lcpMetaComponent);
    }

    /**
     * 选择性修改
     *
     * @param lcpMetaComponent 参数
     * @return
     */
    @Trace("选择性修改组件元数据")
    @Override
    public int updateSelective(LcpMetaComponent lcpMetaComponent) {
        // 更新
        int num = lcpMetaComponentDao.updateSelective(lcpMetaComponent);
        // 插入属性子表
        if (CollectionUtil.isNotEmpty(lcpMetaComponent.getAttrMetaList())) {
            for (LcpMetaComponentAttr componentAttr : lcpMetaComponent.getAttrMetaList()) {
                componentAttr.setComponentId(lcpMetaComponent.getId());
            }
            // 先删除
            lcpMetaComponentAttrService.deleteByComponentId(lcpMetaComponent.getId());
            // 再插入
            lcpMetaComponentAttrService.batchInsertSelective(lcpMetaComponent.getAttrMetaList());
        }
        return num;
    }

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    @Trace("删除组件元数据")
    @Override
    public int delete(Long id) {
        // 删除属性配置
        lcpMetaComponentAttrService.deleteByComponentId(id);
        return lcpMetaComponentDao.delete(id);
    }

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    @Trace("批量删除组件元数据")
    @Override
    public int batchDelete(List<Long> list) {
        for (Long id : list) {
            // 删除属性配置
            lcpMetaComponentAttrService.deleteByComponentId(id);
        }
        return lcpMetaComponentDao.batchDelete(list);
    }

}
