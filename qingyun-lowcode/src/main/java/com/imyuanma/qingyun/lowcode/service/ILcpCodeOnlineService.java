package com.imyuanma.qingyun.lowcode.service;

import com.imyuanma.qingyun.common.model.PageQuery;

import java.util.List;
import java.util.Map;

/**
 * 低代码在线服务
 *
 * @author wangjy
 * @date 2022/07/01 21:27:51
 */
public interface ILcpCodeOnlineService {
    /**
     * 分页查询
     *
     * @param generateId 代码生成配置id
     * @param condition  查询条件
     * @param pageQuery  分页参数
     * @return
     */
    List<Map<String, Object>> getPage(Long generateId, Map<String, Object> condition, PageQuery pageQuery);

    /**
     * 列表查询
     *
     * @param generateId 代码生成配置id
     * @param condition  查询条件
     * @return
     */
    List<Map<String, Object>> getList(Long generateId, Map<String, Object> condition);

    /**
     * 主键查询
     *
     * @param generateId 代码生成配置id
     * @param primaryId  主键值
     * @return
     */
    Map<String, Object> get(Long generateId, Object primaryId);

    /**
     * 选择性插入
     *
     * @param generateId 代码生成配置id
     * @param condition  参数
     * @return
     */
    int insertSelective(Long generateId, Map<String, Object> condition);

    /**
     * 选择性修改
     *
     * @param generateId 代码生成配置id
     * @param condition  参数
     * @return
     */
    int updateSelective(Long generateId, Map<String, Object> condition);

    /**
     * 删除
     *
     * @param generateId 代码生成配置id
     * @param primaryId  主键值
     * @return
     */
    int delete(Long generateId, String primaryId);

    /**
     * 批量删除
     *
     * @param generateId 代码生成配置id
     * @param list       主键值集合
     * @return
     */
    int batchDelete(Long generateId, List<String> list);
}
