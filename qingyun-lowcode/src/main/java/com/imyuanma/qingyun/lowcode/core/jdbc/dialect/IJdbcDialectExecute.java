package com.imyuanma.qingyun.lowcode.core.jdbc.dialect;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.lowcode.model.data.LcpDbColumnDO;
import com.imyuanma.qingyun.lowcode.model.data.LcpDbTableDO;
import com.imyuanma.qingyun.lowcode.model.vo.LcpDbColumnVO;
import com.imyuanma.qingyun.lowcode.model.vo.LcpDbTableDetailVO;

import java.util.List;
import java.util.Map;

/**
 * jdbc template 执行接口
 *
 * @author wangjy
 * @date 2023/04/17 23:52:45
 */
public interface IJdbcDialectExecute {
    /**
     * 查询所有表
     *
     * @param lcpDbTableDO
     * @return
     */
    List<LcpDbTableDO> getTableList(LcpDbTableDO lcpDbTableDO);

    /**
     * 根据表名查询表信息
     *
     * @param tableName 表名
     * @return
     */
    LcpDbTableDO getTableByTableName(String tableName);

    /**
     * 根据表名批量查询表信息
     *
     * @param tableNameList
     * @return
     */
    List<LcpDbTableDO> getTableListByTableName(List<String> tableNameList);

    /**
     * 查询表字段信息
     *
     * @param tableName
     * @return
     */
    List<LcpDbColumnDO> getColumnListByTableName(String tableName);

    /**
     * 修改字段
     *
     * @param columnVO
     */
    void modifyDbColumn(LcpDbColumnVO columnVO);

    /**
     * 新增字段
     *
     * @param columnVO
     */
    void addDbColumn(LcpDbColumnVO columnVO);

    /**
     * 创建表
     *
     * @param tableDetailVO
     */
    void createTable(LcpDbTableDetailVO tableDetailVO);

    /**
     * 执行ddl
     *
     * @param sql
     */
    void executeDdl(String sql);

    /**
     * 单查询
     *
     * @param sql    sql语句
     * @param params 参数
     * @return
     */
    Map<String, Object> selectOne(String sql, Object... params);

    /**
     * 单查询
     *
     * @param sql         sql语句
     * @param elementType 返回值类型
     * @param params      参数
     * @param <T>
     * @return
     */
    <T> T selectOne(String sql, Class<T> elementType, Object... params);

    /**
     * 列表查询
     *
     * @param sql    查询sql
     * @param params 查询参数
     * @return
     */
    List<Map<String, Object>> selectList(String sql, Object... params);

    /**
     * 列表查询
     *
     * @param sql         查询sql
     * @param elementType 返回值类型
     * @param params      查询参数
     * @param <T>
     * @return
     */
    <T> List<T> selectList(String sql, Class<T> elementType, Object... params);

    /**
     * 分页查询
     *
     * @param sql       查询sql
     * @param pageQuery 分页条件
     * @param params    查询参数
     * @return
     */
    List<Map<String, Object>> selectPage(String sql, PageQuery pageQuery, Object... params);

    /**
     * 分页查询
     *
     * @param sql         查询sql
     * @param pageQuery   分页条件
     * @param elementType 返回值类型
     * @param params      查询参数
     * @param <T>
     * @return
     */
    <T> List<T> selectPage(String sql, PageQuery pageQuery, Class<T> elementType, Object... params);

    /**
     * 插入
     *
     * @param sql    sql语句
     * @param params 参数
     * @return
     */
    int insert(String sql, Object... params);

    /**
     * 修改
     *
     * @param sql    sql语句
     * @param params 参数
     * @return
     */
    int update(String sql, Object... params);

    /**
     * 删除
     *
     * @param sql    sql语句
     * @param params 参数
     * @return
     */
    int delete(String sql, Object... params);
}
