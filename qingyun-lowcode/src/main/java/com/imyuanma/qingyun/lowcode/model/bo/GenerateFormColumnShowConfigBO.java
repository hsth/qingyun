package com.imyuanma.qingyun.lowcode.model.bo;

import com.imyuanma.qingyun.common.util.StringUtil;
import com.imyuanma.qingyun.lowcode.model.enums.ELcpFormItemDisableTypeEnum;
import com.imyuanma.qingyun.lowcode.model.enums.ELcpFormItemShowTypeEnum;
import lombok.Data;

/**
 * 表单字段显隐配置
 *
 * @author wangjy
 * @date 2022/05/04 17:50:36
 */
@Data
public class GenerateFormColumnShowConfigBO {
    /**
     * 隐藏类型
     *
     * @see ELcpFormItemShowTypeEnum
     */
    private Integer showType;
    /**
     * 隐藏条件表达式
     * 当showType=4时设置该值
     */
    private String hideCondition;
    /**
     * 禁用类型
     *
     * @see ELcpFormItemDisableTypeEnum
     */
    private Integer disableType;
    /**
     * 禁用条件表达式
     * 当disableType=4时设置该值
     */
    private String disableCondition;

    /**
     * 新增时隐藏
     * @return
     */
    public boolean isAddHide() {
        return ELcpFormItemShowTypeEnum.HIDE.getType().equals(showType)
                || ELcpFormItemShowTypeEnum.ADD_HIDE.getType().equals(showType);
    }

    /**
     * 编辑时隐藏
     * @return
     */
    public boolean isEditHide() {
        return ELcpFormItemShowTypeEnum.HIDE.getType().equals(showType)
                || ELcpFormItemShowTypeEnum.EDIT_HIDE.getType().equals(showType);
    }

    /**
     * 条件隐藏
     * @return
     */
    public boolean isConditionHide() {
        return ELcpFormItemShowTypeEnum.CONDITION_HIDE.getType().equals(showType)
                && StringUtil.isNotBlank(hideCondition);
    }

    /**
     * 新增禁用
     * @return
     */
    public boolean isAddDisable() {
        return ELcpFormItemDisableTypeEnum.DISABLE.getType().equals(disableType)
                || ELcpFormItemDisableTypeEnum.ADD_DISABLE.getType().equals(disableType);
    }

    /**
     * 编辑禁用
     * @return
     */
    public boolean isEditDisable() {
        return ELcpFormItemDisableTypeEnum.DISABLE.getType().equals(disableType)
                || ELcpFormItemDisableTypeEnum.EDIT_DISABLE.getType().equals(disableType);
    }

    /**
     * 条件禁用
     * @return
     */
    public boolean isConditionDisable() {
        return ELcpFormItemDisableTypeEnum.CONDITION_DISABLE.getType().equals(disableType)
                && StringUtil.isNotBlank(disableCondition);
    }


}
