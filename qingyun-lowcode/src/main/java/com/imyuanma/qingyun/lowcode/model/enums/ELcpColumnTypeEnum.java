package com.imyuanma.qingyun.lowcode.model.enums;


import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * 字段类型
 * 常用的
 *
 * @author wangjy
 * @date 2022/03/19 23:52:40
 */
public enum ELcpColumnTypeEnum {

    BIGINT("bigint", "bigint", "Long")
    , TINYINT("tinyint", "tinyint", "Integer")
    , INT("int", "int", "Integer", "NUMBER"::equalsIgnoreCase)
    , DOUBLE("double", "double", "Double")
    , FLOAT("float", "float", "Float")
    , DECIMAL("decimal", "decimal", "BigDecimal")
    , NUMERIC("numeric", "numeric", "BigDecimal")

    , BOOLEAN("boolean", "boolean", "Boolean")

    , DATE("date", "date", "Date")
    , DATETIME("datetime", "datetime", "Date")
    , TIME("time", "time", "Date")
    , TIMESTAMP("timestamp", "timestamp", "Date", dataType -> dataType != null && dataType.toLowerCase().startsWith("timestamp"))

    , TEXT("text", "text", "String")
    , CHAR("char", "char", "Char")
    , VARCHAR("varchar", "varchar", "String", "VARCHAR2"::equalsIgnoreCase)
    ;

    /**
     * 字段类型(平台内定义的枚举值,不一定和数据库完全一致)
     */
    private String type;
    /**
     * 类型说明
     */
    private String text;
    /**
     * 对应java属性类型
     */
    private String javaType;
    /**
     * 类型匹配函数
     */
    private Predicate<String> matchFunc;

    ELcpColumnTypeEnum(String type, String text, String javaType) {
        this(type, text, javaType, null);
    }

    ELcpColumnTypeEnum(String type, String text, String javaType, Predicate<String> matchFunc) {
        this.type = type;
        this.text = text;
        this.javaType = javaType;
        this.matchFunc = matchFunc;
    }

    /**
     * 匹配数据库字段类型
     * @param dataType
     * @return
     */
    public boolean match(String dataType) {
        if (type.equalsIgnoreCase(dataType)) {
            return true;
        }
        if (matchFunc != null) {
            return matchFunc.test(dataType);
        }
        return false;
    }

    /**
     * 是否大文本
     * @return
     */
    public boolean isText() {
        return this == TEXT;
    }

    /**
     * 是否有长度限制的字符类型
     * @return
     */
    public boolean isLengthLimitStringType() {
        return this == CHAR || this == VARCHAR;
    }

    /**
     * 字符类型
     * @return
     */
    public boolean isStringType() {
        return this == CHAR || this == VARCHAR || this == TEXT;
    }

    /**
     * 是否数字类型
     * @return
     */
    public boolean isNumberType() {
        return isNumberTypeJava(this.javaType);
    }

    /**
     * 是否数字类型
     * @param javaType
     * @return
     */
    public static boolean isNumberTypeJava(String javaType) {
        return BIGINT.javaType.equals(javaType) || TINYINT.javaType.equals(javaType) || INT.javaType.equals(javaType)
                || DOUBLE.javaType.equals(javaType) || FLOAT.javaType.equals(javaType) || DECIMAL.javaType.equals(javaType)
                || NUMERIC.javaType.equals(javaType);
    }

    /**
     * 是否时间类型
     * @return
     */
    public boolean isDateType() {
        return this == DATE || this == DATETIME || this == TIME || this == TIMESTAMP;
    }

    public static ELcpColumnTypeEnum ofOrDefault(String type) {
        return Optional.ofNullable(ELcpColumnTypeEnum.of(type)).orElse(VARCHAR);
    }

    public static ELcpColumnTypeEnum of(String type) {
        if (type != null) {
            for (ELcpColumnTypeEnum e : ELcpColumnTypeEnum.values()) {
                if (e.match(type)) {
                    return e;
                }
            }
        }
        return null;
    }

    public String getType() {
        return type;
    }

    public String getText() {
        return text;
    }

    public String getJavaType() {
        return javaType;
    }
}
