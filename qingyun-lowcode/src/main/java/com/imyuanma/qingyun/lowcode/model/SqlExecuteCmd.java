package com.imyuanma.qingyun.lowcode.model;

import com.imyuanma.qingyun.common.model.PageQuery;
import lombok.Data;

/**
 * sql执行
 *
 * @author wangjy
 * @date 2023/05/06 10:28:29
 */
@Data
public class SqlExecuteCmd {
    /**
     * 待执行sql
     */
    private String sql;
    /**
     * 分页查询参数 可以为空
     */
    private PageQuery pageQuery;
}
