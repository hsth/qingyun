package com.imyuanma.qingyun.lowcode.model.bo;

import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.common.util.StringUtil;
import com.imyuanma.qingyun.interfaces.common.model.enums.EYesOrNoEnum;
import com.imyuanma.qingyun.lowcode.model.enums.ELcpQueryConditionTypeEnum;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 代码生成扩展配置
 *
 * @author wangjy
 * @date 2022/05/04 16:40:12
 */
@Data
public class GenerateMainExtBO {
    private Long id;
    /**
     * 链路追踪配置类型
     * 0/null:无,1:web+service,2:仅web
     */
    private Integer traceType;
    /**
     * 服务域名
     */
    private String webDomain;
    /**
     * excel导入
     *
     * @see com.imyuanma.qingyun.interfaces.common.model.enums.EYesOrNoEnum
     */
    private Integer supportImportExcel;
    /**
     * excel导出
     *
     * @see com.imyuanma.qingyun.interfaces.common.model.enums.EYesOrNoEnum
     */
    private Integer supportExportExcel;
    /**
     * 模型继承类型
     * 1继承DbSortDO
     * 2继承BaseDO模型
     * 3继承并自动填充BaseDO
     *
     * @see com.imyuanma.qingyun.lowcode.model.enums.ELcpModelExtendsTypeEnum
     */
    private Integer modelExtendsType;
    /**
     * 视图页表格首列类型
     *
     * @see com.imyuanma.qingyun.lowcode.model.enums.ELcpTableColumnTypeEnum
     */
    private String tableFirstColumnType;
    /**
     * 分页的每页大小
     */
    private Integer pageSize;
    /**
     * 视图页表格行操作按钮集合
     * tag_view_tr_edit:编辑
     * tag_view_tr_del:删除
     */
    @Deprecated
    private List<String> tableRowOperateButtonList;
    /**
     * 按钮栏配置
     */
    private GenerateButtonBarConfigBO buttonBarConfig;
    /**
     * 表格行按钮配置
     */
    private GenerateButtonBarConfigBO tableRowButtonConfig;
    /**
     * 在线编码模式
     *
     * @see com.imyuanma.qingyun.interfaces.common.model.enums.EYesOrNoEnum
     */
    private Integer onlineCodeFlag;
    /**
     * 表格数据url
     */
    private String tableDataUrl;
    /**
     * 详情url
     */
    private String detailUrl;
    /**
     * 插入url
     */
    private String insertUrl;
    /**
     * 更新url
     */
    private String updateUrl;
    /**
     * 数据删除url
     */
    private String deleteUrl;
    /**
     * 模板下载url
     */
    private String templateUrl = "";
    /**
     * 导入url
     */
    private String importUrl = "";
    /**
     * 导出url
     */
    private String exportUrl = "";
    /**
     * 扩展js引用
     */
    private List<JsLinkBO> jsLinkList;
    /**
     * 扩展css引用
     */
    private List<CssLinkBO> cssLinkList;
    /**
     * 查询条件列表
     */
    private List<SearchConditionItem> searchConditionList;

    /**
     * 树形表格, 1是0否
     */
    private Integer supportTreeTable;
    /**
     * 树形表格的row-key
     */
    private String treeTableRowKey;
    /**
     * 树形表格的子数据字段名
     */
    private String treeTableChildrenField;

    /**
     * 默认排序字段
     */
    private String defaultSortBy;
    /**
     * 默认排序类型
     */
    private String defaultSortType;

    // ===========表单全局配置 start=========
    /**
     * 表单弹窗类型
     * dialog:弹窗(默认), drawer:对话框, 其他走默认
     */
    private String formDialogType = "dialog";
    /**
     * 表单宽度
     */
    private String formWidth = "";
    /**
     * 隐藏表单更新按钮标识
     */
    private Integer hideFormUpdateFlag = EYesOrNoEnum.NO.getCode();
    // ===========表单全局配置 end=========

    /**
     * 表格数据刷新时开启loading状态
     */
    private Integer enableTableLoading = EYesOrNoEnum.YES.getCode();

    /**
     * 按字段查询配置
     */
    private List<FieldSearchConfigBO> fieldSearchConfigList = new ArrayList<>();

    /**
     * 清除无效数据
     */
    public void clearInvalidData() {
        if (CollectionUtil.isNotEmpty(this.jsLinkList)) {
            this.jsLinkList = this.jsLinkList.stream()
                    .filter(Objects::nonNull)
                    .filter(item -> StringUtil.isNotBlank(item.getUrl()))
                    .collect(Collectors.toList());
        }
        if (CollectionUtil.isNotEmpty(this.cssLinkList)) {
            this.cssLinkList = this.cssLinkList.stream()
                    .filter(Objects::nonNull)
                    .filter(item -> StringUtil.isNotBlank(item.getUrl()))
                    .collect(Collectors.toList());
        }
        if (CollectionUtil.isNotEmpty(this.searchConditionList)) {
            this.searchConditionList = this.searchConditionList.stream()
                    .filter(Objects::nonNull)
                    .filter(item -> item.getListQueryCondition() != null && !ELcpQueryConditionTypeEnum.NO.getType().equals(item.getListQueryCondition()))
                    .distinct()
                    .collect(Collectors.toList());
        }
    }
}
