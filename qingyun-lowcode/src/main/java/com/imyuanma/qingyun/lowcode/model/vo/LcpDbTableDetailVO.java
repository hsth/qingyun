package com.imyuanma.qingyun.lowcode.model.vo;

import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.lowcode.model.data.LcpDbColumnDO;
import com.imyuanma.qingyun.lowcode.model.data.LcpDbIndexDO;
import com.imyuanma.qingyun.lowcode.model.data.LcpDbTableDO;
import lombok.Data;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 数据库表详情
 *
 * @author wangjy
 * @date 2022/11/13 23:24:03
 */
@Data
public class LcpDbTableDetailVO {
    /**
     * 表名
     */
    private String tableName;
    /**
     * 表注释
     */
    private String tableComment;
    /**
     * 表数据量
     */
    private Long tableRows;
    /**
     * 表数据大小(Byte)
     */
    private Long dataLength;

    /**
     * 字段信息集合
     */
    private List<LcpDbColumnVO> columnList;
    /**
     * 索引集合
     */
    private List<LcpDbIndexDO> indexList;

    public LcpDbTableDetailVO() {
    }

    public LcpDbTableDetailVO(LcpDbTableDO tableDO) {
        this.tableName = tableDO.getTableName();
        this.tableComment = tableDO.getTableComment();
        this.tableRows = tableDO.getTableRows();
        this.dataLength = tableDO.getDataLength();
    }

    public LcpDbTableDetailVO(LcpDbTableDO tableDO, List<LcpDbColumnDO> columnList, List<LcpDbIndexDO> indexList) {
        this.tableName = tableDO.getTableName();
        this.tableComment = tableDO.getTableComment();
        this.tableRows = tableDO.getTableRows();
        this.dataLength = tableDO.getDataLength();
        if (CollectionUtil.isNotEmpty(columnList)) {
            this.columnList = columnList.stream().filter(Objects::nonNull).map(LcpDbColumnVO::new).collect(Collectors.toList());
        }
        this.indexList = indexList;
    }
}
