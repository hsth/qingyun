package com.imyuanma.qingyun.lowcode.service.impl;

import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.common.util.StringUtil;
import com.imyuanma.qingyun.interfaces.common.model.enums.EYesOrNoEnum;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.lowcode.core.jdbc.IDynamicJdbcService;
import com.imyuanma.qingyun.lowcode.dao.ILcpDbDao;
import com.imyuanma.qingyun.lowcode.model.SqlExecuteCmd;
import com.imyuanma.qingyun.lowcode.model.data.LcpDbColumnDO;
import com.imyuanma.qingyun.lowcode.model.data.LcpDbTableDO;
import com.imyuanma.qingyun.lowcode.model.enums.ELcpColumnTypeEnum;
import com.imyuanma.qingyun.lowcode.model.vo.LcpDbColumnVO;
import com.imyuanma.qingyun.lowcode.model.vo.LcpDbTableDetailVO;
import com.imyuanma.qingyun.lowcode.service.ILcpDbService;
import com.imyuanma.qingyun.lowcode.util.DdlFactory;
import com.imyuanma.qingyun.lowcode.util.LcpConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 数据库操作服务
 *
 * @author wangjy
 * @date 2022/05/04 15:30:49
 */
@Slf4j
@Service
public class LcpDbServiceImpl implements ILcpDbService {
    /**
     * DB操作
     */
    @Autowired
    private ILcpDbDao dbDao;
    /**
     * 动态数据源服务
     */
    @Autowired
    private IDynamicJdbcService dynamicJdbcService;

    /**
     * 查询数据库所有表
     *
     * @return
     */
    @Override
    public List<LcpDbTableDO> getTableList(LcpDbTableDO lcpDbTableDO) {
        return dbDao.getTableList(Optional.ofNullable(lcpDbTableDO).orElse(new LcpDbTableDO()));
    }

    /**
     * 查询数据库所有表
     *
     * @param dataSourceCode 数据源code
     * @param lcpDbTableDO   查询条件
     * @return
     */
    @Override
    public List<LcpDbTableDO> getTableListDynamic(String dataSourceCode, LcpDbTableDO lcpDbTableDO) {
        if (LcpConstant.CURRENT_DB_DATA_SOURCE_CODE.equals(dataSourceCode)) {
            return this.getTableList(lcpDbTableDO);
        } else {
            return dynamicJdbcService.getTableList(dataSourceCode, lcpDbTableDO);
        }
    }

    /**
     * 根据表名查询表信息
     *
     * @param tableName 表名
     * @return
     */
    @Override
    public LcpDbTableDO getTableByTableName(String tableName) {
        return dbDao.getTableByTableName(tableName);
    }

    /**
     * 根据表名查询表信息
     *
     * @param dataSourceCode
     * @param tableName
     * @return
     */
    @Override
    public LcpDbTableDO getTableByTableNameDynamic(String dataSourceCode, String tableName) {
        if (LcpConstant.CURRENT_DB_DATA_SOURCE_CODE.equals(dataSourceCode)) {
            return this.getTableByTableName(tableName);
        } else {
            return dynamicJdbcService.getTableByTableName(dataSourceCode, tableName);
        }
    }

    /**
     * 根据表名批量查询表信息
     *
     * @param tableNameList 表名集合
     * @return
     */
    @Override
    public List<LcpDbTableDO> getTableListByTableName(List<String> tableNameList) {
        return dbDao.getTableListByTableName(tableNameList);
    }

    /**
     * 根据表名批量查询表信息
     *
     * @param dataSourceCode
     * @param tableNameList
     * @return
     */
    @Override
    public List<LcpDbTableDO> getTableListByTableNameDynamic(String dataSourceCode, List<String> tableNameList) {
        if (LcpConstant.CURRENT_DB_DATA_SOURCE_CODE.equals(dataSourceCode)) {
            return this.getTableListByTableName(tableNameList);
        } else {
            return dynamicJdbcService.getTableListByTableName(dataSourceCode, tableNameList);
        }
    }

    /**
     * 根据表名查询字段信息
     *
     * @param tableName 表名
     * @return
     */
    @Override
    public List<LcpDbColumnDO> getColumnListByTableName(String tableName) {
        return dbDao.getColumnListByTableName(tableName);
    }

    /**
     * 查询表字段信息
     *
     * @param dataSourceCode
     * @param tableName
     * @return
     */
    @Override
    public List<LcpDbColumnDO> getColumnListByTableNameDynamic(String dataSourceCode, String tableName) {
        if (LcpConstant.CURRENT_DB_DATA_SOURCE_CODE.equals(dataSourceCode)) {
            return this.getColumnListByTableName(tableName);
        } else {
            return dynamicJdbcService.getColumnListByTableName(dataSourceCode, tableName);
        }
    }

    /**
     * 修改字段
     *
     * @param columnVO
     */
    @Trace("修改字段")
    @Override
    public void modifyDbColumn(LcpDbColumnVO columnVO) {
        String sql = DdlFactory.buildModifyColumnSql(columnVO);
        log.info("[修改数据库字段] 待执行字段修改sql:{}", sql);
        dbDao.executeDdl(sql);
    }

    /**
     * 修改字段
     *
     * @param dataSourceCode
     * @param columnVO
     */
    @Override
    public void modifyDbColumnDynamic(String dataSourceCode, LcpDbColumnVO columnVO) {
        if (LcpConstant.CURRENT_DB_DATA_SOURCE_CODE.equals(dataSourceCode)) {
            this.modifyDbColumn(columnVO);
        } else {
            dynamicJdbcService.modifyDbColumn(dataSourceCode, columnVO);
        }
    }

    /**
     * 新增字段
     *
     * @param columnVO
     */
    @Trace("新增字段")
    @Override
    public void addDbColumn(LcpDbColumnVO columnVO) {
        String sql = DdlFactory.buildAddColumnSql(columnVO);
        log.info("[新增数据库字段] 待执行字段新增sql:{}", sql);
        dbDao.executeDdl(sql);
    }

    /**
     * 新增字段
     *
     * @param dataSourceCode
     * @param columnVO
     */
    @Override
    public void addDbColumnDynamic(String dataSourceCode, LcpDbColumnVO columnVO) {
        if (LcpConstant.CURRENT_DB_DATA_SOURCE_CODE.equals(dataSourceCode)) {
            this.addDbColumn(columnVO);
        } else {
            dynamicJdbcService.addDbColumn(dataSourceCode, columnVO);
        }
    }

    /**
     * 创建数据库表
     *
     * @param tableDetailVO
     */
    @Trace("创建数据库表")
    @Override
    public void createTable(LcpDbTableDetailVO tableDetailVO) {
        String sql = DdlFactory.buildCreateTableSql(tableDetailVO);
        log.info("[创建数据库表] 待执行表创建sql:{}", sql);
        dbDao.executeDdl(sql);
    }

    /**
     * 创建表
     *
     * @param dataSourceCode
     * @param tableDetailVO
     */
    @Override
    public void createTableDynamic(String dataSourceCode, LcpDbTableDetailVO tableDetailVO) {
        if (LcpConstant.CURRENT_DB_DATA_SOURCE_CODE.equals(dataSourceCode)) {
            this.createTable(tableDetailVO);
        } else {
            dynamicJdbcService.createTable(dataSourceCode, tableDetailVO);
        }
    }

    /**
     * 执行查询sql
     *
     * @param sqlExecuteCmd
     * @return
     */
    @Override
    public List<Map<String, Object>> executeSearchSql(SqlExecuteCmd sqlExecuteCmd) {
        if (sqlExecuteCmd.getPageQuery() != null) {
            return dbDao.executeSearchSql(sqlExecuteCmd.getSql(), sqlExecuteCmd.getPageQuery());
        }
        return dbDao.executeSearchSql(sqlExecuteCmd.getSql());
    }

    /**
     * 执行查询sql
     *
     * @param dataSourceCode
     * @param sqlExecuteCmd
     * @return
     */
    @Override
    public List<Map<String, Object>> executeSearchSqlDynamic(String dataSourceCode, SqlExecuteCmd sqlExecuteCmd) {
        if (LcpConstant.CURRENT_DB_DATA_SOURCE_CODE.equals(dataSourceCode)) {
            return this.executeSearchSql(sqlExecuteCmd);
        } else {
            if (sqlExecuteCmd.getPageQuery() != null) {
                return dynamicJdbcService.selectPage(dataSourceCode, sqlExecuteCmd.getSql(), sqlExecuteCmd.getPageQuery());
            }
            return dynamicJdbcService.selectList(dataSourceCode, sqlExecuteCmd.getSql());
        }
    }

}
