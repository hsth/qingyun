package com.imyuanma.qingyun.lowcode.model.data;

import com.imyuanma.qingyun.interfaces.common.model.BaseDO;
import lombok.Data;

/**
 * 代码生成字段配置
 *
 * @author wangjy
 * @date 2022/05/04 14:22:29
 */
@Data
public class LcpGenerateFieldDO extends BaseDO {
    /**
     * 主键
     */
    private Long id;
    /**
     * 代码生成id
     */
    private Long generateId;
    /**
     * 数据库字段名
     */
    private String fieldDbName;
    /**
     * 字段注释
     */
    private String fieldDbRemark;
    /**
     * 数据库字段类型
     */
    private String fieldDbType;
    /**
     * 数据库字段长度
     */
    private Integer fieldDbLength;
    /**
     * 数据库字段是否非空
     */
    private Integer fieldDbNotNull;
    /**
     * 数据库字段键
     */
    private String fieldDbKey;
    /**
     * java字段名
     */
    private String fieldJavaName;
    /**
     * java字段类型
     */
    private String fieldJavaType;
    /**
     * 支持导入,1不支持,2支持excel导入
     */
    private Integer supportImport;
    /**
     * 支持导出,1不支持,2支持excel导出
     */
    private Integer supportExport;
    /**
     * 是否在列表显示,1是,0否
     */
    private Integer listColumnShow;
    /**
     * 字段在列表显示的顺序
     */
    private Integer listColumnOrder;
    /**
     * 该列在表格是否固定显示,no,left,right
     */
    private String listColumnFixed;
    /**
     * 列宽度
     */
    private String listColumnWidth;
    /**
     * 该列是否支持排序
     */
    private Integer listColumnSort;
    /**
     * 表头文案
     */
    private String listHeaderTitle;
    /**
     * 表头文案位置,center,left,right
     */
    private String listHeaderAlign;
    /**
     * 表头提示文案
     */
    private String listHeaderTips;
    /**
     * 表格内容位置,center,left,right
     */
    private String listValueAlign;
    /**
     * 表格内容格式,text,html,tag,link,image,button,formatter等
     */
    private String listValueFormat;
    /**
     * 条件查询类型,1无,2等于,3模糊查询,4模糊左匹配等等等等
     */
    private Integer listQueryCondition;
    /**
     * 是否在表单显示,1是,0否
     */
    private Integer formColumnShow;
    /**
     * 表单项显隐控制配置
     */
    private String formColumnShowConfig;
    /**
     * 表单项显示顺序
     */
    private Integer formColumnOrder;
    /**
     * 表单项栅格宽度,1~24
     */
    private Integer formColumnGridWidth;
    /**
     * 表单控件类型,input,date,time等等等等
     */
    private String formControlType;
    /**
     * 表单项默认值,仅新增时有效
     */
    private String formControlDefault;
    /**
     * 表单项标题
     */
    private String formControlTitle;
    /**
     * 表单项placeholder
     */
    private String formControlPlaceholder;
    /**
     * 表单项提示文案
     */
    private String formControlTips;
    /**
     * 表单项校验规则配置
     */
    private String formControlCheckRule;
    /**
     * 表单项数据源配置
     */
    private String formControlDataSource;
    /**
     * 表单项其他配置
     */
    private String formControlConfig;
}
