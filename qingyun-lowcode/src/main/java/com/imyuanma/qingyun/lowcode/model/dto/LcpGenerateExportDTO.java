package com.imyuanma.qingyun.lowcode.model.dto;

import com.imyuanma.qingyun.lowcode.model.data.LcpGenerateFieldDO;
import com.imyuanma.qingyun.lowcode.model.data.LcpGenerateMainDO;
import lombok.Data;

import java.util.List;

/**
 * 导出模型
 *
 * @author wangjy
 * @date 2024/02/22 23:29:06
 */
@Data
public class LcpGenerateExportDTO {
    /**
     * 生成主表
     */
    private LcpGenerateMainDO lcpGenerateMain;
    /**
     * 生成字段列表
     */
    private List<LcpGenerateFieldDO> lcpGenerateFieldList;



}
