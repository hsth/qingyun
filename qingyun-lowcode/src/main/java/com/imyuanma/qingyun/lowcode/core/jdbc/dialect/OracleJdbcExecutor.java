package com.imyuanma.qingyun.lowcode.core.jdbc.dialect;

import com.imyuanma.qingyun.common.db.dal.EDBType;

import javax.sql.DataSource;

/**
 * oracle
 *
 * @author wangjy
 * @date 2023/04/17 23:56:41
 */
public class OracleJdbcExecutor extends AbstractJdbcDialectExecutor {

    public OracleJdbcExecutor(EDBType dbType, DataSource dataSource) {
        super(dbType, dataSource);
    }
}
