package com.imyuanma.qingyun.lowcode.core.jdbc;

import com.imyuanma.qingyun.common.exception.ParamException;
import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.lowcode.core.jdbc.dialect.IJdbcDialectExecute;
import com.imyuanma.qingyun.lowcode.model.data.LcpDbColumnDO;
import com.imyuanma.qingyun.lowcode.model.data.LcpDbTableDO;
import com.imyuanma.qingyun.lowcode.model.vo.LcpDbColumnVO;
import com.imyuanma.qingyun.lowcode.model.vo.LcpDbTableDetailVO;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;
import java.util.Map;

/**
 * 动态jdbc服务
 *
 * @author wangjy
 * @date 2023/04/16 22:43:31
 */
public interface IDynamicJdbcService {

    /**
     * 获取jdbc执行器
     *
     * @param dataSourceCode 数据源code
     * @return
     */
    IJdbcDialectExecute getJdbcDialectExecute(String dataSourceCode);

    /**
     * 列表查询
     *
     * @param dataSourceCode 数据源code
     * @param sql            查询sql
     * @param params         查询参数
     * @return
     */
    List<Map<String, Object>> selectList(String dataSourceCode, String sql, Object... params);

    /**
     * 分页查询
     *
     * @param dataSourceCode 数据源code
     * @param sql            查询sql
     * @param pageQuery      分页条件
     * @param params         查询参数
     * @return
     */
    List<Map<String, Object>> selectPage(String dataSourceCode, String sql, PageQuery pageQuery, Object... params);

    /**
     * 插入
     *
     * @param dataSourceCode
     * @param sql
     * @param params
     * @return
     */
    int insert(String dataSourceCode, String sql, Object... params);

    /**
     * 修改
     *
     * @param dataSourceCode
     * @param sql
     * @param params
     * @return
     */
    int update(String dataSourceCode, String sql, Object... params);

    /**
     * 删除
     *
     * @param dataSourceCode
     * @param sql
     * @param params
     * @return
     */
    int delete(String dataSourceCode, String sql, Object... params);

    /**
     * 查询所有表
     *
     * @param dataSourceCode
     * @param lcpDbTableDO
     * @return
     */
    List<LcpDbTableDO> getTableList(String dataSourceCode, LcpDbTableDO lcpDbTableDO);

    /**
     * 根据表名查询表
     *
     * @param dataSourceCode
     * @param tableName
     * @return
     */
    LcpDbTableDO getTableByTableName(String dataSourceCode, String tableName);

    /**
     * 根据表名批量查询表
     *
     * @param dataSourceCode
     * @param tableNameList
     * @return
     */
    List<LcpDbTableDO> getTableListByTableName(String dataSourceCode, List<String> tableNameList);

    /**
     * 查询表字段信息
     *
     * @param dataSourceCode
     * @param tableName
     * @return
     */
    List<LcpDbColumnDO> getColumnListByTableName(String dataSourceCode, String tableName);

    /**
     * 修改字段
     *
     * @param dataSourceCode
     * @param columnVO
     */
    void modifyDbColumn(String dataSourceCode, LcpDbColumnVO columnVO);

    /**
     * 新增字段
     *
     * @param dataSourceCode
     * @param columnVO
     */
    void addDbColumn(String dataSourceCode, LcpDbColumnVO columnVO);

    /**
     * 创建表
     *
     * @param dataSourceCode
     * @param tableDetailVO
     */
    void createTable(String dataSourceCode, LcpDbTableDetailVO tableDetailVO);
}
