package com.imyuanma.qingyun.lowcode.model.enums;

/**
 * 分类枚举
 *
 * @author YuanMaKeJi
 * @date 2022-12-03 21:44:59
 */
public enum ELcpComponentCategoryEnum {
    CODE_container("container", "容器组件"),
    CODE_form("form", "表单组件"),
    CODE_other("other", "其他组件"),
    CODE_layout("layout", "布局组件"),
    ;

    /**
     * code
     */
    private String code;
    /**
     * 文案
     */
    private String text;

    ELcpComponentCategoryEnum(String code, String text) {
        this.code = code;
        this.text = text;
    }

    /**
     * 根据code获取枚举
     *
     * @param code code值
     */
    public static ELcpComponentCategoryEnum getByCode(String code) {
        for (ELcpComponentCategoryEnum e : ELcpComponentCategoryEnum.values()) {
            if (e.code != null && e.code.equals(code)) {
                return e;
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
