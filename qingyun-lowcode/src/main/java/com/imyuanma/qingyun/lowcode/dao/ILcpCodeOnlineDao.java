package com.imyuanma.qingyun.lowcode.dao;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.lowcode.model.data.CodeOnlineSqlDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 代码在线服务
 *
 * @author wangjy
 * @date 2022/07/01 22:41:01
 */
@Mapper
public interface ILcpCodeOnlineDao {
    /**
     * 分页查询
     *
     * @param codeOnlineSqlDO 查询条件
     * @param rowBounds       分页参数
     * @return
     */
    @Trace("在线code-分页查询")
    List<Map<String, Object>> getList(CodeOnlineSqlDO codeOnlineSqlDO, PageQuery rowBounds);

    /**
     * 列表查询
     * @param codeOnlineSqlDO 查询条件
     * @return
     */
    @Trace("在线code-列表查询")
    List<Map<String, Object>> getList(CodeOnlineSqlDO codeOnlineSqlDO);

    /**
     * 主键查询
     *
     * @param codeOnlineSqlDO 参数
     * @return
     */
    @Trace("在线code-主键查询")
    Map<String, Object> get(CodeOnlineSqlDO codeOnlineSqlDO);

    /**
     * 选择性插入
     *
     * @param codeOnlineSqlDO 参数
     * @return
     */
    @Trace("在线code-插入")
    int insertSelective(CodeOnlineSqlDO codeOnlineSqlDO);

    /**
     * 选择性修改
     *
     * @param codeOnlineSqlDO 参数
     * @return
     */
    @Trace("在线code-修改")
    int updateSelective(CodeOnlineSqlDO codeOnlineSqlDO);

    /**
     * 删除
     *
     * @param codeOnlineSqlDO 参数
     * @return
     */
    @Trace("在线code-删除")
    int delete(CodeOnlineSqlDO codeOnlineSqlDO);

    /**
     * 批量删除
     *
     * @param codeOnlineSqlDO 参数
     * @return
     */
    @Trace("在线code-批量删除")
    int batchDelete(CodeOnlineSqlDO codeOnlineSqlDO);
}
