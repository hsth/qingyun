package com.imyuanma.qingyun.lowcode.core.jdbc.dialect;


import com.imyuanma.qingyun.common.db.dal.EDBType;

import javax.sql.DataSource;

/**
 * postgresql
 *
 * @author wangjy
 * @date 2023/04/17 23:56:41
 */
public class PostgreJdbcExecutor extends AbstractJdbcDialectExecutor {

    public PostgreJdbcExecutor(EDBType dbType, DataSource dataSource) {
        super(dbType, dataSource);
    }
}
