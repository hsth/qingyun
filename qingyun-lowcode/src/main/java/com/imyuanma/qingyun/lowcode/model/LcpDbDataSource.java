package com.imyuanma.qingyun.lowcode.model;

import lombok.Data;
import java.util.Date;
import com.imyuanma.qingyun.interfaces.common.model.BaseDO;

/**
 * DB数据源实体类
 *
 * @author YuanMaKeJi
 * @date 2023-04-22 11:18:10
 */
@Data
public class LcpDbDataSource extends BaseDO {

    /**
     * 主键
     */
    private Long id;

    /**
     * 数据源编码
     */
    private String code;

    /**
     * 数据源名称
     */
    private String name;

    /**
     * 数据库类型
     */
    private String dbType;

    /**
     * 驱动类
     */
    private String driverClass;

    /**
     * 数据库连接
     */
    private String dbUrl;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 备注
     */
    private String remark;



}