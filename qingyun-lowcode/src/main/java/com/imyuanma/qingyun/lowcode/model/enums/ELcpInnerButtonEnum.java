package com.imyuanma.qingyun.lowcode.model.enums;

/**
 * 内置按钮
 *
 * @author wangjy
 * @date 2022/07/03 14:14:07
 */
public enum ELcpInnerButtonEnum {

    LIST_MORE_CONDITION("LIST_MORE_CONDITION", "更多条件", "", "default", ELcpButtonTypeEnum.PRIMARY.getType(), ELcpButtonCategoryEnum.LINK.getType(), "!moreConditionFlag", "moreConditionFlag = true", ""),
    LIST_LESS_CONDITION("LIST_LESS_CONDITION", "收起条件", "", "default", ELcpButtonTypeEnum.PRIMARY.getType(), ELcpButtonCategoryEnum.LINK.getType(), "moreConditionFlag", "moreConditionFlag = false", ""),
    LIST_QUERY("LIST_QUERY", "查询", "<i class=\"fa fa-search\" aria-hidden=\"true\"></i>", "default", ELcpButtonTypeEnum.PRIMARY.getType(), ELcpButtonCategoryEnum.DEFAULT.getType(), "", "list_search", ""),

    LIST_ADD("LIST_ADD", "新增", "", "default", ELcpButtonTypeEnum.PRIMARY.getType(), ELcpButtonCategoryEnum.PLAIN.getType(), "", "list_add", ""),
    LIST_DELETE("LIST_DELETE", "删除", "", "default", ELcpButtonTypeEnum.DANGER.getType(), ELcpButtonCategoryEnum.PLAIN.getType(), "", "list_delete_batch", ""),
    LIST_RELOAD("LIST_RELOAD", "刷新", "", "default", ELcpButtonTypeEnum.WARNING.getType(), ELcpButtonCategoryEnum.PLAIN.getType(), "", "reloadPage", ""),

    TABLE_ROW_EDIT("TABLE_ROW_EDIT", "编辑", "", "default", ELcpButtonTypeEnum.PRIMARY.getType(), ELcpButtonCategoryEnum.LINK.getType(), "", "list_edit(scope.row)", ""),
    TABLE_ROW_DELETE("TABLE_ROW_DELETE", "删除", "", "default", ELcpButtonTypeEnum.DANGER.getType(), ELcpButtonCategoryEnum.LINK.getType(), "", "list_delete(scope.row)", ""),

    FORM_SAVE("FORM_SAVE", "确认", "", "default", ELcpButtonTypeEnum.PRIMARY.getType(), ELcpButtonCategoryEnum.DEFAULT.getType(), "", "form_save", ""),
    ;

    /**
     * 按钮编码
     */
    private String code;
    /**
     * 按钮名称
     */
    private String name;
    /**
     * 图标
     */
    private String icon;
    /**
     * 按钮大小
     */
    private String size;
    /**
     * 按钮样式
     * '',primary,success,info,warning,danger
     *
     * @see ELcpButtonTypeEnum
     */
    private String type;
    /**
     * 按钮分类
     * '',plain,text,link,round,circle
     *
     * @see ELcpButtonCategoryEnum
     */
    private String category;
    /**
     * 展示规则
     */
    private String showRule;
    /**
     * 点击事件方法名
     */
    private String clickFunctionName;
    /**
     * 点击事件脚本
     */
    private String clickScript;

    ELcpInnerButtonEnum(String code, String name, String icon, String size, String type, String category, String showRule, String clickFunctionName, String clickScript) {
        this.code = code;
        this.name = name;
        this.icon = icon;
        this.size = size;
        this.type = type;
        this.category = category;
        this.showRule = showRule;
        this.clickFunctionName = clickFunctionName;
        this.clickScript = clickScript;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getIcon() {
        return icon;
    }

    public String getSize() {
        return size;
    }

    public String getType() {
        return type;
    }

    public String getCategory() {
        return category;
    }

    public String getShowRule() {
        return showRule;
    }

    public String getClickFunctionName() {
        return clickFunctionName;
    }

    public String getClickScript() {
        return clickScript;
    }
}
