package com.imyuanma.qingyun.lowcode.model.vo;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * sql执行结果
 *
 * @author wangjy
 * @date 2023/05/06 10:28:29
 */
@Data
public class SqlExecuteResultVO {
    /**
     * 查询结果数据
     */
    private List<Map<String, Object>> dataList;
    /**
     * 表头
     */
    private List<String> headers;

}
