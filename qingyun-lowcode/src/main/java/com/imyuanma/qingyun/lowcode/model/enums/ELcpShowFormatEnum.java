package com.imyuanma.qingyun.lowcode.model.enums;

/**
 * 展示格式
 *
 * @author wangjy
 * @date 2022/03/20 11:23:01
 */
public enum ELcpShowFormatEnum {
    TEXT("text", "文本")
    , HTML("html", "html")
    , TAG("tag", "标签")
    , TIME("time", "时间格式")
    , DATE("date", "日期格式")
    , LINK("link", "超链")
    , IMAGE("image", "图片")
    , BUTTON("button", "按钮")
    , FILE("file", "文件")
    , FORMATTER("formatter", "自定义格式")
    ;

    private String type;
    private String text;

    ELcpShowFormatEnum(String type, String text) {
        this.type = type;
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public String getText() {
        return text;
    }
}
