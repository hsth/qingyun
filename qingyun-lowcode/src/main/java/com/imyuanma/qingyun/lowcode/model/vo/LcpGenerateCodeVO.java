package com.imyuanma.qingyun.lowcode.model.vo;

import lombok.Data;

/**
 * 代码生成入参
 *
 * @author wangjy
 * @date 2022/05/07 17:47:11
 */
@Data
public class LcpGenerateCodeVO {
    /**
     * 代码生成配置id
     */
    private Long generateId;
    /**
     * 代码模板id
     */
    private Long codeTemplateId;
}
