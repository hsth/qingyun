package com.imyuanma.qingyun.lowcode.dao;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.lowcode.model.data.LcpTemplateItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 代码模板dao
 *
 * @author YuanMaKeJi
 * @date 2022-06-29 23:12:22
 */
@Mapper
public interface ILcpTemplateItemDao {

    /**
     * 列表查询
     *
     * @param lcpTemplateItem 查询条件
     * @return
     */
    List<LcpTemplateItem> getList(LcpTemplateItem lcpTemplateItem);

    /**
     * 分页查询
     *
     * @param lcpTemplateItem 查询条件
     * @param pageQuery 分页参数
     * @return
     */
    List<LcpTemplateItem> getList(LcpTemplateItem lcpTemplateItem, PageQuery pageQuery);

    /**
     * 统计数量
     *
     * @param lcpTemplateItem 查询条件
     * @return
     */
    int count(LcpTemplateItem lcpTemplateItem);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    LcpTemplateItem get(Long id);

    /**
     * 插入
     *
     * @param lcpTemplateItem 参数
     * @return
     */
    int insert(LcpTemplateItem lcpTemplateItem);

    /**
     * 选择性插入
     *
     * @param lcpTemplateItem 参数
     * @return
     */
    int insertSelective(LcpTemplateItem lcpTemplateItem);

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    int batchInsert(List<LcpTemplateItem> list);

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    int batchInsertSelective(List<LcpTemplateItem> list);

    /**
     * 修改
     *
     * @param lcpTemplateItem 参数
     * @return
     */
    int update(LcpTemplateItem lcpTemplateItem);

    /**
     * 选择性修改
     *
     * @param lcpTemplateItem 参数
     * @return
     */
    int updateSelective(LcpTemplateItem lcpTemplateItem);

    /**
     * 批量修改某字段
     *
     * @param idList      主键集合
     * @param fieldDbName 待修改的字段名
     * @param fieldValue  修改后的值
     * @return
     */
    int batchUpdateColumn(@Param("idList") List<Long> idList, @Param("fieldDbName") String fieldDbName, @Param("fieldValue") Object fieldValue);

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    int delete(Long id);

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    int batchDelete(List<Long> list);

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param lcpTemplateItem 参数
     * @return
     */
    int deleteByCondition(LcpTemplateItem lcpTemplateItem);

    /**
     * 删除全部
     *
     * @return
     */
    int deleteAll();

}
