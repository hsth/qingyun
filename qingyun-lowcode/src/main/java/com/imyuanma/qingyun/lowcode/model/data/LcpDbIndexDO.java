package com.imyuanma.qingyun.lowcode.model.data;

import lombok.Data;

/**
 * 数据库索引
 *
 * @author wangjy
 * @date 2022/11/13 23:26:35
 */
@Data
public class LcpDbIndexDO {
    /**
     * 索引名
     */
    private String indexName;
}
