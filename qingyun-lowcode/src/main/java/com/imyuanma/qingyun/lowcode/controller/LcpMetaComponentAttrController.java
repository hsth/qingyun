package com.imyuanma.qingyun.lowcode.controller;

import com.imyuanma.qingyun.common.factory.BaseDOBuilder;
import com.imyuanma.qingyun.common.model.request.WebRequest;
import com.imyuanma.qingyun.common.model.response.Page;
import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.lowcode.model.LcpMetaComponentAttr;
import com.imyuanma.qingyun.lowcode.service.ILcpMetaComponentAttrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 组件属性配置项web
 *
 * @author YuanMaKeJi
 * @date 2022-12-03 21:45:01
 */
@RestController
@RequestMapping(value = "/lowcode/lcpMetaComponentAttr")
public class LcpMetaComponentAttrController {

    /**
     * 组件属性配置项服务
     */
    @Autowired
    private ILcpMetaComponentAttrService lcpMetaComponentAttrService;

    /**
     * 列表查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("查询组件属性配置项列表")
    @PostMapping("/getList")
    public Result<List<LcpMetaComponentAttr>> getList(@RequestBody WebRequest<LcpMetaComponentAttr> webRequest) {
        return Result.success(lcpMetaComponentAttrService.getList(webRequest.getBody()));
    }

    /**
     * 分页查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("分页查询组件属性配置项")
    @PostMapping("/getPage")
    public Page<List<LcpMetaComponentAttr>> getPage(@RequestBody WebRequest<LcpMetaComponentAttr> webRequest) {
        PageQuery pageQuery = webRequest.buildPageQuery();
        List<LcpMetaComponentAttr> list = lcpMetaComponentAttrService.getPage(webRequest.getBody(), pageQuery);
        return Page.success(list, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("统计组件属性配置项数量")
    @PostMapping("/count")
    public Result<Integer> count(@RequestBody WebRequest<LcpMetaComponentAttr> webRequest) {
        return Result.success(lcpMetaComponentAttrService.count(webRequest.getBody()));
    }

    /**
     * 查询
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("查询组件属性配置项")
    @PostMapping("/get")
    public Result<LcpMetaComponentAttr> get(@RequestBody WebRequest<Long> webRequest) {
        AssertUtil.notNull(webRequest.getBody());
        return Result.success(lcpMetaComponentAttrService.get(webRequest.getBody()));
    }

    /**
     * 新增
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("新增组件属性配置项")
    @PostMapping("/insert")
    public Result insert(@RequestBody WebRequest<LcpMetaComponentAttr> webRequest) {
        LcpMetaComponentAttr lcpMetaComponentAttr = webRequest.getBody();
        AssertUtil.notNull(lcpMetaComponentAttr);
        BaseDOBuilder.fillBaseDOForInsert(lcpMetaComponentAttr);
        lcpMetaComponentAttrService.insertSelective(lcpMetaComponentAttr);
        return Result.success();
    }

    /**
     * 修改
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("修改组件属性配置项")
    @PostMapping("/update")
    public Result update(@RequestBody WebRequest<LcpMetaComponentAttr> webRequest) {
        LcpMetaComponentAttr lcpMetaComponentAttr = webRequest.getBody();
        AssertUtil.notNull(lcpMetaComponentAttr);
        AssertUtil.notNull(lcpMetaComponentAttr.getId());
        BaseDOBuilder.fillBaseDOForUpdate(lcpMetaComponentAttr);
        lcpMetaComponentAttrService.updateSelective(lcpMetaComponentAttr);
        return Result.success();
    }

    /**
     * 删除
     *
     * @param webRequest 参数, 待删除主键,多个使用英文逗号拼接
     * @return
     */
    @Trace("删除组件属性配置项")
    @PostMapping("/delete")
    public Result delete(@RequestBody WebRequest<String> webRequest) {
        String ids = webRequest.getBody();
        AssertUtil.notBlank(ids);
        if (ids.contains(",")) {
            lcpMetaComponentAttrService.batchDelete(Arrays.stream(ids.split(",")).map(Long::valueOf).collect(Collectors.toList()));
        } else {
            lcpMetaComponentAttrService.delete(Long.valueOf(ids));
        }
        return Result.success();
    }

}
