package com.imyuanma.qingyun.lowcode.controller;

import com.imyuanma.qingyun.common.model.request.WebRequest;
import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.lowcode.model.LcpDictItem;
import com.imyuanma.qingyun.lowcode.model.bo.OptionItem;
import com.imyuanma.qingyun.lowcode.service.ILcpDictItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 统一视图
 *
 * @author wangjy
 * @date 2022/12/16 22:56:41
 */
@RestController
@RequestMapping(value = "/lowcode/uniView")
public class LcpUniViewController {
    /**
     * 字典数据项服务
     */
    @Autowired
    private ILcpDictItemService lcpDictItemService;


    @Trace("查询字典明细")
    @PostMapping("/dict/list/dict/{dictCode}")
    public Result<List<OptionItem>> dict(@PathVariable("dictCode") String dictCode, @RequestBody WebRequest webRequest) {
        AssertUtil.notBlank(dictCode);
        List<OptionItem> kvList = new ArrayList<>();
        List<LcpDictItem> list = lcpDictItemService.getListByDictCode(dictCode);
        if (CollectionUtil.isNotEmpty(list)) {
            kvList = list.stream().map(item -> new OptionItem(item.getCode(), item.getName(), item.getStyle())).collect(Collectors.toList());
        }
        return Result.success(kvList);
    }

    @Trace("查询字典明细")
    @PostMapping("/dict/{dictCode}/list")
    public Result<List<OptionItem>> dictList(@PathVariable("dictCode") String dictCode, @RequestBody WebRequest webRequest) {
        AssertUtil.notBlank(dictCode);
        List<OptionItem> kvList = new ArrayList<>();
        List<LcpDictItem> list = lcpDictItemService.getListByDictCode(dictCode);
        if (CollectionUtil.isNotEmpty(list)) {
            kvList = list.stream().map(item -> new OptionItem(item.getCode(), item.getName(), item.getStyle())).collect(Collectors.toList());
        }
        return Result.success(kvList);
    }
}
