package com.imyuanma.qingyun.lowcode.model.enums;

/**
 * 组件类型枚举
 *
 * @author YuanMaKeJi
 * @date 2022-12-03 21:44:59
 */
public enum ELcpComponentTypeEnum {
    CODE_base("base", "基础组件"),
    CODE_group("group", "复合组件"),
    ;

    /**
     * code
     */
    private String code;
    /**
     * 文案
     */
    private String text;

    ELcpComponentTypeEnum(String code, String text) {
        this.code = code;
        this.text = text;
    }

    /**
     * 根据code获取枚举
     *
     * @param code code值
     */
    public static ELcpComponentTypeEnum getByCode(String code) {
        for (ELcpComponentTypeEnum e : ELcpComponentTypeEnum.values()) {
            if (e.code != null && e.code.equals(code)) {
                return e;
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
