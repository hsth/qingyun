package com.imyuanma.qingyun.lowcode.model.bo.ui;

import lombok.Data;

/**
 * 元素属性
 *
 * @author wangjy
 * @date 2022/09/03 11:40:32
 */
@Data
public class LcpPageElementAttrBO {
    /**
     * 属性名, 例如: v-if, width
     */
    private String name;
    /**
     * 属性值
     * 可以是字面量, 例如: 5, true, 100px
     * 可以是一个vue变量(vueFlag=true), 例如: showEleFlag
     */
    private String value;
    /**
     * 值类型
     * number表示value是数字
     * string表示value是字符串
     * boolean表示value是字符串
     * var表示配置的值是一个变量, 在渲染属性时会使用 :name="value"
     */
    private String valueType;
}
