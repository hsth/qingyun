package com.imyuanma.qingyun.lowcode.model.data;

import lombok.Data;

import java.util.Date;


/**
 * 代码模板实体类
 *
 * @author YuanMaKeJi
 * @date 2022-06-29 23:12:22
 */
@Data
public class LcpTemplateItem {

    /**
     * 模板ID
     */
    private Long id;

    /**
     * 模板名称
     */
    private String name;

    /**
     * 所属模板分组
     */
    private Long templateGroupId;

    /**
     * 模板内容
     */
    private String templateContent;

    /**
     * 文件目录
     */
    private String filePath;

    /**
     * 显示顺序
     */
    private Long dataSequence;

    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建时间,按时间检索时作为结束时间使用
     */
    private Date createTime2;


    /**
     * 排序字段
     */
    private String dbSortBy;
    /**
     * 排序类型,升序asc/降序desc
     */
    private String dbSortType;

}