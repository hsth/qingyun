package com.imyuanma.qingyun.lowcode.util;

/**
 * 常量
 *
 * @author wangjy
 * @date 2023/04/26 23:46:32
 */
public class LcpConstant {
    /**
     * 当前库的数据源code
     */
    public static final String CURRENT_DB_DATA_SOURCE_CODE = "_current";
}
