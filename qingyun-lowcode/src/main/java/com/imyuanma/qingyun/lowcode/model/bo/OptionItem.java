package com.imyuanma.qingyun.lowcode.model.bo;

import com.imyuanma.qingyun.interfaces.common.model.vo.IKeyValue;

/**
 * 选项
 *
 * @author wangjy
 * @date 2022/04/01 22:35:45
 */
public class OptionItem implements IKeyValue {
    /**
     * 选项的实际值
     */
    private String key;
    /**
     * 选项的展示值
     */
    private String value;
    /**
     * 样式类型, 用于展示不同的枚举项
     */
    private String styleType;

    public OptionItem() {
    }

    public OptionItem(String key, String value, String styleType) {
        this.key = key;
        this.value = value;
        this.styleType = styleType;
    }

    @Override
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getStyleType() {
        return styleType;
    }

    public void setStyleType(String styleType) {
        this.styleType = styleType;
    }
}
