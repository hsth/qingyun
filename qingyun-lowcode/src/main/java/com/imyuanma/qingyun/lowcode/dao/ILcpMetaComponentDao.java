package com.imyuanma.qingyun.lowcode.dao;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.lowcode.model.LcpMetaComponent;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 组件元数据dao
 *
 * @author YuanMaKeJi
 * @date 2022-12-03 21:44:59
 */
@Mapper
public interface ILcpMetaComponentDao {

    /**
     * 列表查询
     *
     * @param lcpMetaComponent 查询条件
     * @return
     */
    List<LcpMetaComponent> getList(LcpMetaComponent lcpMetaComponent);

    /**
     * 分页查询
     *
     * @param lcpMetaComponent 查询条件
     * @param pageQuery 分页参数
     * @return
     */
    List<LcpMetaComponent> getList(LcpMetaComponent lcpMetaComponent, PageQuery pageQuery);

    /**
     * 统计数量
     *
     * @param lcpMetaComponent 查询条件
     * @return
     */
    int count(LcpMetaComponent lcpMetaComponent);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    LcpMetaComponent get(Long id);

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    List<LcpMetaComponent> getListByIds(List<Long> list);

    /**
     * 插入
     *
     * @param lcpMetaComponent 参数
     * @return
     */
    int insert(LcpMetaComponent lcpMetaComponent);

    /**
     * 选择性插入
     *
     * @param lcpMetaComponent 参数
     * @return
     */
    int insertSelective(LcpMetaComponent lcpMetaComponent);

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    int batchInsert(List<LcpMetaComponent> list);

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    int batchInsertSelective(List<LcpMetaComponent> list);

    /**
     * 修改
     *
     * @param lcpMetaComponent 参数
     * @return
     */
    int update(LcpMetaComponent lcpMetaComponent);

    /**
     * 选择性修改
     *
     * @param lcpMetaComponent 参数
     * @return
     */
    int updateSelective(LcpMetaComponent lcpMetaComponent);

    /**
     * 批量修改某字段
     *
     * @param idList      主键集合
     * @param fieldDbName 待修改的字段名
     * @param fieldValue  修改后的值
     * @return
     */
    int batchUpdateColumn(@Param("idList") List<Long> idList, @Param("fieldDbName") String fieldDbName, @Param("fieldValue") Object fieldValue);

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    int delete(Long id);

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    int batchDelete(List<Long> list);

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param lcpMetaComponent 参数
     * @return
     */
    int deleteByCondition(LcpMetaComponent lcpMetaComponent);

    /**
     * 删除全部
     *
     * @return
     */
    int deleteAll();

}
