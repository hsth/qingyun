package com.imyuanma.qingyun.lowcode.controller;

import com.imyuanma.qingyun.common.model.request.WebRequest;
import com.imyuanma.qingyun.common.model.response.Page;
import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.lowcode.model.data.LcpTemplateItem;
import com.imyuanma.qingyun.lowcode.service.ILcpTemplateItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 代码模板web
 *
 * @author YuanMaKeJi
 * @date 2022-06-29 23:12:22
 */
@RestController
@RequestMapping(value = "/lowcode/lcpTemplateItem", method = {RequestMethod.POST})
public class LcpTemplateItemController {

    /**
     * 代码模板服务
     */
    @Autowired
    private ILcpTemplateItemService lcpTemplateItemService;

    /**
     * 列表查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @RequestMapping("/getList")
    public Result<List<LcpTemplateItem>> getList(@RequestBody WebRequest<LcpTemplateItem> webRequest) {
        return Result.success(lcpTemplateItemService.getList(webRequest.getBody()));
    }

    /**
     * 分页查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @RequestMapping("/getPage")
    public Page<List<LcpTemplateItem>> getPage(@RequestBody WebRequest<LcpTemplateItem> webRequest) {
        PageQuery pageQuery = webRequest.buildPageQuery();
        List<LcpTemplateItem> list = lcpTemplateItemService.getPage(webRequest.getBody(), pageQuery);
        return Page.success(list, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param webRequest 查询条件
     * @return
     */
    @RequestMapping("/count")
    public Result<Integer> count(@RequestBody WebRequest<LcpTemplateItem> webRequest) {
        return Result.success(lcpTemplateItemService.count(webRequest.getBody()));
    }

    /**
     * 查询
     *
     * @param webRequest 参数
     * @return
     */
    @RequestMapping("/get")
    public Result<LcpTemplateItem> get(@RequestBody WebRequest<Long> webRequest) {
        AssertUtil.notNull(webRequest.getBody());
        return Result.success(lcpTemplateItemService.get(webRequest.getBody()));
    }

    /**
     * 新增
     *
     * @param webRequest 参数
     * @return
     */
    @RequestMapping("/insert")
    public Result insert(@RequestBody WebRequest<LcpTemplateItem> webRequest) {
        LcpTemplateItem lcpTemplateItem = webRequest.getBody();
        AssertUtil.notNull(lcpTemplateItem);
        lcpTemplateItemService.insertSelective(lcpTemplateItem);
        return Result.success();
    }

    /**
     * 修改
     *
     * @param webRequest 参数
     * @return
     */
    @RequestMapping("/update")
    public Result update(@RequestBody WebRequest<LcpTemplateItem> webRequest) {
        LcpTemplateItem lcpTemplateItem = webRequest.getBody();
        AssertUtil.notNull(lcpTemplateItem);
        AssertUtil.notNull(lcpTemplateItem.getId());
        lcpTemplateItemService.updateSelective(lcpTemplateItem);
        return Result.success();
    }

    /**
     * 删除
     *
     * @param webRequest 参数, 待删除主键,多个使用英文逗号拼接
     * @return
     */
    @RequestMapping("/delete")
    public Result delete(@RequestBody WebRequest<String> webRequest) {
        String ids = webRequest.getBody();
        AssertUtil.notBlank(ids);
        if (ids.contains(",")) {
            lcpTemplateItemService.batchDelete(Arrays.stream(ids.split(",")).map(Long::valueOf).collect(Collectors.toList()));
        } else {
            lcpTemplateItemService.delete(Long.valueOf(ids));
        }
        return Result.success();
    }

}
