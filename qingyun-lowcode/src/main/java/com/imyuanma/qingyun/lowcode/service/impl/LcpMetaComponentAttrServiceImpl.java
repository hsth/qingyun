package com.imyuanma.qingyun.lowcode.service.impl;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.lowcode.dao.ILcpMetaComponentAttrDao;
import com.imyuanma.qingyun.lowcode.model.LcpMetaComponentAttr;
import com.imyuanma.qingyun.lowcode.service.ILcpMetaComponentAttrService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 组件属性配置项服务
 *
 * @author YuanMaKeJi
 * @date 2022-12-03 21:45:01
 */
@Slf4j
@Service
public class LcpMetaComponentAttrServiceImpl implements ILcpMetaComponentAttrService {

    /**
     * 组件属性配置项dao
     */
    @Autowired
    private ILcpMetaComponentAttrDao lcpMetaComponentAttrDao;

    /**
     * 列表查询
     *
     * @param lcpMetaComponentAttr 查询条件
     * @return
     */
    @Trace("查询组件属性配置项列表")
    @Override
    public List<LcpMetaComponentAttr> getList(LcpMetaComponentAttr lcpMetaComponentAttr) {
        return lcpMetaComponentAttrDao.getList(lcpMetaComponentAttr);
    }

    /**
     * 根据组件id查询属性配置
     *
     * @param componentId
     * @return
     */
    @Trace("根据组件id查询属性配置")
    @Override
    public List<LcpMetaComponentAttr> getListByComponentId(Long componentId) {
        return lcpMetaComponentAttrDao.getListByComponentId(componentId);
    }

    /**
     * 分页查询
     *
     * @param lcpMetaComponentAttr 查询条件
     * @param pageQuery 分页参数
     * @return
     */
    @Trace("分页查询组件属性配置项")
    @Override
    public List<LcpMetaComponentAttr> getPage(LcpMetaComponentAttr lcpMetaComponentAttr, PageQuery pageQuery) {
        return lcpMetaComponentAttrDao.getList(lcpMetaComponentAttr, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param lcpMetaComponentAttr 查询条件
     * @return
     */
    @Trace("统计组件属性配置项数量")
    @Override
    public int count(LcpMetaComponentAttr lcpMetaComponentAttr) {
        return lcpMetaComponentAttrDao.count(lcpMetaComponentAttr);
    }

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    @Trace("根据主键查询组件属性配置项")
    @Override
    public LcpMetaComponentAttr get(Long id) {
        return lcpMetaComponentAttrDao.get(id);
    }

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    @Trace("根据主键批量查询组件属性配置项")
    @Override
    public List<LcpMetaComponentAttr> getListByIds(List<Long> list) {
        return lcpMetaComponentAttrDao.getListByIds(list);
    }

    /**
     * 插入
     *
     * @param lcpMetaComponentAttr 参数
     * @return
     */
    @Trace("插入组件属性配置项")
    @Override
    public int insert(LcpMetaComponentAttr lcpMetaComponentAttr) {
        return lcpMetaComponentAttrDao.insert(lcpMetaComponentAttr);
    }

    /**
     * 选择性插入
     *
     * @param lcpMetaComponentAttr 参数
     * @return
     */
    @Trace("选择性插入组件属性配置项")
    @Override
    public int insertSelective(LcpMetaComponentAttr lcpMetaComponentAttr) {
        return lcpMetaComponentAttrDao.insertSelective(lcpMetaComponentAttr);
    }

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量插入组件属性配置项")
    @Override
    public int batchInsert(List<LcpMetaComponentAttr> list) {
        return lcpMetaComponentAttrDao.batchInsert(list);
    }

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量选择性插入组件属性配置项")
    @Override
    public int batchInsertSelective(List<LcpMetaComponentAttr> list) {
        return lcpMetaComponentAttrDao.batchInsertSelective(list);
    }

    /**
     * 修改
     *
     * @param lcpMetaComponentAttr 参数
     * @return
     */
    @Trace("修改组件属性配置项")
    @Override
    public int update(LcpMetaComponentAttr lcpMetaComponentAttr) {
        return lcpMetaComponentAttrDao.update(lcpMetaComponentAttr);
    }

    /**
     * 选择性修改
     *
     * @param lcpMetaComponentAttr 参数
     * @return
     */
    @Trace("选择性修改组件属性配置项")
    @Override
    public int updateSelective(LcpMetaComponentAttr lcpMetaComponentAttr) {
        return lcpMetaComponentAttrDao.updateSelective(lcpMetaComponentAttr);
    }

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    @Trace("删除组件属性配置项")
    @Override
    public int delete(Long id) {
        return lcpMetaComponentAttrDao.delete(id);
    }

    /**
     * 根据组件id删除
     *
     * @param componentId
     * @return
     */
    @Trace("根据组件id删除")
    @Override
    public int deleteByComponentId(Long componentId) {
        return lcpMetaComponentAttrDao.deleteByComponentId(componentId);
    }

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    @Trace("批量删除组件属性配置项")
    @Override
    public int batchDelete(List<Long> list) {
        return lcpMetaComponentAttrDao.batchDelete(list);
    }

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param lcpMetaComponentAttr 参数
     * @return
     */
    @Trace("根据条件删除组件属性配置项")
    @Override
    public int deleteByCondition(LcpMetaComponentAttr lcpMetaComponentAttr) {
        return lcpMetaComponentAttrDao.deleteByCondition(lcpMetaComponentAttr);
    }

}
