package com.imyuanma.qingyun.lowcode.model.bo;

import lombok.Data;

import java.util.Objects;

/**
 * 查询条件项
 *
 * @author wangjy
 * @date 2022/07/29 22:55:46
 */
@Data
public class SearchConditionItem {
    /**
     * 属性名
     */
    private String fieldJavaName;
    /**
     * 字段标题
     */
    private String listHeaderTitle;
    /**
     * 条件查询类型,1无,2等于,3模糊查询,4模糊左匹配等等等等
     * @see com.imyuanma.qingyun.lowcode.model.enums.ELcpQueryConditionTypeEnum
     */
    private Integer listQueryCondition;

    public static SearchConditionItem build(GenerateFieldBO fieldBO) {
        SearchConditionItem searchConditionItem = new SearchConditionItem();
        searchConditionItem.setFieldJavaName(fieldBO.getFieldJavaName());
        searchConditionItem.setListHeaderTitle(fieldBO.getListHeaderTitle());
        searchConditionItem.setListQueryCondition(fieldBO.getListQueryCondition());
        return searchConditionItem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SearchConditionItem that = (SearchConditionItem) o;
        return Objects.equals(fieldJavaName, that.fieldJavaName) && Objects.equals(listHeaderTitle, that.listHeaderTitle) && Objects.equals(listQueryCondition, that.listQueryCondition);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fieldJavaName, listHeaderTitle, listQueryCondition);
    }
}
