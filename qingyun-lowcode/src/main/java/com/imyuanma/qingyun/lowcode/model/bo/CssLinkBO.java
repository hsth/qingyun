package com.imyuanma.qingyun.lowcode.model.bo;

import lombok.Data;

/**
 * css链接
 *
 * @author wangjy
 * @date 2022/07/29 22:31:44
 */
@Data
public class CssLinkBO {
    /**
     * 链接
     */
    private String url;

    public static CssLinkBO buildDefault() {
        CssLinkBO cssLinkBO = new CssLinkBO();
        cssLinkBO.setUrl("");
        return cssLinkBO;
    }
}
