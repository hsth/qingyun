package com.imyuanma.qingyun.lowcode.model;

import lombok.Data;
import java.util.Date;
import com.imyuanma.qingyun.interfaces.common.model.BaseDO;

/**
 * 字典表实体类
 *
 * @author YuanMaKeJi
 * @date 2022-12-04 23:42:56
 */
@Data
public class LcpDict extends BaseDO {

    /**
     * 主键
     */
    private Long id;

    /**
     * 字典编码,唯一
     */
    private String code;

    /**
     * 字典说明
     */
    private String name;

    /**
     * 类型,list:普通字典,tree:树结构字典
     */
    private String type;

    /**
     * 字典扩展属性
     */
    private String extInfo;

    /**
     * 系统内置字典标识
     */
    private String systemState;

    /**
     * 备注
     */
    private String remark;



}