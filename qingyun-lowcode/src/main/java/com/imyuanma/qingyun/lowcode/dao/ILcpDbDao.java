package com.imyuanma.qingyun.lowcode.dao;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.lowcode.model.data.LcpDbColumnDO;
import com.imyuanma.qingyun.lowcode.model.data.LcpDbTableDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 数据库元数据dao
 *
 * @author wangjy
 * @date 2022/05/04 14:39:39
 */
@Mapper
public interface ILcpDbDao {
    /**
     * 查询数据库所有表
     *
     * @return
     */
    List<LcpDbTableDO> getTableList(LcpDbTableDO lcpDbTableDO);

    /**
     * 根据表名查询表信息
     *
     * @param tableName 表名
     * @return
     */
    LcpDbTableDO getTableByTableName(String tableName);

    /**
     * 根据表名批量查询表信息
     *
     * @param tableNameList 表名集合
     * @return
     */
    List<LcpDbTableDO> getTableListByTableName(List<String> tableNameList);

    /**
     * 根据表名查询字段信息
     *
     * @param tableName 表名
     * @return
     */
    List<LcpDbColumnDO> getColumnListByTableName(String tableName);

    /**
     * 执行ddl语句
     *
     * @param sql
     */
    void executeDdl(String sql);

    /**
     * 执行查询sql
     *
     * @param sql
     * @return
     */
    List<Map<String, Object>> executeSearchSql(String sql);

    /**
     * 执行分页查询sql
     *
     * @param sql
     * @param pageQuery
     * @return
     */
    List<Map<String, Object>> executeSearchSql(String sql, PageQuery pageQuery);
}
