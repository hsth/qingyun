package com.imyuanma.qingyun.lowcode.service;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.lowcode.model.LcpDbDataSource;

import java.util.List;

/**
 * DB数据源服务
 *
 * @author YuanMaKeJi
 * @date 2023-04-22 11:18:10
 */
public interface ILcpDbDataSourceService {

    /**
     * 列表查询
     *
     * @param lcpDbDataSource 查询条件
     * @return
     */
    List<LcpDbDataSource> getList(LcpDbDataSource lcpDbDataSource);

    /**
     * 分页查询
     *
     * @param lcpDbDataSource 查询条件
     * @param pageQuery 分页参数
     * @return
     */
    List<LcpDbDataSource> getPage(LcpDbDataSource lcpDbDataSource, PageQuery pageQuery);

    /**
     * 统计数量
     *
     * @param lcpDbDataSource 查询条件
     * @return
     */
    int count(LcpDbDataSource lcpDbDataSource);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    LcpDbDataSource get(Long id);

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    List<LcpDbDataSource> getListByIds(List<Long> list);

    /**
     * 根据code查询
     *
     * @param code 数据源编码
     * @return
     */
    LcpDbDataSource getByCode(String code);

    /**
     * 根据dbType查询
     *
     * @param dbType 数据库类型
     * @return
     */
    List<LcpDbDataSource> getListByDbType(String dbType);

    /**
     * 插入
     *
     * @param lcpDbDataSource 参数
     * @return
     */
    int insert(LcpDbDataSource lcpDbDataSource);

    /**
     * 选择性插入
     *
     * @param lcpDbDataSource 参数
     * @return
     */
    int insertSelective(LcpDbDataSource lcpDbDataSource);

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    int batchInsert(List<LcpDbDataSource> list);

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    int batchInsertSelective(List<LcpDbDataSource> list);

    /**
     * 修改
     *
     * @param lcpDbDataSource 参数
     * @return
     */
    int update(LcpDbDataSource lcpDbDataSource);

    /**
     * 选择性修改
     *
     * @param lcpDbDataSource 参数
     * @return
     */
    int updateSelective(LcpDbDataSource lcpDbDataSource);

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    int delete(Long id);

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    int batchDelete(List<Long> list);

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param lcpDbDataSource 参数
     * @return
     */
    int deleteByCondition(LcpDbDataSource lcpDbDataSource);

}
