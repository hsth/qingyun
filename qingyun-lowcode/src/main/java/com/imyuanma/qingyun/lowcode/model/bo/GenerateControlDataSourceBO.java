package com.imyuanma.qingyun.lowcode.model.bo;

import com.imyuanma.qingyun.lowcode.model.enums.ELcpFormItemDataSourceTypeEnum;
import lombok.Data;

import java.util.List;

/**
 * 表单控件数据源配置
 *
 * @author wangjy
 * @date 2022/05/04 17:47:43
 */
@Data
public class GenerateControlDataSourceBO {
    /**
     * 数据类型
     * no,static,url,dict
     * @see ELcpFormItemDataSourceTypeEnum
     */
    private String dataType;
    /**
     * 字典code
     */
    private String dictCode;
    /**
     * 数据查询地址
     */
    private String dataUrl;
    /**
     * 值字段
     */
    private String keyField;
    /**
     * 文本字段
     */
    private String valueField;
    /**
     * static静态数据选项集合
     */
    private List<OptionItem> optionItemList;

    /**
     * 数据源配置是否有效
     * @return
     */
    public boolean isValid() {
        ELcpFormItemDataSourceTypeEnum typeEnum = ELcpFormItemDataSourceTypeEnum.of(this.dataType);
        return typeEnum != null && typeEnum != ELcpFormItemDataSourceTypeEnum.NO;
    }

}
