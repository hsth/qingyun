package com.imyuanma.qingyun.lowcode.model.bo.ui;

import lombok.Data;

import java.util.List;

/**
 * 低代码页面配置
 *
 * @author wangjy
 * @date 2022/08/18 23:53:19
 */
@Data
public class LcpPageConfig {
    /**
     * 元素集合
     */
    private List<LcpPageElementBO> elementList;
}
