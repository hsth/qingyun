package com.imyuanma.qingyun.lowcode.model.enums;

/**
 * 按钮样式类型
 *
 * @author wangjy
 * @date 2022/07/03 14:04:58
 */
public enum ELcpButtonCategoryEnum {
    DEFAULT("","默认按钮"),
    PLAIN("plain","朴素按钮"),
    LINK("link","链接按钮"),
    TEXT("text","文字按钮"),
    ROUND("round","圆角按钮"),
    CIRCLE("circle","圆形按钮"),
    ;

    private String type;
    private String text;

    ELcpButtonCategoryEnum(String type, String text) {
        this.type = type;
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public String getText() {
        return text;
    }
}
