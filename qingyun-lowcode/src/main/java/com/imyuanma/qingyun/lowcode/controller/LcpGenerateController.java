package com.imyuanma.qingyun.lowcode.controller;

import com.imyuanma.qingyun.common.model.request.WebRequest;
import com.imyuanma.qingyun.common.model.response.Page;
import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.common.util.*;
import com.imyuanma.qingyun.common.util.io.FileDownloadUtil;
import com.imyuanma.qingyun.interfaces.common.model.vo.KeyValueVO;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.lowcode.dao.ILcpGenerateFieldDao;
import com.imyuanma.qingyun.lowcode.dao.ILcpGenerateMainDao;
import com.imyuanma.qingyun.lowcode.model.bo.GenerateMainBO;
import com.imyuanma.qingyun.lowcode.model.data.LcpGenerateFieldDO;
import com.imyuanma.qingyun.lowcode.model.data.LcpGenerateMainDO;
import com.imyuanma.qingyun.lowcode.model.dto.LcpGenerateExportDTO;
import com.imyuanma.qingyun.lowcode.model.enums.*;
import com.imyuanma.qingyun.lowcode.model.vo.LcpGenerateCodeVO;
import com.imyuanma.qingyun.lowcode.service.ILcpGenerateService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


/**
 * 代码生成web
 *
 * @author wangjy
 * @date 2022/05/04 16:12:37
 */
@RestController
@RequestMapping(value = "/lowcode/generate", method = {RequestMethod.POST})
public class LcpGenerateController {
    /**
     * 代码生成服务
     */
    @Autowired
    private ILcpGenerateService lcpGenerateService;
    /**
     * 代码生成dao
     */
    @Autowired
    private ILcpGenerateMainDao generateMainDao;
    /**
     * 代码生成字段配置dao
     */
    @Autowired
    private ILcpGenerateFieldDao generateFieldDao;

    /**
     * 生成代码
     *
     * @param webRequest
     * @return
     */
    @Trace("生成代码")
    @RequestMapping(value = "/generateCode")
    public Result generate(@RequestBody WebRequest<LcpGenerateCodeVO> webRequest) {
        LcpGenerateCodeVO lcpGenerateCodeVO = webRequest.getBody();
        AssertUtil.notNull(lcpGenerateCodeVO, "参数无效");
        AssertUtil.notNull(lcpGenerateCodeVO.getGenerateId(), "参数无效");
        lcpGenerateService.generate(lcpGenerateCodeVO.getGenerateId(), lcpGenerateCodeVO.getCodeTemplateId());
        return Result.success();
    }

    /**
     * 初始化指定表的代码生成配置
     *
     * @param webRequest
     * @return
     */
    @Trace("初始化指定数据表的代码生成配置")
    @RequestMapping(value = "/initGenerateByTableName", method = {RequestMethod.POST})
    public Result<GenerateMainBO> initGenerateByTableName(@RequestBody WebRequest<String> webRequest) {
        return Result.success(lcpGenerateService.initGenerateByTableName(webRequest.getBody()));
    }

    /**
     * 新增代码生成配置
     *
     * @param webRequest
     * @return
     */
    @Trace("新增代码生成配置")
    @RequestMapping(value = "/addGenerateConfig", method = {RequestMethod.POST})
    public Result addGenerateConfig(@RequestBody WebRequest<GenerateMainBO> webRequest) {
        lcpGenerateService.addGenerateConfig(webRequest.getBody());
        return Result.success();
    }

    /**
     * 更新代码生成配置
     *
     * @param webRequest
     * @return
     */
    @Trace("更新代码生成配置")
    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    public Result update(@RequestBody WebRequest<GenerateMainBO> webRequest) {
        lcpGenerateService.update(webRequest.getBody());
        return Result.success();
    }

    /**
     * 查询代码生成详情
     *
     * @param webRequest
     * @return
     */
    @Trace("查询代码生成详情")
    @RequestMapping(value = "/getGenerateDetail", method = {RequestMethod.POST})
    public Result<GenerateMainBO> getGenerateDetail(@RequestBody WebRequest<Long> webRequest) {
        if (webRequest.getBody() == null) {
            return Result.error("参数无效");
        }
        return Result.success(lcpGenerateService.getGenerateDetail(webRequest.getBody()));
    }

    /**
     * 同步字段变化
     *
     * @param webRequest
     * @return
     */
    @Trace("同步字段变化")
    @RequestMapping(value = "/syncFieldChange", method = {RequestMethod.POST})
    public Result<GenerateMainBO> syncFieldChange(@RequestBody WebRequest<Long> webRequest) {
        if (webRequest.getBody() == null) {
            return Result.error("参数无效");
        }
        return Result.success(lcpGenerateService.syncFieldChange(webRequest.getBody()));
    }

//    /**
//     * 查询代码生成列表
//     * @param generateMainBO
//     * @return
//     */
//    @RequestMapping(value = "/getList", method = {RequestMethod.POST})
//    public Result<List<GenerateMainBO>> getList(@RequestBody GenerateMainBO generateMainBO) {
//        return Result.success(lcpGenerateService.getList(generateMainBO));
//    }

    /**
     * 分页查询
     *
     * @param webRequest
     * @return
     */
    @Trace("分页查询代码生成配置")
    @RequestMapping(value = "/getPage", method = {RequestMethod.POST})
    public Page<List<GenerateMainBO>> getPage(@RequestBody WebRequest<GenerateMainBO> webRequest) {
        PageQuery pageQuery = webRequest.buildPageQuery();
        return Page.success(lcpGenerateService.getPage(webRequest.getBody(), pageQuery), pageQuery);
    }

    @Trace("查询代码生成配置")
    @RequestMapping(value = "/getList", method = {RequestMethod.POST})
    public Page<List<GenerateMainBO>> getList(@RequestBody WebRequest<GenerateMainBO> webRequest) {
        return Page.success(lcpGenerateService.getList(webRequest.getBody() != null ? webRequest.getBody() : new GenerateMainBO()));
    }

    /**
     * 删除
     *
     * @param webRequest
     * @return
     */
    @Trace("删除代码生成配置")
    @RequestMapping(value = "/delete", method = {RequestMethod.POST})
    public Result delete(@RequestBody WebRequest<String> webRequest) {
        AssertUtil.notBlank(webRequest.getBody(), "参数无效");
        lcpGenerateService.delete(Arrays.stream(webRequest.getBody().split(",")).map(Long::valueOf).toArray(Long[]::new));
        return Result.success();
    }

    /**
     * 导出
     *
     * @param ids
     * @param request
     * @param response
     * @throws IOException
     */
    @Trace("导出代码生成配置")
    @RequestMapping(value = "/export", method = {RequestMethod.GET, RequestMethod.POST})
    public void export(String ids, HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (StringUtil.isBlank(ids)) {
            WebUtil.write2Response(response, Result.error("参数无效"));
            return;
        }
        List<LcpGenerateExportDTO> list = new ArrayList<>();
        // 查询配置主表
        for (String id : ids.split(",")) {
            // 查询主表
            LcpGenerateMainDO mainDO = generateMainDao.get(Long.valueOf(id));
            if (mainDO == null) {
                continue;
            }
            // 查询字段列表
            LcpGenerateFieldDO fieldQuery = new LcpGenerateFieldDO();
            fieldQuery.setGenerateId(mainDO.getId());
            List<LcpGenerateFieldDO> fieldList = generateFieldDao.getList(fieldQuery);
            if (CollectionUtil.isEmpty(fieldList)) {
                continue;
            }
            LcpGenerateExportDTO exportDTO = new LcpGenerateExportDTO();
            exportDTO.setLcpGenerateMain(mainDO);
            exportDTO.setLcpGenerateFieldList(fieldList);
            list.add(exportDTO);
        }
        String json = JsonUtil.toJson(list);
        byte[] bytes = json.getBytes("UTF-8");
        // 字符串转输入流
        FileDownloadUtil.downloadHandle(new ByteArrayInputStream(bytes), "代码生成配置.json", request, response);
    }

    // 导入
    @Trace("导入代码生成配置")
    @RequestMapping(value = "/import", method = {RequestMethod.POST})
    @ResponseBody
    public Result importConfig(MultipartFile file) {
        if (file == null) {
            return Result.error("文件无效");
        }
        try {
            String json = IOUtils.toString(file.getInputStream(), "UTF-8");
            List<LcpGenerateExportDTO> list = JsonUtil.toList(json, LcpGenerateExportDTO.class);
            if (CollectionUtil.isEmpty(list)) {
                return Result.error("文件内容无效");
            }
            lcpGenerateService.batchImport(list);
            return Result.success();
        } catch (Exception e) {
            return Result.error("导入失败");
        }
    }

    @Trace("查询字段查询类型")
    @RequestMapping(value = "/getQueryConditionTypeList", method = {RequestMethod.GET, RequestMethod.POST})
    public Result<List<KeyValueVO>> getQueryConditionTypeList() {
        return Result.success(Arrays.stream(ELcpQueryConditionTypeEnum.values()).map(item -> new KeyValueVO(item.getType(), item.getText())).collect(Collectors.toList()));
    }

    @Trace("查询表格展示格式类型")
    @RequestMapping(value = "/getShowFormatList", method = {RequestMethod.GET, RequestMethod.POST})
    public Result<List<KeyValueVO>> getShowFormatList() {
        return Result.success(Arrays.stream(ELcpShowFormatEnum.values()).map(item -> new KeyValueVO(item.getType(), item.getText())).collect(Collectors.toList()));
    }

    @Trace("查询表格首列类型")
    @RequestMapping(value = "/getFirstColumnTypeList", method = {RequestMethod.GET, RequestMethod.POST})
    public Result<List<KeyValueVO>> getFirstColumnTypeList() {
        return Result.success(Arrays.stream(ELcpTableColumnTypeEnum.values()).map(item -> new KeyValueVO(item.getType(), item.getText())).collect(Collectors.toList()));
    }

    @Trace("查询控件类型")
    @RequestMapping(value = "/getControlTypeList", method = {RequestMethod.GET, RequestMethod.POST})
    public Result<List<KeyValueVO>> getControlTypeList() {
        return Result.success(Arrays.stream(ELcpFormControlTypeEnum.values()).map(item -> new KeyValueVO(item.getType(), item.getText())).collect(Collectors.toList()));
    }

    @Trace("查询表单校验规则列表")
    @RequestMapping(value = "/getControlCheckList", method = {RequestMethod.GET, RequestMethod.POST})
    public Result<List<KeyValueVO>> getControlCheckList() {
        return Result.success(Arrays.stream(ELcpFormControlCheckEnum.values()).map(item -> new KeyValueVO(item.getType(), item.getText())).collect(Collectors.toList()));
    }

    @Trace("查询服务域名列表")
    @RequestMapping(value = "/getDomainList", method = {RequestMethod.GET, RequestMethod.POST})
    public Result<List<KeyValueVO>> getDomainList() throws Exception {
        List<KeyValueVO> list = new ArrayList<>();
        String[] domainList = {"/", "{contextPath}", "{URL_UMS}", "{URL_FS}", "{URL_CMS}", "{URL_PORTAL}", "{URL_MONITOR}", "{URL_CONFIG}"};
        for (String domain : domainList) {
            list.add(new KeyValueVO(domain, domain));
        }
        return Result.success(list);
    }

    @Trace("查询表单项显隐类型")
    @RequestMapping(value = "/getShowTypeList", method = {RequestMethod.GET, RequestMethod.POST})
    public Result<List<KeyValueVO>> getShowTypeList() {
        return Result.success(Arrays.stream(ELcpFormItemShowTypeEnum.values()).map(item -> new KeyValueVO(item.getType(), item.getText())).collect(Collectors.toList()));
    }

    @Trace("查询表单项禁用类型")
    @RequestMapping(value = "/getDisableTypeList", method = {RequestMethod.GET, RequestMethod.POST})
    public Result<List<KeyValueVO>> getDisableTypeList() {
        return Result.success(Arrays.stream(ELcpFormItemDisableTypeEnum.values()).map(item -> new KeyValueVO(item.getType(), item.getText())).collect(Collectors.toList()));
    }

    @Trace("查询表格行按钮枚举")
    @RequestMapping(value = "/getTableRowBtnList", method = {RequestMethod.GET, RequestMethod.POST})
    public Result<List<KeyValueVO>> getTableRowBtnList() {
        return Result.success(Arrays.stream(ELcpTableRowBtnEnum.values()).map(item -> new KeyValueVO(item.getType(), item.getText())).collect(Collectors.toList()));
    }

    @Trace("查询按钮颜色样式")
    @RequestMapping(value = "/getButtonTypeList", method = {RequestMethod.GET, RequestMethod.POST})
    public Result<List<KeyValueVO>> getButtonTypeList() {
        return Result.success(Arrays.stream(ELcpButtonTypeEnum.values()).map(item -> new KeyValueVO(item.getType(), item.getText())).collect(Collectors.toList()));
    }

    @Trace("查询按钮样式类型")
    @RequestMapping(value = "/getButtonCategoryList", method = {RequestMethod.GET, RequestMethod.POST})
    public Result<List<KeyValueVO>> getButtonCategoryList() {
        return Result.success(Arrays.stream(ELcpButtonCategoryEnum.values()).map(item -> new KeyValueVO(item.getType(), item.getText())).collect(Collectors.toList()));
    }
}
