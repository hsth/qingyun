package com.imyuanma.qingyun.lowcode.model;

import lombok.Data;
import java.util.Date;
import com.imyuanma.qingyun.interfaces.common.model.BaseDO;

/**
 * 字典数据项实体类
 *
 * @author YuanMaKeJi
 * @date 2022-12-09 23:09:04
 */
@Data
public class LcpDictItem extends BaseDO {

    /**
     * 主键
     */
    private Long id;

    /**
     * 父节点code
     */
    private String parentCode;

    /**
     * 编码
     */
    private String code;

    /**
     * 说明
     */
    private String name;

    /**
     * 字典值
     */
    private String val;

    /**
     * 状态,yes:可用,no:禁用
     */
    private String status;

    /**
     * 扩展属性
     */
    private String extInfo;

    /**
     * 样式
     */
    private String style;

    /**
     * 字典编码
     */
    private String dictCode;

    /**
     * 备注
     */
    private String remark;



}