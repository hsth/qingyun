package com.imyuanma.qingyun.lowcode.core.jdbc.mapper;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * jdbc返回值转java值
 *
 * @author wangjy
 * @date 2023/04/21 22:43:03
 */
public class JdbcTypeToJavaTypeUtil {
    /**
     * jdbc返回值转java属性类型
     *
     * @param value
     * @param fieldType
     * @return
     */
    public static Object convertJdbcValueToJavaType(Object value, Class<?> fieldType) {
        if (value == null) {
            return null;
        }
        if (fieldType.isAssignableFrom(value.getClass())) {
            return value;
        }
        if (fieldType.equals(String.class)) {
            return value.toString();
        } else if (fieldType.equals(Integer.class) || fieldType.equals(int.class)) {
            return Integer.parseInt(value.toString());
        } else if (fieldType.equals(Double.class) || fieldType.equals(double.class)) {
            return Double.parseDouble(value.toString());
        } else if (fieldType.equals(Float.class) || fieldType.equals(float.class)) {
            return Float.parseFloat(value.toString());
        } else if (fieldType.equals(Long.class) || fieldType.equals(long.class)) {
            return Long.parseLong(value.toString());
        } else if (fieldType.equals(Date.class)) {
            if (value instanceof LocalDateTime) {
                LocalDateTime ldt = (LocalDateTime) value;
                return Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
            } else if (value instanceof LocalDate) {
                LocalDate ld = (LocalDate) value;
                return Date.from(ld.atStartOfDay(ZoneId.systemDefault()).toInstant());
            }
            return value;
        } else if (fieldType.equals(LocalDateTime.class)) {
            if (value instanceof LocalDateTime) {
                return value;
            } else if (value instanceof LocalDate) {
                LocalDate ld = (LocalDate) value;
                return ld.atStartOfDay();
            } else if (value instanceof Date) {
                Date date = (Date) value;
                return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
            }
        }
        throw new RuntimeException("不支持的数据类型:" + fieldType.getName());
    }
}
