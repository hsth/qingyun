package com.imyuanma.qingyun.lowcode.controller;

import com.imyuanma.qingyun.common.model.request.WebRequest;
import com.imyuanma.qingyun.common.model.response.Page;
import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.lowcode.model.data.LcpTemplateGroup;
import com.imyuanma.qingyun.lowcode.service.ILcpTemplateGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 代码模板组web
 *
 * @author YuanMaKeJi
 * @date 2022-06-29 22:53:35
 */
@RestController
@RequestMapping(value = "/lowcode/lcpTemplateGroup", method = {RequestMethod.POST})
public class LcpTemplateGroupController {

    /**
     * 代码模板组服务
     */
    @Autowired
    private ILcpTemplateGroupService lcpTemplateGroupService;

    /**
     * 列表查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @RequestMapping("/getList")
    public Result<List<LcpTemplateGroup>> getList(@RequestBody WebRequest<LcpTemplateGroup> webRequest) {
        return Result.success(lcpTemplateGroupService.getList(webRequest.getBody()));
    }

    /**
     * 分页查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @RequestMapping("/getPage")
    public Page<List<LcpTemplateGroup>> getPage(@RequestBody WebRequest<LcpTemplateGroup> webRequest) {
        PageQuery pageQuery = webRequest.buildPageQuery();
        List<LcpTemplateGroup> list = lcpTemplateGroupService.getPage(webRequest.getBody(), pageQuery);
        return Page.success(list, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param webRequest 查询条件
     * @return
     */
    @RequestMapping("/count")
    public Result<Integer> count(@RequestBody WebRequest<LcpTemplateGroup> webRequest) {
        return Result.success(lcpTemplateGroupService.count(webRequest.getBody()));
    }

    /**
     * 查询
     *
     * @param webRequest 参数
     * @return
     */
    @RequestMapping("/get")
    public Result<LcpTemplateGroup> get(@RequestBody WebRequest<Long> webRequest) {
        AssertUtil.notNull(webRequest.getBody());
        return Result.success(lcpTemplateGroupService.get(webRequest.getBody()));
    }

    /**
     * 详情
     * @param webRequest
     * @return
     */
    @RequestMapping("/detail")
    public Result<LcpTemplateGroup> detail(@RequestBody WebRequest<Long> webRequest) {
        AssertUtil.notNull(webRequest.getBody());
        return Result.success(lcpTemplateGroupService.detail(webRequest.getBody()));
    }

    /**
     * 新增
     *
     * @param webRequest 参数
     * @return
     */
    @RequestMapping("/insert")
    public Result insert(@RequestBody WebRequest<LcpTemplateGroup> webRequest) {
        LcpTemplateGroup lcpTemplateGroup = webRequest.getBody();
        AssertUtil.notNull(lcpTemplateGroup);
        lcpTemplateGroupService.insertSelective(lcpTemplateGroup);
        return Result.success();
    }

    /**
     * 修改
     *
     * @param webRequest 参数
     * @return
     */
    @RequestMapping("/update")
    public Result update(@RequestBody WebRequest<LcpTemplateGroup> webRequest) {
        LcpTemplateGroup lcpTemplateGroup = webRequest.getBody();
        AssertUtil.notNull(lcpTemplateGroup);
        AssertUtil.notNull(lcpTemplateGroup.getId());
        lcpTemplateGroupService.updateSelective(lcpTemplateGroup);
        return Result.success();
    }

    /**
     * 删除
     *
     * @param webRequest 参数, 待删除主键,多个使用英文逗号拼接
     * @return
     */
    @RequestMapping("/delete")
    public Result delete(@RequestBody WebRequest<String> webRequest) {
        String ids = webRequest.getBody();
        AssertUtil.notBlank(ids);
        if (ids.contains(",")) {
            lcpTemplateGroupService.batchDelete(Arrays.stream(ids.split(",")).map(Long::valueOf).collect(Collectors.toList()));
        } else {
            lcpTemplateGroupService.delete(Long.valueOf(ids));
        }
        return Result.success();
    }

}
