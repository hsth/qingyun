package com.imyuanma.qingyun.lowcode.model.bo;

import lombok.Data;

import java.util.List;

/**
 * 表单项校验规则
 *
 * @author wangjy
 * @date 2022/05/04 17:48:20
 */
@Data
public class GenerateControlCheckRuleBO {
    /**
     * 表单校验
     */
    private List<CheckRuleItem> checkRuleList;
}
