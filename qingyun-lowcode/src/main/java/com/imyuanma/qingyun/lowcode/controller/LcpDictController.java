package com.imyuanma.qingyun.lowcode.controller;

import com.imyuanma.qingyun.common.factory.BaseDOBuilder;
import com.imyuanma.qingyun.common.model.request.WebRequest;
import com.imyuanma.qingyun.common.model.response.Page;
import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.lowcode.model.LcpDict;
import com.imyuanma.qingyun.lowcode.service.ILcpDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 字典表web
 *
 * @author YuanMaKeJi
 * @date 2022-12-04 23:42:56
 */
@RestController
@RequestMapping(value = "/lowcode/lcpDict")
public class LcpDictController {

    /**
     * 字典表服务
     */
    @Autowired
    private ILcpDictService lcpDictService;

    /**
     * 列表查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("查询字典表列表")
    @PostMapping("/getList")
    public Result<List<LcpDict>> getList(@RequestBody WebRequest<LcpDict> webRequest) {
        return Result.success(lcpDictService.getList(webRequest.getBody()));
    }

    /**
     * 分页查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("分页查询字典表")
    @PostMapping("/getPage")
    public Page<List<LcpDict>> getPage(@RequestBody WebRequest<LcpDict> webRequest) {
        PageQuery pageQuery = webRequest.buildPageQuery();
        List<LcpDict> list = lcpDictService.getPage(webRequest.getBody(), pageQuery);
        return Page.success(list, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("统计字典表数量")
    @PostMapping("/count")
    public Result<Integer> count(@RequestBody WebRequest<LcpDict> webRequest) {
        return Result.success(lcpDictService.count(webRequest.getBody()));
    }

    /**
     * 查询
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("查询字典表")
    @PostMapping("/get")
    public Result<LcpDict> get(@RequestBody WebRequest<Long> webRequest) {
        AssertUtil.notNull(webRequest.getBody());
        return Result.success(lcpDictService.get(webRequest.getBody()));
    }

    /**
     * 新增
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("新增字典表")
    @PostMapping("/insert")
    public Result insert(@RequestBody WebRequest<LcpDict> webRequest) {
        LcpDict lcpDict = webRequest.getBody();
        AssertUtil.notNull(lcpDict);
        BaseDOBuilder.fillBaseDOForInsert(lcpDict);
        lcpDictService.insertSelective(lcpDict);
        return Result.success();
    }

    /**
     * 修改
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("修改字典表")
    @PostMapping("/update")
    public Result update(@RequestBody WebRequest<LcpDict> webRequest) {
        LcpDict lcpDict = webRequest.getBody();
        AssertUtil.notNull(lcpDict);
        AssertUtil.notNull(lcpDict.getId());
        BaseDOBuilder.fillBaseDOForUpdate(lcpDict);
        lcpDictService.updateSelective(lcpDict);
        return Result.success();
    }

    /**
     * 删除
     *
     * @param webRequest 参数, 待删除主键,多个使用英文逗号拼接
     * @return
     */
    @Trace("删除字典表")
    @PostMapping("/delete")
    public Result delete(@RequestBody WebRequest<String> webRequest) {
        String ids = webRequest.getBody();
        AssertUtil.notBlank(ids);
        if (ids.contains(",")) {
            lcpDictService.batchDelete(Arrays.stream(ids.split(",")).map(Long::valueOf).collect(Collectors.toList()));
        } else {
            lcpDictService.delete(Long.valueOf(ids));
        }
        return Result.success();
    }

}
