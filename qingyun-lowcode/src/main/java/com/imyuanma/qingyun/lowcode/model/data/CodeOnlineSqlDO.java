package com.imyuanma.qingyun.lowcode.model.data;

import lombok.Data;

import java.util.List;

/**
 * 在线服务sql对象
 *
 * @author wangjy
 * @date 2022/07/01 21:47:21
 */
@Data
public class CodeOnlineSqlDO {
    /**
     * 表名
     */
    private String tableName;
    /**
     * 主键字段名
     */
    private String primaryKeyName;
    /**
     * 主键值
     */
    private Object primaryKeyValue;
    /**
     * 主键值集合
     */
    private List<Object> primaryKeyValueList;
    /**
     * 字段和值集合
     * 待查询字段, 插入字段, 更新字段等场景
     */
    private List<FieldAndValueDO> fieldAndValueList;
    /**
     * 查询条件
     */
    private List<FieldAndValueDO> conditionList;
    /**
     * 排序字段, 例如: id
     */
    private String dbSortBy;
    /**
     * 排序类型,升序asc/降序desc
     */
    private String dbSortType;
}
