package com.imyuanma.qingyun.lowcode.model.bo;

import com.imyuanma.qingyun.lowcode.model.enums.ELcpButtonCategoryEnum;
import com.imyuanma.qingyun.lowcode.model.enums.ELcpButtonTypeEnum;
import com.imyuanma.qingyun.lowcode.model.enums.ELcpInnerButtonEnum;
import lombok.Data;

/**
 * 按钮配置
 *
 * @author wangjy
 * @date 2022/07/03 12:16:32
 */
@Data
public class LcpButtonBO {
    /**
     * 按钮编码
     */
    private String code;
    /**
     * 按钮名称
     */
    private String name;
    /**
     * 图标
     */
    private String icon;
    /**
     * 按钮大小
     * default,large,small
     */
    private String size;
    /**
     * 按钮样式
     * '',primary,success,info,warning,danger
     *
     * @see ELcpButtonTypeEnum
     */
    private String type;
    /**
     * 按钮分类
     * '',plain,text,link,round,circle
     *
     * @see ELcpButtonCategoryEnum
     */
    private String category;
    /**
     * 展示规则
     */
    private String showRule;
    /**
     * 点击事件方法名
     */
    private String clickFunctionName;
    /**
     * 点击事件脚本
     */
    private String clickScript;


    /**
     * 构造按钮对象
     *
     * @param buttonEnum 内置按钮枚举
     * @return
     */
    public static LcpButtonBO of(ELcpInnerButtonEnum buttonEnum) {
        LcpButtonBO lcpButtonBO = new LcpButtonBO();
        lcpButtonBO.setCode(buttonEnum.getCode());
        lcpButtonBO.setName(buttonEnum.getName());
        lcpButtonBO.setIcon(buttonEnum.getIcon());
        lcpButtonBO.setSize(buttonEnum.getSize());
        lcpButtonBO.setType(buttonEnum.getType());
        lcpButtonBO.setCategory(buttonEnum.getCategory());
        lcpButtonBO.setShowRule(buttonEnum.getShowRule());
        lcpButtonBO.setClickFunctionName(buttonEnum.getClickFunctionName());
        lcpButtonBO.setClickScript(buttonEnum.getClickScript());
        return lcpButtonBO;
    }

}
