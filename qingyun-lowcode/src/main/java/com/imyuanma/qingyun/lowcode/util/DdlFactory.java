package com.imyuanma.qingyun.lowcode.util;

import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.common.util.StringUtil;
import com.imyuanma.qingyun.interfaces.common.model.enums.EYesOrNoEnum;
import com.imyuanma.qingyun.lowcode.model.enums.ELcpColumnTypeEnum;
import com.imyuanma.qingyun.lowcode.model.vo.LcpDbColumnVO;
import com.imyuanma.qingyun.lowcode.model.vo.LcpDbTableDetailVO;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * ddl工厂类
 *
 * @author wangjy
 * @date 2023/04/27 23:18:00
 */
public class DdlFactory {
    /**
     * 构造修改字段sql
     *
     * @param columnVO
     * @return
     */
    public static String buildModifyColumnSql(LcpDbColumnVO columnVO) {
        ELcpColumnTypeEnum typeEnum = ELcpColumnTypeEnum.of(columnVO.getDataType());
        String sql = String.format("alter table %s modify column %s %s%s %s %s comment '%s'",
                columnVO.getTableName(),
                columnVO.getColumnName(),
                columnVO.getDataType(),
                columnVO.getLength() == null ? "" : "(" + columnVO.getLength() + ")",
                EYesOrNoEnum.YES.getCode().equals(columnVO.getNotNull()) ? "not null" : "",
                StringUtil.isNotBlank(columnVO.getColumnDefault()) ? "default " + (typeEnum.isStringType() ? "'" + columnVO.getColumnDefault() + "'" : columnVO.getColumnDefault()) : "",
                columnVO.getRemark()
        );
        return sql;
    }

    /**
     * 构造新增字段sql
     *
     * @param columnVO
     * @return
     */
    public static String buildAddColumnSql(LcpDbColumnVO columnVO) {
        ELcpColumnTypeEnum typeEnum = ELcpColumnTypeEnum.of(columnVO.getDataType());
        String sql = String.format("alter table %s add column %s %s%s %s %s comment '%s' %s",
                columnVO.getTableName(),
                columnVO.getColumnName(),
                columnVO.getDataType(),
                columnVO.getLength() == null ? "" : "(" + columnVO.getLength() + ")",
                EYesOrNoEnum.YES.getCode().equals(columnVO.getNotNull()) ? "not null" : "",
                StringUtil.isNotBlank(columnVO.getColumnDefault()) ? "default " + (typeEnum.isStringType() ? "'" + columnVO.getColumnDefault() + "'" : columnVO.getColumnDefault()) : "",
                columnVO.getRemark(),
                StringUtil.isNotBlank(columnVO.getAfterColumn()) ? "after " + columnVO.getAfterColumn() : ""
        );
        return sql;
    }

    /**
     * 构造建表sql
     *
     * @param tableDetailVO
     * @return
     */
    public static String buildCreateTableSql(LcpDbTableDetailVO tableDetailVO) {
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE `").append(tableDetailVO.getTableName()).append("` (");
        Set<String> primaryColumns = new HashSet<>();
        List<LcpDbColumnVO> list = tableDetailVO.getColumnList();
        for (int i = 0; i < list.size(); i++) {
            LcpDbColumnVO columnVO = list.get(i);
            ELcpColumnTypeEnum typeEnum = ELcpColumnTypeEnum.of(columnVO.getDataType());
            sb.append(String.format("`%s` %s%s %s %s %s COMMENT '%s'"
                    , columnVO.getColumnName()
                    , columnVO.getDataType()
                    , columnVO.getLength() == null ? "" : "(" + columnVO.getLength() + ")"
                    , EYesOrNoEnum.YES.getCode().equals(columnVO.getAutoIncrement()) ? "AUTO_INCREMENT" : ""
                    , EYesOrNoEnum.YES.getCode().equals(columnVO.getNotNull()) ? "not null" : ""
                    , StringUtil.isNotBlank(columnVO.getColumnDefault()) ? "default " + (typeEnum.isStringType() ? "'" + columnVO.getColumnDefault() + "'" : columnVO.getColumnDefault()) : ""
                    , columnVO.getRemark()
            ));
            sb.append(i < list.size() - 1 ? "," : "");
            // 主键字段
            if (Boolean.TRUE.equals(columnVO.getPrimaryKeyFlag())) {
                primaryColumns.add(columnVO.getColumnName());
            }
        }
        if (CollectionUtil.isNotEmpty(primaryColumns)) {
            sb.append(", PRIMARY KEY (")
                    .append(primaryColumns.stream().map(item -> "`" + item + "`").collect(Collectors.joining(",")))
                    .append(")");
        }
        sb.append(String.format(") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='%s'", tableDetailVO.getTableComment()));
        return sb.toString();
    }
}
