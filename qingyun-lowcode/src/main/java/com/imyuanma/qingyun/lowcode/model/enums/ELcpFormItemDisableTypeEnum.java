package com.imyuanma.qingyun.lowcode.model.enums;

/**
 * 表单项禁用类型
 *
 * @author wangjy
 * @date 2022/07/02 18:35:03
 */
public enum ELcpFormItemDisableTypeEnum {
    NOT_DISABLE(0, "不禁用"),
    DISABLE(1, "总是禁用"),
    ADD_DISABLE(2, "新增时禁用"),
    EDIT_DISABLE(3, "编辑时禁用"),
    CONDITION_DISABLE(4, "满足条件时禁用"),
    ;

    private Integer type;
    private String text;

    ELcpFormItemDisableTypeEnum(Integer type, String text) {
        this.type = type;
        this.text = text;
    }

    public Integer getType() {
        return type;
    }

    public String getText() {
        return text;
    }
}
