package com.imyuanma.qingyun.lowcode.model.data;

import com.imyuanma.qingyun.interfaces.common.model.BaseDO;
import lombok.Data;


/**
 * 代码生成配置
 *
 * @author wangjy
 * @date 2022/05/04 12:35:51
 */
@Data
public class LcpGenerateMainDO extends BaseDO {
    /**
     * 主键
     */
    private Long id;
    /**
     * 名称
     */
    private String name;
    /**
     * 表名
     */
    private String tableName;
    /**
     * java类名
     */
    private String className;
    /**
     * 基础包路径,例如com.imyuanma.qingyun
     */
    private String packagePath;
    /**
     * 模块名
     */
    private String moduleName;
    /**
     * 代码文件保存路径
     */
    private String savePath;
    /**
     * 作者
     */
    private String author;
    /**
     * 表单项label位置配置,left,right,top
     */
    private String formLabelPosition;
    /**
     * 表单项label宽度,例如120px
     */
    private String formLabelWidth;
    /**
     * 全局css
     */
    private String globalCss;
    /**
     * 全局js
     */
    private String globalJs;
    /**
     * 扩展配置
     */
    private String extInfo;
    /**
     * 表单页配置
     */
    private String formPageConfig;


}
