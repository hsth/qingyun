package com.imyuanma.qingyun.lowcode.core.jdbc;

import com.alibaba.druid.pool.DruidDataSource;
import com.imyuanma.qingyun.common.db.dal.EDBType;
import com.imyuanma.qingyun.common.exception.Exceptions;
import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.common.util.JsonUtil;
import com.imyuanma.qingyun.lowcode.core.jdbc.dialect.IJdbcDialectExecute;
import com.imyuanma.qingyun.lowcode.core.jdbc.dialect.MysqlJdbcExecutor;
import com.imyuanma.qingyun.lowcode.core.jdbc.dialect.OracleJdbcExecutor;
import com.imyuanma.qingyun.lowcode.core.jdbc.dialect.PostgreJdbcExecutor;
import com.imyuanma.qingyun.lowcode.model.LcpDbDataSource;
import com.imyuanma.qingyun.lowcode.model.data.LcpDbColumnDO;
import com.imyuanma.qingyun.lowcode.model.data.LcpDbTableDO;
import com.imyuanma.qingyun.lowcode.model.enums.ELcpDbDataSourceDbTypeEnum;
import com.imyuanma.qingyun.lowcode.model.vo.LcpDbColumnVO;
import com.imyuanma.qingyun.lowcode.model.vo.LcpDbTableDetailVO;
import com.imyuanma.qingyun.lowcode.service.ILcpDbDataSourceService;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 动态jdbc服务
 *
 * @author wangjy
 * @date 2023/04/16 22:43:47
 */
@Slf4j
@Service
public class DynamicJdbcServiceImpl implements IDynamicJdbcService, InitializingBean {
    /**
     * 数据源配置服务
     */
    @Autowired
    private ILcpDbDataSourceService dbDataSourceService;

    /**
     * jdbc执行器
     */
    private Map<String, IJdbcDialectExecute> jdbcDialectExecuteMap = new HashMap<>();

    /**
     * bean初始化完成后创建动态数据源
     *
     * @throws Exception
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        // 查询所有数据源配置
        List<LcpDbDataSource> list = dbDataSourceService.getList(new LcpDbDataSource());
        if (CollectionUtil.isNotEmpty(list)) {
            for (LcpDbDataSource dbDataSource : list) {
                DruidDataSource dataSource = new DruidDataSource();
                dataSource.setUrl(dbDataSource.getDbUrl());
                dataSource.setUsername(dbDataSource.getUsername());
                dataSource.setPassword(dbDataSource.getPassword());
                dataSource.setInitialSize(0);
                dataSource.setMaxActive(10);
                dataSource.setMaxWait(5000);
                dataSource.setValidationQuery("SELECT 'X'");
                dataSource.setTestWhileIdle(true);
                dataSource.setTestOnBorrow(false);
                dataSource.setTestOnReturn(false);
                dataSource.setTimeBetweenEvictionRunsMillis(60000);
                IJdbcDialectExecute jdbcDialectExecute = null;
                if (ELcpDbDataSourceDbTypeEnum.CODE_MYSQL.getCode().equals(dbDataSource.getDbType())) {
                    jdbcDialectExecute = new MysqlJdbcExecutor(EDBType.MYSQL, dataSource);
                } else if (ELcpDbDataSourceDbTypeEnum.CODE_ORACLE.getCode().equals(dbDataSource.getDbType())) {
                    jdbcDialectExecute = new OracleJdbcExecutor(EDBType.ORACLE, dataSource);
                } else if (ELcpDbDataSourceDbTypeEnum.CODE_POSTGRESQL.getCode().equals(dbDataSource.getDbType())) {
                    jdbcDialectExecute = new PostgreJdbcExecutor(EDBType.POSTGRESQL, dataSource);
                } else {
                    log.error("[动态数据源] 未知的数据源类型={}", JsonUtil.toJson(dbDataSource));
                }
                if (jdbcDialectExecute != null) {
                    jdbcDialectExecuteMap.put(dbDataSource.getCode(), jdbcDialectExecute);
                    log.info("[动态数据源] 成功加载数据源[{}]:{}", dbDataSource.getCode(), dbDataSource.getName());
                }
            }
        }
    }

    /**
     * 获取jdbc执行器
     *
     * @param dataSourceCode 数据源code
     * @return
     */
    @Override
    public IJdbcDialectExecute getJdbcDialectExecute(String dataSourceCode) {
        IJdbcDialectExecute jdbcDialectExecute = jdbcDialectExecuteMap.get(dataSourceCode);
        if (jdbcDialectExecute == null) {
            throw Exceptions.paramException("数据源[" + dataSourceCode + "]查找失败");
        }
        return jdbcDialectExecute;
    }

    /**
     * 列表查询
     *
     * @param dataSourceCode 数据源code
     * @param sql            查询sql
     * @param params         查询参数
     * @return
     */
    @Override
    public List<Map<String, Object>> selectList(String dataSourceCode, String sql, Object... params) {
        return this.getJdbcDialectExecute(dataSourceCode).selectList(sql, params);
    }

    /**
     * 分页查询
     *
     * @param dataSourceCode 数据源code
     * @param sql            查询sql
     * @param pageQuery      分页条件
     * @param params         查询参数
     * @return
     */
    @Override
    public List<Map<String, Object>> selectPage(String dataSourceCode, String sql, PageQuery pageQuery, Object... params) {
        return this.getJdbcDialectExecute(dataSourceCode).selectPage(sql, pageQuery, params);
    }

    /**
     * 插入
     *
     * @param dataSourceCode
     * @param sql
     * @param params
     * @return
     */
    @Override
    public int insert(String dataSourceCode, String sql, Object... params) {
        return this.getJdbcDialectExecute(dataSourceCode).insert(sql, params);
    }

    /**
     * 修改
     *
     * @param dataSourceCode
     * @param sql
     * @param params
     * @return
     */
    @Override
    public int update(String dataSourceCode, String sql, Object... params) {
        return this.getJdbcDialectExecute(dataSourceCode).update(sql, params);
    }

    /**
     * 删除
     *
     * @param dataSourceCode
     * @param sql
     * @param params
     * @return
     */
    @Override
    public int delete(String dataSourceCode, String sql, Object... params) {
        return this.getJdbcDialectExecute(dataSourceCode).delete(sql, params);
    }

    /**
     * 查询所有表
     *
     * @param dataSourceCode
     * @param lcpDbTableDO
     * @return
     */
    @Override
    public List<LcpDbTableDO> getTableList(String dataSourceCode, LcpDbTableDO lcpDbTableDO) {
        return this.getJdbcDialectExecute(dataSourceCode).getTableList(lcpDbTableDO);
    }

    /**
     * 根据表名查询表
     *
     * @param dataSourceCode
     * @param tableName
     * @return
     */
    @Override
    public LcpDbTableDO getTableByTableName(String dataSourceCode, String tableName) {
        return this.getJdbcDialectExecute(dataSourceCode).getTableByTableName(tableName);
    }

    /**
     * 根据表名批量查询表
     *
     * @param dataSourceCode
     * @param tableNameList
     * @return
     */
    @Override
    public List<LcpDbTableDO> getTableListByTableName(String dataSourceCode, List<String> tableNameList) {
        return this.getJdbcDialectExecute(dataSourceCode).getTableListByTableName(tableNameList);
    }

    /**
     * 查询表字段信息
     *
     * @param dataSourceCode
     * @param tableName
     * @return
     */
    @Override
    public List<LcpDbColumnDO> getColumnListByTableName(String dataSourceCode, String tableName) {
        return this.getJdbcDialectExecute(dataSourceCode).getColumnListByTableName(tableName);
    }

    /**
     * 修改字段
     *
     * @param dataSourceCode
     * @param columnVO
     */
    @Override
    public void modifyDbColumn(String dataSourceCode, LcpDbColumnVO columnVO) {
        this.getJdbcDialectExecute(dataSourceCode).modifyDbColumn(columnVO);
    }

    /**
     * 新增字段
     *
     * @param dataSourceCode
     * @param columnVO
     */
    @Override
    public void addDbColumn(String dataSourceCode, LcpDbColumnVO columnVO) {
        this.getJdbcDialectExecute(dataSourceCode).addDbColumn(columnVO);
    }

    /**
     * 创建表
     *
     * @param dataSourceCode
     * @param tableDetailVO
     */
    @Override
    public void createTable(String dataSourceCode, LcpDbTableDetailVO tableDetailVO) {
        this.getJdbcDialectExecute(dataSourceCode).createTable(tableDetailVO);
    }
}
