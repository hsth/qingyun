package com.imyuanma.qingyun.lowcode.util;

import com.imyuanma.qingyun.common.exception.Exceptions;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Map;

/**
 * ftl模板文件工具类
 *
 * @author wangjy
 * @date 2023/04/16 16:03:33
 */
public class FtlTemplateUtil {
    private static final Logger logger = LoggerFactory.getLogger(FtlTemplateUtil.class);

    /**
     * 构造freemarker模板对象
     *
     * @param templatePath 模板路径,例如:/com/imyuanma/qingyun/lowcode/template/v4/
     * @param templateName 模板名称,例如:Model.ftl
     * @return
     * @throws IOException
     */
    public static Template buildTemplateForTemplateFile(String templatePath, String templateName) {
        Configuration configuration = new Configuration();
        configuration.setClassForTemplateLoading(FtlTemplateUtil.class, templatePath);
        configuration.setDefaultEncoding("UTF-8");
        // 去掉数字逗号, 最多支持8位小数
        configuration.setNumberFormat("#.########");
        try {
            return configuration.getTemplate(templateName, "UTF-8");
        } catch (IOException e) {
            logger.error("[构造freemarker模板对象] 创建模板对象失败", e);
            throw Exceptions.baseException("创建模板对象失败");
        }
    }

    /**
     * 模板生成写入http response
     *
     * @param template 模板
     * @param param    参数
     * @param response 响应
     */
    public static void writeResponseByTemplate(Template template, Map<String, Object> param, HttpServletResponse response) {
        Writer fw = null;
        try {
            fw = new BufferedWriter(response.getWriter());
            template.process(param, fw);
        } catch (TemplateException | IOException e) {
            logger.error("[模板生成写入response] 模板写入发生异常", e);
            throw Exceptions.baseException("模板生成写入失败");
        } finally {
            if (fw != null) {
                try {
                    fw.flush();
                    fw.close();
                } catch (IOException e) {
                    logger.error("[模板生成写入response] 关闭输出流发生异常", e);
                }
            }
        }
    }
}
