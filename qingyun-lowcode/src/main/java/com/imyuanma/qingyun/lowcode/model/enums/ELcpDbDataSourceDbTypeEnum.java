package com.imyuanma.qingyun.lowcode.model.enums;

/**
 * 数据库类型枚举
 *
 * @author YuanMaKeJi
 * @date 2023-04-22 11:18:10
 */
public enum ELcpDbDataSourceDbTypeEnum {
    CODE_MYSQL("MYSQL", "MySQL"),
    CODE_ORACLE("ORACLE", "Oracle"),
    CODE_POSTGRESQL("POSTGRESQL", "PgSQL"),
    ;

    /**
     * code
     */
    private String code;
    /**
     * 文案
     */
    private String text;

    ELcpDbDataSourceDbTypeEnum(String code, String text) {
        this.code = code;
        this.text = text;
    }

    /**
     * 根据code获取枚举
     *
     * @param code code值
     */
    public static ELcpDbDataSourceDbTypeEnum getByCode(String code) {
        for (ELcpDbDataSourceDbTypeEnum e : ELcpDbDataSourceDbTypeEnum.values()) {
            if (e.code != null && e.code.equals(code)) {
                return e;
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
