package com.imyuanma.qingyun.lowcode.dao;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.lowcode.model.LcpDict;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 字典表dao
 *
 * @author YuanMaKeJi
 * @date 2022-12-04 23:42:56
 */
@Mapper
public interface ILcpDictDao {

    /**
     * 列表查询
     *
     * @param lcpDict 查询条件
     * @return
     */
    List<LcpDict> getList(LcpDict lcpDict);

    /**
     * 分页查询
     *
     * @param lcpDict 查询条件
     * @param pageQuery 分页参数
     * @return
     */
    List<LcpDict> getList(LcpDict lcpDict, PageQuery pageQuery);

    /**
     * 统计数量
     *
     * @param lcpDict 查询条件
     * @return
     */
    int count(LcpDict lcpDict);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    LcpDict get(Long id);

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    List<LcpDict> getListByIds(List<Long> list);

    /**
     * 插入
     *
     * @param lcpDict 参数
     * @return
     */
    int insert(LcpDict lcpDict);

    /**
     * 选择性插入
     *
     * @param lcpDict 参数
     * @return
     */
    int insertSelective(LcpDict lcpDict);

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    int batchInsert(List<LcpDict> list);

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    int batchInsertSelective(List<LcpDict> list);

    /**
     * 修改
     *
     * @param lcpDict 参数
     * @return
     */
    int update(LcpDict lcpDict);

    /**
     * 选择性修改
     *
     * @param lcpDict 参数
     * @return
     */
    int updateSelective(LcpDict lcpDict);

    /**
     * 批量修改某字段
     *
     * @param idList      主键集合
     * @param fieldDbName 待修改的字段名
     * @param fieldValue  修改后的值
     * @return
     */
    int batchUpdateColumn(@Param("idList") List<Long> idList, @Param("fieldDbName") String fieldDbName, @Param("fieldValue") Object fieldValue);

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    int delete(Long id);

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    int batchDelete(List<Long> list);

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param lcpDict 参数
     * @return
     */
    int deleteByCondition(LcpDict lcpDict);

    /**
     * 删除全部
     *
     * @return
     */
    int deleteAll();

}
