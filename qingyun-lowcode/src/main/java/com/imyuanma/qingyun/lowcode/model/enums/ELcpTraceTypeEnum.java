package com.imyuanma.qingyun.lowcode.model.enums;

/**
 * 链路追踪配置类型
 *
 * @author wangjy
 * @date 2022/10/08 11:32:43
 */
public enum ELcpTraceTypeEnum {
    NO(0, "不支持"),
    ALL(1, "支持全链路"),
    ONLY_WEB(2, "仅支持web入口"),
    ;
    private Integer code;
    private String text;

    ELcpTraceTypeEnum(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    public Integer getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
