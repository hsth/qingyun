package com.imyuanma.qingyun.lowcode.service.impl;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.lowcode.dao.ILcpTemplateItemDao;
import com.imyuanma.qingyun.lowcode.model.data.LcpTemplateItem;
import com.imyuanma.qingyun.lowcode.service.ILcpTemplateItemService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 代码模板服务
 *
 * @author YuanMaKeJi
 * @date 2022-06-29 23:12:22
 */
@Slf4j
@Service
public class LcpTemplateItemServiceImpl implements ILcpTemplateItemService {

    /**
     * 代码模板dao
     */
    @Autowired
    private ILcpTemplateItemDao lcpTemplateItemDao;

    /**
     * 列表查询
     *
     * @param lcpTemplateItem 查询条件
     * @return
     */
    @Override
    public List<LcpTemplateItem> getList(LcpTemplateItem lcpTemplateItem) {
        return lcpTemplateItemDao.getList(lcpTemplateItem);
    }

    /**
     * 分页查询
     *
     * @param lcpTemplateItem 查询条件
     * @param pageQuery 分页参数
     * @return
     */
    @Override
    public List<LcpTemplateItem> getPage(LcpTemplateItem lcpTemplateItem, PageQuery pageQuery) {
        return lcpTemplateItemDao.getList(lcpTemplateItem, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param lcpTemplateItem 查询条件
     * @return
     */
    @Override
    public int count(LcpTemplateItem lcpTemplateItem) {
        return lcpTemplateItemDao.count(lcpTemplateItem);
    }

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    @Override
    public LcpTemplateItem get(Long id) {
        return lcpTemplateItemDao.get(id);
    }

    /**
     * 根据组id查询
     *
     * @param groupId 模板组id
     * @return
     */
    @Override
    public List<LcpTemplateItem> getByGroupId(Long groupId) {
        AssertUtil.notNull(groupId);
        LcpTemplateItem templateItem = new LcpTemplateItem();
        templateItem.setTemplateGroupId(groupId);
        return lcpTemplateItemDao.getList(templateItem);
    }

    /**
     * 插入
     *
     * @param lcpTemplateItem 参数
     * @return
     */
    @Override
    public int insert(LcpTemplateItem lcpTemplateItem) {
        return lcpTemplateItemDao.insert(lcpTemplateItem);
    }

    /**
     * 选择性插入
     *
     * @param lcpTemplateItem 参数
     * @return
     */
    @Override
    public int insertSelective(LcpTemplateItem lcpTemplateItem) {
        return lcpTemplateItemDao.insertSelective(lcpTemplateItem);
    }

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    @Override
    public int batchInsert(List<LcpTemplateItem> list) {
        return lcpTemplateItemDao.batchInsert(list);
    }

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    @Override
    public int batchInsertSelective(List<LcpTemplateItem> list) {
        return lcpTemplateItemDao.batchInsertSelective(list);
    }

    /**
     * 修改
     *
     * @param lcpTemplateItem 参数
     * @return
     */
    @Override
    public int update(LcpTemplateItem lcpTemplateItem) {
        return lcpTemplateItemDao.update(lcpTemplateItem);
    }

    /**
     * 选择性修改
     *
     * @param lcpTemplateItem 参数
     * @return
     */
    @Override
    public int updateSelective(LcpTemplateItem lcpTemplateItem) {
        return lcpTemplateItemDao.updateSelective(lcpTemplateItem);
    }

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    @Override
    public int delete(Long id) {
        return lcpTemplateItemDao.delete(id);
    }

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    @Override
    public int batchDelete(List<Long> list) {
        return lcpTemplateItemDao.batchDelete(list);
    }

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param lcpTemplateItem 参数
     * @return
     */
    @Override
    public int deleteByCondition(LcpTemplateItem lcpTemplateItem) {
        return lcpTemplateItemDao.deleteByCondition(lcpTemplateItem);
    }

}
