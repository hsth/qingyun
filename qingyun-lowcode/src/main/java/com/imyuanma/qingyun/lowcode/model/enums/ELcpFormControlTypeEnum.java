package com.imyuanma.qingyun.lowcode.model.enums;

/**
 * 控件类型
 *
 * @author wangjy
 * @date 2022/03/20 18:20:29
 */
public enum ELcpFormControlTypeEnum {
    INPUT("input", "输入框(input)"),
    NUMBER("number", "数字输入框(number)"),
    TEXTAREA("textarea", "大文本框(textarea)"),
    //    ,JO_INPUT("joInput","输入下拉框(带提示的input)")
    DATE("date", "日期选择"),
    TIME("time", "时间选择"),
    SELECT("select", "下拉选择(select)"),
    TREE_SELECT("treeSelect", "树形选择(treeSelect)"),
    ICON_SELECT("iconSelect", "图标选择"),
    RADIO("radio", "单选(radio)"),
    CHECKBOX("checkbox", "多选(checkbox)"),
    SWITCH("switch", "开关"),
    UPLOAD("upload", "文件上传"),
    EDITOR_MD("editor_md", "Markdown编辑器"),
    //    ,TREE("tree","树选择(单选)")
//    ,TREES("trees","树选择(多选)")
//    ,LIST("list","列表选择(单选)")
//    ,LISTS("lists","列表选择(多选)")
    LABEL("label", "文字");

    private String type;
    private String text;

    ELcpFormControlTypeEnum(String type, String text) {
        this.type = type;
        this.text = text;
    }

    /**
     * 支持数据源配置
     * @param type
     * @return
     */
    public static boolean isSupportDataSource(String type) {
        return SELECT.type.equals(type) || TREE_SELECT.type.equals(type) || RADIO.type.equals(type) || CHECKBOX.type.equals(type);
    }

    public String getType() {
        return type;
    }

    public String getText() {
        return text;
    }
}
