package com.imyuanma.qingyun.lowcode.controller;

import com.imyuanma.qingyun.common.factory.BaseDOBuilder;
import com.imyuanma.qingyun.common.model.request.WebRequest;
import com.imyuanma.qingyun.common.model.response.Page;
import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.lowcode.model.LcpMetaComponent;
import com.imyuanma.qingyun.lowcode.model.LcpMetaComponentAttr;
import com.imyuanma.qingyun.lowcode.service.ILcpMetaComponentAttrService;
import com.imyuanma.qingyun.lowcode.service.ILcpMetaComponentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 组件元数据web
 *
 * @author YuanMaKeJi
 * @date 2022-12-03 21:44:59
 */
@RestController
@RequestMapping(value = "/lowcode/lcpMetaComponent")
public class LcpMetaComponentController {

    /**
     * 组件元数据服务
     */
    @Autowired
    private ILcpMetaComponentService lcpMetaComponentService;
    /**
     * 组件属性元数据
     */
    @Autowired
    private ILcpMetaComponentAttrService lcpMetaComponentAttrService;

    @Trace("查询组件元数据列表详情")
    @PostMapping("/getDetailList")
    public Result<List<LcpMetaComponent>> getDetailList(@RequestBody WebRequest<LcpMetaComponent> webRequest) {
        // 组件列表
        List<LcpMetaComponent> componentList = lcpMetaComponentService.getList(webRequest.getBody());
        // 查询组件配置属性信息
        if (CollectionUtil.isNotEmpty(componentList)) {
            // 所有组件属性
            List<LcpMetaComponentAttr> attrList = lcpMetaComponentAttrService.getList(new LcpMetaComponentAttr());
            if (CollectionUtil.isNotEmpty(attrList)) {
                Map<Long, List<LcpMetaComponentAttr>> compAttrMap = attrList.stream().collect(Collectors.groupingBy(LcpMetaComponentAttr::getComponentId));
                for (LcpMetaComponent component : componentList) {
                    component.setAttrMetaList(compAttrMap.get(component.getId()));
                }
            }
        }
        return Result.success(componentList);
    }


    /**
     * 列表查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("查询组件元数据列表")
    @PostMapping("/getList")
    public Result<List<LcpMetaComponent>> getList(@RequestBody WebRequest<LcpMetaComponent> webRequest) {
        return Result.success(lcpMetaComponentService.getList(webRequest.getBody()));
    }

    /**
     * 分页查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("分页查询组件元数据")
    @PostMapping("/getPage")
    public Page<List<LcpMetaComponent>> getPage(@RequestBody WebRequest<LcpMetaComponent> webRequest) {
        PageQuery pageQuery = webRequest.buildPageQuery();
        List<LcpMetaComponent> list = lcpMetaComponentService.getPage(webRequest.getBody(), pageQuery);
        return Page.success(list, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("统计组件元数据数量")
    @PostMapping("/count")
    public Result<Integer> count(@RequestBody WebRequest<LcpMetaComponent> webRequest) {
        return Result.success(lcpMetaComponentService.count(webRequest.getBody()));
    }

    /**
     * 查询
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("查询组件元数据")
    @PostMapping("/get")
    public Result<LcpMetaComponent> get(@RequestBody WebRequest<Long> webRequest) {
        AssertUtil.notNull(webRequest.getBody());
        // 组件查询
        LcpMetaComponent component = lcpMetaComponentService.get(webRequest.getBody());
        // 组件属性
        List<LcpMetaComponentAttr> attrList = lcpMetaComponentAttrService.getListByComponentId(webRequest.getBody());
        component.setAttrMetaList(attrList);
        return Result.success(component);
    }

    /**
     * 新增
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("新增组件元数据")
    @PostMapping("/insert")
    public Result insert(@RequestBody WebRequest<LcpMetaComponent> webRequest) {
        LcpMetaComponent lcpMetaComponent = webRequest.getBody();
        AssertUtil.notNull(lcpMetaComponent);
        BaseDOBuilder.fillBaseDOForInsert(lcpMetaComponent);
        if (CollectionUtil.isNotEmpty(lcpMetaComponent.getAttrMetaList())) {
            for (LcpMetaComponentAttr componentAttr : lcpMetaComponent.getAttrMetaList()) {
                BaseDOBuilder.fillBaseDOForInsert(componentAttr);
            }
        }
        lcpMetaComponentService.insertSelective(lcpMetaComponent);
        return Result.success();
    }

    /**
     * 修改
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("修改组件元数据")
    @PostMapping("/update")
    public Result update(@RequestBody WebRequest<LcpMetaComponent> webRequest) {
        LcpMetaComponent lcpMetaComponent = webRequest.getBody();
        AssertUtil.notNull(lcpMetaComponent);
        AssertUtil.notNull(lcpMetaComponent.getId());
        BaseDOBuilder.fillBaseDOForUpdate(lcpMetaComponent);
        if (CollectionUtil.isNotEmpty(lcpMetaComponent.getAttrMetaList())) {
            for (LcpMetaComponentAttr componentAttr : lcpMetaComponent.getAttrMetaList()) {
                if (componentAttr.getId() != null) {
                    BaseDOBuilder.fillBaseDOForUpdate(componentAttr);
                } else {
                    BaseDOBuilder.fillBaseDOForInsert(componentAttr);
                }
            }
        }
        lcpMetaComponentService.updateSelective(lcpMetaComponent);
        return Result.success();
    }

    /**
     * 删除
     *
     * @param webRequest 参数, 待删除主键,多个使用英文逗号拼接
     * @return
     */
    @Trace("删除组件元数据")
    @PostMapping("/delete")
    public Result delete(@RequestBody WebRequest<String> webRequest) {
        String ids = webRequest.getBody();
        AssertUtil.notBlank(ids);
        if (ids.contains(",")) {
            lcpMetaComponentService.batchDelete(Arrays.stream(ids.split(",")).map(Long::valueOf).collect(Collectors.toList()));
        } else {
            lcpMetaComponentService.delete(Long.valueOf(ids));
        }
        return Result.success();
    }

}
