package com.imyuanma.qingyun.lowcode.service;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.lowcode.model.data.LcpTemplateGroup;

import java.util.List;

/**
 * 代码模板组服务
 *
 * @author YuanMaKeJi
 * @date 2022-06-29 22:53:35
 */
public interface ILcpTemplateGroupService {

    /**
     * 列表查询
     *
     * @param lcpTemplateGroup 查询条件
     * @return
     */
    List<LcpTemplateGroup> getList(LcpTemplateGroup lcpTemplateGroup);

    /**
     * 分页查询
     *
     * @param lcpTemplateGroup 查询条件
     * @param pageQuery        分页参数
     * @return
     */
    List<LcpTemplateGroup> getPage(LcpTemplateGroup lcpTemplateGroup, PageQuery pageQuery);

    /**
     * 统计数量
     *
     * @param lcpTemplateGroup 查询条件
     * @return
     */
    int count(LcpTemplateGroup lcpTemplateGroup);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    LcpTemplateGroup get(Long id);

    /**
     * 详情
     *
     * @param id 主键
     * @return
     */
    LcpTemplateGroup detail(Long id);

    /**
     * 插入
     *
     * @param lcpTemplateGroup 参数
     * @return
     */
    int insert(LcpTemplateGroup lcpTemplateGroup);

    /**
     * 选择性插入
     *
     * @param lcpTemplateGroup 参数
     * @return
     */
    int insertSelective(LcpTemplateGroup lcpTemplateGroup);

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    int batchInsert(List<LcpTemplateGroup> list);

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    int batchInsertSelective(List<LcpTemplateGroup> list);

    /**
     * 修改
     *
     * @param lcpTemplateGroup 参数
     * @return
     */
    int update(LcpTemplateGroup lcpTemplateGroup);

    /**
     * 选择性修改
     *
     * @param lcpTemplateGroup 参数
     * @return
     */
    int updateSelective(LcpTemplateGroup lcpTemplateGroup);

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    int delete(Long id);

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    int batchDelete(List<Long> list);

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param lcpTemplateGroup 参数
     * @return
     */
    int deleteByCondition(LcpTemplateGroup lcpTemplateGroup);

}
