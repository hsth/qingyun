package com.imyuanma.qingyun.lowcode.dao;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.lowcode.model.data.LcpGenerateMainDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 代码生成dao
 *
 * @author wangjy
 * @date 2022/05/04 14:27:09
 */
@Mapper
public interface ILcpGenerateMainDao {
    /**
     * 插入
     *
     * @param lcpGenerateMainDO 主配置
     * @return
     */
    int insert(LcpGenerateMainDO lcpGenerateMainDO);

    /**
     * 更新
     * @param lcpGenerateMainDO
     * @return
     */
    int update(LcpGenerateMainDO lcpGenerateMainDO);

    /**
     * 查询列表
     * @param lcpGenerateMainDO 查询参数
     * @return
     */
    List<LcpGenerateMainDO> getList(LcpGenerateMainDO lcpGenerateMainDO);

    /**
     * 分页查询
     * @param lcpGenerateMainDO
     * @param pageQuery
     * @return
     */
    List<LcpGenerateMainDO> getList(LcpGenerateMainDO lcpGenerateMainDO, PageQuery pageQuery);

    /**
     * 查询
     * @param id 生成id
     * @return
     */
    LcpGenerateMainDO get(Long id);

    /**
     * 删除
     * @param id 生成id
     * @return
     */
    int delete(Long id);
}
