package com.imyuanma.qingyun.lowcode.dao;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.lowcode.model.LcpDictItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 字典数据项dao
 *
 * @author YuanMaKeJi
 * @date 2022-12-04 23:44:03
 */
@Mapper
public interface ILcpDictItemDao {

    /**
     * 列表查询
     *
     * @param lcpDictItem 查询条件
     * @return
     */
    List<LcpDictItem> getList(LcpDictItem lcpDictItem);

    /**
     * 分页查询
     *
     * @param lcpDictItem 查询条件
     * @param pageQuery 分页参数
     * @return
     */
    List<LcpDictItem> getList(LcpDictItem lcpDictItem, PageQuery pageQuery);

    /**
     * 统计数量
     *
     * @param lcpDictItem 查询条件
     * @return
     */
    int count(LcpDictItem lcpDictItem);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    LcpDictItem get(Long id);

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    List<LcpDictItem> getListByIds(List<Long> list);

    /**
     * 插入
     *
     * @param lcpDictItem 参数
     * @return
     */
    int insert(LcpDictItem lcpDictItem);

    /**
     * 选择性插入
     *
     * @param lcpDictItem 参数
     * @return
     */
    int insertSelective(LcpDictItem lcpDictItem);

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    int batchInsert(List<LcpDictItem> list);

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    int batchInsertSelective(List<LcpDictItem> list);

    /**
     * 修改
     *
     * @param lcpDictItem 参数
     * @return
     */
    int update(LcpDictItem lcpDictItem);

    /**
     * 选择性修改
     *
     * @param lcpDictItem 参数
     * @return
     */
    int updateSelective(LcpDictItem lcpDictItem);

    /**
     * 批量修改某字段
     *
     * @param idList      主键集合
     * @param fieldDbName 待修改的字段名
     * @param fieldValue  修改后的值
     * @return
     */
    int batchUpdateColumn(@Param("idList") List<Long> idList, @Param("fieldDbName") String fieldDbName, @Param("fieldValue") Object fieldValue);

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    int delete(Long id);

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    int batchDelete(List<Long> list);

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param lcpDictItem 参数
     * @return
     */
    int deleteByCondition(LcpDictItem lcpDictItem);

    /**
     * 删除全部
     *
     * @return
     */
    int deleteAll();

}
