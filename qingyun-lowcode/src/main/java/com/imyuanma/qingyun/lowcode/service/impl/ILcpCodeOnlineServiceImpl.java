package com.imyuanma.qingyun.lowcode.service.impl;

import com.imyuanma.qingyun.common.factory.BaseDOBuilder;
import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.common.util.JsonUtil;
import com.imyuanma.qingyun.common.util.StringUtil;
import com.imyuanma.qingyun.lowcode.dao.ILcpCodeOnlineDao;
import com.imyuanma.qingyun.lowcode.model.bo.GenerateMainBO;
import com.imyuanma.qingyun.lowcode.model.data.CodeOnlineSqlDO;
import com.imyuanma.qingyun.lowcode.model.data.FieldAndValueDO;
import com.imyuanma.qingyun.lowcode.model.enums.ELcpQueryConditionTypeEnum;
import com.imyuanma.qingyun.lowcode.service.ILcpCodeOnlineService;
import com.imyuanma.qingyun.lowcode.service.ILcpGenerateService;
import com.imyuanma.qingyun.lowcode.util.CodeOnlineUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 低代码在线服务
 *
 * @author wangjy
 * @date 2022/07/01 21:29:48
 */
@Slf4j
@Service
public class ILcpCodeOnlineServiceImpl implements ILcpCodeOnlineService {
    /**
     * 代码生成服务
     */
    @Autowired
    private ILcpGenerateService generateService;
    /**
     * 在线服务dao
     */
    @Autowired
    private ILcpCodeOnlineDao codeOnlineDao;

    /**
     * 分页查询
     *
     * @param generateId 代码生成配置id
     * @param condition  查询条件
     * @param pageQuery  分页参数
     * @return
     */
    @Override
    public List<Map<String, Object>> getPage(Long generateId, Map<String, Object> condition, PageQuery pageQuery) {
        // 查询代码生成配置详情
        GenerateMainBO generateMainBO = generateService.getGenerateDetail(generateId);
        AssertUtil.notNull(generateMainBO, "代码生成配置为空");
        // 构造查询参数
        CodeOnlineSqlDO codeOnlineSql = CodeOnlineUtil.buildListQuerySql(generateMainBO, condition);
        log.info("[代码在线服务-分页查询] 查询语句构造结果={}", JsonUtil.toJson(codeOnlineSql));
        // 分页查询
        return codeOnlineDao.getList(codeOnlineSql, pageQuery);
    }

    /**
     * 列表查询
     *
     * @param generateId 代码生成配置id
     * @param condition  查询条件
     * @return
     */
    @Override
    public List<Map<String, Object>> getList(Long generateId, Map<String, Object> condition) {
        // 查询代码生成配置详情
        GenerateMainBO generateMainBO = generateService.getGenerateDetail(generateId);
        AssertUtil.notNull(generateMainBO, "代码生成配置为空");
        // 构造查询参数
        CodeOnlineSqlDO codeOnlineSql = CodeOnlineUtil.buildListQuerySql(generateMainBO, condition);
        log.info("[代码在线服务-列表查询] 查询语句构造结果={}", JsonUtil.toJson(codeOnlineSql));
        // 分页查询
        return codeOnlineDao.getList(codeOnlineSql);
    }

    /**
     * 主键查询
     *
     * @param generateId 代码生成配置id
     * @param primaryId  主键值
     * @return
     */
    @Override
    public Map<String, Object> get(Long generateId, Object primaryId) {
        // 查询代码生成配置详情
        GenerateMainBO generateMainBO = generateService.getGenerateDetail(generateId);
        AssertUtil.notNull(generateMainBO, "代码生成配置为空");
        // 构造查询参数
        CodeOnlineSqlDO codeOnlineSql = CodeOnlineUtil.buildGetOneSql(generateMainBO, primaryId);
        log.info("[代码在线服务-主键查询] 主键查询语句构造结果={}", JsonUtil.toJson(codeOnlineSql));
        // 查询
        return codeOnlineDao.get(codeOnlineSql);
    }

    /**
     * 选择性插入
     *
     * @param generateId 代码生成配置id
     * @param condition  参数
     * @return
     */
    @Override
    public int insertSelective(Long generateId, Map<String, Object> condition) {
        // 查询代码生成配置详情
        GenerateMainBO generateMainBO = generateService.getGenerateDetail(generateId);
        AssertUtil.notNull(generateMainBO, "代码生成配置为空");
        // 填充属性
        if (generateMainBO.isExtendsBaseDO()) {
            BaseDOBuilder.fillMapForInsert(condition);
        }
        // 构造插入参数
        CodeOnlineSqlDO codeOnlineSql = CodeOnlineUtil.buildInsertSelectiveSql(generateMainBO, condition);
        log.info("[代码在线服务-选择性插入] 插入语句构造结果={}", JsonUtil.toJson(codeOnlineSql));
        // 插入
        return codeOnlineDao.insertSelective(codeOnlineSql);
    }

    /**
     * 选择性修改
     *
     * @param generateId 代码生成配置id
     * @param condition  参数
     * @return
     */
    @Override
    public int updateSelective(Long generateId, Map<String, Object> condition) {
        // 查询代码生成配置详情
        GenerateMainBO generateMainBO = generateService.getGenerateDetail(generateId);
        AssertUtil.notNull(generateMainBO, "代码生成配置为空");
        // 填充属性
        if (generateMainBO.isExtendsBaseDO()) {
            BaseDOBuilder.fillMapForUpdate(condition);
        }
        // 构造更新参数
        CodeOnlineSqlDO codeOnlineSql = CodeOnlineUtil.buildUpdateSelectiveSql(generateMainBO, condition);
        log.info("[代码在线服务-选择性修改] 修改语句构造结果={}", JsonUtil.toJson(codeOnlineSql));
        // 插入
        return codeOnlineDao.updateSelective(codeOnlineSql);
    }

    /**
     * 删除
     *
     * @param generateId 代码生成配置id
     * @param primaryId  主键
     * @return
     */
    @Override
    public int delete(Long generateId, String primaryId) {
        // 查询代码生成配置详情
        GenerateMainBO generateMainBO = generateService.getGenerateDetail(generateId);
        AssertUtil.notNull(generateMainBO, "代码生成配置为空");
        // 构造删除参数
        CodeOnlineSqlDO codeOnlineSql = CodeOnlineUtil.buildDeleteSql(generateMainBO, primaryId);
        log.info("[代码在线服务-删除] 删除语句构造结果={}", JsonUtil.toJson(codeOnlineSql));
        return codeOnlineDao.delete(codeOnlineSql);
    }

    /**
     * 批量删除
     *
     * @param generateId 代码生成配置id
     * @param list       主键集合
     * @return
     */
    @Override
    public int batchDelete(Long generateId, List<String> list) {
        // 查询代码生成配置详情
        GenerateMainBO generateMainBO = generateService.getGenerateDetail(generateId);
        AssertUtil.notNull(generateMainBO, "代码生成配置为空");
        // 构造批量删除参数
        CodeOnlineSqlDO codeOnlineSql = CodeOnlineUtil.buildBatchDeleteSql(generateMainBO, list);
        log.info("[代码在线服务-批量删除] 批量删除语句构造结果={}", JsonUtil.toJson(codeOnlineSql));
        return codeOnlineDao.batchDelete(codeOnlineSql);
    }
}
