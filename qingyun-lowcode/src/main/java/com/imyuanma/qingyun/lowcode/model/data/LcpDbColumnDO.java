package com.imyuanma.qingyun.lowcode.model.data;

import lombok.Data;

/**
 * 数据库字段模型
 *
 * @author wangjy
 * @date 2022/05/04 14:34:45
 */
@Data
public class LcpDbColumnDO {
    /**
     * 表名
     */
    private String tableName;
    /**
     * 字段名
     */
    private String columnName;
    /**
     * 字段数据类型
     */
    private String dataType;
    /**
     * 字段长度
     */
    private Integer length;
    /**
     * 非空
     */
    private Integer notNull;
    /**
     * 备注,字段说明
     */
    private String remark;
    /**
     * 字段键类型
     * PRI: 主键
     */
    private String columnKey;
    /**
     * 自增
     */
    private Integer autoIncrement;
    /**
     * 字段顺序
     */
    private Integer position;
    /**
     * 字段默认值
     */
    private String columnDefault;

    /**
     * 是否主键
     *
     * @return
     */
    public boolean isPrimaryKey() {
        return "PRI".equals(this.columnKey);
    }
}
