package com.imyuanma.qingyun.lowcode.model.bo;

import com.imyuanma.qingyun.lowcode.model.enums.ELcpInnerButtonEnum;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 按钮栏配置
 *
 * @author wangjy
 * @date 2022/07/03 12:13:50
 */
@Data
public class GenerateButtonBarConfigBO {
    /**
     * 按钮集合
     *
     * @see ELcpInnerButtonEnum 内置按钮, 除此之外还支持自定义按钮
     */
    private List<LcpButtonBO> buttonList;

    /**
     * 按钮栏长度预估
     *
     * @return
     */
    public int getButtonBarWidth() {
        int len = 10;
        if (this.buttonList != null) {
            for (LcpButtonBO buttonBO : this.buttonList) {
                // 大按钮加宽10, 链接按钮减少20
                int ext = ("large".equals(buttonBO.getSize()) ? 10 : 0) + ("link".equals(buttonBO.getCategory()) ? -20 : 0);
                // 默认按钮按照74计算, 再加上不同按钮的弹性
                len += 74 + ext;
                // 超出的字数每个字按14计算
                if (buttonBO.getName() != null && buttonBO.getName().length() > 2) {
                    len += (buttonBO.getName().length() - 2) * 14;
                }
            }
        }
        return len;
    }

    /**
     * 添加按钮
     *
     * @param buttonBO
     */
    public void addButton(LcpButtonBO buttonBO) {
        if (this.buttonList == null) {
            this.buttonList = new ArrayList<>();
        }
        this.buttonList.add(buttonBO);
    }

    /**
     * 初始化按钮栏对象, 赋值默认值, 表面存在null引起页面错误
     *
     * @return
     */
    public static GenerateButtonBarConfigBO newInstance() {
        GenerateButtonBarConfigBO configBO = new GenerateButtonBarConfigBO();
        configBO.buttonList = new ArrayList<>();
        return configBO;
    }

}
