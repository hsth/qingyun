package com.imyuanma.qingyun.lowcode.controller;

import com.imyuanma.qingyun.common.factory.BaseDOBuilder;
import com.imyuanma.qingyun.common.model.request.WebRequest;
import com.imyuanma.qingyun.common.model.response.Page;
import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.lowcode.model.LcpDbDataSource;
import com.imyuanma.qingyun.lowcode.service.ILcpDbDataSourceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * DB数据源web
 *
 * @author YuanMaKeJi
 * @date 2023-04-22 11:18:10
 */
@RestController
@RequestMapping(value = "/lowcode/lcpDbDataSource")
public class LcpDbDataSourceController {

    private static final Logger logger = LoggerFactory.getLogger(LcpDbDataSourceController.class);

    /**
     * DB数据源服务
     */
    @Autowired
    private ILcpDbDataSourceService lcpDbDataSourceService;

    /**
     * 列表查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("查询DB数据源列表")
    @PostMapping("/getList")
    public Result<List<LcpDbDataSource>> getList(@RequestBody WebRequest<LcpDbDataSource> webRequest) {
        return Result.success(lcpDbDataSourceService.getList(webRequest.getBody()));
    }

    /**
     * 分页查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("分页查询DB数据源")
    @PostMapping("/getPage")
    public Page<List<LcpDbDataSource>> getPage(@RequestBody WebRequest<LcpDbDataSource> webRequest) {
        PageQuery pageQuery = webRequest.buildPageQuery();
        List<LcpDbDataSource> list = lcpDbDataSourceService.getPage(webRequest.getBody(), pageQuery);
        return Page.success(list, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("统计DB数据源数量")
    @PostMapping("/count")
    public Result<Integer> count(@RequestBody WebRequest<LcpDbDataSource> webRequest) {
        return Result.success(lcpDbDataSourceService.count(webRequest.getBody()));
    }

    /**
     * 查询
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("查询DB数据源")
    @PostMapping("/get")
    public Result<LcpDbDataSource> get(@RequestBody WebRequest<Long> webRequest) {
        AssertUtil.notNull(webRequest.getBody());
        return Result.success(lcpDbDataSourceService.get(webRequest.getBody()));
    }

    /**
     * 新增
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("新增DB数据源")
    @PostMapping("/insert")
    public Result insert(@RequestBody WebRequest<LcpDbDataSource> webRequest) {
        LcpDbDataSource lcpDbDataSource = webRequest.getBody();
        AssertUtil.notNull(lcpDbDataSource);
        BaseDOBuilder.fillBaseDOForInsert(lcpDbDataSource);
        lcpDbDataSourceService.insertSelective(lcpDbDataSource);
        return Result.success();
    }

    /**
     * 修改
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("修改DB数据源")
    @PostMapping("/update")
    public Result update(@RequestBody WebRequest<LcpDbDataSource> webRequest) {
        LcpDbDataSource lcpDbDataSource = webRequest.getBody();
        AssertUtil.notNull(lcpDbDataSource);
        AssertUtil.notNull(lcpDbDataSource.getId());
        BaseDOBuilder.fillBaseDOForUpdate(lcpDbDataSource);
        lcpDbDataSourceService.updateSelective(lcpDbDataSource);
        return Result.success();
    }

    /**
     * 删除
     *
     * @param webRequest 参数, 待删除主键,多个使用英文逗号拼接
     * @return
     */
    @Trace("删除DB数据源")
    @PostMapping("/delete")
    public Result delete(@RequestBody WebRequest<String> webRequest) {
        String ids = webRequest.getBody();
        AssertUtil.notBlank(ids);
        if (ids.contains(",")) {
            lcpDbDataSourceService.batchDelete(Arrays.stream(ids.split(",")).map(Long::valueOf).collect(Collectors.toList()));
        } else {
            lcpDbDataSourceService.delete(Long.valueOf(ids));
        }
        return Result.success();
    }



}
