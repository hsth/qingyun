package com.imyuanma.qingyun.lowcode.service.impl;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.lowcode.dao.ILcpDictDao;
import com.imyuanma.qingyun.lowcode.model.LcpDict;
import com.imyuanma.qingyun.lowcode.service.ILcpDictService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 字典表服务
 *
 * @author YuanMaKeJi
 * @date 2022-12-04 23:42:56
 */
@Slf4j
@Service
public class LcpDictServiceImpl implements ILcpDictService {

    /**
     * 字典表dao
     */
    @Autowired
    private ILcpDictDao lcpDictDao;

    /**
     * 列表查询
     *
     * @param lcpDict 查询条件
     * @return
     */
    @Trace("查询字典表列表")
    @Override
    public List<LcpDict> getList(LcpDict lcpDict) {
        return lcpDictDao.getList(lcpDict);
    }

    /**
     * 分页查询
     *
     * @param lcpDict 查询条件
     * @param pageQuery 分页参数
     * @return
     */
    @Trace("分页查询字典表")
    @Override
    public List<LcpDict> getPage(LcpDict lcpDict, PageQuery pageQuery) {
        return lcpDictDao.getList(lcpDict, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param lcpDict 查询条件
     * @return
     */
    @Trace("统计字典表数量")
    @Override
    public int count(LcpDict lcpDict) {
        return lcpDictDao.count(lcpDict);
    }

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    @Trace("根据主键查询字典表")
    @Override
    public LcpDict get(Long id) {
        return lcpDictDao.get(id);
    }

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    @Trace("根据主键批量查询字典表")
    @Override
    public List<LcpDict> getListByIds(List<Long> list) {
        return lcpDictDao.getListByIds(list);
    }

    /**
     * 插入
     *
     * @param lcpDict 参数
     * @return
     */
    @Trace("插入字典表")
    @Override
    public int insert(LcpDict lcpDict) {
        return lcpDictDao.insert(lcpDict);
    }

    /**
     * 选择性插入
     *
     * @param lcpDict 参数
     * @return
     */
    @Trace("选择性插入字典表")
    @Override
    public int insertSelective(LcpDict lcpDict) {
        return lcpDictDao.insertSelective(lcpDict);
    }

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量插入字典表")
    @Override
    public int batchInsert(List<LcpDict> list) {
        return lcpDictDao.batchInsert(list);
    }

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量选择性插入字典表")
    @Override
    public int batchInsertSelective(List<LcpDict> list) {
        return lcpDictDao.batchInsertSelective(list);
    }

    /**
     * 修改
     *
     * @param lcpDict 参数
     * @return
     */
    @Trace("修改字典表")
    @Override
    public int update(LcpDict lcpDict) {
        return lcpDictDao.update(lcpDict);
    }

    /**
     * 选择性修改
     *
     * @param lcpDict 参数
     * @return
     */
    @Trace("选择性修改字典表")
    @Override
    public int updateSelective(LcpDict lcpDict) {
        return lcpDictDao.updateSelective(lcpDict);
    }

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    @Trace("删除字典表")
    @Override
    public int delete(Long id) {
        return lcpDictDao.delete(id);
    }

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    @Trace("批量删除字典表")
    @Override
    public int batchDelete(List<Long> list) {
        return lcpDictDao.batchDelete(list);
    }

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param lcpDict 参数
     * @return
     */
    @Trace("根据条件删除字典表")
    @Override
    public int deleteByCondition(LcpDict lcpDict) {
        return lcpDictDao.deleteByCondition(lcpDict);
    }

}
