package com.imyuanma.qingyun.lowcode.model.bo;

import com.imyuanma.qingyun.interfaces.common.model.BaseDO;
import com.imyuanma.qingyun.interfaces.common.model.enums.EYesOrNoEnum;
import com.imyuanma.qingyun.lowcode.model.enums.ELcpColumnTypeEnum;
import com.imyuanma.qingyun.lowcode.model.enums.ELcpFormControlTypeEnum;
import com.imyuanma.qingyun.lowcode.model.enums.ELcpShowFormatEnum;
import lombok.Data;

import java.util.Arrays;

/**
 * 代码生成字段业务模型
 *
 * @author wangjy
 * @date 2022/05/04 16:16:58
 */
@Data
public class GenerateFieldBO {
    /**
     * 主键
     */
    private Long id;
    /**
     * 代码生成id
     */
    private Long generateId;
    /**
     * 数据库字段名
     */
    private String fieldDbName;
    /**
     * 字段注释
     */
    private String fieldDbRemark;
    /**
     * 数据库字段类型
     */
    private String fieldDbType;
    /**
     * 数据库字段长度
     */
    private Integer fieldDbLength;
    /**
     * 数据库字段是否非空
     */
    private Integer fieldDbNotNull;
    /**
     * 数据库字段键
     * PRI:主键
     */
    private String fieldDbKey;
    /**
     * java字段名
     */
    private String fieldJavaName;
    /**
     * java字段类型
     *
     * @see ELcpColumnTypeEnum
     */
    private String fieldJavaType;
    /**
     * 支持导入
     * @see com.imyuanma.qingyun.interfaces.common.model.enums.EYesOrNoEnum
     */
    private Integer supportImport;
    /**
     * 支持导出
     * @see com.imyuanma.qingyun.interfaces.common.model.enums.EYesOrNoEnum
     */
    private Integer supportExport;

    /**
     * 是否在列表显示,1是,0否
     */
    private Integer listColumnShow;
    /**
     * 字段在列表显示的顺序
     */
    private Integer listColumnOrder;
    /**
     * 该列在表格是否固定显示,no,left,right
     */
    private String listColumnFixed;
    /**
     * 列宽度
     */
    private String listColumnWidth;
    /**
     * 该列是否支持排序
     */
    private Integer listColumnSort;
    /**
     * 表头文案
     */
    private String listHeaderTitle;
    /**
     * 表头文案位置,center,left,right
     */
    private String listHeaderAlign;
    /**
     * 表头提示文案
     */
    private String listHeaderTips;
    /**
     * 表格内容位置,center,left,right
     */
    private String listValueAlign;
    /**
     * 表格内容格式,text,html,tag,link,image,button,formatter等
     *
     * @see ELcpShowFormatEnum
     */
    private String listValueFormat;
    /**
     * 条件查询类型,1无,2等于,3模糊查询,4模糊左匹配等等等等
     *
     * @see com.imyuanma.qingyun.lowcode.model.enums.ELcpQueryConditionTypeEnum
     */
    private Integer listQueryCondition;

    /**
     * 是否在表单显示,1是,0否
     */
    private Integer formColumnShow;
    /**
     * 表单项显示顺序
     */
    private Integer formColumnOrder;
    /**
     * 表单项栅格宽度,1~24
     */
    private Integer formColumnGridWidth;
    /**
     * 表单控件类型,input,date,time等等等等
     *
     * @see ELcpFormControlTypeEnum
     */
    private String formControlType;
    /**
     * 表单项默认值,仅新增时有效
     */
    private String formControlDefault;
    /**
     * 表单项标题
     */
    private String formControlTitle;
    /**
     * 表单项placeholder
     */
    private String formControlPlaceholder;
    /**
     * 表单项提示文案
     */
    private String formControlTips;
    /**
     * 表单项显隐控制配置
     */
    private GenerateFormColumnShowConfigBO formColumnShowConfig;
    /**
     * 表单项数据源配置
     */
    private GenerateControlDataSourceBO formControlDataSource;
    /**
     * 表单项校验规则配置
     */
    private GenerateControlCheckRuleBO formControlCheckRule;
    /**
     * 表单项其他配置
     */
    private GenerateControlConfigBO formControlConfig;

    /**
     * 判断属性名是否在BaseDO中
     *
     * @return
     */
    public boolean isBaseDOField() {
        return Arrays.stream(BaseDO.allField()).anyMatch(s -> s.equals(this.fieldJavaName));
    }

    /**
     * 是否数字类型字段
     *
     * @return
     */
    public boolean isNumberFlag() {
        return ELcpColumnTypeEnum.isNumberTypeJava(this.fieldJavaType);
    }

    /**
     * 是否主键
     *
     * @return
     */
    public boolean isPrimaryKey() {
        return "PRI".equals(this.fieldDbKey);
    }

    /**
     * 数据源配置是否有效
     *
     * @return
     */
    public boolean isValidDataSourceFlag() {
        return this.formControlDataSource != null && this.formControlDataSource.isValid();
    }

    /**
     * 字段是否支持导入
     * @return
     */
    public boolean isSupportImportFlag() {
        return EYesOrNoEnum.YES.getCode().equals(supportImport);
    }

    /**
     * 字段是否支持导出
     * @return
     */
    public boolean isSupportExportFlag() {
        return EYesOrNoEnum.YES.getCode().equals(supportExport);
    }

    /**
     * 是否使用cos上传控件
     * @return
     */
    public boolean useCosUploadControl() {
        return ELcpFormControlTypeEnum.UPLOAD.getType().equals(formControlType) && formControlConfig != null && formControlConfig.useCosStorage();
    }

    /**
     * md富文本编辑器
     * @return
     */
    public boolean useEditorMdControl() {
        return ELcpFormControlTypeEnum.EDITOR_MD.getType().equals(formControlType);
    }
}
