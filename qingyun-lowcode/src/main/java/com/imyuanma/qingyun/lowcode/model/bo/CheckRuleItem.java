package com.imyuanma.qingyun.lowcode.model.bo;

import com.imyuanma.qingyun.common.util.StringUtil;
import com.imyuanma.qingyun.lowcode.model.enums.ELcpFormControlCheckEnum;
import lombok.Data;

/**
 * 校验规则单项
 *
 * @author wangjy
 * @date 2022/04/02 10:59:09
 */
@Data
public class CheckRuleItem {
    /**
     * 校验类型
     *
     * @see ELcpFormControlCheckEnum
     */
    private String checkType;
    /**
     * 正则表达式
     */
    private String checkReg;
    /**
     * 错误提示
     */
    private String errorTips;
    /**
     * 最小值
     */
    private Integer min;
    /**
     * 最大值
     */
    private Integer max;

    /**
     * 构造校验项
     *
     * @param controlCheckEnum
     * @param fieldBO
     * @return
     */
    public static CheckRuleItem build(ELcpFormControlCheckEnum controlCheckEnum, GenerateFieldBO fieldBO) {
        CheckRuleItem checkRuleItem = new CheckRuleItem();
        checkRuleItem.setCheckType(controlCheckEnum.getType());
        String error;
        if (ELcpFormControlCheckEnum.ErrEmpty == controlCheckEnum) {
            error = String.format("%s不允许为空", StringUtil.getDefaultValue(fieldBO.getFormControlTitle(), fieldBO.getFieldDbRemark()));
        } else if (ELcpFormControlCheckEnum.ErrLength == controlCheckEnum) {
            checkRuleItem.setMin(1);
            checkRuleItem.setMax(fieldBO.getFieldDbLength());
            error = String.format("%s内容长度限制为%s~%s", StringUtil.getDefaultValue(fieldBO.getFormControlTitle(), fieldBO.getFieldDbRemark()), checkRuleItem.getMin(), checkRuleItem.getMax());
        } else if (ELcpFormControlCheckEnum.ErrNumber == controlCheckEnum) {
            error = String.format("%s请输入数字类型", StringUtil.getDefaultValue(fieldBO.getFormControlTitle(), fieldBO.getFieldDbRemark()));
        } else if (ELcpFormControlCheckEnum.ErrMail == controlCheckEnum) {
            error = String.format("%s请输入正确的邮箱格式", StringUtil.getDefaultValue(fieldBO.getFormControlTitle(), fieldBO.getFieldDbRemark()));
        } else if (ELcpFormControlCheckEnum.ErrPhone == controlCheckEnum) {
            error = String.format("%s请输入正确的手机号格式", StringUtil.getDefaultValue(fieldBO.getFormControlTitle(), fieldBO.getFieldDbRemark()));
        } else if (ELcpFormControlCheckEnum.ErrReg == controlCheckEnum) {
            error = String.format("%s请输入正确的手机号格式", StringUtil.getDefaultValue(fieldBO.getFormControlTitle(), fieldBO.getFieldDbRemark()));
        } else {
            error = "Error";
        }
        checkRuleItem.setErrorTips(error);
        return checkRuleItem;
    }


}
