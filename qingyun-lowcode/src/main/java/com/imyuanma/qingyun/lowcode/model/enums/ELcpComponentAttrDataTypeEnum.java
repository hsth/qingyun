package com.imyuanma.qingyun.lowcode.model.enums;

/**
 * 属性数据类型枚举
 *
 * @author YuanMaKeJi
 * @date 2022-12-03 21:45:01
 */
public enum ELcpComponentAttrDataTypeEnum {
//    CODE_var("var", "vue变量"),
    CODE_string("string", "字符串"),
    CODE_number("number", "数字"),
    CODE_boolean("boolean", "布尔"),
    CODE_object("object", "对象"),
    CODE_array("array", "数组"),
    CODE_time("time", "时间"),
    CODE_function("function", "方法"),
    ;

    /**
     * code
     */
    private String code;
    /**
     * 文案
     */
    private String text;

    ELcpComponentAttrDataTypeEnum(String code, String text) {
        this.code = code;
        this.text = text;
    }

    /**
     * 根据code获取枚举
     *
     * @param code code值
     */
    public static ELcpComponentAttrDataTypeEnum getByCode(String code) {
        for (ELcpComponentAttrDataTypeEnum e : ELcpComponentAttrDataTypeEnum.values()) {
            if (e.code != null && e.code.equals(code)) {
                return e;
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
