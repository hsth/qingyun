package com.imyuanma.qingyun.lowcode.model.enums;

/**
 * 表单项数据源类型
 *
 * @author wangjy
 * @date 2022/06/26 11:47:45
 */
public enum ELcpFormItemDataSourceTypeEnum {
    NO("no", "无"),
    STATIC("static", "静态数据"),
    URL("url", "URL数据"),
    DICT("dict", "字典数据"),

    ;

    private String type;
    private String text;

    ELcpFormItemDataSourceTypeEnum(String type, String text) {
        this.type = type;
        this.text = text;
    }

    public static ELcpFormItemDataSourceTypeEnum of(String type) {
        for (ELcpFormItemDataSourceTypeEnum e : ELcpFormItemDataSourceTypeEnum.values()) {
            if (e.getType().equalsIgnoreCase(type)) {
                return e;
            }
        }
        return null;
    }

    public String getType() {
        return type;
    }

    public String getText() {
        return text;
    }
}
