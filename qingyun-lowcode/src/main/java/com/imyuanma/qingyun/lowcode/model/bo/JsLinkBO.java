package com.imyuanma.qingyun.lowcode.model.bo;

import lombok.Data;

/**
 * js链接
 *
 * @author wangjy
 * @date 2022/07/29 22:31:44
 */
@Data
public class JsLinkBO {
    /**
     * 链接
     */
    private String url;

    public static JsLinkBO buildDefault() {
        JsLinkBO jsLinkBO = new JsLinkBO();
        jsLinkBO.setUrl("");
        return jsLinkBO;
    }
}
