package com.imyuanma.qingyun.lowcode.model.enums;

/**
 * 模型类继承类型
 *
 * @author wangjy
 * @date 2022/05/07 21:21:18
 */
public enum ELcpModelExtendsTypeEnum {
    DbSortDO(1, "继承DbSortDO"),
    BaseDO(2, "继承BaseDO模型"),
    BaseDOAndFill(3, "继承并自动填充BaseDO"),
    ;

    private Integer code;
    private String text;

    ELcpModelExtendsTypeEnum(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    public Integer getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
