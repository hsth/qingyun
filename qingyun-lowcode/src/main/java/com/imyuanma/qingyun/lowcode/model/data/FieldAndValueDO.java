package com.imyuanma.qingyun.lowcode.model.data;

import com.imyuanma.qingyun.lowcode.model.bo.GenerateFieldBO;
import lombok.Data;

import java.util.Map;

/**
 * 字段与字段值
 *
 * @author wangjy
 * @date 2022/07/01 21:52:14
 */
@Data
public class FieldAndValueDO {
    /**
     * 表字段名
     */
    private String fieldDbName;
    /**
     * java字段名
     */
    private String fieldJavaName;
    /**
     * 表字段类型
     */
    private String fieldDbType;
    /**
     * java字段类型
     */
    private String fieldJavaType;
    /**
     * 字段值
     */
    private Object value;
    /**
     * 范围查询时用来作为end值
     */
    private Object value2;
    /**
     * 件查询类型,1无,2等于,3模糊查询,4模糊左匹配等等等等
     *
     * @see com.imyuanma.qingyun.lowcode.model.enums.ELcpQueryConditionTypeEnum
     */
    private Integer queryConditionType;

    /**
     * 创建普通字段对象
     *
     * @param fieldBO
     * @return
     */
    public static FieldAndValueDO of(GenerateFieldBO fieldBO) {
        FieldAndValueDO fieldAndValueDO = new FieldAndValueDO();
        fieldAndValueDO.setFieldDbName(fieldBO.getFieldDbName());
        fieldAndValueDO.setFieldJavaName(fieldBO.getFieldJavaName());
        fieldAndValueDO.setFieldDbType(fieldBO.getFieldDbType());
        fieldAndValueDO.setFieldJavaType(fieldBO.getFieldJavaType());
        return fieldAndValueDO;
    }

    /**
     * 构造查询条件对象
     *
     * @param fieldBO
     * @param condition
     * @return
     */
    public static FieldAndValueDO of(GenerateFieldBO fieldBO, Map<String, Object> condition) {
        FieldAndValueDO fieldAndValueDO = new FieldAndValueDO();
        fieldAndValueDO.setFieldDbName(fieldBO.getFieldDbName());
        fieldAndValueDO.setFieldJavaName(fieldBO.getFieldJavaName());
        fieldAndValueDO.setFieldDbType(fieldBO.getFieldDbType());
        fieldAndValueDO.setFieldJavaType(fieldBO.getFieldJavaType());
        fieldAndValueDO.setValue(condition.get(fieldBO.getFieldJavaName()));
        fieldAndValueDO.setValue2(condition.get(fieldBO.getFieldJavaName()+"2"));
        fieldAndValueDO.setQueryConditionType(fieldBO.getListQueryCondition());
        return fieldAndValueDO;

    }
}
