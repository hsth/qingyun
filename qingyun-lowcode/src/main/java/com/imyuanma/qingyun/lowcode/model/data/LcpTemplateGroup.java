package com.imyuanma.qingyun.lowcode.model.data;

import lombok.Data;
import java.util.Date;
import java.util.List;


/**
 * 代码模板组实体类
 *
 * @author YuanMaKeJi
 * @date 2022-06-29 22:53:35
 */
@Data
public class LcpTemplateGroup {

    /**
     * 主键
     */
    private Long id;

    /**
     * 模板组名
     */
    private String name;

    /**
     * 说明
     */
    private String remark;

    /**
     * 显示顺序
     */
    private Long dataSequence;

    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建时间,按时间检索时作为结束时间使用
     */
    private Date createTime2;

    /**
     * 组下的模板集合
     */
    private List<LcpTemplateItem> templateItemList;


    /**
     * 排序字段
     */
    private String dbSortBy;
    /**
     * 排序类型,升序asc/降序desc
     */
    private String dbSortType;

}