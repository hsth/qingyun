package com.imyuanma.qingyun.lowcode.model.enums;

/**
 * 按钮样式类型
 *
 * @author wangjy
 * @date 2022/07/03 14:04:58
 */
public enum ELcpButtonTypeEnum {
    DEFAULT("","默认"),
    PRIMARY("primary","primary"),
    SUCCESS("success","success"),
    INFO("info","info"),
    WARNING("warning","warning"),
    DANGER("danger","danger"),
    ;

    private String type;
    private String text;

    ELcpButtonTypeEnum(String type, String text) {
        this.type = type;
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public String getText() {
        return text;
    }
}
