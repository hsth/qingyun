package com.imyuanma.qingyun.lowcode.service.impl;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.lowcode.dao.ILcpTemplateGroupDao;
import com.imyuanma.qingyun.lowcode.model.data.LcpTemplateGroup;
import com.imyuanma.qingyun.lowcode.service.ILcpTemplateGroupService;
import com.imyuanma.qingyun.lowcode.service.ILcpTemplateItemService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 代码模板组服务
 *
 * @author YuanMaKeJi
 * @date 2022-06-29 22:53:35
 */
@Slf4j
@Service
public class LcpTemplateGroupServiceImpl implements ILcpTemplateGroupService {

    /**
     * 代码模板组dao
     */
    @Autowired
    private ILcpTemplateGroupDao lcpTemplateGroupDao;
    /**
     * 模板项
     */
    @Autowired
    private ILcpTemplateItemService templateItemService;

    /**
     * 列表查询
     *
     * @param lcpTemplateGroup 查询条件
     * @return
     */
    @Override
    public List<LcpTemplateGroup> getList(LcpTemplateGroup lcpTemplateGroup) {
        return lcpTemplateGroupDao.getList(lcpTemplateGroup);
    }

    /**
     * 分页查询
     *
     * @param lcpTemplateGroup 查询条件
     * @param pageQuery 分页参数
     * @return
     */
    @Override
    public List<LcpTemplateGroup> getPage(LcpTemplateGroup lcpTemplateGroup, PageQuery pageQuery) {
        return lcpTemplateGroupDao.getList(lcpTemplateGroup, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param lcpTemplateGroup 查询条件
     * @return
     */
    @Override
    public int count(LcpTemplateGroup lcpTemplateGroup) {
        return lcpTemplateGroupDao.count(lcpTemplateGroup);
    }

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    @Override
    public LcpTemplateGroup get(Long id) {
        return lcpTemplateGroupDao.get(id);
    }

    /**
     * 详情
     *
     * @param id 主键
     * @return
     */
    @Override
    public LcpTemplateGroup detail(Long id) {
        LcpTemplateGroup templateGroup = this.get(id);
        if (templateGroup != null) {
            templateGroup.setTemplateItemList(templateItemService.getByGroupId(id));
        }
        return templateGroup;
    }

    /**
     * 插入
     *
     * @param lcpTemplateGroup 参数
     * @return
     */
    @Override
    public int insert(LcpTemplateGroup lcpTemplateGroup) {
        return lcpTemplateGroupDao.insert(lcpTemplateGroup);
    }

    /**
     * 选择性插入
     *
     * @param lcpTemplateGroup 参数
     * @return
     */
    @Override
    public int insertSelective(LcpTemplateGroup lcpTemplateGroup) {
        return lcpTemplateGroupDao.insertSelective(lcpTemplateGroup);
    }

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    @Override
    public int batchInsert(List<LcpTemplateGroup> list) {
        return lcpTemplateGroupDao.batchInsert(list);
    }

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    @Override
    public int batchInsertSelective(List<LcpTemplateGroup> list) {
        return lcpTemplateGroupDao.batchInsertSelective(list);
    }

    /**
     * 修改
     *
     * @param lcpTemplateGroup 参数
     * @return
     */
    @Override
    public int update(LcpTemplateGroup lcpTemplateGroup) {
        return lcpTemplateGroupDao.update(lcpTemplateGroup);
    }

    /**
     * 选择性修改
     *
     * @param lcpTemplateGroup 参数
     * @return
     */
    @Override
    public int updateSelective(LcpTemplateGroup lcpTemplateGroup) {
        return lcpTemplateGroupDao.updateSelective(lcpTemplateGroup);
    }

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    @Override
    public int delete(Long id) {
        return lcpTemplateGroupDao.delete(id);
    }

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    @Override
    public int batchDelete(List<Long> list) {
        return lcpTemplateGroupDao.batchDelete(list);
    }

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param lcpTemplateGroup 参数
     * @return
     */
    @Override
    public int deleteByCondition(LcpTemplateGroup lcpTemplateGroup) {
        return lcpTemplateGroupDao.deleteByCondition(lcpTemplateGroup);
    }

}
