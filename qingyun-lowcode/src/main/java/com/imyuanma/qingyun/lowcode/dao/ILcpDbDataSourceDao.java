package com.imyuanma.qingyun.lowcode.dao;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.lowcode.model.LcpDbDataSource;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * DB数据源dao
 *
 * @author YuanMaKeJi
 * @date 2023-04-22 11:18:10
 */
@Mapper
public interface ILcpDbDataSourceDao {

    /**
     * 列表查询
     *
     * @param lcpDbDataSource 查询条件
     * @return
     */
    @Trace("查询DB数据源列表")
    List<LcpDbDataSource> getList(LcpDbDataSource lcpDbDataSource);

    /**
     * 分页查询
     *
     * @param lcpDbDataSource 查询条件
     * @param pageQuery 分页参数
     * @return
     */
    @Trace("分页查询DB数据源")
    List<LcpDbDataSource> getList(LcpDbDataSource lcpDbDataSource, PageQuery pageQuery);

    /**
     * 统计数量
     *
     * @param lcpDbDataSource 查询条件
     * @return
     */
    @Trace("统计DB数据源数量")
    int count(LcpDbDataSource lcpDbDataSource);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    @Trace("主键查询DB数据源")
    LcpDbDataSource get(Long id);

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    @Trace("主键批量查询DB数据源")
    List<LcpDbDataSource> getListByIds(List<Long> list);

    /**
     * 根据code查询
     *
     * @param code 数据源编码
     * @return
     */
    @Trace("根据code查询DB数据源")
    LcpDbDataSource getByCode(@Param("code") String code);

    /**
     * 根据dbType查询
     *
     * @param dbType 数据库类型
     * @return
     */
    @Trace("根据dbType查询DB数据源")
    List<LcpDbDataSource> getListByDbType(@Param("dbType") String dbType);

    /**
     * 插入
     *
     * @param lcpDbDataSource 参数
     * @return
     */
    @Trace("插入DB数据源")
    int insert(LcpDbDataSource lcpDbDataSource);

    /**
     * 选择性插入
     *
     * @param lcpDbDataSource 参数
     * @return
     */
    @Trace("选择性插入DB数据源")
    int insertSelective(LcpDbDataSource lcpDbDataSource);

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量插入DB数据源")
    int batchInsert(List<LcpDbDataSource> list);

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量选择性插入DB数据源")
    int batchInsertSelective(List<LcpDbDataSource> list);

    /**
     * 修改
     *
     * @param lcpDbDataSource 参数
     * @return
     */
    @Trace("修改DB数据源")
    int update(LcpDbDataSource lcpDbDataSource);

    /**
     * 选择性修改
     *
     * @param lcpDbDataSource 参数
     * @return
     */
    @Trace("选择性修改DB数据源")
    int updateSelective(LcpDbDataSource lcpDbDataSource);

    /**
     * 批量修改某字段
     *
     * @param idList      主键集合
     * @param fieldDbName 待修改的字段名
     * @param fieldValue  修改后的值
     * @return
     */
    @Trace("批量修改DB数据源属性")
    int batchUpdateColumn(@Param("idList") List<Long> idList, @Param("fieldDbName") String fieldDbName, @Param("fieldValue") Object fieldValue);

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    @Trace("删除DB数据源")
    int delete(Long id);

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    @Trace("批量删除DB数据源")
    int batchDelete(List<Long> list);

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param lcpDbDataSource 参数
     * @return
     */
    @Trace("条件删除DB数据源")
    int deleteByCondition(LcpDbDataSource lcpDbDataSource);

    /**
     * 删除全部
     *
     * @return
     */
    @Trace("全量删除DB数据源")
    int deleteAll();

}
