package com.imyuanma.qingyun.lowcode.controller;

import com.imyuanma.qingyun.common.core.excel.ExcelExport;
import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.model.request.WebRequest;
import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.common.util.DateUtil;
import com.imyuanma.qingyun.common.util.io.FileDownloadUtil;
import com.imyuanma.qingyun.interfaces.common.model.enums.EYesOrNoEnum;
import com.imyuanma.qingyun.interfaces.common.model.vo.KeyValueVO;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.lowcode.model.SqlExecuteCmd;
import com.imyuanma.qingyun.lowcode.model.data.LcpDbColumnDO;
import com.imyuanma.qingyun.lowcode.model.data.LcpDbTableDO;
import com.imyuanma.qingyun.lowcode.model.enums.ELcpColumnTypeEnum;
import com.imyuanma.qingyun.lowcode.model.vo.LcpDbColumnVO;
import com.imyuanma.qingyun.lowcode.model.vo.LcpDbTableDetailVO;
import com.imyuanma.qingyun.lowcode.model.vo.SqlExecuteResultVO;
import com.imyuanma.qingyun.lowcode.service.ILcpDbService;
import com.imyuanma.qingyun.lowcode.util.FtlTemplateUtil;
import freemarker.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 数据库web
 *
 * @author wangjy
 * @date 2022/05/04 15:36:29
 */
@RestController
@RequestMapping(value = "/lowcode/db", method = {RequestMethod.POST})
public class LcpDbController {
    /**
     * 数据库服务
     */
    @Autowired
    private ILcpDbService dbService;

    @Trace("查询支持的数据库字段类型")
    @RequestMapping(value = "/getDbColumnTypeList")
    public Result<List<KeyValueVO>> getDbColumnTypeList() {
        return Result.success(Arrays.stream(ELcpColumnTypeEnum.values()).map(item -> new KeyValueVO(item.getType(), item.getText())).collect(Collectors.toList()));
    }

    @Trace("执行SQL查询")
    @RequestMapping(value = "/{dataSourceCode}/executeSearchSql")
    public Result<SqlExecuteResultVO> executeSearchSql(@RequestBody WebRequest<SqlExecuteCmd> webRequest, @PathVariable("dataSourceCode") String dataSourceCode) {
        AssertUtil.notNull(webRequest.getBody());
        AssertUtil.notBlank(webRequest.getBody().getSql());
        SqlExecuteCmd cmd = webRequest.getBody();
        String sql = cmd.getSql().toLowerCase();
        String[] filters = {"insert ", "delete ", "update ", "create ", "modify ", "drop ", "truncate "};
        for (String filter : filters) {
            if (sql.contains(filter)) {
                return Result.error("执行失败：非法的关键字[" + filter + "]");
            }
        }
        if (!sql.startsWith("select ") && !sql.startsWith("explain ") && !sql.startsWith("show ") && !sql.startsWith("desc ")) {
            return Result.error("执行失败：非法的查询SQL");
        }
//        if (sql.startsWith("select ")) {
//            cmd.setSql("select * from (" + cmd.getSql() + ") tempp limit 100");
//        }
        cmd.setPageQuery(new PageQuery(1, 100));
        List<Map<String, Object>> list = dbService.executeSearchSqlDynamic(dataSourceCode, webRequest.getBody());
        SqlExecuteResultVO resultVO = new SqlExecuteResultVO();
        resultVO.setDataList(list);
        if (CollectionUtil.isNotEmpty(list)) {
            resultVO.setHeaders(list.get(0).entrySet().stream().map(Map.Entry::getKey).collect(Collectors.toList()));
        }
        return Result.success(resultVO);
    }

//    public static void main(String[] args) {
//        List<SQLStatement> statementList = SQLUtils.parseStatements("SELECT f.id,f.name,u.id FROM tb_fs_file f LEFT JOIN tb_ums_user u ON f.create_user_id=u.`id` WHERE u.id=1 AND f.`id` IN (SELECT CODE FROM tb_ums_app WHERE CODE='1')", JdbcUtils.MYSQL);
//        for (SQLStatement sqlStatement : statementList) {
//            if (sqlStatement instanceof SQLSelectStatement) {
//                SQLSelectStatement sqlSelectStatement = ((SQLSelectStatement) sqlStatement);
//                SQLSelect select = sqlSelectStatement.getSelect();
//                System.out.println(select);
//            }
//        }
//    }

    /**
     * 查询数据库表列表
     *
     * @return
     */
    @Trace("查询数据库表列表")
    @RequestMapping("/{dataSourceCode}/getTableList")
    public Result<List<LcpDbTableDO>> getTableList(@RequestBody WebRequest<LcpDbTableDO> webRequest, @PathVariable("dataSourceCode") String dataSourceCode) {
        return Result.success(dbService.getTableListDynamic(dataSourceCode, webRequest.getBody()));
    }

    /**
     * 查询数据库表详情
     *
     * @param webRequest
     * @return
     */
    @Trace("查询数据库表详情")
    @RequestMapping("/{dataSourceCode}/getTableDetail")
    public Result<LcpDbTableDetailVO> getTableDetail(@RequestBody WebRequest<String> webRequest, @PathVariable("dataSourceCode") String dataSourceCode) {
        AssertUtil.notNull(webRequest);
        AssertUtil.notBlank(webRequest.getBody());
        // 表信息
        LcpDbTableDO tableDO = dbService.getTableByTableNameDynamic(dataSourceCode, webRequest.getBody());
        if (tableDO == null) {
            return Result.error(String.format("表[%s]不存在", webRequest.getBody()));
        }

        // 表字段
        List<LcpDbColumnDO> columns = dbService.getColumnListByTableNameDynamic(dataSourceCode, webRequest.getBody());
        AssertUtil.notEmpty(columns, "表字段为空");

        // todo 表索引

        // 返回结果
        return Result.success(new LcpDbTableDetailVO(tableDO, columns, null));
    }

    @Trace("修改数据库字段")
    @RequestMapping(value = "/{dataSourceCode}/updateDbColumn")
    public Result updateDbColumn(@RequestBody WebRequest<LcpDbColumnVO> webRequest, @PathVariable("dataSourceCode") String dataSourceCode) {
        AssertUtil.notNull(webRequest);
        AssertUtil.notNull(webRequest.getBody());
        dbService.modifyDbColumnDynamic(dataSourceCode, webRequest.getBody());
        return Result.success();
    }

    @Trace("新增数据库字段")
    @RequestMapping(value = "/{dataSourceCode}/addDbColumn")
    public Result addDbColumn(@RequestBody WebRequest<LcpDbColumnVO> webRequest, @PathVariable("dataSourceCode") String dataSourceCode) {
        AssertUtil.notNull(webRequest);
        AssertUtil.notNull(webRequest.getBody());
        dbService.addDbColumnDynamic(dataSourceCode, webRequest.getBody());
        return Result.success();
    }

    @Trace("创建数据库表")
    @RequestMapping(value = "/{dataSourceCode}/createTable")
    public Result createTable(@RequestBody WebRequest<LcpDbTableDetailVO> webRequest, @PathVariable("dataSourceCode") String dataSourceCode) {
        AssertUtil.notNull(webRequest);
        AssertUtil.notNull(webRequest.getBody());
        dbService.createTableDynamic(dataSourceCode, webRequest.getBody());
        return Result.success();
    }

    @Trace("下载数据库说明文档Excel")
    @RequestMapping(value = "/{dataSourceCode}/downloadDbExcel", method = {RequestMethod.GET, RequestMethod.POST})
    public void downloadDbExcel(String tableNameList, @PathVariable("dataSourceCode") String dataSourceCode, HttpServletResponse response) throws Exception {
        AssertUtil.notBlank(tableNameList, "请选择待导出表");
        // 待导出的表
        List<LcpDbTableDO> tableList = dbService.getTableListByTableNameDynamic(dataSourceCode, Arrays.asList(tableNameList.split(",")));
        if (CollectionUtil.isEmpty(tableList)) {
            return;
        }

        List<LcpDbTableDetailVO> tableDetailList = new ArrayList<>();

        // 补充字段信息
        for (LcpDbTableDO tableDO : tableList) {
            List<LcpDbColumnDO> columns = dbService.getColumnListByTableNameDynamic(dataSourceCode, tableDO.getTableName());
            tableDetailList.add(new LcpDbTableDetailVO(tableDO, columns, null));
        }

        //生成excel
        ExcelExport ee = new ExcelExport();
        for (LcpDbTableDetailVO table : tableDetailList) {
            ee.insertRow(new String[]{table.getTableName() + "(" + table.getTableComment() + ")"});
            ee.insertRow(new String[]{"字段", "类型", "长度", "非空", "注释"});
            for (LcpDbColumnVO col : table.getColumnList()) {
                ee.insertRow(new Object[]{col.getColumnName(), col.getDataType(), col.getLength(), EYesOrNoEnum.YES.getCode().equals(col.getNotNull()) ? "是" : "否", col.getRemark()});
            }
            ee.insertRow(new String[]{});
        }
        ee.write2Response(response, "数据库文档_" + DateUtil.getCurrentDay() + ".xls");
    }

    @Trace("下载数据库说明文档word")
    @RequestMapping(value = "/{dataSourceCode}/downloadDbWord", method = {RequestMethod.GET, RequestMethod.POST})
    public void downloadDbWord(String tableNameList, @PathVariable("dataSourceCode") String dataSourceCode, HttpServletRequest request, HttpServletResponse response) throws Exception {
        AssertUtil.notBlank(tableNameList, "请选择待导出表");
        // 待导出的表
        List<LcpDbTableDO> tableList = dbService.getTableListByTableNameDynamic(dataSourceCode, Arrays.asList(tableNameList.split(",")));
        if (CollectionUtil.isEmpty(tableList)) {
            return;
        }

        List<LcpDbTableDetailVO> tableDetailList = new ArrayList<>();

        // 补充字段信息
        for (LcpDbTableDO tableDO : tableList) {
            List<LcpDbColumnDO> columns = dbService.getColumnListByTableNameDynamic(dataSourceCode, tableDO.getTableName());
            tableDetailList.add(new LcpDbTableDetailVO(tableDO, columns, null));
        }


        //获取模板文件
        Template template = FtlTemplateUtil.buildTemplateForTemplateFile("/com/imyuanma/qingyun/lowcode/template/db/", "db_doc.ftl");

        // 构造模板参数
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("tableList", tableDetailList);

        // 下载头
        FileDownloadUtil.setDownloadContentType(response);
        // 下载文件名
        FileDownloadUtil.setFileNameOfCN(request, response, "数据库文档_" + DateUtil.getCurrentDay() + ".doc");
        // 模板结果输出到响应流
        FtlTemplateUtil.writeResponseByTemplate(template, map, response);
    }

//    /**
//     * 备份表结构
//     * @param tableIds
//     * @param request
//     * @param response
//     * @throws Exception
//     */
//    @Trace("备份表结构")
//    @RequestMapping(value = "/backupTable", method = {RequestMethod.GET, RequestMethod.POST})
//    public void backupTable(String tableIds, HttpServletRequest request, HttpServletResponse response) throws Exception{
//        //参数校验
//        if (StringUtils.isBlank(tableIds)){
//            response.getWriter().write("表名无效!");
//        }
//        String[] tableArr = tableIds.split(",");
//        if (tableArr == null || tableArr.length == 0){
//            response.getWriter().write("表名无效!");
//        }
//        Set<String> tableSet = new HashSet<String>(Arrays.asList(tableArr));//便于后面的查询
//
//        //库中所有的表
//        List<Table> tableList = cmsDBService.getTableListOfSimpleInfo();
//        if (tableList == null || tableList.size() == 0){
//            response.getWriter().write("数据库中没有表!");
//        }
//        //筛选中选中的表信息
//        Iterator<Table> iterator = tableList.iterator();
//        while (iterator.hasNext()){
//            Table table = iterator.next();
//            if (!tableSet.contains(table.getId())){//表不在入参里面,就移除
//                iterator.remove();
//            }
//        }
//
//        //循环查询各表的字段信息
//        for (Table table : tableList){
//            //查询表的字段信息
//            table.setColumnList(cmsDBService.getColumsByTableName(table.getId()));
//        }
//
//        //解析为json字符串
//        String str = CommonJsonUtil.beanListToJsonStrArr(tableList);
//        logger.info("[备份表结构] 备份内容:{}", str);
//        //将json串写入流中供下载
//        byte[] bytes = str.getBytes("UTF-8");
//        //下载处理
//        response.setContentType("application/x-msdownload;");//文件下载类型
//        FileDownloadUtil.setFileNameOfCN(request, response, "backup" + DateUtil.dateFormat(new Date(), "yyyyMMddHHmmss") + ".table");//设置中文文件名
//        OutputStream os = response.getOutputStream();
//        os.write(bytes);
//        os.close();
//    }
//
//    /**
//     * 恢复表结构
//     * @param file
//     * @param request
//     * @param response
//     * @return
//     * @throws Exception
//     */
//    @Trace("恢复表结构")
//    @RequestMapping(value = "/recoverTable", method = {RequestMethod.GET, RequestMethod.POST})
//    @ResponseBody
//    public Object recoverTable(MultipartFile file, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        if (file != null && !file.isEmpty()){
//            InputStream is = file.getInputStream();
//            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//            IOUtils.copy(is, byteArrayOutputStream);
//            byte[] bytes = byteArrayOutputStream.toByteArray();
//            String str = new String(bytes, "UTF-8");
//            logger.info("[备份表结构] 恢复内容:{}", str);
//            List<JSONObject> list = CommonJsonUtil.jsonStrArrToBeanList(str, JSONObject.class);
//            if (list != null && list.size() > 0){
//                Map<String, Class> map = new HashMap<String, Class>();
//                map.put("columnList", Column.class);
//                for (JSONObject jsonObject : list){
//                    //转为Table对象
//                    Table table = (Table)JSONObject.toBean(jsonObject, Table.class, map);
//                    cmsDBService.createTable(table);//创建表
//                }
//            }
//        }
//        return Result.buildSuccess();
//    }
}
