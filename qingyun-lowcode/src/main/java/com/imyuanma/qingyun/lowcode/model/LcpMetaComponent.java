package com.imyuanma.qingyun.lowcode.model;

import lombok.Data;
import java.util.Date;
import java.util.List;

import com.imyuanma.qingyun.interfaces.common.model.BaseDO;

/**
 * 组件元数据实体类
 *
 * @author YuanMaKeJi
 * @date 2022-12-03 21:44:59
 */
@Data
public class LcpMetaComponent extends BaseDO {

    /**
     * 主键
     */
    private Long id;

    /**
     * 组件图标
     */
    private String icon;

    /**
     * 组件code
     */
    private String code;

    /**
     * 组件名称
     */
    private String name;

    /**
     * 组件类型,base:基础组件,group:复合组件
     */
    private String type;

    /**
     * 分类
     */
    private String category;

    /**
     * 复合组件内容,当组件为复合组件时,组件模板以group_content为准
     */
    private String groupContent;
    /**
     * 是否可选
     */
    private String showInOptional;
    /**
     * 是否可作为父节点
     */
    private String canAsParent;

    /**
     * 备注
     */
    private String remark;

    /**
     * 属性配置集合
     */
    private List<LcpMetaComponentAttr> attrMetaList;



}