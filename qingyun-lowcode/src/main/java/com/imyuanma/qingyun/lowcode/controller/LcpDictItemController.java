package com.imyuanma.qingyun.lowcode.controller;

import com.imyuanma.qingyun.common.factory.BaseDOBuilder;
import com.imyuanma.qingyun.common.model.request.WebRequest;
import com.imyuanma.qingyun.common.model.response.Page;
import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.lowcode.model.LcpDictItem;
import com.imyuanma.qingyun.lowcode.service.ILcpDictItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 字典数据项web
 *
 * @author YuanMaKeJi
 * @date 2022-12-04 23:44:03
 */
@RestController
@RequestMapping(value = "/lowcode/lcpDictItem")
public class LcpDictItemController {

    /**
     * 字典数据项服务
     */
    @Autowired
    private ILcpDictItemService lcpDictItemService;

    /**
     * 列表查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("查询字典数据项列表")
    @PostMapping("/getList")
    public Result<List<LcpDictItem>> getList(@RequestBody WebRequest<LcpDictItem> webRequest) {
        return Result.success(lcpDictItemService.getList(webRequest.getBody()));
    }

    /**
     * 分页查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("分页查询字典数据项")
    @PostMapping("/getPage")
    public Page<List<LcpDictItem>> getPage(@RequestBody WebRequest<LcpDictItem> webRequest) {
        PageQuery pageQuery = webRequest.buildPageQuery();
        List<LcpDictItem> list = lcpDictItemService.getPage(webRequest.getBody(), pageQuery);
        return Page.success(list, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("统计字典数据项数量")
    @PostMapping("/count")
    public Result<Integer> count(@RequestBody WebRequest<LcpDictItem> webRequest) {
        return Result.success(lcpDictItemService.count(webRequest.getBody()));
    }

    /**
     * 查询
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("查询字典数据项")
    @PostMapping("/get")
    public Result<LcpDictItem> get(@RequestBody WebRequest<Long> webRequest) {
        AssertUtil.notNull(webRequest.getBody());
        return Result.success(lcpDictItemService.get(webRequest.getBody()));
    }

    /**
     * 新增
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("新增字典数据项")
    @PostMapping("/insert")
    public Result insert(@RequestBody WebRequest<LcpDictItem> webRequest) {
        LcpDictItem lcpDictItem = webRequest.getBody();
        AssertUtil.notNull(lcpDictItem);
        BaseDOBuilder.fillBaseDOForInsert(lcpDictItem);
        lcpDictItemService.insertSelective(lcpDictItem);
        return Result.success();
    }

    /**
     * 修改
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("修改字典数据项")
    @PostMapping("/update")
    public Result update(@RequestBody WebRequest<LcpDictItem> webRequest) {
        LcpDictItem lcpDictItem = webRequest.getBody();
        AssertUtil.notNull(lcpDictItem);
        AssertUtil.notNull(lcpDictItem.getId());
        BaseDOBuilder.fillBaseDOForUpdate(lcpDictItem);
        lcpDictItemService.updateSelective(lcpDictItem);
        return Result.success();
    }

    /**
     * 删除
     *
     * @param webRequest 参数, 待删除主键,多个使用英文逗号拼接
     * @return
     */
    @Trace("删除字典数据项")
    @PostMapping("/delete")
    public Result delete(@RequestBody WebRequest<String> webRequest) {
        String ids = webRequest.getBody();
        AssertUtil.notBlank(ids);
        if (ids.contains(",")) {
            lcpDictItemService.batchDelete(Arrays.stream(ids.split(",")).map(Long::valueOf).collect(Collectors.toList()));
        } else {
            lcpDictItemService.delete(Long.valueOf(ids));
        }
        return Result.success();
    }

}
