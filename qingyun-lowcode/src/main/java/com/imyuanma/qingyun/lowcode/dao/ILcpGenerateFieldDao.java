package com.imyuanma.qingyun.lowcode.dao;

import com.imyuanma.qingyun.lowcode.model.data.LcpGenerateFieldDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 代码生成字段dao
 *
 * @author wangjy
 * @date 2022/05/04 14:27:09
 */
@Mapper
public interface ILcpGenerateFieldDao {

    /**
     * 插入字段配置
     *
     * @param lcpGenerateFieldDO 字段配置
     * @return
     */
    int insert(LcpGenerateFieldDO lcpGenerateFieldDO);

    /**
     * 更新
     * @param lcpGenerateFieldDO
     * @return
     */
    int update(LcpGenerateFieldDO lcpGenerateFieldDO);

    /**
     * 查询列表
     * @param lcpGenerateFieldDO
     * @return
     */
    List<LcpGenerateFieldDO> getList(LcpGenerateFieldDO lcpGenerateFieldDO);

    /**
     * 删除字段配置
     * @param generateId
     * @return
     */
    int deleteByGenerateId(Long generateId);
}
