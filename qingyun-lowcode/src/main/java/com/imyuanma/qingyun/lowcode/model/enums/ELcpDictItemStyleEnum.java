package com.imyuanma.qingyun.lowcode.model.enums;

/**
 * 样式枚举
 *
 * @author YuanMaKeJi
 * @date 2022-12-04 23:44:03
 */
public enum ELcpDictItemStyleEnum {
    CODE_primary("primary", "primary"),
    CODE_success("success", "success"),
    CODE_info("info", "info"),
    CODE_warning("warning", "warning"),
    CODE_danger("danger", "danger"),
    ;

    /**
     * code
     */
    private String code;
    /**
     * 文案
     */
    private String text;

    ELcpDictItemStyleEnum(String code, String text) {
        this.code = code;
        this.text = text;
    }

    /**
     * 根据code获取枚举
     *
     * @param code code值
     */
    public static ELcpDictItemStyleEnum getByCode(String code) {
        for (ELcpDictItemStyleEnum e : ELcpDictItemStyleEnum.values()) {
            if (e.code != null && e.code.equals(code)) {
                return e;
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
