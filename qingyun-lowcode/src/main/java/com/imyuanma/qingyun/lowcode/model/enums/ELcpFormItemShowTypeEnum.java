package com.imyuanma.qingyun.lowcode.model.enums;

/**
 * 表单项显隐类型
 *
 * @author wangjy
 * @date 2022/07/02 18:35:03
 */
public enum ELcpFormItemShowTypeEnum {
    SHOW(0, "总是显示"),
    HIDE(1, "总是隐藏"),
    ADD_HIDE(2, "新增时隐藏"),
    EDIT_HIDE(3, "编辑时隐藏"),
    CONDITION_HIDE(4, "满足条件时隐藏"),
    ;

    private Integer type;
    private String text;

    ELcpFormItemShowTypeEnum(Integer type, String text) {
        this.type = type;
        this.text = text;
    }

    public Integer getType() {
        return type;
    }

    public String getText() {
        return text;
    }
}
