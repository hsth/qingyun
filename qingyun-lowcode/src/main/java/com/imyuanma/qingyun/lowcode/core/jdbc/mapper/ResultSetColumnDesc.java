package com.imyuanma.qingyun.lowcode.core.jdbc.mapper;

import java.sql.ResultSetMetaData;
import java.sql.SQLException;

/**
 * jdbc结果列描述
 *
 * @author wangjy
 * @date 2023/04/21 22:38:07
 */
public class ResultSetColumnDesc {
    private String name;
    private String label;
    private int type;
    private String typeName;
    private String className;
    private int displaySize;

    public ResultSetColumnDesc(ResultSetMetaData rsMetaData, int column) throws SQLException {
        this.name = rsMetaData.getColumnName(column);
        this.label = rsMetaData.getColumnLabel(column);
        this.type = rsMetaData.getColumnType(column);
        this.typeName = rsMetaData.getColumnTypeName(column);
        this.className = rsMetaData.getColumnClassName(column);
        this.displaySize = rsMetaData.getColumnDisplaySize(column);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("{");
        sb.append("\"name\":").append(name != null ? "\"" : "").append(name).append(name != null ? "\"" : "");
        sb.append(", \"label\":").append(label != null ? "\"" : "").append(label).append(label != null ? "\"" : "");
        sb.append(", \"type\":").append(type);
        sb.append(", \"typeName\":").append(typeName != null ? "\"" : "").append(typeName).append(typeName != null ? "\"" : "");
        sb.append(", \"className\":").append(className != null ? "\"" : "").append(className).append(className != null ? "\"" : "");
        sb.append(", \"displaySize\":").append(displaySize);
        sb.append('}');
        return sb.toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public int getDisplaySize() {
        return displaySize;
    }

    public void setDisplaySize(int displaySize) {
        this.displaySize = displaySize;
    }
}
