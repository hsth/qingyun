<!DOCTYPE html>
<html lang="en">
<head>
    <script src="/static/js/dev.js"></script>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!--bootstrap插件-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="/static/plugin/element-ui/element-ui.css" rel="stylesheet"/>
    <!--字体插件-->
    <link href="/static/plugin/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
    <!--common-->
    <link href="/static/css/common.css" rel="stylesheet"/>
    <link href="/static/css/common-component.css" rel="stylesheet"/>
    <link href="/static/css/common-element-plus.css" rel="stylesheet"/>
    <link href="/static/css/common_biz.css" rel="stylesheet"/>
<#list model.extInfo.cssLinkList as css>
    <#if css.url?? && (css.url?length>0)>
    <link href="${css.url}" rel="stylesheet"/>
    </#if>
</#list>

    <title>${model.name!}</title>
    <style>
    <#if model.globalCss??>
        ${model.globalCss}
    </#if>
    </style>
</head>
<body class="skin-default" qy-loading>
<div id="app" class="qy-view-page-app">
    <el-scrollbar view-class="qy-el-scrollbar-height-p100">
        <div class="qy-view-page-content">

            <!-- 查询条件 -->
            <div id="qy-view-search-bar" class="qy-view-search-bar qy-height-resize">
                <div class="qy-view-search-condition-bar">
                    <el-form :inline="true" :model="searchCondition">
                <#if conditionFields?? && (conditionFields?size > 0)>
                    <#list conditionFields as field>
                        <el-form-item label="${field.listHeaderTitle!}" <#if (field_index > 2)>v-if="moreConditionFlag"</#if> >
                        <#if field.formControlType == 'date'>
                            <#if field.listQueryCondition?? && field.listQueryCondition==10>
                            <el-date-picker v-model="searchCondition.${field.fieldJavaName}" type="date" format="YYYY-MM-DD" value-format="YYYY-MM-DD"></el-date-picker>
                                &nbsp;~&nbsp;
                            <el-date-picker v-model="searchCondition.${field.fieldJavaName}2" type="date" format="YYYY-MM-DD" value-format="YYYY-MM-DD"></el-date-picker>
                            <#else>
                            <el-date-picker v-model="searchCondition.${field.fieldJavaName}" type="date" format="YYYY-MM-DD" value-format="YYYY-MM-DD"></el-date-picker>
                            </#if>
                        <#elseif field.formControlType == 'time'>
                            <#if field.listQueryCondition?? && field.listQueryCondition==10>
                            <el-date-picker v-model="searchCondition.${field.fieldJavaName}" type="datetime"
                                            format="YYYY-MM-DD HH:mm:ss" value-format="YYYY-MM-DD HH:mm:ss" :default-time="new Date('2022-10-09 08:00:00')"></el-date-picker>
                                &nbsp;~&nbsp;
                            <el-date-picker v-model="searchCondition.${field.fieldJavaName}2" type="datetime"
                                            format="YYYY-MM-DD HH:mm:ss" value-format="YYYY-MM-DD HH:mm:ss" :default-time="new Date('2022-10-09 08:00:00')"></el-date-picker>
                            <#else>
                            <el-date-picker v-model="searchCondition.${field.fieldJavaName}" type="datetime"
                                            format="YYYY-MM-DD HH:mm:ss" value-format="YYYY-MM-DD HH:mm:ss" :default-time="new Date('2022-10-09 08:00:00')"></el-date-picker>
                            </#if>
                        <#elseif field.formControlType == 'switch'>
                            <#if field.fieldJavaType == 'String'>
                                <el-switch v-model="searchCondition.${field.fieldJavaName}"
                                           <#if field.formControlConfig.yesValue?? && (field.formControlConfig.yesValue?length>0)>active-value="${field.formControlConfig.yesValue}"<#else>active-value="1"</#if>
                                        <#if field.formControlConfig.noValue?? && (field.formControlConfig.noValue?length>0)>inactive-value="${field.formControlConfig.noValue}"<#else>inactive-value="0"</#if>
                                ></el-switch>
                            <#elseif field.fieldJavaType == 'Boolean'>
                                <el-switch v-model="searchCondition.${field.fieldJavaName}"
                                           <#if field.formControlConfig.yesValue?? && (field.formControlConfig.yesValue?length>0)>:active-value="${field.formControlConfig.yesValue}"<#else>:active-value="true"</#if>
                                        <#if field.formControlConfig.noValue?? && (field.formControlConfig.noValue?length>0)>:inactive-value="${field.formControlConfig.noValue}"<#else>:inactive-value="false"</#if>
                                ></el-switch>
                            <#else>
                                <el-switch v-model="searchCondition.${field.fieldJavaName}"
                                           <#if field.formControlConfig.yesValue?? && (field.formControlConfig.yesValue?length>0)>:active-value="${field.formControlConfig.yesValue}"<#else>:active-value="1"</#if>
                                        <#if field.formControlConfig.noValue?? && (field.formControlConfig.noValue?length>0)>:inactive-value="${field.formControlConfig.noValue}"<#else>:inactive-value="0"</#if>
                                ></el-switch>
                            </#if>
                        <#elseif field.formControlType == 'select'>
                            <el-select v-model="searchCondition.${field.fieldJavaName}" :clearable="true">
                            <#if field.validDataSourceFlag>
                                <el-option v-for="option in ${field.fieldJavaName}Options" :value="option.${field.formControlDataSource.keyField!}" :label="option.${field.formControlDataSource.valueField!}"></el-option>
                            <#else >
                                <el-option value="" label="Option A"></el-option>
                            </#if>
                            </el-select>
                        <#elseif field.formControlType == 'treeSelect'>
                            <#if field.validDataSourceFlag>
                            <el-tree-select v-model="searchCondition.${field.fieldJavaName}" :clearable="true"
                                            :data="${field.fieldJavaName}Options" node-key="${field.formControlDataSource.keyField!}" :props="{label:'${field.formControlDataSource.valueField!}'<#if field.formControlConfig.childrenField?? && (field.formControlConfig.childrenField?length>0)>,children:'${field.formControlConfig.childrenField!}'</#if>}"
                                    <#if field.formControlConfig.treeSupportCheckAny?? && field.formControlConfig.treeSupportCheckAny == 1>:check-strictly="true"<#else>:check-strictly="false"</#if>
                                            :render-after-expand="false">
                            </el-tree-select>
                            <#else>
                            <el-tree-select v-model="searchCondition.${field.fieldJavaName}" :clearable="true"
                                            :data="${field.fieldJavaName}Options" node-key="id" :props="{label:'name',children:'children'}"
                                            :check-strictly="true" :render-after-expand="false">
                            </el-tree-select>
                            </#if>
                        <#elseif field.formControlType == 'iconSelect'>
                            <jo-el-icon-select v-model="searchCondition.${field.fieldJavaName}"></jo-el-icon-select>
                        <#elseif field.formControlType == 'radio'>
                            <el-radio-group v-model="searchCondition.${field.fieldJavaName}">
                            <#if field.validDataSourceFlag>
                                <el-radio v-for="option in ${field.fieldJavaName}Options" :label="option.${field.formControlDataSource.keyField!}">{{option.${field.formControlDataSource.valueField!}}}</el-radio>
                            <#else>
                                <el-radio label="1">Option A</el-radio>
                            </#if>
                            </el-radio-group>
                        <#elseif field.formControlType == 'checkbox'>
                            <el-checkbox-group v-model="searchCondition.${field.fieldJavaName}">
                            <#if field.validDataSourceFlag>
                                <el-checkbox v-for="option in ${field.fieldJavaName}Options" :label="option.${field.formControlDataSource.keyField!}">{{option.${field.formControlDataSource.valueField!}}}</el-checkbox>
                            <#else>
                                <el-checkbox label="1">Option A</el-checkbox>
                            </#if>
                            </el-checkbox-group>
                        <#elseif field.formControlType == 'number'>
                            <#assign numberHtmlAttr = "" />
                            <#if field.formControlConfig.min??>
                                <#assign numberHtmlAttr = (numberHtmlAttr + " :min=\"" + field.formControlConfig.min + "\"") />
                            </#if>
                            <#if field.formControlConfig.max??>
                                <#assign numberHtmlAttr = (numberHtmlAttr + " :max=\"" + field.formControlConfig.max + "\"") />
                            </#if>
                            <#if field.formControlConfig.step??>
                                <#assign numberHtmlAttr = (numberHtmlAttr + " :step=\"" + field.formControlConfig.step + "\"") />
                            </#if>
                            <#if !field.formControlConfig.numberControls?? || field.formControlConfig.numberControls==0>
                                <#assign numberHtmlAttr = (numberHtmlAttr + " :controls=\"false\"") />
                            </#if>
                            <#if field.listQueryCondition?? && field.listQueryCondition==10>
                                <el-input-number v-model="searchCondition.${field.fieldJavaName}" ${numberHtmlAttr} ></el-input-number>
                                &nbsp;~&nbsp;
                                <el-input-number v-model="searchCondition.${field.fieldJavaName}2" ${numberHtmlAttr} ></el-input-number>
                            <#else>
                                <el-input-number v-model="searchCondition.${field.fieldJavaName}" ${numberHtmlAttr} ></el-input-number>
                            </#if>
                        <#else>
                            <el-input v-model="searchCondition.${field.fieldJavaName}" placeholder=""></el-input>
                        </#if>
                        </el-form-item>
                    </#list>
                    <#if (conditionFields?size > 3)>
                        <el-form-item>
                            <el-button type="primary" link @click="moreConditionFlag = true" v-if="!moreConditionFlag">更多条件</el-button>
                            <el-button type="primary" link @click="moreConditionFlag = false" v-if="moreConditionFlag">收起条件</el-button>
                        </el-form-item>
                    </#if>
                </#if>
                        <el-form-item>
                            <el-button type="primary" @click="list_search"><i class="fa fa-search" aria-hidden="true"></i>&nbsp;查询</el-button>
                        </el-form-item>
                    </el-form>
                </div>
<#--                <div class="qy-view-search-button-bar">-->
<#--                    <el-form :inline="true" :model="searchCondition">-->
<#--                        <el-form-item>-->
<#--                            <el-button type="primary" @click="list_search"><i class="fa fa-search" aria-hidden="true"></i>&nbsp;查询</el-button>-->
<#--                        </el-form-item>-->
<#--                    </el-form>-->
<#--                </div>-->
            </div>


        <#if model.extInfo?? && model.extInfo.buttonBarConfig?? && model.extInfo.buttonBarConfig.buttonList?? && (model.extInfo.buttonBarConfig.buttonList?size>0)>
            <!-- 按钮区 -->
            <div id="qy-view-button-bar" class="qy-view-button-bar qy-height-resize">
            <#list model.extInfo.buttonBarConfig.buttonList as btn>
                <el-button type="${btn.type!}" <#if btn.category??>${btn.category!}</#if>
                        <#if btn.size??>size="${btn.size}"</#if>
                        <#if btn.clickFunctionName?? && (btn.clickFunctionName?length>0)>@click="${btn.clickFunctionName}"</#if>
                        <#if btn.showRule?? && (btn.showRule?length>0)>v-if="${btn.showRule}"</#if>
                ><#if btn.icon?? && (btn.icon?length>0)>${btn.icon}&nbsp;</#if>${btn.name}</el-button>
            </#list>
            <#if model.supportImportFlag>
                <jo-el-upload-btn storage="" @submit-upload="list_import_excel" :template-url="templateUrl" :progress="true">导入</jo-el-upload-btn>
            </#if>
            <#if model.supportExportFlag>
                <el-button type="primary" @click="list_export_excel" plain>导出</el-button>
            </#if>
            </div>
        </#if>

            <!-- 数据表格 -->
            <div id="qy-view-data-bar" class="qy-view-data-bar qy-height-resize">
                <#--首列类型-->
                <#assign firstColumnType = (model.extInfo.tableFirstColumnType?? && model.extInfo.tableFirstColumnType?length>0)?string(model.extInfo.tableFirstColumnType,'none')  />
                <jo-el-table :url="tableDataUrl" :ref="joTableRef" :search-param="searchCondition" first-column="${firstColumnType!}" <#if model.extInfo.pageSize??>:page-size="${model.extInfo.pageSize}"</#if> <#if model.extInfo.enableTableLoading?? && model.extInfo.enableTableLoading == 1>:enable-loading="true"</#if> >
                    <template #default="scope">
                        <el-table :ref="tableRef" :data="scope.data" header-cell-class-name="jo-el-table-header"
                                  <#if model.extInfo.supportTreeTable?? && model.extInfo.supportTreeTable==1>row-key="<#if model.extInfo.treeTableRowKey?? && (model.extInfo.treeTableRowKey?length>0)>${model.extInfo.treeTableRowKey}<#else>id</#if>"</#if>
                                  :tree-props="{ children: '<#if model.extInfo.treeTableChildrenField?? && (model.extInfo.treeTableChildrenField?length>0)>${model.extInfo.treeTableChildrenField}<#else>children</#if>' }"
                                  @sort-change="list_table_sort"
                                  :max-height="tableHeight">
                        <#if firstColumnType == 'radio'>
                            <el-table-column type="radio" width="50" fixed="left" header-align="center" align="center">
                                <template #default="scope2">
                                    <el-radio v-model="scope.radio.value" :label="scope2.$index"><span></span></el-radio>
                                </template>
                            </el-table-column>
                        <#elseif firstColumnType == 'checkbox'>
                            <el-table-column type="selection" prop="selection" width="50" fixed="left" label="#" header-align="center" align="center"></el-table-column>
                        <#elseif firstColumnType == 'index'>
                            <el-table-column type="index" width="50" fixed="left" label="#" header-align="center" align="center"></el-table-column>
                        <#else>
                        </#if>
                    <#list listFields as field>
                        <#--过滤掉不在列表展示的-->
                        <#if field.listColumnShow == 1>
                            <el-table-column prop="${field.fieldJavaName}" min-width="${field.listColumnWidth!}" header-align="${field.listHeaderAlign!}" align="${field.listValueAlign!}" <#if field.listColumnFixed?? && field.listColumnFixed != 'no'>fixed="${field.listColumnFixed!}"</#if> <#if field.listColumnSort?? && field.listColumnSort == 1>sortable="custom"</#if> >
                                <template #header="scope"><span title="${field.listHeaderTips!}">${field.listHeaderTitle!}</span></template>
                                <#if field.listValueFormat != 'text'>
                                <template #default="scope">
                                    <#if field.listValueFormat == 'html'>
                                        <div v-html="scope.row.${field.fieldJavaName}"></div>
                                    <#elseif field.listValueFormat == 'tag'>
                                        <#if field.formControlType == 'switch'>
                                            <jo-el-tag :value="scope.row.${field.fieldJavaName}" :options="${field.fieldJavaName}Options"
                                                       code-field="key" text-field="value"></jo-el-tag>
                                        <#else>
                                        <jo-el-tag :value="scope.row.${field.fieldJavaName}" <#if field.validDataSourceFlag>:options="${field.fieldJavaName}Options"</#if>
                                                   code-field="${field.formControlDataSource.keyField}" text-field="${field.formControlDataSource.valueField}"></jo-el-tag>
                                        </#if>
                                    <#elseif field.listValueFormat == 'time'>
                                        {{ formatTime(scope.row.${field.fieldJavaName}) }}
                                    <#elseif field.listValueFormat == 'date'>
                                        {{ formatDate(scope.row.${field.fieldJavaName}) }}
                                    <#elseif field.listValueFormat == 'link'>
                                        <el-link type="primary" :href="scope.row.${field.fieldJavaName}" target="_blank">链接</el-link>
                                    <#elseif field.listValueFormat == 'image'>
                                        <el-image :src="scope.row.${field.fieldJavaName}" style="width: 100px; height: 100px"></el-image>
                                    <#elseif field.listValueFormat == 'button'>
                                        <el-button type="primary">{{ scope.row.${field.fieldJavaName} }}</el-button>
                                    <#elseif field.listValueFormat == 'formatter'>
                                        <#if field.formControlConfig?? && field.formControlConfig.listValueFormatHtml?? && (field.formControlConfig.listValueFormatHtml?length>0)>
                                            ${field.formControlConfig.listValueFormatHtml!}
                                        <#else>
                                            {{ scope.row.${field.fieldJavaName} }}
                                        </#if>
                                    <#elseif field.listValueFormat == 'file'>
                                        <jo-el-file-show :file-id="scope.row.${field.fieldJavaName}"></jo-el-file-show>
                                    <#else>
                                        {{ scope.row.${field.fieldJavaName} }}
                                    </#if>
                                </template>
                                </#if>
                            </el-table-column>
                        </#if>
                    </#list>
                        <#--操作按钮列-->
                        <#if model.extInfo?? && model.extInfo.tableRowButtonConfig?? && model.extInfo.tableRowButtonConfig.buttonList?? && (model.extInfo.tableRowButtonConfig.buttonList?size>0)>
                            <#--表格行操作列宽度计算-->
                            <#assign operWidth = model.extInfo.tableRowButtonConfig.buttonBarWidth />
                            <el-table-column label="操作" width="${operWidth}" fixed="right">
                                <template #default="scope">
                                <#list model.extInfo.tableRowButtonConfig.buttonList as btn>
                                <#if (btn_index > 0) >
                                    <el-divider direction="vertical"></el-divider>
                                </#if>
                                    <el-button type="${btn.type!}" <#if btn.category??>${btn.category!}</#if>
                                            <#if btn.size??>size="${btn.size}"</#if>
                                            <#if btn.clickFunctionName?? && (btn.clickFunctionName?length>0)>@click="${btn.clickFunctionName}"</#if>
                                            <#if btn.showRule?? && (btn.showRule?length>0)>v-if="${btn.showRule}"</#if>
                                    ><#if btn.icon?? && (btn.icon?length>0)>${btn.icon}&nbsp;</#if>${btn.name}</el-button>
                                </#list>
                                </template>
                            </el-table-column>
                        </#if>
                        </el-table>
                    </template>
                </jo-el-table>
            </div>

        </div>

        <!-- 表单 -->
    <#if model.extInfo.formDialogType?has_content && model.extInfo.formDialogType=='drawer'>
        <el-drawer v-model="formShow" :title="formTitle" :size="formWidth" :destroy-on-close="true">
    <#else >
        <el-dialog v-model="formShow" :title="formTitle" :width="formWidth" :draggable="true" :destroy-on-close="true">
    </#if>
            <el-form :ref="formRef" :model="formData" label-position="right" :rules="checkRules" :label-width="formLabelWidth">
                <el-row :gutter="15">
            <#list formFields as field>
            <#--新增时隐藏-->
                <#assign addHide = field.formColumnShowConfig.addHide />
            <#--编辑时隐藏-->
                <#assign editHide = field.formColumnShowConfig.editHide />
            <#--指定条件表达式隐藏-->
                <#assign conditionHide = field.formColumnShowConfig.conditionHide/>
            <#--新增时禁用-->
                <#assign addDisable = field.formColumnShowConfig.addDisable />
            <#--编辑时禁用-->
                <#assign editDisable = field.formColumnShowConfig.editDisable />
            <#--指定条件表达式隐藏-->
                <#assign conditionDisable = field.formColumnShowConfig.conditionDisable />
            <#--禁用条件html代码段 <#if addDisable>:disabled="addFlag"<#elseif editDisable>:disabled="editFlag"</#if> -->
                <#assign disableHtml>
                    <#if addDisable && editDisable>
                    :disabled="true"
                    <#elseif addDisable>
                    :disabled="addFlag"
                    <#elseif editDisable>
                    :disabled="editFlag"
                    <#elseif conditionDisable>
                    :disabled="${field.formColumnShowConfig.disableCondition}"
                    <#else >

                    </#if>
                </#assign>
            <#--过滤掉不展示的-->
                <#if field.formColumnShow == 1 && (!field.formColumnShowConfig.showType?? || field.formColumnShowConfig.showType != 1)>
                    <el-col :span="${field.formColumnGridWidth!}" <#if addHide>v-if="!addFlag"<#elseif editHide>v-if="!editFlag"<#elseif conditionHide>v-if="!(${field.formColumnShowConfig.hideCondition})"</#if> >
                        <el-form-item label="${field.formControlTitle!}" prop="${field.fieldJavaName}">
                        <#if field.formControlType == 'date'>
                            <el-date-picker v-model="formData.${field.fieldJavaName}" type="date" ${disableHtml!} placeholder="${field.formControlPlaceholder!}" format="YYYY-MM-DD" value-format="YYYY-MM-DD"></el-date-picker>
                        <#elseif field.formControlType == 'time'>
                            <el-date-picker v-model="formData.${field.fieldJavaName}" type="datetime" ${disableHtml!} placeholder="${field.formControlPlaceholder!}" format="YYYY-MM-DD HH:mm:ss" value-format="YYYY-MM-DD HH:mm:ss" :default-time="new Date('2022-10-09 08:00:00')"></el-date-picker>
                        <#elseif field.formControlType == 'switch'>
                            <#if field.fieldJavaType == 'String'>
                            <el-switch v-model="formData.${field.fieldJavaName}" ${disableHtml!}
                                       <#if field.formControlConfig.yesValue?? && (field.formControlConfig.yesValue?length>0)>active-value="${field.formControlConfig.yesValue}"<#else>active-value="1"</#if>
                                    <#if field.formControlConfig.noValue?? && (field.formControlConfig.noValue?length>0)>inactive-value="${field.formControlConfig.noValue}"<#else>inactive-value="0"</#if>
                            ></el-switch>
                            <#elseif field.fieldJavaType == 'Boolean'>
                            <el-switch v-model="formData.${field.fieldJavaName}" ${disableHtml!}
                                       <#if field.formControlConfig.yesValue?? && (field.formControlConfig.yesValue?length>0)>:active-value="${field.formControlConfig.yesValue}"<#else>:active-value="true"</#if>
                                    <#if field.formControlConfig.noValue?? && (field.formControlConfig.noValue?length>0)>:inactive-value="${field.formControlConfig.noValue}"<#else>:inactive-value="false"</#if>
                            ></el-switch>
                            <#else>
                            <el-switch v-model="formData.${field.fieldJavaName}" ${disableHtml!}
                                       <#if field.formControlConfig.yesValue?? && (field.formControlConfig.yesValue?length>0)>:active-value="${field.formControlConfig.yesValue}"<#else>:active-value="1"</#if>
                                    <#if field.formControlConfig.noValue?? && (field.formControlConfig.noValue?length>0)>:inactive-value="${field.formControlConfig.noValue}"<#else>:inactive-value="0"</#if>
                            ></el-switch>
                            </#if>
                        <#elseif field.formControlType == 'select'>
                            <el-select v-model="formData.${field.fieldJavaName}" ${disableHtml!} placeholder="${field.formControlPlaceholder!}">
                                <#if field.validDataSourceFlag>
                                    <el-option v-for="option in ${field.fieldJavaName}Options" :value="option.${field.formControlDataSource.keyField!}" :label="option.${field.formControlDataSource.valueField!}"></el-option>
                                <#else>
                                    <el-option value="" label="Option A"></el-option>
                                </#if>
                            </el-select>
                        <#elseif field.formControlType == 'treeSelect'>
                            <#if field.validDataSourceFlag>
                            <el-tree-select v-model="formData.${field.fieldJavaName}" placeholder="${field.formControlPlaceholder!}" :clearable="true"
                                    ${disableHtml!}
                                    :data="${field.fieldJavaName}Options" node-key="${field.formControlDataSource.keyField!}" :props="{label:'${field.formControlDataSource.valueField!}'<#if field.formControlConfig.childrenField?? && (field.formControlConfig.childrenField?length>0)>,children:'${field.formControlConfig.childrenField!}'</#if>}"
                                    <#if field.formControlConfig.treeSupportCheckAny?? && field.formControlConfig.treeSupportCheckAny == 1>:check-strictly="true"<#else>:check-strictly="false"</#if>
                                    :render-after-expand="false">
                            </el-tree-select>
                            <#else>
                            <el-tree-select v-model="formData.${field.fieldJavaName}" placeholder="${field.formControlPlaceholder!}" :clearable="true"
                                    ${disableHtml!}
                                    :data="${field.fieldJavaName}Options" node-key="id" :props="{label:'name',children:'children'}"
                                    :check-strictly="true" :render-after-expand="false">
                            </el-tree-select>
                            </#if>
                        <#elseif field.formControlType == 'iconSelect'>
                            <jo-el-icon-select v-model="formData.${field.fieldJavaName}" ${disableHtml!} placeholder="${field.formControlPlaceholder!}"></jo-el-icon-select>
                        <#elseif field.formControlType == 'radio'>
                            <el-radio-group v-model="formData.${field.fieldJavaName}" ${disableHtml!} >
                            <#if field.validDataSourceFlag>
                                <el-radio v-for="option in ${field.fieldJavaName}Options" :label="option.${field.formControlDataSource.keyField!}">{{option.${field.formControlDataSource.valueField!}}}</el-radio>
                            <#else>
                                <el-radio label="1">Option A</el-radio>
                            </#if>
                            </el-radio-group>
                        <#elseif field.formControlType == 'checkbox'>
                            <el-checkbox-group v-model="formData.${field.fieldJavaName}" ${disableHtml!} >
                            <#if field.validDataSourceFlag>
                                <el-checkbox v-for="option in ${field.fieldJavaName}Options" :label="option.${field.formControlDataSource.keyField!}">{{option.${field.formControlDataSource.valueField!}}}</el-checkbox>
                            <#else>
                                <el-checkbox label="1">Option A</el-checkbox>
                            </#if>
                            </el-checkbox-group>
                        <#elseif field.formControlType == 'textarea'>
                            <el-input type="textarea" v-model="formData.${field.fieldJavaName}" ${disableHtml!} placeholder="${field.formControlPlaceholder!}" <#if field.formControlConfig.rows??>:rows="${field.formControlConfig.rows}"</#if> <#if field.formControlConfig.autosize?? && field.formControlConfig.autosize=1>:autosize="true"</#if> ></el-input>
                        <#elseif field.formControlType == 'number'>
                            <el-input-number v-model="formData.${field.fieldJavaName}" ${disableHtml!} placeholder="${field.formControlPlaceholder!}" <#if field.formControlConfig.min??>:min="${field.formControlConfig.min}"</#if> <#if field.formControlConfig.max??>:max="${field.formControlConfig.max}"</#if> <#if field.formControlConfig.step??>:step="${field.formControlConfig.step}"</#if> <#if !field.formControlConfig.numberControls?? || field.formControlConfig.numberControls==0>:controls="false"</#if> ></el-input-number>
                        <#elseif field.formControlType == 'upload'>
                            <jo-el-form-upload v-model="formData.${field.fieldJavaName}" ${disableHtml!} placeholder="${field.formControlPlaceholder!}" ${field.formControlConfig.buildUploadAttrHtml()!} ></jo-el-form-upload>
                        <#elseif field.formControlType == 'editor_md'>
                            <jo-el-editor v-model="formData.${field.fieldJavaName}" <#if field.formControlConfig.editorHtmlBind?has_content>v-model:html-code="formData.${field.formControlConfig.editorHtmlBind}"</#if> ${disableHtml!} placeholder="${field.formControlPlaceholder!}" <#if field.formControlConfig.editorMdHeight?has_content>:height="${field.formControlConfig.editorMdHeight}"</#if> <#if field.formControlConfig.editorMdToolbar?has_content>toolbar="${field.formControlConfig.editorMdToolbar}"</#if> ></jo-el-editor>
                        <#else>
                            <el-input v-model="formData.${field.fieldJavaName}" ${disableHtml!} placeholder="${field.formControlPlaceholder!}"></el-input>
                        </#if>
                        </el-form-item>
                    </el-col>
                </#if>
            </#list>
                </el-row>
            </el-form>
            <template #footer>
                <span class="dialog-footer">
                    <el-button @click="close_form">关闭</el-button>
                    <el-button type="primary" @click="form_save" v-if="addSaveBtnShow && addFlag">确认</el-button>
                    <el-button type="primary" @click="form_save" v-else-if="updateSaveBtnShow && editFlag">确认</el-button>
                </span>
            </template>
    <#if model.extInfo.formDialogType?has_content && model.extInfo.formDialogType=='drawer'>
        </el-drawer>
    <#else >
        </el-dialog>
    </#if>

    <#list model.fieldList as field>
        <#if field.formControlDataSource??>
            <#if field.formControlDataSource.dataType == 'static'>
            <#elseif field.formControlDataSource.dataType == 'url'>
        <!-- ${field.formControlTitle}(${field.fieldJavaName})枚举项加载 -->
        <jo-el-data v-model="${field.fieldJavaName}Options" url="${field.formControlDataSource.dataUrl!}"></jo-el-data>
            <#elseif field.formControlDataSource.dataType == 'dict'>
        <jo-el-data v-model="${field.fieldJavaName}Options" url="/lowcode/uniView/dict/list/dict/${field.formControlDataSource.dictCode!}"></jo-el-data>
            </#if>
        </#if>
    </#list>

    </el-scrollbar>
</div>


<!--配置信息-->
<script src="/static/js/config.js"></script>
<!--jquery-->
<script src="/static/plugin/jquery/jquery-3.3.1.js"></script>
<script src="/static/plugin/jquery/jquery.cookie.js"></script>
<!--layer-->
<!--<script src="/static/plugin/layer/layer.js"></script>-->
<!--vue-->
<script src="/static/plugin/vue/v3/vue.global.js"></script>
<script src="/static/plugin/element-ui/element-ui.js"></script>
<script src="/static/plugin/element-ui/locale/zh-cn.js"></script>
<!--[if lt IE 9]>
<script src="/static/plugin/other/html5shiv.js"></script>
<script src="/static/plugin/other/respond.min.js"></script>
<![endif]-->
<#if model.dependTencentCos()>
<!--cos对象存储-->
<script src="/static/js/fs/cos-js-sdk-v5.js"></script>
</#if>
<#if model.dependEditorMd()>
<!--md编辑器-->
<link rel="stylesheet" href="/static/plugin/editor_md/css/editormd.css"/>
<link rel="stylesheet" href="/static/css/adapt/editor-md-adapt.css"/>
<script src="/static/plugin/editor_md/editormd.min.js"></script>
</#if>
<!--jo-->
<script src="/static/plugin/jo/jo.js"></script>
<script src="/static/plugin/jo/jo-adapt.js"></script>
<script src="/static/plugin/jo/jo-adapt-element-plus.js"></script>
<script src="/static/plugin/jo/jo-page-view.js"></script>
<script src="/static/plugin/jo/jo-page-form.js"></script>
<script src="/static/plugin/jo/jo-listener.js"></script>
<script src="/static/plugin/jo/jo-page-element-plus.js"></script>
<!--common-->
<script src="/static/js/common.js"></script>
<script src="/static/js/common_biz.js"></script>
<#list model.extInfo.jsLinkList as js>
<#if js.url?? && (js.url?length>0)>
<script src="${js.url}"></script>
</#if>
</#list>
<#--  自定义事件定义,单独放在这里,避免配置错误整坏页面  -->
<script type="text/javascript">
    // 代码生成按钮事件定义
    window.TEMP_BTN_FUNCTION = {
        methods: {
            // 自定义按钮栏事件 start
<#if model.extInfo.buttonBarConfig?? && model.extInfo.buttonBarConfig.buttonList?? && (model.extInfo.buttonBarConfig.buttonList?size>0)>
    <#list model.extInfo.buttonBarConfig.buttonList as btn>
        <#if btn.clickFunctionName?? && (btn.clickFunctionName?length>0) && btn.clickScript?? && (btn.clickScript?length>0)>
            ${btn.clickScript},
        </#if>
    </#list>
</#if>
            // 自定义按钮栏事件 end

            // 自定义表格行按钮事件 start
<#if model.extInfo.tableRowButtonConfig?? && model.extInfo.tableRowButtonConfig.buttonList?? && (model.extInfo.tableRowButtonConfig.buttonList?size>0)>
    <#list model.extInfo.tableRowButtonConfig.buttonList as btn>
        <#if btn.clickFunctionName?? && (btn.clickFunctionName?length>0) && btn.clickScript?? && (btn.clickScript?length>0)>
            ${btn.clickScript},
        </#if>
    </#list>
</#if>
            // 自定义表格行按钮事件 end
        }
    };
</script>
<script type="text/javascript">
    // vue参数, 留着给全局js定制用, 如果有需要定制vue参数, 则覆盖TEMP_VUE_PARAM
    window.TEMP_VUE_PARAM = {};

<#if model.globalJs??>
    ${model.globalJs}
</#if>
</script>
<script type="text/javascript">
    // vue应用构造
    const app = Vue.createApp(joEl.buildVueAppParam(window.TEMP_VUE_PARAM || {}, {
        data: function () {
            return {
                // 表格高度
                tableHeight: 'auto',
                // 表单宽度
                formWidth: <#if model.extInfo.formWidth?? && (model.extInfo.formWidth?length>0)>'${model.extInfo.formWidth}'<#else>'60%'</#if>,
                // 表单项标题宽度
                formLabelWidth: <#if model.formLabelWidth?? && (model.formLabelWidth?length>0)>'${model.formLabelWidth}'<#else>'100px'</#if>,
                // 是否支持更新保存按钮展示
                updateSaveBtnShow: <#if model.extInfo.hideFormUpdateFlag?? && model.extInfo.hideFormUpdateFlag == 1>false<#else>true</#if>,
                // 是否支持新增保存按钮展示
                addSaveBtnShow: true,
                // 查询参数
                searchCondition: {},
                // 表格数据url
                tableDataUrl: '${tableDataUrl}',
                // 表单详情查询url
                formQueryUrl: '${formQueryUrl}',
                // 新增url
                formInsertUrl: '${formInsertUrl}',
                // 修改url
                formUpdateUrl: '${formUpdateUrl}',
                // 删除url
                deleteUrl: '${deleteUrl}',
                // 模板下载url
                templateUrl: '${templateUrl}',
                // 导入url
                importUrl: '${importUrl!}',
                // 导出url
                exportUrl: '${exportUrl!}',
                // 校验规则
                checkRules: {
                    <#list formFields as field>
                        <#if field.formColumnShow == 1 && field.formControlCheckRule?? && field.formControlCheckRule.checkRuleList??
                            && (field.formControlCheckRule.checkRuleList?size > 0)>
                    ${field.fieldJavaName}: [
                            <#list field.formControlCheckRule.checkRuleList as rule>
                                <#if rule.checkType == 'ErrEmpty'>
                        joEl.rules.required('${rule.errorTips}', 'blur'),
                                <#elseif rule.checkType == 'ErrLength'>
                        joEl.rules.length('${rule.errorTips}', 'blur', ${rule.max}),
                                <#elseif rule.checkType == 'ErrNumber'>
                        joEl.rules.notNaN('${rule.errorTips}', 'blur'),
                                <#elseif rule.checkType == 'ErrMail'>
                        joEl.rules.mail('${rule.errorTips}', 'blur'),
                                <#elseif rule.checkType == 'ErrPhone'>
                        joEl.rules.phone('${rule.errorTips}', 'blur'),
                                <#elseif rule.checkType == 'ErrReg'>
                        joEl.rules.reg('${rule.errorTips}', 'blur', '${rule.checkReg}'),
                                </#if>
                            </#list>
                    ],
                        </#if>
                    </#list>
                },
                <#if model.existsIconSelectField>
                // 图标选择数据集
                joEl_constants_iconOptions: joEl.constants.iconOptions,
                </#if>
        <#--枚举数据-->
        <#list model.fieldList as field>
            <#if field.validDataSourceFlag>
                <#if field.formControlDataSource.dataType == 'static'
                    && field.formControlDataSource.optionItemList??
                    && (field.formControlDataSource.optionItemList?size > 0)>
                // 数据选项: ${field.listHeaderTitle!}
                ${field.fieldJavaName}Options: [
                   <#list field.formControlDataSource.optionItemList as option>
                    {${field.formControlDataSource.keyField}: <#if !field.numberFlag>'</#if>${option.key!}<#if !field.numberFlag>'</#if>, ${field.formControlDataSource.valueField}: '${option.value!}', styleType: '${option.styleType!}'},
                   </#list>
                ],
                <#elseif field.formControlDataSource.dataType == 'url'>
                // 数据选项: ${field.listHeaderTitle!}
                ${field.fieldJavaName}Options: [],
                <#elseif field.formControlDataSource.dataType == 'dict'>
                // 数据选项: ${field.listHeaderTitle!}
                ${field.fieldJavaName}Options: [],
                </#if>
            <#elseif field.formControlType == 'switch'>
                // 数据选项: ${field.listHeaderTitle!}
                ${field.fieldJavaName}Options: [
                    <#if field.formControlConfig??>
                    {key: <#if !field.numberFlag>'</#if>${field.formControlConfig.yesValue!}<#if !field.numberFlag>'</#if>, value: '${field.formControlConfig.yesText!}'},
                    {key: <#if !field.numberFlag>'</#if>${field.formControlConfig.noValue!}<#if !field.numberFlag>'</#if>, value: '${field.formControlConfig.noText!}', styleType: 'danger'},
                    </#if>
                ],
            </#if>
        </#list>

            }
        }
        , computed: {}
        , methods: {
            // 新增表单数据初始化前的处理
            handle_form_init_data_add(data) {
                // 默认值
                <#list formFields as field>
                <#if field.formColumnShow == 1 && field.formControlDefault?? && (field.formControlDefault?length > 0)>
                data.${field.fieldJavaName} = <#if field.numberFlag>${field.formControlDefault}<#else>'${field.formControlDefault}'</#if>;
                </#if>
                </#list>
            },
            // 编辑表单数据初始化前的处理
            handle_form_init_data_edit(data) {
            },

            // 数据区高度适应
            dataBarResize() {
                // var total = 0;
                // jo.forEach($('.qy-view-data-bar').siblings('.qy-height-resize'), (item) => {
                //     var id = item.id;
                //     if (id) {
                //         total += jo.getDefVal($('#' + id).outerHeight(true), 0);
                //     }
                // });
                // var boxPadding = $('.qy-view-page-content').outerHeight(true) - $('.qy-view-page-content').height();
                // var h = $('.qy-view-page-app').height() - boxPadding - total - jo.getDefVal($('.jo-el-table-page-bar').outerHeight(true));
                var h = this.calcDataBarHeight();
                console.info('更新数据表格高度: %s -> %s', this.tableHeight, h);
                this.tableHeight = h;
            },
            // 数据区监听
            dataBarResizeListener() {
                // 初始计算
                this.dataBarResize();
                // 监听注册
                var arr = $('.qy-view-data-bar').siblings('.qy-height-resize');
                jo.forEach(arr, (item) => {
                    var id = item.id;
                    if (id) {
                        this.registerElementHeightResize('#' + item.id, this.dataBarResize);
                    } else {
                        console.warn('注册数据区高度监听需要兄弟节点有id属性:', item);
                    }
                });
                // 数据区高度变化
                // this.registerElementHeightResize('#qy-view-data-bar', this.dataBarResize);
            },
        }
        , mounted() {
            // 页面加载完成
            this.qy_page_loaded();
            // 数据区大小监听
            this.dataBarResizeListener();
        }
        , setup() {

        }
    }, window.TEMP_BTN_FUNCTION || {}, joEl.VUE_COMMON_VIEW, joEl.VUE_COMMON));
    app.use(ElementPlus, {locale: ElementPlusLocaleZhCn});
    app.use(joEl);

    var appVM = app.mount("#app");
</script>

</body>
</html>
