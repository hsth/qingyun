package ${model.packagePath}.${model.moduleName}.service;

import com.imyuanma.qingyun.common.model.PageQuery;
import ${model.packagePath}.${model.moduleName}.model.${model.className};

import java.util.List;

/**
 * ${model.name!}服务
 *
 * @author ${model.author!}
 * @date ${nowTimeStr}
 */
public interface I${model.className}Service {

    /**
     * 列表查询
     *
     * @param ${model.className?uncap_first} 查询条件
     * @return
     */
    List<${model.className}> getList(${model.className} ${model.className?uncap_first});

    /**
     * 分页查询
     *
     * @param ${model.className?uncap_first} 查询条件
     * @param pageQuery 分页参数
     * @return
     */
    List<${model.className}> getPage(${model.className} ${model.className?uncap_first}, PageQuery pageQuery);

    /**
     * 统计数量
     *
     * @param ${model.className?uncap_first} 查询条件
     * @return
     */
    int count(${model.className} ${model.className?uncap_first});

    /**
     * 主键查询
     *
     * @param ${primaryKeyJavaName} 主键
     * @return
     */
    ${model.className} get(${primaryKeyJavaType} ${primaryKeyJavaName});

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    List<${model.className}> getListByIds(List<${primaryKeyJavaType}> list);
<#list fieldSearchConfigList as item>

    /**
     * 根据${item.fieldJavaNameJoinStr}查询
     *
<#list item.fieldList as field>
     * @param ${field.fieldJavaName} ${field.fieldDbRemark}
</#list>
     * @return
     */
    <#if !item.selectOneFlag>List${"<"}</#if>${model.className}<#if !item.selectOneFlag>${">"}</#if> ${item.methodName}(<#list item.fieldList as field><#if (field_index > 0)>, </#if>${field.fieldJavaType} ${field.fieldJavaName}</#list>);
</#list>

    /**
     * 插入
     *
     * @param ${model.className?uncap_first} 参数
     * @return
     */
    int insert(${model.className} ${model.className?uncap_first});

    /**
     * 选择性插入
     *
     * @param ${model.className?uncap_first} 参数
     * @return
     */
    int insertSelective(${model.className} ${model.className?uncap_first});

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    int batchInsert(List<${model.className}> list);

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    int batchInsertSelective(List<${model.className}> list);

    /**
     * 修改
     *
     * @param ${model.className?uncap_first} 参数
     * @return
     */
    int update(${model.className} ${model.className?uncap_first});

    /**
     * 选择性修改
     *
     * @param ${model.className?uncap_first} 参数
     * @return
     */
    int updateSelective(${model.className} ${model.className?uncap_first});

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    int delete(${primaryKeyJavaType} id);

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    int batchDelete(List<${primaryKeyJavaType}> list);

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param ${model.className?uncap_first} 参数
     * @return
     */
    int deleteByCondition(${model.className} ${model.className?uncap_first});

}
