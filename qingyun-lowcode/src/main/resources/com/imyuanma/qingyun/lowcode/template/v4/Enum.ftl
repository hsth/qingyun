package ${model.packagePath}.${model.moduleName}.model.enums;
<#assign enumClassName = "E" + model.className + enumFieldNameUpperFirst + "Enum" />

/**
 * ${enumField.formControlTitle!}枚举
 *
 * @author ${model.author!}
 * @date ${nowTimeStr}
 */
public enum ${enumClassName} {
<#if enumField.numberFlag>
    <#list enumOptionList as option>
    CODE_${option.key}(${option.key}, "${option.value}"),
    </#list>
<#else>
    <#list enumOptionList as option>
    CODE_${option.key}("${option.key}", "${option.value}"),
    </#list>
</#if>
    ;

    /**
     * code
     */
    private ${enumField.fieldJavaType} code;
    /**
     * 文案
     */
    private String text;

    ${enumClassName}(${enumField.fieldJavaType} code, String text) {
        this.code = code;
        this.text = text;
    }

    /**
     * 根据code获取枚举
     *
     * @param code code值
     */
    public static ${enumClassName} getByCode(${enumField.fieldJavaType} code) {
        for (${enumClassName} e : ${enumClassName}.values()) {
            if (e.code != null && e.code.equals(code)) {
                return e;
            }
        }
        return null;
    }

    public ${enumField.fieldJavaType} getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
