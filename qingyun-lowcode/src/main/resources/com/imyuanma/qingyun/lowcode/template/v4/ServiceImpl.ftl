package ${model.packagePath}.${model.moduleName}.service.impl;

import com.imyuanma.qingyun.common.model.PageQuery;
<#if model.supportTraceAll>
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
</#if>
import ${model.packagePath}.${model.moduleName}.dao.I${model.className}Dao;
import ${model.packagePath}.${model.moduleName}.model.${model.className};
import ${model.packagePath}.${model.moduleName}.service.I${model.className}Service;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ${model.name!}服务
 *
 * @author ${model.author!}
 * @date ${nowTimeStr}
 */
@Slf4j
@Service
public class ${model.className}ServiceImpl implements I${model.className}Service {

    /**
     * ${model.name!}dao
     */
    @Autowired
    private I${model.className}Dao ${model.className?uncap_first}Dao;

    /**
     * 列表查询
     *
     * @param ${model.className?uncap_first} 查询条件
     * @return
     */
<#if model.supportTraceAll>
    @Trace("查询${model.name!}列表")
</#if>
    @Override
    public List<${model.className}> getList(${model.className} ${model.className?uncap_first}) {
        return ${model.className?uncap_first}Dao.getList(${model.className?uncap_first});
    }

    /**
     * 分页查询
     *
     * @param ${model.className?uncap_first} 查询条件
     * @param pageQuery 分页参数
     * @return
     */
<#if model.supportTraceAll>
    @Trace("分页查询${model.name!}")
</#if>
    @Override
    public List<${model.className}> getPage(${model.className} ${model.className?uncap_first}, PageQuery pageQuery) {
        return ${model.className?uncap_first}Dao.getList(${model.className?uncap_first}, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param ${model.className?uncap_first} 查询条件
     * @return
     */
<#if model.supportTraceAll>
    @Trace("统计${model.name!}数量")
</#if>
    @Override
    public int count(${model.className} ${model.className?uncap_first}) {
        return ${model.className?uncap_first}Dao.count(${model.className?uncap_first});
    }

    /**
     * 主键查询
     *
     * @param ${primaryKeyJavaName} 主键
     * @return
     */
<#if model.supportTraceAll>
    @Trace("主键查询${model.name!}")
</#if>
    @Override
    public ${model.className} get(${primaryKeyJavaType} ${primaryKeyJavaName}) {
        return ${model.className?uncap_first}Dao.get(${primaryKeyJavaName});
    }

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
<#if model.supportTraceAll>
    @Trace("主键批量查询${model.name!}")
</#if>
    @Override
    public List<${model.className}> getListByIds(List<${primaryKeyJavaType}> list) {
        return ${model.className?uncap_first}Dao.getListByIds(list);
    }
<#list fieldSearchConfigList as item>

    /**
     * 根据${item.fieldJavaNameJoinStr}查询
     *
  <#list item.fieldList as field>
     * @param ${field.fieldJavaName} ${field.fieldDbRemark}
  </#list>
     * @return
     */
  <#if model.supportTraceAll>
    @Trace("根据${item.fieldJavaNameJoinStr}查询${model.name!}")
  </#if>
    @Override
    public <#if !item.selectOneFlag>List${"<"}</#if>${model.className}<#if !item.selectOneFlag>${">"}</#if> ${item.methodName}(<#list item.fieldList as field><#if (field_index > 0)>, </#if>${field.fieldJavaType} ${field.fieldJavaName}</#list>) {
        return ${model.className?uncap_first}Dao.${item.methodName}(<#list item.fieldList as field><#if (field_index > 0)>, </#if>${field.fieldJavaName}</#list>);
    }
</#list>

    /**
     * 插入
     *
     * @param ${model.className?uncap_first} 参数
     * @return
     */
<#if model.supportTraceAll>
    @Trace("插入${model.name!}")
</#if>
    @Override
    public int insert(${model.className} ${model.className?uncap_first}) {
        this.handleDataBeforeInsert(${model.className?uncap_first});
        return ${model.className?uncap_first}Dao.insert(${model.className?uncap_first});
    }

    /**
     * 选择性插入
     *
     * @param ${model.className?uncap_first} 参数
     * @return
     */
<#if model.supportTraceAll>
    @Trace("选择性插入${model.name!}")
</#if>
    @Override
    public int insertSelective(${model.className} ${model.className?uncap_first}) {
        this.handleDataBeforeInsert(${model.className?uncap_first});
        return ${model.className?uncap_first}Dao.insertSelective(${model.className?uncap_first});
    }

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
<#if model.supportTraceAll>
    @Trace("批量插入${model.name!}")
</#if>
    @Override
    public int batchInsert(List<${model.className}> list) {
        list.forEach(this::handleDataBeforeInsert);
        return ${model.className?uncap_first}Dao.batchInsert(list);
    }

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
<#if model.supportTraceAll>
    @Trace("批量选择性插入${model.name!}")
</#if>
    @Override
    public int batchInsertSelective(List<${model.className}> list) {
        list.forEach(this::handleDataBeforeInsert);
        return ${model.className?uncap_first}Dao.batchInsertSelective(list);
    }

    /**
     * 插入前数据处理
     *
     * @param ${model.className?uncap_first} ${model.name!}
     */
    private void handleDataBeforeInsert(${model.className} ${model.className?uncap_first}) {

    }

    /**
     * 更新前数据处理
     *
     * @param ${model.className?uncap_first} ${model.name!}
     */
    private void handleDataBeforeUpdate(${model.className} ${model.className?uncap_first}) {

    }

    /**
     * 修改
     *
     * @param ${model.className?uncap_first} 参数
     * @return
     */
<#if model.supportTraceAll>
    @Trace("修改${model.name!}")
</#if>
    @Override
    public int update(${model.className} ${model.className?uncap_first}) {
        this.handleDataBeforeUpdate(${model.className?uncap_first});
        return ${model.className?uncap_first}Dao.update(${model.className?uncap_first});
    }

    /**
     * 选择性修改
     *
     * @param ${model.className?uncap_first} 参数
     * @return
     */
<#if model.supportTraceAll>
    @Trace("选择性修改${model.name!}")
</#if>
    @Override
    public int updateSelective(${model.className} ${model.className?uncap_first}) {
        this.handleDataBeforeUpdate(${model.className?uncap_first});
        return ${model.className?uncap_first}Dao.updateSelective(${model.className?uncap_first});
    }

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
<#if model.supportTraceAll>
    @Trace("删除${model.name!}")
</#if>
    @Override
    public int delete(${primaryKeyJavaType} id) {
        return ${model.className?uncap_first}Dao.delete(id);
    }

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
<#if model.supportTraceAll>
    @Trace("批量删除${model.name!}")
</#if>
    @Override
    public int batchDelete(List<${primaryKeyJavaType}> list) {
        return ${model.className?uncap_first}Dao.batchDelete(list);
    }

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param ${model.className?uncap_first} 参数
     * @return
     */
<#if model.supportTraceAll>
    @Trace("根据条件删除${model.name!}")
</#if>
    @Override
    public int deleteByCondition(${model.className} ${model.className?uncap_first}) {
        return ${model.className?uncap_first}Dao.deleteByCondition(${model.className?uncap_first});
    }

}
