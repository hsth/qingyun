<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${model.packagePath}.${model.moduleName}.dao.I${model.className}Dao">
    <!--查询字段信息-->
    <sql id="searchCol">
    <#list model.fieldList as item>
        <#if item_has_next>
        o.${item.fieldDbName} AS "${item.fieldJavaName}",
        <#else>
        o.${item.fieldDbName} AS "${item.fieldJavaName}"
        </#if>
    </#list>
    </sql>
    <!--关联查询相关sql-->
    <sql id="joinSql">
    </sql>
    <!--where条件-->
    <sql id="whereSql">
        <where>
        <#list model.fieldList as item>
        <#if item.listQueryCondition?? && item.listQueryCondition != 1>
        <#if item.listQueryCondition == 2>
            <if test="${item.fieldJavaName} != null<#if item.fieldJavaType == "String"> and ${item.fieldJavaName} != ''</#if>">
                AND o.${item.fieldDbName} = ${"#"}{${item.fieldJavaName}}
            </if>
        <#elseif item.listQueryCondition == 3>
            <if test="${item.fieldJavaName} != null<#if item.fieldJavaType == "String"> and ${item.fieldJavaName} != ''</#if>">
                AND o.${item.fieldDbName} LIKE CONCAT('%', CONCAT(${"#"}{${item.fieldJavaName}}, '%'))
            </if>
        <#elseif item.listQueryCondition == 4>
            <if test="${item.fieldJavaName} != null<#if item.fieldJavaType == "String"> and ${item.fieldJavaName} != ''</#if>">
                AND o.${item.fieldDbName} LIKE CONCAT('%', ${"#"}{${item.fieldJavaName}})
            </if>
        <#elseif item.listQueryCondition == 5>
            <if test="${item.fieldJavaName} != null<#if item.fieldJavaType == "String"> and ${item.fieldJavaName} != ''</#if>">
                AND o.${item.fieldDbName} LIKE CONCAT(${"#"}{${item.fieldJavaName}}, '%')
            </if>
        <#elseif item.listQueryCondition == 6>
            <if test="${item.fieldJavaName} != null<#if item.fieldJavaType == "String"> and ${item.fieldJavaName} != ''</#if>">
                AND o.${item.fieldDbName} &gt; ${"#"}{${item.fieldJavaName}}
            </if>
        <#elseif item.listQueryCondition == 7>
            <if test="${item.fieldJavaName} != null<#if item.fieldJavaType == "String"> and ${item.fieldJavaName} != ''</#if>">
                AND o.${item.fieldDbName} &lt; ${"#"}{${item.fieldJavaName}}
            </if>
        <#elseif item.listQueryCondition == 8>
            <if test="${item.fieldJavaName} != null<#if item.fieldJavaType == "String"> and ${item.fieldJavaName} != ''</#if>">
                AND o.${item.fieldDbName} &gt;= ${"#"}{${item.fieldJavaName}}
            </if>
        <#elseif item.listQueryCondition == 9>
            <if test="${item.fieldJavaName} != null<#if item.fieldJavaType == "String"> and ${item.fieldJavaName} != ''</#if>">
                AND o.${item.fieldDbName} &lt;= ${"#"}{${item.fieldJavaName}}
            </if>
        <#elseif item.listQueryCondition == 10>
            <if test="${item.fieldJavaName} != null<#if item.fieldJavaType == "String"> and ${item.fieldJavaName} != ''</#if>">
                AND o.${item.fieldDbName} &gt;= ${"#"}{${item.fieldJavaName}}
            </if>
            <if test="${item.fieldJavaName}2 != null<#if item.fieldJavaType == "String"> and ${item.fieldJavaName}2 != ''</#if>">
                AND o.${item.fieldDbName} &lt;= ${"#"}{${item.fieldJavaName}2}
            </if>
        </#if>
        </#if>
        </#list>
        </where>
    </sql>
    <!--删除sql的where条件-->
    <sql id="whereSqlForDelete">
        <where>
        <#list model.fieldList as item>
        <#if item.listQueryCondition?? && item.listQueryCondition != 1>
        <#if item.listQueryCondition == 2>
            <if test="${item.fieldJavaName} != null<#if item.fieldJavaType == "String"> and ${item.fieldJavaName} != ''</#if>">
                AND ${item.fieldDbName} = ${"#"}{${item.fieldJavaName}}
            </if>
        <#elseif item.listQueryCondition == 3>
            <if test="${item.fieldJavaName} != null<#if item.fieldJavaType == "String"> and ${item.fieldJavaName} != ''</#if>">
                AND ${item.fieldDbName} LIKE CONCAT('%', CONCAT(${"#"}{${item.fieldJavaName}}, '%'))
            </if>
        <#elseif item.listQueryCondition == 4>
            <if test="${item.fieldJavaName} != null<#if item.fieldJavaType == "String"> and ${item.fieldJavaName} != ''</#if>">
                AND ${item.fieldDbName} LIKE CONCAT('%', ${"#"}{${item.fieldJavaName}})
            </if>
        <#elseif item.listQueryCondition == 5>
            <if test="${item.fieldJavaName} != null<#if item.fieldJavaType == "String"> and ${item.fieldJavaName} != ''</#if>">
                AND ${item.fieldDbName} LIKE CONCAT(${"#"}{${item.fieldJavaName}}, '%')
            </if>
        <#elseif item.listQueryCondition == 6>
            <if test="${item.fieldJavaName} != null<#if item.fieldJavaType == "String"> and ${item.fieldJavaName} != ''</#if>">
                AND ${item.fieldDbName} &gt; ${"#"}{${item.fieldJavaName}}
            </if>
        <#elseif item.listQueryCondition == 7>
            <if test="${item.fieldJavaName} != null<#if item.fieldJavaType == "String"> and ${item.fieldJavaName} != ''</#if>">
                AND ${item.fieldDbName} &lt; ${"#"}{${item.fieldJavaName}}
            </if>
        <#elseif item.listQueryCondition == 8>
            <if test="${item.fieldJavaName} != null<#if item.fieldJavaType == "String"> and ${item.fieldJavaName} != ''</#if>">
                AND ${item.fieldDbName} &gt;= ${"#"}{${item.fieldJavaName}}
            </if>
        <#elseif item.listQueryCondition == 9>
            <if test="${item.fieldJavaName} != null<#if item.fieldJavaType == "String"> and ${item.fieldJavaName} != ''</#if>">
                AND ${item.fieldDbName} &lt;= ${"#"}{${item.fieldJavaName}}
            </if>
        <#elseif item.listQueryCondition == 10>
            <if test="${item.fieldJavaName} != null<#if item.fieldJavaType == "String"> and ${item.fieldJavaName} != ''</#if>">
                AND ${item.fieldDbName} &gt;= ${"#"}{${item.fieldJavaName}}
            </if>
            <if test="${item.fieldJavaName}2 != null<#if item.fieldJavaType == "String"> and ${item.fieldJavaName}2 != ''</#if>">
                AND ${item.fieldDbName} &lt;= ${"#"}{${item.fieldJavaName}2}
            </if>
        </#if>
        </#if>
        </#list>
        </where>
    </sql>
    <!--排序sql-->
    <sql id="sortSql">
        <choose>
            <#list model.fieldList as item>
            <#if item.listColumnSort == 1>
            <when test="dbSortBy != null and dbSortBy == '${item.fieldJavaName}'">
                ORDER BY o.${item.fieldDbName} ${"$"}{dbSortType}
            </when>
            <#else>
            </#if>
            </#list>
            <otherwise>
                <#if model.extInfo?? && model.extInfo.defaultSortBy?? && (model.extInfo.defaultSortBy?length>0)>
                    ORDER BY o.${model.extInfo.defaultSortBy} <#if model.extInfo.defaultSortType?? && model.extInfo.defaultSortType == "desc">${model.extInfo.defaultSortType}<#else>asc</#if>
                </#if>
            </otherwise>
        </choose>
    </sql>
    <!-- 查询 -->
    <select id="getList" parameterType="${modelClassPath}" resultType="${modelClassPath}">
        SELECT
        <include refid="searchCol"></include>
        FROM ${model.tableName} o
        <include refid="joinSql"></include>
        <include refid="whereSql"></include>
        <include refid="sortSql"></include>
    </select>
    <!-- 统计数量 -->
    <select id="count" parameterType="${modelClassPath}" resultType="int">
        SELECT
            count(*)
        FROM ${model.tableName} o
        <include refid="whereSql"></include>
    </select>
    <!-- 根据id查询 -->
    <select id="get" resultType="${modelClassPath}">
        SELECT
        <include refid="searchCol"></include>
        FROM ${model.tableName} o
        <include refid="joinSql"></include>
        WHERE o.${primaryKeyColumnName} = ${"#"}{${primaryKeyJavaName}}
    </select>
    <!-- 根据id批量查询 -->
    <select id="getListByIds" resultType="${modelClassPath}">
        SELECT
        <include refid="searchCol"></include>
        FROM ${model.tableName} o
        <include refid="joinSql"></include>
        WHERE o.${primaryKeyColumnName} in (
        <foreach collection="list" separator="," item="item">
            ${"#"}{item}
        </foreach>
        )
    </select>
<#list fieldSearchConfigList as item>
    <!-- 根据${item.fieldDbNameJoinStr}查询 -->
    <select id="${item.methodName}" resultType="${modelClassPath}">
        SELECT
        <include refid="searchCol"></include>
        FROM ${model.tableName} o
        <include refid="joinSql"></include>
        <where>
        <#list item.fieldList as field>
            and o.${field.fieldDbName} = ${"#"}{${field.fieldJavaName}}
        </#list>
        </where>
    </select>
</#list>
    <!-- 全字段插入 -->
    <insert id="insert" parameterType="${modelClassPath}">
        INSERT INTO ${model.tableName} (
        <trim suffixOverrides=",">
        <#list model.fieldList as item>
            ${item.fieldDbName},
        </#list>
        </trim>
        ) VALUES (
        <trim suffixOverrides=",">
        <#list model.fieldList as item>
            ${"#"}{${item.fieldJavaName}},
        </#list>
        </trim>
        )
    </insert>
    <!-- 选择性插入 -->
    <insert id="insertSelective" parameterType="${modelClassPath}">
        INSERT INTO ${model.tableName} (
        <trim suffixOverrides=",">
        <#list model.fieldList as item>
            <if test="${item.fieldJavaName} != null">
                ${item.fieldDbName},
            </if>
        </#list>
        </trim>
        ) VALUES (
        <trim suffixOverrides=",">
        <#list model.fieldList as item>
            <if test="${item.fieldJavaName} != null">
                ${"#"}{${item.fieldJavaName}},
            </if>
        </#list>
        </trim>
        )
    </insert>
    <!-- 批量全字段插入 -->
    <insert id="batchInsert" parameterType="list">
        INSERT INTO ${model.tableName} (
        <trim suffixOverrides=",">
            <#list model.fieldList as item>
                ${item.fieldDbName},
            </#list>
        </trim>
        ) VALUES
        <foreach collection="list" item="data" separator=",">
        (
        <trim suffixOverrides=",">
            <#list model.fieldList as item>
                ${"#"}{data.${item.fieldJavaName}},
            </#list>
        </trim>
        )
        </foreach>
    </insert>
    <!-- 批量选择性插入 -->
    <insert id="batchInsertSelective" parameterType="list">
        <foreach collection="list" item="data" separator=";">
            INSERT INTO ${model.tableName} (
            <trim suffixOverrides=",">
            <#list model.fieldList as item>
                <if test="data.${item.fieldJavaName} != null">
                    ${item.fieldDbName},
                </if>
            </#list>
            </trim>
            ) VALUES (
            <trim suffixOverrides=",">
            <#list model.fieldList as item>
                <if test="data.${item.fieldJavaName} != null">
                    ${"#"}{data.${item.fieldJavaName}},
                </if>
            </#list>
            </trim>
            )
        </foreach>
    </insert>
    <!-- 全字段修改 -->
    <update id="update" parameterType="${modelClassPath}">
        UPDATE ${model.tableName}
        <set>
        <#list model.fieldList as item>
        <#if item.fieldJavaName != "${primaryKeyJavaName}">
            ${item.fieldDbName} = ${"#"}{${item.fieldJavaName}},
        </#if>
        </#list>
        </set>
        WHERE ${primaryKeyColumnName} = ${"#"}{${primaryKeyJavaName}}
    </update>
    <!-- 选择性修改 -->
    <update id="updateSelective" parameterType="${modelClassPath}">
        UPDATE ${model.tableName}
        <set>
        <#list model.fieldList as item>
        <#if item.fieldJavaName != "${primaryKeyJavaName}">
            <if test="${item.fieldJavaName} != null">
                ${item.fieldDbName} = ${"#"}{${item.fieldJavaName}},
            </if>
        </#if>
        </#list>
        </set>
        WHERE ${primaryKeyColumnName} = ${"#"}{${primaryKeyJavaName}}
    </update>
    <!-- 批量修改某字段 -->
    <update id="batchUpdateColumn">
        UPDATE ${model.tableName}
        <set>
            ${"$"}{fieldDbName} = ${"#"}{fieldValue},
        </set>
        WHERE ${primaryKeyColumnName} in (
        <foreach collection="idList" item="item" separator=",">
            ${"#"}{item}
        </foreach>
        )
    </update>
    <!-- 删除 -->
    <delete id="delete">
        DELETE FROM ${model.tableName} WHERE ${primaryKeyColumnName} = ${"#"}{${primaryKeyJavaName}}
    </delete>
    <!-- 批量删除 -->
    <delete id="batchDelete">
        DELETE FROM ${model.tableName} WHERE ${primaryKeyColumnName} in (
        <foreach collection="list" item="item" separator=",">
            ${"#"}{item}
        </foreach>
        )
    </delete>
    <!-- 条件删除(慎用,后面加and 1=1是为了防止查询条件没有命中时误删除全部数据) -->
    <delete id="deleteByCondition">
        DELETE FROM ${model.tableName}
        <include refid="whereSqlForDelete"></include> and 1=1
    </delete>
    <!-- 删除全部 -->
    <delete id="deleteAll">
        DELETE FROM ${model.tableName}
    </delete>
</mapper>