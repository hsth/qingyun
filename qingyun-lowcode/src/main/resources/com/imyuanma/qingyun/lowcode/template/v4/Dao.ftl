package ${model.packagePath}.${model.moduleName}.dao;

import com.imyuanma.qingyun.common.model.PageQuery;
<#if model.supportTraceAll>
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
</#if>
import ${model.packagePath}.${model.moduleName}.model.${model.className};
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * ${model.name!}dao
 *
 * @author ${model.author!}
 * @date ${nowTimeStr}
 */
@Mapper
public interface I${model.className}Dao {

    /**
     * 列表查询
     *
     * @param ${model.className?uncap_first} 查询条件
     * @return
     */
<#if model.supportTraceAll>
    @Trace("查询${model.name!}列表")
</#if>
    List<${model.className}> getList(${model.className} ${model.className?uncap_first});

    /**
     * 分页查询
     *
     * @param ${model.className?uncap_first} 查询条件
     * @param pageQuery 分页参数
     * @return
     */
<#if model.supportTraceAll>
    @Trace("分页查询${model.name!}")
</#if>
    List<${model.className}> getList(${model.className} ${model.className?uncap_first}, PageQuery pageQuery);

    /**
     * 统计数量
     *
     * @param ${model.className?uncap_first} 查询条件
     * @return
     */
<#if model.supportTraceAll>
    @Trace("统计${model.name!}数量")
</#if>
    int count(${model.className} ${model.className?uncap_first});

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
<#if model.supportTraceAll>
    @Trace("主键查询${model.name!}")
</#if>
    ${model.className} get(${primaryKeyJavaType} id);

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
<#if model.supportTraceAll>
    @Trace("主键批量查询${model.name!}")
</#if>
    List<${model.className}> getListByIds(List<${primaryKeyJavaType}> list);
<#list fieldSearchConfigList as item>

    /**
     * 根据${item.fieldJavaNameJoinStr}查询
     *
<#list item.fieldList as field>
     * @param ${field.fieldJavaName} ${field.fieldDbRemark}
</#list>
     * @return
     */
<#if model.supportTraceAll>
    @Trace("根据${item.fieldJavaNameJoinStr}查询${model.name!}")
</#if>
    <#if !item.selectOneFlag>List${"<"}</#if>${model.className}<#if !item.selectOneFlag>${">"}</#if> ${item.methodName}(<#list item.fieldList as field><#if (field_index > 0)>, </#if>@Param("${field.fieldJavaName}") ${field.fieldJavaType} ${field.fieldJavaName}</#list>);
</#list>

    /**
     * 插入
     *
     * @param ${model.className?uncap_first} 参数
     * @return
     */
<#if model.supportTraceAll>
    @Trace("插入${model.name!}")
</#if>
    int insert(${model.className} ${model.className?uncap_first});

    /**
     * 选择性插入
     *
     * @param ${model.className?uncap_first} 参数
     * @return
     */
<#if model.supportTraceAll>
    @Trace("选择性插入${model.name!}")
</#if>
    int insertSelective(${model.className} ${model.className?uncap_first});

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
<#if model.supportTraceAll>
    @Trace("批量插入${model.name!}")
</#if>
    int batchInsert(List<${model.className}> list);

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
<#if model.supportTraceAll>
    @Trace("批量选择性插入${model.name!}")
</#if>
    int batchInsertSelective(List<${model.className}> list);

    /**
     * 修改
     *
     * @param ${model.className?uncap_first} 参数
     * @return
     */
<#if model.supportTraceAll>
    @Trace("修改${model.name!}")
</#if>
    int update(${model.className} ${model.className?uncap_first});

    /**
     * 选择性修改
     *
     * @param ${model.className?uncap_first} 参数
     * @return
     */
<#if model.supportTraceAll>
    @Trace("选择性修改${model.name!}")
</#if>
    int updateSelective(${model.className} ${model.className?uncap_first});

    /**
     * 批量修改某字段
     *
     * @param idList      主键集合
     * @param fieldDbName 待修改的字段名
     * @param fieldValue  修改后的值
     * @return
     */
<#if model.supportTraceAll>
    @Trace("批量修改${model.name!}属性")
</#if>
    int batchUpdateColumn(@Param("idList") List<${primaryKeyJavaType}> idList, @Param("fieldDbName") String fieldDbName, @Param("fieldValue") Object fieldValue);

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
<#if model.supportTraceAll>
    @Trace("删除${model.name!}")
</#if>
    int delete(${primaryKeyJavaType} id);

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
<#if model.supportTraceAll>
    @Trace("批量删除${model.name!}")
</#if>
    int batchDelete(List<${primaryKeyJavaType}> list);

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param ${model.className?uncap_first} 参数
     * @return
     */
<#if model.supportTraceAll>
    @Trace("条件删除${model.name!}")
</#if>
    int deleteByCondition(${model.className} ${model.className?uncap_first});

    /**
     * 删除全部
     *
     * @return
     */
<#if model.supportTraceAll>
    @Trace("全量删除${model.name!}")
</#if>
    int deleteAll();

}
