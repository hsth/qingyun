package ${model.packagePath}.${model.moduleName}.controller;

<#if model.supportImportFlag || model.supportExportFlag>
import com.imyuanma.qingyun.common.core.excel.ExcelExport;
import com.imyuanma.qingyun.common.core.excel.ExcelImport;
import com.imyuanma.qingyun.common.core.excel.IExcelExport;
import com.imyuanma.qingyun.common.core.excel.IExcelImport;
</#if>
<#if model.extendsBaseDO>
import com.imyuanma.qingyun.common.factory.BaseDOBuilder;
</#if>
import com.imyuanma.qingyun.common.model.request.WebRequest;
import com.imyuanma.qingyun.common.model.response.Page;
import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.common.util.AssertUtil;
<#if model.supportTraceWeb>
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
</#if>
import ${model.packagePath}.${model.moduleName}.model.${model.className};
import ${model.packagePath}.${model.moduleName}.service.I${model.className}Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * ${model.name!}web
 *
 * @author ${model.author!}
 * @date ${nowTimeStr}
 */
@RestController
@RequestMapping(value = "/${model.moduleName}/${model.className?uncap_first}")
public class ${model.className}Controller {

    private static final Logger logger = LoggerFactory.getLogger(${model.className}Controller.class);

    /**
     * ${model.name!}服务
     */
    @Autowired
    private I${model.className}Service ${model.className?uncap_first}Service;

    /**
     * 列表查询
     *
     * @param webRequest 查询条件
     * @return
     */
<#if model.supportTraceWeb>
    @Trace("查询${model.name!}列表")
</#if>
    @PostMapping("/getList")
    public Result<${"List"}<${model.className}>> getList(@RequestBody WebRequest<${model.className}> webRequest) {
        return Result.success(${model.className?uncap_first}Service.getList(webRequest.getBody()));
    }

    /**
     * 分页查询
     *
     * @param webRequest 查询条件
     * @return
     */
<#if model.supportTraceWeb>
    @Trace("分页查询${model.name!}")
</#if>
    @PostMapping("/getPage")
    public Page<${"List"}<${model.className}>> getPage(@RequestBody WebRequest<${model.className}> webRequest) {
        PageQuery pageQuery = webRequest.buildPageQuery();
        List<${model.className}> list = ${model.className?uncap_first}Service.getPage(webRequest.getBody(), pageQuery);
        return Page.success(list, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param webRequest 查询条件
     * @return
     */
<#if model.supportTraceWeb>
    @Trace("统计${model.name!}数量")
</#if>
    @PostMapping("/count")
    public Result<${"Integer"}> count(@RequestBody WebRequest<${model.className}> webRequest) {
        return Result.success(${model.className?uncap_first}Service.count(webRequest.getBody()));
    }

    /**
     * 查询
     *
     * @param webRequest 参数
     * @return
     */
<#if model.supportTraceWeb>
    @Trace("查询${model.name!}")
</#if>
    @PostMapping("/get")
    public Result<${model.className}> get(@RequestBody WebRequest<${primaryKeyJavaType}> webRequest) {
        AssertUtil.notNull(webRequest.getBody());
        return Result.success(${model.className?uncap_first}Service.get(webRequest.getBody()));
    }

    /**
     * 新增
     *
     * @param webRequest 参数
     * @return
     */
<#if model.supportTraceWeb>
    @Trace("新增${model.name!}")
</#if>
    @PostMapping("/insert")
    public Result insert(@RequestBody WebRequest<${model.className}> webRequest) {
        ${model.className} ${model.className?uncap_first} = webRequest.getBody();
        AssertUtil.notNull(${model.className?uncap_first});
        <#if model.extendsBaseDO>
        BaseDOBuilder.fillBaseDOForInsert(${model.className?uncap_first});
        </#if>
        ${model.className?uncap_first}Service.insertSelective(${model.className?uncap_first});
        return Result.success();
    }

    /**
     * 修改
     *
     * @param webRequest 参数
     * @return
     */
<#if model.supportTraceWeb>
    @Trace("修改${model.name!}")
</#if>
    @PostMapping("/update")
    public Result update(@RequestBody WebRequest<${model.className}> webRequest) {
        ${model.className} ${model.className?uncap_first} = webRequest.getBody();
        AssertUtil.notNull(${model.className?uncap_first});
        AssertUtil.notNull(${model.className?uncap_first}.get${primaryKeyJavaName?cap_first}());
        <#if model.extendsBaseDO>
        BaseDOBuilder.fillBaseDOForUpdate(${model.className?uncap_first});
        </#if>
        ${model.className?uncap_first}Service.updateSelective(${model.className?uncap_first});
        return Result.success();
    }

    /**
     * 删除
     *
     * @param webRequest 参数, 待删除主键,多个使用英文逗号拼接
     * @return
     */
<#if model.supportTraceWeb>
    @Trace("删除${model.name!}")
</#if>
    @PostMapping("/delete")
    public Result delete(@RequestBody WebRequest<${"String"}> webRequest) {
        String ids = webRequest.getBody();
        AssertUtil.notBlank(ids);
        if (ids.contains(",")) {
        <#if primaryKeyJavaType == "String">
            ${model.className?uncap_first}Service.batchDelete(Arrays.stream(ids.split(",")).collect(Collectors.toList()));
        <#else>
            ${model.className?uncap_first}Service.batchDelete(Arrays.stream(ids.split(",")).map(${primaryKeyJavaType}::valueOf).collect(Collectors.toList()));
        </#if>
        } else {
        <#if primaryKeyJavaType == "String">
            ${model.className?uncap_first}Service.delete(ids);
        <#else>
            ${model.className?uncap_first}Service.delete(${primaryKeyJavaType}.valueOf(ids));
        </#if>
        }
        return Result.success();
    }

<#if model.supportImportFlag>
    /**
    * 导入
    * @param file
    * @return
    */
    <#if model.supportTraceWeb>
    @Trace("导入${model.name!}")
    </#if>
    @PostMapping("/import")
    public Result importExcel(MultipartFile file) {
        try {
            if (file != null && !file.isEmpty()) {
                //excel导入处理,返回excel中的数据集合
                IExcelImport ei = new ExcelImport(file);//将文件转为ExcelImport对象
                //从excel读取数据
                List<${model.className}> list = ei.getImportDataAsBean(${model.className}.class);
                if (list != null && list.size() > 0) {
                <#if model.extendsBaseDO>
                    for (${model.className} item : list) {
                        BaseDOBuilder.fillBaseDOForInsert(item);
                    }
                </#if>
                    int num = ${model.className?uncap_first}Service.batchInsertSelective(list);//批量插入
                    return Result.success("成功导入数据" + num + "条!");
                } else {
                    return Result.error("导入失败:excel解析后结果为空!");
                }
            } else {
                return Result.error("导入失败:文件为空!");
            }
        } catch (Throwable e) {
            logger.error("[导入${model.name!}] 导入发生异常", e);
            return Result.error("导入失败!");
        }
    }

    /**
    * 下载excel导入模板
    * @param response
    * @throws Exception
    */
    <#if model.supportTraceWeb>
    @Trace("下载${model.name!}excel导入模板")
    </#if>
    @GetMapping("/templateDownload")
    public void templateDownload(HttpServletResponse response) throws Exception {
        IExcelExport ee = new ExcelExport();
        ee.insertBeanList(new ArrayList(), ${model.className}.class);
        ee.write2Response(response, "template.xls");
    }
</#if>

<#if model.supportExportFlag>
    /**
    * 导出
    * @param obj 查询参数
    * @param response
    * @throws Exception
    */
    <#if model.supportTraceWeb>
        @Trace("导出${model.name!}")
    </#if>
    @GetMapping("/export")
    public void exportExcel(${model.className} obj, HttpServletResponse response) throws Exception {
        List<${model.className}> list = ${model.className?uncap_first}Service.getList(obj);
        if (list != null && list.size() > 0) {
            IExcelExport ee = new ExcelExport();
            ee.insertBeanList(list, ${model.className}.class);
            ee.write2Response(response, "excel_" + System.currentTimeMillis() + ".xls");
        } else {
            response.getWriter().write("数据为空!");
        }
    }
</#if>

}
