package ${model.packagePath}.${model.moduleName}.model;

import lombok.Data;
import java.util.Date;
<#if model.extendsBaseDO>
import com.imyuanma.qingyun.interfaces.common.model.BaseDO;
<#elseif model.extendsDbSortDO>
import com.imyuanma.qingyun.interfaces.common.model.DbSortDO;
</#if>
<#if model.supportImportFlag || model.supportExportFlag>
import com.imyuanma.qingyun.common.core.excel.ExcelColumn;
</#if>

/**
 * ${model.name!}实体类
 *
 * @author ${model.author!}
 * @date ${nowTimeStr}
 */
@Data
public class ${model.className} <#if model.extendsBaseDO>extends BaseDO <#elseif model.extendsDbSortDO>extends DbSortDO </#if>{

<#--变量-->
<#list model.fieldList as item>
    <#--判断需不需要生成字段,因为部分字段可能在父类有了-->
    <#if !model.extendsBaseDO || !item.baseDOField>
    /**
     * ${item.fieldDbRemark!item.fieldDbName}
     */
    <#if model.supportImportFlag || model.supportExportFlag>
        <#if item.supportImportFlag || item.supportExportFlag>
    @ExcelColumn(value = "${item.listHeaderTitle!item.fieldJavaName}", order = ${item_index+1}0)
        </#if>
    </#if>
    private ${item.fieldJavaType} ${item.fieldJavaName};
    <#if item.listQueryCondition?? && item.listQueryCondition != 1 && item.fieldJavaType == "Date">
    /**
     * ${item.fieldDbRemark!item.fieldDbName},按时间检索时作为结束时间使用
     */
    private ${item.fieldJavaType} ${item.fieldJavaName}2;
    <#elseif item.listQueryCondition?? && item.listQueryCondition == 10>
    /**
     * ${item.fieldDbRemark!item.fieldDbName},范围检索时作为结束值使用
     */
    private ${item.fieldJavaType} ${item.fieldJavaName}2;
    </#if>

    </#if>
</#list>

<#--支持BaseDO则不需要生成排序字段-->
<#if !model.extendsBaseDO && !model.extendsDbSortDO>
    /**
     * 排序字段
     */
    private String dbSortBy;
    /**
     * 排序类型,升序asc/降序desc
     */
    private String dbSortType;
</#if>

}