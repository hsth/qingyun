package com.imyuanma.qingyun.interfaces.common.model;

import java.util.Date;

/**
 * 基础的公共字段数据模型
 * 轻云平台定义了部分常用字段作为公共字段(建议业务表都加上这些字段):
 * ALTER TABLE xxxx
 * ADD COLUMN create_time DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
 * , ADD COLUMN update_time DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间'
 * , ADD COLUMN create_user_id BIGINT NOT NULL COMMENT '创建人id'
 * , ADD COLUMN update_user_id BIGINT NOT NULL COMMENT '更新人id'
 * , ADD COLUMN data_sequence BIGINT NOT NULL DEFAULT 1 COMMENT '数据顺序'
 * , ADD COLUMN data_version BIGINT NOT NULL DEFAULT 1 COMMENT '数据版本号'
 * , ADD COLUMN trash_flag INT NOT NULL DEFAULT 0 COMMENT '删除标识,1删除0正常';
 * <p>
 * 通过继承这个基础模型, 避免在所有的业务模型中都需要重复定义这些字段, 将重点放在业务相关的字段
 *
 * @author wangjy
 * @date 2022/05/04 12:40:30
 */
public class BaseDO extends DbSortDO {
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 创建时间 end, 仅用于范围查询
     */
    private Date createTime2;
    /**
     * 修改时间
     */
    private Date updateTime;
    /**
     * 修改时间 end, 仅用于范围查询
     */
    private Date updateTime2;
    /**
     * 创建人id
     */
    private Long createUserId;
    /**
     * 创建人姓名
     */
    private String createUserName;
    /**
     * 更新人id
     */
    private Long updateUserId;
    /**
     * 更新人姓名
     */
    private String updateUserName;
    /**
     * 数据顺序
     */
    private Long dataSequence;
    /**
     * 数据版本号
     */
    private Long dataVersion;
    /**
     * 删除标识,1删除0正常
     */
    private Integer trashFlag;

    /**
     * 返回所有字段名
     *
     * @return
     */
    public static String[] allField() {
        return new String[]{"createTime", "createTime2", "updateTime", "updateTime2",
                "createUserId", "createUserName", "updateUserId", "updateUserName",
                "dataSequence", "dataVersion", "trashFlag"};
    }

    /**
     * 拷贝所有属性
     *
     * @param baseDO
     */
    public void fillBaseField(BaseDO baseDO) {
        if (this.getCreateTime() == null) {
            this.setCreateTime(baseDO.getCreateTime());
        }
        if (this.getCreateTime2() == null) {
            this.setCreateTime2(baseDO.getCreateTime2());
        }
        if (this.getUpdateTime() == null) {
            this.setUpdateTime(baseDO.getUpdateTime());
        }
        if (this.getUpdateTime2() == null) {
            this.setUpdateTime2(baseDO.getUpdateTime2());
        }
        if (this.getCreateUserId() == null) {
            this.setCreateUserId(baseDO.getCreateUserId());
        }
        if (this.getCreateUserName() == null) {
            this.setCreateUserName(baseDO.getCreateUserName());
        }
        if (this.getUpdateUserId() == null) {
            this.setUpdateUserId(baseDO.getUpdateUserId());
        }
        if (this.getUpdateUserName() == null) {
            this.setUpdateUserName(baseDO.getUpdateUserName());
        }
        if (this.getDataSequence() == null) {
            this.setDataSequence(baseDO.getDataSequence());
        }
        if (this.getDataVersion() == null) {
            this.setDataVersion(baseDO.getDataVersion());
        }
        if (this.getTrashFlag() == null) {
            this.setTrashFlag(baseDO.getTrashFlag());
        }
        if (this.getDbSortBy() == null) {
            this.setDbSortBy(baseDO.getDbSortBy());
        }
        if (this.getDbSortType() == null) {
            this.setDbSortType(baseDO.getDbSortType());
        }
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getCreateTime2() {
        return createTime2;
    }

    public void setCreateTime2(Date createTime2) {
        this.createTime2 = createTime2;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getUpdateTime2() {
        return updateTime2;
    }

    public void setUpdateTime2(Date updateTime2) {
        this.updateTime2 = updateTime2;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Long getDataSequence() {
        return dataSequence;
    }

    public void setDataSequence(Long dataSequence) {
        this.dataSequence = dataSequence;
    }

    public Long getDataVersion() {
        return dataVersion;
    }

    public void setDataVersion(Long dataVersion) {
        this.dataVersion = dataVersion;
    }

    public Integer getTrashFlag() {
        return trashFlag;
    }

    public void setTrashFlag(Integer trashFlag) {
        this.trashFlag = trashFlag;
    }
}
