package com.imyuanma.qingyun.interfaces.ums.service;

/**
 * 密码服务
 *
 * @author wangjy
 * @date 2024/03/08 23:08:40
 */
public interface IUmsPasswordOutService {
    /**
     * 密码加密
     *
     * @param rawPassword 明文密码
     * @return 密文密码
     */
    String encode(CharSequence rawPassword);

    /**
     * 校验密码
     *
     * @param rawPassword     明文密码
     * @param encodedPassword 密文密码
     * @return 是否匹配
     */
    boolean matches(CharSequence rawPassword, String encodedPassword);

    /**
     * 解析前端传入的密码
     *
     * @param inputPassword 前端传入的密码, 前端加密过程: 原文 -> base64编码(utf8) -> rsa加密得到密文 -> 对密文base64编码(utf8)
     * @return 用户真正输入的密码
     */
    String parseInputPassword(CharSequence inputPassword);
}
