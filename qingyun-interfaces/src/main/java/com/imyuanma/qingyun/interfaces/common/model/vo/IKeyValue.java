package com.imyuanma.qingyun.interfaces.common.model.vo;

/**
 * key-value
 *
 * @author wangjy
 * @date 2022/03/20 13:15:10
 */
public interface IKeyValue {
    /**
     * 获取key
     * @return
     */
    String getKey();

    /**
     * 获取value
     * @return
     */
    String getValue();
}
