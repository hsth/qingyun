package com.imyuanma.qingyun.interfaces.ums.ext.param;

import com.imyuanma.qingyun.interfaces.ums.model.UserBaseInfoDTO;

/**
 * 用户新增后置处理参数
 *
 * @author wangjy
 * @date 2023/04/05 20:47:25
 */
public class UserInsertPostHandleParam {
    /**
     * 插入的用户
     */
    private Object insertUser;

    public UserInsertPostHandleParam() {
    }

    public UserInsertPostHandleParam(Object insertUser) {
        this.insertUser = insertUser;
    }

    public Object getInsertUser() {
        return insertUser;
    }

    public void setInsertUser(Object insertUser) {
        this.insertUser = insertUser;
    }
}
