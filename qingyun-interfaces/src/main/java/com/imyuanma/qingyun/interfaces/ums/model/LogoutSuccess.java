package com.imyuanma.qingyun.interfaces.ums.model;

/**
 * 退出成功结果
 *
 * @author wangjy
 * @date 2023/05/28 13:22:13
 */
public class LogoutSuccess {
    /**
     * 重定向url
     */
    private String redirectTo;

    public LogoutSuccess(String redirectTo) {
        this.redirectTo = redirectTo;
    }

    public String getRedirectTo() {
        return redirectTo;
    }

    public void setRedirectTo(String redirectTo) {
        this.redirectTo = redirectTo;
    }
}
