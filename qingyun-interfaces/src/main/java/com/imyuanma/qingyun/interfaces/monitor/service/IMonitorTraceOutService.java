package com.imyuanma.qingyun.interfaces.monitor.service;

import com.imyuanma.qingyun.interfaces.monitor.model.TraceDTO;

import java.util.List;

/**
 * 链路对外服务
 *
 * @author wangjy
 * @date 2022/09/21 22:57:06
 */
public interface IMonitorTraceOutService {
    /**
     * 批量保存链路日志
     *
     * @param traceDTOList
     */
    void batchSaveTraceLog(List<TraceDTO> traceDTOList);
}
