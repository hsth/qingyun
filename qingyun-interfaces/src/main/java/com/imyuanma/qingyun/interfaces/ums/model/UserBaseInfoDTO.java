package com.imyuanma.qingyun.interfaces.ums.model;

import java.io.Serializable;

/**
 * 用户基础信息
 *
 * @author wangjy
 * @date 2023/01/02 12:48:35
 */
public class UserBaseInfoDTO implements Serializable {
    static final long serialVersionUID = 2423488969681744392L;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 姓名
     */
    private String name;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 账号
     */
    private String account;
    /**
     * 状态,10有效,20无效
     */
    private Integer status;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
