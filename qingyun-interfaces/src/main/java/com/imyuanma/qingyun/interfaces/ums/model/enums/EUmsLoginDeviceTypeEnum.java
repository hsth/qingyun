package com.imyuanma.qingyun.interfaces.ums.model.enums;

import java.util.Objects;

/**
 * 设备类型枚举
 *
 * @author YuanMaKeJi
 * @date 2023-04-08 15:16:30
 */
public enum EUmsLoginDeviceTypeEnum {
    CODE_H5(10, "H5"),
    CODE_IOS(20, "IOS"),
    CODE_ANDROID(30, "ANDROID"),
    CODE_WINDOWS(40, "WINDOWS"),
    CODE_MINI(50, "MINI"),
    OTHER(60, "OTHER"),
    ;

    /**
     * code
     */
    private Integer code;
    /**
     * 文案
     */
    private String text;

    EUmsLoginDeviceTypeEnum(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    /**
     * 根据code获取枚举
     *
     * @param code code值
     */
    public static EUmsLoginDeviceTypeEnum getByCode(Integer code) {
        for (EUmsLoginDeviceTypeEnum e : EUmsLoginDeviceTypeEnum.values()) {
            if (e.code != null && e.code.equals(code)) {
                return e;
            }
        }
        return null;
    }

    public static Integer getCodeByText(String text) {
        if (text != null) {
            for (EUmsLoginDeviceTypeEnum e : EUmsLoginDeviceTypeEnum.values()) {
                if (e.text.equalsIgnoreCase(text)) {
                    return e.code;
                }
            }
        }
        return null;
    }

    public static EUmsLoginDeviceTypeEnum getByText(String text) {
        if (text != null) {
            for (EUmsLoginDeviceTypeEnum e : EUmsLoginDeviceTypeEnum.values()) {
                if (e.text.equalsIgnoreCase(text)) {
                    return e;
                }
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
