package com.imyuanma.qingyun.interfaces.common.ext;

/**
 * 扩展点
 *
 * @author wangjy
 * @date 2023/01/02 14:09:38
 */
public interface IExtNode<P, R> {
    /**
     * 执行扩展点
     *
     * @param extNodeParam
     * @return
     */
    ExtNodeResult<R> execute(P extNodeParam);

    /**
     * 过滤
     *
     * @param extNodeParam
     * @return true表示可以执行execute()方法
     */
    default boolean filter(P extNodeParam) {
        return true;
    }

    /**
     * 排序序号
     *
     * @return 越小优先级越高
     */
    default long ordered() {
        return 0L;
    }

}
