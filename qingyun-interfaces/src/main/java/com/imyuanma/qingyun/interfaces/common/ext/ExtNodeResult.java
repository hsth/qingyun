package com.imyuanma.qingyun.interfaces.common.ext;

/**
 * 扩展点返回结果
 *
 * @author wangjy
 * @date 2023/01/02 12:55:33
 */
public class ExtNodeResult<T> {
    /**
     * 扩展点实现为空标识
     */
    private boolean emptyImpl;
    /**
     * 抛出的异常
     */
    private Throwable throwable;
    /**
     * 返回码
     */
    private int code = 0;
    /**
     * 文案
     */
    private String info = "成功";
    /**
     * 返回结果
     */
    private T data;

    /**
     * 是否成功
     *
     * @return
     */
    public boolean isSuccess() {
        return code == 0;
    }

    /**
     * 是否失败
     *
     * @return
     */
    public boolean isError() {
        return !isSuccess();
    }

    /**
     * 错误类型是否为扩展点实现为空
     *
     * @return
     */
    public boolean isEmptyImpl() {
        return emptyImpl;
    }

    public static <T> ExtNodeResult<T> success() {
        return new ExtNodeResult<>();
    }

    public static <T> ExtNodeResult<T> success(T data) {
        ExtNodeResult<T> extNodeResult = new ExtNodeResult<>();
        extNodeResult.setData(data);
        return extNodeResult;
    }

    public static <T> ExtNodeResult<T> error() {
        ExtNodeResult<T> extNodeResult = new ExtNodeResult<>();
        extNodeResult.setCode(-1);
        extNodeResult.setInfo("失败");
        return extNodeResult;
    }

    public static <T> ExtNodeResult<T> error(String info) {
        ExtNodeResult<T> extNodeResult = new ExtNodeResult<>();
        extNodeResult.setCode(-1);
        extNodeResult.setInfo(info);
        return extNodeResult;
    }

    public static <T> ExtNodeResult<T> emptyImplError() {
        ExtNodeResult<T> extNodeResult = new ExtNodeResult<>();
        extNodeResult.setCode(404);
        extNodeResult.setInfo("扩展点实现为空");
        extNodeResult.emptyImpl = true;
        return extNodeResult;
    }

    public static <T> ExtNodeResult<T> exceptionError(Throwable t) {
        ExtNodeResult<T> extNodeResult = new ExtNodeResult<>();
        extNodeResult.setCode(500);
        extNodeResult.setInfo("扩展点抛出异常");
        extNodeResult.throwable = t;
        return extNodeResult;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public void setEmptyImpl(boolean emptyImpl) {
        this.emptyImpl = emptyImpl;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public void setThrowable(Throwable throwable) {
        this.throwable = throwable;
    }
}
