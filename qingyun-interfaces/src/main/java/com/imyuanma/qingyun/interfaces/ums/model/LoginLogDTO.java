package com.imyuanma.qingyun.interfaces.ums.model;

import com.imyuanma.qingyun.interfaces.ums.model.enums.EUmsLoginDeviceTypeEnum;

import java.io.Serializable;
import java.util.Date;

/**
 * 登录日志
 *
 * @author wangjy
 * @date 2023/04/08 15:20:31
 */
public class LoginLogDTO implements Serializable {
    static final long serialVersionUID = -5894331140017663617L;
    /**
     * 登录账号
     */
    private String account;

    /**
     * 登录结果
     */
    private Boolean loginSuccess;

    /**
     * 登录时间
     */
    private Date loginTime;

    /**
     * 客户端ip
     */
    private String clientIp;

    /**
     * 服务端ip
     */
    private String serverIp;

    /**
     * 备注
     */
    private String remark;

    /**
     * 设备类型
     */
    private Integer deviceType;

    public LoginLogDTO() {
    }

    /**
     * 登录成功日志
     *
     * @param account
     * @param clientIp
     * @param serverIp
     * @param loginDeviceTypeEnum
     * @return
     */
    public static LoginLogDTO success(String account, String clientIp, String serverIp, EUmsLoginDeviceTypeEnum loginDeviceTypeEnum) {
        LoginLogDTO loginLogDTO = new LoginLogDTO();
        loginLogDTO.setAccount(account);
        loginLogDTO.setLoginSuccess(true);
        loginLogDTO.setLoginTime(new Date());
        loginLogDTO.setClientIp(clientIp);
        loginLogDTO.setServerIp(serverIp);
        loginLogDTO.setRemark(null);
        loginLogDTO.setDeviceType(loginDeviceTypeEnum.getCode());
        return loginLogDTO;
    }

    /**
     * 登录失败日志
     *
     * @param account
     * @param clientIp
     * @param serverIp
     * @param loginDeviceTypeEnum
     * @param errorInfo
     * @return
     */
    public static LoginLogDTO error(String account, String clientIp, String serverIp, EUmsLoginDeviceTypeEnum loginDeviceTypeEnum, String errorInfo) {
        LoginLogDTO loginLogDTO = new LoginLogDTO();
        loginLogDTO.setAccount(account);
        loginLogDTO.setLoginSuccess(false);
        loginLogDTO.setLoginTime(new Date());
        loginLogDTO.setClientIp(clientIp);
        loginLogDTO.setServerIp(serverIp);
        loginLogDTO.setRemark(errorInfo);
        if (loginDeviceTypeEnum != null) {
            loginLogDTO.setDeviceType(loginDeviceTypeEnum.getCode());
        }
        return loginLogDTO;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Boolean getLoginSuccess() {
        return loginSuccess;
    }

    public void setLoginSuccess(Boolean loginSuccess) {
        this.loginSuccess = loginSuccess;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public String getServerIp() {
        return serverIp;
    }

    public void setServerIp(String serverIp) {
        this.serverIp = serverIp;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(Integer deviceType) {
        this.deviceType = deviceType;
    }
}
