package com.imyuanma.qingyun.interfaces.common.model;

import java.io.Serializable;

/**
 * 结果对象
 *
 * @author wangjy
 * @date 2023/06/04 21:45:36
 */
public class ApiResult<T> implements Serializable {
    static final long serialVersionUID = -2412365618321418255L;
    /**
     * 返回码
     */
    private int code = 0;
    /**
     * 文案
     */
    private String info = "成功";
    /**
     * 返回结果
     */
    private T data;

    public static <T> ApiResult<T> success() {
        return success(null);
    }

    public static <T> ApiResult<T> success(T data) {
        ApiResult<T> apiResult = new ApiResult<>();
        apiResult.setData(data);
        return apiResult;
    }

    public static <T> ApiResult<T> error(int code, String info) {
        return new ApiResult<>(code, info, null);
    }

    public ApiResult() {
    }

    public ApiResult(int code, String info, T data) {
        this.code = code;
        this.info = info;
        this.data = data;
    }

    public boolean isSuccess() {
        return code == 0;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
