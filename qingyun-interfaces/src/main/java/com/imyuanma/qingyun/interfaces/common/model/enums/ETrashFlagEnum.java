package com.imyuanma.qingyun.interfaces.common.model.enums;

/**
 * 删除标识
 *
 * @author wangjy
 * @date 2021/10/02 23:23:41
 */
public enum ETrashFlagEnum {
    /**
     * 已删除
     */
    TRASH(1, "已删除"),
    /**
     * 未删除, 有效的
     */
    VALID(0, "未删除")
    ;
    /**
     * 类型
     */
    private final int type;
    /**
     * 说明
     */
    private final String text;

    /**
     * 判断相等
     * @param type
     * @return true表示相等
     */
    public boolean eq(Integer type) {
        return type != null && this.type == type;
    }

    /**
     * 判断不相等
     * @param type
     * @return true表示不相等
     */
    public boolean notEq(Integer type) {
        return !this.eq(type);
    }

    ETrashFlagEnum(int type, String text) {
        this.type = type;
        this.text = text;
    }

    public int getType() {
        return type;
    }

    public String getText() {
        return text;
    }
}
