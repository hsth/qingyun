package com.imyuanma.qingyun.interfaces.ums.ext;

import com.imyuanma.qingyun.interfaces.common.ext.IExtNode;
import com.imyuanma.qingyun.interfaces.ums.ext.param.UserInsertPostHandleParam;

/**
 * 用户新增后处理扩展点
 *
 * @author wangjy
 * @date 2023/04/05 20:46:45
 */
@Deprecated
public interface UmsUserInsertPostHandleExt extends IExtNode<UserInsertPostHandleParam, Boolean> {
}
