package com.imyuanma.qingyun.interfaces.ums.service;

import com.imyuanma.qingyun.interfaces.common.model.ApiResult;
import com.imyuanma.qingyun.interfaces.ums.model.*;

/**
 * 统一用户权限对外服务
 *
 * @author wangjy
 * @date 2022/07/09 16:33:03
 */
public interface IUmsPermissionsOutService {
    /**
     * 根据账号获取用户
     *
     * @param account 账号
     * @return
     */
    LoginUserDTO getByAccount(String account);

    /**
     * 登录校验通过后创建会话
     *
     * @param loginUserDTO 登录用户
     * @param terminalDTO  客户端信息
     * @return 会话凭证(令牌)
     */
    String createSessionAfterLoginCheckSuccess(LoginUserDTO loginUserDTO, TerminalDTO terminalDTO);

    /**
     * 下线会话
     *
     * @param token 会话凭证(令牌)
     */
    void offlineSession(String token);

    /**
     * 单点验证
     * 入参token,返回单点校验传输对象(包含用户信息)
     *
     * @param token
     * @return
     */
    SsoVerifyDTO verifySSO(String token);

    /**
     * 插入登录日志
     *
     * @param loginLog 登录日志
     */
    void insertLoginLog(LoginLogDTO loginLog);

    /**
     * 验证uri访问权限
     *
     * @param uriAccessVerifyDTO 校验参数
     * @return
     */
    ApiResult verifyUriAccess(UriAccessVerifyDTO uriAccessVerifyDTO);
}
