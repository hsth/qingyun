package com.imyuanma.qingyun.interfaces.ums.ext.param;

import com.imyuanma.qingyun.interfaces.ums.model.UserBaseInfoDTO;

/**
 * 找回密码消息发送参数
 *
 * @author wangjy
 * @date 2023/01/02 12:52:05
 */
public class SendFindPwdMsgParam {
    /**
     * 用户信息
     */
    private UserBaseInfoDTO userBaseInfo;
    /**
     * 找回key
     */
    private String findPwdKey;

    public SendFindPwdMsgParam() {
    }

    public SendFindPwdMsgParam(UserBaseInfoDTO userBaseInfo, String findPwdKey) {
        this.userBaseInfo = userBaseInfo;
        this.findPwdKey = findPwdKey;
    }

    public UserBaseInfoDTO getUserBaseInfo() {
        return userBaseInfo;
    }

    public void setUserBaseInfo(UserBaseInfoDTO userBaseInfo) {
        this.userBaseInfo = userBaseInfo;
    }

    public String getFindPwdKey() {
        return findPwdKey;
    }

    public void setFindPwdKey(String findPwdKey) {
        this.findPwdKey = findPwdKey;
    }
}
