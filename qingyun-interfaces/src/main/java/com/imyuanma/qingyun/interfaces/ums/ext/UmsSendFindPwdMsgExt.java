package com.imyuanma.qingyun.interfaces.ums.ext;

import com.imyuanma.qingyun.interfaces.common.ext.IExtNode;
import com.imyuanma.qingyun.interfaces.ums.ext.param.SendFindPwdMsgParam;

/**
 * ums消息通知扩展点
 *
 * @author wangjy
 * @date 2023/01/02 12:29:15
 */
@Deprecated
public interface UmsSendFindPwdMsgExt extends IExtNode<SendFindPwdMsgParam, Boolean> {
//    /**
//     * 发送密码找回消息
//     *
//     * @param userBaseInfoDTO
//     * @return
//     */
//    ExtNodeResult sendFindPwdMsg(UserBaseInfoDTO userBaseInfoDTO);
}
