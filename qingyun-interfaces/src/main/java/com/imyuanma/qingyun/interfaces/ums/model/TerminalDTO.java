package com.imyuanma.qingyun.interfaces.ums.model;

import java.io.Serializable;

/**
 * 终端信息
 *
 * @author wangjy
 * @date 2022/07/16 14:42:05
 */
public class TerminalDTO implements Serializable {
    static final long serialVersionUID = 7743507534056670006L;
    /**
     * 设备标识
     */
    private String deviceAddr;

    /**
     * 设备类型
     */
    private Integer deviceType;

    /**
     * 版本号
     */
    private String deviceVersion;

    /**
     * 客户端ip
     */
    private String clientIp;

    public String getDeviceAddr() {
        return deviceAddr;
    }

    public void setDeviceAddr(String deviceAddr) {
        this.deviceAddr = deviceAddr;
    }

    public Integer getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(Integer deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceVersion() {
        return deviceVersion;
    }

    public void setDeviceVersion(String deviceVersion) {
        this.deviceVersion = deviceVersion;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }
}
