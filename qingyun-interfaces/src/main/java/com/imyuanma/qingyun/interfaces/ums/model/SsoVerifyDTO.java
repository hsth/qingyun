package com.imyuanma.qingyun.interfaces.ums.model;

import java.io.Serializable;
import java.util.List;

/**
 * 单点登录验证结果
 *
 * @author wangjy
 * @date 2022/07/16 11:03:32
 */
public class SsoVerifyDTO implements Serializable {
    static final long serialVersionUID = 3225449982691701448L;
    /**
     * 结果码
     */
    private Integer code;
    /**
     * 结果文案
     */
    private String info;
    /**
     * 登录令牌
     */
    private String token;
    /**
     * 登录用户信息
     */
    private LoginUserDTO loginUser;
    /**
     * 用户拥有的资源权限列表
     */
    private List<String> resourceCodeList;

    /**
     * 判断是否验证成功
     *
     * @return
     */
    public boolean isSuccess() {
        return code != null && code == 0;
    }

    /**
     * 构造成功结果
     *
     * @param token
     * @param loginUser
     * @return
     */
    public static SsoVerifyDTO buildSuccess(String token, LoginUserDTO loginUser) {
        SsoVerifyDTO ssoVerifyDTO = new SsoVerifyDTO();
        ssoVerifyDTO.setCode(0);
        ssoVerifyDTO.setInfo("成功");
        ssoVerifyDTO.setToken(token);
        ssoVerifyDTO.setLoginUser(loginUser);
        return ssoVerifyDTO;
    }

    /**
     * 构造失败结果
     *
     * @param info
     * @return
     */
    public static SsoVerifyDTO buildFail(String info) {
        SsoVerifyDTO ssoVerifyDTO = new SsoVerifyDTO();
        ssoVerifyDTO.setCode(-1);
        ssoVerifyDTO.setInfo(info);
        return ssoVerifyDTO;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LoginUserDTO getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(LoginUserDTO loginUser) {
        this.loginUser = loginUser;
    }

    public List<String> getResourceCodeList() {
        return resourceCodeList;
    }

    public void setResourceCodeList(List<String> resourceCodeList) {
        this.resourceCodeList = resourceCodeList;
    }
}
