package com.imyuanma.qingyun.interfaces.monitor.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 链路日志注解
 *
 * @author wangjy
 * @date 2022/09/15 22:57:17
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Trace {
    String value() default "";//节点说明,例如:查询用户详情

    String key() default "";//节点唯一标识,可以是方法全路径,例如:com.young.interfaces.log.annotation.xxx

    String appName() default "";//应用名,例如:young-ums

    String[] tags() default "";//标签,用于自定义扩展
}
