package com.imyuanma.qingyun.interfaces.common.model;

/**
 * 数据库排序对象
 *
 * @author wangjy
 * @date 2022/05/21 21:24:31
 */
public class DbSortDO {
    /**
     * 排序字段, 例如: id
     */
    private String dbSortBy;
    /**
     * 排序类型,升序asc/降序desc
     */
    private String dbSortType;

    public String getDbSortBy() {
        return dbSortBy;
    }

    public void setDbSortBy(String dbSortBy) {
        this.dbSortBy = dbSortBy;
    }

    public String getDbSortType() {
        return dbSortType;
    }

    public void setDbSortType(String dbSortType) {
        this.dbSortType = dbSortType;
    }
}
