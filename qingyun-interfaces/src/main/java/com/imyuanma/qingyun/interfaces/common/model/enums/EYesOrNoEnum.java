package com.imyuanma.qingyun.interfaces.common.model.enums;

/**
 * 是否枚举
 *
 * @author wangjy
 * @date 2022/03/20 14:52:37
 */
public enum EYesOrNoEnum {
    YES(1, "是"),
    NO(0, "否")
    ;
    private Integer code;
    private String text;

    EYesOrNoEnum(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    public String getCodeAsStr() {
        return String.valueOf(code);
    }

    public Integer getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
