package com.imyuanma.qingyun.interfaces.common.model.dto;

/**
 * 客户端信息
 *
 * @author wangjy
 * @date 2023/12/14 23:39:57
 */
public class ClientInfoDTO {
    /**
     * 客户端ip
     */
    private String ip;
    /**
     * 应用名
     */
    private String appName;
    /**
     * 版本号
     */
    private String appVersion;
    /**
     * 设备号
     */
    private String deviceId;
    /**
     * 设备类型
     */
    private String deviceType;
    /**
     * 设备型号 例如:华为meta60
     */
    private String deviceModel;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }


}
