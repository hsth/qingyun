package com.imyuanma.qingyun.interfaces.ums.model;

import java.io.Serializable;

/**
 * uri访问权限验证
 *
 * @author wangjy
 * @date 2023/06/04 20:43:32
 */
public class UriAccessVerifyDTO implements Serializable {
    static final long serialVersionUID = -2750118739466961941L;

    /**
     * 用户id
     */
    private Long userId;
    /**
     * 登录token
     */
    private String ssoToken;
    /**
     * 访问的uri
     */
    private String uri;


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getSsoToken() {
        return ssoToken;
    }

    public void setSsoToken(String ssoToken) {
        this.ssoToken = ssoToken;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
