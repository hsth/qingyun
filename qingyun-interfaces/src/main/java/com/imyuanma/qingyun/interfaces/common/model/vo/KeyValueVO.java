package com.imyuanma.qingyun.interfaces.common.model.vo;

/**
 * 键值对
 *
 * @author wangjy
 * @date 2022/05/04 21:32:10
 */
public class KeyValueVO<K> {
    /**
     * 键
     */
    private K key;
    /**
     * 值
     */
    private String value;

    public KeyValueVO() {
    }

    public KeyValueVO(K key, String value) {
        this.key = key;
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
