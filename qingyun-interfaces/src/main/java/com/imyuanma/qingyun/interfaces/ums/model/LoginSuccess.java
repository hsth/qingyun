package com.imyuanma.qingyun.interfaces.ums.model;

/**
 * 登录成功结果
 *
 * @author wangjy
 * @date 2022/07/09 22:12:44
 */
public class LoginSuccess {
    /**
     * 账号
     */
    private String account;
    /**
     * 令牌
     */
    private String token;
    /**
     * 重定向url,登录成功后跳转url
     */
    private String redirectTo;

    public LoginSuccess(String account, String token, String redirectTo) {
        this.account = account;
        this.token = token;
        this.redirectTo = redirectTo;
    }

    public static LoginSuccess success(String account, String token, String redirectTo) {
        return new LoginSuccess(account, token, redirectTo);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("{");
        sb.append("\"account\":").append(account != null ? "\"" : "").append(account).append(account != null ? "\"" : "");
        sb.append(", \"token\":").append(token != null ? "\"" : "").append(token).append(token != null ? "\"" : "");
        sb.append(", \"redirectTo\":").append(redirectTo != null ? "\"" : "").append(redirectTo).append(redirectTo != null ? "\"" : "");
        sb.append('}');
        return sb.toString();
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRedirectTo() {
        return redirectTo;
    }

    public void setRedirectTo(String redirectTo) {
        this.redirectTo = redirectTo;
    }
}
