package com.imyuanma.qingyun.interfaces.ums.model;

import java.io.Serializable;

/**
 * 登录用户对象
 *
 * @author wangjy
 * @date 2022/07/09 16:36:08
 */
public class LoginUserDTO implements Serializable {
    static final long serialVersionUID = 5141888659726020815L;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 姓名
     */
    private String name;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 账号
     */
    private String account;
    /**
     * 密码
     */
    private String password;
    /**
     * 状态,10有效,20无效
     */
    private Integer status;

    /**
     * 用户是否有效
     *
     * @return
     */
    public boolean isEnabled() {
        return status != null && status == 10;
    }


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
