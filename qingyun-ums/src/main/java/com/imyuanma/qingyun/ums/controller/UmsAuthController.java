package com.imyuanma.qingyun.ums.controller;

import com.imyuanma.qingyun.common.client.ums.LoginUserHolder;
import com.imyuanma.qingyun.common.core.structure.tree.TreeBuilder;
import com.imyuanma.qingyun.common.model.request.WebRequest;
import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.common.util.StringUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.ums.model.UmsResource;
import com.imyuanma.qingyun.ums.model.UmsRole;
import com.imyuanma.qingyun.ums.model.UmsRoleResource;
import com.imyuanma.qingyun.ums.model.UmsRoleUser;
import com.imyuanma.qingyun.ums.model.param.GrantAuthParam;
import com.imyuanma.qingyun.ums.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 权限网关
 *
 * @author wangjy
 * @date 2023/03/26 11:34:07
 */
@RestController
@RequestMapping(value = "/ums/umsAuth")
public class UmsAuthController {
    /**
     * 资源服务
     */
    @Autowired
    private IUmsResourceService resourceService;
    /**
     * 角色资源关联服务
     */
    @Autowired
    private IUmsRoleResourceService roleResourceService;
    /**
     * 角色用户关联服务
     */
    @Autowired
    private IUmsRoleUserService roleUserService;
    /**
     * 角色服务
     */
    @Autowired
    private IUmsRoleService roleService;
    /**
     * 权限服务
     */
    @Autowired
    private IUmsAuthService umsAuthService;

    @Trace("查询用户拥有的权限")
    @PostMapping("/getUserOwnedResourceCodes")
    public Result<List<String>> getUserOwnedResourceCodes() {
        Long userId = LoginUserHolder.getLoginUserId();
        if (userId != null) {
            List<UmsResource> list = umsAuthService.getUserOwnedResourceList(userId);
            if (CollectionUtil.isNotEmpty(list)) {
                return Result.success(list.stream().map(UmsResource::getCode).filter(StringUtil::isNotBlank).distinct().collect(Collectors.toList()));
            }
        }
        return Result.success();
    }

    @Trace("查询用户关联角色列表")
    @PostMapping("/getUserBindRoleList")
    public Result<List<UmsRole>> getUserBindRoleList(@RequestBody WebRequest<Long> webRequest) {
        AssertUtil.notNull(webRequest.getBody(), "入参用户不允许为空");
        // 角色列表
        List<UmsRole> roleList = roleService.getList(new UmsRole());
        if (CollectionUtil.isNotEmpty(roleList)) {
            // 用户已关联角色
            List<UmsRoleUser> relation = roleUserService.getListByUserId(webRequest.getBody());
            if (CollectionUtil.isNotEmpty(relation)) {
                Set<Long> checkedRole = relation.stream().map(UmsRoleUser::getRoleId).collect(Collectors.toSet());
                for (UmsRole umsRole : roleList) {
                    // 选中状态
                    umsRole.setChecked(checkedRole.contains(umsRole.getId()));
                }
            }
        }
        return Result.success(roleList);
    }


    @Trace("查询角色资源授权树")
    @PostMapping("/getRoleAuthResourceTree")
    public Result<List<UmsResource>> getRoleAuthResourceTree(@RequestBody WebRequest<Long> webRequest) {
        AssertUtil.notNull(webRequest.getBody(), "入参角色不允许为空");
        List<UmsResource> resourceList = resourceService.getList(new UmsResource());
        if (CollectionUtil.isNotEmpty(resourceList)) {
            Collections.sort(resourceList);
            // 查询角色关联的资源
            List<UmsRoleResource> list = roleResourceService.getListByRoleId(webRequest.getBody());
            if (CollectionUtil.isNotEmpty(list)) {
                Set<Long> set = list.stream().map(UmsRoleResource::getResourceId).collect(Collectors.toSet());
                for (UmsResource umsResource : resourceList) {
                    umsResource.setChecked(set.contains(umsResource.getId()));
                }
            }
        }
        return Result.success(TreeBuilder.buildTree(resourceList));
    }


    @Trace("角色授权")
    @PostMapping("/authToRole")
    public Result authToRole(@RequestBody WebRequest<GrantAuthParam> webRequest) {
        AssertUtil.notNull(webRequest.getBody(), "入参不允许为空");
        AssertUtil.notNull(webRequest.getBody().getRoleId(), "入参角色不允许为空");
        // 授权
        roleResourceService.grantAuth(webRequest.getBody().getRoleId(), webRequest.getBody().getResourceIdList());
        return Result.success();
    }


    @Trace("关联用户")
    @PostMapping("/roleBindUser")
    public Result roleBindUser(@RequestBody WebRequest<GrantAuthParam> webRequest) {
        AssertUtil.notNull(webRequest.getBody(), "入参不允许为空");
        AssertUtil.notNull(webRequest.getBody().getRoleId(), "入参角色不允许为空");
        AssertUtil.notEmpty(webRequest.getBody().getUserIdList(), "入参用户不允许为空");
        roleUserService.bindUser(webRequest.getBody().getRoleId(), webRequest.getBody().getUserIdList());
        return Result.success();
    }

    @Trace("分配角色")
    @PostMapping("/userBindRole")
    public Result userBindRole(@RequestBody WebRequest<GrantAuthParam> webRequest) {
        AssertUtil.notNull(webRequest.getBody(), "入参不允许为空");
        AssertUtil.notNull(webRequest.getBody().getUserId(), "入参用户不允许为空");
//        AssertUtil.notEmpty(webRequest.getBody().getRoleIdList(), "入参角色不允许为空");
        roleUserService.bindRole(webRequest.getBody().getUserId(), webRequest.getBody().getRoleIdList());
        return Result.success();
    }
}
