package com.imyuanma.qingyun.ums.model.enums;

/**
 * 组织类型枚举
 *
 * @author YuanMaKeJi
 * @date 2022-10-08 15:19:19
 */
public enum EUmsOrgTypeEnum {
    CODE_10(10, "单位"),
    CODE_20(20, "部门"),
    ;

    /**
     * code
     */
    private Integer code;
    /**
     * 文案
     */
    private String text;

    EUmsOrgTypeEnum(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    /**
     * 根据code获取枚举
     *
     * @param code code值
     */
    public static EUmsOrgTypeEnum getByCode(Integer code) {
        for (EUmsOrgTypeEnum e : EUmsOrgTypeEnum.values()) {
            if (e.code != null && e.code.equals(code)) {
                return e;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
