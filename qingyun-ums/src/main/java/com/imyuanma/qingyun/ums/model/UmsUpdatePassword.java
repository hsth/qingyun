package com.imyuanma.qingyun.ums.model;

import lombok.Data;

/**
 * 更新密码
 *
 * @author wangjy
 * @date 2022/07/16 19:07:42
 */
@Data
public class UmsUpdatePassword {
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 老密码
     */
    private String oldPwd;
    /**
     * 新密码
     */
    private String newPwd;
}
