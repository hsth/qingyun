package com.imyuanma.qingyun.ums.dao;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.ums.model.UmsOrg;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 组织机构dao
 *
 * @author YuanMaKeJi
 * @date 2022-10-08 15:19:19
 */
@Mapper
public interface IUmsOrgDao {

    /**
     * 列表查询
     *
     * @param umsOrg 查询条件
     * @return
     */
    List<UmsOrg> getList(UmsOrg umsOrg);

    /**
     * 分页查询
     *
     * @param umsOrg 查询条件
     * @param pageQuery 分页参数
     * @return
     */
    List<UmsOrg> getList(UmsOrg umsOrg, PageQuery pageQuery);

    /**
     * 统计数量
     *
     * @param umsOrg 查询条件
     * @return
     */
    int count(UmsOrg umsOrg);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    UmsOrg get(Long id);

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    List<UmsOrg> getListByIds(List<Long> list);

    /**
     * 插入
     *
     * @param umsOrg 参数
     * @return
     */
    int insert(UmsOrg umsOrg);

    /**
     * 选择性插入
     *
     * @param umsOrg 参数
     * @return
     */
    int insertSelective(UmsOrg umsOrg);

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    int batchInsert(List<UmsOrg> list);

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    int batchInsertSelective(List<UmsOrg> list);

    /**
     * 修改
     *
     * @param umsOrg 参数
     * @return
     */
    int update(UmsOrg umsOrg);

    /**
     * 选择性修改
     *
     * @param umsOrg 参数
     * @return
     */
    int updateSelective(UmsOrg umsOrg);

    /**
     * 批量修改某字段
     *
     * @param idList      主键集合
     * @param fieldDbName 待修改的字段名
     * @param fieldValue  修改后的值
     * @return
     */
    int batchUpdateColumn(@Param("idList") List<Long> idList, @Param("fieldDbName") String fieldDbName, @Param("fieldValue") Object fieldValue);

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    int delete(Long id);

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    int batchDelete(List<Long> list);

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param umsOrg 参数
     * @return
     */
    int deleteByCondition(UmsOrg umsOrg);

    /**
     * 删除全部
     *
     * @return
     */
    int deleteAll();

}
