package com.imyuanma.qingyun.ums.model;

import com.imyuanma.qingyun.common.core.excel.ExcelColumn;
import com.imyuanma.qingyun.common.core.structure.tree.ITreeNode;
import com.imyuanma.qingyun.ums.model.enums.EUmsResourceStatusEnum;
import com.imyuanma.qingyun.ums.model.enums.EUmsResourceTypeEnum;
import com.imyuanma.qingyun.ums.util.UmsConstants;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

import com.imyuanma.qingyun.interfaces.common.model.BaseDO;

/**
 * 资源实体类
 *
 * @author YuanMaKeJi
 * @date 2022-10-05 21:06:53
 */
@Data
public class UmsResource extends BaseDO implements ITreeNode<Long>, Comparable<UmsResource> {

    /**
     * 主键
     */
    private Long id;

    /**
     * 资源编号
     */
    @ExcelColumn(value = "资源编码", order = 10)
    private String code;

    /**
     * 图标
     */
    @ExcelColumn(value = "图标", order = 30)
    private String icon;

    /**
     * 资源名称
     */
    @ExcelColumn(value = "资源名称", order = 20)
    private String name;

    /**
     * 上级id
     */
    private Long parentId;

    /**
     * 链接
     */
    @ExcelColumn(value = "链接", order = 60)
    private String href;

    /**
     * 类型,10页面,20按钮,30操作,40逻辑(一切皆逻辑)
     */
    @ExcelColumn(value = "类型", order = 70)
    private Integer type;

    /**
     * 备注
     */
    @ExcelColumn(value = "备注", order = 80)
    private String remark;

    /**
     * 标签
     */
    @ExcelColumn(value = "标签", order = 90)
    private String tag;

    /**
     * 状态,10有效,20无效
     */
    @ExcelColumn(value = "状态", order = 100)
    private Integer status;

    /**
     * 所属单位id
     */
    @ExcelColumn(value = "所属单位id", order = 110)
    private Long companyId;

    /**
     * 数据权限id
     */
    @ExcelColumn(value = "数据权限id", order = 120)
    private Long dataPermissionId;

    /**
     * 子节点
     */
    private List<UmsResource> children;

    /**
     * 选中状态
     */
    private Boolean checked;
    /**
     * 待查询资源id集合
     * 作为查询参数用
     */
    private List<Long> searchIdList;
    /**
     * 上级code
     */
    @ExcelColumn(value = "上级code", order = 50)
    private String parentCode;

    /**
     * 构造虚拟根节点
     * 用来创建资源时作为根节点供选择
     * @return
     */
    public static UmsResource buildVirtualRoot() {
        UmsResource umsResource = new UmsResource();
        umsResource.setId(0L);
        umsResource.setCode("__virtual_root__");
        umsResource.setName("ROOT");
        umsResource.setParentId(-1L);
        umsResource.setType(EUmsResourceTypeEnum.CODE_10.getCode());
        umsResource.setStatus(EUmsResourceStatusEnum.VALID.getCode());
        umsResource.setCompanyId(UmsConstants.ROOT_ORG_ID);
        umsResource.setDataSequence(0L);
        return umsResource;
    }

    /**
     * 当前节点id
     *
     * @return
     */
    @Override
    public Long nodeId() {
        return id;
    }

    /**
     * 父节点id
     *
     * @return
     */
    @Override
    public Long parentNodeId() {
        return parentId;
    }

    /**
     * 添加子节点
     *
     * @param node 节点对象
     */
    @Override
    public void appendChildren(ITreeNode<Long> node) {
        if (children == null) {
            children = new ArrayList<>();
        }
        children.add((UmsResource) node);
    }

    /**
     * 比较
     * @param o
     * @return
     */
    @Override
    public int compareTo(UmsResource o) {
        Long a1 = this.getDataSequence() != null ? this.getDataSequence() : 0L;
        Long a2 = o.getDataSequence() != null ? o.getDataSequence() : 0L;
        return a1.compareTo(a2);
    }
}