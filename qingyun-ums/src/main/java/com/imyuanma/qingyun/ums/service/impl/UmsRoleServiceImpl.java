package com.imyuanma.qingyun.ums.service.impl;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.ums.dao.IUmsRoleDao;
import com.imyuanma.qingyun.ums.model.UmsRole;
import com.imyuanma.qingyun.ums.service.IUmsRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 角色信息服务
 *
 * @author YuanMaKeJi
 * @date 2023-03-25 13:23:39
 */
@Slf4j
@Service
public class UmsRoleServiceImpl implements IUmsRoleService {

    /**
     * 角色信息dao
     */
    @Autowired
    private IUmsRoleDao umsRoleDao;

    /**
     * 列表查询
     *
     * @param umsRole 查询条件
     * @return
     */
    @Trace("查询角色信息列表")
    @Override
    public List<UmsRole> getList(UmsRole umsRole) {
        return umsRoleDao.getList(umsRole);
    }

    /**
     * 分页查询
     *
     * @param umsRole 查询条件
     * @param pageQuery 分页参数
     * @return
     */
    @Trace("分页查询角色信息")
    @Override
    public List<UmsRole> getPage(UmsRole umsRole, PageQuery pageQuery) {
        return umsRoleDao.getList(umsRole, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param umsRole 查询条件
     * @return
     */
    @Trace("统计角色信息数量")
    @Override
    public int count(UmsRole umsRole) {
        return umsRoleDao.count(umsRole);
    }

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    @Trace("根据主键查询角色信息")
    @Override
    public UmsRole get(Long id) {
        return umsRoleDao.get(id);
    }

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    @Trace("根据主键批量查询角色信息")
    @Override
    public List<UmsRole> getListByIds(List<Long> list) {
        return umsRoleDao.getListByIds(list);
    }

    /**
     * 根据code查询
     *
     * @param code 角色编号
     * @return
     */
    @Trace("根据code查询角色信息")
    @Override
    public UmsRole getByCode(String code) {
        return umsRoleDao.getByCode(code);
    }

    /**
     * 根据companyId查询
     *
     * @param companyId 所属单位id
     * @return
     */
    @Trace("根据companyId查询角色信息")
    @Override
    public List<UmsRole> getListByCompanyId(Long companyId) {
        return umsRoleDao.getListByCompanyId(companyId);
    }

    /**
     * 根据category查询
     *
     * @param category 角色类别
     * @return
     */
    @Trace("根据category查询角色信息")
    @Override
    public List<UmsRole> getListByCategory(String category) {
        return umsRoleDao.getListByCategory(category);
    }

    /**
     * 根据openType查询
     *
     * @param openType 公开类型,10不公开,20向直属下级单位公开,30向所有下级单位公开,40向部分下级单位公开
     * @return
     */
    @Trace("根据openType查询角色信息")
    @Override
    public List<UmsRole> getListByOpenType(Integer openType) {
        return umsRoleDao.getListByOpenType(openType);
    }

    /**
     * 根据dataPermissionId查询
     *
     * @param dataPermissionId 数据权限id
     * @return
     */
    @Trace("根据dataPermissionId查询角色信息")
    @Override
    public List<UmsRole> getListByDataPermissionId(Long dataPermissionId) {
        return umsRoleDao.getListByDataPermissionId(dataPermissionId);
    }

    /**
     * 插入
     *
     * @param umsRole 参数
     * @return
     */
    @Trace("插入角色信息")
    @Override
    public int insert(UmsRole umsRole) {
        return umsRoleDao.insert(umsRole);
    }

    /**
     * 选择性插入
     *
     * @param umsRole 参数
     * @return
     */
    @Trace("选择性插入角色信息")
    @Override
    public int insertSelective(UmsRole umsRole) {
        return umsRoleDao.insertSelective(umsRole);
    }

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量插入角色信息")
    @Override
    public int batchInsert(List<UmsRole> list) {
        return umsRoleDao.batchInsert(list);
    }

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量选择性插入角色信息")
    @Override
    public int batchInsertSelective(List<UmsRole> list) {
        return umsRoleDao.batchInsertSelective(list);
    }

    /**
     * 修改
     *
     * @param umsRole 参数
     * @return
     */
    @Trace("修改角色信息")
    @Override
    public int update(UmsRole umsRole) {
        return umsRoleDao.update(umsRole);
    }

    /**
     * 选择性修改
     *
     * @param umsRole 参数
     * @return
     */
    @Trace("选择性修改角色信息")
    @Override
    public int updateSelective(UmsRole umsRole) {
        return umsRoleDao.updateSelective(umsRole);
    }

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    @Trace("删除角色信息")
    @Override
    public int delete(Long id) {
        return umsRoleDao.delete(id);
    }

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    @Trace("批量删除角色信息")
    @Override
    public int batchDelete(List<Long> list) {
        return umsRoleDao.batchDelete(list);
    }

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param umsRole 参数
     * @return
     */
    @Trace("根据条件删除角色信息")
    @Override
    public int deleteByCondition(UmsRole umsRole) {
        return umsRoleDao.deleteByCondition(umsRole);
    }

}
