package com.imyuanma.qingyun.ums.service;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.ums.model.UmsUpdatePassword;
import com.imyuanma.qingyun.ums.model.UmsUser;

import java.util.List;

/**
 * 用户信息服务
 *
 * @author YuanMaKeJi
 * @date 2022-07-09 16:23:45
 */
public interface IUmsUserService {

    /**
     * 列表查询
     *
     * @param umsUser 查询条件
     * @return
     */
    List<UmsUser> getList(UmsUser umsUser);

    /**
     * 分页查询
     *
     * @param umsUser   查询条件
     * @param pageQuery 分页参数
     * @return
     */
    List<UmsUser> getPage(UmsUser umsUser, PageQuery pageQuery);

    /**
     * 统计数量
     *
     * @param umsUser 查询条件
     * @return
     */
    int count(UmsUser umsUser);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    UmsUser get(Long id);

    /**
     * 根据账号查询用户信息
     *
     * @param account 账号
     * @return
     */
    UmsUser getByAccount(String account);

    /**
     * 根据密码找回key查询用户
     *
     * @param findPasswordKey
     * @return
     */
    UmsUser getByFindPwdKey(String findPasswordKey);

    /**
     * 插入
     *
     * @param umsUser 参数
     * @return
     */
    int insert(UmsUser umsUser);

    /**
     * 选择性插入
     *
     * @param umsUser 参数
     * @return
     */
    int insertSelective(UmsUser umsUser);

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    int batchInsert(List<UmsUser> list);

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    int batchInsertSelective(List<UmsUser> list);

    /**
     * 修改
     *
     * @param umsUser 参数
     * @return
     */
    int update(UmsUser umsUser);

    /**
     * 选择性修改
     *
     * @param umsUser 参数
     * @return
     */
    int updateSelective(UmsUser umsUser);

    /**
     * 更新密码
     *
     * @param umsUpdatePassword
     * @return
     */
    int updatePassword(UmsUpdatePassword umsUpdatePassword);

    /**
     * 管理员更新密码
     *
     * @param umsUpdatePassword
     * @return
     */
    void updatePasswordForAdmin(UmsUpdatePassword umsUpdatePassword);

    /**
     * 管理员重置用户密码
     *
     * @param userIds
     */
    void resetPasswordForAdmin(Long[] userIds);

    /**
     * 重置密码
     *
     * @param userId
     * @param pwd
     * @return
     */
    int resetPassword(Long userId, String pwd);

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    int delete(Long id);

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    int batchDelete(List<Long> list);

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param umsUser 参数
     * @return
     */
    int deleteByCondition(UmsUser umsUser);

    /**
     * 密码找回
     *
     * @param umsUser
     */
    String findPasswordInit(UmsUser umsUser);

}
