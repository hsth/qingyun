package com.imyuanma.qingyun.ums.model.enums;

/**
 * 资源类型枚举
 *
 * @author YuanMaKeJi
 * @date 2022-10-05 22:53:20
 */
public enum EUmsResourceTypeEnum {
    CODE_10(10, "菜单"),
    CODE_20(20, "按钮"),
    CODE_30(30, "操作"),
    CODE_40(40, "逻辑"),
    CODE_50(50, "独立页面"),
    CODE_60(60, "URI"),
    ;

    /**
     * code
     */
    private Integer code;
    /**
     * 文案
     */
    private String text;

    EUmsResourceTypeEnum(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    /**
     * 根据code获取枚举
     *
     * @param code code值
     */
    public static EUmsResourceTypeEnum getByCode(Integer code) {
        for (EUmsResourceTypeEnum e : EUmsResourceTypeEnum.values()) {
            if (e.code != null && e.code.equals(code)) {
                return e;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
