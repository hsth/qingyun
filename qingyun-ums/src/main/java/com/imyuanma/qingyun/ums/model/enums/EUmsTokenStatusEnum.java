package com.imyuanma.qingyun.ums.model.enums;

/**
 * 会话(令牌)状态
 *
 * @author wangjy
 * @date 2022/07/16 14:38:44
 */
public enum EUmsTokenStatusEnum {
    ENABLE(10, "有效"),
    DISABLE(20, "无效"),
    /**
     * 无实际意义
     */
    EXPIRE(30, "已过期")
    ;
    /**
     * code
     */
    private Integer code;
    /**
     * 文案
     */
    private String text;

    EUmsTokenStatusEnum(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    public Integer getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
