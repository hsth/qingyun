package com.imyuanma.qingyun.ums.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 小程序登录配置
 *
 * @author wangjy
 * @date 2023/10/15 17:05:56
 */
@Configuration
@ConfigurationProperties(prefix="qingyun.ums.sso.mini")
public class UmsSsoMiniConfiguration {
    /**
     * 小程序appId
     */
    private String appid;
    /**
     * 小程序secret
     */
    private String secret;

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
