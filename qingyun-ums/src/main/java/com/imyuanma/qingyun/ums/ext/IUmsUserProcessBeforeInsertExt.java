package com.imyuanma.qingyun.ums.ext;

import com.imyuanma.qingyun.interfaces.common.ext.IExtNode;
import com.imyuanma.qingyun.ums.model.UmsUser;

/**
 * 用户插入前的处理
 *
 * @author wangjy
 * @date 2023/07/08 13:36:34
 */
public interface IUmsUserProcessBeforeInsertExt extends IExtNode<UmsUser, Boolean> {
}
