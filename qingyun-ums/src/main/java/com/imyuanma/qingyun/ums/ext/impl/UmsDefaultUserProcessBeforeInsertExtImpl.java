package com.imyuanma.qingyun.ums.ext.impl;

import com.imyuanma.qingyun.interfaces.common.ext.ExtNodeResult;
import com.imyuanma.qingyun.ums.ext.IUmsUserProcessBeforeInsertExt;
import com.imyuanma.qingyun.ums.model.UmsUser;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Service;

/**
 * 用户插入前扩展点默认实现
 *
 * @author wangjy
 * @date 2023/07/09 13:10:08
 */
@Service
public class UmsDefaultUserProcessBeforeInsertExtImpl implements IUmsUserProcessBeforeInsertExt {
    /**
     * 执行扩展点
     *
     * @param extNodeParam
     * @return
     */
    @Override
    public ExtNodeResult<Boolean> execute(UmsUser extNodeParam) {
        return ExtNodeResult.success(true);
    }

    /**
     * 排序序号
     *
     * @return 越小优先级越高
     */
    @Override
    public long ordered() {
        return Ordered.LOWEST_PRECEDENCE;
    }
}
