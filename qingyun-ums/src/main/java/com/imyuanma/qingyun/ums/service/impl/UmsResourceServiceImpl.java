package com.imyuanma.qingyun.ums.service.impl;

import com.imyuanma.qingyun.common.core.structure.tree.TreeBuilder;
import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.common.util.StringUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.ums.dao.IUmsResourceDao;
import com.imyuanma.qingyun.ums.model.UmsResource;
import com.imyuanma.qingyun.ums.model.enums.EUmsResourceStatusEnum;
import com.imyuanma.qingyun.ums.model.param.BatchAddResourceParam;
import com.imyuanma.qingyun.ums.service.IUmsResourceService;
import com.imyuanma.qingyun.ums.util.UmsConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 资源服务
 *
 * @author YuanMaKeJi
 * @date 2022-10-08 11:59:54
 */
@Slf4j
@Service
public class UmsResourceServiceImpl implements IUmsResourceService {

    /**
     * 资源dao
     */
    @Autowired
    private IUmsResourceDao umsResourceDao;

    /**
     * 获取所有链接有效的资源数据
     *
     * @return
     */
    @Trace("获取所有链接有效的资源数据")
    @Override
    public List<UmsResource> getAllValidHrefResource() {
        return umsResourceDao.getAllValidHrefResource();
    }

    /**
     * 查询资源树
     *
     * @param umsResource
     * @return
     */
    @Trace("查询资源树")
    @Override
    public List<UmsResource> getTree(UmsResource umsResource) {
        // 查询列表
        List<UmsResource> list = this.getList(umsResource);
        if (CollectionUtil.isNotEmpty(list)) {
            Collections.sort(list);
        }
        return TreeBuilder.buildTree(list);
    }

    /**
     * 列表查询
     *
     * @param umsResource 查询条件
     * @return
     */
    @Trace("查询资源列表")
    @Override
    public List<UmsResource> getList(UmsResource umsResource) {
        return umsResourceDao.getList(umsResource);
    }

    /**
     * 分页查询
     *
     * @param umsResource 查询条件
     * @param pageQuery   分页参数
     * @return
     */
    @Trace("分页查询资源")
    @Override
    public List<UmsResource> getPage(UmsResource umsResource, PageQuery pageQuery) {
        return umsResourceDao.getList(umsResource, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param umsResource 查询条件
     * @return
     */
    @Trace("统计资源数量")
    @Override
    public int count(UmsResource umsResource) {
        return umsResourceDao.count(umsResource);
    }

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    @Trace("根据主键查询资源")
    @Override
    public UmsResource get(Long id) {
        return umsResourceDao.get(id);
    }

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    @Trace("根据主键批量查询资源")
    @Override
    public List<UmsResource> getListByIds(List<Long> list) {
        return umsResourceDao.getListByIds(list);
    }

    /**
     * code批量查询
     *
     * @param codes
     * @return
     */
    @Trace("code批量查询")
    @Override
    public List<UmsResource> getListByCodes(List<String> codes) {
        return umsResourceDao.getListByCodes(codes);
    }

    /**
     * 插入
     *
     * @param umsResource 参数
     * @return
     */
    @Trace("插入资源")
    @Override
    public int insert(UmsResource umsResource) {
        return umsResourceDao.insert(umsResource);
    }

    /**
     * 选择性插入
     *
     * @param umsResource 参数
     * @return
     */
    @Trace("选择性插入资源")
    @Override
    public int insertSelective(UmsResource umsResource) {
        this.handleDataBeforeInsert(umsResource);
        return umsResourceDao.insertSelective(umsResource);
    }

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量插入资源")
    @Override
    public int batchInsert(List<UmsResource> list) {
        list.forEach(this::handleDataBeforeInsert);
        return umsResourceDao.batchInsert(list);
    }

    /**
     * 批量创建资源
     *
     * @param batchAddResourceParam
     * @return
     */
    @Trace("批量创建资源")
    @Override
    public void batchAddResource(BatchAddResourceParam batchAddResourceParam) {
        // 查询上级资源
        UmsResource parent = this.get(batchAddResourceParam.getParentId());
        AssertUtil.notNull(parent, "上级资源查询失败");
        for (UmsResource umsResource : batchAddResourceParam.getResourceList()) {
            umsResource.setParentId(parent.getId());
            umsResource.setCompanyId(parent.getCompanyId());
            umsResource.fillBaseField(batchAddResourceParam);
        }
        // 批量插入
        this.batchInsert(batchAddResourceParam.getResourceList());
    }

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量选择性插入资源")
    @Override
    public int batchInsertSelective(List<UmsResource> list) {
        list.forEach(this::handleDataBeforeInsert);
        return umsResourceDao.batchInsertSelective(list);
    }

    /**
     * 插入前数据处理
     *
     * @param umsResource
     */
    private void handleDataBeforeInsert(UmsResource umsResource) {
        if (umsResource.getParentId() == null) {
            umsResource.setParentId(UmsConstants.ROOT_RESOURCE_ID);
        }
        if (umsResource.getStatus() == null) {
            umsResource.setStatus(EUmsResourceStatusEnum.VALID.getCode());
        }
        if (umsResource.getCompanyId() == null) {
            umsResource.setCompanyId(UmsConstants.ROOT_ORG_ID);
        }
    }

    /**
     * 更新前数据处理
     *
     * @param umsResource
     */
    private void handleDataBeforeUpdate(UmsResource umsResource) {

    }

    /**
     * 修改
     *
     * @param umsResource 参数
     * @return
     */
    @Trace("修改资源")
    @Override
    public int update(UmsResource umsResource) {
        this.handleDataBeforeUpdate(umsResource);
        return umsResourceDao.update(umsResource);
    }

    /**
     * 选择性修改
     *
     * @param umsResource 参数
     * @return
     */
    @Trace("选择性修改资源")
    @Override
    public int updateSelective(UmsResource umsResource) {
        this.handleDataBeforeUpdate(umsResource);
        return umsResourceDao.updateSelective(umsResource);
    }

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    @Trace("删除资源")
    @Override
    public int delete(Long id) {
        return umsResourceDao.delete(id);
    }

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    @Trace("批量删除资源")
    @Override
    public int batchDelete(List<Long> list) {
        return umsResourceDao.batchDelete(list);
    }

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param umsResource 参数
     * @return
     */
    @Trace("根据条件删除资源")
    @Override
    public int deleteByCondition(UmsResource umsResource) {
        return umsResourceDao.deleteByCondition(umsResource);
    }

    /**
     * 补全资源集合的parentCode属性
     *
     * @param list
     */
    @Trace("补全资源集合的parentCode属性")
    @Override
    public void fillParentCodeByParentId(List<UmsResource> list) {
        if (CollectionUtil.isEmpty(list)) {
            return;
        }
        // 父id
        List<Long> parentIds = list.stream().map(UmsResource::getParentId).filter(Objects::nonNull).distinct().collect(Collectors.toList());
        if (CollectionUtil.isEmpty(parentIds)) {
            return;
        }
        // 查询上级
        List<UmsResource> parentList = this.getListByIds(parentIds);
        if (CollectionUtil.isEmpty(parentList)) {
            return;
        }
        Map<Long, UmsResource> map = CollectionUtil.toMap(parentList, UmsResource::getId);
        for (UmsResource resource : list) {
            if (resource.getParentId() != null) {
                UmsResource parent = map.get(resource.getParentId());
                if (parent != null) {
                    resource.setParentCode(parent.getCode());
                }
            }
        }
    }

    /**
     * 补全资源集合的parentId属性
     *
     * @param list
     */
    @Trace("补全资源集合的parentId属性")
    @Override
    public void fillParentIdByParentCode(List<UmsResource> list) {
        if (CollectionUtil.isEmpty(list)) {
            return;
        }
        // 父code
        List<String> parentCodes = list.stream().map(UmsResource::getParentCode).filter(StringUtil::isNotBlank).distinct().collect(Collectors.toList());
        if (CollectionUtil.isEmpty(parentCodes)) {
            return;
        }
        // 查询父资源
        List<UmsResource> parentList = this.getListByCodes(parentCodes);
        if (CollectionUtil.isEmpty(parentList)) {
            return;
        }
        Map<String, UmsResource> map = CollectionUtil.toMap(parentList, UmsResource::getCode);
        for (UmsResource resource : list) {
            if (StringUtil.isNotBlank(resource.getParentCode())) {
                UmsResource parent = map.get(resource.getParentCode());
                if (parent != null) {
                    resource.setParentId(parent.getId());
                }
            }
        }
    }

}
