package com.imyuanma.qingyun.ums.util;


import com.imyuanma.qingyun.common.exception.ParamException;
import com.imyuanma.qingyun.common.util.encript.MD5Util;
import com.imyuanma.qingyun.common.util.encript.RSAUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * 密码处理工具类
 *
 * @author wangjy
 * @date 2023/04/08 11:04:31
 */
public class UmsUserPwdUtil {
    private static final Logger logger = LoggerFactory.getLogger(UmsUserPwdUtil.class);
    /**
     * 加密盐
     */
    private static String SALT = "qingyun";
    /**
     * 密码解密私钥, 公钥在前端
     */
    public static final String SSO_CLIENT_RSA_KEY_PRIVATE = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAJ+brwwCQyYJ9c+Y/P9JLBo6sRCjuXEMpO2bbXOBNGgpjn41N6iD2ulRRukR8po7FHLsrAX+B459xHQBZIEkdCS3r135K5Q5Ei3LSlyEiNQ0ZyVvGP4sJSVnDIIi4aMGTkKSUF2Lg1GhseDHawrhWSf2hlWwFpDTItD704kW9rmJAgMBAAECgYEAh6a1u6sgju/gLzSDsiTqitBfNRkxvtdURW3YNFcXx3+qT7HDQEHwiGQ/tE9AtbyIwLRN8DnbCkvx7/ZPCUOGQWR8FDZ7BLHl9EVJXQ8ffWlCwrT9ZCbOOCpF3yBhpJNG5LCtxt4yrZnX5PibGqcQkP3jW0/+X+uQlvPMFYmMA1ECQQDkhfjI4pxDUaHPU7O6twpMkpyj0Tt0wUFQ4maGFPn2s2PC03mgGxPkuKEaxHL4B7ao5DtWHiJWGCpwMcGCNH/rAkEAssx6cobfOqAk1X8KetHu7YwCQnbyloIjQJ9kg43t2bR2GJcp1bTsLveZf1Ch3ZxpwSO5c4j8qbZ+mEQt9IiDWwJAM7/m887lZhVBWErzI3A549c7o5lJJopw+Rkb8Hcll+lNyRvMqiYXni71RLOB+Yr9oUd17G2MhwSX76pE0PCEmwJAYli2wWgOQWD04boPOZ9fnKn2VDi5FrBeU51Y3EOlIKpyivQavsHVZ8ApXi4r2om+Yc4Uo8glsfP/jiFyZZ7xIQJBAIPqU1Fu8xc4+xmuc7Ab2+gDQjK58rGMM7ODOlXuPjB7tkPQCWW5TppCX87y8znn0kT/WAKYSD8wxsOYULIipXA=";


    /**
     * 密码加密
     *
     * @param pwd 明文密码
     * @return
     */
    public static String encodePwd(String pwd) {
        return MD5Util.MD5(pwd + SALT);
    }

    /**
     * 对前端传入的密码密文解密
     *
     * @param pwdCiphertext 前端传入的密码密文
     * @return 密码明文
     */
    public static String parsePwd(String pwdCiphertext) {
        try {
            byte[] bytes = RSAUtil.decryptByPrivate(Base64.getDecoder().decode(pwdCiphertext), SSO_CLIENT_RSA_KEY_PRIVATE);
            return new String(Base64.getDecoder().decode(bytes), StandardCharsets.UTF_8);
        } catch (Throwable e) {
            logger.error("[解析用户真正输入的密码] 解密时发生异常,入参={}", pwdCiphertext, e);
            throw new ParamException("入参密码解密失败", e);
        }
    }
}
