package com.imyuanma.qingyun.ums.model.param;

import lombok.Data;

import java.util.List;

/**
 * 分配权限入参
 *
 * @author wangjy
 * @date 2023/03/26 12:33:58
 */
@Data
public class GrantAuthParam {
    /**
     * 角色
     */
    private Long roleId;
    /**
     * 用户
     */
    private Long userId;
    /**
     * 资源id集合
     */
    private List<Long> resourceIdList;
    /**
     * 用户id集合
     */
    private List<Long> userIdList;
    /**
     * 角色id集合
     */
    private List<Long> roleIdList;
}
