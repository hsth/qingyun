package com.imyuanma.qingyun.ums.service;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.ums.model.UmsUserWx;

import java.util.List;

/**
 * 用户微信信息服务
 *
 * @author YuanMaKeJi
 * @date 2023-11-05 23:03:28
 */
public interface IUmsUserWxService {

    /**
     * 列表查询
     *
     * @param umsUserWx 查询条件
     * @return
     */
    List<UmsUserWx> getList(UmsUserWx umsUserWx);

    /**
     * 分页查询
     *
     * @param umsUserWx 查询条件
     * @param pageQuery 分页参数
     * @return
     */
    List<UmsUserWx> getPage(UmsUserWx umsUserWx, PageQuery pageQuery);

    /**
     * 统计数量
     *
     * @param umsUserWx 查询条件
     * @return
     */
    int count(UmsUserWx umsUserWx);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    UmsUserWx get(Long id);

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    List<UmsUserWx> getListByIds(List<Long> list);

    /**
     * 根据userId查询
     *
     * @param userId 用户ID
     * @return
     */
    UmsUserWx getByUserId(Long userId);

    /**
     * 根据unionId查询
     *
     * @param unionId 微信unionid
     * @return
     */
    UmsUserWx getByUnionId(String unionId);

    /**
     * 根据openId查询
     *
     * @param openId 微信openid
     * @return
     */
    UmsUserWx getByOpenId(String openId);

    /**
     * 根据sessionKey查询
     *
     * @param sessionKey 会话key
     * @return
     */
    UmsUserWx getBySessionKey(String sessionKey);

    /**
     * 插入
     *
     * @param umsUserWx 参数
     * @return
     */
    int insert(UmsUserWx umsUserWx);

    /**
     * 选择性插入
     *
     * @param umsUserWx 参数
     * @return
     */
    int insertSelective(UmsUserWx umsUserWx);

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    int batchInsert(List<UmsUserWx> list);

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    int batchInsertSelective(List<UmsUserWx> list);

    /**
     * 修改
     *
     * @param umsUserWx 参数
     * @return
     */
    int update(UmsUserWx umsUserWx);

    /**
     * 选择性修改
     *
     * @param umsUserWx 参数
     * @return
     */
    int updateSelective(UmsUserWx umsUserWx);

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    int delete(Long id);

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    int batchDelete(List<Long> list);

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param umsUserWx 参数
     * @return
     */
    int deleteByCondition(UmsUserWx umsUserWx);

}
