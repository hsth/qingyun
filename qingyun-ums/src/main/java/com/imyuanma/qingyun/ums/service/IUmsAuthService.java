package com.imyuanma.qingyun.ums.service;

import com.imyuanma.qingyun.ums.model.UmsResource;

import java.util.List;

/**
 * 权限服务
 *
 * @author wangjy
 * @date 2023/06/08 23:21:35
 */
public interface IUmsAuthService {
    /**
     * 校验uri权限
     *
     * @param userId 用户id
     * @param uri    uri路径
     * @return
     */
    boolean verifyUri(Long userId, String uri);

    /**
     * 获取用户拥有的资源id集合
     *
     * @param userId 用户id
     * @return
     */
    List<Long> getUserOwnedResourceIdList(Long userId);

    /**
     * 获取用户拥有的资源集合
     *
     * @param userId 用户id
     * @return
     */
    List<UmsResource> getUserOwnedResourceList(Long userId);
}
