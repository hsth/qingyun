package com.imyuanma.qingyun.ums.model;

import lombok.Data;
import java.util.Date;
import com.imyuanma.qingyun.interfaces.common.model.BaseDO;

/**
 * 角色信息实体类
 *
 * @author YuanMaKeJi
 * @date 2023-03-25 13:23:39
 */
@Data
public class UmsRole extends BaseDO {

    /**
     * 主键
     */
    private Long id;

    /**
     * 角色编号
     */
    private String code;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 所属单位id
     */
    private Long companyId;

    /**
     * 数据权限id
     */
    private Long dataPermissionId;

    /**
     * 角色类别
     */
    private String category;

    /**
     * 公开类型,10不公开,20向直属下级单位公开,30向所有下级单位公开,40向部分下级单位公开
     */
    private Integer openType;

    /**
     * 备注
     */
    private String remark;

    /**
     * 选中状态
     */
    private Boolean checked;

    // 单位名称
    private String companyName;

}