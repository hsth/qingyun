package com.imyuanma.qingyun.ums.service.out;

import com.imyuanma.qingyun.common.model.ECommonResultCode;
import com.imyuanma.qingyun.common.util.DateUtil;
import com.imyuanma.qingyun.common.util.IPUtil;
import com.imyuanma.qingyun.common.util.JsonUtil;
import com.imyuanma.qingyun.common.util.StringUtil;
import com.imyuanma.qingyun.interfaces.common.model.ApiResult;
import com.imyuanma.qingyun.interfaces.ums.model.*;
import com.imyuanma.qingyun.interfaces.ums.service.IUmsPermissionsOutService;
import com.imyuanma.qingyun.ums.model.UmsLoginLog;
import com.imyuanma.qingyun.ums.model.UmsSession;
import com.imyuanma.qingyun.ums.model.UmsUser;
import com.imyuanma.qingyun.interfaces.ums.model.enums.EUmsLoginDeviceTypeEnum;
import com.imyuanma.qingyun.ums.model.enums.EUmsLoginResultEnum;
import com.imyuanma.qingyun.ums.model.enums.EUmsTokenStatusEnum;
import com.imyuanma.qingyun.ums.service.IUmsAuthService;
import com.imyuanma.qingyun.ums.service.IUmsLoginLogService;
import com.imyuanma.qingyun.ums.service.IUmsSessionService;
import com.imyuanma.qingyun.ums.service.IUmsUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 统一用户权限对外服务
 *
 * @author wangjy
 * @date 2022/07/09 16:34:49
 */
@Service
public class UmsPermissionsOutServiceImpl implements IUmsPermissionsOutService {
    private static final Logger logger = LoggerFactory.getLogger(UmsPermissionsOutServiceImpl.class);
    /**
     * 用户服务
     */
    @Autowired
    private IUmsUserService umsUserService;
    /**
     * 会话服务
     */
    @Autowired
    private IUmsSessionService umsSessionService;
    /**
     * 登录日志服务
     */
    @Autowired
    private IUmsLoginLogService umsLoginLogService;
    /**
     * 权限
     */
    @Autowired
    private IUmsAuthService umsAuthService;

    /**
     * 根据账号获取用户
     *
     * @param account 账号
     * @return
     */
    @Override
    public LoginUserDTO getByAccount(String account) {
        UmsUser umsUser = umsUserService.getByAccount(account);
        logger.info("[根据账号获取用户] 账号={},用户信息={}", account, umsUser);
        return umsUser != null ? umsUser.convert() : null;
    }

    /**
     * 登录校验通过后创建会话
     *
     * @param loginUserDTO 登录用户
     * @param terminalDTO  客户端信息
     * @return 会话凭证(令牌)
     */
    @Override
    public String createSessionAfterLoginCheckSuccess(LoginUserDTO loginUserDTO, TerminalDTO terminalDTO) {
        return umsSessionService.createLoginSession(loginUserDTO, terminalDTO);
    }

    /**
     * 下线会话
     *
     * @param token 会话凭证(令牌)
     */
    @Override
    public void offlineSession(String token) {
        umsSessionService.offlineSession(token);
    }

    /**
     * 单点验证
     * 入参token,返回单点校验传输对象(包含用户信息)
     *
     * @param token
     * @return
     */
    @Override
    public SsoVerifyDTO verifySSO(String token) {
        if (StringUtil.isBlank(token)) {
            return SsoVerifyDTO.buildFail("token无效");
        }
        // 查询会话
        UmsSession session = umsSessionService.getByToken(token);
        logger.info("[单点验证] 根据token={}查询会话信息={}", token, JsonUtil.toJson(session));
        if (session == null) {
            return SsoVerifyDTO.buildFail("查询会话为空");
        }
        if (EUmsTokenStatusEnum.DISABLE.getCode().equals(session.getTokenStatus())) {
            logger.info("[单点验证] 会话为失效状态,token={}", token);
            return SsoVerifyDTO.buildFail("会话已失效");
        }
        // 查询用户信息
        UmsUser user = umsUserService.get(session.getUserId());
        logger.info("[单点验证] token={}对应用户信息={}", token, JsonUtil.toJson(user));
        if (user == null) {
            return SsoVerifyDTO.buildFail("查询用户信息为空");
        }
        return SsoVerifyDTO.buildSuccess(token, user.convert());
    }

    /**
     * 插入登录日志
     *
     * @param loginLog 登录日志
     */
    @Override
    public void insertLoginLog(LoginLogDTO loginLog) {
        if (loginLog == null) {
            return;
        }
        umsLoginLogService.writeLoginErrorLog(loginLog);
    }

    /**
     * 验证uri访问权限
     *
     * @param uriAccessVerifyDTO 校验参数
     * @return
     */
    @Override
    public ApiResult verifyUriAccess(UriAccessVerifyDTO uriAccessVerifyDTO) {
        boolean success = umsAuthService.verifyUri(uriAccessVerifyDTO.getUserId(), uriAccessVerifyDTO.getUri());
        if (success) {
            logger.debug("[验证uri访问权限] 验证通过,用户:{},uri:{}", uriAccessVerifyDTO.getUserId(), uriAccessVerifyDTO.getUri());
            return ApiResult.success();
        } else {
            logger.warn("[验证uri访问权限] 验证未通过,用户:{},uri:{}", uriAccessVerifyDTO.getUserId(), uriAccessVerifyDTO.getUri());
            return ApiResult.error(ECommonResultCode.NOT_AUTH.code, ECommonResultCode.NOT_AUTH.info);
        }
    }


}
