package com.imyuanma.qingyun.ums.dao;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.ums.model.UmsRoleResource;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 角色资源关联表dao
 *
 * @author YuanMaKeJi
 * @date 2023-03-26 11:42:34
 */
@Mapper
public interface IUmsRoleResourceDao {

    /**
     * 列表查询
     *
     * @param umsRoleResource 查询条件
     * @return
     */
    List<UmsRoleResource> getList(UmsRoleResource umsRoleResource);

    /**
     * 分页查询
     *
     * @param umsRoleResource 查询条件
     * @param pageQuery       分页参数
     * @return
     */
    List<UmsRoleResource> getList(UmsRoleResource umsRoleResource, PageQuery pageQuery);

    /**
     * 统计数量
     *
     * @param umsRoleResource 查询条件
     * @return
     */
    int count(UmsRoleResource umsRoleResource);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    UmsRoleResource get(Long id);

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    List<UmsRoleResource> getListByIds(List<Long> list);

    /**
     * 根据roleId查询
     *
     * @param roleId 角色id
     * @return
     */
    List<UmsRoleResource> getListByRoleId(@Param("roleId") Long roleId);

    /**
     * 批量查询角色关联资源
     *
     * @param roleIdList 角色集合
     * @return
     */
    List<Long> getResourceIdListByRoleIdList(@Param("roleIdList") List<Long> roleIdList);

    /**
     * 根据resourceId查询
     *
     * @param resourceId 资源id
     * @return
     */
    List<UmsRoleResource> getListByResourceId(@Param("resourceId") Long resourceId);

    /**
     * 插入
     *
     * @param umsRoleResource 参数
     * @return
     */
    int insert(UmsRoleResource umsRoleResource);

    /**
     * 选择性插入
     *
     * @param umsRoleResource 参数
     * @return
     */
    int insertSelective(UmsRoleResource umsRoleResource);

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    int batchInsert(List<UmsRoleResource> list);

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    int batchInsertSelective(List<UmsRoleResource> list);

    /**
     * 修改
     *
     * @param umsRoleResource 参数
     * @return
     */
    int update(UmsRoleResource umsRoleResource);

    /**
     * 选择性修改
     *
     * @param umsRoleResource 参数
     * @return
     */
    int updateSelective(UmsRoleResource umsRoleResource);

    /**
     * 批量修改某字段
     *
     * @param idList      主键集合
     * @param fieldDbName 待修改的字段名
     * @param fieldValue  修改后的值
     * @return
     */
    int batchUpdateColumn(@Param("idList") List<Long> idList, @Param("fieldDbName") String fieldDbName, @Param("fieldValue") Object fieldValue);

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    int delete(Long id);

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    int batchDelete(List<Long> list);

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param umsRoleResource 参数
     * @return
     */
    int deleteByCondition(UmsRoleResource umsRoleResource);

    /**
     * 删除全部
     *
     * @return
     */
    int deleteAll();

    /**
     * 根据角色删除
     *
     * @param roleId 角色id
     * @return
     */
    int deleteByRoleId(Long roleId);

}
