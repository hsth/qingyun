package com.imyuanma.qingyun.ums.model;

import lombok.Data;
import java.util.Date;
import com.imyuanma.qingyun.interfaces.common.model.DbSortDO;

/**
 * 登录日志实体类
 *
 * @author YuanMaKeJi
 * @date 2023-04-08 15:16:30
 */
@Data
public class UmsLoginLog extends DbSortDO {

    /**
     * 主键
     */
    private Long id;

    /**
     * 登录账号
     */
    private String account;

    /**
     * 登录结果,10成功,20失败
     */
    private Integer result;

    /**
     * 登录时间
     */
    private Date loginTime;
    /**
     * 登录时间,按时间检索时作为结束时间使用
     */
    private Date loginTime2;

    /**
     * 客户端ip
     */
    private String clientIp;

    /**
     * 服务端ip
     */
    private String serverIp;

    /**
     * 备注
     */
    private String remark;

    /**
     * 设备类型
     */
    private Integer deviceType;



}