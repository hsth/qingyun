package com.imyuanma.qingyun.ums.model.param;

import com.imyuanma.qingyun.interfaces.common.model.BaseDO;
import com.imyuanma.qingyun.ums.model.UmsResource;
import lombok.Data;

import java.util.List;

/**
 * 批量新增资源入参
 *
 * @author wangjy
 * @date 2023/06/04 14:09:09
 */
@Data
public class BatchAddResourceParam extends BaseDO {
    /**
     * 上级资源id
     */
    private Long parentId;
    /**
     * 待创建资源集合
     */
    private List<UmsResource> resourceList;

}
