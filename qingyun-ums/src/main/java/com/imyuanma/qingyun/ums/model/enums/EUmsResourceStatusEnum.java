package com.imyuanma.qingyun.ums.model.enums;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 资源状态枚举
 *
 * @author YuanMaKeJi
 * @date 2022-10-05 22:53:20
 */
public enum EUmsResourceStatusEnum {
    VALID(10, "有效"),
    INVALID(20, "无效"),
    ;

    /**
     * code
     */
    private Integer code;
    /**
     * 文案
     */
    private String text;

    EUmsResourceStatusEnum(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    public static List<Integer> codes() {
        return Arrays.stream(values()).map(EUmsResourceStatusEnum::getCode).collect(Collectors.toList());
    }

    /**
     * 根据code获取枚举
     *
     * @param code code值
     */
    public static EUmsResourceStatusEnum getByCode(Integer code) {
        for (EUmsResourceStatusEnum e : EUmsResourceStatusEnum.values()) {
            if (e.code != null && e.code.equals(code)) {
                return e;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
