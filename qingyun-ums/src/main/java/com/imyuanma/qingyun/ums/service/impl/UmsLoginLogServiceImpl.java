package com.imyuanma.qingyun.ums.service.impl;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.util.IPUtil;
import com.imyuanma.qingyun.common.util.StringUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.interfaces.ums.model.LoginLogDTO;
import com.imyuanma.qingyun.interfaces.ums.model.LoginUserDTO;
import com.imyuanma.qingyun.interfaces.ums.model.TerminalDTO;
import com.imyuanma.qingyun.interfaces.ums.model.enums.EUmsLoginDeviceTypeEnum;
import com.imyuanma.qingyun.ums.dao.IUmsLoginLogDao;
import com.imyuanma.qingyun.ums.model.UmsLoginLog;
import com.imyuanma.qingyun.ums.model.enums.EUmsLoginResultEnum;
import com.imyuanma.qingyun.ums.service.IUmsLoginLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 登录日志服务
 *
 * @author YuanMaKeJi
 * @date 2023-04-08 15:16:30
 */
@Slf4j
@Service
public class UmsLoginLogServiceImpl implements IUmsLoginLogService {

    /**
     * 登录日志dao
     */
    @Autowired
    private IUmsLoginLogDao umsLoginLogDao;

    /**
     * 列表查询
     *
     * @param umsLoginLog 查询条件
     * @return
     */
    @Trace("查询登录日志列表")
    @Override
    public List<UmsLoginLog> getList(UmsLoginLog umsLoginLog) {
        return umsLoginLogDao.getList(umsLoginLog);
    }

    /**
     * 分页查询
     *
     * @param umsLoginLog 查询条件
     * @param pageQuery   分页参数
     * @return
     */
    @Trace("分页查询登录日志")
    @Override
    public List<UmsLoginLog> getPage(UmsLoginLog umsLoginLog, PageQuery pageQuery) {
        return umsLoginLogDao.getList(umsLoginLog, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param umsLoginLog 查询条件
     * @return
     */
    @Trace("统计登录日志数量")
    @Override
    public int count(UmsLoginLog umsLoginLog) {
        return umsLoginLogDao.count(umsLoginLog);
    }

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    @Trace("根据主键查询登录日志")
    @Override
    public UmsLoginLog get(Long id) {
        return umsLoginLogDao.get(id);
    }

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    @Trace("根据主键批量查询登录日志")
    @Override
    public List<UmsLoginLog> getListByIds(List<Long> list) {
        return umsLoginLogDao.getListByIds(list);
    }

    /**
     * 根据account查询
     *
     * @param account 登录账号
     * @return
     */
    @Trace("根据account查询登录日志")
    @Override
    public List<UmsLoginLog> getListByAccount(String account) {
        return umsLoginLogDao.getListByAccount(account);
    }

    /**
     * 根据result查询
     *
     * @param result 登录结果,10成功,20失败
     * @return
     */
    @Trace("根据result查询登录日志")
    @Override
    public List<UmsLoginLog> getListByResult(Integer result) {
        return umsLoginLogDao.getListByResult(result);
    }

    /**
     * 根据clientIp查询
     *
     * @param clientIp 客户端ip
     * @return
     */
    @Trace("根据clientIp查询登录日志")
    @Override
    public List<UmsLoginLog> getListByClientIp(String clientIp) {
        return umsLoginLogDao.getListByClientIp(clientIp);
    }

    /**
     * 根据serverIp查询
     *
     * @param serverIp 服务端ip
     * @return
     */
    @Trace("根据serverIp查询登录日志")
    @Override
    public List<UmsLoginLog> getListByServerIp(String serverIp) {
        return umsLoginLogDao.getListByServerIp(serverIp);
    }

    /**
     * 根据deviceType查询
     *
     * @param deviceType 设备类型
     * @return
     */
    @Trace("根据deviceType查询登录日志")
    @Override
    public List<UmsLoginLog> getListByDeviceType(Integer deviceType) {
        return umsLoginLogDao.getListByDeviceType(deviceType);
    }

    /**
     * 插入
     *
     * @param umsLoginLog 参数
     * @return
     */
    @Trace("插入登录日志")
    @Override
    public int insert(UmsLoginLog umsLoginLog) {
        return umsLoginLogDao.insert(umsLoginLog);
    }

    /**
     * 选择性插入
     *
     * @param umsLoginLog 参数
     * @return
     */
    @Trace("选择性插入登录日志")
    @Override
    public int insertSelective(UmsLoginLog umsLoginLog) {
        return umsLoginLogDao.insertSelective(umsLoginLog);
    }

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量插入登录日志")
    @Override
    public int batchInsert(List<UmsLoginLog> list) {
        return umsLoginLogDao.batchInsert(list);
    }

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量选择性插入登录日志")
    @Override
    public int batchInsertSelective(List<UmsLoginLog> list) {
        return umsLoginLogDao.batchInsertSelective(list);
    }

    /**
     * 修改
     *
     * @param umsLoginLog 参数
     * @return
     */
    @Trace("修改登录日志")
    @Override
    public int update(UmsLoginLog umsLoginLog) {
        return umsLoginLogDao.update(umsLoginLog);
    }

    /**
     * 选择性修改
     *
     * @param umsLoginLog 参数
     * @return
     */
    @Trace("选择性修改登录日志")
    @Override
    public int updateSelective(UmsLoginLog umsLoginLog) {
        return umsLoginLogDao.updateSelective(umsLoginLog);
    }

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    @Trace("删除登录日志")
    @Override
    public int delete(Long id) {
        return umsLoginLogDao.delete(id);
    }

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    @Trace("批量删除登录日志")
    @Override
    public int batchDelete(List<Long> list) {
        return umsLoginLogDao.batchDelete(list);
    }

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param umsLoginLog 参数
     * @return
     */
    @Trace("根据条件删除登录日志")
    @Override
    public int deleteByCondition(UmsLoginLog umsLoginLog) {
        return umsLoginLogDao.deleteByCondition(umsLoginLog);
    }

    /**
     * 写入登录成功日志
     *
     * @param loginUserDTO 登录用户
     * @param terminalDTO  登录终端
     * @return 日志对象
     */
    @Trace("写入登录成功日志")
    @Override
    public UmsLoginLog writeLoginSuccessLog(LoginUserDTO loginUserDTO, TerminalDTO terminalDTO) {
        UmsLoginLog loginLog = this.buildLoginSuccessLog(loginUserDTO, terminalDTO);
        this.insertSelective(loginLog);
        return loginLog;
    }

    /**
     * 写入登录失败日志
     *
     * @param loginLogDTO 日志对象
     * @return 日志对象
     */
    @Trace("写入登录失败日志")
    @Override
    public UmsLoginLog writeLoginErrorLog(LoginLogDTO loginLogDTO) {
        UmsLoginLog umsLoginLog = this.buildUmsLoginLog(loginLogDTO);
        this.insertSelective(umsLoginLog);
        return umsLoginLog;
    }

    /**
     * 构造登录日志对象
     *
     * @param loginLog
     * @return
     */
    private UmsLoginLog buildUmsLoginLog(LoginLogDTO loginLog) {
        UmsLoginLog umsLoginLog = new UmsLoginLog();
        umsLoginLog.setAccount(loginLog.getAccount());
        umsLoginLog.setResult(Boolean.TRUE.equals(loginLog.getLoginSuccess()) ? EUmsLoginResultEnum.SUCCESS.getCode() : EUmsLoginResultEnum.FAIL.getCode());
        umsLoginLog.setLoginTime(loginLog.getLoginTime() != null ? loginLog.getLoginTime() : new Date());
        umsLoginLog.setClientIp(StringUtil.getDefaultValue(loginLog.getClientIp(), "0"));
        umsLoginLog.setServerIp(StringUtil.getDefaultValue(loginLog.getServerIp(), "0"));
        umsLoginLog.setRemark(loginLog.getRemark());
        umsLoginLog.setDeviceType(loginLog.getDeviceType() != null ? loginLog.getDeviceType() : EUmsLoginDeviceTypeEnum.OTHER.getCode());
        return umsLoginLog;
    }

    /**
     * 构造登录成功日志
     *
     * @param loginUserDTO
     * @param terminalDTO
     * @return
     */
    private UmsLoginLog buildLoginSuccessLog(LoginUserDTO loginUserDTO, TerminalDTO terminalDTO) {
        UmsLoginLog umsLoginLog = new UmsLoginLog();
        umsLoginLog.setAccount(loginUserDTO.getAccount());
        umsLoginLog.setResult(EUmsLoginResultEnum.SUCCESS.getCode());
        umsLoginLog.setLoginTime(new Date());
        umsLoginLog.setClientIp(StringUtil.getDefaultValue(terminalDTO.getClientIp(), "0"));
        umsLoginLog.setServerIp(StringUtil.getDefaultValue(IPUtil.getLocalIp(), "0"));
        umsLoginLog.setRemark(EUmsLoginResultEnum.SUCCESS.getText());
        umsLoginLog.setDeviceType(terminalDTO.getDeviceType() != null ? terminalDTO.getDeviceType() : EUmsLoginDeviceTypeEnum.OTHER.getCode());
        return umsLoginLog;
    }


}
