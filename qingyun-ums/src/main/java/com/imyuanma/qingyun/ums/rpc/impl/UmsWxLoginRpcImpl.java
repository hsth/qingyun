package com.imyuanma.qingyun.ums.rpc.impl;

import com.imyuanma.qingyun.common.util.HttpUtil;
import com.imyuanma.qingyun.common.util.JsonUtil;
import com.imyuanma.qingyun.common.util.StringUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.ums.configuration.UmsSsoMiniConfiguration;
import com.imyuanma.qingyun.ums.model.wechat.WxCode2SessionResponse;
import com.imyuanma.qingyun.ums.rpc.IUmsWxLoginRpc;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 微信登录
 *
 * @author wangjy
 * @date 2023/09/29 12:04:59
 */
@Slf4j
@Service
public class UmsWxLoginRpcImpl implements IUmsWxLoginRpc {
    /**
     * 登陆验证小程序地址
     */
    private final String code2SessionUri = "https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code";
    /**
     * 小程序登录配置
     */
    @Autowired
    private UmsSsoMiniConfiguration umsSsoMiniConfiguration;

    /**
     * code换取微信用户信息
     *
     * @param code 前端传入的从微信获取的code
     * @return
     */
    @Trace("code换取微信用户信息")
    @Override
    public WxCode2SessionResponse code2Session(String code) {
        String url = String.format(code2SessionUri, umsSsoMiniConfiguration.getAppid(), umsSsoMiniConfiguration.getSecret(), code);
        String res = HttpUtil.post(url, null);
        log.info("[code换取微信用户信息] url={},返回结果={}", url, res);
        if (StringUtil.isBlank(res)) {
            return null;
        }
        return JsonUtil.toBean(res, WxCode2SessionResponse.class);
    }
}
