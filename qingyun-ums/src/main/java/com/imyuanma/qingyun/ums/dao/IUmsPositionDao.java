package com.imyuanma.qingyun.ums.dao;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.ums.model.UmsPosition;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 职位dao
 *
 * @author wangjy
 * @date 2022/05/21 12:26:48
 */
@Mapper
public interface IUmsPositionDao {

    /**
     * 列表查询
     *
     * @param umsPosition 查询条件
     * @return
     */
    List<UmsPosition> getList(UmsPosition umsPosition);

    /**
     * 分页查询
     *
     * @param umsPosition 查询条件
     * @param rowBounds   分页参数
     * @return
     */
    List<UmsPosition> getList(UmsPosition umsPosition, PageQuery rowBounds);

    /**
     * 统计数量
     *
     * @param umsPosition 查询条件
     * @return
     */
    int count(UmsPosition umsPosition);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    UmsPosition get(Long id);

    /**
     * 插入
     *
     * @param umsPosition 参数
     * @return
     */
    int insert(UmsPosition umsPosition);

    /**
     * 选择性插入
     *
     * @param umsPosition 参数
     * @return
     */
    int insertSelective(UmsPosition umsPosition);

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    int batchInsert(List<UmsPosition> list);

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    int batchInsertSelective(List<UmsPosition> list);

    /**
     * 修改
     *
     * @param umsPosition 参数
     * @return
     */
    int update(UmsPosition umsPosition);

    /**
     * 选择性修改
     *
     * @param umsPosition 参数
     * @return
     */
    int updateSelective(UmsPosition umsPosition);

    /**
     * 批量修改某字段
     *
     * @param idList      主键集合
     * @param fieldDbName 待修改的字段名
     * @param fieldValue  修改后的值
     * @return
     */
    int batchUpdateColumn(@Param("idList") List<Long> idList, @Param("fieldDbName") String fieldDbName, @Param("fieldValue") Object fieldValue);

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    int delete(Long id);

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    int batchDelete(List<Long> list);

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param umsPosition 参数
     * @return
     */
    int deleteByCondition(UmsPosition umsPosition);

    /**
     * 删除全部
     *
     * @return
     */
    int deleteAll();

}
