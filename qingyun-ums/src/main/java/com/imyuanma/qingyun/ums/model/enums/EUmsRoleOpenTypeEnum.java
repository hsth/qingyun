package com.imyuanma.qingyun.ums.model.enums;

/**
 * 公开类型枚举
 *
 * @author YuanMaKeJi
 * @date 2023-03-25 13:23:39
 */
public enum EUmsRoleOpenTypeEnum {
    CODE_10(10, "不公开"),
    CODE_20(20, "向直属下级单位公开"),
    CODE_30(30, "向所有下级单位公开"),
    CODE_40(40, "向部分下级单位公开"),
    ;

    /**
     * code
     */
    private Integer code;
    /**
     * 文案
     */
    private String text;

    EUmsRoleOpenTypeEnum(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    /**
     * 根据code获取枚举
     *
     * @param code code值
     */
    public static EUmsRoleOpenTypeEnum getByCode(Integer code) {
        for (EUmsRoleOpenTypeEnum e : EUmsRoleOpenTypeEnum.values()) {
            if (e.code != null && e.code.equals(code)) {
                return e;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
