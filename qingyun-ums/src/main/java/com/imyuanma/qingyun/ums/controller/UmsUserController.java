package com.imyuanma.qingyun.ums.controller;

import com.imyuanma.qingyun.common.client.ums.LoginUserHolder;
import com.imyuanma.qingyun.common.factory.BaseDOBuilder;
import com.imyuanma.qingyun.common.model.request.WebRequest;
import com.imyuanma.qingyun.common.model.response.Page;
import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.interfaces.ums.model.LoginUserDTO;
import com.imyuanma.qingyun.ums.model.UmsUpdatePassword;
import com.imyuanma.qingyun.ums.model.UmsUser;
import com.imyuanma.qingyun.ums.model.enums.EUmsUserStatusEnum;
import com.imyuanma.qingyun.ums.service.IUmsUserService;
import com.imyuanma.qingyun.ums.util.UmsConstants;
import com.imyuanma.qingyun.ums.util.UmsUserBusinessUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 用户信息web
 *
 * @author YuanMaKeJi
 * @date 2022-07-09 16:23:45
 */
@RestController
@RequestMapping(value = "/ums/umsUser")
public class UmsUserController {

    /**
     * 用户信息服务
     */
    @Autowired
    private IUmsUserService umsUserService;

    /**
     * 列表查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("查询用户列表")
    @PostMapping("/getList")
    public Result<List<UmsUser>> getList(@RequestBody WebRequest<UmsUser> webRequest) {
        List<UmsUser> list = umsUserService.getList(webRequest.getBody());
        UmsUserBusinessUtil.clearPassword(list);
        return Result.success(list);
    }

    /**
     * 分页查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("分页查询用户")
    @PostMapping("/getPage")
    public Page<List<UmsUser>> getPage(@RequestBody WebRequest<UmsUser> webRequest) {
        PageQuery pageQuery = webRequest.buildPageQuery();
        List<UmsUser> list = umsUserService.getPage(webRequest.getBody(), pageQuery);
        UmsUserBusinessUtil.clearPassword(list);
        return Page.success(list, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("统计用户数量")
    @PostMapping("/count")
    public Result<Integer> count(@RequestBody WebRequest<UmsUser> webRequest) {
        return Result.success(umsUserService.count(webRequest.getBody()));
    }

    /**
     * 查询
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("查询用户")
    @PostMapping("/get")
    public Result<UmsUser> get(@RequestBody WebRequest<Long> webRequest) {
        AssertUtil.notNull(webRequest.getBody());
        UmsUser user = umsUserService.get(webRequest.getBody());
        UmsUserBusinessUtil.clearPassword(user);
        return Result.success(user);
    }

    /**
     * 新增
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("新增用户")
    @PostMapping("/insert")
    public Result insert(@RequestBody WebRequest<UmsUser> webRequest) {
        UmsUser umsUser = webRequest.getBody();
        AssertUtil.notNull(umsUser);
        BaseDOBuilder.fillBaseDOForInsert(umsUser);
        umsUserService.insertSelective(umsUser);
        return Result.success();
    }

    /**
     * 修改
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("修改用户")
    @PostMapping("/update")
    public Result update(@RequestBody WebRequest<UmsUser> webRequest) {
        UmsUser umsUser = webRequest.getBody();
        AssertUtil.notNull(umsUser);
        AssertUtil.notNull(umsUser.getId());
        BaseDOBuilder.fillBaseDOForUpdate(umsUser);
        umsUserService.updateSelective(umsUser);
        return Result.success();
    }


    /**
     * 更新用户状态
     * @param webRequest
     * @return
     */
    @Trace("更新用户状态")
    @PostMapping("/updateUserStatus")
    public Result updateUserStatus(@RequestBody WebRequest<UmsUser> webRequest) {
        UmsUser umsUser = webRequest.getBody();
        AssertUtil.notNull(umsUser);
        AssertUtil.notNull(umsUser.getId());
        AssertUtil.notNull(umsUser.getStatus());
        AssertUtil.inEnums(umsUser.getStatus(), EUmsUserStatusEnum.codes(), "用户状态不合法!");
        AssertUtil.isFalse(umsUser.getId().equals(LoginUserHolder.getLoginUserId()), "不支持修改自身状态!");
        AssertUtil.isFalse(UmsConstants.ROOT_USER_ID.equals(umsUser.getId()), "不支持超管状态修改!");
        // 更新对象, 避免前端传入其他字段导致误修改
        UmsUser update = new UmsUser();
        update.setId(umsUser.getId());
        update.setStatus(umsUser.getStatus());
        BaseDOBuilder.fillBaseDOForUpdate(update);
        umsUserService.updateSelective(update);
        return Result.success();
    }

    /**
     * 删除
     *
     * @param webRequest 参数, 待删除主键,多个使用英文逗号拼接
     * @return
     */
    @Trace("删除用户")
    @PostMapping("/delete")
    public Result delete(@RequestBody WebRequest<String> webRequest) {
        String ids = webRequest.getBody();
        AssertUtil.notBlank(ids);
        if (ids.contains(",")) {
            umsUserService.batchDelete(Arrays.stream(ids.split(",")).map(Long::valueOf).collect(Collectors.toList()));
        } else {
            umsUserService.delete(Long.valueOf(ids));
        }
        return Result.success();
    }

    /**
     * 用户更新密码
     * @param webRequest
     * @return
     */
    @Trace("更新登录密码")
    @PostMapping("/updatePassword")
    public Result updatePassword(@RequestBody WebRequest<UmsUpdatePassword> webRequest) {
        UmsUpdatePassword updatePassword = webRequest.getBody();
        AssertUtil.notNull(updatePassword);
        AssertUtil.notBlank(updatePassword.getOldPwd());
        AssertUtil.notBlank(updatePassword.getNewPwd());
        updatePassword.setUserId(LoginUserHolder.getLoginUser().getUserId());
        umsUserService.updatePassword(updatePassword);
        return Result.success();
    }

    /**
     * 更新头像
     * @param webRequest
     * @return
     */
    @Trace("更新头像")
    @PostMapping("/updateHeader")
    public Result updateHeader(@RequestBody WebRequest<String> webRequest) {
        AssertUtil.notNull(webRequest);
        AssertUtil.notBlank(webRequest.getBody());
        UmsUser user = new UmsUser();
        user.setId(LoginUserHolder.getLoginUser().getUserId());
        user.setHeader(webRequest.getBody());
        umsUserService.updateSelective(user);
        return Result.success();
    }

    @Trace("管理员修改用户密码")
    @PostMapping("/updatePasswordForAdmin")
    public Result updatePasswordForAdmin(@RequestBody WebRequest<UmsUpdatePassword> webRequest) {
        UmsUpdatePassword updatePassword = webRequest.getBody();
        AssertUtil.notNull(updatePassword);
        AssertUtil.notNull(updatePassword.getUserId());
        AssertUtil.notBlank(updatePassword.getNewPwd());
        umsUserService.updatePasswordForAdmin(updatePassword);
        return Result.success();
    }

    @Trace("管理员重置用户密码")
    @PostMapping("/resetPasswordForAdmin")
    public Result resetPasswordForAdmin(@RequestBody WebRequest<Long[]> webRequest) {
        AssertUtil.notNull(webRequest);
        AssertUtil.notEmpty(webRequest.getBody(), "待重置密码的用户ID为空");
        umsUserService.resetPasswordForAdmin(webRequest.getBody());
        return Result.success();
    }

}
