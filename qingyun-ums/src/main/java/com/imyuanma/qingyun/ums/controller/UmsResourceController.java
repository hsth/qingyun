package com.imyuanma.qingyun.ums.controller;

import com.imyuanma.qingyun.common.core.excel.ExcelExport;
import com.imyuanma.qingyun.common.core.excel.ExcelImport;
import com.imyuanma.qingyun.common.core.excel.IExcelExport;
import com.imyuanma.qingyun.common.core.excel.IExcelImport;
import com.imyuanma.qingyun.common.core.structure.tree.TreeBuilder;
import com.imyuanma.qingyun.common.factory.BaseDOBuilder;
import com.imyuanma.qingyun.common.model.request.WebRequest;
import com.imyuanma.qingyun.common.model.response.Page;
import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.common.util.StringUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.ums.model.UmsResource;
import com.imyuanma.qingyun.ums.model.enums.EUmsResourceStatusEnum;
import com.imyuanma.qingyun.ums.model.enums.EUmsUserStatusEnum;
import com.imyuanma.qingyun.ums.model.param.BatchAddResourceParam;
import com.imyuanma.qingyun.ums.service.IUmsResourceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 资源web
 *
 * @author YuanMaKeJi
 * @date 2022-10-08 11:59:54
 */
@RestController
@RequestMapping(value = "/ums/umsResource")
public class UmsResourceController {

    private static final Logger logger = LoggerFactory.getLogger(UmsResourceController.class);

    /**
     * 资源服务
     */
    @Autowired
    private IUmsResourceService umsResourceService;


    @Trace("查询资源树")
    @PostMapping("/getTree")
    public Result<List<UmsResource>> getTree(@RequestBody WebRequest<UmsResource> webRequest) {
        return Result.success(umsResourceService.getTree(webRequest.getBody()));
    }

    @Trace("查询父资源节点选择树")
    @PostMapping("/getParentSelectedTree")
    public Result<List<UmsResource>> getParentSelectedTree(@RequestBody WebRequest<UmsResource> webRequest) {
        // 查询列表
        List<UmsResource> list = umsResourceService.getList(webRequest.getBody());
        if (list == null) {
            list = new ArrayList<>();
        }
        list.add(UmsResource.buildVirtualRoot());
        if (CollectionUtil.isNotEmpty(list)) {
            Collections.sort(list);
        }
        // 树构造
        return Result.success(TreeBuilder.buildTree(list));
    }

    /**
     * 列表查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("查询资源列表")
    @PostMapping("/getList")
    public Result<List<UmsResource>> getList(@RequestBody WebRequest<UmsResource> webRequest) {
        return Result.success(umsResourceService.getList(webRequest.getBody()));
    }

    /**
     * 分页查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("分页查询资源")
    @PostMapping("/getPage")
    public Page<List<UmsResource>> getPage(@RequestBody WebRequest<UmsResource> webRequest) {
        PageQuery pageQuery = webRequest.buildPageQuery();
        List<UmsResource> list = umsResourceService.getPage(webRequest.getBody(), pageQuery);
        return Page.success(list, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("统计资源数量")
    @PostMapping("/count")
    public Result<Integer> count(@RequestBody WebRequest<UmsResource> webRequest) {
        return Result.success(umsResourceService.count(webRequest.getBody()));
    }

    /**
     * 查询
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("查询资源")
    @PostMapping("/get")
    public Result<UmsResource> get(@RequestBody WebRequest<Long> webRequest) {
        AssertUtil.notNull(webRequest.getBody());
        return Result.success(umsResourceService.get(webRequest.getBody()));
    }

    /**
     * 新增
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("新增资源")
    @PostMapping("/insert")
    public Result insert(@RequestBody WebRequest<UmsResource> webRequest) {
        UmsResource umsResource = webRequest.getBody();
        AssertUtil.notNull(umsResource);
        BaseDOBuilder.fillBaseDOForInsert(umsResource);
        umsResourceService.insertSelective(umsResource);
        return Result.success();
    }

    @Trace("批量创建资源")
    @PostMapping("/batchAddResource")
    public Result batchAddResource(@RequestBody WebRequest<BatchAddResourceParam> webRequest) {
        BatchAddResourceParam batchAddResourceParam = webRequest.getBody();
        AssertUtil.notNull(batchAddResourceParam);
        AssertUtil.notNull(batchAddResourceParam.getParentId(), "上级不允许为空");
        AssertUtil.notEmpty(batchAddResourceParam.getResourceList(), "资源列表不允许为空");
        BaseDOBuilder.fillBaseDOForInsert(batchAddResourceParam);
        umsResourceService.batchAddResource(batchAddResourceParam);
        return Result.success();
    }

    /**
     * 修改
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("修改资源")
    @PostMapping("/update")
    public Result update(@RequestBody WebRequest<UmsResource> webRequest) {
        UmsResource umsResource = webRequest.getBody();
        AssertUtil.notNull(umsResource);
        AssertUtil.notNull(umsResource.getId());
        BaseDOBuilder.fillBaseDOForUpdate(umsResource);
        umsResourceService.updateSelective(umsResource);
        return Result.success();
    }

    @Trace("更新资源状态")
    @PostMapping("/updateStatus")
    public Result updateStatus(@RequestBody WebRequest<UmsResource> webRequest) {
        UmsResource umsResource = webRequest.getBody();
        AssertUtil.notNull(umsResource);
        AssertUtil.notNull(umsResource.getId());
        AssertUtil.notNull(umsResource.getStatus());
        AssertUtil.inEnums(umsResource.getStatus(), EUmsResourceStatusEnum.codes(), "状态不合法!");

        UmsResource update = new UmsResource();
        update.setId(umsResource.getId());
        update.setStatus(umsResource.getStatus());
        BaseDOBuilder.fillBaseDOForUpdate(update);
        umsResourceService.updateSelective(update);
        return Result.success();
    }

    /**
     * 删除
     *
     * @param webRequest 参数, 待删除主键,多个使用英文逗号拼接
     * @return
     */
    @Trace("删除资源")
    @PostMapping("/delete")
    public Result delete(@RequestBody WebRequest<String> webRequest) {
        String ids = webRequest.getBody();
        AssertUtil.notBlank(ids);
        if (ids.contains(",")) {
            umsResourceService.batchDelete(Arrays.stream(ids.split(",")).map(Long::valueOf).collect(Collectors.toList()));
        } else {
            umsResourceService.delete(Long.valueOf(ids));
        }
        return Result.success();
    }

    /**
     * 导入
     *
     * @param file
     * @return
     */
    @Trace("导入资源")
    @PostMapping("/import")
    public Result importExcel(MultipartFile file) {
        try {
            if (file != null && !file.isEmpty()) {
                //excel导入处理,返回excel中的数据集合
                IExcelImport ei = new ExcelImport(file);//将文件转为ExcelImport对象
                //从excel读取数据
                List<UmsResource> list = ei.getImportDataAsBean(UmsResource.class);
                if (list != null && list.size() > 0) {
                    for (UmsResource item : list) {
                        BaseDOBuilder.fillBaseDOForInsert(item);
                    }
                    // 补全上级id
                    umsResourceService.fillParentIdByParentCode(list);
                    int num = umsResourceService.batchInsertSelective(list);//批量插入
                    return Result.success("成功导入数据" + num + "条!");
                } else {
                    return Result.error("导入失败:excel解析后结果为空!");
                }
            } else {
                return Result.error("导入失败:文件为空!");
            }
        } catch (Throwable e) {
            logger.error("[导入资源] 导入发生异常", e);
            return Result.error("导入失败!");
        }
    }

    /**
     * 下载excel导入模板
     *
     * @param response
     * @throws Exception
     */
    @Trace("下载资源excel导入模板")
    @GetMapping("/templateDownload")
    public void templateDownload(HttpServletResponse response) throws Exception {
        IExcelExport ee = new ExcelExport();
        ee.insertBeanList(new ArrayList(), UmsResource.class);
        ee.write2Response(response, "template.xls");
    }

    /**
     * 导出
     *
     * @param obj      查询参数
     * @param response
     * @throws Exception
     */
    @Trace("导出资源")
    @GetMapping("/export")
    public void exportExcel(UmsResource obj, String ids, HttpServletResponse response) throws Exception {
        List<UmsResource> list = StringUtil.isNotBlank(ids)
                ? umsResourceService.getListByIds(Arrays.stream(ids.split(",")).map(Long::valueOf).collect(Collectors.toList()))
                : umsResourceService.getList(obj);
        if (list != null && list.size() > 0) {
            // 补全上级code
            umsResourceService.fillParentCodeByParentId(list);
            IExcelExport ee = new ExcelExport();
            ee.insertBeanList(list, UmsResource.class);
            ee.write2Response(response, "excel_" + System.currentTimeMillis() + ".xls");
        } else {
            response.getWriter().write("数据为空!");
        }
    }

}
