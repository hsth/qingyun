package com.imyuanma.qingyun.ums.service;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.interfaces.ums.model.LoginLogDTO;
import com.imyuanma.qingyun.interfaces.ums.model.LoginUserDTO;
import com.imyuanma.qingyun.interfaces.ums.model.TerminalDTO;
import com.imyuanma.qingyun.ums.model.UmsLoginLog;

import java.util.List;

/**
 * 登录日志服务
 *
 * @author YuanMaKeJi
 * @date 2023-04-08 15:16:30
 */
public interface IUmsLoginLogService {

    /**
     * 列表查询
     *
     * @param umsLoginLog 查询条件
     * @return
     */
    List<UmsLoginLog> getList(UmsLoginLog umsLoginLog);

    /**
     * 分页查询
     *
     * @param umsLoginLog 查询条件
     * @param pageQuery   分页参数
     * @return
     */
    List<UmsLoginLog> getPage(UmsLoginLog umsLoginLog, PageQuery pageQuery);

    /**
     * 统计数量
     *
     * @param umsLoginLog 查询条件
     * @return
     */
    int count(UmsLoginLog umsLoginLog);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    UmsLoginLog get(Long id);

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    List<UmsLoginLog> getListByIds(List<Long> list);

    /**
     * 根据account查询
     *
     * @param account 登录账号
     * @return
     */
    List<UmsLoginLog> getListByAccount(String account);

    /**
     * 根据result查询
     *
     * @param result 登录结果,10成功,20失败
     * @return
     */
    List<UmsLoginLog> getListByResult(Integer result);

    /**
     * 根据clientIp查询
     *
     * @param clientIp 客户端ip
     * @return
     */
    List<UmsLoginLog> getListByClientIp(String clientIp);

    /**
     * 根据serverIp查询
     *
     * @param serverIp 服务端ip
     * @return
     */
    List<UmsLoginLog> getListByServerIp(String serverIp);

    /**
     * 根据deviceType查询
     *
     * @param deviceType 设备类型
     * @return
     */
    List<UmsLoginLog> getListByDeviceType(Integer deviceType);

    /**
     * 插入
     *
     * @param umsLoginLog 参数
     * @return
     */
    int insert(UmsLoginLog umsLoginLog);

    /**
     * 选择性插入
     *
     * @param umsLoginLog 参数
     * @return
     */
    int insertSelective(UmsLoginLog umsLoginLog);

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    int batchInsert(List<UmsLoginLog> list);

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    int batchInsertSelective(List<UmsLoginLog> list);

    /**
     * 修改
     *
     * @param umsLoginLog 参数
     * @return
     */
    int update(UmsLoginLog umsLoginLog);

    /**
     * 选择性修改
     *
     * @param umsLoginLog 参数
     * @return
     */
    int updateSelective(UmsLoginLog umsLoginLog);

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    int delete(Long id);

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    int batchDelete(List<Long> list);

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param umsLoginLog 参数
     * @return
     */
    int deleteByCondition(UmsLoginLog umsLoginLog);

    /**
     * 写入登录成功日志
     *
     * @param loginUserDTO 登录用户
     * @param terminalDTO  登录终端
     * @return 日志对象
     */
    UmsLoginLog writeLoginSuccessLog(LoginUserDTO loginUserDTO, TerminalDTO terminalDTO);

    /**
     * 写入登录失败日志
     *
     * @param loginLogDTO 日志对象
     * @return 日志对象
     */
    UmsLoginLog writeLoginErrorLog(LoginLogDTO loginLogDTO);

}
