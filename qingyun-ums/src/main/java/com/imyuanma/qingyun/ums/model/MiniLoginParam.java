package com.imyuanma.qingyun.ums.model;

import com.imyuanma.qingyun.interfaces.common.model.dto.ClientInfoDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 小程序登录参数
 *
 * @author wangjy
 * @date 2023/09/24 18:10:48
 */
@Data
public class MiniLoginParam {
    /**
     * 小程序登录时颁发的临时code
     */
    @ApiModelProperty(value = "微信登录code", required = true, position = 10)
    private String code;
    @ApiModelProperty(value = "昵称", required = false, position = 20)
    private String nickName;
    @ApiModelProperty(value = "头像", required = false, position = 30)
    private String avatar;
    @ApiModelProperty(value = "性别0：未知、10：男、20：女", required = false, position = 40)
    private Integer sex;

    /**
     * 客户端信息
     */
    @ApiModelProperty(value = "客户端信息", required = false, position = 50)
    private ClientInfoDTO clientInfo;

}
