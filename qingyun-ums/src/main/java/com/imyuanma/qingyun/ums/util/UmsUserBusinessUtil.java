package com.imyuanma.qingyun.ums.util;

import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.ums.model.UmsUser;

import java.util.List;

/**
 * 用户业务工具
 *
 * @author wangjy
 * @date 2023/04/08 11:32:57
 */
public class UmsUserBusinessUtil {
    /**
     * 清空密码字段
     *
     * @param list
     */
    public static void clearPassword(List<UmsUser> list) {
        if (CollectionUtil.isNotEmpty(list)) {
            for (UmsUser user : list) {
                clearPassword(user);
            }
        }
    }

    /**
     * 清空密码字段
     *
     * @param user
     */
    public static void clearPassword(UmsUser user) {
        if (user != null) {
            user.setPassword(null);
        }
    }
}
