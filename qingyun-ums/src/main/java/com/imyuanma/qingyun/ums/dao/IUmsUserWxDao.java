package com.imyuanma.qingyun.ums.dao;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.ums.model.UmsUserWx;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户微信信息dao
 *
 * @author YuanMaKeJi
 * @date 2023-11-05 23:03:28
 */
@Mapper
public interface IUmsUserWxDao {

    /**
     * 列表查询
     *
     * @param umsUserWx 查询条件
     * @return
     */
    @Trace("查询用户微信信息列表")
    List<UmsUserWx> getList(UmsUserWx umsUserWx);

    /**
     * 分页查询
     *
     * @param umsUserWx 查询条件
     * @param pageQuery 分页参数
     * @return
     */
    @Trace("分页查询用户微信信息")
    List<UmsUserWx> getList(UmsUserWx umsUserWx, PageQuery pageQuery);

    /**
     * 统计数量
     *
     * @param umsUserWx 查询条件
     * @return
     */
    @Trace("统计用户微信信息数量")
    int count(UmsUserWx umsUserWx);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    @Trace("主键查询用户微信信息")
    UmsUserWx get(Long id);

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    @Trace("主键批量查询用户微信信息")
    List<UmsUserWx> getListByIds(List<Long> list);

    /**
     * 根据userId查询
     *
     * @param userId 用户ID
     * @return
     */
    @Trace("根据userId查询用户微信信息")
    UmsUserWx getByUserId(@Param("userId") Long userId);

    /**
     * 根据unionId查询
     *
     * @param unionId 微信unionid
     * @return
     */
    @Trace("根据unionId查询用户微信信息")
    UmsUserWx getByUnionId(@Param("unionId") String unionId);

    /**
     * 根据openId查询
     *
     * @param openId 微信openid
     * @return
     */
    @Trace("根据openId查询用户微信信息")
    UmsUserWx getByOpenId(@Param("openId") String openId);

    /**
     * 根据sessionKey查询
     *
     * @param sessionKey 会话key
     * @return
     */
    @Trace("根据sessionKey查询用户微信信息")
    UmsUserWx getBySessionKey(@Param("sessionKey") String sessionKey);

    /**
     * 插入
     *
     * @param umsUserWx 参数
     * @return
     */
    @Trace("插入用户微信信息")
    int insert(UmsUserWx umsUserWx);

    /**
     * 选择性插入
     *
     * @param umsUserWx 参数
     * @return
     */
    @Trace("选择性插入用户微信信息")
    int insertSelective(UmsUserWx umsUserWx);

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量插入用户微信信息")
    int batchInsert(List<UmsUserWx> list);

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量选择性插入用户微信信息")
    int batchInsertSelective(List<UmsUserWx> list);

    /**
     * 修改
     *
     * @param umsUserWx 参数
     * @return
     */
    @Trace("修改用户微信信息")
    int update(UmsUserWx umsUserWx);

    /**
     * 选择性修改
     *
     * @param umsUserWx 参数
     * @return
     */
    @Trace("选择性修改用户微信信息")
    int updateSelective(UmsUserWx umsUserWx);

    /**
     * 批量修改某字段
     *
     * @param idList      主键集合
     * @param fieldDbName 待修改的字段名
     * @param fieldValue  修改后的值
     * @return
     */
    @Trace("批量修改用户微信信息属性")
    int batchUpdateColumn(@Param("idList") List<Long> idList, @Param("fieldDbName") String fieldDbName, @Param("fieldValue") Object fieldValue);

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    @Trace("删除用户微信信息")
    int delete(Long id);

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    @Trace("批量删除用户微信信息")
    int batchDelete(List<Long> list);

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param umsUserWx 参数
     * @return
     */
    @Trace("条件删除用户微信信息")
    int deleteByCondition(UmsUserWx umsUserWx);

    /**
     * 删除全部
     *
     * @return
     */
    @Trace("全量删除用户微信信息")
    int deleteAll();

}
