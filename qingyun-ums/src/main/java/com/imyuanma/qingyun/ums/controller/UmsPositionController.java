package com.imyuanma.qingyun.ums.controller;

import com.imyuanma.qingyun.common.core.excel.ExcelExport;
import com.imyuanma.qingyun.common.core.excel.ExcelImport;
import com.imyuanma.qingyun.common.core.excel.IExcelExport;
import com.imyuanma.qingyun.common.core.excel.IExcelImport;
import com.imyuanma.qingyun.common.factory.BaseDOBuilder;
import com.imyuanma.qingyun.common.model.request.WebRequest;
import com.imyuanma.qingyun.common.model.response.Page;
import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.ums.model.UmsPosition;
import com.imyuanma.qingyun.ums.service.IUmsPositionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 岗位表web
 *
 * @author YuanMaKeJi
 * @date 2023-01-15 19:06:57
 */
@RestController
@RequestMapping(value = "/ums/umsPosition")
public class UmsPositionController {
    private static final Logger logger = LoggerFactory.getLogger(UmsPositionController.class);

    /**
     * 岗位表服务
     */
    @Autowired
    private IUmsPositionService umsPositionService;

    /**
     * 列表查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("查询岗位表列表")
    @PostMapping("/getList")
    public Result<List<UmsPosition>> getList(@RequestBody WebRequest<UmsPosition> webRequest) {
        return Result.success(umsPositionService.getList(webRequest.getBody()));
    }

    /**
     * 分页查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("分页查询岗位表")
    @PostMapping("/getPage")
    public Page<List<UmsPosition>> getPage(@RequestBody WebRequest<UmsPosition> webRequest) {
        PageQuery pageQuery = webRequest.buildPageQuery();
        List<UmsPosition> list = umsPositionService.getPage(webRequest.getBody(), pageQuery);
        return Page.success(list, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("统计岗位表数量")
    @PostMapping("/count")
    public Result<Integer> count(@RequestBody WebRequest<UmsPosition> webRequest) {
        return Result.success(umsPositionService.count(webRequest.getBody()));
    }

    /**
     * 查询
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("查询岗位表")
    @PostMapping("/get")
    public Result<UmsPosition> get(@RequestBody WebRequest<Long> webRequest) {
        AssertUtil.notNull(webRequest.getBody());
        return Result.success(umsPositionService.get(webRequest.getBody()));
    }

    /**
     * 新增
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("新增岗位表")
    @PostMapping("/insert")
    public Result insert(@RequestBody WebRequest<UmsPosition> webRequest) {
        UmsPosition umsPosition = webRequest.getBody();
        AssertUtil.notNull(umsPosition);
        BaseDOBuilder.fillBaseDOForInsert(umsPosition);
        umsPositionService.insertSelective(umsPosition);
        return Result.success();
    }

    /**
     * 修改
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("修改岗位表")
    @PostMapping("/update")
    public Result update(@RequestBody WebRequest<UmsPosition> webRequest) {
        UmsPosition umsPosition = webRequest.getBody();
        AssertUtil.notNull(umsPosition);
        AssertUtil.notNull(umsPosition.getId());
        BaseDOBuilder.fillBaseDOForUpdate(umsPosition);
        umsPositionService.updateSelective(umsPosition);
        return Result.success();
    }

    /**
     * 删除
     *
     * @param webRequest 参数, 待删除主键,多个使用英文逗号拼接
     * @return
     */
    @Trace("删除岗位表")
    @PostMapping("/delete")
    public Result delete(@RequestBody WebRequest<String> webRequest) {
        String ids = webRequest.getBody();
        AssertUtil.notBlank(ids);
        if (ids.contains(",")) {
            umsPositionService.batchDelete(Arrays.stream(ids.split(",")).map(Long::valueOf).collect(Collectors.toList()));
        } else {
            umsPositionService.delete(Long.valueOf(ids));
        }
        return Result.success();
    }

    /**
     * 导入
     * @param file
     * @param request
     * @return
     */
    @Trace("导入岗位表")
    @PostMapping("/import")
    public Result importExcel(MultipartFile file, HttpServletRequest request) {
        try {
            if (file != null && !file.isEmpty()) {
                //excel导入处理,返回excel中的数据集合
                IExcelImport ei = new ExcelImport(file);//将文件转为ExcelImport对象
                //从excel读取数据
                List<UmsPosition> list = ei.getImportDataAsBean(UmsPosition.class);
                if (list != null && list.size() > 0) {
                    for (UmsPosition item : list) {
                        BaseDOBuilder.fillBaseDOForInsert(item);
                    }
                    int num = umsPositionService.batchInsertSelective(list);//批量插入
                    return Result.success("成功导入数据" + num + "条!");
                } else {
                    return Result.error("导入失败:excel解析后结果为空!");
                }
            } else {
                return Result.error("导入失败:文件为空!");
            }
        } catch (Throwable e) {
            logger.error("[导入岗位表] 导入发生异常", e);
            return Result.error("导入失败!");
        }
    }

    /**
     * 下载excel导入模板
     * @param request
     * @param response
     * @throws Exception
     */
    @Trace("下载岗位表excel导入模板")
    @GetMapping("/templateDownload")
    public void templateDownload(HttpServletRequest request, HttpServletResponse response) throws Exception {
        IExcelExport ee = new ExcelExport();
        ee.insertBeanList(new ArrayList(), UmsPosition.class);
        ee.write2Response(response, "template.xls");
    }

    /**
     * 导出
     * @param obj 查询参数
     * @param request
     * @param response
     * @throws Exception
     */
    @Trace("导出岗位表")
    @GetMapping("/export")
    public void exportExcel(UmsPosition obj, HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<UmsPosition> list = umsPositionService.getList(obj);
        if (list != null && list.size() > 0) {
            IExcelExport ee = new ExcelExport();
            ee.insertBeanList(list, UmsPosition.class);
            ee.write2Response(response, "excel_" + System.currentTimeMillis() + ".xls");
        } else {
            response.getWriter().write("数据为空!");
        }
    }

}
