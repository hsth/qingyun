package com.imyuanma.qingyun.ums.model;

import lombok.Data;
import java.util.Date;
import com.imyuanma.qingyun.interfaces.common.model.DbSortDO;

/**
 * 角色用户关联表实体类
 *
 * @author YuanMaKeJi
 * @date 2023-03-26 15:11:39
 */
@Data
public class UmsRoleUser extends DbSortDO {

    /**
     * 主键
     */
    private Long id;

    /**
     * 角色id
     */
    private Long roleId;

    /**
     * 用户id
     */
    private Long userId;

    public UmsRoleUser() {
    }

    public UmsRoleUser(Long roleId, Long userId) {
        this.roleId = roleId;
        this.userId = userId;
    }
}