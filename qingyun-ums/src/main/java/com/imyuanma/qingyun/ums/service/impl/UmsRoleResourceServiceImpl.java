package com.imyuanma.qingyun.ums.service.impl;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.ums.dao.IUmsRoleResourceDao;
import com.imyuanma.qingyun.ums.model.UmsRoleResource;
import com.imyuanma.qingyun.ums.service.IUmsRoleResourceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 角色资源关联表服务
 *
 * @author YuanMaKeJi
 * @date 2023-03-26 11:42:34
 */
@Slf4j
@Service
public class UmsRoleResourceServiceImpl implements IUmsRoleResourceService {

    /**
     * 角色资源关联表dao
     */
    @Autowired
    private IUmsRoleResourceDao umsRoleResourceDao;

    /**
     * 分配权限
     *
     * @param roleId      角色id
     * @param resourceIds 角色授权的全部资源id(原有的权限会被替换为新的)
     */
    @Transactional(rollbackFor = Throwable.class)
    @Trace("分配权限")
    @Override
    public void grantAuth(Long roleId, List<Long> resourceIds) {
        AssertUtil.notNull(roleId);
        // 先删除
        umsRoleResourceDao.deleteByRoleId(roleId);
        // 再批量插入
        if (CollectionUtil.isNotEmpty(resourceIds)) {
            List<UmsRoleResource> list = resourceIds.stream().distinct().map(resourceId -> new UmsRoleResource(roleId, resourceId)).collect(Collectors.toList());
            if (CollectionUtil.isNotEmpty(list)) {
                // 批量插入
                this.batchInsert(list);
            }
        } else {
            log.warn("[分配权限] 传入资源为空,相当于清空角色关联资源,角色={}", roleId);
        }
    }

    /**
     * 列表查询
     *
     * @param umsRoleResource 查询条件
     * @return
     */
    @Trace("查询角色资源关联表列表")
    @Override
    public List<UmsRoleResource> getList(UmsRoleResource umsRoleResource) {
        return umsRoleResourceDao.getList(umsRoleResource);
    }

    /**
     * 分页查询
     *
     * @param umsRoleResource 查询条件
     * @param pageQuery 分页参数
     * @return
     */
    @Trace("分页查询角色资源关联表")
    @Override
    public List<UmsRoleResource> getPage(UmsRoleResource umsRoleResource, PageQuery pageQuery) {
        return umsRoleResourceDao.getList(umsRoleResource, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param umsRoleResource 查询条件
     * @return
     */
    @Trace("统计角色资源关联表数量")
    @Override
    public int count(UmsRoleResource umsRoleResource) {
        return umsRoleResourceDao.count(umsRoleResource);
    }

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    @Trace("根据主键查询角色资源关联表")
    @Override
    public UmsRoleResource get(Long id) {
        return umsRoleResourceDao.get(id);
    }

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    @Trace("根据主键批量查询角色资源关联表")
    @Override
    public List<UmsRoleResource> getListByIds(List<Long> list) {
        return umsRoleResourceDao.getListByIds(list);
    }

    /**
     * 根据roleId查询
     *
     * @param roleId 角色id
     * @return
     */
    @Trace("根据roleId查询角色资源关联表")
    @Override
    public List<UmsRoleResource> getListByRoleId(Long roleId) {
        return umsRoleResourceDao.getListByRoleId(roleId);
    }

    /**
     * 根据角色id查询关联资源id
     *
     * @param roleIdList 角色集合
     * @return
     */
    @Trace("批量查询角色关联资源")
    @Override
    public List<Long> getResourceIdListByRoleIdList(List<Long> roleIdList) {
        return umsRoleResourceDao.getResourceIdListByRoleIdList(roleIdList);
    }

    /**
     * 根据resourceId查询
     *
     * @param resourceId 资源id
     * @return
     */
    @Trace("根据resourceId查询角色资源关联表")
    @Override
    public List<UmsRoleResource> getListByResourceId(Long resourceId) {
        return umsRoleResourceDao.getListByResourceId(resourceId);
    }

    /**
     * 插入
     *
     * @param umsRoleResource 参数
     * @return
     */
    @Trace("插入角色资源关联表")
    @Override
    public int insert(UmsRoleResource umsRoleResource) {
        return umsRoleResourceDao.insert(umsRoleResource);
    }

    /**
     * 选择性插入
     *
     * @param umsRoleResource 参数
     * @return
     */
    @Trace("选择性插入角色资源关联表")
    @Override
    public int insertSelective(UmsRoleResource umsRoleResource) {
        return umsRoleResourceDao.insertSelective(umsRoleResource);
    }

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量插入角色资源关联表")
    @Override
    public int batchInsert(List<UmsRoleResource> list) {
        return umsRoleResourceDao.batchInsert(list);
    }

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量选择性插入角色资源关联表")
    @Override
    public int batchInsertSelective(List<UmsRoleResource> list) {
        return umsRoleResourceDao.batchInsertSelective(list);
    }

    /**
     * 修改
     *
     * @param umsRoleResource 参数
     * @return
     */
    @Trace("修改角色资源关联表")
    @Override
    public int update(UmsRoleResource umsRoleResource) {
        return umsRoleResourceDao.update(umsRoleResource);
    }

    /**
     * 选择性修改
     *
     * @param umsRoleResource 参数
     * @return
     */
    @Trace("选择性修改角色资源关联表")
    @Override
    public int updateSelective(UmsRoleResource umsRoleResource) {
        return umsRoleResourceDao.updateSelective(umsRoleResource);
    }

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    @Trace("删除角色资源关联表")
    @Override
    public int delete(Long id) {
        return umsRoleResourceDao.delete(id);
    }

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    @Trace("批量删除角色资源关联表")
    @Override
    public int batchDelete(List<Long> list) {
        return umsRoleResourceDao.batchDelete(list);
    }

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param umsRoleResource 参数
     * @return
     */
    @Trace("根据条件删除角色资源关联表")
    @Override
    public int deleteByCondition(UmsRoleResource umsRoleResource) {
        return umsRoleResourceDao.deleteByCondition(umsRoleResource);
    }

}
