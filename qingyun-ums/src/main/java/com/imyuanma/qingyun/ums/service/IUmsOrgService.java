package com.imyuanma.qingyun.ums.service;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.ums.model.UmsOrg;

import java.util.List;
import java.util.Map;

/**
 * 组织机构服务
 *
 * @author YuanMaKeJi
 * @date 2022-10-08 15:19:19
 */
public interface IUmsOrgService {

    /**
     * 列表查询
     *
     * @param umsOrg 查询条件
     * @return
     */
    List<UmsOrg> getList(UmsOrg umsOrg);

    /**
     * 分页查询
     *
     * @param umsOrg    查询条件
     * @param pageQuery 分页参数
     * @return
     */
    List<UmsOrg> getPage(UmsOrg umsOrg, PageQuery pageQuery);

    /**
     * 统计数量
     *
     * @param umsOrg 查询条件
     * @return
     */
    int count(UmsOrg umsOrg);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    UmsOrg get(Long id);

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    List<UmsOrg> getListByIds(List<Long> list);

    /**
     * 批量查询组织机构名称
     *
     * @param list
     * @return
     */
    Map<Long, String> getOrgNameByIds(List<Long> list);

    /**
     * 插入
     *
     * @param umsOrg 参数
     * @return
     */
    int insert(UmsOrg umsOrg);

    /**
     * 选择性插入
     *
     * @param umsOrg 参数
     * @return
     */
    int insertSelective(UmsOrg umsOrg);

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    int batchInsert(List<UmsOrg> list);

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    int batchInsertSelective(List<UmsOrg> list);

    /**
     * 修改
     *
     * @param umsOrg 参数
     * @return
     */
    int update(UmsOrg umsOrg);

    /**
     * 选择性修改
     *
     * @param umsOrg 参数
     * @return
     */
    int updateSelective(UmsOrg umsOrg);

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    int delete(Long id);

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    int batchDelete(List<Long> list);

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param umsOrg 参数
     * @return
     */
    int deleteByCondition(UmsOrg umsOrg);

}
