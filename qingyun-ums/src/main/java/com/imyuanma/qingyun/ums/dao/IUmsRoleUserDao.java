package com.imyuanma.qingyun.ums.dao;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.ums.model.UmsRoleUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 角色用户关联表dao
 *
 * @author YuanMaKeJi
 * @date 2023-03-26 15:11:39
 */
@Mapper
public interface IUmsRoleUserDao {

    /**
     * 列表查询
     *
     * @param umsRoleUser 查询条件
     * @return
     */
    List<UmsRoleUser> getList(UmsRoleUser umsRoleUser);

    /**
     * 分页查询
     *
     * @param umsRoleUser 查询条件
     * @param pageQuery   分页参数
     * @return
     */
    List<UmsRoleUser> getList(UmsRoleUser umsRoleUser, PageQuery pageQuery);

    /**
     * 统计数量
     *
     * @param umsRoleUser 查询条件
     * @return
     */
    int count(UmsRoleUser umsRoleUser);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    UmsRoleUser get(Long id);

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    List<UmsRoleUser> getListByIds(List<Long> list);

    /**
     * 根据roleId查询
     *
     * @param roleId 角色id
     * @return
     */
    List<UmsRoleUser> getListByRoleId(@Param("roleId") Long roleId);

    /**
     * 根据userId查询
     *
     * @param userId 用户id
     * @return
     */
    List<UmsRoleUser> getListByUserId(@Param("userId") Long userId);

    /**
     * 插入
     *
     * @param umsRoleUser 参数
     * @return
     */
    int insert(UmsRoleUser umsRoleUser);

    /**
     * 选择性插入
     *
     * @param umsRoleUser 参数
     * @return
     */
    int insertSelective(UmsRoleUser umsRoleUser);

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    int batchInsert(List<UmsRoleUser> list);

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    int batchInsertSelective(List<UmsRoleUser> list);

    /**
     * 修改
     *
     * @param umsRoleUser 参数
     * @return
     */
    int update(UmsRoleUser umsRoleUser);

    /**
     * 选择性修改
     *
     * @param umsRoleUser 参数
     * @return
     */
    int updateSelective(UmsRoleUser umsRoleUser);

    /**
     * 批量修改某字段
     *
     * @param idList      主键集合
     * @param fieldDbName 待修改的字段名
     * @param fieldValue  修改后的值
     * @return
     */
    int batchUpdateColumn(@Param("idList") List<Long> idList, @Param("fieldDbName") String fieldDbName, @Param("fieldValue") Object fieldValue);

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    int delete(Long id);

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    int batchDelete(List<Long> list);

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param umsRoleUser 参数
     * @return
     */
    int deleteByCondition(UmsRoleUser umsRoleUser);

    /**
     * 删除全部
     *
     * @return
     */
    int deleteAll();

    /**
     * 根据用户删除关联
     *
     * @param userId
     * @return
     */
    int deleteByUserId(Long userId);

    /**
     * 根据角色删除关联
     *
     * @param roleId
     * @return
     */
    int deleteByRoleId(Long roleId);

}
