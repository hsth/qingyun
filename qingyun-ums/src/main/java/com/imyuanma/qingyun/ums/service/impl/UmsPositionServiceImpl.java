package com.imyuanma.qingyun.ums.service.impl;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.ums.dao.IUmsPositionDao;
import com.imyuanma.qingyun.ums.model.UmsPosition;
import com.imyuanma.qingyun.ums.service.IUmsPositionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 职位服务
 *
 * @author wangjy
 * @date 2022/05/21 14:26:53
 */
@Slf4j
@Service
public class UmsPositionServiceImpl implements IUmsPositionService {

    /**
     * 职位dao
     */
    @Autowired
    private IUmsPositionDao umsPositionDao;

    /**
     * 列表查询
     *
     * @param umsPosition 查询条件
     * @return
     */
    @Override
    public List<UmsPosition> getList(UmsPosition umsPosition) {
        return umsPositionDao.getList(umsPosition);
    }

    /**
     * 分页查询
     *
     * @param umsPosition 查询条件
     * @param pageQuery   分页参数
     * @return
     */
    @Override
    public List<UmsPosition> getPage(UmsPosition umsPosition, PageQuery pageQuery) {
        return umsPositionDao.getList(umsPosition, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param umsPosition 查询条件
     * @return
     */
    @Override
    public int count(UmsPosition umsPosition) {
        return umsPositionDao.count(umsPosition);
    }

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    @Override
    public UmsPosition get(Long id) {
        return umsPositionDao.get(id);
    }

    /**
     * 插入
     *
     * @param umsPosition 参数
     * @return
     */
    @Override
    public int insert(UmsPosition umsPosition) {
        return umsPositionDao.insert(umsPosition);
    }

    /**
     * 选择性插入
     *
     * @param umsPosition 参数
     * @return
     */
    @Override
    public int insertSelective(UmsPosition umsPosition) {
        return umsPositionDao.insertSelective(umsPosition);
    }

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    @Override
    public int batchInsert(List<UmsPosition> list) {
        return umsPositionDao.batchInsert(list);
    }

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    @Override
    public int batchInsertSelective(List<UmsPosition> list) {
        return umsPositionDao.batchInsertSelective(list);
    }

    /**
     * 修改
     *
     * @param umsPosition 参数
     * @return
     */
    @Override
    public int update(UmsPosition umsPosition) {
        return umsPositionDao.update(umsPosition);
    }

    /**
     * 选择性修改
     *
     * @param umsPosition 参数
     * @return
     */
    @Override
    public int updateSelective(UmsPosition umsPosition) {
        return umsPositionDao.updateSelective(umsPosition);
    }

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    @Override
    public int delete(Long id) {
        return umsPositionDao.delete(id);
    }

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    @Override
    public int batchDelete(List<Long> list) {
        return umsPositionDao.batchDelete(list);
    }

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param umsPosition 参数
     * @return
     */
    @Override
    public int deleteByCondition(UmsPosition umsPosition) {
        return umsPositionDao.deleteByCondition(umsPosition);
    }
}
