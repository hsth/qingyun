package com.imyuanma.qingyun.ums.model;

import com.imyuanma.qingyun.common.core.structure.tree.ITreeNode;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.imyuanma.qingyun.interfaces.common.model.BaseDO;

/**
 * 组织机构实体类
 *
 * @author YuanMaKeJi
 * @date 2022-10-08 15:19:19
 */
@Data
public class UmsOrg extends BaseDO implements ITreeNode<Long>, Comparable<UmsOrg> {

    /**
     * 主键
     */
    private Long id;

    /**
     * 组织机构编号
     */
    private String code;

    /**
     * 组织机构名称
     */
    private String name;

    /**
     * 别名
     */
    private String alias;

    /**
     * 所属单位id,若本身就是单位,则为自身
     */
    private Long companyId;

    /**
     * 上级id
     * 单位的上级一定是单位, 部门的上级可以是单位或部门
     */
    private Long parentId;

    /**
     * 组织机构id全路径
     */
    private String wholeId;

    /**
     * 组织机构名称全路径
     */
    private String wholeName;

    /**
     * 组织机构类型,10单位,20部门
     */
    private Integer type;

    /**
     * 备注
     */
    private String remark;


    /**
     * 子节点
     */
    private List<UmsOrg> children;


    /**
     * 当前节点id
     *
     * @return
     */
    @Override
    public Long nodeId() {
        return id;
    }

    /**
     * 父节点id
     *
     * @return
     */
    @Override
    public Long parentNodeId() {
        return parentId;
    }

    /**
     * 添加子节点
     *
     * @param node 节点对象
     */
    @Override
    public void appendChildren(ITreeNode<Long> node) {
        if (children == null) {
            children = new ArrayList<>();
        }
        children.add((UmsOrg) node);
    }

    /**
     * 比较
     * @param o
     * @return
     */
    @Override
    public int compareTo(UmsOrg o) {
        Long a1 = this.getDataSequence() != null ? this.getDataSequence() : 0L;
        Long a2 = o.getDataSequence() != null ? o.getDataSequence() : 0L;
        return a1.compareTo(a2);
    }
}