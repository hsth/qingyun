package com.imyuanma.qingyun.ums.model.enums;

/**
 * 登录结果枚举
 *
 * @author wangjy
 * @date 2023/04/08 15:43:02
 */
public enum EUmsLoginResultEnum {
    SUCCESS(10, "成功"),
    FAIL(20, "失败"),
    ;
    /**
     * code
     */
    private Integer code;
    /**
     * 文案
     */
    private String text;

    EUmsLoginResultEnum(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    public Integer getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
