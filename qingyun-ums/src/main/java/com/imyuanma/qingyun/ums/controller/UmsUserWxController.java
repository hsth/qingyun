package com.imyuanma.qingyun.ums.controller;

import com.imyuanma.qingyun.common.factory.BaseDOBuilder;
import com.imyuanma.qingyun.common.model.request.WebRequest;
import com.imyuanma.qingyun.common.model.response.Page;
import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.ums.model.UmsUserWx;
import com.imyuanma.qingyun.ums.service.IUmsUserWxService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 用户微信信息web
 *
 * @author YuanMaKeJi
 * @date 2023-11-05 23:03:28
 */
@RestController
@RequestMapping(value = "/ums/umsUserWx")
public class UmsUserWxController {

    private static final Logger logger = LoggerFactory.getLogger(UmsUserWxController.class);

    /**
     * 用户微信信息服务
     */
    @Autowired
    private IUmsUserWxService umsUserWxService;

    /**
     * 列表查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("查询用户微信信息列表")
    @PostMapping("/getList")
    public Result<List<UmsUserWx>> getList(@RequestBody WebRequest<UmsUserWx> webRequest) {
        return Result.success(umsUserWxService.getList(webRequest.getBody()));
    }

    /**
     * 分页查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("分页查询用户微信信息")
    @PostMapping("/getPage")
    public Page<List<UmsUserWx>> getPage(@RequestBody WebRequest<UmsUserWx> webRequest) {
        PageQuery pageQuery = webRequest.buildPageQuery();
        List<UmsUserWx> list = umsUserWxService.getPage(webRequest.getBody(), pageQuery);
        return Page.success(list, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("统计用户微信信息数量")
    @PostMapping("/count")
    public Result<Integer> count(@RequestBody WebRequest<UmsUserWx> webRequest) {
        return Result.success(umsUserWxService.count(webRequest.getBody()));
    }

    /**
     * 查询
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("查询用户微信信息")
    @PostMapping("/get")
    public Result<UmsUserWx> get(@RequestBody WebRequest<Long> webRequest) {
        AssertUtil.notNull(webRequest.getBody());
        return Result.success(umsUserWxService.get(webRequest.getBody()));
    }

    /**
     * 新增
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("新增用户微信信息")
    @PostMapping("/insert")
    public Result insert(@RequestBody WebRequest<UmsUserWx> webRequest) {
        UmsUserWx umsUserWx = webRequest.getBody();
        AssertUtil.notNull(umsUserWx);
        BaseDOBuilder.fillBaseDOForInsert(umsUserWx);
        umsUserWxService.insertSelective(umsUserWx);
        return Result.success();
    }

    /**
     * 修改
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("修改用户微信信息")
    @PostMapping("/update")
    public Result update(@RequestBody WebRequest<UmsUserWx> webRequest) {
        UmsUserWx umsUserWx = webRequest.getBody();
        AssertUtil.notNull(umsUserWx);
        AssertUtil.notNull(umsUserWx.getId());
        BaseDOBuilder.fillBaseDOForUpdate(umsUserWx);
        umsUserWxService.updateSelective(umsUserWx);
        return Result.success();
    }

    /**
     * 删除
     *
     * @param webRequest 参数, 待删除主键,多个使用英文逗号拼接
     * @return
     */
    @Trace("删除用户微信信息")
    @PostMapping("/delete")
    public Result delete(@RequestBody WebRequest<String> webRequest) {
        String ids = webRequest.getBody();
        AssertUtil.notBlank(ids);
        if (ids.contains(",")) {
            umsUserWxService.batchDelete(Arrays.stream(ids.split(",")).map(Long::valueOf).collect(Collectors.toList()));
        } else {
            umsUserWxService.delete(Long.valueOf(ids));
        }
        return Result.success();
    }



}
