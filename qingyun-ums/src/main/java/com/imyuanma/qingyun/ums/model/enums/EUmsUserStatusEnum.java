package com.imyuanma.qingyun.ums.model.enums;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 用户状态
 *
 * @author wangjy
 * @date 2022/07/16 14:38:44
 */
public enum EUmsUserStatusEnum {
    ENABLE(10, "有效"),
    DISABLE(20, "无效");
    /**
     * code
     */
    private Integer code;
    /**
     * 文案
     */
    private String text;

    EUmsUserStatusEnum(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    /**
     * 枚举code集合
     *
     * @return
     */
    public static List<Integer> codes() {
        return Arrays.stream(EUmsUserStatusEnum.values()).map(EUmsUserStatusEnum::getCode).collect(Collectors.toList());
    }

    public Integer getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
