package com.imyuanma.qingyun.ums.service.out;

import com.imyuanma.qingyun.interfaces.ums.service.IUmsPasswordOutService;
import com.imyuanma.qingyun.ums.util.UmsUserPwdUtil;
import org.springframework.stereotype.Service;

/**
 * 密码服务
 *
 * @author wangjy
 * @date 2024/03/08 23:12:15
 */
@Service
public class UmsPasswordOutServiceImpl implements IUmsPasswordOutService {
    /**
     * 密码加密
     *
     * @param rawPassword 明文密码
     * @return 密文密码
     */
    @Override
    public String encode(CharSequence rawPassword) {
        return UmsUserPwdUtil.encodePwd(rawPassword.toString());
    }

    /**
     * 密码匹配
     *
     * @param rawPassword     明文密码
     * @param encodedPassword 密文密码
     * @return 是否匹配
     */
    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return UmsUserPwdUtil.encodePwd(rawPassword.toString()).equals(encodedPassword);
    }

    /**
     * 解析前端传入的密码
     *
     * @param inputPassword 前端传入的密码, 前端加密过程: 原文 -> base64编码(utf8) -> rsa加密得到密文 -> 对密文base64编码(utf8)
     * @return 用户真正输入的密码
     */
    @Override
    public String parseInputPassword(CharSequence inputPassword) {
        return UmsUserPwdUtil.parsePwd(inputPassword.toString());
    }
}
