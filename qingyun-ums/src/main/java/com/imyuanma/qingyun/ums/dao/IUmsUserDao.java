package com.imyuanma.qingyun.ums.dao;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.ums.model.UmsUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户信息dao
 *
 * @author YuanMaKeJi
 * @date 2022-07-09 16:23:45
 */
@Mapper
public interface IUmsUserDao {

    /**
     * 列表查询
     *
     * @param umsUser 查询条件
     * @return
     */
    @Trace("查询用户列表")
    List<UmsUser> getList(UmsUser umsUser);

    /**
     * 分页查询
     *
     * @param umsUser   查询条件
     * @param pageQuery 分页参数
     * @return
     */
    @Trace("分页查询用户")
    List<UmsUser> getList(UmsUser umsUser, PageQuery pageQuery);

    /**
     * 统计数量
     *
     * @param umsUser 查询条件
     * @return
     */
    int count(UmsUser umsUser);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    @Trace("查询用户")
    UmsUser get(Long id);

    /**
     * 根据账号查询用户信息
     *
     * @param account 账号
     * @return
     */
    UmsUser getByAccount(String account);

    /**
     * 根据密码找回key查询用户
     *
     * @param findPasswordKey
     * @return
     */
    UmsUser getByFindPwdKey(String findPasswordKey);

    /**
     * 插入
     *
     * @param umsUser 参数
     * @return
     */
    int insert(UmsUser umsUser);

    /**
     * 选择性插入
     *
     * @param umsUser 参数
     * @return
     */
    int insertSelective(UmsUser umsUser);

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    int batchInsert(List<UmsUser> list);

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    int batchInsertSelective(List<UmsUser> list);

    /**
     * 修改
     *
     * @param umsUser 参数
     * @return
     */
    int update(UmsUser umsUser);

    /**
     * 选择性修改
     *
     * @param umsUser 参数
     * @return
     */
    int updateSelective(UmsUser umsUser);

    /**
     * 批量修改某字段
     *
     * @param idList      主键集合
     * @param fieldDbName 待修改的字段名
     * @param fieldValue  修改后的值
     * @return
     */
    int batchUpdateColumn(@Param("idList") List<Long> idList, @Param("fieldDbName") String fieldDbName, @Param("fieldValue") Object fieldValue);

    /**
     * 批量更新密码
     *
     * @param idList   主键集合
     * @param password 新密码
     * @return
     */
    int batchUpdatePassword(@Param("idList") List<Long> idList, @Param("password") String password);

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    int delete(Long id);

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    int batchDelete(List<Long> list);

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param umsUser 参数
     * @return
     */
    int deleteByCondition(UmsUser umsUser);

    /**
     * 删除全部
     *
     * @return
     */
    int deleteAll();

}
