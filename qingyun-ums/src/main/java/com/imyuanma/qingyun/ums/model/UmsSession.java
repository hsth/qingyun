package com.imyuanma.qingyun.ums.model;

import com.imyuanma.qingyun.ums.model.enums.EUmsTokenStatusEnum;
import lombok.Data;
import java.util.Date;
import com.imyuanma.qingyun.interfaces.common.model.BaseDO;

/**
 * 登录会话实体类
 *
 * @author YuanMaKeJi
 * @date 2022-07-16 11:43:58
 */
@Data
public class UmsSession extends BaseDO {

    /**
     * 主键
     */
    private Long id;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 令牌
     */
    private String token;

    /**
     * 令牌生效时间
     */
    private Date signTime;
    /**
     * 令牌生效时间,按时间检索时作为结束时间使用
     */
    private Date signTime2;

    /**
     * 令牌过期时间
     */
    private Date expireTime;
    /**
     * 令牌过期时间,按时间检索时作为结束时间使用
     */
    private Date expireTime2;

    /**
     * 状态,10有效,20无效
     * @see EUmsTokenStatusEnum
     */
    private Integer tokenStatus;

    /**
     * 设备标识
     */
    private String deviceAddr;

    /**
     * 设备类型
     */
    private Integer deviceType;

    /**
     * 版本号
     */
    private String deviceVersion;

    /**
     * 客户端ip
     */
    private String clientIp;

    /**
     * 服务端ip
     */
    private String serverIp;



}