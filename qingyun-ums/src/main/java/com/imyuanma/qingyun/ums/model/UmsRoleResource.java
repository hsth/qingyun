package com.imyuanma.qingyun.ums.model;

import lombok.Data;
import java.util.Date;
import com.imyuanma.qingyun.interfaces.common.model.DbSortDO;

/**
 * 角色资源关联表实体类
 *
 * @author YuanMaKeJi
 * @date 2023-03-26 11:42:34
 */
@Data
public class UmsRoleResource extends DbSortDO {

    /**
     * 主键
     */
    private Long id;

    /**
     * 角色id
     */
    private Long roleId;

    /**
     * 资源id
     */
    private Long resourceId;

    public UmsRoleResource() {
    }

    public UmsRoleResource(Long roleId, Long resourceId) {
        this.roleId = roleId;
        this.resourceId = resourceId;
    }
}