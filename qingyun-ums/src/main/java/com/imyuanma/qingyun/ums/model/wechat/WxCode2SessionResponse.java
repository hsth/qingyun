package com.imyuanma.qingyun.ums.model.wechat;

import lombok.Data;

/**
 * 微信登录code换会话信息结果
 *
 * @author wangjy
 * @date 2023/09/29 12:00:35
 */
@Data
public class WxCode2SessionResponse {
    /**
     * 错误码
     */
    private Long errcode;
    /**
     * 信息
     */
    private String errmsg;
    /**
     * 微信用户的唯一标识
     */
    private String openid;
    /**
     * 会话密钥
     */
    private String session_key;
    /**
     * 用户在微信开放平台的唯一标识符。本字段在满足一定条件的情况下才返回。
     */
    private String unionid;

    /**
     * 返回请求是否成功
     * @return
     */
    public boolean success() {
        return errcode == null || errcode.intValue() == 0;
    }
}
