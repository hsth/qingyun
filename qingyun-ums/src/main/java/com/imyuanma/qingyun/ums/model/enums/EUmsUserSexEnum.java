package com.imyuanma.qingyun.ums.model.enums;

/**
 * 性别枚举
 *
 * @author YuanMaKeJi
 * @date 2022-10-07 14:10:13
 */
public enum EUmsUserSexEnum {
    CODE_10(10, "男"),
    CODE_20(20, "女"),
    ;

    /**
     * code
     */
    private Integer code;
    /**
     * 文案
     */
    private String text;

    EUmsUserSexEnum(Integer code, String text) {
        this.code = code;
        this.text = text;
    }

    /**
     * 根据code获取枚举
     *
     * @param code code值
     */
    public static EUmsUserSexEnum getByCode(Integer code) {
        for (EUmsUserSexEnum e : EUmsUserSexEnum.values()) {
            if (e.code != null && e.code.equals(code)) {
                return e;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
