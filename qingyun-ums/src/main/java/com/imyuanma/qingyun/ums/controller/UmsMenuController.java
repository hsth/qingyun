package com.imyuanma.qingyun.ums.controller;

import com.imyuanma.qingyun.common.client.ums.LoginUserHolder;
import com.imyuanma.qingyun.common.core.structure.tree.TreeBuilder;
import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.interfaces.common.model.enums.ETrashFlagEnum;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.interfaces.ums.model.LoginUserDTO;
import com.imyuanma.qingyun.ums.model.UmsMenu;
import com.imyuanma.qingyun.ums.model.UmsResource;
import com.imyuanma.qingyun.ums.model.UmsRoleUser;
import com.imyuanma.qingyun.ums.model.enums.EUmsResourceStatusEnum;
import com.imyuanma.qingyun.ums.model.enums.EUmsResourceTypeEnum;
import com.imyuanma.qingyun.ums.service.IUmsResourceService;
import com.imyuanma.qingyun.ums.service.IUmsRoleResourceService;
import com.imyuanma.qingyun.ums.service.IUmsRoleUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 菜单web
 *
 * @author wangjy
 * @date 2022/10/05 21:01:57
 */
@RestController
@RequestMapping(value = "/ums/menu")
public class UmsMenuController {

    @Autowired
    private IUmsResourceService umsResourceService;
    @Autowired
    private IUmsRoleUserService roleUserService;
    @Autowired
    private IUmsRoleResourceService roleResourceService;

    @Trace("查询菜单")
    @PostMapping("/getMenuTree")
    public Result<List<UmsMenu>> getMenuTree() {
        // 当前用户
        LoginUserDTO loginUserDTO = LoginUserHolder.getLoginUser();
        // 用户关联的角色
        List<UmsRoleUser> roleUserList = roleUserService.getListByUserId(loginUserDTO.getUserId());
        if (CollectionUtil.isNotEmpty(roleUserList)) {
            // 查询角色关联的菜单
            List<Long> resourceIds = roleResourceService.getResourceIdListByRoleIdList(roleUserList.stream().map(UmsRoleUser::getRoleId).distinct().collect(Collectors.toList()));
            if (CollectionUtil.isNotEmpty(resourceIds)) {
                // 查询菜单
                List<UmsResource> resourceList = this.getValidMenuResource(resourceIds);
                // 转菜单树
                if (CollectionUtil.isNotEmpty(resourceList)) {
                    return Result.success(TreeBuilder.buildTree(resourceList.stream().map(UmsMenu::convertFrom).sorted().collect(Collectors.toList())));
                }
            }
        }
        // TODO 超管暂时返回全部, 兼容老系统
        if (loginUserDTO.getUserId() == 1) {
            // 所有菜单
            List<UmsResource> resourceList = this.getValidMenuResource(null);
            if (CollectionUtil.isNotEmpty(resourceList)) {
                return Result.success(TreeBuilder.buildTree(resourceList.stream().map(UmsMenu::convertFrom).sorted().collect(Collectors.toList())));
            }
        }
        return Result.success();
    }

    /**
     * 查询资源
     *
     * @param resourceIds
     * @return
     */
    private List<UmsResource> getValidMenuResource(List<Long> resourceIds) {
        UmsResource query = new UmsResource();
        query.setType(EUmsResourceTypeEnum.CODE_10.getCode());
        query.setStatus(EUmsResourceStatusEnum.VALID.getCode());
        query.setTrashFlag(ETrashFlagEnum.VALID.getType());
        query.setSearchIdList(resourceIds);
        return umsResourceService.getList(query);
    }

}
