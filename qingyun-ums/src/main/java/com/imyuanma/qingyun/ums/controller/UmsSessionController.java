package com.imyuanma.qingyun.ums.controller;

import com.imyuanma.qingyun.common.factory.BaseDOBuilder;
import com.imyuanma.qingyun.common.model.request.WebRequest;
import com.imyuanma.qingyun.common.model.response.Page;
import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.ums.model.UmsSession;
import com.imyuanma.qingyun.ums.service.IUmsSessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 登录会话web
 *
 * @author YuanMaKeJi
 * @date 2022-07-16 11:43:58
 */
@RestController
@RequestMapping(value = "/ums/umsSession")
public class UmsSessionController {

    /**
     * 登录会话服务
     */
    @Autowired
    private IUmsSessionService umsSessionService;

//    /**
//     * 列表查询
//     *
//     * @param webRequest 查询条件
//     * @return
//     */
//    @PostMapping("/getList")
//    public Result<List<UmsSession>> getList(@RequestBody WebRequest<UmsSession> webRequest) {
//        return Result.success(umsSessionService.getList(webRequest.getBody()));
//    }

    /**
     * 分页查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @PostMapping("/getPage")
    public Page<List<UmsSession>> getPage(@RequestBody WebRequest<UmsSession> webRequest) {
        PageQuery pageQuery = webRequest.buildPageQuery();
        List<UmsSession> list = umsSessionService.getPage(webRequest.getBody(), pageQuery);
        return Page.success(list, pageQuery);
    }

//    /**
//     * 统计数量
//     *
//     * @param webRequest 查询条件
//     * @return
//     */
//    @PostMapping("/count")
//    public Result<Integer> count(@RequestBody WebRequest<UmsSession> webRequest) {
//        return Result.success(umsSessionService.count(webRequest.getBody()));
//    }

    /**
     * 查询
     *
     * @param webRequest 参数
     * @return
     */
    @PostMapping("/get")
    public Result<UmsSession> get(@RequestBody WebRequest<Long> webRequest) {
        AssertUtil.notNull(webRequest.getBody());
        return Result.success(umsSessionService.get(webRequest.getBody()));
    }

//    /**
//     * 新增
//     *
//     * @param webRequest 参数
//     * @return
//     */
//    @PostMapping("/insert")
//    public Result insert(@RequestBody WebRequest<UmsSession> webRequest) {
//        UmsSession umsSession = webRequest.getBody();
//        AssertUtil.notNull(umsSession);
//        BaseDOBuilder.fillBaseDOForInsert(umsSession);
//        umsSessionService.insertSelective(umsSession);
//        return Result.success();
//    }
//
//    /**
//     * 修改
//     *
//     * @param webRequest 参数
//     * @return
//     */
//    @PostMapping("/update")
//    public Result update(@RequestBody WebRequest<UmsSession> webRequest) {
//        UmsSession umsSession = webRequest.getBody();
//        AssertUtil.notNull(umsSession);
//        AssertUtil.notNull(umsSession.getId());
//        BaseDOBuilder.fillBaseDOForUpdate(umsSession);
//        umsSessionService.updateSelective(umsSession);
//        return Result.success();
//    }

    @Trace("下线会话")
    @PostMapping("/offline")
    public Result offline(@RequestBody WebRequest<String> webRequest) {
        AssertUtil.notNull(webRequest);
        AssertUtil.notBlank(webRequest.getBody());
        umsSessionService.offlineSession(webRequest.getBody());
        return Result.success();
    }

    /**
     * 删除
     *
     * @param webRequest 参数, 待删除主键,多个使用英文逗号拼接
     * @return
     */
    @PostMapping("/delete")
    public Result delete(@RequestBody WebRequest<String> webRequest) {
        String ids = webRequest.getBody();
        AssertUtil.notBlank(ids);
        if (ids.contains(",")) {
            umsSessionService.batchDelete(Arrays.stream(ids.split(",")).map(Long::valueOf).collect(Collectors.toList()));
        } else {
            umsSessionService.delete(Long.valueOf(ids));
        }
        return Result.success();
    }

}
