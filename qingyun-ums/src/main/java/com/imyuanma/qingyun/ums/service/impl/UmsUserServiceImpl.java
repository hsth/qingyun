package com.imyuanma.qingyun.ums.service.impl;

import com.imyuanma.qingyun.common.exception.BaseException;
import com.imyuanma.qingyun.common.exception.Exceptions;
import com.imyuanma.qingyun.common.ext.ExtNodeExecuteUtil;
import com.imyuanma.qingyun.common.ext.ExtNodeExecutor;
import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.util.*;
import com.imyuanma.qingyun.interfaces.common.ext.ExtNodeResult;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.interfaces.ums.ext.UmsUserInsertPostHandleExt;
import com.imyuanma.qingyun.interfaces.ums.ext.param.UserInsertPostHandleParam;
import com.imyuanma.qingyun.ums.dao.IUmsUserDao;
import com.imyuanma.qingyun.ums.ext.IUmsUserProcessBeforeInsertExt;
import com.imyuanma.qingyun.ums.model.UmsUpdatePassword;
import com.imyuanma.qingyun.ums.model.UmsUser;
import com.imyuanma.qingyun.ums.model.enums.EUmsUserStatusEnum;
import com.imyuanma.qingyun.ums.service.IUmsUserService;
import com.imyuanma.qingyun.ums.util.UmsConstants;
import com.imyuanma.qingyun.ums.util.UmsUserPwdUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 用户信息服务
 *
 * @author YuanMaKeJi
 * @date 2022-07-09 16:23:45
 */
@Slf4j
@Service
public class UmsUserServiceImpl implements IUmsUserService {

    /**
     * 用户信息dao
     */
    @Autowired
    private IUmsUserDao umsUserDao;

    /**
     * 列表查询
     *
     * @param umsUser 查询条件
     * @return
     */
    @Trace("查询用户列表")
    @Override
    public List<UmsUser> getList(UmsUser umsUser) {
        return umsUserDao.getList(umsUser);
    }

    /**
     * 分页查询
     *
     * @param umsUser   查询条件
     * @param pageQuery 分页参数
     * @return
     */
    @Trace("分页查询用户")
    @Override
    public List<UmsUser> getPage(UmsUser umsUser, PageQuery pageQuery) {
        return umsUserDao.getList(umsUser, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param umsUser 查询条件
     * @return
     */
    @Trace("统计用户数量")
    @Override
    public int count(UmsUser umsUser) {
        return umsUserDao.count(umsUser);
    }

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    @Trace("根据主键查询用户")
    @Override
    public UmsUser get(Long id) {
        return umsUserDao.get(id);
    }

    /**
     * 根据账号查询用户信息
     *
     * @param account 账号
     * @return
     */
    @Trace("根据账号查询用户")
    @Override
    public UmsUser getByAccount(String account) {
        return umsUserDao.getByAccount(account);
    }

    /**
     * 根据密码找回key查询用户
     *
     * @param findPasswordKey
     * @return
     */
    @Trace("根据密码找回key查询用户")
    @Override
    public UmsUser getByFindPwdKey(String findPasswordKey) {
        return umsUserDao.getByFindPwdKey(findPasswordKey);
    }

    /**
     * 插入
     *
     * @param umsUser 参数
     * @return
     */
    @Transactional(rollbackFor = Throwable.class)
    @Trace("插入用户")
    @Override
    public int insert(UmsUser umsUser) {
        return this.insertSelective(umsUser);
    }

    /**
     * 选择性插入
     *
     * @param umsUser 参数
     * @return
     */
    @Transactional(rollbackFor = Throwable.class)
    @Trace("选择性插入用户")
    @Override
    public int insertSelective(UmsUser umsUser) {
        // 补全信息
        this.fillAndCheckForAdd(umsUser);
        // 密码处理
        this.handlePasswordForAdd(umsUser);
        // 插入前处理
        ExtNodeExecutor.of(IUmsUserProcessBeforeInsertExt.class)
                .successIfEmpty()
                .execute(umsUser);
        // 插入
        int num = umsUserDao.insertSelective(umsUser);
        // 插入后置处理扩展点
        ExtNodeExecutor.of(UmsUserInsertPostHandleExt.class)
                .successIfEmpty()
                .errorHandle(BaseException::new)
                .execute(new UserInsertPostHandleParam(umsUser));
        return num;
    }

    /**
     * 新增用户时信息补全
     *
     * @param umsUser
     */
    private void fillAndCheckForAdd(UmsUser umsUser) {
        // 有效状态
        if (umsUser.getStatus() == null) {
            umsUser.setStatus(EUmsUserStatusEnum.ENABLE.getCode());
        }
        if (StringUtil.isBlank(umsUser.getName())) {
            umsUser.setName(umsUser.getAccount());
        }
        if (umsUser.getCompanyId() == null) {
            umsUser.setCompanyId(1L);
        }
    }

    /**
     * 新增用户时的密码处理
     *
     * @param umsUser
     */
    private void handlePasswordForAdd(UmsUser umsUser) {
        if (StringUtil.isBlank(umsUser.getPassword())) {
            umsUser.setPassword(UmsConstants.DEFAULT_PASSWORD);
        }
        // 对密码进行加密
        umsUser.setPassword(UmsUserPwdUtil.encodePwd(umsUser.getPassword()));
    }

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    @Transactional(rollbackFor = Throwable.class)
    @Trace("批量插入用户")
    @Override
    public int batchInsert(List<UmsUser> list) {
        return this.batchInsertSelective(list);
    }

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    @Transactional(rollbackFor = Throwable.class)
    @Trace("批量选择性插入用户")
    @Override
    public int batchInsertSelective(List<UmsUser> list) {
        for (UmsUser umsUser : list) {
            // 补全信息
            this.fillAndCheckForAdd(umsUser);
            // 密码处理
            this.handlePasswordForAdd(umsUser);
            // 插入前处理
            ExtNodeExecutor.of(IUmsUserProcessBeforeInsertExt.class)
                    .successIfEmpty()
                    .execute(umsUser);
        }
        return umsUserDao.batchInsertSelective(list);
    }

    /**
     * 修改
     *
     * @param umsUser 参数
     * @return
     */
    @Transactional(rollbackFor = Throwable.class)
    @Trace("修改用户")
    @Override
    public int update(UmsUser umsUser) {
        // 不允许在这里修改密码
        umsUser.setPassword(null);
        return umsUserDao.update(umsUser);
    }

    /**
     * 选择性修改
     *
     * @param umsUser 参数
     * @return
     */
    @Transactional(rollbackFor = Throwable.class)
    @Trace("选择性修改用户")
    @Override
    public int updateSelective(UmsUser umsUser) {
        // 不允许在这里修改密码
        umsUser.setPassword(null);
        return umsUserDao.updateSelective(umsUser);
    }

    /**
     * 更新密码
     *
     * @param umsUpdatePassword
     * @return
     */
    @Transactional(rollbackFor = Throwable.class)
    @Trace("更新用户密码")
    @Override
    public int updatePassword(UmsUpdatePassword umsUpdatePassword) {
        // 先查询
        UmsUser user = this.get(umsUpdatePassword.getUserId());
        AssertUtil.notNull(user, "用户不存在");
        // 判断旧密码输入是否正确
        if (!user.getPassword().equals(UmsUserPwdUtil.encodePwd(umsUpdatePassword.getOldPwd()))) {
            log.warn("[修改密码] 原密码输入错误,入参={}", JsonUtil.toJson(umsUpdatePassword));
            throw Exceptions.paramException("原密码错误");
        }
        UmsUser umsUser = new UmsUser();
        umsUser.setId(umsUpdatePassword.getUserId());
        umsUser.setPassword(UmsUserPwdUtil.encodePwd(umsUpdatePassword.getNewPwd()));
        return umsUserDao.updateSelective(umsUser);
    }

    /**
     * 管理员更新密码
     *
     * @param umsUpdatePassword
     * @return
     */
    @Transactional(rollbackFor = Throwable.class)
    @Trace("管理员修改用户密码")
    @Override
    public void updatePasswordForAdmin(UmsUpdatePassword umsUpdatePassword) {
        // 先查询
        UmsUser user = this.get(umsUpdatePassword.getUserId());
        AssertUtil.notNull(user, "用户不存在");
        UmsUser umsUser = new UmsUser();
        umsUser.setId(umsUpdatePassword.getUserId());
        umsUser.setPassword(UmsUserPwdUtil.encodePwd(umsUpdatePassword.getNewPwd()));
        umsUserDao.updateSelective(umsUser);
    }

    /**
     * 管理员重置用户密码
     *
     * @param userIds
     */
    @Transactional(rollbackFor = Throwable.class)
    @Trace("管理员重置用户密码")
    @Override
    public void resetPasswordForAdmin(Long[] userIds) {
        umsUserDao.batchUpdatePassword(Arrays.asList(userIds), UmsUserPwdUtil.encodePwd(UmsConstants.DEFAULT_PASSWORD));
    }

    /**
     * 重置密码
     *
     * @param userId
     * @param pwd
     * @return
     */
    @Transactional(rollbackFor = Throwable.class)
    @Trace("重置密码")
    @Override
    public int resetPassword(Long userId, String pwd) {
        // 先查询
        UmsUser user = this.get(userId);
        AssertUtil.notNull(user, "用户不存在");
        UmsUser umsUser = new UmsUser();
        umsUser.setId(userId);
        umsUser.setPassword(UmsUserPwdUtil.encodePwd(pwd));
        umsUser.setFindPasswordKey("");
        return umsUserDao.updateSelective(umsUser);
    }

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    @Trace("删除用户")
    @Override
    public int delete(Long id) {
        return umsUserDao.delete(id);
    }

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    @Trace("批量删除用户")
    @Override
    public int batchDelete(List<Long> list) {
        return umsUserDao.batchDelete(list);
    }

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param umsUser 参数
     * @return
     */
    @Trace("根据条件删除用户")
    @Override
    public int deleteByCondition(UmsUser umsUser) {
        return umsUserDao.deleteByCondition(umsUser);
    }

    /**
     * 密码找回
     *
     * @param umsUser
     */
    @Override
    public String findPasswordInit(UmsUser umsUser) {
        UmsUser update = new UmsUser();
        update.setId(umsUser.getId());
        update.setFindPasswordEnd(DateUtil.afterSeconds(new Date(), UmsConstants.FIND_PWD_TIMEOUT_S));
        update.setFindPasswordKey(StringUtil.getUUID());
        umsUserDao.updateSelective(update);
        return update.getFindPasswordKey();
    }

}
