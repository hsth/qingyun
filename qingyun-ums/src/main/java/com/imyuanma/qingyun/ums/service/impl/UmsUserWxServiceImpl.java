package com.imyuanma.qingyun.ums.service.impl;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.ums.dao.IUmsUserWxDao;
import com.imyuanma.qingyun.ums.model.UmsUserWx;
import com.imyuanma.qingyun.ums.service.IUmsUserWxService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户微信信息服务
 *
 * @author YuanMaKeJi
 * @date 2023-11-05 23:03:28
 */
@Slf4j
@Service
public class UmsUserWxServiceImpl implements IUmsUserWxService {

    /**
     * 用户微信信息dao
     */
    @Autowired
    private IUmsUserWxDao umsUserWxDao;

    /**
     * 列表查询
     *
     * @param umsUserWx 查询条件
     * @return
     */
    @Trace("查询用户微信信息列表")
    @Override
    public List<UmsUserWx> getList(UmsUserWx umsUserWx) {
        return umsUserWxDao.getList(umsUserWx);
    }

    /**
     * 分页查询
     *
     * @param umsUserWx 查询条件
     * @param pageQuery 分页参数
     * @return
     */
    @Trace("分页查询用户微信信息")
    @Override
    public List<UmsUserWx> getPage(UmsUserWx umsUserWx, PageQuery pageQuery) {
        return umsUserWxDao.getList(umsUserWx, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param umsUserWx 查询条件
     * @return
     */
    @Trace("统计用户微信信息数量")
    @Override
    public int count(UmsUserWx umsUserWx) {
        return umsUserWxDao.count(umsUserWx);
    }

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    @Trace("主键查询用户微信信息")
    @Override
    public UmsUserWx get(Long id) {
        return umsUserWxDao.get(id);
    }

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    @Trace("主键批量查询用户微信信息")
    @Override
    public List<UmsUserWx> getListByIds(List<Long> list) {
        return umsUserWxDao.getListByIds(list);
    }

    /**
     * 根据userId查询
     *
     * @param userId 用户ID
     * @return
     */
    @Trace("根据userId查询用户微信信息")
    @Override
    public UmsUserWx getByUserId(Long userId) {
        return umsUserWxDao.getByUserId(userId);
    }

    /**
     * 根据unionId查询
     *
     * @param unionId 微信unionid
     * @return
     */
    @Trace("根据unionId查询用户微信信息")
    @Override
    public UmsUserWx getByUnionId(String unionId) {
        return umsUserWxDao.getByUnionId(unionId);
    }

    /**
     * 根据openId查询
     *
     * @param openId 微信openid
     * @return
     */
    @Trace("根据openId查询用户微信信息")
    @Override
    public UmsUserWx getByOpenId(String openId) {
        return umsUserWxDao.getByOpenId(openId);
    }

    /**
     * 根据sessionKey查询
     *
     * @param sessionKey 会话key
     * @return
     */
    @Trace("根据sessionKey查询用户微信信息")
    @Override
    public UmsUserWx getBySessionKey(String sessionKey) {
        return umsUserWxDao.getBySessionKey(sessionKey);
    }

    /**
     * 插入
     *
     * @param umsUserWx 参数
     * @return
     */
    @Trace("插入用户微信信息")
    @Override
    public int insert(UmsUserWx umsUserWx) {
        this.handleDataBeforeInsert(umsUserWx);
        return umsUserWxDao.insert(umsUserWx);
    }

    /**
     * 选择性插入
     *
     * @param umsUserWx 参数
     * @return
     */
    @Trace("选择性插入用户微信信息")
    @Override
    public int insertSelective(UmsUserWx umsUserWx) {
        this.handleDataBeforeInsert(umsUserWx);
        return umsUserWxDao.insertSelective(umsUserWx);
    }

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量插入用户微信信息")
    @Override
    public int batchInsert(List<UmsUserWx> list) {
        list.forEach(this::handleDataBeforeInsert);
        return umsUserWxDao.batchInsert(list);
    }

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量选择性插入用户微信信息")
    @Override
    public int batchInsertSelective(List<UmsUserWx> list) {
        list.forEach(this::handleDataBeforeInsert);
        return umsUserWxDao.batchInsertSelective(list);
    }

    /**
     * 插入前数据处理
     *
     * @param umsUserWx 用户微信信息
     */
    private void handleDataBeforeInsert(UmsUserWx umsUserWx) {

    }

    /**
     * 更新前数据处理
     *
     * @param umsUserWx 用户微信信息
     */
    private void handleDataBeforeUpdate(UmsUserWx umsUserWx) {

    }

    /**
     * 修改
     *
     * @param umsUserWx 参数
     * @return
     */
    @Trace("修改用户微信信息")
    @Override
    public int update(UmsUserWx umsUserWx) {
        this.handleDataBeforeUpdate(umsUserWx);
        return umsUserWxDao.update(umsUserWx);
    }

    /**
     * 选择性修改
     *
     * @param umsUserWx 参数
     * @return
     */
    @Trace("选择性修改用户微信信息")
    @Override
    public int updateSelective(UmsUserWx umsUserWx) {
        this.handleDataBeforeUpdate(umsUserWx);
        return umsUserWxDao.updateSelective(umsUserWx);
    }

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    @Trace("删除用户微信信息")
    @Override
    public int delete(Long id) {
        return umsUserWxDao.delete(id);
    }

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    @Trace("批量删除用户微信信息")
    @Override
    public int batchDelete(List<Long> list) {
        return umsUserWxDao.batchDelete(list);
    }

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param umsUserWx 参数
     * @return
     */
    @Trace("根据条件删除用户微信信息")
    @Override
    public int deleteByCondition(UmsUserWx umsUserWx) {
        return umsUserWxDao.deleteByCondition(umsUserWx);
    }

}
