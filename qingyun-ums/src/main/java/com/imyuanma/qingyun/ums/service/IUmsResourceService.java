package com.imyuanma.qingyun.ums.service;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.ums.model.UmsResource;
import com.imyuanma.qingyun.ums.model.param.BatchAddResourceParam;

import java.util.List;

/**
 * 资源服务
 *
 * @author YuanMaKeJi
 * @date 2022-10-05 21:06:53
 */
public interface IUmsResourceService {
    /**
     * 获取所有链接有效的资源数据
     *
     * @return
     */
    List<UmsResource> getAllValidHrefResource();

    /**
     * 查询资源树
     *
     * @param umsResource
     * @return
     */
    List<UmsResource> getTree(UmsResource umsResource);

    /**
     * 列表查询
     *
     * @param umsResource 查询条件
     * @return
     */
    List<UmsResource> getList(UmsResource umsResource);

    /**
     * 分页查询
     *
     * @param umsResource 查询条件
     * @param pageQuery   分页参数
     * @return
     */
    List<UmsResource> getPage(UmsResource umsResource, PageQuery pageQuery);

    /**
     * 统计数量
     *
     * @param umsResource 查询条件
     * @return
     */
    int count(UmsResource umsResource);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    UmsResource get(Long id);

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    List<UmsResource> getListByIds(List<Long> list);

    /**
     * code批量查询
     *
     * @param codes
     * @return
     */
    List<UmsResource> getListByCodes(List<String> codes);

    /**
     * 插入
     *
     * @param umsResource 参数
     * @return
     */
    int insert(UmsResource umsResource);

    /**
     * 选择性插入
     *
     * @param umsResource 参数
     * @return
     */
    int insertSelective(UmsResource umsResource);

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    int batchInsert(List<UmsResource> list);

    /**
     * 批量创建资源
     *
     * @param batchAddResourceParam
     * @return
     */
    void batchAddResource(BatchAddResourceParam batchAddResourceParam);

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    int batchInsertSelective(List<UmsResource> list);

    /**
     * 修改
     *
     * @param umsResource 参数
     * @return
     */
    int update(UmsResource umsResource);

    /**
     * 选择性修改
     *
     * @param umsResource 参数
     * @return
     */
    int updateSelective(UmsResource umsResource);

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    int delete(Long id);

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    int batchDelete(List<Long> list);

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param umsResource 参数
     * @return
     */
    int deleteByCondition(UmsResource umsResource);

    /**
     * 补全资源集合的parentCode属性
     *
     * @param list
     */
    void fillParentCodeByParentId(List<UmsResource> list);

    /**
     * 补全资源集合的parentId属性
     *
     * @param list
     */
    void fillParentIdByParentCode(List<UmsResource> list);

}
