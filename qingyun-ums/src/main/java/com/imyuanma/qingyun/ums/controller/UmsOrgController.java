package com.imyuanma.qingyun.ums.controller;

import com.imyuanma.qingyun.common.core.structure.tree.TreeBuilder;
import com.imyuanma.qingyun.common.factory.BaseDOBuilder;
import com.imyuanma.qingyun.common.model.request.WebRequest;
import com.imyuanma.qingyun.common.model.response.Page;
import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.ums.model.UmsOrg;
import com.imyuanma.qingyun.ums.model.enums.EUmsOrgTypeEnum;
import com.imyuanma.qingyun.ums.service.IUmsOrgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 组织机构web
 *
 * @author YuanMaKeJi
 * @date 2022-10-08 15:19:19
 */
@RestController
@RequestMapping(value = "/ums/umsOrg")
public class UmsOrgController {

    /**
     * 组织机构服务
     */
    @Autowired
    private IUmsOrgService umsOrgService;

    @Trace("查询组织结构树")
    @PostMapping("/getTree")
    public Result<List<UmsOrg>> getTree(@RequestBody WebRequest<UmsOrg> webRequest) {
        // 查询组织机构列表
        List<UmsOrg> orgList = umsOrgService.getList(webRequest.getBody());
        if (CollectionUtil.isNotEmpty(orgList)) {
            Collections.sort(orgList);
        }
        // 树构造
        return Result.success(TreeBuilder.buildTree(orgList));
    }

    @Trace("查询单位树")
    @PostMapping("/getCompanyTree")
    public Result<List<UmsOrg>> getCompanyTree(@RequestBody WebRequest<UmsOrg> webRequest) {
        UmsOrg query = webRequest.getBody() != null ? webRequest.getBody() : new UmsOrg();
        query.setType(EUmsOrgTypeEnum.CODE_10.getCode());
        // 查询单位列表
        List<UmsOrg> orgList = umsOrgService.getList(query);
        if (CollectionUtil.isNotEmpty(orgList)) {
            Collections.sort(orgList);
        }
        // 树构造
        return Result.success(TreeBuilder.buildTree(orgList));
    }

    /**
     * 列表查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("查询组织机构列表")
    @PostMapping("/getList")
    public Result<List<UmsOrg>> getList(@RequestBody WebRequest<UmsOrg> webRequest) {
        return Result.success(umsOrgService.getList(webRequest.getBody()));
    }

    /**
     * 分页查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("分页查询组织机构")
    @PostMapping("/getPage")
    public Page<List<UmsOrg>> getPage(@RequestBody WebRequest<UmsOrg> webRequest) {
        PageQuery pageQuery = webRequest.buildPageQuery();
        List<UmsOrg> list = umsOrgService.getPage(webRequest.getBody(), pageQuery);
        return Page.success(list, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("统计组织机构数量")
    @PostMapping("/count")
    public Result<Integer> count(@RequestBody WebRequest<UmsOrg> webRequest) {
        return Result.success(umsOrgService.count(webRequest.getBody()));
    }

    /**
     * 查询
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("查询组织机构")
    @PostMapping("/get")
    public Result<UmsOrg> get(@RequestBody WebRequest<Long> webRequest) {
        AssertUtil.notNull(webRequest.getBody());
        return Result.success(umsOrgService.get(webRequest.getBody()));
    }

    /**
     * 新增
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("新增组织机构")
    @PostMapping("/insert")
    public Result insert(@RequestBody WebRequest<UmsOrg> webRequest) {
        UmsOrg umsOrg = webRequest.getBody();
        AssertUtil.notNull(umsOrg);
        BaseDOBuilder.fillBaseDOForInsert(umsOrg);
        umsOrgService.insertSelective(umsOrg);
        return Result.success();
    }

    /**
     * 修改
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("修改组织机构")
    @PostMapping("/update")
    public Result update(@RequestBody WebRequest<UmsOrg> webRequest) {
        UmsOrg umsOrg = webRequest.getBody();
        AssertUtil.notNull(umsOrg);
        AssertUtil.notNull(umsOrg.getId());
        BaseDOBuilder.fillBaseDOForUpdate(umsOrg);
        umsOrgService.updateSelective(umsOrg);
        return Result.success();
    }

    /**
     * 删除
     *
     * @param webRequest 参数, 待删除主键,多个使用英文逗号拼接
     * @return
     */
    @Trace("删除组织机构")
    @PostMapping("/delete")
    public Result delete(@RequestBody WebRequest<String> webRequest) {
        String ids = webRequest.getBody();
        AssertUtil.notBlank(ids);
        if (ids.contains(",")) {
            umsOrgService.batchDelete(Arrays.stream(ids.split(",")).map(Long::valueOf).collect(Collectors.toList()));
        } else {
            umsOrgService.delete(Long.valueOf(ids));
        }
        return Result.success();
    }

}
