package com.imyuanma.qingyun.ums.controller;

import com.imyuanma.qingyun.common.model.request.WebRequest;
import com.imyuanma.qingyun.common.model.response.Page;
import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.ums.model.UmsLoginLog;
import com.imyuanma.qingyun.ums.service.IUmsLoginLogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 登录日志web
 *
 * @author YuanMaKeJi
 * @date 2023-04-08 15:16:30
 */
@RestController
@RequestMapping(value = "/ums/umsLoginLog")
public class UmsLoginLogController {

    private static final Logger logger = LoggerFactory.getLogger(UmsLoginLogController.class);

    /**
     * 登录日志服务
     */
    @Autowired
    private IUmsLoginLogService umsLoginLogService;

    /**
     * 列表查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("查询登录日志列表")
    @PostMapping("/getList")
    public Result<List<UmsLoginLog>> getList(@RequestBody WebRequest<UmsLoginLog> webRequest) {
        return Result.success(umsLoginLogService.getList(webRequest.getBody()));
    }

    /**
     * 分页查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("分页查询登录日志")
    @PostMapping("/getPage")
    public Page<List<UmsLoginLog>> getPage(@RequestBody WebRequest<UmsLoginLog> webRequest) {
        PageQuery pageQuery = webRequest.buildPageQuery();
        List<UmsLoginLog> list = umsLoginLogService.getPage(webRequest.getBody(), pageQuery);
        return Page.success(list, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("统计登录日志数量")
    @PostMapping("/count")
    public Result<Integer> count(@RequestBody WebRequest<UmsLoginLog> webRequest) {
        return Result.success(umsLoginLogService.count(webRequest.getBody()));
    }

    /**
     * 查询
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("查询登录日志")
    @PostMapping("/get")
    public Result<UmsLoginLog> get(@RequestBody WebRequest<Long> webRequest) {
        AssertUtil.notNull(webRequest.getBody());
        return Result.success(umsLoginLogService.get(webRequest.getBody()));
    }

    /**
     * 新增
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("新增登录日志")
    @PostMapping("/insert")
    public Result insert(@RequestBody WebRequest<UmsLoginLog> webRequest) {
        UmsLoginLog umsLoginLog = webRequest.getBody();
        AssertUtil.notNull(umsLoginLog);
        umsLoginLogService.insertSelective(umsLoginLog);
        return Result.success();
    }

    /**
     * 修改
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("修改登录日志")
    @PostMapping("/update")
    public Result update(@RequestBody WebRequest<UmsLoginLog> webRequest) {
        UmsLoginLog umsLoginLog = webRequest.getBody();
        AssertUtil.notNull(umsLoginLog);
        AssertUtil.notNull(umsLoginLog.getId());
        umsLoginLogService.updateSelective(umsLoginLog);
        return Result.success();
    }

    /**
     * 删除
     *
     * @param webRequest 参数, 待删除主键,多个使用英文逗号拼接
     * @return
     */
    @Trace("删除登录日志")
    @PostMapping("/delete")
    public Result delete(@RequestBody WebRequest<String> webRequest) {
        String ids = webRequest.getBody();
        AssertUtil.notBlank(ids);
        if (ids.contains(",")) {
            umsLoginLogService.batchDelete(Arrays.stream(ids.split(",")).map(Long::valueOf).collect(Collectors.toList()));
        } else {
            umsLoginLogService.delete(Long.valueOf(ids));
        }
        return Result.success();
    }



}
