package com.imyuanma.qingyun.ums.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 找回密码入参
 *
 * @author wangjy
 * @date 2023/01/02 11:09:36
 */
@Data
@ApiModel
public class FindPwdParam {
    /**
     * 账号
     */
    @ApiModelProperty(value = "账号,发起密码找回时只需要账号")
    private String account;
    /**
     * 密码找回的key
     */
    @ApiModelProperty(value = "密码找回的key,重置密码时需要")
    private String key;
    /**
     * 新密码
     */
    @ApiModelProperty(value = "密码找回的key,重置密码时需要")
    private String password;
}
