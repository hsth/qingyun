package com.imyuanma.qingyun.ums.controller;

import com.imyuanma.qingyun.common.model.request.WebRequest;
import com.imyuanma.qingyun.common.model.response.Page;
import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.ums.model.UmsRoleUser;
import com.imyuanma.qingyun.ums.service.IUmsRoleUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 角色用户关联表web
 *
 * @author YuanMaKeJi
 * @date 2023-03-26 15:11:39
 */
@RestController
@RequestMapping(value = "/ums/umsRoleUser")
public class UmsRoleUserController {

    private static final Logger logger = LoggerFactory.getLogger(UmsRoleUserController.class);

    /**
     * 角色用户关联表服务
     */
    @Autowired
    private IUmsRoleUserService umsRoleUserService;

    /**
     * 列表查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("查询角色用户关联表列表")
    @PostMapping("/getList")
    public Result<List<UmsRoleUser>> getList(@RequestBody WebRequest<UmsRoleUser> webRequest) {
        return Result.success(umsRoleUserService.getList(webRequest.getBody()));
    }

    /**
     * 分页查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("分页查询角色用户关联表")
    @PostMapping("/getPage")
    public Page<List<UmsRoleUser>> getPage(@RequestBody WebRequest<UmsRoleUser> webRequest) {
        PageQuery pageQuery = webRequest.buildPageQuery();
        List<UmsRoleUser> list = umsRoleUserService.getPage(webRequest.getBody(), pageQuery);
        return Page.success(list, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param webRequest 查询条件
     * @return
     */
    @Trace("统计角色用户关联表数量")
    @PostMapping("/count")
    public Result<Integer> count(@RequestBody WebRequest<UmsRoleUser> webRequest) {
        return Result.success(umsRoleUserService.count(webRequest.getBody()));
    }

    /**
     * 查询
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("查询角色用户关联表")
    @PostMapping("/get")
    public Result<UmsRoleUser> get(@RequestBody WebRequest<Long> webRequest) {
        AssertUtil.notNull(webRequest.getBody());
        return Result.success(umsRoleUserService.get(webRequest.getBody()));
    }

    /**
     * 新增
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("新增角色用户关联表")
    @PostMapping("/insert")
    public Result insert(@RequestBody WebRequest<UmsRoleUser> webRequest) {
        UmsRoleUser umsRoleUser = webRequest.getBody();
        AssertUtil.notNull(umsRoleUser);
        umsRoleUserService.insertSelective(umsRoleUser);
        return Result.success();
    }

    /**
     * 修改
     *
     * @param webRequest 参数
     * @return
     */
    @Trace("修改角色用户关联表")
    @PostMapping("/update")
    public Result update(@RequestBody WebRequest<UmsRoleUser> webRequest) {
        UmsRoleUser umsRoleUser = webRequest.getBody();
        AssertUtil.notNull(umsRoleUser);
        AssertUtil.notNull(umsRoleUser.getId());
        umsRoleUserService.updateSelective(umsRoleUser);
        return Result.success();
    }

    /**
     * 删除
     *
     * @param webRequest 参数, 待删除主键,多个使用英文逗号拼接
     * @return
     */
    @Trace("删除角色用户关联表")
    @PostMapping("/delete")
    public Result delete(@RequestBody WebRequest<String> webRequest) {
        String ids = webRequest.getBody();
        AssertUtil.notBlank(ids);
        if (ids.contains(",")) {
            umsRoleUserService.batchDelete(Arrays.stream(ids.split(",")).map(Long::valueOf).collect(Collectors.toList()));
        } else {
            umsRoleUserService.delete(Long.valueOf(ids));
        }
        return Result.success();
    }



}
