package com.imyuanma.qingyun.ums.model.seccode;

import java.awt.image.BufferedImage;
import java.util.Date;

/**
 * 验证码图片对象
 *
 * @author wangjy
 * @date 2022/12/19 23:33:31
 */
public class SecurityCodeImage {
    private String code;//验证码
    private BufferedImage image;//图片
    private Date createTime;//创建时间

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
