package com.imyuanma.qingyun.ums.service;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.ums.model.UmsRoleUser;

import java.util.List;

/**
 * 角色用户关联表服务
 *
 * @author YuanMaKeJi
 * @date 2023-03-26 15:11:39
 */
public interface IUmsRoleUserService {
    /**
     * 绑定用户
     *
     * @param roleId  角色id
     * @param userIds 追加绑定的用户
     */
    void bindUser(Long roleId, List<Long> userIds);

    /**
     * 为用户分配角色
     *
     * @param userId  用户id
     * @param roleIds 角色集合
     */
    void bindRole(Long userId, List<Long> roleIds);

    /**
     * 列表查询
     *
     * @param umsRoleUser 查询条件
     * @return
     */
    List<UmsRoleUser> getList(UmsRoleUser umsRoleUser);

    /**
     * 分页查询
     *
     * @param umsRoleUser 查询条件
     * @param pageQuery   分页参数
     * @return
     */
    List<UmsRoleUser> getPage(UmsRoleUser umsRoleUser, PageQuery pageQuery);

    /**
     * 统计数量
     *
     * @param umsRoleUser 查询条件
     * @return
     */
    int count(UmsRoleUser umsRoleUser);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    UmsRoleUser get(Long id);

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    List<UmsRoleUser> getListByIds(List<Long> list);

    /**
     * 根据roleId查询
     *
     * @param roleId 角色id
     * @return
     */
    List<UmsRoleUser> getListByRoleId(Long roleId);

    /**
     * 根据userId查询
     *
     * @param userId 用户id
     * @return
     */
    List<UmsRoleUser> getListByUserId(Long userId);

    /**
     * 插入
     *
     * @param umsRoleUser 参数
     * @return
     */
    int insert(UmsRoleUser umsRoleUser);

    /**
     * 选择性插入
     *
     * @param umsRoleUser 参数
     * @return
     */
    int insertSelective(UmsRoleUser umsRoleUser);

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    int batchInsert(List<UmsRoleUser> list);

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    int batchInsertSelective(List<UmsRoleUser> list);

    /**
     * 修改
     *
     * @param umsRoleUser 参数
     * @return
     */
    int update(UmsRoleUser umsRoleUser);

    /**
     * 选择性修改
     *
     * @param umsRoleUser 参数
     * @return
     */
    int updateSelective(UmsRoleUser umsRoleUser);

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    int delete(Long id);

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    int batchDelete(List<Long> list);

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param umsRoleUser 参数
     * @return
     */
    int deleteByCondition(UmsRoleUser umsRoleUser);

}
