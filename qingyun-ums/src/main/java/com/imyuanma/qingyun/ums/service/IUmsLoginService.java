package com.imyuanma.qingyun.ums.service;

import com.imyuanma.qingyun.interfaces.ums.model.LoginSuccess;
import com.imyuanma.qingyun.ums.model.MiniLoginParam;

/**
 * 登录服务
 *
 * @author wangjy
 * @date 2023/09/24 20:34:05
 */
public interface IUmsLoginService {
    /**
     * 小程序登录
     *
     * @param miniLoginParam
     * @return
     */
    LoginSuccess login4mini(MiniLoginParam miniLoginParam);
}
