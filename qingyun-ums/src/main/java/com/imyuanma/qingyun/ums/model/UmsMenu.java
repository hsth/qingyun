package com.imyuanma.qingyun.ums.model;

import com.imyuanma.qingyun.common.core.structure.tree.ITreeNode;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 菜单
 *
 * @author wangjy
 * @date 2022/10/05 21:12:18
 */
@Data
public class UmsMenu implements ITreeNode<Long>, Comparable<UmsMenu> {

    /**
     * 主键
     */
    private Long id;

    /**
     * 资源编号
     */
    private String code;

    /**
     * 图标
     */
    private String icon;

    /**
     * 资源名称
     */
    private String name;

    /**
     * 上级id
     */
    private Long parentId;

    /**
     * 链接
     */
    private String href;

    /**
     * 类型,10页面,20按钮,30操作,40逻辑(一切皆逻辑)
     */
    private Integer type;

    /**
     * 备注
     */
    private String remark;

    /**
     * 标签
     */
    private String tag;

    /**
     * 数据顺序
     */
    private Long dataSequence;

    /**
     * 子菜单
     */
    private List<UmsMenu> children;

    /**
     * 对象转换
     *
     * @param umsResource
     * @return
     */
    public static UmsMenu convertFrom(UmsResource umsResource) {
        UmsMenu umsMenu = new UmsMenu();
        umsMenu.setId(umsResource.getId());
        umsMenu.setCode(umsResource.getCode());
        umsMenu.setIcon(umsResource.getIcon());
        umsMenu.setName(umsResource.getName());
        umsMenu.setParentId(umsResource.getParentId());
        umsMenu.setHref(umsResource.getHref());
        umsMenu.setType(umsResource.getType());
        umsMenu.setRemark(umsResource.getRemark());
        umsMenu.setTag(umsResource.getTag());
        umsMenu.setDataSequence(umsResource.getDataSequence());
        return umsMenu;
    }

    /**
     * 当前节点id
     *
     * @return
     */
    @Override
    public Long nodeId() {
        return id;
    }

    /**
     * 父节点id
     *
     * @return
     */
    @Override
    public Long parentNodeId() {
        return parentId;
    }

    /**
     * 添加子节点
     *
     * @param node 节点对象
     */
    @Override
    public void appendChildren(ITreeNode<Long> node) {
        if (children == null) {
            children = new ArrayList<>();
        }
        children.add((UmsMenu) node);
    }

    /**
     * 比较
     *
     * @param o
     * @return
     */
    @Override
    public int compareTo(UmsMenu o) {
        long s1 = this.dataSequence != null ? this.dataSequence : 0L;
        long s2 = o.dataSequence != null ? o.dataSequence : 0L;
        if (s1 == s2) {
            long i1 = this.id != null ? this.id : 0L;
            long i2 = o.id != null ? o.id : 0L;
            return Long.compare(i1, i2);
        } else {
            return Long.compare(s1, s2);
        }
    }
}
