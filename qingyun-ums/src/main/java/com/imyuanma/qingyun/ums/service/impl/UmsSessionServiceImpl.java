package com.imyuanma.qingyun.ums.service.impl;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.util.DateUtil;
import com.imyuanma.qingyun.common.util.IPUtil;
import com.imyuanma.qingyun.common.util.StringUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.interfaces.ums.model.LoginUserDTO;
import com.imyuanma.qingyun.interfaces.ums.model.TerminalDTO;
import com.imyuanma.qingyun.ums.dao.IUmsSessionDao;
import com.imyuanma.qingyun.ums.model.UmsSession;
import com.imyuanma.qingyun.ums.model.enums.EUmsTokenStatusEnum;
import com.imyuanma.qingyun.ums.service.IUmsSessionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 登录会话服务
 *
 * @author YuanMaKeJi
 * @date 2022-07-16 11:43:58
 */
@Slf4j
@Service
public class UmsSessionServiceImpl implements IUmsSessionService {

    /**
     * 登录会话dao
     */
    @Autowired
    private IUmsSessionDao umsSessionDao;

    /**
     * 列表查询
     *
     * @param umsSession 查询条件
     * @return
     */
    @Override
    public List<UmsSession> getList(UmsSession umsSession) {
        if (umsSession != null && EUmsTokenStatusEnum.EXPIRE.getCode().equals(umsSession.getTokenStatus())) {
            umsSession.setTokenStatus(null);
            umsSession.setExpireTime2(new Date());
        }
        return umsSessionDao.getList(umsSession);
    }

    /**
     * 分页查询
     *
     * @param umsSession 查询条件
     * @param pageQuery  分页参数
     * @return
     */
    @Override
    public List<UmsSession> getPage(UmsSession umsSession, PageQuery pageQuery) {
        if (umsSession != null && EUmsTokenStatusEnum.EXPIRE.getCode().equals(umsSession.getTokenStatus())) {
            umsSession.setTokenStatus(null);
            umsSession.setExpireTime2(new Date());
        }
        return umsSessionDao.getList(umsSession, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param umsSession 查询条件
     * @return
     */
    @Override
    public int count(UmsSession umsSession) {
        if (umsSession != null && EUmsTokenStatusEnum.EXPIRE.getCode().equals(umsSession.getTokenStatus())) {
            umsSession.setTokenStatus(null);
            umsSession.setExpireTime2(new Date());
        }
        return umsSessionDao.count(umsSession);
    }

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    @Override
    public UmsSession get(Long id) {
        return umsSessionDao.get(id);
    }

    /**
     * 根据token查询会话
     *
     * @param token 会话token
     * @return
     */
    @Override
    public UmsSession getByToken(String token) {
        return umsSessionDao.getByToken(token);
    }

    /**
     * 插入
     *
     * @param umsSession 参数
     * @return
     */
    @Override
    public int insert(UmsSession umsSession) {
        return umsSessionDao.insert(umsSession);
    }

    /**
     * 选择性插入
     *
     * @param umsSession 参数
     * @return
     */
    @Override
    public int insertSelective(UmsSession umsSession) {
        return umsSessionDao.insertSelective(umsSession);
    }

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    @Override
    public int batchInsert(List<UmsSession> list) {
        return umsSessionDao.batchInsert(list);
    }

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    @Override
    public int batchInsertSelective(List<UmsSession> list) {
        return umsSessionDao.batchInsertSelective(list);
    }

    /**
     * 修改
     *
     * @param umsSession 参数
     * @return
     */
    @Override
    public int update(UmsSession umsSession) {
        return umsSessionDao.update(umsSession);
    }

    /**
     * 选择性修改
     *
     * @param umsSession 参数
     * @return
     */
    @Override
    public int updateSelective(UmsSession umsSession) {
        return umsSessionDao.updateSelective(umsSession);
    }

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    @Override
    public int delete(Long id) {
        return umsSessionDao.delete(id);
    }

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    @Override
    public int batchDelete(List<Long> list) {
        return umsSessionDao.batchDelete(list);
    }

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param umsSession 参数
     * @return
     */
    @Override
    public int deleteByCondition(UmsSession umsSession) {
        if (umsSession != null && EUmsTokenStatusEnum.EXPIRE.getCode().equals(umsSession.getTokenStatus())) {
            umsSession.setTokenStatus(null);
            umsSession.setExpireTime2(new Date());
        }
        return umsSessionDao.deleteByCondition(umsSession);
    }

    /**
     * 下线会话
     *
     * @param token 令牌
     * @return
     */
    @Override
    public int offlineSession(String token) {
        UmsSession umsSession = new UmsSession();
        umsSession.setToken(token);
        umsSession.setTokenStatus(EUmsTokenStatusEnum.DISABLE.getCode());
        return umsSessionDao.offlineSession(umsSession);
    }

    /**
     * 创建登录会话
     *
     * @param loginUserDTO 登录用户
     * @param terminalDTO  终端信息
     * @return 当前登录会话token
     */
    @Trace("创建登录会话")
    @Override
    public String createLoginSession(LoginUserDTO loginUserDTO, TerminalDTO terminalDTO) {
        UmsSession umsSession = this.buildUmsSession(loginUserDTO, terminalDTO);
        // 插入会话
        this.insertSelective(umsSession);
        return umsSession.getToken();
    }

    /**
     * 构造会话对象
     *
     * @param loginUserDTO
     * @param terminalDTO
     * @return
     */
    private UmsSession buildUmsSession(LoginUserDTO loginUserDTO, TerminalDTO terminalDTO) {
        UmsSession umsSession = new UmsSession();
        umsSession.setUserId(loginUserDTO.getUserId());
        umsSession.setToken(StringUtil.getUUID());
        umsSession.setSignTime(new Date());
        umsSession.setExpireTime(DateUtil.afterSeconds(umsSession.getSignTime(), 60 * 60 * 24));
        umsSession.setTokenStatus(EUmsTokenStatusEnum.ENABLE.getCode());
        umsSession.setDeviceAddr(terminalDTO.getDeviceAddr());
        umsSession.setDeviceType(terminalDTO.getDeviceType());
        umsSession.setDeviceVersion(terminalDTO.getDeviceVersion());
        umsSession.setClientIp(terminalDTO.getClientIp());
        umsSession.setServerIp(IPUtil.getLocalIp());
        umsSession.setCreateTime(new Date());
        umsSession.setUpdateTime(new Date());
        umsSession.setCreateUserId(loginUserDTO.getUserId());
        umsSession.setUpdateUserId(loginUserDTO.getUserId());
        return umsSession;
    }

}
