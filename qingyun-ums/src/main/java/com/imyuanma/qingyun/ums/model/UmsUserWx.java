package com.imyuanma.qingyun.ums.model;

import lombok.Data;
import java.util.Date;
import com.imyuanma.qingyun.interfaces.common.model.BaseDO;

/**
 * 用户微信信息实体类
 *
 * @author YuanMaKeJi
 * @date 2023-11-05 22:19:41
 */
@Data
public class UmsUserWx extends BaseDO {

    /**
     * 主键
     */
    private Long id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 性别
     */
    private Integer sex;

    /**
     * 头像
     */
    private String header;

    /**
     * 微信unionid
     */
    private String unionId;

    /**
     * 微信openid
     */
    private String openId;

    /**
     * 会话key
     */
    private String sessionKey;



}