package com.imyuanma.qingyun.ums.service.impl;

import com.imyuanma.qingyun.common.exception.Exceptions;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.common.util.ClientInfoUtil;
import com.imyuanma.qingyun.common.util.IPUtil;
import com.imyuanma.qingyun.common.util.JsonUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.interfaces.ums.model.LoginLogDTO;
import com.imyuanma.qingyun.interfaces.ums.model.LoginSuccess;
import com.imyuanma.qingyun.interfaces.ums.model.LoginUserDTO;
import com.imyuanma.qingyun.interfaces.ums.model.TerminalDTO;
import com.imyuanma.qingyun.interfaces.ums.model.enums.EUmsLoginDeviceTypeEnum;
import com.imyuanma.qingyun.ums.model.MiniLoginParam;
import com.imyuanma.qingyun.ums.model.UmsUser;
import com.imyuanma.qingyun.ums.model.UmsUserWx;
import com.imyuanma.qingyun.ums.model.enums.EUmsUserStatusEnum;
import com.imyuanma.qingyun.ums.model.wechat.WxCode2SessionResponse;
import com.imyuanma.qingyun.ums.rpc.IUmsWxLoginRpc;
import com.imyuanma.qingyun.ums.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 登录服务
 *
 * @author wangjy
 * @date 2023/09/29 11:02:55
 */
@Slf4j
@Service
public class UmsLoginServiceImpl implements IUmsLoginService {
    @Autowired
    private IUmsWxLoginRpc umsWxLoginRpc;
    @Autowired
    private IUmsUserWxService umsUserWxService;
    @Autowired
    private IUmsUserService umsUserService;
    @Autowired
    private IUmsSessionService umsSessionService;
    @Autowired
    private IUmsLoginLogService umsLoginLogService;

    /**
     * 小程序登录
     *
     * @param miniLoginParam
     * @return
     */
    @Trace("小程序登录")
    @Override
    public LoginSuccess login4mini(MiniLoginParam miniLoginParam) {
        try {
            // 调用腾讯用code换取小程序登录信息
            WxCode2SessionResponse wxCode2SessionResponse = umsWxLoginRpc.code2Session(miniLoginParam.getCode());
            AssertUtil.notNull(wxCode2SessionResponse);
            AssertUtil.isTrue(wxCode2SessionResponse.success(), "获取微信登录信息失败");
            // openid判断是否已有此微信用户
            UmsUserWx bindUserWx = umsUserWxService.getByOpenId(wxCode2SessionResponse.getOpenid());
            // 关联的平台用户, 可能为空
            UmsUser platUser = null;
            // 微信新用户则插入微信用户信息, 并同时插入一条平台用户信息并绑定关系 (或者有手机号也可以通过手机号尝试绑定平台用户,暂时直接创建新用户)
            if (bindUserWx == null) {
                // 插入平台用户
                platUser = this.buildPlatUserByUmsUserWx4Mini(wxCode2SessionResponse, miniLoginParam);
                umsUserService.insertSelective(platUser);

                // 初始化微信用户对象
                bindUserWx = this.buildWxUser4Mini(wxCode2SessionResponse, miniLoginParam, platUser.getId());
                // 插入微信用户
                umsUserWxService.insertSelective(bindUserWx);

            } else {
                if (bindUserWx.getUserId() != null) {
                    platUser = umsUserService.get(bindUserWx.getUserId());
                } else {
                    throw Exceptions.baseException("微信号暂未绑定平台账号");
                }
            }

            // 为平台用户创建会话
            TerminalDTO terminalDTO = this.buildTerminalDTO(miniLoginParam);
            LoginUserDTO loginUserDTO = platUser.convert();
            String token = umsSessionService.createLoginSession(loginUserDTO, terminalDTO);

            // 写入登录日志
            umsLoginLogService.writeLoginSuccessLog(loginUserDTO, terminalDTO);

            // 返回结果
            return LoginSuccess.success(platUser.getAccount(), token, null);
        } catch (Throwable t) {
            log.error("[小程序登录] 发生异常,入参={}", JsonUtil.toJson(miniLoginParam), t);
            Exceptions.tryCatch(() -> {
                // 写入登录失败日志
                LoginLogDTO loginLog = LoginLogDTO.error(miniLoginParam.getCode(), miniLoginParam.getClientInfo().getIp(), IPUtil.getLocalIp(), EUmsLoginDeviceTypeEnum.getByText(miniLoginParam.getClientInfo().getDeviceType()), t.getMessage());
                umsLoginLogService.writeLoginErrorLog(loginLog);
            }, null);
            throw t;
        }
    }

    /**
     * 构造登录终端对象
     *
     * @param miniLoginParam
     * @return
     */
    private TerminalDTO buildTerminalDTO(MiniLoginParam miniLoginParam) {
        TerminalDTO terminalDTO = new TerminalDTO();
        terminalDTO.setDeviceAddr(miniLoginParam.getClientInfo().getIp());
        terminalDTO.setDeviceType(EUmsLoginDeviceTypeEnum.getCodeByText(miniLoginParam.getClientInfo().getDeviceType()));
        terminalDTO.setDeviceVersion(miniLoginParam.getClientInfo().getAppVersion());
        terminalDTO.setClientIp(miniLoginParam.getClientInfo().getIp());
        return terminalDTO;
    }

    /**
     * 构造平台用户对象
     *
     * @param wxCode2SessionResponse
     * @param miniLoginParam
     * @return
     */
    private UmsUser buildPlatUserByUmsUserWx4Mini(WxCode2SessionResponse wxCode2SessionResponse, MiniLoginParam miniLoginParam) {
        UmsUser umsUser = new UmsUser();
        umsUser.setName(miniLoginParam.getNickName());
        umsUser.setAccount(wxCode2SessionResponse.getOpenid());
        umsUser.setSex(miniLoginParam.getSex());
        umsUser.setStatus(EUmsUserStatusEnum.ENABLE.getCode());
        umsUser.setHeader(miniLoginParam.getAvatar());
        umsUser.setThisLoginTime(new Date());
        umsUser.setThisLoginTime2(new Date());
        return umsUser;

    }

    /**
     * 构造微信用户对象
     *
     * @param wxCode2SessionResponse
     * @param miniLoginParam
     * @param userId
     * @return
     */
    private UmsUserWx buildWxUser4Mini(WxCode2SessionResponse wxCode2SessionResponse, MiniLoginParam miniLoginParam, Long userId) {
        UmsUserWx umsUserWx = new UmsUserWx();
        umsUserWx.setUserId(userId);
        umsUserWx.setNickname(miniLoginParam.getNickName());
        umsUserWx.setSex(miniLoginParam.getSex());
        umsUserWx.setHeader(miniLoginParam.getAvatar());
        umsUserWx.setUnionId(wxCode2SessionResponse.getUnionid());
        umsUserWx.setOpenId(wxCode2SessionResponse.getOpenid());
        umsUserWx.setSessionKey(wxCode2SessionResponse.getSession_key());
        return umsUserWx;

    }
}
