package com.imyuanma.qingyun.ums.rpc;

import com.imyuanma.qingyun.ums.model.wechat.WxCode2SessionResponse;

/**
 * 微信登录
 *
 * @author wangjy
 * @date 2023/09/29 11:57:44
 */
public interface IUmsWxLoginRpc {
    /**
     * code换取微信用户信息
     *
     * @param code 前端传入的从微信获取的code
     * @return
     */
    WxCode2SessionResponse code2Session(String code);
}
