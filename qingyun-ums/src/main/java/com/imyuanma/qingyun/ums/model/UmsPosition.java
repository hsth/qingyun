package com.imyuanma.qingyun.ums.model;

import lombok.Data;
import java.util.Date;
import com.imyuanma.qingyun.interfaces.common.model.BaseDO;
import com.imyuanma.qingyun.common.core.excel.ExcelColumn;

/**
 * 岗位表实体类
 *
 * @author YuanMaKeJi
 * @date 2023-01-15 19:06:57
 */
@Data
public class UmsPosition extends BaseDO {

    /**
     * 主键
     */
    private Long id;

    /**
     * 岗位编号
     */
    @ExcelColumn(value = "岗位编号", order = 20)
    private String code;

    /**
     * 岗位名称
     */
    @ExcelColumn(value = "岗位名称", order = 30)
    private String name;

    /**
     * 岗位类型
     */
    @ExcelColumn(value = "岗位类型", order = 40)
    private Integer type;

    /**
     * 备注
     */
    @ExcelColumn(value = "备注", order = 50)
    private String remark;



}