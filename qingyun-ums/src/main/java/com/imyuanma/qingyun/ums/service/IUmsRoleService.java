package com.imyuanma.qingyun.ums.service;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.ums.model.UmsRole;

import java.util.List;

/**
 * 角色信息服务
 *
 * @author YuanMaKeJi
 * @date 2023-03-25 13:23:39
 */
public interface IUmsRoleService {

    /**
     * 列表查询
     *
     * @param umsRole 查询条件
     * @return
     */
    List<UmsRole> getList(UmsRole umsRole);

    /**
     * 分页查询
     *
     * @param umsRole 查询条件
     * @param pageQuery 分页参数
     * @return
     */
    List<UmsRole> getPage(UmsRole umsRole, PageQuery pageQuery);

    /**
     * 统计数量
     *
     * @param umsRole 查询条件
     * @return
     */
    int count(UmsRole umsRole);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    UmsRole get(Long id);

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    List<UmsRole> getListByIds(List<Long> list);

    /**
     * 根据code查询
     *
     * @param code 角色编号
     * @return
     */
    UmsRole getByCode(String code);

    /**
     * 根据companyId查询
     *
     * @param companyId 所属单位id
     * @return
     */
    List<UmsRole> getListByCompanyId(Long companyId);

    /**
     * 根据category查询
     *
     * @param category 角色类别
     * @return
     */
    List<UmsRole> getListByCategory(String category);

    /**
     * 根据openType查询
     *
     * @param openType 公开类型,10不公开,20向直属下级单位公开,30向所有下级单位公开,40向部分下级单位公开
     * @return
     */
    List<UmsRole> getListByOpenType(Integer openType);

    /**
     * 根据dataPermissionId查询
     *
     * @param dataPermissionId 数据权限id
     * @return
     */
    List<UmsRole> getListByDataPermissionId(Long dataPermissionId);

    /**
     * 插入
     *
     * @param umsRole 参数
     * @return
     */
    int insert(UmsRole umsRole);

    /**
     * 选择性插入
     *
     * @param umsRole 参数
     * @return
     */
    int insertSelective(UmsRole umsRole);

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    int batchInsert(List<UmsRole> list);

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    int batchInsertSelective(List<UmsRole> list);

    /**
     * 修改
     *
     * @param umsRole 参数
     * @return
     */
    int update(UmsRole umsRole);

    /**
     * 选择性修改
     *
     * @param umsRole 参数
     * @return
     */
    int updateSelective(UmsRole umsRole);

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    int delete(Long id);

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    int batchDelete(List<Long> list);

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param umsRole 参数
     * @return
     */
    int deleteByCondition(UmsRole umsRole);

}
