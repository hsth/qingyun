package com.imyuanma.qingyun.ums.service.impl;

import com.imyuanma.qingyun.common.core.concurrent.QingYunThreadFactory;
import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.common.util.StringUtil;
import com.imyuanma.qingyun.ums.model.UmsResource;
import com.imyuanma.qingyun.ums.model.UmsRoleUser;
import com.imyuanma.qingyun.ums.service.IUmsAuthService;
import com.imyuanma.qingyun.ums.service.IUmsResourceService;
import com.imyuanma.qingyun.ums.service.IUmsRoleResourceService;
import com.imyuanma.qingyun.ums.service.IUmsRoleUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.RouteMatcher;
import org.springframework.web.util.pattern.PathPatternParser;
import org.springframework.web.util.pattern.PathPatternRouteMatcher;

import java.util.*;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 权限服务
 *
 * @author wangjy
 * @date 2023/06/08 23:57:56
 */
@Service
public class UmsAuthServiceImpl implements IUmsAuthService, InitializingBean {

    private static final Logger logger = LoggerFactory.getLogger(UmsAuthServiceImpl.class);
    /**
     * 路径匹配器
     */
    private static final PathPatternRouteMatcher PATH_MATCHER = new PathPatternRouteMatcher(new PathPatternParser());
    /**
     * 常规路径的uri资源
     */
    private Map<String, List<UmsResource>> URI_NORM_PATH_MAP = new HashMap<>();
    /**
     * 通配符路径的uri资源(带*的,或者带{}的)
     */
    private Map<String, List<UmsResource>> URI_WILDCARD_PATH_MAP = new HashMap<>();

    /**
     * 资源服务
     */
    @Autowired
    private IUmsResourceService umsResourceService;

    /**
     * 角色资源关联服务
     */
    @Autowired
    private IUmsRoleResourceService roleResourceService;
    /**
     * 角色用户关联服务
     */
    @Autowired
    private IUmsRoleUserService roleUserService;

    @Override
    public void afterPropertiesSet() throws Exception {
        new ScheduledThreadPoolExecutor(1, new QingYunThreadFactory("ums-auth-cache")).scheduleWithFixedDelay(this::refreshCache, 0, 5, TimeUnit.MINUTES);
    }

    /**
     * 刷新本地缓存
     */
    private void refreshCache() {
        try {
            // 查询所有资源链接有效的资源
            List<UmsResource> resourceList = umsResourceService.getAllValidHrefResource();

            URI_NORM_PATH_MAP.clear();
            URI_WILDCARD_PATH_MAP.clear();
            if (CollectionUtil.isNotEmpty(resourceList)) {
                resourceList.stream()
                        .filter(item -> StringUtil.isNotBlank(item.getHref()))
                        .forEach(item -> {
                            if (item.getHref().contains("*") || item.getHref().contains("{")) {
                                URI_WILDCARD_PATH_MAP.computeIfAbsent(item.getHref(), key -> new ArrayList<>()).add(item);
                            } else {
                                URI_NORM_PATH_MAP.computeIfAbsent(item.getHref(), key -> new ArrayList<>()).add(item);
                            }
                        });
            }
        } catch (Throwable t) {
            logger.error("刷新URI权限本地缓存异常", t);
        }

    }

    /**
     * 校验uri权限
     *
     * @param userId 用户id
     * @param uri    uri路径
     * @return
     */
    @Override
    public boolean verifyUri(Long userId, String uri) {
        // uri对应的资源配置
        List<UmsResource> resourceList = this.findUriResource(uri);
        if (CollectionUtil.isEmpty(resourceList)) {
            logger.debug("[校验uri权限] 验证通过:当前请求uri在资源配置中不存在,不验证uri权限,uri:{}", uri);
            return true;
        }
        // 用户是否有该uri权限
        List<Long> ownedResourceIdList = this.getUserOwnedResourceIdList(userId);
        // 判断用户是否拥有uri对应的资源
        if (CollectionUtil.isNotEmpty(ownedResourceIdList)) {
            Set<Long> owned = new HashSet<>(ownedResourceIdList);
            for (UmsResource resource : resourceList) {
                if (owned.contains(resource.getId())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 获取用户拥有的资源id集合
     *
     * @param userId 用户id
     * @return
     */
    @Override
    public List<Long> getUserOwnedResourceIdList(Long userId) {
        // 查询用户关联的角色
        List<UmsRoleUser> roleList = roleUserService.getListByUserId(userId);
        if (CollectionUtil.isNotEmpty(roleList)) {
            List<Long> roleIdList = roleList.stream().map(UmsRoleUser::getRoleId).distinct().collect(Collectors.toList());
            // 查询角色关联的资源
            return roleResourceService.getResourceIdListByRoleIdList(roleIdList);
        }
        return null;
    }

    /**
     * 获取用户拥有的资源集合
     *
     * @param userId 用户id
     * @return
     */
    @Override
    public List<UmsResource> getUserOwnedResourceList(Long userId) {
        List<Long> list = this.getUserOwnedResourceIdList(userId);
        if (CollectionUtil.isNotEmpty(list)) {
            return umsResourceService.getListByIds(list);
        }
        return null;
    }

    /**
     * 查找uri对应的资源
     *
     * @param uri
     * @return
     */
    private List<UmsResource> findUriResource(String uri) {
        List<UmsResource> list = new ArrayList<>();
        // 匹配常规uri
        List<UmsResource> normList = URI_NORM_PATH_MAP.get(uri);
        if (CollectionUtil.isNotEmpty(normList)) {
            list.addAll(normList);
        }
        // 匹配通配符类型的uri
        for (Map.Entry<String, List<UmsResource>> entry : URI_WILDCARD_PATH_MAP.entrySet()) {
            RouteMatcher.Route route = PATH_MATCHER.parseRoute(uri);
            if (PATH_MATCHER.match(entry.getKey(), route)) {
                list.addAll(entry.getValue());
            }
        }
        return list;
    }


}
