package com.imyuanma.qingyun.ums.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 登录安全验证
 * @author wangjiyu@imdada.cn
 * @create 2018/12/14
 */
public class SsoSecure implements Serializable {
    private String sessionId;//会话id
    private String securityCode;//验证码
    private Date securityCodeCreateTime;//验证码创建(刷新)时间
    private Date securedTime;//安全验证时间

    @Override
    public String toString() {
        return "SsoSecure{" +
                "sessionId='" + sessionId + '\'' +
                ", securityCode='" + securityCode + '\'' +
                ", securityCodeCreateTime=" + securityCodeCreateTime +
                ", securedTime=" + securedTime +
                '}';
    }

    /**
     * 清空验证码
     */
    public void clear() {
        this.setSecurityCode(null);
        this.setSecurityCodeCreateTime(null);
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    public Date getSecurityCodeCreateTime() {
        return securityCodeCreateTime;
    }

    public void setSecurityCodeCreateTime(Date securityCodeCreateTime) {
        this.securityCodeCreateTime = securityCodeCreateTime;
    }

    public Date getSecuredTime() {
        return securedTime;
    }

    public void setSecuredTime(Date securedTime) {
        this.securedTime = securedTime;
    }
}
