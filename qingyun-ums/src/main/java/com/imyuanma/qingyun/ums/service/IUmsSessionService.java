package com.imyuanma.qingyun.ums.service;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.interfaces.ums.model.LoginUserDTO;
import com.imyuanma.qingyun.interfaces.ums.model.TerminalDTO;
import com.imyuanma.qingyun.ums.model.UmsSession;

import java.util.List;

/**
 * 登录会话服务
 *
 * @author YuanMaKeJi
 * @date 2022-07-16 11:43:58
 */
public interface IUmsSessionService {

    /**
     * 列表查询
     *
     * @param umsSession 查询条件
     * @return
     */
    List<UmsSession> getList(UmsSession umsSession);

    /**
     * 分页查询
     *
     * @param umsSession 查询条件
     * @param pageQuery  分页参数
     * @return
     */
    List<UmsSession> getPage(UmsSession umsSession, PageQuery pageQuery);

    /**
     * 统计数量
     *
     * @param umsSession 查询条件
     * @return
     */
    int count(UmsSession umsSession);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    UmsSession get(Long id);

    /**
     * 根据token查询会话
     *
     * @param token 会话token
     * @return
     */
    UmsSession getByToken(String token);

    /**
     * 插入
     *
     * @param umsSession 参数
     * @return
     */
    int insert(UmsSession umsSession);

    /**
     * 选择性插入
     *
     * @param umsSession 参数
     * @return
     */
    int insertSelective(UmsSession umsSession);

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    int batchInsert(List<UmsSession> list);

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    int batchInsertSelective(List<UmsSession> list);

    /**
     * 修改
     *
     * @param umsSession 参数
     * @return
     */
    int update(UmsSession umsSession);

    /**
     * 选择性修改
     *
     * @param umsSession 参数
     * @return
     */
    int updateSelective(UmsSession umsSession);

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    int delete(Long id);

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    int batchDelete(List<Long> list);

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param umsSession 参数
     * @return
     */
    int deleteByCondition(UmsSession umsSession);

    /**
     * 下线会话
     *
     * @param token 令牌
     * @return
     */
    int offlineSession(String token);

    /**
     * 创建登录会话
     *
     * @param loginUserDTO 登录用户
     * @param terminalDTO  终端信息
     * @return 当前登录会话token
     */
    String createLoginSession(LoginUserDTO loginUserDTO, TerminalDTO terminalDTO);

}
