package com.imyuanma.qingyun.ums.service.impl;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.ums.dao.IUmsRoleUserDao;
import com.imyuanma.qingyun.ums.model.UmsRoleUser;
import com.imyuanma.qingyun.ums.service.IUmsRoleUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 角色用户关联表服务
 *
 * @author YuanMaKeJi
 * @date 2023-03-26 15:11:39
 */
@Slf4j
@Service
public class UmsRoleUserServiceImpl implements IUmsRoleUserService {

    /**
     * 角色用户关联表dao
     */
    @Autowired
    private IUmsRoleUserDao umsRoleUserDao;

    /**
     * 绑定用户
     *
     * @param roleId  角色id
     * @param userIds 追加绑定的用户
     */
    @Trace("绑定用户")
    @Override
    public void bindUser(Long roleId, List<Long> userIds) {
        List<UmsRoleUser> list = this.getListByRoleId(roleId);
        Set<Long> bindUserIds = new HashSet<>(userIds);
        if (CollectionUtil.isNotEmpty(list)) {
            // 已绑定用户
            Set<Long> binded = list.stream().map(UmsRoleUser::getUserId).collect(Collectors.toSet());
            // 排除已绑定用户id
            for (Long userId : binded) {
                bindUserIds.remove(userId);
            }
        }
        if (CollectionUtil.isNotEmpty(bindUserIds)) {
            List<UmsRoleUser> binds = bindUserIds.stream().map(userId-> new UmsRoleUser(roleId, userId)).collect(Collectors.toList());
            // 批量插入
            this.batchInsert(binds);
        }
    }

    /**
     * 为用户分配角色
     *
     * @param userId  用户id
     * @param roleIds 角色集合
     */
    @Transactional(rollbackFor = Throwable.class)
    @Trace("为用户分配角色")
    @Override
    public void bindRole(Long userId, List<Long> roleIds) {
        // 删除用户关联的角色关系
        umsRoleUserDao.deleteByUserId(userId);
        log.info("[为用户分配角色] 删除用户原有角色,用户={}", userId);
        // 关系绑定
        if (CollectionUtil.isNotEmpty(roleIds)) {
            List<UmsRoleUser> list = roleIds.stream().distinct().map(roleId -> new UmsRoleUser(roleId, userId)).collect(Collectors.toList());
            // 批量插入
            this.batchInsert(list);
            log.info("[为用户分配角色] 重新绑定角色,用户={},角色={}", userId, roleIds);
        } else {
            log.warn("[为用户分配角色] 传入绑定角色为空,相当于清空用户关联角色,用户={}", userId);
        }
    }

    /**
     * 列表查询
     *
     * @param umsRoleUser 查询条件
     * @return
     */
    @Trace("查询角色用户关联表列表")
    @Override
    public List<UmsRoleUser> getList(UmsRoleUser umsRoleUser) {
        return umsRoleUserDao.getList(umsRoleUser);
    }

    /**
     * 分页查询
     *
     * @param umsRoleUser 查询条件
     * @param pageQuery 分页参数
     * @return
     */
    @Trace("分页查询角色用户关联表")
    @Override
    public List<UmsRoleUser> getPage(UmsRoleUser umsRoleUser, PageQuery pageQuery) {
        return umsRoleUserDao.getList(umsRoleUser, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param umsRoleUser 查询条件
     * @return
     */
    @Trace("统计角色用户关联表数量")
    @Override
    public int count(UmsRoleUser umsRoleUser) {
        return umsRoleUserDao.count(umsRoleUser);
    }

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    @Trace("根据主键查询角色用户关联表")
    @Override
    public UmsRoleUser get(Long id) {
        return umsRoleUserDao.get(id);
    }

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    @Trace("根据主键批量查询角色用户关联表")
    @Override
    public List<UmsRoleUser> getListByIds(List<Long> list) {
        return umsRoleUserDao.getListByIds(list);
    }

    /**
     * 根据roleId查询
     *
     * @param roleId 角色id
     * @return
     */
    @Trace("根据roleId查询角色用户关联表")
    @Override
    public List<UmsRoleUser> getListByRoleId(Long roleId) {
        return umsRoleUserDao.getListByRoleId(roleId);
    }

    /**
     * 根据userId查询
     *
     * @param userId 用户id
     * @return
     */
    @Trace("根据userId查询角色用户关联表")
    @Override
    public List<UmsRoleUser> getListByUserId(Long userId) {
        return umsRoleUserDao.getListByUserId(userId);
    }

    /**
     * 插入
     *
     * @param umsRoleUser 参数
     * @return
     */
    @Trace("插入角色用户关联表")
    @Override
    public int insert(UmsRoleUser umsRoleUser) {
        return umsRoleUserDao.insert(umsRoleUser);
    }

    /**
     * 选择性插入
     *
     * @param umsRoleUser 参数
     * @return
     */
    @Trace("选择性插入角色用户关联表")
    @Override
    public int insertSelective(UmsRoleUser umsRoleUser) {
        return umsRoleUserDao.insertSelective(umsRoleUser);
    }

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量插入角色用户关联表")
    @Override
    public int batchInsert(List<UmsRoleUser> list) {
        return umsRoleUserDao.batchInsert(list);
    }

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    @Trace("批量选择性插入角色用户关联表")
    @Override
    public int batchInsertSelective(List<UmsRoleUser> list) {
        return umsRoleUserDao.batchInsertSelective(list);
    }

    /**
     * 修改
     *
     * @param umsRoleUser 参数
     * @return
     */
    @Trace("修改角色用户关联表")
    @Override
    public int update(UmsRoleUser umsRoleUser) {
        return umsRoleUserDao.update(umsRoleUser);
    }

    /**
     * 选择性修改
     *
     * @param umsRoleUser 参数
     * @return
     */
    @Trace("选择性修改角色用户关联表")
    @Override
    public int updateSelective(UmsRoleUser umsRoleUser) {
        return umsRoleUserDao.updateSelective(umsRoleUser);
    }

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    @Trace("删除角色用户关联表")
    @Override
    public int delete(Long id) {
        return umsRoleUserDao.delete(id);
    }

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    @Trace("批量删除角色用户关联表")
    @Override
    public int batchDelete(List<Long> list) {
        return umsRoleUserDao.batchDelete(list);
    }

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param umsRoleUser 参数
     * @return
     */
    @Trace("根据条件删除角色用户关联表")
    @Override
    public int deleteByCondition(UmsRoleUser umsRoleUser) {
        return umsRoleUserDao.deleteByCondition(umsRoleUser);
    }

}
