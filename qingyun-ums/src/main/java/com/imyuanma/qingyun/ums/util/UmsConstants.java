package com.imyuanma.qingyun.ums.util;

/**
 * ums常量
 *
 * @author wangjy
 * @date 2022/10/08 15:35:48
 */
public class UmsConstants {
    /**
     * 超级管理员用户id
     */
    public static final Long ROOT_USER_ID = 1L;
    /**
     * 根节点id
     */
    public static final Long ROOT_ORG_ID = 1L;
    /**
     * 资源根节点id
     * 一级节点的父节点为0
     */
    public static final Long ROOT_RESOURCE_ID = 0L;
    /**
     * 找回密码时限,秒
     */
    public static final int FIND_PWD_TIMEOUT_S = 1800;
    /**
     * 默认密码
     */
    public static final String DEFAULT_PASSWORD = "123456";
}
