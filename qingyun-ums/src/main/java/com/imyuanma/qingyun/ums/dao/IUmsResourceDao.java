package com.imyuanma.qingyun.ums.dao;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.ums.model.UmsResource;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 资源dao
 *
 * @author YuanMaKeJi
 * @date 2022-10-05 21:06:53
 */
@Mapper
public interface IUmsResourceDao {
    /**
     * 获取所有链接有效的资源数据
     *
     * @return
     */
    List<UmsResource> getAllValidHrefResource();

    /**
     * 列表查询
     *
     * @param umsResource 查询条件
     * @return
     */
    List<UmsResource> getList(UmsResource umsResource);

    /**
     * 分页查询
     *
     * @param umsResource 查询条件
     * @param pageQuery   分页参数
     * @return
     */
    List<UmsResource> getList(UmsResource umsResource, PageQuery pageQuery);

    /**
     * 统计数量
     *
     * @param umsResource 查询条件
     * @return
     */
    int count(UmsResource umsResource);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    UmsResource get(Long id);

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    List<UmsResource> getListByIds(List<Long> list);

    /**
     * code批量查询
     *
     * @param list code集合
     * @return
     */
    List<UmsResource> getListByCodes(List<String> list);

    /**
     * 插入
     *
     * @param umsResource 参数
     * @return
     */
    int insert(UmsResource umsResource);

    /**
     * 选择性插入
     *
     * @param umsResource 参数
     * @return
     */
    int insertSelective(UmsResource umsResource);

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    int batchInsert(List<UmsResource> list);

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    int batchInsertSelective(List<UmsResource> list);

    /**
     * 修改
     *
     * @param umsResource 参数
     * @return
     */
    int update(UmsResource umsResource);

    /**
     * 选择性修改
     *
     * @param umsResource 参数
     * @return
     */
    int updateSelective(UmsResource umsResource);

    /**
     * 批量修改某字段
     *
     * @param idList      主键集合
     * @param fieldDbName 待修改的字段名
     * @param fieldValue  修改后的值
     * @return
     */
    int batchUpdateColumn(@Param("idList") List<Long> idList, @Param("fieldDbName") String fieldDbName, @Param("fieldValue") Object fieldValue);

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    int delete(Long id);

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    int batchDelete(List<Long> list);

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param umsResource 参数
     * @return
     */
    int deleteByCondition(UmsResource umsResource);

    /**
     * 删除全部
     *
     * @return
     */
    int deleteAll();

}
