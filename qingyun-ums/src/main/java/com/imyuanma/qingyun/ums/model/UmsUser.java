package com.imyuanma.qingyun.ums.model;

import com.imyuanma.qingyun.interfaces.ums.model.LoginUserDTO;
import com.imyuanma.qingyun.interfaces.ums.model.UserBaseInfoDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

import com.imyuanma.qingyun.interfaces.common.model.BaseDO;

/**
 * 用户信息实体类
 *
 * @author YuanMaKeJi
 * @date 2022-07-09 16:23:45
 */
@Data
@ApiModel
public class UmsUser extends BaseDO {

    /**
     * 主键
     */
    @ApiModelProperty(value = "用户ID")
    private Long id;

    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名")
    private String name;

    /**
     * 账号
     */
    @ApiModelProperty(value = "账号")
    private String account;

    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String password;

    /**
     * 出生日期
     */
    @ApiModelProperty(value = "生日")
    private String birthday;

    /**
     * 性别
     */
    @ApiModelProperty(value = "性别,10:男,20:女")
    private Integer sex;

    /**
     * 电话
     */
    @ApiModelProperty(value = "电话号码")
    private String tel;

    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱")
    private String email;

    /**
     * 状态,10有效,20无效
     */
    @ApiModelProperty(value = "状态,10:有效,20:无效")
    private Integer status;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
     * 头像
     */
    @ApiModelProperty(value = "头像")
    private String header;

    /**
     * 所属单位
     */
    @ApiModelProperty(value = "单位ID")
    private Long companyId;

    /**
     * 用户类别
     */
    @ApiModelProperty(value = "用户分类")
    private String category;

    /**
     * 上次登录时间(第二近)
     */
    @ApiModelProperty(value = "上次登录时间")
    private Date lastLoginTime;
    /**
     * 上次登录时间(第二近),按时间检索时作为结束时间使用
     */
    private Date lastLoginTime2;

    /**
     * 最近这次登录时间
     */
    @ApiModelProperty(value = "本次登录时间")
    private Date thisLoginTime;
    /**
     * 最近这次登录时间,按时间检索时作为结束时间使用
     */
    private Date thisLoginTime2;

    /**
     * 上次密码更新时间
     */
    @ApiModelProperty(value = "密码更新时间")
    private Date passwordUpdateTime;
    /**
     * 上次密码更新时间,按时间检索时作为结束时间使用
     */
    private Date passwordUpdateTime2;

    /**
     * 密码找回KEY
     */
    private String findPasswordKey;

    /**
     * 密码找回截止时间
     */
    private Date findPasswordEnd;

    /**
     * 对象转换
     *
     * @return
     */
    public LoginUserDTO convert() {
        LoginUserDTO loginUserDTO = new LoginUserDTO();
        loginUserDTO.setUserId(this.id);
        loginUserDTO.setName(this.name);
        loginUserDTO.setEmail(this.email);
        loginUserDTO.setAccount(this.account);
        loginUserDTO.setPassword(this.password);
        loginUserDTO.setStatus(this.status);
        return loginUserDTO;
    }

    /**
     * 对象转换
     * @return
     */
    public UserBaseInfoDTO convertBaseInfo() {
        UserBaseInfoDTO userBaseInfoDTO = new UserBaseInfoDTO();
        userBaseInfoDTO.setUserId(this.id);
        userBaseInfoDTO.setName(this.name);
        userBaseInfoDTO.setEmail(this.email);
        userBaseInfoDTO.setAccount(this.account);
        userBaseInfoDTO.setStatus(this.status);
        return userBaseInfoDTO;
    }

}