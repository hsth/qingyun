package com.imyuanma.qingyun.monitor.model;


import com.imyuanma.qingyun.common.core.concurrent.QingYunThreadPoolExecutor;

import java.util.concurrent.TimeUnit;

/**
 * 线程池信息
 *
 * @author wangjy
 * @date 2021/10/16 17:23:46
 */
public class ThreadPoolStatus {
    /**
     * 线程池名字
     */
    private String poolName;
    /**
     * 当前线程数
     */
    private int poolSize;
    /**
     * 核心线程数
     */
    private int corePoolSize;
    /**
     * 最大线程数
     */
    private int maxPoolSize;
    /**
     * 峰值线程数
     */
    private int largestPoolSize;
    /**
     * 返回处于活动状态的线程的大约数量
     */
    private int activeCount;
    /**
     * 已完成任务数
     */
    private long completedTaskCount;
    /**
     * 线程池任务总数, 含已完成和未完成的
     */
    private long taskCount;
    /**
     * 队列汇总等待的任务数
     */
    private int waitCount;
    /**
     * 空闲线程过期时间
     */
    private long keepAliveTime;
    /**
     * 拒绝策略类名
     */
    private String rejectedHandlerName;
    /**
     * 队列类型名称
     */
    private String queueClassName;


    /**
     * 构建线程池状态对象
     *
     * @param executor
     * @return
     */
    public static ThreadPoolStatus build(QingYunThreadPoolExecutor executor) {
        ThreadPoolStatus status = new ThreadPoolStatus();
        status.poolName = executor.getPoolName();
        status.poolSize = executor.getPoolSize();
        status.corePoolSize = executor.getCorePoolSize();
        status.maxPoolSize = executor.getMaximumPoolSize();
        status.keepAliveTime = executor.getKeepAliveTime(TimeUnit.SECONDS);
        status.largestPoolSize = executor.getLargestPoolSize();
        status.activeCount = executor.getActiveCount();
        status.completedTaskCount = executor.getCompletedTaskCount();
        status.taskCount = executor.getTaskCount();
        status.waitCount = executor.getQueue().size();
        status.rejectedHandlerName = executor.getRejectedExecutionHandler().getClass().getSimpleName();
        status.queueClassName = executor.getQueue().getClass().getSimpleName();
        return status;
    }

    public ThreadPoolStatus() {
    }

    public String getPoolName() {
        return poolName;
    }

    public void setPoolName(String poolName) {
        this.poolName = poolName;
    }

    public int getPoolSize() {
        return poolSize;
    }

    public void setPoolSize(int poolSize) {
        this.poolSize = poolSize;
    }

    public int getCorePoolSize() {
        return corePoolSize;
    }

    public void setCorePoolSize(int corePoolSize) {
        this.corePoolSize = corePoolSize;
    }

    public int getMaxPoolSize() {
        return maxPoolSize;
    }

    public void setMaxPoolSize(int maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

    public int getLargestPoolSize() {
        return largestPoolSize;
    }

    public void setLargestPoolSize(int largestPoolSize) {
        this.largestPoolSize = largestPoolSize;
    }

    public int getActiveCount() {
        return activeCount;
    }

    public void setActiveCount(int activeCount) {
        this.activeCount = activeCount;
    }

    public long getCompletedTaskCount() {
        return completedTaskCount;
    }

    public void setCompletedTaskCount(long completedTaskCount) {
        this.completedTaskCount = completedTaskCount;
    }

    public long getTaskCount() {
        return taskCount;
    }

    public void setTaskCount(long taskCount) {
        this.taskCount = taskCount;
    }

    public int getWaitCount() {
        return waitCount;
    }

    public void setWaitCount(int waitCount) {
        this.waitCount = waitCount;
    }

    public long getKeepAliveTime() {
        return keepAliveTime;
    }

    public void setKeepAliveTime(long keepAliveTime) {
        this.keepAliveTime = keepAliveTime;
    }

    public String getRejectedHandlerName() {
        return rejectedHandlerName;
    }

    public void setRejectedHandlerName(String rejectedHandlerName) {
        this.rejectedHandlerName = rejectedHandlerName;
    }

    public String getQueueClassName() {
        return queueClassName;
    }

    public void setQueueClassName(String queueClassName) {
        this.queueClassName = queueClassName;
    }
}
