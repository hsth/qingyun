package com.imyuanma.qingyun.monitor.service.impl;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.monitor.dao.IMonitorApiTraceDao;
import com.imyuanma.qingyun.monitor.model.MonitorApiTrace;
import com.imyuanma.qingyun.monitor.service.IMonitorApiTraceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 链路日志服务
 *
 * @author YuanMaKeJi
 * @date 2023-06-11 23:17:24
 */
@Slf4j
@Service
public class MonitorApiTraceServiceImpl implements IMonitorApiTraceService {

    /**
     * 链路日志dao
     */
    @Autowired
    private IMonitorApiTraceDao monitorApiTraceDao;

    /**
     * 列表查询
     *
     * @param monitorApiTrace 查询条件
     * @return
     */
    @Override
    public List<MonitorApiTrace> getList(MonitorApiTrace monitorApiTrace) {
        return monitorApiTraceDao.getList(monitorApiTrace);
    }

    /**
     * 分页查询
     *
     * @param monitorApiTrace 查询条件
     * @param pageQuery 分页参数
     * @return
     */
    @Override
    public List<MonitorApiTrace> getPage(MonitorApiTrace monitorApiTrace, PageQuery pageQuery) {
        return monitorApiTraceDao.getList(monitorApiTrace, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param monitorApiTrace 查询条件
     * @return
     */
    @Override
    public int count(MonitorApiTrace monitorApiTrace) {
        return monitorApiTraceDao.count(monitorApiTrace);
    }

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    @Override
    public MonitorApiTrace get(Long id) {
        return monitorApiTraceDao.get(id);
    }

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    @Override
    public List<MonitorApiTrace> getListByIds(List<Long> list) {
        return monitorApiTraceDao.getListByIds(list);
    }

    /**
     * 根据uniqueKey查询
     *
     * @param uniqueKey 唯一标识
     * @return
     */
    @Override
    public MonitorApiTrace getByUniqueKey(String uniqueKey) {
        return monitorApiTraceDao.getByUniqueKey(uniqueKey);
    }

    /**
     * 根据businessId查询
     *
     * @param businessId 业务id
     * @return
     */
    @Override
    public List<MonitorApiTrace> getListByBusinessId(String businessId) {
        return monitorApiTraceDao.getListByBusinessId(businessId);
    }

    /**
     * 根据traceId查询
     *
     * @param traceId 链路追踪id
     * @return
     */
    @Override
    public List<MonitorApiTrace> getListByTraceId(String traceId) {
        return monitorApiTraceDao.getListByTraceId(traceId);
    }

    /**
     * 根据spanId查询
     *
     * @param spanId 块id
     * @return
     */
    @Override
    public List<MonitorApiTrace> getListBySpanId(String spanId) {
        return monitorApiTraceDao.getListBySpanId(spanId);
    }

    /**
     * 根据parentId查询
     *
     * @param parentId 父块id
     * @return
     */
    @Override
    public List<MonitorApiTrace> getListByParentId(String parentId) {
        return monitorApiTraceDao.getListByParentId(parentId);
    }

    /**
     * 插入
     *
     * @param monitorApiTrace 参数
     * @return
     */
    @Override
    public int insert(MonitorApiTrace monitorApiTrace) {
        return monitorApiTraceDao.insert(monitorApiTrace);
    }

    /**
     * 选择性插入
     *
     * @param monitorApiTrace 参数
     * @return
     */
    @Override
    public int insertSelective(MonitorApiTrace monitorApiTrace) {
        return monitorApiTraceDao.insertSelective(monitorApiTrace);
    }

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    @Override
    public int batchInsert(List<MonitorApiTrace> list) {
        return monitorApiTraceDao.batchInsert(list);
    }

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    @Override
    public int batchInsertSelective(List<MonitorApiTrace> list) {
        return monitorApiTraceDao.batchInsertSelective(list);
    }

    /**
     * 修改
     *
     * @param monitorApiTrace 参数
     * @return
     */
    @Override
    public int update(MonitorApiTrace monitorApiTrace) {
        return monitorApiTraceDao.update(monitorApiTrace);
    }

    /**
     * 选择性修改
     *
     * @param monitorApiTrace 参数
     * @return
     */
    @Override
    public int updateSelective(MonitorApiTrace monitorApiTrace) {
        return monitorApiTraceDao.updateSelective(monitorApiTrace);
    }

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    @Override
    public int delete(Long id) {
        return monitorApiTraceDao.delete(id);
    }

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    @Override
    public int batchDelete(List<Long> list) {
        return monitorApiTraceDao.batchDelete(list);
    }

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param monitorApiTrace 参数
     * @return
     */
    @Override
    public int deleteByCondition(MonitorApiTrace monitorApiTrace) {
        return monitorApiTraceDao.deleteByCondition(monitorApiTrace);
    }

    /**
     * 清空
     */
    @Trace("清空链路数据")
    @Override
    public void clean() {
        monitorApiTraceDao.clean();
    }

}
