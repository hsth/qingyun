package com.imyuanma.qingyun.monitor.model;

import com.imyuanma.qingyun.common.util.StringUtil;
import com.imyuanma.qingyun.interfaces.common.model.DbSortDO;
import com.imyuanma.qingyun.interfaces.monitor.model.TraceDTO;
import lombok.Data;

import java.util.Date;


/**
 * 链路日志实体类
 *
 * @author YuanMaKeJi
 * @date 2022-09-21 22:50:57
 */
@Data
public class MonitorApiTrace extends DbSortDO {

    /**
     * 主键
     */
    private Long id;

    /**
     * 唯一标识
     */
    private String uniqueKey;

    /**
     * 业务id
     */
    private String businessId;

    /**
     * 链路追踪id
     */
    private String traceId;

    /**
     * 块id
     */
    private String spanId;

    /**
     * 父块id
     */
    private String parentId;

    /**
     * 调用端发送调用请求的时间
     */
    private Date cs;

    /**
     * 服务端接收调用请求的时间
     */
    private Date sr;

    /**
     * 服务端发送响应的时间
     */
    private Date ss;

    /**
     * 调用端接收响应的时间
     */
    private Date cr;

    /**
     * 调用端ip
     */
    private String cip;

    /**
     * 服务端ip
     */
    private String sip;

    /**
     * 应用名
     */
    private String appName;

    /**
     * 方法key
     */
    private String keyCode;

    /**
     * key描述
     */
    private String keyInfo;

    /**
     * 类名
     */
    private String className;

    /**
     * 方法名
     */
    private String methodName;

    /**
     * 开始时间
     */
    private Date beginTime;
    /**
     * 开始时间,按时间检索时作为结束时间使用
     */
    private Date beginTime2;

    /**
     * 结束时间
     */
    private Date endTime;
    /**
     * 结束时间,按时间检索时作为结束时间使用
     */
    private Date endTime2;

    /**
     * 耗时
     */
    private Integer ms;

    /**
     * 服务器ip
     */
    private String serverIp;

    /**
     * 异常标识
     */
    private Integer hasEx;

    /**
     * 异常信息
     */
    private String ex;

    /**
     * 入参
     */
    private String inParam;

    /**
     * 出参
     */
    private String outParam;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 用户姓名
     */
    private String userName;

    /**
     * 用户ip
     */
    private String userIp;

    /**
     * 操作系统
     */
    private String os;

    /**
     * 浏览器
     */
    private String browser;

    /**
     * 请求的协议,http/https等
     */
    private String scheme;

    /**
     * 请求的host
     */
    private String host;

    /**
     * 请求的端口号
     */
    private Integer port;

    /**
     * 请求的uri
     */
    private String uri;

    /**
     * 请求的url
     */
    private String url;

    /**
     * 请求方式,post/get等
     */
    private String method;

    /**
     * 请求头中的referer
     */
    private String referer;

    /**
     * 请求头中的origin
     */
    private String origin;

    /**
     * 扩展参数
     */
    private String extParam;

    /**
     * 对象转换
     *
     * @param traceDTO
     * @return
     */
    public static MonitorApiTrace convertFrom(TraceDTO traceDTO) {
        MonitorApiTrace monitorApiTrace = new MonitorApiTrace();
        monitorApiTrace.setUniqueKey(traceDTO.getUniqueKey());
        monitorApiTrace.setBusinessId(traceDTO.getBusinessId());
        monitorApiTrace.setTraceId(traceDTO.getTraceId());
        monitorApiTrace.setSpanId(traceDTO.getSpanId());
        monitorApiTrace.setParentId(traceDTO.getParentId());
        monitorApiTrace.setCs(traceDTO.getCs() != null ? new Date(traceDTO.getCs()) : null);
        monitorApiTrace.setSr(traceDTO.getSr() != null ? new Date(traceDTO.getSr()) : null);
        monitorApiTrace.setSs(traceDTO.getSs() != null ? new Date(traceDTO.getSs()) : null);
        monitorApiTrace.setCr(traceDTO.getCr() != null ? new Date(traceDTO.getCr()) : null);
        monitorApiTrace.setCip(traceDTO.getCip());
        monitorApiTrace.setSip(traceDTO.getSip());
        monitorApiTrace.setAppName(traceDTO.getAppName());
        monitorApiTrace.setKeyCode(StringUtil.subString(traceDTO.getKeyCode(), 256));
        monitorApiTrace.setKeyInfo(traceDTO.getKeyInfo());
        monitorApiTrace.setClassName(StringUtil.subString(traceDTO.getClassName(), 256));
        monitorApiTrace.setMethodName(StringUtil.subString(traceDTO.getMethodName(), 128));
        monitorApiTrace.setBeginTime(traceDTO.getBeginTime() != null ? new Date(traceDTO.getBeginTime()) : null);
        monitorApiTrace.setEndTime(traceDTO.getEndTime() != null ? new Date(traceDTO.getEndTime()) : null);
        monitorApiTrace.setMs(traceDTO.getMs());
        monitorApiTrace.setServerIp(StringUtil.subString(traceDTO.getServerIp(), 50));
        monitorApiTrace.setHasEx(traceDTO.getHasEx());
        monitorApiTrace.setEx(StringUtil.subString(traceDTO.getEx(), 1024));
        monitorApiTrace.setInParam(StringUtil.subString(traceDTO.getInParam(), 4096));
        monitorApiTrace.setOutParam(StringUtil.subString(traceDTO.getOutParam(), 4096));
        monitorApiTrace.setUserId(traceDTO.getUserId());
        monitorApiTrace.setUserName(traceDTO.getUserName());
        monitorApiTrace.setUserIp(traceDTO.getUserIp());
        monitorApiTrace.setOs(StringUtil.subString(traceDTO.getOs(), 128));
        monitorApiTrace.setBrowser(StringUtil.subString(traceDTO.getBrowser(), 128));
        monitorApiTrace.setScheme(traceDTO.getScheme());
        monitorApiTrace.setHost(StringUtil.subString(traceDTO.getHost(), 128));
        monitorApiTrace.setPort(traceDTO.getPort());
        monitorApiTrace.setUri(StringUtil.subString(traceDTO.getUri(), 128));
        monitorApiTrace.setUrl(StringUtil.subString(traceDTO.getUrl(), 256));
        monitorApiTrace.setMethod(traceDTO.getMethod());
        monitorApiTrace.setReferer(StringUtil.subString(traceDTO.getReferer(), 256));
        monitorApiTrace.setOrigin(StringUtil.subString(traceDTO.getOrigin(), 128));
        monitorApiTrace.setExtParam(StringUtil.subString(traceDTO.getExtParam(), 4096));
        return monitorApiTrace;
    }


}