package com.imyuanma.qingyun.monitor.model.param;

/**
 * 线程池状态查询参数
 *
 * @author wangjy
 * @date 2023/06/10 22:50:00
 */
public class ThreadPoolUpdateParam {
    /**
     * 线程池名称
     */
    private String poolName;
    /**
     * 属性
     */
    private String attribute;
    /**
     * 更新值
     */
    private String value;

    public String getPoolName() {
        return poolName;
    }

    public void setPoolName(String poolName) {
        this.poolName = poolName;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
