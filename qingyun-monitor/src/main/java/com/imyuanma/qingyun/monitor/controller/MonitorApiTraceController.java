package com.imyuanma.qingyun.monitor.controller;

import com.imyuanma.qingyun.common.model.request.WebRequest;
import com.imyuanma.qingyun.common.model.response.Page;
import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.monitor.model.MonitorApiTrace;
import com.imyuanma.qingyun.monitor.service.IMonitorApiTraceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 链路日志web
 *
 * @author YuanMaKeJi
 * @date 2022-09-21 22:50:57
 */
@RestController
@RequestMapping(value = "/monitor/monitorApiTrace")
public class MonitorApiTraceController {

    /**
     * 链路日志服务
     */
    @Autowired
    private IMonitorApiTraceService monitorApiTraceService;

    @PostMapping("/clean")
    public Result<List<MonitorApiTrace>> clean(@RequestBody WebRequest webRequest) {
        monitorApiTraceService.clean();
        return Result.success();
    }

    /**
     * 列表查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @PostMapping("/getList")
    public Result<List<MonitorApiTrace>> getList(@RequestBody WebRequest<MonitorApiTrace> webRequest) {
        return Result.success(monitorApiTraceService.getList(webRequest.getBody()));
    }

    /**
     * 分页查询
     *
     * @param webRequest 查询条件
     * @return
     */
    @PostMapping("/getPage")
    public Page<List<MonitorApiTrace>> getPage(@RequestBody WebRequest<MonitorApiTrace> webRequest) {
        PageQuery pageQuery = webRequest.buildPageQuery();
        List<MonitorApiTrace> list = monitorApiTraceService.getPage(webRequest.getBody(), pageQuery);
        return Page.success(list, pageQuery);
    }

    /**
     * 统计数量
     *
     * @param webRequest 查询条件
     * @return
     */
    @PostMapping("/count")
    public Result<Integer> count(@RequestBody WebRequest<MonitorApiTrace> webRequest) {
        return Result.success(monitorApiTraceService.count(webRequest.getBody()));
    }

    /**
     * 查询
     *
     * @param webRequest 参数
     * @return
     */
    @PostMapping("/get")
    public Result<MonitorApiTrace> get(@RequestBody WebRequest<Long> webRequest) {
        AssertUtil.notNull(webRequest.getBody());
        return Result.success(monitorApiTraceService.get(webRequest.getBody()));
    }

    /**
     * 新增
     *
     * @param webRequest 参数
     * @return
     */
    @PostMapping("/insert")
    public Result insert(@RequestBody WebRequest<MonitorApiTrace> webRequest) {
        MonitorApiTrace monitorApiTrace = webRequest.getBody();
        AssertUtil.notNull(monitorApiTrace);
        monitorApiTraceService.insertSelective(monitorApiTrace);
        return Result.success();
    }

    /**
     * 修改
     *
     * @param webRequest 参数
     * @return
     */
    @PostMapping("/update")
    public Result update(@RequestBody WebRequest<MonitorApiTrace> webRequest) {
        MonitorApiTrace monitorApiTrace = webRequest.getBody();
        AssertUtil.notNull(monitorApiTrace);
        AssertUtil.notNull(monitorApiTrace.getId());
        monitorApiTraceService.updateSelective(monitorApiTrace);
        return Result.success();
    }

    /**
     * 删除
     *
     * @param webRequest 参数, 待删除主键,多个使用英文逗号拼接
     * @return
     */
    @PostMapping("/delete")
    public Result delete(@RequestBody WebRequest<String> webRequest) {
        String ids = webRequest.getBody();
        AssertUtil.notBlank(ids);
        if (ids.contains(",")) {
            monitorApiTraceService.batchDelete(Arrays.stream(ids.split(",")).map(Long::valueOf).collect(Collectors.toList()));
        } else {
            monitorApiTraceService.delete(Long.valueOf(ids));
        }
        return Result.success();
    }

}
