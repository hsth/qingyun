package com.imyuanma.qingyun.monitor.model;

/**
 * 线程池更新类型枚举
 *
 * @author wangjy
 * @date 2021/10/16 23:42:38
 */
public enum EThreadPoolUpdateTypeEnum {
    /**
     * 核心线程数
     */
    CORE_POOL_SIZE("corePoolSize", "核心线程数"),
    /**
     * 最大线程数
     */
    MAX_POOL_SIZE("maxPoolSize", "最大线程数")
    ;

    /**
     * 类型
     */
    public final String type;
    /**
     * 说明
     */
    public final String text;

    /**
     * 判断相等
     * @param type
     * @return true表示相等
     */
    public boolean eq(String type) {
        return this.type.equals(type);
    }

    /**
     * 判断不相等
     * @param type
     * @return true表示不相等
     */
    public boolean notEq(String type) {
        return !this.eq(type);
    }

    EThreadPoolUpdateTypeEnum(String type, String text) {
        this.type = type;
        this.text = text;
    }
}
