package com.imyuanma.qingyun.monitor.service.out;

import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.common.util.StringUtil;
import com.imyuanma.qingyun.interfaces.monitor.model.TraceDTO;
import com.imyuanma.qingyun.interfaces.monitor.service.IMonitorTraceOutService;
import com.imyuanma.qingyun.monitor.model.MonitorApiTrace;
import com.imyuanma.qingyun.monitor.service.IMonitorApiTraceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 链路对外服务
 *
 * @author wangjy
 * @date 2022/09/21 23:05:31
 */
//@Transactional(rollbackFor = Throwable.class)
@Slf4j
@Service
public class MonitorTraceOutServiceImpl implements IMonitorTraceOutService {
    @Autowired
    private IMonitorApiTraceService monitorApiTraceService;

    /**
     * 批量保存链路日志
     *
     * @param traceDTOList
     */
    @Override
    public void batchSaveTraceLog(List<TraceDTO> traceDTOList) {
        if (CollectionUtil.isEmpty(traceDTOList)) {
            return;
        }

        List<MonitorApiTrace> list = traceDTOList.stream().filter(item -> StringUtil.isNotBlank(item.getTraceId())).map(MonitorApiTrace::convertFrom).collect(Collectors.toList());
        if (CollectionUtil.isEmpty(list)) {
            return;
        }
        monitorApiTraceService.batchInsert(list);
    }
}
