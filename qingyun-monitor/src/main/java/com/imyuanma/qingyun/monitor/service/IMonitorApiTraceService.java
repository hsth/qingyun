package com.imyuanma.qingyun.monitor.service;

import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.monitor.model.MonitorApiTrace;

import java.util.List;

/**
 * 链路日志服务
 *
 * @author YuanMaKeJi
 * @date 2023-06-11 23:17:24
 */
public interface IMonitorApiTraceService {

    /**
     * 列表查询
     *
     * @param monitorApiTrace 查询条件
     * @return
     */
    List<MonitorApiTrace> getList(MonitorApiTrace monitorApiTrace);

    /**
     * 分页查询
     *
     * @param monitorApiTrace 查询条件
     * @param pageQuery       分页参数
     * @return
     */
    List<MonitorApiTrace> getPage(MonitorApiTrace monitorApiTrace, PageQuery pageQuery);

    /**
     * 统计数量
     *
     * @param monitorApiTrace 查询条件
     * @return
     */
    int count(MonitorApiTrace monitorApiTrace);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return
     */
    MonitorApiTrace get(Long id);

    /**
     * 主键批量查询
     *
     * @param list 主键集合
     * @return
     */
    List<MonitorApiTrace> getListByIds(List<Long> list);

    /**
     * 根据uniqueKey查询
     *
     * @param uniqueKey 唯一标识
     * @return
     */
    MonitorApiTrace getByUniqueKey(String uniqueKey);

    /**
     * 根据businessId查询
     *
     * @param businessId 业务id
     * @return
     */
    List<MonitorApiTrace> getListByBusinessId(String businessId);

    /**
     * 根据traceId查询
     *
     * @param traceId 链路追踪id
     * @return
     */
    List<MonitorApiTrace> getListByTraceId(String traceId);

    /**
     * 根据spanId查询
     *
     * @param spanId 块id
     * @return
     */
    List<MonitorApiTrace> getListBySpanId(String spanId);

    /**
     * 根据parentId查询
     *
     * @param parentId 父块id
     * @return
     */
    List<MonitorApiTrace> getListByParentId(String parentId);

    /**
     * 插入
     *
     * @param monitorApiTrace 参数
     * @return
     */
    int insert(MonitorApiTrace monitorApiTrace);

    /**
     * 选择性插入
     *
     * @param monitorApiTrace 参数
     * @return
     */
    int insertSelective(MonitorApiTrace monitorApiTrace);

    /**
     * 批量插入
     *
     * @param list 参数
     * @return
     */
    int batchInsert(List<MonitorApiTrace> list);

    /**
     * 批量选择性插入
     *
     * @param list 参数
     * @return
     */
    int batchInsertSelective(List<MonitorApiTrace> list);

    /**
     * 修改
     *
     * @param monitorApiTrace 参数
     * @return
     */
    int update(MonitorApiTrace monitorApiTrace);

    /**
     * 选择性修改
     *
     * @param monitorApiTrace 参数
     * @return
     */
    int updateSelective(MonitorApiTrace monitorApiTrace);

    /**
     * 删除
     *
     * @param id 主键
     * @return
     */
    int delete(Long id);

    /**
     * 批量删除
     *
     * @param list 主键集合
     * @return
     */
    int batchDelete(List<Long> list);

    /**
     * 条件删除(默认和getList方法的条件相同)
     *
     * @param monitorApiTrace 参数
     * @return
     */
    int deleteByCondition(MonitorApiTrace monitorApiTrace);

    /**
     * 清空
     */
    void clean();

}
