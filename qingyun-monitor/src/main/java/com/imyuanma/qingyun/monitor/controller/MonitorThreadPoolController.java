package com.imyuanma.qingyun.monitor.controller;

import com.imyuanma.qingyun.common.core.concurrent.QingYunThreadPoolExecutor;
import com.imyuanma.qingyun.common.model.request.WebRequest;
import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.interfaces.monitor.annotation.Trace;
import com.imyuanma.qingyun.monitor.model.EThreadPoolUpdateTypeEnum;
import com.imyuanma.qingyun.monitor.model.ThreadPoolStatus;
import com.imyuanma.qingyun.monitor.model.param.ThreadPoolSearchParam;
import com.imyuanma.qingyun.monitor.model.param.ThreadPoolUpdateParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

/**
 * 线程池管理web
 *
 * @author wangjy
 * @date 2021/10/16 23:24:36
 */
@RestController
@RequestMapping(value = "/monitor/threadpool")
public class MonitorThreadPoolController {

    private static final Logger logger = LoggerFactory.getLogger(MonitorThreadPoolController.class);

    /**
     * 查询线程池状态列表
     *
     * @return
     */
    @Trace("查询线程池状态列表")
    @PostMapping("/list")
    public Object list(@RequestBody WebRequest<ThreadPoolSearchParam> webRequest) {
        return Result.success(QingYunThreadPoolExecutor.executorList().stream().map(ThreadPoolStatus::build).collect(Collectors.toList()));
    }

    /**
     * 查询线程池状态
     *
     * @return
     */
    @Trace("查询线程池状态")
    @PostMapping("/get")
    public Object get(@RequestBody WebRequest<String> webRequest) {
        AssertUtil.notBlank(webRequest.getBody());
        QingYunThreadPoolExecutor executor = QingYunThreadPoolExecutor.get(webRequest.getBody());
        return executor != null ? Result.success(ThreadPoolStatus.build(executor)) : Result.error("名字为[" + webRequest.getBody() + "]的线程池不存在");
    }

    /**
     * 更新线程池参数
     *
     * @return
     */
    @Trace("更新线程池参数")
    @PostMapping("/update")
    public Object updateCoreThreadSize(@RequestBody WebRequest<ThreadPoolUpdateParam> webRequest) {
        ThreadPoolUpdateParam updateParam = webRequest.getBody();
        AssertUtil.notNull(updateParam, "非法参数");
        AssertUtil.notBlank(updateParam.getPoolName(), "非法的线程池");
        AssertUtil.notBlank(updateParam.getAttribute(), "非法的参数类型");
        AssertUtil.notBlank(updateParam.getValue(), "非法的参数值");
        QingYunThreadPoolExecutor executor = QingYunThreadPoolExecutor.get(updateParam.getPoolName());
        if (executor == null) {
            return Result.error("名字为[" + updateParam.getPoolName() + "]的线程池不存在");
        }
        if (EThreadPoolUpdateTypeEnum.CORE_POOL_SIZE.eq(updateParam.getAttribute())) {
            int old = executor.getCorePoolSize();
            executor.setCorePoolSize(Integer.parseInt(updateParam.getValue()));
            logger.info("[更新线程池参数] 更新的线程池={},更新的属性={},旧值={},新值={}", updateParam.getPoolName(), updateParam.getAttribute(), old, updateParam.getValue());
        } else if (EThreadPoolUpdateTypeEnum.MAX_POOL_SIZE.eq(updateParam.getAttribute())) {
            int old = executor.getMaximumPoolSize();
            executor.setMaximumPoolSize(Integer.parseInt(updateParam.getValue()));
            logger.info("[更新线程池参数] 更新的线程池={},更新的属性={},旧值={},新值={}", updateParam.getPoolName(), updateParam.getAttribute(), old, updateParam.getValue());
        } else {
            return Result.error("属性[" + updateParam.getAttribute() + "]不支持修改");
        }
        return Result.success();
    }

}
