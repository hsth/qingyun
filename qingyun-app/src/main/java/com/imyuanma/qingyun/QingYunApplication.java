package com.imyuanma.qingyun;

import com.imyuanma.qingyun.common.ext.ums.UmsSendFindPwdMsgExt4Mail;
import com.imyuanma.qingyun.interfaces.ums.ext.UmsSendFindPwdMsgExt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * Created by rookie on 2018/9/8.
 */
@SpringBootApplication(scanBasePackages = {"com.imyuanma.qingyun"})
public class QingYunApplication {
    private static Logger logger = LoggerFactory.getLogger(QingYunApplication.class);

    /**
     * 程序入口，启动springboot
     *
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(QingYunApplication.class, args);
        logger.warn("[QingYunApplication] 应用启动完成!");
    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.addAllowedOrigin("*"); // 1允许任何域名使用
        corsConfiguration.addAllowedHeader("*"); // 2允许任何头
        corsConfiguration.addAllowedMethod("*"); // 3允许任何方法（post、get等）
        source.registerCorsConfiguration("/**", corsConfiguration); // 4
        return new CorsFilter(source);
    }

    @Bean
    public UmsSendFindPwdMsgExt umsSendFindPwdMsgExt() {
        return new UmsSendFindPwdMsgExt4Mail("", "","smtp.163.com", "轻云", sendFindPwdMsgParam -> {
            String url = String.format("%s?key=%s", "http://localhost/page/resetpwd.html", sendFindPwdMsgParam.getFindPwdKey());
            return String.format("您好，您正在发起密码找回，请点击右侧链接重置您的密码：%s。", url);
        });
    }
}
