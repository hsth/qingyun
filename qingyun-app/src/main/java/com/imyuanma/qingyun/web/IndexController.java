package com.imyuanma.qingyun.web;

import com.imyuanma.qingyun.common.model.response.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 首页
 *
 * @author wangjy
 * @date 2022/07/09 23:06:32
 */
@Controller
public class IndexController implements ErrorController {

    private static final Logger logger = LoggerFactory.getLogger(IndexController.class);

    @RequestMapping("/error")
    @ResponseBody
    public Result error(HttpServletRequest request, HttpServletResponse response) {
        return Result.error("error:" + response.getStatus());
    }

    @RequestMapping("/index")
    public void index(HttpServletRequest request, HttpServletResponse response) {
        try {
            response.sendRedirect("/page/index.html");
        } catch (IOException e) {
            logger.error("[首页] 重定向设置异常");
        }
    }

//    @RequestMapping("/")
//    public void index(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            response.sendRedirect("/page/index.html");
//        } catch (IOException e) {
//            logger.error("[首页] 重定向设置异常");
//        }
//    }
}
