package com.imyuanma.qingyun.swagger;

import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

/**
 * @author yangwei
 */
@Configuration
@EnableSwagger2WebMvc
@Profile({ "dev", "prod"})
public class SwaggerConfig {

	@Bean
	public Docket wdAPI() {
		return new Docket(DocumentationType.SWAGGER_2)
				.enable(true)
				.groupName("QingYun")
				.apiInfo(apiInfo())
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.imyuanma.qingyun"))
				.apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
//				.paths(PathSelectors.any())
				.build();
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("轻云平台 API")
				.description("关于轻云平台的API接口文档")
				.version("1.0")
				.build();
	}

}
