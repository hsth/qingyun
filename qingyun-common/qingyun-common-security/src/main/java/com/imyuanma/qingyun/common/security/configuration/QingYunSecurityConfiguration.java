package com.imyuanma.qingyun.common.security.configuration;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 轻云安全认证配置参数
 *
 * @author wangjy
 * @date 2022/09/08 23:16:06
 */
@Configuration
@ConditionalOnWebApplication
@ConditionalOnProperty(name = "qingyun.security.enable", havingValue = "true", matchIfMissing = true)
@ConfigurationProperties(prefix="qingyun.security")
public class QingYunSecurityConfiguration {
    /**
     * 不需要强校验登录的名单
     */
    private String[] ssoWhite;
    /**
     * 忽略security的名单, 通常是静态资源
     */
    private String[] ignoreWhite;

    public String[] getSsoWhite() {
        return ssoWhite;
    }

    public void setSsoWhite(String[] ssoWhite) {
        this.ssoWhite = ssoWhite;
    }

    public String[] getIgnoreWhite() {
        return ignoreWhite;
    }

    public void setIgnoreWhite(String[] ignoreWhite) {
        this.ignoreWhite = ignoreWhite;
    }
}
