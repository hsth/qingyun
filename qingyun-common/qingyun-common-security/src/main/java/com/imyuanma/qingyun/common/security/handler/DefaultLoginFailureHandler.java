package com.imyuanma.qingyun.common.security.handler;

import com.imyuanma.qingyun.common.security.SecurityThreadPoolUtil;
import com.imyuanma.qingyun.common.security.configuration.QingYunSecurityConfiguration;
import com.imyuanma.qingyun.common.security.exception.PasswordDecodeException;
import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.common.util.ClientInfoUtil;
import com.imyuanma.qingyun.common.util.IPUtil;
import com.imyuanma.qingyun.common.util.WebUtil;
import com.imyuanma.qingyun.interfaces.ums.model.LoginLogDTO;
import com.imyuanma.qingyun.interfaces.ums.model.enums.EUmsLoginDeviceTypeEnum;
import com.imyuanma.qingyun.interfaces.ums.service.IUmsPermissionsOutService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.security.authentication.*;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 登录失败处理
 *
 * @author wangjy
 * @date 2022/07/09 21:10:59
 */
@Component
@ConditionalOnBean(QingYunSecurityConfiguration.class)
public class DefaultLoginFailureHandler implements AuthenticationFailureHandler {
    private static final Logger logger = LoggerFactory.getLogger(DefaultLoginFailureHandler.class);
    /**
     * 权限&认证服务
     */
    @Autowired
    private IUmsPermissionsOutService permissionsOutService;

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        logger.info("[登录失败后处理器] 登录失败,账号={},URI={}", request.getParameter("account"), request.getRequestURI());
        String errorInfo;
        String realErrorInfo;
        if (exception instanceof BadCredentialsException ||
                exception instanceof UsernameNotFoundException) {
            errorInfo = "账户名或者密码输入错误!";
            realErrorInfo = "账号或凭证错误";
        } else if (exception instanceof PasswordDecodeException) {
            errorInfo = "账户名或者密码输入错误!";
            realErrorInfo = "入参密码解析异常";
        } else if (exception instanceof LockedException) {
            errorInfo = "账户被锁定，请联系管理员!";
            realErrorInfo = "账号被锁定";
        } else if (exception instanceof CredentialsExpiredException) {
            errorInfo = "密码过期，请联系管理员!";
            realErrorInfo = "密码或凭证过期";
        } else if (exception instanceof AccountExpiredException) {
            errorInfo = "账户过期，请联系管理员!";
            realErrorInfo = "账号过期";
        } else if (exception instanceof DisabledException) {
            errorInfo = "账户被禁用，请联系管理员!";
            realErrorInfo = "账号禁用";
        } else {
            errorInfo = "登录失败!";
            realErrorInfo = "未知登录异常";
        }
        WebUtil.write2Response(response, Result.error(errorInfo));

        try {
            // 记录登录日志
            LoginLogDTO loginLog = LoginLogDTO.error(request.getParameter("account"), ClientInfoUtil.getClientIpAddr(request), IPUtil.getLocalIp(), EUmsLoginDeviceTypeEnum.CODE_H5, realErrorInfo);
            SecurityThreadPoolUtil.execute(() -> permissionsOutService.insertLoginLog(loginLog));
        } catch (Throwable t) {
            logger.error("[登录失败后处理器] 插入登录日志异常,账号={},登录失败原因={}", request.getParameter("account"), realErrorInfo, t);
        }
    }
}
