package com.imyuanma.qingyun.common.security;

import com.imyuanma.qingyun.common.core.concurrent.QingYunThreadPoolExecutor;

/**
 * 认证线程池工具类
 *
 * @author wangjy
 * @date 2023/04/08 16:09:40
 */
public class SecurityThreadPoolUtil {
    /**
     * 线程池
     */
    private static final QingYunThreadPoolExecutor THREAD_POOL = QingYunThreadPoolExecutor.build("securityThreadPool");

    /**
     * 执行任务
     *
     * @param runnable 任务
     */
    public static void execute(Runnable runnable) {
        THREAD_POOL.execute(runnable);
    }
}
