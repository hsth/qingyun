package com.imyuanma.qingyun.common.security.model;

import com.imyuanma.qingyun.common.exception.Exceptions;
import com.imyuanma.qingyun.interfaces.ums.model.LoginUserDTO;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Spring Security UserDetails实现
 *
 * @author wangjy
 * @date 2022/07/09 16:59:55
 */
public class DefaultUserDetails implements UserDetails {
    /**
     * 登录用户
     */
    private LoginUserDTO loginUserDTO;
    /**
     * 令牌
     */
    private String token;

    /**
     * 构造UserDetails实例
     *
     * @param loginUserDTO
     * @return
     */
    public static DefaultUserDetails newInstance(LoginUserDTO loginUserDTO) {
        DefaultUserDetails defaultUserDetails = new DefaultUserDetails();
        defaultUserDetails.loginUserDTO = loginUserDTO;
        return defaultUserDetails;
    }

    public static DefaultUserDetails newInstance(LoginUserDTO loginUserDTO, String token) {
        DefaultUserDetails defaultUserDetails = new DefaultUserDetails();
        defaultUserDetails.loginUserDTO = loginUserDTO;
        defaultUserDetails.token = token;
        return defaultUserDetails;
    }

    /**
     * 登录成功后写入token
     *
     * @param token
     */
    public void tokenAfterLoginSuccess(String token) {
        if (this.token != null) {
            throw Exceptions.baseException("重复设置token");
        }
        this.token = token;
    }

    private DefaultUserDetails() {
    }

    public LoginUserDTO getLoginUserDTO() {
        return loginUserDTO;
    }

    /**
     * Returns the authorities granted to the user. Cannot return <code>null</code>.
     *
     * @return the authorities, sorted by natural key (never <code>null</code>)
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return new ArrayList<>();
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * @return the password
     */
    @Override
    public String getPassword() {
        return loginUserDTO.getPassword();
    }

    /**
     * Returns the username used to authenticate the user. Cannot return
     * <code>null</code>.
     *
     * @return the username (never <code>null</code>)
     */
    @Override
    public String getUsername() {
        return loginUserDTO.getAccount();
    }

    /**
     * 账户是否未过期
     *
     * @return
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * 账户是否未锁定
     *
     * @return
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * 凭证未过期
     *
     * @return
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * 账户是否有效, 有效才能认证
     *
     * @return
     */
    @Override
    public boolean isEnabled() {
        return loginUserDTO.isEnabled();
    }

    public String getToken() {
        return token;
    }
}
