package com.imyuanma.qingyun.common.security.model;

import org.springframework.security.core.GrantedAuthority;

import java.util.Objects;

/**
 * 资源权限对象
 *
 * @author wangjy
 * @date 2022/07/16 13:59:47
 */
public class ResourceGrantedAuthority implements GrantedAuthority {
    /**
     * 资源编码
     */
    private String code;

    public ResourceGrantedAuthority(String code) {
        this.code = code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o instanceof ResourceGrantedAuthority) {
            ResourceGrantedAuthority that = (ResourceGrantedAuthority) o;
            return Objects.equals(code, that.code);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }

    @Override
    public String getAuthority() {
        return code;
    }


}
