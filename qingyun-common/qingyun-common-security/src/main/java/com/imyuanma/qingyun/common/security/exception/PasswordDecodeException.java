package com.imyuanma.qingyun.common.security.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * 密码解密异常
 *
 * @author wangjy
 * @date 2022/07/09 21:17:01
 */
public class PasswordDecodeException extends AuthenticationException {
    /**
     * Constructs an {@code AuthenticationException} with the specified message and root
     * cause.
     *
     * @param msg   the detail message
     * @param cause the root cause
     */
    public PasswordDecodeException(String msg, Throwable cause) {
        super(msg, cause);
    }

    /**
     * Constructs an {@code AuthenticationException} with the specified message and no
     * root cause.
     *
     * @param msg the detail message
     */
    public PasswordDecodeException(String msg) {
        super(msg);
    }
}
