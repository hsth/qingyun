package com.imyuanma.qingyun.common.security.constants;

/**
 * 安全认证常量
 *
 * @author wangjy
 * @date 2022/08/14 23:50:11
 */
public class SecurityConstants {

    /**
     * 会话token保存的key
     * cookie,header,参数共用同一个key
     */
    public static final String TOKEN_KEY = "qy_token";
}
