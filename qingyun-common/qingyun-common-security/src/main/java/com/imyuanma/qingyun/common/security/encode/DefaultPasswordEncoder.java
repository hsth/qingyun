package com.imyuanma.qingyun.common.security.encode;

import com.imyuanma.qingyun.common.security.exception.PasswordDecodeException;
import com.imyuanma.qingyun.interfaces.ums.service.IUmsPasswordOutService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


/**
 * 默认的加密实现
 *
 * @author wangjy
 * @date 2022/07/09 15:30:12
 */
@Service
public class DefaultPasswordEncoder implements PasswordEncoder {
    private static final Logger logger = LoggerFactory.getLogger(DefaultPasswordEncoder.class);


    @Autowired
    private IUmsPasswordOutService umsPasswordOutService;

    /**
     * 加密
     *
     * @param rawPassword 前端入参密码, 注册时使用
     * @return
     */
    @Override
    public String encode(CharSequence rawPassword) {
        logger.debug("[默认的加密实现-encode] 输入的密码为:{}", rawPassword);
        return umsPasswordOutService.encode(rawPassword.toString());
    }

    /**
     * 密码验证
     *
     * @param rawPassword     前端入参密码
     * @param encodedPassword 数据库存储密码
     * @return
     */
    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        String pwd;
        try {
             pwd = umsPasswordOutService.parseInputPassword(rawPassword);

        } catch (Throwable e) {
            logger.error("[解析用户真正输入的密码] 解密时发生异常,入参={}", rawPassword, e);
            throw new PasswordDecodeException("入参密码解密失败");
        }
        logger.debug("[默认的加密实现-matches] 输入的密码为:{},数据库返回的密码为:{}", pwd, encodedPassword);
        return umsPasswordOutService.encode(pwd).equals(encodedPassword);
    }

    /**
     * 是否加密2次
     *
     * @param encodedPassword 加密一次后的密码
     * @return
     */
    @Override
    public boolean upgradeEncoding(String encodedPassword) {
        return false;
    }
}
