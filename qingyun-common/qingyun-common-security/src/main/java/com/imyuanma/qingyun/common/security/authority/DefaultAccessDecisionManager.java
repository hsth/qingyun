package com.imyuanma.qingyun.common.security.authority;

import com.imyuanma.qingyun.common.security.configuration.QingYunSecurityConfiguration;
import com.imyuanma.qingyun.common.security.model.SsoAuthenticationToken;
import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.common.util.JsonUtil;
import com.imyuanma.qingyun.interfaces.common.model.ApiResult;
import com.imyuanma.qingyun.interfaces.ums.model.UriAccessVerifyDTO;
import com.imyuanma.qingyun.interfaces.ums.service.IUmsPermissionsOutService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.FilterInvocation;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

/**
 * 访问验证
 *
 * @author wangjy
 * @date 2023/06/04 17:34:31
 */
@Configuration
@ConditionalOnBean(QingYunSecurityConfiguration.class)
public class DefaultAccessDecisionManager implements AccessDecisionManager {
    private static final Logger logger = LoggerFactory.getLogger(DefaultAccessDecisionManager.class);
    /**
     * 权限服务
     */
    @Autowired
    private IUmsPermissionsOutService umsPermissionsOutService;

    /**
     * 权限校验方法
     *
     * @param authentication   当前登录用户认证信息
     * @param object           the secured object being called
     * @param configAttributes 当前请求命中的验证配置
     * @throws AccessDeniedException               if access is denied as the authentication does not
     *                                             hold a required authority or ACL privilege
     * @throws InsufficientAuthenticationException if access is denied as the
     *                                             authentication does not provide a sufficient level of trust
     */
    @Override
    public void decide(Authentication authentication, Object object, Collection<ConfigAttribute> configAttributes) throws AccessDeniedException, InsufficientAuthenticationException {
        if (CollectionUtil.isEmpty(configAttributes)) {
            logger.warn("[访问控制] 未配置访问控制属性,直接通过");
            return;
        }
        if (!(object instanceof FilterInvocation)) {
            logger.warn("[访问控制] 安全调用对象仅支持FilterInvocation类型,直接通过,当前安全对象:{}", object);
            return;
        }
        for (ConfigAttribute configAttribute : configAttributes) {
            String exp = configAttribute.toString();
            if ("authenticated".equals(exp) || "fullyAuthenticated".equals(exp)) {
                this.handleAuthenticated(authentication, object);
            } else if ("denyAll".equals(exp)) {
                throw new AccessDeniedException("无访问权限!");
            } else if ("permitAll".equals(exp) || "anonymous".equals(exp)) {
                logger.debug("[访问控制] 无需验证权限:{}", exp);
            } else {
                logger.warn("[访问控制] 未知的访问控制配置:{},默认通过", exp);
            }
        }
    }

    /**
     * 处理需认证的请求
     *
     * @param authentication
     * @param object
     */
    private void handleAuthenticated(Authentication authentication, Object object) {
        if (!(authentication instanceof SsoAuthenticationToken)) {
            logger.debug("[访问控制] 认证对象非SsoAuthenticationToken类型,需要进行登录认证,当前认证对象:{}", authentication);
            throw new InsufficientAuthenticationException("未身份认证");
        }
        // 认证对象
        SsoAuthenticationToken ssoAuthenticationToken = (SsoAuthenticationToken) authentication;
        // 安全执行对象
        FilterInvocation invocation = (FilterInvocation) object;
        HttpServletRequest request = invocation.getRequest();
        if (request == null) {
            logger.error("[访问控制] request对象为空,直接通过,当前安全对象:{}", object);
            return;
        }
        String uri = request.getRequestURI();
        logger.debug("[访问控制] 当前请求URI:{}", uri);

        // 访问验证
        UriAccessVerifyDTO uriAccessVerifyDTO = this.buildUriAccessVerifyDTO(ssoAuthenticationToken, uri);
        ApiResult apiResult = umsPermissionsOutService.verifyUriAccess(uriAccessVerifyDTO);
        if (apiResult != null && apiResult.isSuccess()) {
            return;
        }
        logger.warn("[访问控制] 访问权限验证未通过,uri:{},验证结果:{}", uri, JsonUtil.toJson(apiResult));
        throw new AccessDeniedException("无访问权限!");
    }

    /**
     * 校验入参
     *
     * @param ssoAuthenticationToken
     * @param uri
     * @return
     */
    private UriAccessVerifyDTO buildUriAccessVerifyDTO(SsoAuthenticationToken ssoAuthenticationToken, String uri) {
        UriAccessVerifyDTO uriAccessVerifyDTO = new UriAccessVerifyDTO();
        uriAccessVerifyDTO.setUserId(ssoAuthenticationToken.getSsoVerifyDTO().getLoginUser().getUserId());
        uriAccessVerifyDTO.setSsoToken(ssoAuthenticationToken.getSsoVerifyDTO().getToken());
        uriAccessVerifyDTO.setUri(uri);
        return uriAccessVerifyDTO;
    }

    @Override
    public boolean supports(ConfigAttribute attribute) {
        return true;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return true;
    }
}
