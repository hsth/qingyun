package com.imyuanma.qingyun.common.security;

import com.imyuanma.qingyun.common.client.ums.ILoginUserProvider;
import com.imyuanma.qingyun.common.security.configuration.QingYunSecurityConfiguration;
import com.imyuanma.qingyun.common.security.model.SsoAuthenticationToken;
import com.imyuanma.qingyun.interfaces.ums.model.LoginUserDTO;
import com.imyuanma.qingyun.interfaces.ums.model.SsoVerifyDTO;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * 当前等了用户提供者
 *
 * @author wangjy
 * @date 2024/03/08 00:13:00
 */
@Service
@ConditionalOnBean(QingYunSecurityConfiguration.class)
public class LoginUserProvider4SpringSecurity implements ILoginUserProvider {
    /**
     * 获取当前登录用户
     *
     * @return 当前登录用户信息
     */
    @Override
    public LoginUserDTO getLoginUser() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        if (securityContext != null && securityContext.getAuthentication() instanceof SsoAuthenticationToken) {
            SsoAuthenticationToken ssoAuthenticationToken = (SsoAuthenticationToken) securityContext.getAuthentication();
            SsoVerifyDTO ssoVerifyDTO = ssoAuthenticationToken.getSsoVerifyDTO();
            if (ssoVerifyDTO != null) {
                return ssoVerifyDTO.getLoginUser();
            }
        }
        return null;
    }
}
