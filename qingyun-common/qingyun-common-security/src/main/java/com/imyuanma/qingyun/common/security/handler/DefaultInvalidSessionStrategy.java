package com.imyuanma.qingyun.common.security.handler;

import com.imyuanma.qingyun.common.model.ECommonResultCode;
import com.imyuanma.qingyun.common.model.response.Result;
import com.imyuanma.qingyun.common.util.WebUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.session.InvalidSessionStrategy;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 会话失效策略
 *
 * @author wangjy
 * @date 2022/07/10 23:50:44
 */
@Deprecated
@Component
public class DefaultInvalidSessionStrategy implements InvalidSessionStrategy {
    private static final Logger logger = LoggerFactory.getLogger(DefaultInvalidSessionStrategy.class);

    @Override
    public void onInvalidSessionDetected(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        logger.info("[会话失效策略] 失效的会话,URI={}", request.getRequestURI());
        if (WebUtil.isAjaxRequest(request)) {
            WebUtil.write2Response(response, Result.error(ECommonResultCode.SESSION_TIMEOUT));
        } else {
            //请求的url
            response.sendRedirect("/page/login.html");
        }
    }
}
