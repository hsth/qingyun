package com.imyuanma.qingyun.common.util;

import java.util.UUID;

/**
 * uuid工具类
 *
 * @author wangjy
 * @date 2021/12/28 00:25:21
 */
public class UuidUtil {

    /**
     * 获取UUID
     *
     * @return
     */
    public static String getUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
}
