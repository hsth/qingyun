package com.imyuanma.qingyun.common.util;


import com.imyuanma.qingyun.common.exception.ParamException;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 断言工具
 *
 * @author wangjy
 * @date 2021/10/16 23:54:58
 */
public class AssertUtil {
    public static void notEmpty(Object[] arr, String message) {
        if (CollectionUtil.isEmpty(arr)) {
            throw new ParamException(message);
        }
    }

    public static void notEmpty(Map map, String message) {
        if (CollectionUtil.isEmpty(map)) {
            throw new ParamException(message);
        }
    }

    public static void notEmpty(Collection collection, String message) {
        if (CollectionUtil.isEmpty(collection)) {
            throw new ParamException(message);
        }
    }

    public static void notBlank(String str) {
        if (StringUtil.isBlank(str)) {
            throw new ParamException();
        }
    }

    public static void notBlank(String str, String message) {
        if (StringUtil.isBlank(str)) {
            throw new ParamException(message);
        }
    }

    public static void isBlank(String str, String message) {
        if (StringUtil.isNotBlank(str)) {
            throw new ParamException(message);
        }
    }

    public static void notNull(Object object) {
        if (object == null) {
            throw new ParamException();
        }
    }

    public static void notNull(Object object, String message) {
        if (object == null) {
            throw new ParamException(message);
        }
    }

    public static void isNull(Object object, String message) {
        if (object != null) {
            throw new ParamException(message);
        }
    }

    public static void isTrue(boolean boo, String message) {
        if (!boo) {
            throw new ParamException(message);
        }
    }

    public static void isFalse(boolean boo, String message) {
        if (boo) {
            throw new ParamException(message);
        }
    }

    public static <T> void inEnums(T object, List<T> enums) {
        inEnums(object, enums, null);
    }

    public static <T> void inEnums(T object, List<T> enums, String message) {
        for (Object anEnum : enums) {
            if (Objects.equals(object, anEnum)) {
                return;
            }
        }
        throw message != null ? new ParamException(message) : new ParamException();
    }

}
