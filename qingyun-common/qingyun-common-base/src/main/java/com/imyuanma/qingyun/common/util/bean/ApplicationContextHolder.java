package com.imyuanma.qingyun.common.util.bean;

import com.imyuanma.qingyun.common.util.CollectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * xxx
 *
 * @author wangjy
 * @date 2024/01/21 22:35:57
 */
@Component
public class ApplicationContextHolder implements ApplicationContextAware {
    private static final Logger logger = LoggerFactory.getLogger(ApplicationContextHolder.class);

    private static ApplicationContext applicationContext;


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        logger.info("[应用上下文holder] 更新上下文,更新前={}", ApplicationContextHolder.applicationContext);
        ApplicationContextHolder.applicationContext = applicationContext;
        logger.info("[应用上下文holder] 更新上下文,更新后={}", applicationContext);
    }

    /**
     * 根据类型获取bean
     *
     * @param clz
     * @param <T>
     * @return
     */
    public static <T> List<T> getBeans(Class<T> clz) {
        Map<String, T> map = applicationContext.getBeansOfType(clz);
        logger.info("[应用上下文holder] 根据类型获取beans,Class={},beans={}", clz, map);
        return CollectionUtil.isNotEmpty(map) ? new ArrayList<>(map.values()) : null;
    }
}
