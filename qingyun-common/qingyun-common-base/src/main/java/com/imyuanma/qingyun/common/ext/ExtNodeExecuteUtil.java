package com.imyuanma.qingyun.common.ext;

import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.interfaces.common.ext.ExtNodeResult;
import com.imyuanma.qingyun.interfaces.common.ext.IExtNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 扩展点执行工具类
 *
 * @author wangjy
 * @date 2023/01/02 14:05:36
 */
@Component
public class ExtNodeExecuteUtil {
    private static List<? extends IExtNode<?, ?>> extList;

    @Autowired(required = false)
    public void initExtNodeList(List<IExtNode<?, ?>> extList) {
        ExtNodeExecuteUtil.extList = extList;
    }

    /**
     * 获取指定类型的扩展点实现
     *
     * @param extNodeClass
     * @param <P>
     * @param <R>
     * @return
     */
    public static <P, R> List<? extends IExtNode<P, R>> extNodeList(Class<? extends IExtNode<P, R>> extNodeClass) {
        if (CollectionUtil.isNotEmpty(extList)) {
            return (List<? extends IExtNode<P, R>>) extList.stream().filter(extNodeClass::isInstance).collect(Collectors.toList());
        }
        return null;
    }

    /**
     * 执行扩展点
     *
     * @param extList
     * @param extNodeParam
     * @param <P>
     * @param <R>
     * @return
     */
    @Deprecated
    public static <P, R> ExtNodeResult<R> execute(List<? extends IExtNode<P, R>> extList, P extNodeParam) {
        return executeFirst(extList, extNodeParam);
    }

    /**
     * 执行第一个有效扩展点
     *
     * @param extList
     * @param extNodeParam
     * @param <P>
     * @param <R>
     * @return
     */
    @Deprecated
    public static <P, R> ExtNodeResult<R> executeFirst(List<? extends IExtNode<P, R>> extList, P extNodeParam) {
        if (CollectionUtil.isNotEmpty(extList)) {
            for (IExtNode<P, R> extNode : extList) {
                if (extNode != null) {
                    return extNode.execute(extNodeParam);
                }
            }
        }
        return ExtNodeResult.error("Ext Node Is Null");
    }
}
