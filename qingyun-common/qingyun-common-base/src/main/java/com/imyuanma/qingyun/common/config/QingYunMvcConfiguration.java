package com.imyuanma.qingyun.common.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Map;


/**
 * 轻云mvc配置参数
 */
@Configuration
@ConditionalOnWebApplication
@ConditionalOnProperty(
        name = "qingyun.mvc.enable",
        havingValue = "true"
)
@ConfigurationProperties(prefix="qingyun.mvc")
public class QingYunMvcConfiguration {
    /**
     * 是否启用轻云的mvc配置
     * false的话需要自行注入mvc配置bean
     */
    private Boolean enable;
    /**
     * 拦截器的bean,多个使用逗号拼接
     */
    private String interceptorBeanNames;

    /**
     * 默认访问地址
     */
    private String welcomeUrl;
    /**
     * 默认拦截器排除路径
     * 这里配置的路径默认不走拦截器,通常是一些静态资源
     * 例如: /static/**, /jo/**
     */
    private String[] defaultInterceptorExcludePathPatterns;
    /**
     * 资源映射
     * key有2个:
     * 1.patterns: 资源访问路径, 例如: /static/**
     * 2.locations: 对应的实际地址, 例如: classpath:/static/, file:/home/fs/
     */
    private List<Map<String, String[]>> resourceMappers;


    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public String getInterceptorBeanNames() {
        return interceptorBeanNames;
    }

    public void setInterceptorBeanNames(String interceptorBeanNames) {
        this.interceptorBeanNames = interceptorBeanNames;
    }

    public String getWelcomeUrl() {
        return welcomeUrl;
    }

    public void setWelcomeUrl(String welcomeUrl) {
        this.welcomeUrl = welcomeUrl;
    }

    public String[] getDefaultInterceptorExcludePathPatterns() {
        return defaultInterceptorExcludePathPatterns;
    }

    public void setDefaultInterceptorExcludePathPatterns(String[] defaultInterceptorExcludePathPatterns) {
        this.defaultInterceptorExcludePathPatterns = defaultInterceptorExcludePathPatterns;
    }

    public List<Map<String, String[]>> getResourceMappers() {
        return resourceMappers;
    }

    public void setResourceMappers(List<Map<String, String[]>> resourceMappers) {
        this.resourceMappers = resourceMappers;
    }
}
