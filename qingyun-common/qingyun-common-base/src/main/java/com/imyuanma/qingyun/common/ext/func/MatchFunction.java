package com.imyuanma.qingyun.common.ext.func;

/**
 * 匹配函数
 *
 * @author wangjy
 * @date 2023/07/06 23:11:47
 */
@FunctionalInterface
public interface MatchFunction<T> {
    /**
     * 匹配
     *
     * @param t
     * @return
     */
    boolean match(T t);
}
