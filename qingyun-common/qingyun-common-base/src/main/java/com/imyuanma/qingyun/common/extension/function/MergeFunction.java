package com.imyuanma.qingyun.common.extension.function;

/**
 * xxx
 *
 * @author wangjy
 * @date 2024/01/21 22:01:06
 */
@FunctionalInterface
public interface MergeFunction {
    /**
     * 结果合并
     *
     * @param result        上一次合并后的结果
     * @param currentResult 本次待合并的结果
     * @param throwable     本次扩展实现抛出的异常
     * @return 合并后的新结果
     */

    Object merge(Object result, Object currentResult, Throwable throwable);

    /**
     * 使用老结果
     *
     * @return
     */
    static MergeFunction oldest() {
        return (result, currentResult, throwable) -> result;
    }

    /**
     * 使用新结果
     *
     * @return
     */
    static MergeFunction newest() {
        return (result, currentResult, throwable) -> currentResult;
    }

}
