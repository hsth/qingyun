package com.imyuanma.qingyun.common.ext.func;

import com.imyuanma.qingyun.interfaces.common.ext.ExtNodeResult;

/**
 * 结果处理
 *
 * @author wangjy
 * @date 2023/07/07 23:08:10
 */
@FunctionalInterface
public interface ResultHandleFunction<R> extends HandleFunction<ExtNodeResult<R>> {

}
