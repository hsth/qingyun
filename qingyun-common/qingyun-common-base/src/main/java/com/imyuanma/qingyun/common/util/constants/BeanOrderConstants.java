package com.imyuanma.qingyun.common.util.constants;

/**
 * bean顺序常量
 *
 * @author wangjy
 * @date 2022/09/16 22:13:02
 */
public class BeanOrderConstants {
    /**
     * 最高优先级
     */
    public static final int HIGHEST_PRECEDENCE = Integer.MIN_VALUE;
    /**
     * 更高优先级
     */
    public static final int HIGHER_PRECEDENCE = -2000;
    /**
     * 高优先级
     */
    public static final int HIGH_PRECEDENCE = -1000;
    /**
     * 中等优先级
     */
    public static final int MID_PRECEDENCE = 1;
    /**
     * 低优先级
     */
    public static final int LOW_PRECEDENCE = 1000;
    /**
     * 更低优先级
     */
    public static final int LOWER_PRECEDENCE = 2000;
    /**
     * 最低优先级
     */
    public static final int LOWEST_PRECEDENCE = Integer.MAX_VALUE;
}
