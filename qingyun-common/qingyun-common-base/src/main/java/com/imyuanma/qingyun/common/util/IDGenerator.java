package com.imyuanma.qingyun.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;

/**
 * id生成
 *
 * @author wangjy
 */
public class IDGenerator {
    private static final Logger logger = LoggerFactory.getLogger(IDGenerator.class);
    /**
     * 上一个ID生成时的秒数
     */
    private static volatile long lastSeconds = 0L;
    /**
     * 当前毫秒生成的id序号
     */
    private static volatile long seq = 0L;

    /**
     * 最大序号
     */
    private static final long MAX_SEQ = 9999;

    /**
     * 获取id
     *
     * @return id
     */
    public static long getId() {
        long[] seq = seq();
        long millis = seq[0];
        long no = seq[1];

        StringBuilder sb = new StringBuilder();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis * 1000);

        sb.append(calendar.get(Calendar.YEAR) % 100);
        int m = calendar.get(Calendar.MONTH) + 1;
        if (m < 10) {
            sb.append("0");
        }
        sb.append(m);//月 4bit

        int d = calendar.get(Calendar.DAY_OF_MONTH);
        if (d < 10) {
            sb.append("0");
        }
        sb.append(d);//日 5bit

        int h = calendar.get(Calendar.HOUR_OF_DAY);
        if (h < 10) {
            sb.append("0");
        }
        sb.append(h);//时 5bit

        int min = calendar.get(Calendar.MINUTE);
        if (min < 10) {
            sb.append("0");
        }
        sb.append(min);//分 6bit

        int s = calendar.get(Calendar.SECOND);
        if (s < 10) {
            sb.append("0");
        }
        sb.append(s);//秒 6bit

//        int ms = calendar.get(Calendar.MILLISECOND);
//        if (ms < 10) {
//            sb.append("00");
//        } else if (ms < 100) {
//            sb.append("0");
//        }
//        sb.append(ms);//毫秒 10bit

        sb.append(String.format("%04d", no));//序号

        long id = Long.parseLong(sb.toString());

        logger.info("[ID生成器] 时间戳={},序号={},生成的ID={}", millis, no, id);
        return id;
    }

    private static long[] seq() {
        long millis = curSeconds();
        long no;
        if (millis < lastSeconds) {
            throw new RuntimeException("时钟回退导致无法生成订单号");
        }
        synchronized (IDGenerator.class) {
            if (millis < lastSeconds || seq == MAX_SEQ) {
                millis = curSeconds();
            }
            if (millis < lastSeconds) {
                throw new RuntimeException("时钟回退导致无法生成订单号");
            }
            if (millis > lastSeconds) {
                //当前时间戳大于上一次生成时的时间戳, 则重置
                lastSeconds = millis;
                seq = 0L;
            }
            no = ++seq;
            if (no > MAX_SEQ) {
                throw new RuntimeException("当前秒内生成的序号已经达到最大值");
            }
        }
        return new long[]{millis, no};
    }

    private static long curSeconds() {
        return System.currentTimeMillis() / 1000;
    }

    public static void main(String[] args) {
//        36位+6+4;
//        24 05 05 20 04 59 999;15
//        24 365   1440  59 999;14
//        24 365   86400    999;13;
//
//        3 8;
//        4 16;
//        5 32;
//        6 64;
//        7 128;
//        8 256;
//        9 512;
//        10 1024;
        for (int i = 0; i < 100000; i++) {
            System.out.println(getId());
        }
    }

}
