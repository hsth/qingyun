package com.imyuanma.qingyun.common.core.concurrent.rejected;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * 拒绝策略:丢弃任务
 *
 * @author wangjy
 * @date 2021/10/23 15:39:38
 */
public class QingYunDiscardPolicy extends ThreadPoolExecutor.DiscardPolicy {
    private static final Logger logger = LoggerFactory.getLogger(QingYunDiscardPolicy.class);
    @Override
    public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {
        logger.info("[拒绝策略-YoungDiscardPolicy] 执行拒绝策略:丢弃任务.");
        super.rejectedExecution(r, e);
    }
}