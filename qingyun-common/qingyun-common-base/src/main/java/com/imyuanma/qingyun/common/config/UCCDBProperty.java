package com.imyuanma.qingyun.common.config;

import com.imyuanma.qingyun.common.util.StringUtil;
import org.springframework.core.env.PropertySource;

import java.util.Properties;

/**
 * 配置中心db配置
 * Created by Admin on 2021/7/4.
 */
public class UCCDBProperty {
    /**
     * 驱动
     */
    private String driver;
    /**
     * db链接
     */
    private String url;
    /**
     * 用户名
     */
    private String user;
    /**
     * 密码
     */
    private String pwd;

    public void init(Properties properties) {
        driver = properties.getProperty("db-driver");
        url = properties.getProperty("db-url");
        user = properties.getProperty("db-user");
        pwd = properties.getProperty("db-pwd");
    }

    public void init(PropertySource properties) {
        driver = (String) properties.getProperty("db-driver");
        url = (String) properties.getProperty("db-url");
        user = (String) properties.getProperty("db-user");
        pwd = (String) properties.getProperty("db-pwd");
    }

    /**
     * 判断参数是否完整
     * @return
     */
    public boolean completed() {
        return StringUtil.isNotBlank(driver) && StringUtil.isNotBlank(url) && StringUtil.isNotBlank(user) && StringUtil.isNotBlank(pwd);
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
}
