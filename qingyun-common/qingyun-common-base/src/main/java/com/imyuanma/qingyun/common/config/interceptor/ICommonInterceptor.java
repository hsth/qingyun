package com.imyuanma.qingyun.common.config.interceptor;


import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;

/**
 * 公共的拦截器接口,继承自HandlerInterceptor,新增了部分方法来实现拦截器注册等功能
 * @author imrookie
 * @date 2018/10/7
 */
public interface ICommonInterceptor extends HandlerInterceptor {

    /**
     * 注册拦截器
     * 接管WebMvcConfig中的addInterceptors
     * @param registry
     */
    InterceptorRegistration addInterceptors(InterceptorRegistry registry);

}
