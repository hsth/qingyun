package com.imyuanma.qingyun.common.core.concurrent.rejected;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * 拒绝策略:主线程执行
 *
 * @author wangjy
 * @date 2021/10/23 15:38:29
 */
public class QingYunCallerRunsPolicy extends ThreadPoolExecutor.CallerRunsPolicy {
    private static final Logger logger = LoggerFactory.getLogger(QingYunCallerRunsPolicy.class);
    @Override
    public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {
        logger.info("[拒绝策略-YoungCallerRunsPolicy] 执行拒绝策略:主线程执行.");
        super.rejectedExecution(r, e);
    }
}