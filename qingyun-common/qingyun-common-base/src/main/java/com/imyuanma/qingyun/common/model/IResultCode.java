package com.imyuanma.qingyun.common.model;

/**
 * 结果码/错误码
 *
 * @author wangjy
 * @date 2021/12/26 23:41:35
 */
public interface IResultCode {
    /**
     * 错误码
     * @return
     */
    int code();

    /**
     * 提示文案
     * @return
     */
    String info();

    /**
     * 详情
     * @return
     */
    String detail();
}
