package com.imyuanma.qingyun.common.ext.func;

/**
 * 异常处理
 *
 * @author wangjy
 * @date 2023/07/07 23:18:10
 */
@FunctionalInterface
public interface ExceptionHandleFunction extends HandleFunction<Throwable> {

}
