package com.imyuanma.qingyun.common.model.response;


import com.imyuanma.qingyun.common.model.ECommonResultCode;
import com.imyuanma.qingyun.common.model.IResultCode;
import com.imyuanma.qingyun.common.model.PageQuery;

/**
 * 分页查询结果
 *
 * @author wangjy
 * @date 2021/12/26 23:48:05
 */
public class Page<T> extends Result<T> {
    /**
     * 页码，默认是第一页
     */
    private int pageNumber = 1;
    /**
     * 每页显示的记录数，默认是10
     */
    private int pageSize = 10;
    /**
     * 总记录数
     */
    private int total;

    public static <T> Page<T> success(T data, PageQuery pageQuery) {
        Page<T> page = new Page<T>();
        page.setPageNumber(pageQuery.getPageNumber());
        page.setPageSize(pageQuery.getPageSize());
        page.setTotal(pageQuery.getTotal());
        page.setData(data);
        return page;
    }

    public static <T> Page<T> success(T data) {
        Page<T> result = new Page<>();
        result.setData(data);
        return result;
    }

    public static <T> Page<T> error(String info) {
        Page<T> result = new Page<>();
        result.setCode(ECommonResultCode.FAIL.code);
        result.setInfo(info);
        return result;
    }

    public static <T> Page<T> error(IResultCode resultCode) {
        Page<T> result = new Page<>();
        result.setCode(resultCode.code());
        result.setInfo(resultCode.info());
        return result;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
