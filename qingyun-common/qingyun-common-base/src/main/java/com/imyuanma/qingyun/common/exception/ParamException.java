package com.imyuanma.qingyun.common.exception;

import com.imyuanma.qingyun.common.model.ECommonResultCode;

/**
 * 参数异常
 *
 * @author wangjy
 * @date 2022/05/04 16:19:44
 */
public class ParamException extends BaseException {
    public ParamException() {
        super(ECommonResultCode.PARAM_ERROR.code, ECommonResultCode.PARAM_ERROR.info, ECommonResultCode.PARAM_ERROR.detail);
    }

    public ParamException(String info) {
        super(ECommonResultCode.PARAM_ERROR.code, info, info);
    }

    public ParamException(String message, Throwable cause) {
        super(message, cause);
    }
}
