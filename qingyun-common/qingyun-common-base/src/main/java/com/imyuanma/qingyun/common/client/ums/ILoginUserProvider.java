package com.imyuanma.qingyun.common.client.ums;

import com.imyuanma.qingyun.common.extension.annotation.Extension;
import com.imyuanma.qingyun.interfaces.ums.model.LoginUserDTO;

/**
 * 登录用户提供者
 *
 * @author wangjy
 * @date 2024/03/08 00:06:35
 */
@Extension(value = "qingyun:ums:sdk:loginUserProvider", desc = "登录用户提供")
public interface ILoginUserProvider {
    /**
     * 获取当前登录用户
     *
     * @return 当前登录用户信息
     */
    LoginUserDTO getLoginUser();

}
