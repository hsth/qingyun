package com.imyuanma.qingyun.common.util;

import com.alibaba.fastjson.JSON;

import java.util.List;


/**
 * json工具类
 *
 * @author wangjy
 * @date 2021/12/29 00:15:20
 */
public class JsonUtil {

    /**
     * 对象转json
     *
     * @param object 待转为json字符串的对象
     * @return
     */
    public static String toJson(Object object) {
        return JSON.toJSONString(object);
    }

    /**
     * json字符串转bean
     *
     * @param json  json字符串
     * @param clazz 目标类class对象
     * @param <T>
     * @return
     */
    public static <T> T toBean(String json, Class<T> clazz) {
        return JSON.parseObject(json, clazz);
    }

    /**
     * json数组字符串转bean集合
     *
     * @param json  json数组字符串
     * @param clazz 集合
     * @param <T>
     * @return
     */
    public static <T> List<T> toList(String json, Class<T> clazz) {
        return JSON.parseArray(json, clazz);
    }
}
