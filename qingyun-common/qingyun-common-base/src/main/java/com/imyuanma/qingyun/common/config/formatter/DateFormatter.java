package com.imyuanma.qingyun.common.config.formatter;

import com.imyuanma.qingyun.common.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * 日期转换,springmvc
 * Created by rookie on 2017/6/28.
 */
@Component
public class DateFormatter implements Formatter<Date> {

    private static final Logger logger = LoggerFactory.getLogger(DateFormatter.class);

    @Override
    public Date parse(String text, Locale locale) throws ParseException {
        logger.debug("[时间参数绑定格式转换] 待转换时间字符串为:{}", text);
        Date date = null;
        if (text != null && StringUtil.isNotBlank(text)){
            String formatMode = this.getFormatMode(text);//匹配时间格式
            if (formatMode == null) {
                logger.error("[时间参数绑定格式转换] *异常* 匹配不到对应的时间格式,待格式化文本为:{}", text);
                throw new IllegalArgumentException("Invalid date value '" + text + "'");
            }
            SimpleDateFormat format = new SimpleDateFormat(formatMode);
            date = format.parse(text);
        }
        return date;
    }

    @Override
    public String print(Date object, Locale locale) {
        System.out.println("print-----");
        return null;
    }

    /**
     * 匹配时间格式
     * @param dateStr 时间字符串
     * @return
     */
    private String getFormatMode(String dateStr){
        String result = null;
        if(dateStr.matches("\\d{4}-\\d{1,2}-\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2}.\\d+")) {
            result = "yyyy-MM-dd HH:mm:ss.SSS";
        } else if(dateStr.matches("\\d{4}-\\d{1,2}-\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2}")) {
            result = "yyyy-MM-dd HH:mm:ss";
        } else if(dateStr.matches("\\d{4}-\\d{1,2}-\\d{1,2} \\d{1,2}:\\d{1,2}")) {
            result = "yyyy-MM-dd HH:mm";
        } else if(dateStr.matches("\\d{4}-\\d{1,2}-\\d{1,2} \\d{1,2}")) {
            result = "yyyy-MM-dd HH";
        } else if(dateStr.matches("\\d{4}-\\d{1,2}-\\d{1,2}")) {
            result = "yyyy-MM-dd";
        } else if(dateStr.matches("\\d{4}-\\d{1,2}")) {
            result = "yyyy-MM";
        } else if(dateStr.matches("\\d{4}/\\d{1,2}/\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2}.\\d+")) {
            result = "yyyy/MM/dd HH:mm:ss.SSS";
        } else if(dateStr.matches("\\d{4}/\\d{1,2}/\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2}")) {
            result = "yyyy/MM/dd HH:mm:ss";
        } else if(dateStr.matches("\\d{4}/\\d{1,2}/\\d{1,2} \\d{1,2}:\\d{1,2}")) {
            result = "yyyy/MM/dd HH:mm";
        } else if(dateStr.matches("\\d{4}/\\d{1,2}/\\d{1,2} \\d{1,2}")) {
            result = "yyyy/MM/dd HH";
        } else if(dateStr.matches("\\d{4}/\\d{1,2}/\\d{1,2}")) {
            result = "yyyy/MM/dd";
        } else if(dateStr.matches("\\d{4}/\\d{1,2}")) {
            result = "yyyy/MM";
        } else if(dateStr.matches("\\d{4}年\\d{1,2}月\\d{1,2}日\\d{1,2}时\\d{1,2}分\\d{1,2}秒")) {
            result = "yyyy年MM月dd日HH时mm分ss秒";
        } else if(dateStr.matches("\\d{4}年\\d{1,2}月\\d{1,2}日\\d{1,2}时\\d{1,2}分")) {
            result = "yyyy年MM月dd日HH时mm分";
        } else if(dateStr.matches("\\d{4}年\\d{1,2}月\\d{1,2}日\\d{1,2}时")) {
            result = "yyyy年MM月dd日HH时";
        } else if(dateStr.matches("\\d{4}年\\d{1,2}月\\d{1,2}日")) {
            result = "yyyy年MM月dd日";
        } else if(dateStr.matches("\\d{4}年\\d{1,2}月")) {
            result = "yyyy年MM月";
        } else if(dateStr.matches("\\d{4}年\\d{1,2}月\\d{1,2}日 \\d{1,2}时\\d{1,2}分\\d{1,2}秒")) {
            result = "yyyy年MM月dd日 HH时mm分ss秒";
        } else if(dateStr.matches("\\d{4}年\\d{1,2}月\\d{1,2}日 \\d{1,2}时\\d{1,2}分")) {
            result = "yyyy年MM月dd日 HH时mm分";
        } else if(dateStr.matches("\\d{4}年\\d{1,2}月\\d{1,2}日 \\d{1,2}时")) {
            result = "yyyy年MM月dd日 HH时";
        } else if(dateStr.matches("\\d{14}.\\d+")) {
            result = "yyyyMMddHHmmss.SSS";
        } else if(dateStr.matches("\\d{14}")) {
            result = "yyyyMMddHHmmss";
        } else if(dateStr.matches("\\d{12}")) {
            result = "yyyyMMddHHmm";
        } else if(dateStr.matches("\\d{10}")) {
            result = "yyyyMMddHH";
        } else if(dateStr.matches("\\d{8}")) {
            result = "yyyyMMdd";
        } else if(dateStr.matches("\\d{6}")) {
            result = "yyyyMM";
        }
        return result;
    }
}
