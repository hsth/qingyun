package com.imyuanma.qingyun.common.model;

import org.apache.ibatis.session.RowBounds;

/**
 * 分页查询参数
 *
 * @author wangjy
 * @date 2022/05/21 22:58:29
 */
public class PageQuery extends RowBounds {

    /**
     * 页码，默认是第一页
     */
    private int pageNumber = 1;
    /**
     * 每页显示的记录数，默认是10
     */
    private int pageSize = 10;
    /**
     * 总记录数
     */
    private int total = 0;
    /**
     * 自动查询总条数
     * 当为true且total为0时会自动查询数据条数并填充到total
     */
    private boolean autoQueryTotal = true;

    public PageQuery(Integer pageNumber, Integer pageSize) {
        super(0, pageSize != null ? pageSize : 10);
        this.pageNumber = pageNumber != null ? pageNumber : 1;
        this.pageSize = pageSize != null ? pageSize : 10;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public boolean isAutoQueryTotal() {
        return autoQueryTotal;
    }

    public void setAutoQueryTotal(boolean autoQueryTotal) {
        this.autoQueryTotal = autoQueryTotal;
    }
}
