package com.imyuanma.qingyun.common.core.excel;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * excel导出接口
 * Created by rookie on 2017/11/3.
 */
public interface IExcelExport extends IExcel {

    /**
     * 写入头
     * @param headers
     */
    void insertHead(String[] headers);

    /**
     * 写入一条数据
     * @param data
     * @return
     */
    int insertRow(List<Object> data);

    /**
     * 写入一条数据
     * @param data
     * @return
     */
    int insertRow(Object[] data);

    /**
     * 批量写入数据
     * @param list
     * @return
     */
    int insertRows(List<List<Object>> list);

    /**
     * 插入bean的集合
     * @param list
     * @param cls
     * @param <T>
     * @return
     */
    <T> void insertBeanList(List<T> list, Class<T> cls) throws NoSuchMethodException;

    /**
     * 写入响应
     * @param response
     * @param fileName
     */
    void write2Response(HttpServletResponse response, String fileName);

    /**
     * 写入响应,兼容IE
     * @param request
     * @param response
     * @param fileName
     */
    void write2Response(HttpServletRequest request, HttpServletResponse response, String fileName);

    /**
     * 写入流
     * @param outputStream
     */
    void write2Stream(OutputStream outputStream) throws IOException;
}
