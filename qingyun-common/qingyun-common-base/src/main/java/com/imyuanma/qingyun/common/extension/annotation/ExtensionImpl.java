package com.imyuanma.qingyun.common.extension.annotation;

import org.springframework.core.Ordered;

/**
 * 扩展点实现
 * 用在扩展点实现类上, 描述扩展点实现的相关信息
 *
 * @author wangjy
 * @date 2024/01/21 21:55:26
 */
public @interface ExtensionImpl {
    /**
     * 扩展点实现编码
     *
     * @return
     */
    String value() default "";

    /**
     * 扩展点实现说明
     *
     * @return
     */
    String desc() default "";

    /**
     * 业务标识
     * 用来区分不同的实现, 可以用来过滤扩展实现
     *
     * @return
     */
    String businessTag() default "";

    /**
     * 执行顺序
     *
     * @return
     */
    int order() default Ordered.LOWEST_PRECEDENCE;
}
