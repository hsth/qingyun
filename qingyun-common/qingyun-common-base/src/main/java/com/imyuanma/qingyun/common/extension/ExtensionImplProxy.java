package com.imyuanma.qingyun.common.extension;

import com.imyuanma.qingyun.common.extension.function.EmptyImplFunction;
import com.imyuanma.qingyun.common.extension.function.FilterFunction;
import com.imyuanma.qingyun.common.extension.function.MergeFunction;
import com.imyuanma.qingyun.common.extension.function.StopFunction;
import com.imyuanma.qingyun.common.extension.sort.SortAide;
import com.imyuanma.qingyun.common.util.bean.ApplicationContextHolder;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 扩展点动态代理执行器
 *
 * @author wangjy
 * @date 2024/01/21 22:26:50
 */
public class ExtensionImplProxy<T> implements InvocationHandler {
    /**
     * 被代理接口
     */
    private Class<T> proxyInterfaces;
    /**
     * 默认实现
     */
    private T defaultImpl;
    /**
     * 实现为空时(且defaultImpl也为null)的处理函数
     */
    private EmptyImplFunction emptyImplFunction = EmptyImplFunction.throwException();
    /**
     * 扩展点结果合并函数
     * 当执行完一个实现后, 会调用此方法将上次的和这次的结果进行合并
     */
    private MergeFunction mergeFunction = MergeFunction.newest();
    /**
     * 中断函数
     * 当执行完一个实现并处理合并后, 会调用中断函数判断是否继续执行下一个扩展点, 返回true则停止继续, 返回当前结果
     */
    private StopFunction stopFunction = StopFunction.stopWhenFirst();
    /**
     * 过滤函数
     * 从N个扩展实现中选择出本次待执行的实现
     */
    private FilterFunction<T> filterFunction = FilterFunction.all();


    public ExtensionImplProxy(Class<T> proxyInterfaces) {
        this.proxyInterfaces = proxyInterfaces;
    }

    public ExtensionImplProxy(Class<T> proxyInterfaces, EmptyImplFunction emptyImplFunction) {
        this.proxyInterfaces = proxyInterfaces;
        this.emptyImplFunction = emptyImplFunction;
    }

    public ExtensionImplProxy(Class<T> proxyInterfaces, T defaultImpl) {
        this.proxyInterfaces = proxyInterfaces;
        this.defaultImpl = defaultImpl;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        // 获取所有扩展实现
        List<T> list = this.impls();
        // 排序
        list = this.sort(list);
        // 过滤出符合条件的实现
        list = this.filter(list);
        // 如果扩展点实现为空
        if (list.isEmpty()) {
            if (this.defaultImpl != null) {
                return method.invoke(this.defaultImpl, args);
            }
            emptyImplFunction.handle();
            return null;
        }
        // 只有一个实现时, 直接执行
        if (list.size() == 1) {
            return method.invoke(list.get(0), args);
        }
        // 一次执行扩展点, 并处理中断逻辑
        Object result = null;
        int size = list.size();
        for (int i = 0; i < list.size(); i++) {
            T impl = list.get(i);
            Throwable throwable = null;
            Object currentResult = null;
            try {
                currentResult = method.invoke(impl, args);
            } catch (Throwable t) {
                throwable = t;
            }
            // 结果合并
            result = this.doMerge(result, currentResult, throwable, i, size);
            // 中断逻辑
            if (this.doStop(result, throwable, i, size)) {
                break;
            }
        }
        return result;
    }

    private Object doMerge(Object result, Object currentResult, Throwable throwable, int i, int size) {
        return i > 0 ? mergeFunction.merge(result, currentResult, throwable) : currentResult;
    }


    private boolean doStop(Object result, Throwable throwable, int i, int size) {
        return stopFunction.stop(result, throwable);
    }

    /**
     * 获取扩展实现
     *
     * @return
     */
    private List<T> impls() {
        return Optional.ofNullable(ApplicationContextHolder.getBeans(this.proxyInterfaces)).orElse(new ArrayList<>());
    }

    /**
     * 排序
     *
     * @param list
     * @return
     */
    private List<T> sort(List<T> list) {
        return SortAide.sort(list);
    }

    /**
     * 过滤出合适的实现
     *
     * @param list
     * @return
     */
    private List<T> filter(List<T> list) {
        return list.stream().filter(filterFunction::filter).collect(Collectors.toList());
    }

}
