package com.imyuanma.qingyun.common.core.excel;

import org.apache.poi.ss.usermodel.Sheet;

import java.util.List;
import java.util.Map;

/**
 * excel导入接口
 * Created by rookie on 2017/8/17.
 */
public interface IExcelImport extends IExcel {
    /**
     * 获取指定索引的sheet页
     * @param sheetIndex sheet页的索引
     * @return
     */
    Sheet getSheetAt(int sheetIndex);

    /**
     * 获取所有sheet页
     * @return
     */
    Sheet[] getAllSheet();

    /**
     * 获取excel中的所有数据,组装成List<map>格式
     * @return
     */
    List<Map<String, Object>> getAllData();

    /**
     * 获取指定sheet页的数据
     * @param sheet
     * @return
     */
    List<Map<String, Object>> getDataBySheet(Sheet sheet);

    /**
     * 获取指定索引的sheet页的数据
     * @param sheetIndex
     * @return
     */
    List<Map<String, Object>> getDataBySheetNum(int sheetIndex);

    /**
     * 获取待导入的数据
     * @return
     */
    List<Map<String, Object>> getImportData();

    /**
     * 获取待导入数据(返回指定类型)
     * @param cls
     * @return
     */
    <T> List<T> getImportDataAsBean(Class<T> cls) throws NoSuchMethodException;
}
