package com.imyuanma.qingyun.common.extension.sort;

import com.imyuanma.qingyun.common.extension.annotation.ExtensionImpl;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 排序辅助类
 *
 * @author wangjy
 * @date 2024/01/21 22:15:19
 */
public class SortAide<T> implements Comparable<SortAide<T>> {
    /**
     * 待排序实现类
     */
    private T target;
    /**
     * 顺序
     */
    private int order = Ordered.LOWEST_PRECEDENCE;

    public SortAide(T target) {
        this.target = target;
    }

    /**
     * 构造排序辅助对象
     *
     * @param target
     * @param <T>
     * @return
     */
    public static <T> SortAide<T> build(T target) {
        SortAide<T> sortAide = new SortAide<>(target);
        // 1.优先从扩展点注解中拿
        Integer order = sortAide.orderFromExtension();//没有则从spring的@0rder注解中拿
        // 2.没有则从spring的@0rder注解中拿
        if (order == null) {
            order = sortAide.orderFromOrder();
        }
        // 3.还没有则判断是否实现了Ordered接口，从getOrder()方法中拿
        if (order == null) {
            order = sortAide.orderFromOrdered();
        }
        // 4.设置顺序
        if (order != null) {
            sortAide.order = order;
        }
        return sortAide;
    }

    /**
     * 扩展点排序
     *
     * @param list
     * @param <T>
     * @return
     */
    public static <T> List<T> sort(List<T> list) {
        return list.stream()
                .filter(Objects::nonNull)
                .map(SortAide::build)
                .sorted()
                .map(SortAide::getTarget)
                .collect(Collectors.toList());
    }

    private Integer orderFromExtension() {
        ExtensionImpl extension = this.target.getClass().getAnnotation(ExtensionImpl.class);
        return extension != null ? extension.order() : null;
    }

    private Integer orderFromOrder() {
        Order orderAnnotation = this.target.getClass().getAnnotation(Order.class);
        return orderAnnotation != null ? orderAnnotation.value() : null;
    }

    private Integer orderFromOrdered() {
        return this.target instanceof Ordered ? ((Ordered) this.target).getOrder() : null;
    }

    @Override
    public int compareTo(SortAide<T> o) {
        return Long.compare(order, o.order);
    }

    public T getTarget() {
        return target;
    }
}
