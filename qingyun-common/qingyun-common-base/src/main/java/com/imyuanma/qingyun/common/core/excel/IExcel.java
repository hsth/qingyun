package com.imyuanma.qingyun.common.core.excel;

/**
 * excel顶级接口
 * Created by rookie on 2017/8/17.
 */
public interface IExcel {
    /**
     * 返回当前excel对象是否为空
     * @return
     */
    boolean isEmpty();

    /**
     * 返回当前excel的sheet页数
     * @return
     */
    int getSheetNum();


}
