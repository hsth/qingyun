package com.imyuanma.qingyun.common.extension.annotation;

/**
 * 扩展点描述
 * 用在扩展点接口上, 描述扩展点相关信息
 *
 * @author wangjy
 * @date 2024/01/21 21:55:26
 */
public @interface Extension {
    /**
     * 扩展点编码
     *
     * @return
     */
    String value() default "";

    /**
     * 扩展点说明
     *
     * @return
     */
    String desc() default "";
}
