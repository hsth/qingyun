package com.imyuanma.qingyun.common.extension.function;

import java.util.Objects;

/**
 * xxx
 *
 * @author wangjy
 * @date 2024/01/21 22:04:38
 */
public interface StopFunction {
    /**
     * 是否终止
     *
     * @param result 扩展点结果
     * @param t      扩展点抛出的异常
     * @return true表示停止继续下一个扩展点实现
     */
    boolean stop(Object result, Throwable t);

    /**
     * 或者
     *
     * @param function
     */
    default StopFunction or(StopFunction function) {
        Objects.requireNonNull(function);
        return (result, t) -> this.stop(result, t) || function.stop(result, t);
    }


    /**
     * 并且
     *
     * @param function
     */
    default StopFunction and(StopFunction function) {
        Objects.requireNonNull(function);
        return (result, t) -> this.stop(result, t) && function.stop(result, t);
    }

    /**
     * 第一次执行后停止
     *
     * @return
     */
    static StopFunction stopWhenFirst() {
        return ((result, t) -> true);
    }

    /**
     * 不中断
     *
     * @return
     */
    static StopFunction notStop() {
        return ((result, t) -> false);
    }

    /**
     * 发生异常时停止
     *
     * @return
     */
    static StopFunction stopWhenException() {
        return ((result, t) -> t != null);
    }

    /**
     * 第一个没有抛出异常的实现执行后停止
     *
     * @return
     */
    static StopFunction stopWhenNotException() {
        return ((result, t) -> t == null);
    }


    /**
     * 第一个返回结果不为空的实现执行后停止
     *
     * @return
     */
    static StopFunction stopWhenResultIsNotNull() {
        return ((result, t) -> t == null);
    }

}
