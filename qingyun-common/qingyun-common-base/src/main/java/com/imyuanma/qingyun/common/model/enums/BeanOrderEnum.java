package com.imyuanma.qingyun.common.model.enums;


import com.imyuanma.qingyun.common.util.constants.BeanOrderConstants;

/**
 * bean顺序枚举
 *
 * @author wangjy
 * @date 2022/09/16 21:59:55
 */
public enum BeanOrderEnum {

    HIGHEST_PRECEDENCE(BeanOrderConstants.HIGHEST_PRECEDENCE, "最高优先级"),
    HIGHER_PRECEDENCE(BeanOrderConstants.HIGHER_PRECEDENCE, "更高优先级"),
    HIGH_PRECEDENCE(BeanOrderConstants.HIGH_PRECEDENCE, "高优先级"),
    MID_PRECEDENCE(BeanOrderConstants.MID_PRECEDENCE, "中等优先级"),
    LOW_PRECEDENCE(BeanOrderConstants.LOW_PRECEDENCE, "低优先级"),
    LOWER_PRECEDENCE(BeanOrderConstants.LOWER_PRECEDENCE, "更低优先级"),
    LOWEST_PRECEDENCE(BeanOrderConstants.LOWEST_PRECEDENCE, "最低优先级"),
    ;
    public final int order;
    public final String text;

    BeanOrderEnum(int order, String text) {
        this.order = order;
        this.text = text;
    }

    public int getOrder() {
        return order;
    }

    public String getText() {
        return text;
    }
}
