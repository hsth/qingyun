package com.imyuanma.qingyun.common.util;

import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.nio.charset.StandardCharsets;
import java.util.Properties;

/**
 * xxx
 *
 * @author wangjy
 * @date 2023/05/03 15:54:48
 */
public class MailUtil {

    /**
     * 构造邮件发送实现
     *
     * @param host
     * @param email
     * @param pwd
     * @return
     */
    public static JavaMailSenderImpl newMailSender(String host, String email, String pwd) {
        JavaMailSenderImpl sender = new JavaMailSenderImpl();
        sender.setHost(host);
        sender.setPort(465);
        sender.setUsername(email);
        sender.setPassword(pwd);
        sender.setProtocol("smtp");
        sender.setDefaultEncoding(StandardCharsets.UTF_8.name());
        // 加密传输
        Properties properties = new Properties();
        properties.setProperty("mail.smtp.auth", "true");
        properties.setProperty("mail.smtp.timeout", "5000");
        properties.setProperty("mail.smtp.starttls.enable", "true");
        properties.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.setProperty("mail.smtp.socketFactory.fallback", "false");
        properties.setProperty("mail.smtp.ssl.enable", "true");
        sender.setJavaMailProperties(properties);
        return sender;
    }
}
