package com.imyuanma.qingyun.common.client.ums;

import com.imyuanma.qingyun.common.extension.ExtensionImplProvider;
import com.imyuanma.qingyun.interfaces.ums.model.LoginUserDTO;

/**
 * 登录用户工具类
 *
 * @author wangjy
 * @date 2022/07/16 17:04:45
 */
public class LoginUserHolder {
    private static final ILoginUserProvider DEFAULT_IMPL = new ILoginUserProvider() {
        @Override
        public LoginUserDTO getLoginUser() {
            return null;
        }
    };
    /**
     * 获取当前登录用户
     *
     * @return 当前登录用户信息
     */
    public static LoginUserDTO getLoginUser() {
        return ExtensionImplProvider.of(ILoginUserProvider.class, DEFAULT_IMPL).getLoginUser();
    }

    /**
     * 获取当前登录用户id
     *
     * @return 当前登录用户id
     */
    public static Long getLoginUserId() {
        LoginUserDTO userDTO = getLoginUser();
        return userDTO != null ? userDTO.getUserId() : null;
    }
}
