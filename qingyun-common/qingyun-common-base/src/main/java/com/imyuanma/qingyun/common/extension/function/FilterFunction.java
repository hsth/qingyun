package com.imyuanma.qingyun.common.extension.function;

/**
 * xxx
 *
 * @author wangjy
 * @date 2024/01/21 22:49:03
 */
@FunctionalInterface
public interface FilterFunction<T> {
    /**
     * 过滤方法
     *
     * @param impl
     * @return true表示要执行, false表示丢弃
     */
    boolean filter(T impl);

    /**
     * 全部通过
     * @param <T>
     * @return
     */
    static <T> FilterFunction<T> all() {
        return (T impl) -> true;
    }
}
