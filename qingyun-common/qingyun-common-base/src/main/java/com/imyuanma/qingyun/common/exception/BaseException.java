package com.imyuanma.qingyun.common.exception;


import com.imyuanma.qingyun.common.model.ECommonResultCode;
import com.imyuanma.qingyun.common.model.IResultCode;
import com.imyuanma.qingyun.interfaces.common.ext.ExtNodeResult;

/**
 * 基础异常
 */
public class BaseException extends RuntimeException {
    /**
     * 错误码
     */
    private int code = ECommonResultCode.FAIL.code();
    /**
     * 错误信息
     */
    private String info = ECommonResultCode.FAIL.info();
    /**
     * 详情
     */
    private String detail = ECommonResultCode.FAIL.detail();

    public BaseException() {
        super();
    }

    public BaseException(Throwable cause) {
        super(cause);
    }

    public BaseException(String info) {
        super(info);
        this.info = info;
    }

    public BaseException(String message, Throwable cause) {
        super(message, cause);
        this.info = message;
    }

    public BaseException(int code, String info) {
        super(info);
        this.code = code;
        this.info = info;
    }

    public BaseException(int code, String info, String detail) {
        super(detail);
        this.code = code;
        this.info = info;
        this.detail = detail;
    }

    public BaseException(IResultCode resultCode) {
        this(resultCode.code(), resultCode.info(), resultCode.detail());
    }

    public BaseException(ExtNodeResult resultCode) {
        this(resultCode.getCode(), resultCode.getInfo());
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
