package com.imyuanma.qingyun.common.model;

/**
 * 公共错误码枚举
 *
 * @author wangjy
 * @date 2021/12/26 23:43:57
 */
public enum ECommonResultCode implements IResultCode {

    SUCCESS(0, "成功", "成功"),
    FAIL(-1, "失败", "失败"),
    PARAM_INVALID(-11, "参数无效", "参数无效"),
    PARAM_ERROR(-12, "参数错误", "参数错误"),
    REPEAT_COMMIT(-13, "重复提交", "重复提交"),
    SESSION_TIMEOUT(-101, "登录超时，请重新登录！", "会话超时"),
    BUSINESS_EXCEPTION(-102, "业务异常", "业务异常"),
    NOT_AUTH(-103, "无权限访问", "无权限访问"),
    SYSTEM_EXCEPTION(-999, "系统异常", "系统异常"),
    ;

    /**
     * 错误码
     */
    public final int code;
    /**
     * 文案
     */
    public final String info;
    /**
     * 详情
     */
    public final String detail;

    ECommonResultCode(int code, String info, String detail) {
        this.code = code;
        this.info = info;
        this.detail = detail;
    }

    /**
     * 错误码
     *
     * @return
     */
    @Override
    public int code() {
        return code;
    }

    /**
     * 提示文案
     *
     * @return
     */
    @Override
    public String info() {
        return info;
    }

    /**
     * 详情
     *
     * @return
     */
    @Override
    public String detail() {
        return detail;
    }
}
