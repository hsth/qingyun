package com.imyuanma.qingyun.common.core.concurrent;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 自定义线程工厂
 *
 * @author wangjy
 * @date 2021/10/16 15:59:21
 */
public class QingYunThreadFactory implements ThreadFactory {

    /**
     * 线程数
     */
    private final AtomicInteger threadNumber = new AtomicInteger(1);
    /**
     * 线程名前缀
     */
    private final String namePrefix;

    public QingYunThreadFactory(String threadPoolName) {
        SecurityManager s = System.getSecurityManager();
        namePrefix = "pool-" + threadPoolName + "-";
    }

    @Override
    public Thread newThread(Runnable r) {
        Thread t = new Thread(r, namePrefix + threadNumber.getAndIncrement());
        if (t.isDaemon()) {
            t.setDaemon(false);
        }
        if (t.getPriority() != Thread.NORM_PRIORITY) {
            t.setPriority(Thread.NORM_PRIORITY);
        }
        return t;
    }
}
