package com.imyuanma.qingyun.common.util;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 集合工具类
 * Created by Admin on 2021/6/20.
 */
public class CollectionUtil {

    public static <E> ArrayList<E> newArrayList() {
        return new ArrayList<E>();
    }

    public static <E> ArrayList<E> newArrayList(int expectedSize) {
        return new ArrayList<E>(computeArrayListCapacity(expectedSize));
    }

    public static <E> ArrayList<E> newArrayList(E... elements) {
        if (elements == null) {
            return newArrayList();
        }
        ArrayList<E> list = newArrayList(elements.length);
        Collections.addAll(list, elements);
        return list;
    }

    static int computeArrayListCapacity(int expectedSize) {
        return (int) Long.min(5L + (long) expectedSize + (long) (expectedSize / 10), Integer.MAX_VALUE);
    }

    public static <K, V> HashMap<K, V> newHashMap() {
        return new HashMap<>();
    }

    public static <K, V> HashMap<K, V> newHashMap(int expectedSize) {
        return new HashMap<K, V>(capacity(expectedSize));
    }

    static int capacity(int expectedSize) {
        if (expectedSize < 3) {
            return expectedSize + 1;
        } else {
            return expectedSize < 1073741824 ? (int) (expectedSize / 0.75F + 1.0F) : 2147483647;
        }
    }

    public static <K, E> Map<K, E> toMap(List<E> list, Function<? super E, ? extends K> keyMapper) {
        return toMap(list, keyMapper, Function.identity());
    }

    public static <K, V, E> Map<K, V> toMap(List<E> list, Function<? super E, ? extends K> keyMapper, Function<? super E, ? extends V> valueMapper) {
        return list.stream().collect(Collectors.toMap(keyMapper, valueMapper, (o, n) -> n));
    }

    public static boolean isEmpty(Collection collection) {
        return collection == null || collection.isEmpty();
    }

    public static boolean isNotEmpty(Collection collection) {
        return !isEmpty(collection);
    }

    public static boolean isEmpty(Map map) {
        return map == null || map.isEmpty();
    }

    public static boolean isNotEmpty(Map map) {
        return !isEmpty(map);
    }

    public static boolean isEmpty(Object[] arr) {
        return arr == null || arr.length == 0;
    }

    public static boolean isNotEmpty(Object[] arr) {
        return !isEmpty(arr);
    }
}
