package com.imyuanma.qingyun.common.util;


import java.util.UUID;

/**
 * 字符串工具类
 */
public class StringUtil {

    /**
     * 判断字符串为非空
     *
     * @param str
     * @return
     */
    public static boolean isNotBlank(String str) {
        return !isBlank(str);
    }

    /**
     * 判断字符串为空
     *
     * @param str
     * @return
     */
    public static boolean isBlank(String str) {
        if (null == str || "".equals(str.trim())) {
            return true;
        }
        return false;
    }

    /**
     * xx_xx_xx格式字符串转驼峰格式
     * 例如: USER_NAME ==> userName
     *
     * @param value
     * @return
     */
    public static String toCamel(String value) {
        StringBuilder sbResult = new StringBuilder();
        String[] terms = value.toLowerCase().split("_");
        for (int i = 0; i < terms.length; i++) {
            if (isBlank(terms[i])) {
                continue;
            }
            if (i == 0) {
                sbResult.append(terms[i]);
            }
            if (i != 0) {
                sbResult.append(terms[i].substring(0, 1).toUpperCase());
                sbResult.append(terms[i].substring(1));
            }
        }
        return sbResult.toString();
    }

    /**
     * 转为首字母大写的驼峰
     * 例如: USER_NAME ==> UserName
     *
     * @param value
     * @return
     */
    public static String toFirstUpperCamel(String value) {
        String camel = toCamel(value);
        if (isBlank(camel)) {
            return "";
        }
        if (camel.length() == 1) {
            return camel.toUpperCase();
        }
        return camel.substring(0, 1).toUpperCase() + camel.substring(1);
    }

    /**
     * 首字母大写
     *
     * @param value
     * @return
     */
    public static String toFirstUpper(String value) {
        return value.substring(0, 1).toUpperCase() + value.substring(1);
    }

    /**
     * 首字母小写
     *
     * @param value
     * @return
     */
    public static String toFirstLower(String value) {
        return value.substring(0, 1).toLowerCase() + value.substring(1);
    }

    /**
     * 比较2个字符串值是否相等
     *
     * @param str1
     * @param str2
     * @return
     */
    public static boolean equals(String str1, String str2) {
        if (str1 == null && str2 == null) {
            return true;
        }
        if (str1 != null) {
            return str1.equals(str2);
        }
        return false;
    }

    /**
     * 获取默认值
     *
     * @param str1
     * @param str2
     * @return
     */
    public static String getDefaultValue(String str1, String str2) {
        if (StringUtil.isNotBlank(str1)) {
            return str1;
        } else {
            return str2;
        }
    }

    /**
     * 获取第一个有效的字符串
     *
     * @param strArr
     * @return
     */
    public static String getFirstValid(String... strArr) {
        if (strArr == null || strArr.length == 0) {
            return null;
        }
        for (String s : strArr) {
            if (StringUtil.isNotBlank(s)) {
                return s;
            }
        }
        return null;
    }

    /**
     * 获取默认值
     *
     * @param str1
     * @param str2
     * @return
     */
    public static String getDefaultValue(Object str1, String str2) {
        return getDefaultValue(valueOf(str1), str2);
    }

    /**
     * 对象转字符串, null时返回null
     *
     * @param obj
     * @return
     */
    public static String valueOf(Object obj) {
        return obj != null ? obj.toString() : null;
    }

    /**
     * 获取uuid
     *
     * @return
     */
    public static String getUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    /**
     * 截取字符串
     *
     * @param str       字符串
     * @param maxLength 最大长度
     * @return
     */
    public static String subString(String str, int maxLength) {
        if (str == null || str.length() <= maxLength) {
            return str;
        }
        return str.substring(0, maxLength);
    }

}
