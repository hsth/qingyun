package com.imyuanma.qingyun.common.ext.func;

/**
 * 合并函数
 *
 * @author wangjy
 * @date 2023/07/06 23:16:54
 */
@FunctionalInterface
public interface MergeFunction<T> {
    /**
     * 合并
     *
     * @param o 老的
     * @param n 新的
     * @return 返回合并后的对象, 可能是o, 也可能是n, 也可能是别的对象
     */
    T merge(T o, T n);
}
