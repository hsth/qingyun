package com.imyuanma.qingyun.common.extension;

import com.imyuanma.qingyun.common.extension.function.EmptyImplFunction;

import java.lang.reflect.Proxy;

/**
 * xxx
 *
 * @author wangjy
 * @date 2024/01/21 23:05:37
 */
public class ExtensionImplProvider {
    public static <T> T of(Class<T> clz) {
        return (T) Proxy.newProxyInstance(clz.getClassLoader(), new Class[]{clz}, new ExtensionImplProxy<T>(clz));
    }

    public static <T> T of(Class<T> clz, EmptyImplFunction emptyImplFunction) {
        return (T) Proxy.newProxyInstance(clz.getClassLoader(), new Class[]{clz}, new ExtensionImplProxy<T>(clz, emptyImplFunction));
    }

    public static <T> T of(Class<T> clz, T defaultImpl) {
        return (T) Proxy.newProxyInstance(clz.getClassLoader(), new Class[]{clz}, new ExtensionImplProxy<T>(clz, defaultImpl));
    }

}
