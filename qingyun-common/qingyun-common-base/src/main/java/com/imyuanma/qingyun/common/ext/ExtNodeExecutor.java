package com.imyuanma.qingyun.common.ext;

import com.imyuanma.qingyun.common.exception.BaseException;
import com.imyuanma.qingyun.common.ext.func.ExceptionHandleFunction;
import com.imyuanma.qingyun.common.ext.func.MatchFunction;
import com.imyuanma.qingyun.common.ext.func.MergeFunction;
import com.imyuanma.qingyun.common.ext.func.ResultHandleFunction;
import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.interfaces.common.ext.ExtNodeResult;
import com.imyuanma.qingyun.interfaces.common.ext.IExtNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.NonNull;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * 扩展点执行器
 *
 * @author wangjy
 * @date 2023/07/05 22:55:44
 */
public class ExtNodeExecutor<P, R> implements IExtNode<P, R> {
    private static final Logger logger = LoggerFactory.getLogger(ExtNodeExecutor.class);
    /**
     * 扩展点接口
     */
    private Class<? extends IExtNode<P, R>> extNodeClass;
    /**
     * 待执行的扩展点
     */
    private List<? extends IExtNode<P, R>> extList;
    /**
     * 中断检查函数
     * 默认总是中断(也就是只执行一个扩展点实现)
     */
    private MatchFunction<ExtNodeResult<R>> stopCheckFunction;
    /**
     * 结果合并函数
     * 默认合并结果为新结果
     */
    private MergeFunction<ExtNodeResult<R>> resultMergeFunction;
    /**
     * 扩展实现为空时抛出异常
     * 默认返回空实现失败
     */
    private boolean successIfEmpty;
    /**
     * 扩展实现为空时返回成功
     * 默认返回空实现失败
     */
    private boolean throwIfEmpty;
    /**
     * 扩展实现为空时默认返回结果
     */
    private ExtNodeResult<R> defaultIfEmpty;
    /**
     * 默认扩展实现
     * 扩展实现为空时使用默认实现执行
     */
    private IExtNode<P, R> defaultImpl;
    /**
     * 结果处理
     * 结果处理和异常处理只会有一种被执行
     */
    private List<ResultHandleFunction<R>> resultHandleFunctionList;
    /**
     * 异常处理
     * 结果处理和异常处理只会有一种被执行
     */
    private List<ExceptionHandleFunction> exceptionHandleFunctionList;

    /**
     * 带参构造器
     *
     * @param extList
     */
    private ExtNodeExecutor(Class<? extends IExtNode<P, R>> extNodeClass, List<? extends IExtNode<P, R>> extList) {
        this.extNodeClass = extNodeClass;
        this.extList = extList;
    }

    /**
     * 构造执行器对象
     *
     * @param extNodeClass
     * @param <P>
     * @param <R>
     * @return
     */
    public static <P, R> ExtNodeExecutor<P, R> of(@NonNull Class<? extends IExtNode<P, R>> extNodeClass) {
        List<? extends IExtNode<P, R>> list = ExtNodeExecuteUtil.extNodeList(extNodeClass);
        if (CollectionUtil.isNotEmpty(list)) {
            list.sort(Comparator.comparingLong(IExtNode::ordered));
        }
        return new ExtNodeExecutor<>(extNodeClass, list);
    }

    /**
     * 设置执行中断判断逻辑
     * 当存在多个扩展实现时,每执行一个扩展实现,都会使用指定的中断函数判断是否结束
     *
     * @param matchFunction
     * @return
     */
    public ExtNodeExecutor<P, R> stop(MatchFunction<ExtNodeResult<R>> matchFunction) {
        this.stopCheckFunction = matchFunction;
        return this;
    }

    /**
     * 当返回成功时中断
     *
     * @param matchFunction
     * @return
     */
    public ExtNodeExecutor<P, R> stopWhenSuccess(MatchFunction<ExtNodeResult<R>> matchFunction) {
        this.stopCheckFunction = ExtNodeResult::isSuccess;
        return this;
    }

    /**
     * 当返回失败时中断
     *
     * @param matchFunction
     * @return
     */
    public ExtNodeExecutor<P, R> stopWhenError(MatchFunction<ExtNodeResult<R>> matchFunction) {
        this.stopCheckFunction = ExtNodeResult::isError;
        return this;
    }

    /**
     * 设置结果合并函数
     *
     * @param mergeFunction
     * @return
     */
    public ExtNodeExecutor<P, R> merge(MergeFunction<ExtNodeResult<R>> mergeFunction) {
        this.resultMergeFunction = mergeFunction;
        return this;
    }

    /**
     * 扩展实现为空时抛出异常
     *
     * @return
     */
    public ExtNodeExecutor<P, R> throwIfEmpty() {
        this.defaultImpl = null;
        this.defaultIfEmpty = null;
        this.throwIfEmpty = true;
        this.successIfEmpty = false;
        return this;
    }

    /**
     * 扩展实现为空时返回成功
     *
     * @return
     */
    public ExtNodeExecutor<P, R> successIfEmpty() {
        this.defaultImpl = null;
        this.defaultIfEmpty = null;
        this.throwIfEmpty = false;
        this.successIfEmpty = true;
        return this;
    }

    /**
     * 扩展实现为空时返回默认结果
     *
     * @param extNodeResult 默认返回结果
     * @return
     */
    public ExtNodeExecutor<P, R> defaultIfEmpty(@NonNull ExtNodeResult<R> extNodeResult) {
        this.defaultImpl = null;
        this.defaultIfEmpty = extNodeResult;
        this.throwIfEmpty = false;
        this.successIfEmpty = false;
        return this;
    }

    public ExtNodeExecutor<P, R> defaultImpl(@NonNull ExtNodeExecutor<P, R> defaultImpl) {
        this.defaultImpl = defaultImpl;
        this.defaultIfEmpty = null;
        this.throwIfEmpty = false;
        this.successIfEmpty = false;
        return this;
    }

    /**
     * 结果处理
     *
     * @param resultHandleFunction
     * @return
     */
    public ExtNodeExecutor<P, R> resultHandle(ResultHandleFunction<R> resultHandleFunction) {
        if (resultHandleFunctionList == null) {
            resultHandleFunctionList = new ArrayList<>();
        }
        resultHandleFunctionList.add(resultHandleFunction);
        return this;
    }

    /**
     * 结果为成功时的处理
     *
     * @param successResultHandleFunction
     * @return
     */
    public ExtNodeExecutor<P, R> successHandle(ResultHandleFunction<R> successResultHandleFunction) {
        return resultHandle(t -> {
            if (t.isSuccess()) {
                successResultHandleFunction.handle(t);
            }
        });
    }

    /**
     * 结果为失败时的处理
     *
     * @param errorResultHandleFunction
     * @return
     */
    public ExtNodeExecutor<P, R> errorHandle(ResultHandleFunction<R> errorResultHandleFunction) {
        return resultHandle(t -> {
            if (t.isError()) {
                errorResultHandleFunction.handle(t);
            }
        });
    }

    /**
     * 结果为失败并且不是空扩展实现失败时的处理
     *
     * @param errorResultHandleFunction
     * @return
     */
    public ExtNodeExecutor<P, R> nonEmptyErrorHandle(ResultHandleFunction<R> errorResultHandleFunction) {
        return resultHandle(t -> {
            if (t.isError() && !t.isEmptyImpl()) {
                errorResultHandleFunction.handle(t);
            }
        });
    }

    /**
     * 异常处理
     *
     * @param exceptionHandleFunction
     * @return
     */
    public ExtNodeExecutor<P, R> exceptionHandle(ExceptionHandleFunction exceptionHandleFunction) {
        if (exceptionHandleFunctionList == null) {
            exceptionHandleFunctionList = new ArrayList<>();
        }
        exceptionHandleFunctionList.add(exceptionHandleFunction);
        return this;
    }


    /**
     * 执行扩展点
     *
     * @param extNodeParam
     * @return
     */
    @Override
    public ExtNodeResult<R> execute(P extNodeParam) {
        ExtNodeResult<R> extNodeResult;
        try {
            extNodeResult = doExecute(extNodeParam);
        } catch (Throwable t) {
            if (CollectionUtil.isNotEmpty(exceptionHandleFunctionList)) {
                for (ExceptionHandleFunction exceptionHandleFunction : exceptionHandleFunctionList) {
                    exceptionHandleFunction.handle(t);
                }
                return ExtNodeResult.exceptionError(t);
            } else {
                throw t;
            }
        }
        if (CollectionUtil.isNotEmpty(resultHandleFunctionList)) {
            for (ResultHandleFunction<R> resultHandleFunction : resultHandleFunctionList) {
                resultHandleFunction.handle(extNodeResult);
            }
        }
        return extNodeResult;
    }

    /**
     * 执行扩展点
     *
     * @param extNodeParam
     * @return
     */
    private ExtNodeResult<R> doExecute(P extNodeParam) {
        if (CollectionUtil.isEmpty(extList)) {
            return emptyImplResult(extNodeParam);
        }
        ExtNodeResult<R> mergeResult = null;
        for (IExtNode<P, R> extNode : extList) {
            if (extNode.filter(extNodeParam)) {
                // 执行扩展点
                ExtNodeResult<R> result = extNode.execute(extNodeParam);
                // 合并结果
                mergeResult = doMergeFunc(mergeResult, result);
                // 中断则直接返回
                if (doStopFunc(mergeResult)) {
                    return mergeResult;
                }
            } else {
                logger.debug("[执行扩展点] 扩展点[{}]不执行(filter()返回false)", extNode.getClass());
            }
        }
        return mergeResult != null ? mergeResult : emptyImplResult(extNodeParam);
    }

    /**
     * 返回空实现结果
     *
     * @return
     */
    private ExtNodeResult<R> emptyImplResult(P extNodeParam) {
        if (this.throwIfEmpty) {
            throw new BaseException("扩展点实现为空:" + extNodeClass.getName());
        }
        if (this.successIfEmpty) {
            return ExtNodeResult.success();
        }
        if (this.defaultIfEmpty != null) {
            return this.defaultIfEmpty;
        }
        if (this.defaultImpl != null) {
            return this.defaultImpl.execute(extNodeParam);
        }
        return ExtNodeResult.emptyImplError();
    }

    /**
     * 结果合并
     *
     * @param o
     * @param n
     * @return
     */
    private ExtNodeResult<R> doMergeFunc(ExtNodeResult<R> o, ExtNodeResult<R> n) {
        if (o == null || resultMergeFunction == null) {
            return n;
        }
        return resultMergeFunction.merge(o, n);
    }

    /**
     * 中断判断
     *
     * @param result
     * @return true表示中断
     */
    private boolean doStopFunc(ExtNodeResult<R> result) {
        return stopCheckFunction == null || stopCheckFunction.match(result);
    }
}
