package com.imyuanma.qingyun.common.ext.ums;

import com.imyuanma.qingyun.common.util.AssertUtil;
import com.imyuanma.qingyun.common.util.MailUtil;
import com.imyuanma.qingyun.interfaces.common.ext.ExtNodeResult;
import com.imyuanma.qingyun.interfaces.ums.ext.UmsSendFindPwdMsgExt;
import com.imyuanma.qingyun.interfaces.ums.ext.param.SendFindPwdMsgParam;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.function.Function;

/**
 * 发送密码找回邮件消息
 *
 * @author wangjy
 * @date 2023/05/03 15:47:32
 */
public class UmsSendFindPwdMsgExt4Mail implements UmsSendFindPwdMsgExt {
    /**
     * 发送邮箱
     */
    private String email;
    /**
     * 发送邮箱密码
     */
    private String pwd;
    /**
     * 端口号
     */
    private String host;
    /**
     * 应用名
     */
    private String appName;
    /**
     * 邮件消息构造器
     */
    private Function<SendFindPwdMsgParam, String> msgBuilder;
    /**
     * 邮件标题构造器
     */
    private Function<SendFindPwdMsgParam, String> titleBuilder;
    private JavaMailSenderImpl sender;

//    private static final String CONTENT_TEMP = "您好，您正在发起密码找回，请点击右侧链接重置您的密码：%s。";

    public UmsSendFindPwdMsgExt4Mail(String email, String pwd, String host, String appName, Function<SendFindPwdMsgParam, String> msgBuilder) {
        this.email = email;
        this.pwd = pwd;
        this.host = host;
        this.appName = appName;
        this.msgBuilder = msgBuilder;
        this.titleBuilder = sendFindPwdMsgParam -> "找回密码";
        this.sender = MailUtil.newMailSender(host, email, pwd);
    }

    public UmsSendFindPwdMsgExt4Mail(String email, String pwd, String host, String appName, Function<SendFindPwdMsgParam, String> msgBuilder, Function<SendFindPwdMsgParam, String> titleBuilder) {
        this.email = email;
        this.pwd = pwd;
        this.host = host;
        this.appName = appName;
        this.msgBuilder = msgBuilder;
        this.titleBuilder = titleBuilder;
        this.sender = MailUtil.newMailSender(host, email, pwd);
    }

    /**
     * 执行扩展点
     *
     * @param extNodeParam
     * @return
     */
    @Override
    public ExtNodeResult<Boolean> execute(SendFindPwdMsgParam extNodeParam) {
        AssertUtil.notBlank(extNodeParam.getUserBaseInfo().getEmail(), "用户尚未绑定邮箱");
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(String.format("%s<%s>", appName, email));
        message.setTo(extNodeParam.getUserBaseInfo().getEmail());
        message.setSubject(titleBuilder.apply(extNodeParam));
        message.setText(msgBuilder.apply(extNodeParam));
        sender.send(message);
        return ExtNodeResult.success(true);
    }


}
