package com.imyuanma.qingyun.common.exception;

import com.imyuanma.qingyun.common.model.IResultCode;

import java.util.function.Consumer;

/**
 * 异常类工具
 *
 * @author wangjy
 * @date 2022/05/07 20:52:35
 */
public class Exceptions {


    /**
     * 构造基础异常
     *
     * @param info 错误信息
     * @return
     */
    public static BaseException baseException(String info) {
        return new BaseException(info);
    }

    /**
     * 构造基础异常
     *
     * @param resultCode 错误码
     * @return
     */
    public static BaseException baseException(IResultCode resultCode) {
        return new BaseException(resultCode);
    }

    /**
     * 构造参数异常
     *
     * @param info 错误信息
     * @return
     */
    public static ParamException paramException(String info) {
        return new ParamException(info);
    }

    /**
     * 构造异常链消息
     *
     * @param t
     * @return
     */
    public static String exceptionCauseMessage(Throwable t) {
        StringBuilder msg = new StringBuilder(t.getClass().getName()).append(": ").append(t.getMessage());
        Throwable cause = t.getCause();
        if (cause != null) {
            msg.append("; Cause by: ").append(cause.getClass().getName()).append(": ").append(exceptionCauseMessage(cause));
        }
        return msg.toString();
    }

    /**
     * try catch 执行某动作
     *
     * @param runnable 待执行动作
     * @param consumer 动作出现异常时的处理
     */
    public static void tryCatch(Runnable runnable, Consumer<Throwable> consumer) {
        try {
            runnable.run();
        } catch (Throwable t) {
            if (consumer != null) {
                consumer.accept(t);
            }
        }
    }
}
