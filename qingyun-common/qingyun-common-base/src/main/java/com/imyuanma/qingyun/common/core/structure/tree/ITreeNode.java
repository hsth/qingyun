package com.imyuanma.qingyun.common.core.structure.tree;

/**
 * 树节点
 *
 * @author wangjy
 * @date 2022/05/01 20:46:32
 */
public interface ITreeNode<T> {
    /**
     * 当前节点id
     *
     * @return
     */
    T nodeId();

    /**
     * 父节点id
     *
     * @return
     */
    T parentNodeId();

    /**
     * 添加子节点
     *
     * @param node 节点对象
     */
    void appendChildren(ITreeNode<T> node);
}
