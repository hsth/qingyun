package com.imyuanma.qingyun.common.util.constants;

/**
 * 常量
 *
 * @author wangjy
 * @date 2021/12/28 00:19:04
 */
public class CommonConstants {
    /**
     * 编码方式
     */
    public static final String CHARSET = "UTF-8";
    /**
     * 全链路追踪的key值
     */
    public static final String TRACE_KEY = "_TRACE_CODE";
    /**
     * MDC的入口key
     */
    public static final String MDC_KEY_ENTRANCE = "ENTRANCE";
    /**
     * MDC用户id的key
     */
    public static final String MDC_KEY_USER_ID = "USER_ID";
}
