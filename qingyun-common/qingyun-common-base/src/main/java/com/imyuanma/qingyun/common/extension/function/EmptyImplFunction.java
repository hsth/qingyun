package com.imyuanma.qingyun.common.extension.function;

/**
 * 空实现处理
 *
 * @author wangjy
 * @date 2024/03/08 23:42:41
 */
@FunctionalInterface
public interface EmptyImplFunction {

    void handle();

    /**
     * 抛出异常
     *
     * @return
     */
    static EmptyImplFunction throwException() {
        return () -> {
            throw new UnsupportedOperationException("No implementation found");
        };
    }

    /**
     * 不处理
     *
     * @return
     */
    static EmptyImplFunction none() {
        return () -> {
        };
    }
}
