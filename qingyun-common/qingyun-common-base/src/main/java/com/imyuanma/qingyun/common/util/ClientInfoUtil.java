package com.imyuanma.qingyun.common.util;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.StringTokenizer;

/**
 * 客户端信息获取工具栏
 *
 * @author rookie
 * @Description: 获取客户端的信息
 * @date 2016-03-29
 */
public class ClientInfoUtil {
    /**
     * 获取客户端计算机名称
     *
     * @return
     */
    public static String getClientComputerName() {
        BufferedReader br = null;
        InputStream is = null;
        Process pro = null;
        String message = null;
        try {
            pro = Runtime.getRuntime().exec("cmd.exe /c ipconfig/all");
            is = pro.getInputStream();
            br = new BufferedReader(new InputStreamReader(is));
            message = br.readLine();
            while (message != null) {
                if (message.indexOf(".") > 0) {
                    message = message.substring(36, message.length());
                    break;
                }
                message = br.readLine();
            }
            br.close();
            is.close();
            pro.destroy();
        } catch (IOException e) {
            System.out.println("Can't get mac address!");
            return null;
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
                if (is != null) {
                    is.close();
                }
                if (pro != null) {
                    pro.destroy();
                }
            } catch (IOException e) {

            }
        }
        return message;
    }

    /**
     * 获取客户端IP地址，考虑到含有代理的情况
     *
     * @return
     */
    public static String getClientIpAddr(HttpServletRequest request) {
        String ipAddress = request.getHeader("x-forwarded-for");
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("Proxy-Client-IP");
        }

        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("WL-Proxy-Client-IP");
        }

        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("HTTP_CLIENT_IP");
        }

        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("HTTP_X_FORWARDED_FOR");
        }

        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getRemoteAddr();
			/*if(ipAddress.equals("127.0.0.1") || ipAddress.equals("0:0:0:0:0:0:0:1")) {
				InetAddress inet = null;
				try {
					//根据网卡取本机配置的IP
					inet = InetAddress.getLocalHost();
				} catch (UnknownHostException var4) {
					var4.printStackTrace();
				}
				ipAddress = inet.getHostAddress();
			}*/
        }
        //对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
        if (ipAddress != null && ipAddress.length() > 15 && ipAddress.indexOf(",") > 0) {
            ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
        }
        return ipAddress;
    }

    /**
     * 获取操作系统的版本
     *
     * @return
     */
    public static String getClientOS(HttpServletRequest request) {
        String Agent = request.getHeader("User-Agent");
        StringTokenizer st = new StringTokenizer(Agent, ";");
        if (st != null) {
            if (st.hasMoreTokens()) {
                st.nextToken();
            }
            if (st.hasMoreTokens()) {
                st.nextToken();
            }
            if (st.hasMoreTokens()) {
                return st.nextToken();
            }
        }
        return "";
    }

    /**
     * 获取浏览器系统的版本
     *
     * @return
     */
    public static String getClientBrowser(HttpServletRequest request) {
        String Agent = request.getHeader("User-Agent");
        if (Agent == null) return null;
        StringTokenizer st = new StringTokenizer(Agent, ";");
        if (st == null) return null;
        if (st.hasMoreTokens()) {
            st.nextToken();
        }
        if (st.hasMoreTokens()) {
            return st.nextToken();
        }
        return "";
    }

    /**
     * 获取发起请求的完整路径
     *
     * @param request
     * @return
     */
    public static String getClientFullURL(HttpServletRequest request) {
        StringBuffer url = request.getRequestURL();
        if (request.getQueryString() != null) {
            url.append('?');
            url.append(request.getQueryString());
        }
        return url.toString();
    }

    /**
     * 获取MAC地址
     *
     * @param ip
     * @return
     */
    public static String getMACAddress(String ip) {
        String str = "";
        String macAddress = "";
        try {
            Process p = Runtime.getRuntime().exec("nbtstat -A " + ip);
            InputStreamReader ir = new InputStreamReader(p.getInputStream());
            LineNumberReader input = new LineNumberReader(ir);
            for (int i = 1; i < 100; i++) {
                str = input.readLine();
                if (str != null) {
                    if (str.indexOf("MAC Address") > 1) {
                        macAddress = str.substring(
                                str.indexOf("MAC Address") + 14, str.length());
                        break;
                    }
                }
            }
        } catch (IOException e) {

        }
        return macAddress;
    }


    /**
     * 获取请求所属页面url
     * 例如:http://localhost:8080/page/ums/userList.html?_t=1543068105037
     *
     * @param request
     * @return
     */
    public static String getReferer(HttpServletRequest request) {
        if (request == null) {
            return null;
        }
        return request.getHeader("Referer");
    }

    /**
     * 获取request中的Origin
     * 只有post请求才有此请求头,例如:http://localhost:8080
     * 到了端口号就没了
     *
     * @param request
     * @return
     */
    public static String getOrigin(HttpServletRequest request) {
        if (request == null) {
            return null;
        }
        return request.getHeader("Origin");
    }
}
