package com.imyuanma.qingyun.common.model.request;


import com.imyuanma.qingyun.common.model.PageQuery;
import com.imyuanma.qingyun.common.util.ClientInfoUtil;
import com.imyuanma.qingyun.common.util.JsonUtil;
import com.imyuanma.qingyun.interfaces.common.model.dto.ClientInfoDTO;

import javax.servlet.http.HttpServletRequest;

/**
 * web请求参数
 *
 * @author wangjy
 * @date 2022/06/24 10:35:41
 */
public class WebRequest<T> {
    /**
     * 业务入参对象
     */
    private T body;
    /**
     * 分页查询时的页码
     */
    private Integer pageNumber;
    /**
     * 分页查询时的分页大小
     */
    private Integer pageSize;
    /**
     * 应用名
     */
    private String appName;
    /**
     * 版本号
     */
    private String appVersion;
    /**
     * 设备号
     */
    private String deviceId;
    /**
     * uuid
     */
    private String uuid;
    /**
     * 设备类型
     */
    private String deviceType;
    /**
     * 设备型号 例如:华为meta60
     */
    private String deviceModel;
    /**
     * 客户端ip
     */
    private String ip;

    /**
     * 构造客户端信息
     *
     * @return
     */
    public ClientInfoDTO buildClientInfo() {
        ClientInfoDTO clientInfoDTO = new ClientInfoDTO();
        clientInfoDTO.setIp(ip);
        clientInfoDTO.setAppName(appName);
        clientInfoDTO.setAppVersion(appVersion);
        clientInfoDTO.setDeviceId(deviceId);
        clientInfoDTO.setDeviceType(deviceType);
        clientInfoDTO.setDeviceModel(deviceModel);
        return clientInfoDTO;
    }

    /**
     * 构造客户端信息
     *
     * @param request web请求
     * @return
     */
    public ClientInfoDTO buildClientInfo(HttpServletRequest request) {
        ClientInfoDTO clientInfoDTO = buildClientInfo();
        clientInfoDTO.setIp(ClientInfoUtil.getClientIpAddr(request));
        return clientInfoDTO;

    }

    /**
     * 构造分页参数
     *
     * @return
     */
    public PageQuery buildPageQuery() {
        return new PageQuery(pageNumber, pageSize);
    }

    public String toJsonString() {
        return JsonUtil.toJson(this);
    }

    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
