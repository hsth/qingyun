package com.imyuanma.qingyun.common.ext.func;

/**
 * 处理函数
 *
 * @author wangjy
 * @date 2023/07/07 23:19:42
 */
@FunctionalInterface
public interface HandleFunction<T> {
    /**
     * 处理
     *
     * @param t
     */
    void handle(T t);

    /**
     * 追加处理
     *
     * @param after
     * @return
     */
    default HandleFunction<T> andThen(HandleFunction<T> after) {
        return (T t) -> {
            handle(t);
            after.handle(t);
        };
    }

}
