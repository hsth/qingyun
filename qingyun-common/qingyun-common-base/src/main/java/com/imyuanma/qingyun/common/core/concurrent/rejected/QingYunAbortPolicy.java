package com.imyuanma.qingyun.common.core.concurrent.rejected;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * 拒绝策略:抛出异常
 *
 * @author wangjy
 * @date 2021/10/23 15:39:14
 */
public class QingYunAbortPolicy extends ThreadPoolExecutor.AbortPolicy {
    private static final Logger logger = LoggerFactory.getLogger(QingYunAbortPolicy.class);
    @Override
    public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {
        logger.info("[拒绝策略-YoungAbortPolicy] 执行拒绝策略:抛出异常.");
        super.rejectedExecution(r, e);
    }
}