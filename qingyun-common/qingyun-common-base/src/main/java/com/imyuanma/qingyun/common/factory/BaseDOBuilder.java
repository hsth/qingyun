package com.imyuanma.qingyun.common.factory;

import com.imyuanma.qingyun.common.client.ums.LoginUserHolder;
import com.imyuanma.qingyun.interfaces.common.model.BaseDO;
import com.imyuanma.qingyun.interfaces.common.model.enums.ETrashFlagEnum;
import com.imyuanma.qingyun.interfaces.ums.model.LoginUserDTO;

import java.util.Date;
import java.util.Map;

/**
 * BaseDO构建
 *
 * @author wangjy
 * @date 2022/05/21 18:12:06
 */
public class BaseDOBuilder {

    /**
     * 填充BaseDO
     *
     * @param baseDO 基础模型
     */
    public static void fillBaseDOForInsert(BaseDO baseDO) {
        baseDO.setCreateTime(new Date());
        baseDO.setUpdateTime(new Date());
        LoginUserDTO loginUserDTO = LoginUserHolder.getLoginUser();
        if (loginUserDTO != null) {
            baseDO.setCreateUserId(loginUserDTO.getUserId());
            baseDO.setCreateUserName(loginUserDTO.getName());
            baseDO.setUpdateUserId(loginUserDTO.getUserId());
            baseDO.setUpdateUserName(loginUserDTO.getName());
        }
        baseDO.setDataSequence(baseDO.getDataSequence() != null ? baseDO.getDataSequence() : 1L);
        baseDO.setDataVersion(1L);
        baseDO.setTrashFlag(ETrashFlagEnum.VALID.getType());
    }

    /**
     * 填充BaseDO
     *
     * @param condition 基础模型
     */
    public static void fillMapForInsert(Map<String, Object> condition) {
        condition.put("createTime", new Date());
        condition.put("updateTime", new Date());
        LoginUserDTO loginUserDTO = LoginUserHolder.getLoginUser();
        if (loginUserDTO != null) {
            condition.put("createUserId", loginUserDTO.getUserId());
            condition.put("createUserName", loginUserDTO.getName());
            condition.put("updateUserId", loginUserDTO.getUserId());
            condition.put("updateUserName", loginUserDTO.getName());
        }
        condition.putIfAbsent("dataSequence", 1L);
        condition.putIfAbsent("dataVersion", 1L);
        condition.putIfAbsent("trashFlag", ETrashFlagEnum.VALID.getType());
    }

    /**
     * 填充BaseDO
     *
     * @param baseDO 基础模型
     */
    public static void fillBaseDOForUpdate(BaseDO baseDO) {
        baseDO.setUpdateTime(new Date());
        LoginUserDTO loginUserDTO = LoginUserHolder.getLoginUser();
        if (loginUserDTO != null) {
            baseDO.setUpdateUserId(loginUserDTO.getUserId());
            baseDO.setUpdateUserName(loginUserDTO.getName());
        }
    }

    /**
     * 填充BaseDO
     *
     * @param condition 基础模型
     */
    public static void fillMapForUpdate(Map<String, Object> condition) {
        condition.put("updateTime", new Date());
        LoginUserDTO loginUserDTO = LoginUserHolder.getLoginUser();
        if (loginUserDTO != null) {
            condition.put("updateUserId", loginUserDTO.getUserId());
            condition.put("updateUserName", loginUserDTO.getName());
        }
    }
}
