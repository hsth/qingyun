package com.imyuanma.qingyun.common.db.dal;

/**
 * postgres数据库的分页sql构建者
 * @author wangjiyu@imdada.cn
 * @create 2018/10/9
 */
public class PostgresPageSqlBuilder implements IPageSqlBuilder {

    /**
     * 返回count语句
     *
     * @param originalSql 原始的sql
     * @return
     */
    @Override
    public String getCountSql(String originalSql) {
        return "select count(*) total_count from (" + originalSql + ") temp_count";
    }

    /**
     * 返回分页语句
     *
     * @param originalSql 原始的sql
     * @param pageNumber  第几页
     * @param pageSize    每页多少条
     * @return
     */
    @Override
    public String getPageSql(String originalSql, int pageNumber, int pageSize) {
        StringBuilder sb = new StringBuilder(100);
        sb.append(originalSql).append(" LIMIT ").append(pageSize).append(" offset ").append((pageNumber - 1) * pageSize);
        return sb.toString();
    }
}
