package com.imyuanma.qingyun.common.db.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 数据源
 * 动态数据源场景下, 用来指定方法使用的数据源
 *
 * @author wangjy
 * @date 2024/05/17 22:21:33
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface DS {
    /**
     * 数据源key
     *
     * @return
     */
    String value() default "";

}
