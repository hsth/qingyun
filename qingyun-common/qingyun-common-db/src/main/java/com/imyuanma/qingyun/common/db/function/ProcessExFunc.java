package com.imyuanma.qingyun.common.db.function;

/**
 * 执行
 *
 * @author wangjy
 * @date 2024/05/18 12:47:41
 */
@FunctionalInterface
public interface ProcessExFunc<T> {

    T process() throws Throwable;
}
