package com.imyuanma.qingyun.common.db.aspect;

import com.imyuanma.qingyun.common.db.annotation.DS;
import com.imyuanma.qingyun.common.db.datasource.DynamicDataSource;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;

import java.lang.reflect.Method;

/**
 * 动态数据源切面
 *
 * @author wangjy
 * @date 2024/05/17 23:19:04
 */
@Aspect
public class DynamicDataSourceAspect implements Ordered {
    private static final Logger logger = LoggerFactory.getLogger(DynamicDataSourceAspect.class);

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }

    /**
     * 切入点
     * 使用了DS注解的方法
     */
    @Pointcut("@annotation(com.imyuanma.qingyun.common.db.annotation.DS)")
    public void pointcut() {
    }

    /**
     * 切入点2
     * 使用了DS注解的类下所有的public方法方法
     */
    @Pointcut("execution(public * (@com.imyuanma.qingyun.common.db.annotation.DS *).*(..))")
    public void pointcut2() {
    }

    /**
     * 环绕通知
     *
     * @param joinPoint
     * @return
     */
    @Around("pointcut() || pointcut2()")
    public Object around(JoinPoint joinPoint) throws Throwable {
        // 动态数据源注解
        DS ds = this.parse(joinPoint);
        if (ds == null) {
            logger.warn("[DynamicDataSourceAspect] 获取DS注解为空,请检查切入点是否设置正确!");
            return ((ProceedingJoinPoint) joinPoint).proceed();
        }
        return DynamicDataSource.executeEx(ds.value(), () -> ((ProceedingJoinPoint) joinPoint).proceed());
    }

    /**
     * 解析DS注解
     *
     * @param joinPoint
     * @return
     */
    private DS parse(JoinPoint joinPoint) {
        try {
            // 获取目标方法的标记
            MethodSignature ms = (MethodSignature) joinPoint.getSignature();
            // 目标方法
            Method method = ms.getMethod();
            // 获取方法上的DS注解
            DS ds = method.getAnnotation(DS.class);
            if (ds == null) {
                // 获取方法对应的类
                Class<?> clz = method.getDeclaringClass();
                ds = clz.getAnnotation(DS.class);
            }
            return ds;
        } catch (Throwable t) {
            logger.error("[DynamicDataSourceAspect] 获取DS注解异常");
        }
        return null;
    }
}
