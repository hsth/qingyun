package com.imyuanma.qingyun.common.db.dal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 分页sql构建器工厂
 *
 * @author wangjiyu@imdada.cn
 * @create 2018/10/9
 */
public class PageSqlBuilderFactory {

    private static final Logger logger = LoggerFactory.getLogger(PageSqlBuilderFactory.class);

    /**
     * 获取构建器
     *
     * @param edbType 数据库类型,例如:mysql,oracle
     * @return
     */
    public static IPageSqlBuilder getBuilder(EDBType edbType) {
        return getBuilder(edbType.name());
    }

    /**
     * 获取构建器
     *
     * @param db 数据库类型,例如:mysql,oracle
     * @return
     */
    public static IPageSqlBuilder getBuilder(String db) {
        if (EDBType.MYSQL.name().equalsIgnoreCase(db)) {
            return new MysqlPageSqlBuilder();
        } else if (EDBType.ORACLE.name().equalsIgnoreCase(db)) {
            return new OraclePageSqlBuilder();
        } else if (EDBType.POSTGRESQL.name().equalsIgnoreCase(db)) {
            return new PostgresPageSqlBuilder();
        } else {
            logger.warn("[分页sql构建器工厂] 根据db={}匹配不到已知的sql构造器,已返回默认的sql构造器!若这不是您想要的,请在mybatis-config的分页插件配置中配置正确的db值,取值参考:com.young.common.core.dal.EDBType", db);
            return getDefaultBuilder();
        }
    }

    /**
     * 获取默认的sql构建器
     *
     * @return 默认返回mysql构建器
     */
    public static IPageSqlBuilder getDefaultBuilder() {
        return new MysqlPageSqlBuilder();
    }
}
