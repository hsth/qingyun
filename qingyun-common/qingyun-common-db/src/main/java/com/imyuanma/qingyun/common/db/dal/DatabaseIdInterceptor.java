package com.imyuanma.qingyun.common.db.dal;

import com.alibaba.druid.pool.DruidDataSource;
import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.common.util.StringUtil;
import org.apache.ibatis.cache.CacheKey;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;
import java.util.Properties;

/**
 * 数据库ID拦截器，用于拦截数据库操作，设置数据库ID
 *
 * @author wangjy
 * @date 2024/05/05 23:26:06
 */
@Intercepts({
        @Signature(method = "update", type = Executor.class, args = {MappedStatement.class, Object.class})
        , @Signature(method = "query", type = Executor.class, args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class})
        , @Signature(method = "query", type = Executor.class, args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class, CacheKey.class, BoundSql.class})
})
public class DatabaseIdInterceptor implements Interceptor {
    private static final Logger logger = LoggerFactory.getLogger(DatabaseIdInterceptor.class);

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        MappedStatement mappedStatement = parseMappedStatement(invocation);
        Configuration configuration = parseConfiguration(mappedStatement);
        String dbType = parseDbType(configuration);
        if (mappedStatement == null || configuration == null || StringUtil.isBlank(dbType)) {
            return invocation.proceed();
        }
        // 原db类型
        String originalDbType = configuration.getDatabaseId();
        // 设置为新的
        configuration.setDatabaseId(dbType);
        logger.debug("[DatabaseIdInterceptor] mappedStatement:{},原databaseId:{},现databaseId:{}", mappedStatement.getId(), originalDbType, dbType);
        try {
            return invocation.proceed();
        } finally {
            // 恢复原db类型
            configuration.setDatabaseId(originalDbType);
        }
    }

    private MappedStatement parseMappedStatement(Invocation invocation) {
        return (MappedStatement) Optional.ofNullable(invocation.getArgs())
                .filter(CollectionUtil::isNotEmpty)
                .map(args -> args[0])
                .filter(item -> item instanceof MappedStatement)
                .orElse(null);
    }

    private Configuration parseConfiguration(MappedStatement mappedStatement) {
        return Optional.ofNullable(mappedStatement).map(MappedStatement::getConfiguration).orElse(null);
    }

    private String parseDbType(Configuration configuration) {
        if (configuration != null && configuration.getEnvironment() != null) {
            // 数据库类型
            return Optional.ofNullable(configuration.getEnvironment())
                    .map(Environment::getDataSource)
                    .filter(ds -> ds instanceof DruidDataSource)
                    .map(ds -> (DruidDataSource) ds)
                    .map(DruidDataSource::getDbType)
                    .map(String::toLowerCase)
                    .orElse(null);

        }
        return null;
    }

    @Override
    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }

    @Override
    public void setProperties(Properties properties) {
    }
}
