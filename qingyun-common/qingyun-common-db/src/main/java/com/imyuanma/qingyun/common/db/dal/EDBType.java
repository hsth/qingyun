package com.imyuanma.qingyun.common.db.dal;

/**
 * 数据库类型枚举
 */
public enum EDBType {
    ORACLE, MYSQL, POSTGRESQL, OTHER;

    /**
     * 根据数据库类型获取枚举值
     *
     * @param dbType
     * @return
     */
    public static EDBType of(String dbType) {
        for (EDBType e : EDBType.values()) {
            if (e.name().equalsIgnoreCase(dbType)) {
                return e;
            }
        }
        return OTHER;
    }
}
