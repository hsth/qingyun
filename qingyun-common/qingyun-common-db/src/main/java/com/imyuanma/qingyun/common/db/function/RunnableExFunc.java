package com.imyuanma.qingyun.common.db.function;

/**
 * 执行
 *
 * @author wangjy
 * @date 2024/05/18 12:58:58
 */
public interface RunnableExFunc {
    void run() throws Throwable;
}
