package com.imyuanma.qingyun.common.db.dal;

/**
 * 分页sql构造接口
 * @author wangjiyu@imdada.cn
 * @create 2018/9/6
 */
public interface IPageSqlBuilder {

    /**
     * 返回count语句
     * @param originalSql 原始的sql
     * @return
     */
    String getCountSql(String originalSql);

    /**
     * 返回分页语句
     * @param originalSql 原始的sql
     * @param pageNumber 第几页
     * @param pageSize 每页多少条
     * @return
     */
    String getPageSql(String originalSql, int pageNumber, int pageSize);
}
