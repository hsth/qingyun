package com.imyuanma.qingyun.common.db.configuration;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import com.imyuanma.qingyun.common.db.aspect.DynamicDataSourceAspect;
import com.imyuanma.qingyun.common.db.datasource.DynamicDataSource;
import com.imyuanma.qingyun.common.util.CollectionUtil;
import com.imyuanma.qingyun.common.util.StringUtil;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * 动态数据源配置
 *
 * @author wangjy
 * @date 2024/05/18 11:51:34
 */
@Configuration
@ConditionalOnProperty(
        name = "qingyun.datasource.dynamic.enable",
        havingValue = "true"
)
@ConditionalOnMissingBean(DataSource.class)
@ConfigurationProperties("qingyun.datasource.dynamic")
@EnableAspectJAutoProxy
public class DynamicDataSourceConfiguration {
    /**
     * 默认库
     */
    private String primary;
    /**
     * 数据源配置
     */
    private Map<String, Properties> datasource;

    @Bean(name = "dataSource")
    @Autowired
    public DataSource dataSource(DynamicDataSourceConfiguration dynamicDataSourceConfiguration) {
        if (CollectionUtil.isEmpty(dynamicDataSourceConfiguration.datasource)) {
            throw new IllegalArgumentException("请检查动态数据源配置是否有效:qingyun.datasource.dynamic.datasource");
        }
        if (StringUtil.isBlank(dynamicDataSourceConfiguration.primary)) {
            throw new IllegalArgumentException("请检查默认数据源配置是否有效:qingyun.datasource.dynamic.primary");
        }
        DynamicDataSource dataSource = new DynamicDataSource();
        Map<Object, Object> targetDataSources = new HashMap<>();
        for (Map.Entry<String, Properties> entry : dynamicDataSourceConfiguration.datasource.entrySet()) {
            targetDataSources.put(entry.getKey(), createDataSource(entry.getValue()));
        }
        dataSource.setTargetDataSources(targetDataSources);
        dataSource.setDefaultDataSourceKey(dynamicDataSourceConfiguration.getPrimary());
        return dataSource;
    }

    @Bean
    public DynamicDataSourceAspect dynamicDataSourceAspect() {
        return new DynamicDataSourceAspect();
    }

    private DataSource createDataSource(Properties properties) {
        DruidDataSource dataSource = DruidDataSourceBuilder.create().build();
        dataSource.configFromPropety(properties);
        return dataSource;
    }

    public String getPrimary() {
        return primary;
    }

    public void setPrimary(String primary) {
        this.primary = primary;
    }

    public Map<String, Properties> getDatasource() {
        return datasource;
    }

    public void setDatasource(Map<String, Properties> datasource) {
        this.datasource = datasource;
    }
}
