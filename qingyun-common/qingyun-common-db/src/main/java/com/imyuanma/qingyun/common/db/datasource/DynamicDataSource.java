package com.imyuanma.qingyun.common.db.datasource;

import com.imyuanma.qingyun.common.db.function.ProcessExFunc;
import com.imyuanma.qingyun.common.db.function.ProcessFunc;
import com.imyuanma.qingyun.common.db.function.RunnableExFunc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import java.util.Optional;

/**
 * 动态数据源
 *
 * @author wangjy
 * @date 2024/05/17 21:58:25
 */
public class DynamicDataSource extends AbstractRoutingDataSource {
    private static final Logger logger = LoggerFactory.getLogger(DynamicDataSource.class);
    /**
     * 数据源上下文
     * 用于存储当前线程使用的数据源key
     */
    private static final ThreadLocal<String> CONTEXT_HOLDER = new ThreadLocal<>();
    /**
     * 默认数据源key
     */
    private String defaultDataSourceKey;

    @Override
    protected Object determineCurrentLookupKey() {
        String dsKey = Optional.ofNullable(CONTEXT_HOLDER.get()).orElse(defaultDataSourceKey);
        logger.debug("[DynamicDataSource] determine DataSource key: {}", dsKey);
        return dsKey;
    }

    /**
     * 使用指定数据源执行方法
     *
     * @param dataSourceKey
     * @param runnable
     */
    public static void execute(String dataSourceKey, Runnable runnable) {
        execute(dataSourceKey, () -> {
            runnable.run();
            return null;
        });
    }

    /**
     * 使用指定数据源执行方法
     *
     * @param dataSourceKey
     * @param runnable
     * @throws Throwable
     */
    public static void executeEx(String dataSourceKey, RunnableExFunc runnable) throws Throwable {
        executeEx(dataSourceKey, () -> {
            runnable.run();
            return null;
        });
    }

    /**
     * 使用指定数据源执行方法
     *
     * @param dataSourceKey
     * @param process
     * @param <T>
     * @return
     */
    public static <T> T execute(String dataSourceKey, ProcessFunc<T> process) {
        String oldDataSource = DynamicDataSource.getDataSourceKey();
        DynamicDataSource.setDataSource(dataSourceKey);
        try {
            return process.process();
        } finally {
            DynamicDataSource.setDataSource(oldDataSource);
        }
    }

    /**
     * 使用指定数据源执行方法
     *
     * @param dataSourceKey
     * @param process
     * @param <T>
     * @return
     * @throws Throwable
     */
    public static <T> T executeEx(String dataSourceKey, ProcessExFunc<T> process) throws Throwable {
        String oldDataSource = DynamicDataSource.getDataSourceKey();
        DynamicDataSource.setDataSource(dataSourceKey);
        try {
            return process.process();
        } finally {
            DynamicDataSource.setDataSource(oldDataSource);
        }
    }

    /**
     * 设置数据源
     *
     * @param dataSourceKey 数据源key
     */
    public static void setDataSource(String dataSourceKey) {
        CONTEXT_HOLDER.set(dataSourceKey);
        logger.debug("[DynamicDataSource] setDataSource key: {}", dataSourceKey);
    }

    /**
     * 移除数据源
     */
    public static void removeDataSource() {
        CONTEXT_HOLDER.remove();
    }

    /**
     * 获取数据源key
     *
     * @return 当前使用的数据源key
     */
    public static String getDataSourceKey() {
        return CONTEXT_HOLDER.get();
    }

    public String getDefaultDataSourceKey() {
        return defaultDataSourceKey;
    }

    public void setDefaultDataSourceKey(String defaultDataSourceKey) {
        this.defaultDataSourceKey = defaultDataSourceKey;
    }
}
